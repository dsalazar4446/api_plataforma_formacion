'use strict';

let array = []

function setSequenceAttachment(sequnceId, attachmentId) {
  const data = {
    class_sequence_id: sequnceId,
    class_attachment_id: attachmentId,
    created_at: new Date(),
    updated_at: new Date()
  }
  return data;
}



module.exports = {
  up: async (queryInterface, Sequelize) => {
    const query1 = await queryInterface.sequelize.query(
      `SELECT id FROM class_sequences;`
    )
    const query2 = await queryInterface.sequelize.query(
      `SELECT id FROM class_attachments;`
    )
    const sequences = query1[0]
    const attachments = query2[0]
    sequences.forEach(sequence => {
      attachments.forEach(attachment => {
        array.push(setSequenceAttachment(sequence.id, attachment.id))
      })
    });

    return queryInterface.bulkInsert('cl_sequences_h_cl_attachments', array, {});

  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('cl_sequences_h_cl_attachments', null, {});
  }
}
