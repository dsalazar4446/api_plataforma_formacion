'use strict';

const faker = require('faker');

let array = [];

function setQuestionAnswers(formationId, userId) {
  return {
    formation_class_id: formationId,
    user_id: userId,
    question_answer: faker.lorem.words(5),
    usefull: faker.random.boolean(),
    question_answer_status: faker.random.boolean(),
    created_at: new Date(),
    updated_at: new Date(),
  }
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const query1 = await queryInterface.sequelize.query(
      'SELECT id FROM  users'
    );
    const users = query1[0]
    const query2 = await queryInterface.sequelize.query(
      'SELECT id FROM  formation_classes'
    );
    const classes = query2[0]
    classes.forEach(classe => {
      array.push(setQuestionAnswers(classe.id, users[0].id))
    });
    // console.log(array)
    return queryInterface.bulkInsert('user_class_questions_answers', array, {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_class_questions_answers', null, {});
  }
};
