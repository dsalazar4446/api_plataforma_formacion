'use strict';

const faker = require('faker')


let program = [
  {
    category_id: "",
    program_status: "1",
    program_code: "CODE1",
    program_background: faker.image.imageUrl(),
    program_icon: faker.image.imageUrl(),
    fb_promotion_img: faker.image.imageUrl(),
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    category_id: "",
    program_code: "CODE2",
    program_status: "1",
    program_background: faker.image.imageUrl(),
    program_icon: faker.image.imageUrl(),
    fb_promotion_img: faker.image.imageUrl(),
    created_at: new Date(),
    updated_at: new Date(),
  },
] 

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const categories = await queryInterface.sequelize.query(
      `SELECT id FROM categories WHERE language='es' AND category_target='formation' AND category_name='Forex';`
    )
    const category = categories[0].find(element => {
      return element
    })
    program.map(element => {
      element.category_id = category.id
    })
    return queryInterface.bulkInsert('programs', program, {}); 
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('programs', null, {});
  }
};
