'use strict';
const faker = require('faker')
let lessonDetails = [];
function setLessonDetails(lessonId) {
  const data = {
    lesson_id: lessonId,
    lesson_title: faker.name.title(),
    lesson_description: faker.lorem.lines(1),
    language_type: 'es',
    created_at: new Date(),
    updated_at: new Date()
  }
  return data;
}
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const queryLessons = await queryInterface.sequelize.query(
      `SELECT id FROM lessons;`
    )
    const lessons = queryLessons[0];
    lessons.forEach(lesson => {
      lessonDetails.push(setLessonDetails(lesson.id));
    });
    
    return queryInterface.bulkInsert('lesson_details', lessonDetails, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('lesson_details', null, {});
  }
};
