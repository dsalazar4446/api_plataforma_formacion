'use strict';
const faker = require('faker')
let text = [];
function setText() {
  const data = {
    class_description: faker.lorem.paragraph(),
    language_type: 'es',
    created_at: new Date(),
    updated_at: new Date()
  }
  return data
}

module.exports = {
  up: (queryInterface, Sequelize) => {
    for (let i = 0; i < 3; i++) {
      text.push(setText());
    }
    return queryInterface.bulkInsert('class_texts', text, {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('class_texts', null, {});
  }
};
