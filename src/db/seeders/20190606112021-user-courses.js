'use strict';
let userCourses = [];

function setUserCourses(userId, courseId) {
  const data = {
    user_id: userId,
    course_id: courseId,
    course_status: true,
    created_at: new Date(),
    updated_at: new Date()
  }
  return data;
}
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const queryCourses = await queryInterface.sequelize.query(
      `SELECT id FROM courses;`
    )
    const courses = queryCourses[0];
    const queryUsers = await queryInterface.sequelize.query(
      `SELECT id FROM users;`
    )
    const users = queryUsers[0];

    courses.forEach(course => {
      userCourses.push(setUserCourses(users[0].id,course.id))  
    });
    
    return queryInterface.bulkInsert('user_courses', userCourses, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_courses', null, {});
  }
};
