'use strict';
const faker = require('faker')
let array = []
function setClassSequences(classId, sequence) {
  const data = {
    formation_class_id: classId,
    class_sequence: sequence,
    created_at: new Date(),
    updated_at: new Date(),
  }
  return data;
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const query = await queryInterface.sequelize.query(
      `SELECT id FROM formation_classes;`
    )
    const result = query[0];
    
    result.forEach(element => {
      for (let i = 0; i < 5; i++) {
        array.push(setClassSequences(element.id, i+1))
      }
    });
    
    return queryInterface.bulkInsert('class_sequences', array, {});

  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('class_sequences', null, {});
  }
};
