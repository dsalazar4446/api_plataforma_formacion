'use strict';
const faker = require('faker')
let courses = [];

function setCourses(userId) {
  const data = {
    user_id: userId,
    course_date: '2019-06-15',
    hours: faker.random.number({ min: 1, max: 3 }),
    course_status: '2',
    course_type: 'membership',
    premium_creex_price: faker.random.number({ precision: 2 }),
    premium_external_price: faker.random.number({ precision: 2 }),
    vip_price: faker.random.number({ precision: 2 }),
    course_odoo_id: faker.random.number({ min: 0, max: 100 }),
    created_at: new Date(),
    updated_at: new Date(),
  }
  return data;
}


module.exports = {
  up: async (queryInterface, Sequelize) => {
    const userQuery = await queryInterface.sequelize.query(
      `SELECT id FROM users;`
    )
    const users = userQuery[0]

    for (let i = 0; i < 3; i++) {
      courses.push(setCourses(users[0].id))
    }

    return queryInterface.bulkInsert('courses', courses, {});

  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('courses', null, {});
  }
};
