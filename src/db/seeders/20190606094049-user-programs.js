'use strict';
let userPrograms = []


function setUserPrograms(userId,programId){
  const data = {
    user_id: userId,
    program_id: programId,
    user_program_status: "1",
    user_program_favorite: true,
    created_at: new Date(),
    updated_at: new Date()
  }
  return data;
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const queryUsers = await queryInterface.sequelize.query(
      `SELECT id FROM users;`
    )
    const users = queryUsers[0]
    const queryPrograms = await queryInterface.sequelize.query(
      `SELECT id FROM programs;`
    )
    const programs = queryPrograms[0];
    for (let i = 0; i < programs.length; i++) {
      userPrograms.push(setUserPrograms(users[0].id,programs[i].id))
    }
    return queryInterface.bulkInsert('user_programs', userPrograms, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_programs', null, {});
  }
};
