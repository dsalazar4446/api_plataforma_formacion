'use strict';
const faker= require('faker');
let array = []

function setAttachment() {
  const data  = {
    class_attachment_type: `${faker.random.number({min:1,max:3})}`,
    class_attachment: faker.name.jobArea(),
    created_at: new Date(),
    updated_at: new Date(),
  }
  return data;
}
module.exports = {
  up: async (queryInterface, Sequelize) => {
    for (let i = 0; i < 3; i++) {
       array.push(setAttachment()) 
    }
    return queryInterface.bulkInsert('class_attachments', array, {});
  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.bulkDelete('class_attachments', null, {});

  }
};
