'use strict';
let lessons = [];

function setLessons(courseId, lessonNumber) {
  const data = {
    course_id: courseId,
    lesson_number: lessonNumber,
    lesson_status: '2',
    created_at: new Date(),
    updated_at: new Date(),
  }
  return data;
}

module.exports = {


  up: async (queryInterface, Sequelize) => {
    const queryCourses = await queryInterface.sequelize.query(
      `SELECT id FROM courses;`
    )
    const courses = queryCourses[0];
    
    courses.forEach(course => {
      for (let i = 0; i < 5; i++) {
        lessons.push(setLessons(course.id, i+1))
      }
    });
    
    return queryInterface.bulkInsert('lessons', lessons, {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('lessons', null, {});
  }
};
