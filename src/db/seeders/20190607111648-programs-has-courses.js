'use strict';

const faker = require('faker');
let array = []
function setProgramsCourses(programId, courseId) {
  return {
    program_id: programId,
    course_id: courseId,
    created_at: new Date(),
    updated_at: new Date(),
  }
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const query1 = await queryInterface.sequelize.query(
      `SELECT id FROM programs;`
    )
    const query2 = await queryInterface.sequelize.query(
      `SELECT id FROM courses;`
    )
    const programs = query1[0]
    const courses = query2[0]

    for (let i = 0; i < courses.length; i++) {
      if (i<2) {
        array.push(setProgramsCourses(programs[0].id, courses[i].id))
      }else {
        array.push(setProgramsCourses(programs[1].id, courses[i].id))
      }
    }

    return queryInterface.bulkInsert('programs_has_courses', array, {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('programs_has_courses', null, {});
  }
};
