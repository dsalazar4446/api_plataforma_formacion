'use strict';

const faker = require('faker')

let array = [];
function setHomeworks() {
  const data = {
    homework_code: faker.random.alphaNumeric(9),
    created_at: new Date(),
    updated_at: new Date(),
  }
  return data;
}

module.exports = {
  up: (queryInterface, Sequelize) => {

    for (let i = 0; i < 3; i++) {
      array.push(setHomeworks());
    }
    return queryInterface.bulkInsert('class_homeworks', array, {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('class_homeworks', null, {});
  }
};
