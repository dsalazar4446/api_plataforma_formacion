'use strict';
const faker = require('faker');

const categoryName = {
  BLOCKCHAIN: "BlockChain",
  FOREX: "Forex"
}

const language = ["es", "en", "it", "pr"]

let categories = []

function fillCategory(categoryName, language) {
  return {
    category_name: categoryName,
    language: language,
    category_target: "formation",
    created_at: new Date(),
    updated_at: new Date(),
  }
}
for (let i = 0; i < 2; i++) {
  let name = null;
  for (let j = 0; j < language.length; j++) {
    let lang = null;
    if (i == 0) {
      name = categoryName.BLOCKCHAIN
    }
    if (i == 1) {
      name = categoryName.FOREX
    }
    lang = language[j]

    categories.push(fillCategory(name, lang));
  }
}

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('categories', categories, {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('categories', null, {});
  }
};
