'use strict';

const faker = require('faker');

const array = [];

function setVideoFavorites(classVideoId,userId){
  return {
    classVideoId,
    userId,
    video_time: faker.random.number({min:1, max: 180}),
    favorite_text: faker.lorem.lines(),
  }
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
      // const query1 = await queryInterface.sequelize.query(
      //   'SELECT id FROM users'
      // );
      // const users = query1[0]
      // const query2 = await queryInterface.sequelize.query(
      //   'SELECT id FROM class_videos'
      // );
      // const classVideos = query2[0]

      // return queryInterface.bulkInsert('user_video_favorites', array, {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('user_video_favorites', null, {});
  }
};
