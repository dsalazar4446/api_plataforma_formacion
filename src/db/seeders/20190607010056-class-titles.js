'use strict';
const faker = require('faker');

let classTitles = [];

function setClassTitles(formationClassId) {
  const data = {
    formation_class_id: formationClassId,
    class_title: faker.name.title(),
    language_type: 'es',
    created_at: new Date(),
    updated_at: new Date(),
  }
  return data;
}
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const queryFormationClasses = await queryInterface.sequelize.query(
      'SELECT id FROM formation_classes'
    );

    const formationClass = queryFormationClasses[0];

    formationClass.forEach(formation => {
      classTitles.push(setClassTitles(formation.id));
    });

    return queryInterface.bulkInsert('class_titles', classTitles, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('class_titles', null, {});

  }
};
