'use strict';
const faker = require('faker')
let userLessons = [];
function setUserLessons(userid, lessonId) {
  const data = {
    user_id: userid,
    lesson_id: lessonId,
    user_lesson_status: '2',
    created_at: new Date(),
    updated_at: new Date(),
  }
  return data;
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const queryUsers = await queryInterface.sequelize.query(
      `SELECT id FROM users`
    )
    const users = queryUsers[0];
    const queryLessons = await queryInterface.sequelize.query(
      `SELECT id FROM lessons`
    )
    const lessons = queryLessons[0];

      lessons.forEach(lesson => {
        userLessons.push(setUserLessons(users[0].id, lesson.id))
      });
    return queryInterface.bulkInsert('user_lessons', userLessons, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_lessons', null, {});
  }
};
