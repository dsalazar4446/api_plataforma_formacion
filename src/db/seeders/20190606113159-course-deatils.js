'use strict';
const faker = require('faker')
let courseDetails = [];

function setCourseDetails(courseId) {
  const data = {
    course_id: courseId,
    course_title: faker.name.title(),
    course_description: faker.lorem.paragraph(),
    course_image: faker.image.imageUrl(),
    language_type: 'es',
    created_at: new Date,
    updated_at: new Date,
  }
  return data;
}

module.exports = {
  up: async (queryInterface, Sequelize) => {

    const queryCourses = await queryInterface.sequelize.query(
      `SELECT id FROM courses;`
    )
    const courses = queryCourses[0];

    courses.forEach(course => {
      courseDetails.push(setCourseDetails(course.id))
    });
    return queryInterface.bulkInsert('course_details', courseDetails, {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('course_details', null, {});
  }
};
