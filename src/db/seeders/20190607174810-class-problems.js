'use strict';

const faker = require('faker');

let array = [];

function setProblems(userId, formationId) {
  return {
    user_id: userId,
    formation_class_id: formationId,
    problem_type: `${faker.random.number({ min: 1, max: 5 })}`,
    problem_explanation: faker.lorem.paragraph(),
    problem_status: true,
    created_at: new Date(),
    updated_at: new Date(),
  }
}
module.exports = {
  up: async (queryInterface, Sequelize) => {

    const query1 = await queryInterface.sequelize.query(
      'SELECT id FROM  users'
    );
    const users = query1[0]
    const query2 = await queryInterface.sequelize.query(
      'SELECT id FROM  formation_classes'
    );
    const classes = query2[0]

    classes.forEach(classe => {
      array.push(setProblems(users[0].id, classe.id))
    });

    return queryInterface.bulkInsert('class_problems', array, {});
  },
  
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('class_problems', null, {});
  }
};
