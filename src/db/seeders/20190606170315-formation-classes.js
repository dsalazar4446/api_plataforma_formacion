'use strict';

const faker = require('faker')

let formationcClasses = []

function setFormationClasses(lessonId) {
  const data = {
    lesson_id: lessonId,
    class_code: faker.random.alphaNumeric(6),
    class_status: true,
    created_at: new Date(),
    updated_at: new Date(),
  }
  return data;
}
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const queryLessons = await queryInterface.sequelize.query(
      `SELECT id FROM lessons;`
    )
    const lessons = queryLessons[0];

    lessons.forEach(lesson => {
      for (let i = 0; i < 5; i++) {
        formationcClasses.push(setFormationClasses(lesson.id))
      }

    });

    return queryInterface.bulkInsert('formation_classes', formationcClasses, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('formation_classes', null, {});
  }
};
