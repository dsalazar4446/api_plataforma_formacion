'use strict';
let array = []

function setSequenceText(sequnceId, homeworkId) {
  const data = {
    class_sequence_id: sequnceId,
    class_homework_id: homeworkId,
    created_at: new Date(),
    updated_at: new Date()
  }
  return data;
}
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const query1 = await queryInterface.sequelize.query(
      `SELECT id FROM class_sequences;`
    )
    const query2 = await queryInterface.sequelize.query(
      `SELECT id FROM class_homeworks;`
    )
    const sequences = query1[0]
    const homeworks = query2[0]
    sequences.forEach(sequence => {
      homeworks.forEach(homework => {
        array.push(setSequenceText(sequence.id, homework.id))
      })
    });

    return queryInterface.bulkInsert('class_sequences_has_class_homeworks', array, {});

  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('class_sequences_has_class_homeworks', null, {});
  }
};
