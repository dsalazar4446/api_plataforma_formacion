'use strict';

const faker = require('faker');

let array = []

function setDetail(homeworkId) {
  return {
    class_homework_id: homeworkId,
    homework_title: faker.name.jobTitle(),
    homework_description: faker.lorem.paragraph(),
    language_type: 'es',
    created_at: new Date(),
    updated_at: new Date()
  }
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const query1 = await queryInterface.sequelize.query(
      `SELECT id FROM class_homeworks;`
    )
    const result = query1[0];
    result.forEach(element => {
      array.push(setDetail(element.id))
    });
    return queryInterface.bulkInsert('class_homework_details', array, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('class_homework_details', null, {});
  }
};
