'use strict';
const faker = require('faker');
let rates = [];
function setRates(programId) {
  const data = {
    user_program_id: programId,
    language_type: 'es',
    user_program_rate: faker.random.number({min: 0, max: 10}),
    user_program_comments: faker.lorem.lines(1),
    user_program_status: true,
    created_at: new Date(),
    updated_at: new Date(),
  }
  return data;
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const queryPrograms = await queryInterface.sequelize.query(
      `SELECT id FROM user_programs;`
    )
    const userPrograms = queryPrograms[0];

    userPrograms.forEach(userProgram => {
      rates.push(setRates(userProgram.id))
    });
  
    return queryInterface.bulkInsert('user_program_rates', rates, {});
    
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('user_program_rates', null, {});
    
  }
};
