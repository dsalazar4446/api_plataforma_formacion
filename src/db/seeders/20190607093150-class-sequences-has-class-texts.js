'use strict';
let array = []

function setSequenceText(sequnceId, textId) {
  const data = {
    class_sequence_id: sequnceId,
    class_text_id: textId,
    created_at: new Date(),
    updated_at: new Date()
  }
  return data;
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const query1 = await queryInterface.sequelize.query(
      `SELECT id FROM class_sequences;`
    )
    const query2 = await queryInterface.sequelize.query(
      `SELECT id FROM class_texts;`
    )
    const sequences = query1[0]
    const texts = query2[0]
    sequences.forEach(sequence => {
      texts.forEach(text => {
        array.push(setSequenceText(sequence.id, text.id))
      })
    });
    return queryInterface.bulkInsert('class_sequence_has_class_texts', array, {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('class_sequence_has_class_texts', null, {});
  }
};
