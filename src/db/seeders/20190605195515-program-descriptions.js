'use strict';
const faker = require('faker');
let programDescriptions = []



function setProgramId(id){
  const data = {
    program_id: id,
    program_title: faker.name.title(),
    program_detail: faker.lorem.paragraph(),
    language_type: "es",
    created_at: new Date,
    updated_at: new Date
  } 
  return data;
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const programs = await queryInterface.sequelize.query(
      `SELECT id FROM programs;`
    )
    programs[0].forEach(element => {
      programDescriptions.push(setProgramId(element.id));
    });
    return queryInterface.bulkInsert('program_descriptions', programDescriptions, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('program_descriptions', null, {});
  }
};
