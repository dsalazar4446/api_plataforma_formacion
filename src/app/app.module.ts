import { Module, MiddlewareConsumer } from '@nestjs/common';
import { PaymentModule } from './payment/payment.module';
import { UserModule } from './user/user.module';
import { HandleLanguageMiddleware } from './shared/middleware/handle-language.middleware';
import { NotificationsModule } from './notifications/notifications.module';
import { UserLocationModule } from './user-location/user-location.module';
import { MembershipsModule } from './memberships/memberships.module';
import { RolesModule } from './roles/roles.module';
import { NotificationsMsgModule } from './notifications-msg/notifications-msg.module';
import { SessionsModule } from './sessions/sessions.module';
import { AuthsModule } from './auths/auths.module';
import { DeviceModule } from './device/device.module';
import { BiosModule } from './bios/bios.module';
import { CityModule } from './city/city.module';
import { UserActionsModule } from './user-actions/user-actions.module';
import { TransactionsModule } from './transactions/transactions.module';
import { CreexPayCardModule } from './creex-pay-card/creex-pay-cards.module';
import { SurveysModule } from './surveys/surveys.module';
import { NewsModule } from './news/news.module';
import { ResourcesModule } from './resources/resources.module';
import { TestModule } from './test/test.module';
import { ProgramsModule } from './programs/programs.module';
import { CertificatesAcademiesModule } from './certificateAcademies/academies.module';
import { ForumsModule } from './forums/forums.module';
import { CategoriesModule } from './categories/categories.module';
import { MainVideosModule } from './main-videos/main-videos.module';
import { ClassVideoSubtitlesModule } from './class-video-subtitles/class-video-subtitles.module';
import { ClassVideoSubtitleTextsModule } from './class-video-subtitle-texts/class-video-subtitle-texts.module';
import { BroadCastModule } from './broadcast/broadcast.module';
import { BroadcastAttendanceModule } from './broadcast-attendance/broadcast-attendance.module';
import { BroadcastHasCategoriesModule } from './broadcast-has-categories/broadcast-has-categories.module';
import { BroadcastMessageModule } from './broadcast-message/broadcast-message.module';
import { BroadcastPrebuiltMessageModule } from './broadcast-prebuilt-message/broadcast-prebuilt-message.module';
// tslint:disable-next-line: max-line-length
import { BroadcastPrebuiltMessageHasCategoriesModule } from './broadcast-prebuilt-message-has-categories/broadcast-prebuilt-message-has-categories.module';
// tslint:disable-next-line: max-line-length
import { BroadcastHasBroadcastPrebuiltMessagesModule } from './broadcasts_has_broadcast_prebuilt_messages/broadcasts_has_broadcast_prebuilt_messages.module';
import { PrebuiltUsersModule } from './prebuilt-users/prebuilt-users.module';
import { EventsModule } from './events/events.module';
import { ProductsModule } from './products/products.module';
import { CompaniesModule } from './companies/companies.module';
import { ClassesModule } from './classes/classes.module';
import { LessonsModule } from './lessons/lessons.module';
import { SupportModule } from './support/support.module';
import { CoursesModule } from './courses/courses.module';
import { MyAccountModule } from './my-account/my-account.module';
import { EmailModule } from './email/email.module';
import { ChatModule } from './chat/chat.module';
import { MessagesModule } from './messages/messages.module';
import { InboxModule } from './inbox/inbox.module';
import { MagazinesModule } from './magazines/magazines.module';
import { AnouncementsModule } from './anouncements/anouncements.module';
import { PromotionsModule } from './promotions/promotions.module';
import { InboxRecieversModule } from './inboxRecievers/inbox-recievers.module';
import { RankingModule } from './ranking/ranking.module';
import { GatewayModule } from './gateway/gateway.module';
import { DocumentModule } from './document-types/document.module';
import { DownlinesModule } from './downlines/downlines.module';
import { TransferalsModule } from './transferals/transferals.module';
import { TraderModule } from './trader/trader.module';
import { ConsultingsModule } from './consultings/consultings.module';
import { FacebookModule } from './facebook/facebook.module';
import { OneSignalModule } from './onesignal/onesignal.module';
import { VimeoModule } from './vimeo/vimeo.module';
import { UserComissionModule } from './user-comission/user-comission.module';
import { GroupGatewayModule } from './group-gateway/group-gateway.module';
import { ToDoListModule } from './todo/todo.module';
import { CalendarsModule } from './calendars/calendars.module';
import { UserComissionsHasDownlineModule } from './user_comissions_has_downline/user_comissions_has_downline.module';
import { FollowersModule } from './followers/followers.module';
import { ToolsModule } from './tools/tools.module';
import { LogsModule } from './logs/logs.module';
import { UserComissionStatusModule } from './user-comission-status/user-comission-status.module';
import { ChatHasUsersModule } from './chat-has-users/chat-has-users.module';
import { MessageAttachmentModule } from './message-attachments/message-attachments.module';
@Module({
  imports: [
    PaymentModule,
    UserModule,
    NotificationsModule,
    UserLocationModule,
    MembershipsModule,
    RolesModule,
    NotificationsMsgModule,
    SessionsModule,
    AuthsModule,
    DeviceModule,
    BiosModule,
    CityModule,
    UserActionsModule,
    TransactionsModule,
    BroadCastModule,
    PrebuiltUsersModule,
    BroadcastMessageModule,
    BroadcastPrebuiltMessageModule,
    BroadcastAttendanceModule,
    CreexPayCardModule,
    SurveysModule,
    NewsModule,
    ResourcesModule,
    TestModule,
    ProgramsModule,
    CertificatesAcademiesModule,
    ForumsModule,
    CategoriesModule,
    MainVideosModule,
    ClassVideoSubtitlesModule,
    ClassVideoSubtitleTextsModule,
    BroadcastHasCategoriesModule,
    EventsModule,
    ProductsModule,
    CompaniesModule,
    ClassesModule,
    LessonsModule,
    ProgramsModule,
    SupportModule,
    CoursesModule,
    MyAccountModule,
    EmailModule,
    ChatModule,
    MessagesModule,
    InboxModule,
    InboxRecieversModule,
    MagazinesModule,
    AnouncementsModule,
    PromotionsModule,
    RankingModule,
    GatewayModule,
    DocumentModule,
    BroadcastPrebuiltMessageHasCategoriesModule,
    BroadcastHasBroadcastPrebuiltMessagesModule,
    DownlinesModule,
    TransferalsModule,
    TraderModule,
    ConsultingsModule,
    FacebookModule,
    OneSignalModule,
    VimeoModule,
    UserComissionModule,
    GroupGatewayModule,
    ToDoListModule,
    CalendarsModule,
    UserComissionsHasDownlineModule,
    FollowersModule,
    ToolsModule,
    LogsModule,
    UserComissionStatusModule,
    ChatHasUsersModule,
    MessageAttachmentModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {

  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(HandleLanguageMiddleware)
      .forRoutes('*');
  }
}
