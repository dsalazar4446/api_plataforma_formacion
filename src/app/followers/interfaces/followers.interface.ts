import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class FollowersSaveJson {
    @ApiModelProperty()
    @IsUUID('4',{message: 'followerUserId property must a be uuid'})
    @IsNotEmpty({message: 'followerUserId property not must null'})
    followerUserId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'followedUserId property must a be uuid'})
    @IsNotEmpty({message: 'followedUserId property not must null'})
    followedUserId: string;

}
export class FollowersUpdateJson extends FollowersSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
