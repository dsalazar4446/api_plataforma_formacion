import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'followers',
})
export class Followers extends Model<Followers>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'follower_user_id',
    })
    followerUserId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'followed_user_id',
    })
    followedUserId: string;
    
    @ApiModelPropertyOptional()
    @BelongsTo(() => UserModel, 'followerUserId')
    followerUser: UserModel;
    
    @ApiModelPropertyOptional()
    @BelongsTo(() => UserModel, 'followedUserId')
    followedUser: UserModel;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
