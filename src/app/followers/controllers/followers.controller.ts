import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param, Req } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from '@nestjs/swagger';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { FollowersService } from '../services/followers.service';
import { FollowersSaveJson } from '../interfaces/followers.interface';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller FollowersController
 * @Creado 11 de Mayo 2019
 */

@ApiUseTags('Module-Followers')
@Controller('Followers')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class FollowersController {

    constructor(private readonly _followersService: FollowersService,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyFollowers: FollowersSaveJson) {
        const createFollowers = await this._followersService.saveFollowers(bodyFollowers);
        if (!createFollowers) {
            throw this.appUtilsService.httpCommonError('Followers no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM43',
                languageType: 'es',
            });
        }
        return createFollowers;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idFollowers: string) {

        const fetchFollowers = await this._followersService.getDetails(idFollowers);

        if (!fetchFollowers) {
            throw this.appUtilsService.httpCommonError('Followers no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM44',
                languageType: 'es',
            });
        }
        return fetchFollowers;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllFollowers() {

        const fetch = await this._followersService.showAllFollowers();
        if (!fetch) {
            throw new NotFoundException('Followers does not exist!');
        }
        return fetch;
    }

    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateFollowers(@Param('id') id: string, @Body() bodyFollowers: Partial<FollowersSaveJson>) {

        const updateFollowers = await this._followersService.updateFollowers(id, bodyFollowers);
        if (!updateFollowers[1][0]) {
            throw this.appUtilsService.httpCommonError('Followers no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM44',
                languageType: 'es',
            });
        }
        return updateFollowers;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteFollowers(@Param('id') idFollowers: string) {

        const deleteFollowers = await this._followersService.destroyFollowers(idFollowers);
        if (!deleteFollowers) {
            throw this.appUtilsService.httpCommonError('Followers no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM44',
                languageType: 'es',
            });
        }
        return deleteFollowers;
    }
}
