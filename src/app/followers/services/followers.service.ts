import { Injectable, Inject } from '@nestjs/common';
import { UserModel } from '../../user/models/user.Model';
import { Followers } from '../models/followers.model';
import { FollowersSaveJson, FollowersUpdateJson } from '../interfaces/followers.interface';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services FollowersService
 * @Creado 11 de Mayo 2019
 */

@Injectable()
export class FollowersService {

    private include = {
        include: [
            {
                as: 'followerUser',
                model: UserModel
            },
            {
                as: 'followedUser',
                model: UserModel
            }
        ]
    }

    constructor(
        @Inject('Followers') private readonly _followers: typeof Followers
    ) { }

    async saveFollowers(bodyFollowers: FollowersSaveJson): Promise<FollowersUpdateJson> {
        return await new this._followers(bodyFollowers).save();
    }

    async showAllFollowers(): Promise<FollowersUpdateJson[]> {
        return await this._followers.findAll(this.include);
    }

    async getDetails(FollowersId: string) {
        return await this._followers.findByPk(FollowersId, this.include);
    }

    async updateFollowers(id: string, bodyFollowers: Partial<FollowersUpdateJson>): Promise<[number, Array<FollowersUpdateJson>]> {
        return await this._followers.update(bodyFollowers, {
            where: { id },
            returning: true
        });

    }

    async destroyFollowers(FollowersId: string): Promise<number> {
        return await this._followers.destroy({
            where: { id: FollowersId },
        });
    }
}
