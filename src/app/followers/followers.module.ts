import { Module, HttpModule, Provider } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { FollowersController } from './controllers/followers.controller';
import { Followers } from './models/followers.model';
import { FollowersService } from './services/followers.service';

const models = [
    Followers
];

const controllers = [
    FollowersController
];

const providers: Provider[] = [
    FollowersService
];

@Module({
    imports: [
        SharedModule,
        HttpModule,
        DatabaseModule.forRoot({
            sequelize: {
                models,
            },
            loki: true,
        }),
    ],
    providers,
    controllers
})

@Module({})
export class FollowersModule { }


