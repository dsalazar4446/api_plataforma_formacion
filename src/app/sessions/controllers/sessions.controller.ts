import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, Req, HttpStatus} from '@nestjs/common';
import { SessionsService } from '../services/sessions.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { SessionsSaveJson, SessionsUpdateJson } from '../interfaces/sessions.interface';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiImplicitQuery, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { SessionsModel } from '../models/sessions.model';
import { UserModel } from '../../user/models/user.Model';
import { DeviceModel } from '../../device/models/device.model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
@Controller('sessions')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class SessionsController {
    constructor(
        private readonly sessionsService: SessionsService,
        private readonly appUtilsService: AppUtilsService,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('DeviceModel') private readonly deviceModel: typeof DeviceModel,
        @Inject('SessionsModel') private readonly sessionsModel: typeof SessionsModel,
        ) {}
        @ApiImplicitQuery({ name: 'sessionStatus', enum: ['1','2'] })
        @ApiResponse({
            status: 200,
            type: SessionsModel
          })
        @Post()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async create(@Body() sessions: SessionsSaveJson, @Req() req) {
            const data2 = await this.deviceModel.findByPk(sessions.deviceId);
            if(data2 == null){
                throw this.appUtilsService.httpCommonError('Sessions does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA6',
                    languageType:  req.headers.language,
                });
            }
            const data3 = await this.userModel.findByPk(sessions.userId);
            if(data3 == null){
                throw this.appUtilsService.httpCommonError('Sessions does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA4',
                    languageType:  req.headers.language,
                });
            }
            return await this.sessionsService.create(sessions);
        }
        // LIST SESSION
        @ApiImplicitParam({ name: 'status', required: true, type: 'string' })
        @ApiImplicitParam({ name: 'start_date', required: true, type: 'string' })
        @ApiImplicitParam({ name: 'end_date', required: true, type: 'string' })
        @ApiImplicitParam({ name: 'limit', required: true, type: 'number' })
        @ApiImplicitParam({ name: 'page', required: true, type: 'number' })
        @ApiResponse({
            status: 200,
            type: SessionsModel,
            isArray: true
          })
        @Get(':status/:start_date/:end_date/:limit/:page')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async getAll(
            @Param('status') statusSession,
            @Param('start_date') startDate,
            @Param('end_date') endDate,
            @Param('limit') limit,
            @Param('page') page,
        ) {
            return await this.sessionsService.findAll(statusSession, startDate, endDate, limit, page);
        }
        // DETAIL SESSION
        @Get(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: SessionsModel,
          })
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        async findById(@Param('id') idSession) {
            return await this.sessionsService.findById(idSession);
        }
        // UPDATE SESSION
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @ApiImplicitQuery({ name: 'sessionStatus', enum: ['1','2'] })
        @Put(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: SessionsModel,
          })
        async update(
            @Body()
            updateSessions: Partial<SessionsUpdateJson>, 
            @Param('id') idSession,
            @Req() req
        ) {
            const data2 = await this.deviceModel.findByPk(updateSessions.deviceId);
            if(data2 == null){
                throw this.appUtilsService.httpCommonError('Sessions does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA6',
                    languageType:  req.headers.language,
                });
            }
            const data3 = await this.userModel.findByPk(updateSessions.userId);
            if(data3 == null){
                throw this.appUtilsService.httpCommonError('Sessions does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA4',
                    languageType:  req.headers.language,
                });
            }
            const data4 = await this.sessionsModel.findByPk(idSession);
            if(data4 == null){
                throw this.appUtilsService.httpCommonError('Sessions does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA8',
                    languageType:  req.headers.language,
                });
            }
            return await this.sessionsService.update(idSession, updateSessions);
        }
        // DELETE SESSION
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Delete(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async delete(@Param('id') idSession) {
            return await this.sessionsService.deleted(idSession);
        }
}
