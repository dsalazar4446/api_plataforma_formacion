import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { SessionsController } from './controllers/sessions.controller';
import { SessionsService } from './services/sessions.service';
import { SessionsModel } from './models/sessions.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { DeviceModel } from '../device/models/device.model';
const models = [
  SessionsModel,
  DeviceModel
];
@Module({
  imports: [
    forwardRef(() => SharedModule),
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [SessionsController],
  providers: [SessionsService],
  exports: [SessionsService]
})
export class SessionsModule {}
