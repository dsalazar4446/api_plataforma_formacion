import { Injectable, Inject } from '@nestjs/common';
import { SessionsSaveJson, SessionsUpdateJson } from '../interfaces/sessions.interface';
import { SessionsModel } from '../models/sessions.model';
import { Sequelize } from 'sequelize-typescript';
const Op = Sequelize.Op;
@Injectable()
export class SessionsService {
    constructor(
        @Inject('SessionsModel') private readonly sessionsModel: typeof SessionsModel,
    ) { }

    async findAll(status: string, startDate: string, endDate: string, limit: number, page: number): Promise<any> {
        const counter = await this.sessionsModel.findAndCountAll({
            where: {
                sessionStatus: status,
                [Op.or]: {
                    sessionDate: {
                        [Op.between]: [startDate, endDate],
                    },
                },
            },
        });
        const pages = Math.ceil(counter.count / limit);
        const offset = limit * (page - 1);
        const sessions = await this.sessionsModel.findAll({
            where: {
                sessionStatus: status,
                [Op.or]: {
                    sessionDate: {
                        [Op.between]: [startDate, endDate],
                    },
                },
            },
            limit,
            offset,
        });
        const dataResult = {
            result: sessions,
            count: counter.count,
            pages,
        };
        return dataResult;
    }

    async findById(id: string): Promise<SessionsModel> {
        return await this.sessionsModel.findById<SessionsModel>(id);
    }

    async create(session: SessionsSaveJson): Promise<SessionsModel> {
        return await this.sessionsModel.create<SessionsModel>(session, {
            returning: true,
        });
    }

    async update(idSession: string, sessionUpdate: Partial<SessionsUpdateJson>){
        return  await this.sessionsModel.update(sessionUpdate, {
            where: {
                id: idSession,
            },
            returning: true,
        });
    }

    async deleted(idSession: string): Promise<any> {
        return await this.sessionsModel.destroy({
            where: {
                id: idSession,
            },
        });
    }
}
