import { ApiModelProperty } from '@nestjs/swagger';
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class SessionsSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    userId: string;
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    deviceId: string;
    @ApiModelProperty({ enum: ['1','2']})
    @IsNotEmpty()
    @IsString()
    sessionStatus: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    sessionDate: string;
}


// tslint:disable-next-line: max-classes-per-file
export class SessionsUpdateJson extends SessionsSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}
