import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { DeviceModel } from '../../device/models/device.model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'sessions',
})
export class SessionsModel extends Model<SessionsModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'user_id',
    })
    userId: string;
    @ApiModelProperty()
    @ForeignKey(() => DeviceModel)
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'device_id',
    })
    deviceId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.ENUM(
            '1',
            '2',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'session_status',
    })
    sessionStatus: string;
    @ApiModelProperty()
    @Column({
        type: DataType.TIME,
        allowNull: false,
        field: 'session_date',
    })
    sessionDate: string;

    @CreatedAt created_at: Date;
    @UpdatedAt updated_at: Date;
}
