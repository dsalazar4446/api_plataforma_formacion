export const UserLessonsExample = {
    "id": "string",
    "usersId": "string",
    "lessonsId": "string",
    "userLessonStatus": "string"
}

export const UserLessonsArrayExample = [
    UserLessonsExample
]