export const LessonsDetailsExample = {
    "id": "string",
    "lessonsId": "string",
    "lessonTitle": "string",
    "lessonDescription": "string",
    "languageType": "string"
}

export const LessonsDetailsArrayExample = [
    LessonsDetailsExample
]