import {
  Model,
  Table,
  PrimaryKey,
  AutoIncrement,
  AllowNull,
  Validate,
  Unique,
  Column,
  DataType,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
} from 'sequelize-typescript';
import { LessonsModel } from './lessons.model';
import { UserModel } from './../../user/models/user.Model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({ tableName: 'user_lessons' })
export class UserLessonsModel extends Model<UserLessonsModel> {
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    autoIncrement: true,
    unique: true,
    primaryKey: true,
    validate: {
      notEmpty: true,
    },
  })
  id: string;
  @ApiModelPropertyOptional()
  @ForeignKey(() => UserModel)
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    unique: true,
    validate: {
      notEmpty: true,
    },
    field: 'user_id',
  })
  usersId: string;

  @BelongsTo(() => UserModel)
  user: UserModel;
  @ApiModelPropertyOptional()
  @ForeignKey(() => LessonsModel)
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    unique: true,
    validate: {
      notEmpty: true,
    },
    field: 'lesson_id',
  })
  lessonsId: string;

  @BelongsTo(() => LessonsModel)
  lessons: LessonsModel;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.ENUM('1', '2', '3'),
    allowNull: false,
    unique: true,
    validate: {
      notEmpty: true,
    },
    field: 'user_lesson_status',
  })
  userLessonStatus: string;

  // tslint:disable-next-line: variable-name
  @CreatedAt created_at: Date;
  // tslint:disable-next-line: variable-name
  @UpdatedAt updated_at: Date;
}
