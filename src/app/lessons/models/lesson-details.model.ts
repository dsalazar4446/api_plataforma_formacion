import {
  Model,
  Table,
  PrimaryKey,
  AutoIncrement,
  AllowNull,
  Validate,
  Unique,
  Column,
  DataType,
  ForeignKey,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';
import { LessonsModel } from './lessons.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({ tableName: 'lesson_details' })
export class LessonDetailsModel extends Model<LessonDetailsModel> {
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    autoIncrement: true,
    unique: true,
    primaryKey: true,
    validate: {
      notEmpty: true,
    },
  })
  id: string;
  @ApiModelPropertyOptional()
  @ForeignKey(() => LessonsModel)
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    unique: true,
    validate: {
      notEmpty: true,
    },
    field: 'lesson_id',
  })
  lessonsId: string;
  @BelongsTo(() => LessonsModel)
  lessons: LessonsModel;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'lesson_title',
  })
  lessonTitle: string;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'lesson_description',
  })
  lessonDescription: string;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.ENUM('es', 'en', 'it', 'pr'),
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'language_type',
  })
  languageType: string;

  // tslint:disable-next-line: variable-name
  @CreatedAt created_at: Date;
  // tslint:disable-next-line: variable-name
  @UpdatedAt updated_at: Date;
}
