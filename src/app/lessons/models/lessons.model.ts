import {
  Model,
  Table,
  PrimaryKey,
  AutoIncrement,
  AllowNull,
  Validate,
  Unique,
  Column,
  DataType,
  ForeignKey,
  BelongsTo,
  HasMany,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';
import { LessonDetailsModel } from './lesson-details.model';
import { UserLessonsModel } from './user-lessons.model';
import { FormationClassesModel } from './../../classes/models/formation-classes.model';
import { CoursesModel } from './../../courses/models/courses.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { LessonsDetailsArrayExample } from '../examples/lessons-details.example';
import { UserLessonsArrayExample } from '../examples/user-lessons.example';
import { FormationCalssessArrayExample } from 'src/app/classes/examples/formation-classes.example';

@Table({ tableName: 'lessons' })
export class LessonsModel extends Model<LessonsModel> {
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    autoIncrement: true,
    unique: true,
    primaryKey: true,
    validate: {
      notEmpty: true,
    },
  })
  id: string;
  @ApiModelPropertyOptional()
  @ForeignKey(() => CoursesModel)
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    unique: true,
    validate: {
      notEmpty: true,
    },
    field: 'course_id'
  })
  courseId: string;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.SMALLINT,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'lesson_number',
  })
  lessonNumber: number;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.ENUM('1', '2', '3'),
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'lesson_status',
  })
  lessonStatus: string;

  @BelongsTo(() => CoursesModel)
  courses: CoursesModel;
  @ApiModelPropertyOptional({example:LessonsDetailsArrayExample})
  @HasMany(() => LessonDetailsModel)
  lessonDetails: LessonDetailsModel[];
  @ApiModelPropertyOptional({example:UserLessonsArrayExample})
  @HasMany(() => UserLessonsModel)
  userLessonsModel: UserLessonsModel[];
  @ApiModelPropertyOptional({example: FormationCalssessArrayExample})
  @HasMany(() => FormationClassesModel)
  formationClasses: FormationClassesModel[];

  // tslint:disable-next-line: variable-name
  @CreatedAt created_at: Date;
  // tslint:disable-next-line: variable-name
  @UpdatedAt updated_at: Date;
}
