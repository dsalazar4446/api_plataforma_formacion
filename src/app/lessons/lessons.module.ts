import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';

import { LessonsModel } from './models/lessons.model';
import { LessonDetailsModel } from './models/lesson-details.model';
import { UserLessonsModel } from './models/user-lessons.model';
import { LessonsService } from './services/lessons/lessons.service';
import { LessonDetailsService } from './services/lesson-details/lesson-details.service';
import { UserLessonsService } from './services/user-lessons/user-lessons.service';
import { LessonsController } from './controllers/lessons/lessons.controller';
import { LessonDetailsController } from './controllers/lesson-details/lesson-details.controller';
import { UserLessonsController } from './controllers/user-lessons/user-lessons.controller';

const models = [LessonsModel, LessonDetailsModel, UserLessonsModel];

@Module({
  imports: [
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: false,
    }),
  ],
  providers: [LessonsService, LessonDetailsService, UserLessonsService],
  controllers: [
    LessonsController,
    LessonDetailsController,
    UserLessonsController,
  ],
})
export class LessonsModule {}
