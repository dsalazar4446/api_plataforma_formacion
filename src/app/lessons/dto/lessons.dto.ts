import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString, IsNumber } from "class-validator";

export class CreateLessonsDto {
  @ApiModelProperty()
  @IsUUID('4')
  courseId: string;
  @ApiModelProperty()
  @IsNumber()
  lessonNumber: number;
  @ApiModelProperty()
  @IsString()
  lessonStatus: string;
}

export class UpdateLessonsDto extends CreateLessonsDto {
  @ApiModelProperty()
  @IsUUID('4')
  id: string;
}
