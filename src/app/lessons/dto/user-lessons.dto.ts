import { ApiUseTags, ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsString } from 'class-validator';
@ApiUseTags('Lessons')
export class CreateUserLessonsDto {
  @ApiModelProperty()
  @IsUUID('4')
  usersId: string;
  @ApiModelProperty()
  @IsString()
  lessonsId: string;
  @ApiModelProperty()
  @IsString()
  userLessonStatus: string;
}
export class UpdateUserLessonsDto extends CreateUserLessonsDto {
  @ApiModelProperty()
  @IsUUID('4')
  id: string;
}
