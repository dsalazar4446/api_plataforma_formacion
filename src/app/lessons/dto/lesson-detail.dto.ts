import { ApiModelProperty } from "@nestjs/swagger";
import { ApiUseTags } from '@nestjs/swagger';
import { IsUUID, IsString, IsOptional } from "class-validator";
@ApiUseTags('Lessons')
export class CreateLessonDetailDto {
  @ApiModelProperty()
  @IsUUID('4')
  lessonsId: string;
  @ApiModelProperty()
  @IsString()
  lessonTitle: string;
  @ApiModelProperty()
  @IsString()
  lessonDescription: string;
  @ApiModelProperty()
  @IsString()
  @IsOptional()
  languageType: string;
}

export class UpdateLessonDetailDto extends CreateLessonDetailDto {
  @ApiModelProperty()
  @IsUUID('4')
  id: string;
}
