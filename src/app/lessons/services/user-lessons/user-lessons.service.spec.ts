import { Test, TestingModule } from '@nestjs/testing';
import { UserLessonsService } from './user-lessons.service';

describe('UserLessonsService', () => {
  let service: UserLessonsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserLessonsService],
    }).compile();

    service = module.get<UserLessonsService>(UserLessonsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
