import { Injectable, Inject } from '@nestjs/common';
import { UserLessonsModel } from '../../models/user-lessons.model';
import {
  CreateUserLessonsDto,
  UpdateUserLessonsDto,
} from '../../dto/user-lessons.dto';

@Injectable()
export class UserLessonsService {
  constructor(
    @Inject('UserLessonsModel') private readonly userLessons: typeof UserLessonsModel,
  ) {}

  async create(body: CreateUserLessonsDto) {
    const createLessonDetail = new UserLessonsModel(body);
    return await createLessonDetail.save();
  }

  async list() {
    return await this.userLessons.findAll();
  }

  async detail(id: string) {
    return await this.userLessons.findByPk(id);
  }

  async update(id: string, body: Partial<UpdateUserLessonsDto>) {
    return await this.userLessons.update(body, { where: { id }, returning: true });
  }

  async delete(id: string) {
    return await this.userLessons.destroy({ where: { id } });
  }
}
