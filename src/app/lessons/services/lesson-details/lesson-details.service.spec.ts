import { Test, TestingModule } from '@nestjs/testing';
import { LessonDetailsService } from './lesson-details.service';

describe('LessonDetailsService', () => {
  let service: LessonDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LessonDetailsService],
    }).compile();

    service = module.get<LessonDetailsService>(LessonDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
