import { Injectable, Inject } from '@nestjs/common';
import { LessonDetailsModel } from '../../models/lesson-details.model';
import {
  CreateLessonDetailDto,
  UpdateLessonDetailDto,
} from '../../dto/lesson-detail.dto';

@Injectable()
export class LessonDetailsService {
  constructor(
    @Inject('LessonDetailsModel')
    private readonly lessonDetail: typeof LessonDetailsModel,
  ) {}

  async create(body: CreateLessonDetailDto) {
    const createLessonDetail = new LessonDetailsModel(body);
    return await createLessonDetail.save();
  }

  async list(languageType:string) {
    return await this.lessonDetail.findAll({where:{languageType}});
  }

  async detail(id: string, languageType:string) {
    return await this.lessonDetail.findByPk(id, {where:{languageType}});
  }

  async update(id: string, body: Partial<UpdateLessonDetailDto>) {
    return await this.lessonDetail.update(body, { where: { id }, returning: true});
  }

  async delete(id: string) {
    return await this.lessonDetail.destroy({ where: { id } });
  }
}
