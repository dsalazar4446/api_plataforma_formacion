import { Injectable, Inject } from '@nestjs/common';
import { LessonsModel } from '../../models/lessons.model';
import { CreateLessonsDto, UpdateLessonsDto } from '../../dto/lessons.dto';
import { LessonDetailsModel } from '../../models/lesson-details.model';
import { UserLessonsModel } from '../../models/user-lessons.model';
import { FormationClassesModel } from '../../../classes/models/formation-classes.model';
const includes = [
  {
    model: LessonDetailsModel,
    attributes: ['lesson_title', 'lesson_description'],
  },
  UserLessonsModel,
  FormationClassesModel
]

@Injectable()
export class LessonsService {
  constructor(
    @Inject('LessonsModel') private readonly lessons: typeof LessonsModel,
  ) {}

  async create(body: CreateLessonsDto) {
    const createLesson = new LessonsModel(body);
    return await createLesson.save();
  }
  async list() {
    return await this.lessons.findAll({
      include: includes,
      where: {
        lessonStatus: "2"
      }
    });
  }
  async listAdmin(){
    return await this.lessons.findAll({
      include: includes,
    });
  }
  async details(id: string) {
    return await this.lessons.findByPk(id, {
      include: includes,
      where: {
        lessonStatus: "2"
      }
    });
  }
  async detailAdmin(id: string) {
    return await this.lessons.findByPk(id, {
      include: includes,
      
    });
  }
  async update(id: string, body: Partial<UpdateLessonsDto>) {
    return await this.lessons.update(body, { where: { id }, returning: true });
  }
  async delete(id: string) {
    return await this.lessons.destroy({ where: { id } });
  }
}
