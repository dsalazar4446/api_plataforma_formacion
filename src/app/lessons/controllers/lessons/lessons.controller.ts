
import {
  Controller,
  UseInterceptors,
  UseFilters,
  UseGuards,
  Post,
  UsePipes,
  Body,
  Get,
  Param,
  Put,
  Delete,
} from '@nestjs/common';

import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { CreateLessonsDto, UpdateLessonsDto } from '../../dto/lessons.dto';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { LessonsService } from '../../services/lessons/lessons.service';
import { LessonsModel } from '../../models/lessons.model';

@ApiUseTags('Lessons')
@Controller('lessons')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// @UseGuards(new JwtAuthGuard())
export class LessonsController {
  constructor(private readonly lessons: LessonsService) { }

  @Post()
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: LessonsModel })
  create(@Body() body: CreateLessonsDto) {
    console.log(body)
    return this.lessons.create(body);
  }

  @Get()
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: LessonsModel })
  list() {
    return this.lessons.list();
  }
  @Get('admin')
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: LessonsModel })
  listAdmin() {
    return this.lessons.listAdmin();
  }

  @Get(':id')
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: LessonsModel })
  @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
  details(
    @Param('id') id: string,
  ) {
    return this.lessons.details(id);
  }

  @Get(':id/:status')
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: LessonsModel })
  @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
  detailsAdmin(
    @Param('id') id: string,
  ) {
    return this.lessons.detailAdmin(id);
  }

  @Put(':id')
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: LessonsModel })
  @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
  update(
    @Param('id') id,
    @Body()
    body: Partial<UpdateLessonsDto>,
  ) {
    return this.lessons.update(id, body);
  }
  @Delete(':id')
  @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
  delete(@Param('id') id: string) {
    return this.lessons.delete(id);
  }
}
