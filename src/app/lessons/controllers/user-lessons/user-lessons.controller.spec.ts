import { Test, TestingModule } from '@nestjs/testing';
import { UserLessonsController } from './user-lessons.controller';

describe('UserLessons Controller', () => {
  let controller: UserLessonsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserLessonsController],
    }).compile();

    controller = module.get<UserLessonsController>(UserLessonsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
