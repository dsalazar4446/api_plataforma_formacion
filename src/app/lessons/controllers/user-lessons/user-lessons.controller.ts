import {
  Controller,
  UseInterceptors,
  UseFilters,
  UseGuards,
  Post,
  UsePipes,
  Body,
  Get,
  Param,
  Put,
  Delete,
} from '@nestjs/common';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { CreateUserLessonsDto, UpdateUserLessonsDto } from '../../dto/user-lessons.dto';
import { UserLessonsService } from '../../services/user-lessons/user-lessons.service';
import { UserLessonsModel } from '../../models/user-lessons.model';

@ApiUseTags('Lessons')
@Controller('user-lessons')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // // @UseGuards(new JwtAuthGuard())
export class UserLessonsController {
  constructor(private readonly userLessons: UserLessonsService) {}
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: UserLessonsModel })
  @Post()
  create(@Body() body: CreateUserLessonsDto) {
    return this.userLessons.create(body);
  }
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: UserLessonsModel })
  @Get()
  list() {
    return this.userLessons.list();
  }
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: UserLessonsModel })
  @Get(':id')
  @ApiImplicitParam({name:'id', required: true, type: 'string'})
  details(@Param('id') id: string) {
    return this.userLessons.detail(id);
  }
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: UserLessonsModel })
  @Put(':id')
  @ApiImplicitParam({name:'id', required: true, type: 'string'})
  update(
    @Param('id') id,
    @Body()
    body: Partial<UpdateUserLessonsDto>,
  ) {
    return this.userLessons.update(id, body);
  }
  @Delete(':id')
  @ApiImplicitParam({name:'id', required: true, type: 'string'})
  delete(@Param('id') id: string) {
    return this.userLessons.delete(id);
  }
}
