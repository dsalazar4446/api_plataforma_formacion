import { Test, TestingModule } from '@nestjs/testing';
import { LessonDetailsController } from './lesson-details.controller';

describe('LessonDetails Controller', () => {
  let controller: LessonDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LessonDetailsController],
    }).compile();

    controller = module.get<LessonDetailsController>(LessonDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
