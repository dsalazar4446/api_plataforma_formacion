import {
  Controller,
  UseInterceptors,
  UseFilters,
  UseGuards,
  Post,
  UsePipes,
  Body,
  Get,
  Param,
  Put,
  Delete,
  Req,
} from '@nestjs/common';

import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { CreateLessonDetailDto, UpdateLessonDetailDto } from '../../dto/lesson-detail.dto';
import { LessonDetailsService } from '../../services/lesson-details/lesson-details.service';
import { LessonDetailsModel } from '../../models/lesson-details.model';

@ApiUseTags('Lessons')
@Controller('lesson-details')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // @UseGuards(new JwtAuthGuard())
export class LessonDetailsController {
  constructor(private readonly lessonsDetails: LessonDetailsService) {}

  @Post()
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: LessonDetailsModel })
  create(@Body() body: CreateLessonDetailDto, @Req() req) {
    body.languageType = req.headers.language
    return this.lessonsDetails.create(body);
  }

  @Get()
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: LessonDetailsModel })
  list( @Req() req) {
    return this.lessonsDetails.list(req.headers.language);
  }

  @Get(':id')
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: LessonDetailsModel })
  @ApiImplicitParam({name:'id', required: true, type: 'string'})
  details(@Param('id') id: string,  @Req() req) {
    return this.lessonsDetails.detail(id, req.headers.language);
  }

  @Put(':id')
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: LessonDetailsModel })
  @ApiImplicitParam({name:'id', required: true, type: 'string'})
  update(
    @Param('id') id,
    @Body()
    body: Partial<UpdateLessonDetailDto>,
    @Req() req
  ) {
    body.languageType = req.headers.language
    return this.lessonsDetails.update(id, body);
  }
  @Delete(':id')
  @ApiImplicitParam({name:'id', required: true, type: 'string'})
  delete(@Param('id') id: string) {
    return this.lessonsDetails.delete(id);
  }
}
