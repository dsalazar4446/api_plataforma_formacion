import { Injectable, Inject } from '@nestjs/common';
import { UserModel } from './../../user/models/user.Model';
import { ToDo } from '../models/todo.models';
import { ToDoSaveJson, ToDoUpdateJson } from '../interfaces/todo.interface';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller ToDoService
 * @Creado 09 de Mayo 2019
 */

@Injectable()
export class ToDoService {

    private include = {
        include: [
            {
                model: UserModel
            }
        ]
    }

    constructor(
        @Inject('ToDo') private readonly _toDo: typeof ToDo
    ) { }

    async saveToDo(bodyToDo: ToDoSaveJson): Promise<ToDoUpdateJson> {
        return await new this._toDo(bodyToDo).save();
    }

    async showAllToDo(): Promise<ToDoUpdateJson[]> {
        return await this._toDo.findAll(this.include);
    }

    async getDetails(toDoId: string) {
        return await this._toDo.findByPk(toDoId, this.include);
    }

    async updateToDo(id: string, bodyToDo: Partial<ToDoUpdateJson>): Promise<[number, Array<ToDoUpdateJson>]> {
        return await this._toDo.update(bodyToDo, {
            where: { id },
            returning: true
        });
    }

    async destroyToDo(toDoId: string): Promise<number> {
        return await this._toDo.destroy({
            where: { id: toDoId },
        });
    }

}
