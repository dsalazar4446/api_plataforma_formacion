import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { UserModel } from './../../user/models/user.Model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'to_do_lists',
})
export class ToDo extends Model<ToDo>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'to_do_txt',
    })
    toDoTxt: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('undone', 'done'),
        defaultValue: 'undone',
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'to_do_status',
    })
    toDoStatus: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('high', 'medium', 'low'),
        defaultValue: 'low',
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'to_do_prioirity',
    })
    toDoPrioirity: string;
        
    @ApiModelPropertyOptional()
    @BelongsTo(() => UserModel)
    userModel: UserModel;
    
    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
