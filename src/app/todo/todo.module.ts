import { Module, HttpModule } from "@nestjs/common";
import { DatabaseModule } from "../database/database.module";
import { SharedModule } from "../shared/shared.module";
import { ToDoController } from './controllers/todo.controller';
import { ToDo } from './models/todo.models';
import { ToDoService } from './services/todo.service';


const controllers = [ToDoController];
const models = [ToDo];
const providers = [ToDoService];

@Module({
    imports: [
        SharedModule,
        HttpModule,
        DatabaseModule.forRoot({
            sequelize: {
                models,
            },
            loki: true,
        }),
    ],
    providers,
    controllers,
})
export class ToDoListModule { }
