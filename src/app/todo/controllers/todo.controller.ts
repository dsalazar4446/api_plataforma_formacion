import { Controller, UseFilters, UseInterceptors, Post, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { ToDoService } from '../services/todo.service';
import { ToDoSaveJson } from '../interfaces/todo.interface';
import { ToDo } from '../models/todo.models';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller ToDoController
 * @Creado 09 de Mayo 2019
 */

@ApiUseTags('Module-ToDo')
@Controller('to-do')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class ToDoController {

    constructor(private readonly _toDoService: ToDoService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: ToDo,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyToDo: ToDoSaveJson) {
        const createToDo = await this._toDoService.saveToDo(bodyToDo);
        if (!createToDo) {
            throw this.appUtilsService.httpCommonError('ToDo no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM63',
                languageType: 'es',
            });
        }
        return createToDo;
    }


    @ApiResponse({
        status: 200,
        type: ToDo,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idToDo: string) {
        const fetchToDo = await this._toDoService.getDetails(idToDo);
        if (!fetchToDo) {
            throw this.appUtilsService.httpCommonError('ToDo no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM64',
                languageType: 'es',
            });
        }
        return fetchToDo;
    }


    @ApiResponse({
        status: 200,
        type: ToDo,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAll() {
        const fetch = await this._toDoService.showAllToDo();
        if (!fetch) {
            throw this.appUtilsService.httpCommonError('ToDo no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM64',
                languageType: 'es',
            });
        }
        return fetch;
    }


    @ApiResponse({
        status: 200,
        type: ToDo,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    async updateToDo(@Param('id') id: string, @Body() bodyToDo: Partial<ToDoSaveJson>) {
        const updateToDo = await this._toDoService.updateToDo(id, bodyToDo);
        if (!updateToDo[1][0]) {
            throw this.appUtilsService.httpCommonError('ToDo no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM64',
                languageType: 'es',
            });
        }
        return updateToDo;
    }


    @ApiResponse({
        status: 200,
        type: ToDo,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteToDo(@Param('id') idToDo: string) {

        const deleteToDo = await this._toDoService.destroyToDo(idToDo);
        if (!deleteToDo) {
            throw this.appUtilsService.httpCommonError('ToDo no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM64',
                languageType: 'es',
            });
        }
        return deleteToDo;
    }
}
