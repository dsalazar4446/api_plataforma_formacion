import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional } from 'class-validator';
export class ToDoSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    userId: string;

    @ApiModelProperty()
    @IsString({message: 'toDoTxt property must a be string'})
    @IsNotEmpty({message: 'toDoTxt property not must null'})
    toDoTxt: string;

    @ApiModelProperty()
    @IsString({message: 'toDoStatus property must a be string'})
    @IsOptional()
    toDoStatus: string;

    @ApiModelProperty()
    @IsString({message: 'toDoPrioirity property must a be string'})
    @IsOptional()
    toDoPrioirity: string;
}
export class ToDoUpdateJson extends ToDoSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
