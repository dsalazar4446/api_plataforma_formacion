import {  MulterOptions } from "@nestjs/common/interfaces/external/multer-options.interface";

import { diskStorage } from 'multer';
import { extname } from "path";

export const arrayFiles = [
    { name: 'membershipImage', maxCount: 1 },
    { name: 'membershipIcon', maxCount: 1 },
];

export const localOptions: MulterOptions = {
    storage: diskStorage({
        destination: './uploads/memberships'
        , filename: (req, file, cb) => {
            let extValidas = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG'];

            if (extValidas.indexOf(extname(file.originalname)) < 0) {
                cb('valid extensions: ' + extValidas.join(', '));
                return;
            }
            const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
            if (file.fieldname === 'membershipIcon') {
                req.body.membershipIcon = `${randomName}${extname(file.originalname)}`;
            }
            if (file.fieldname === 'membershipImage') {
                req.body.membershipImage = `${randomName}${extname(file.originalname)}`;
            }
            cb(null, `${randomName}${extname(file.originalname)}`)
        }
    })
}