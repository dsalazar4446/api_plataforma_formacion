import { Test, TestingModule } from '@nestjs/testing';
import { MembershipDetailService } from './membership-detail.service';

describe('MembershipDetailService', () => {
  let service: MembershipDetailService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MembershipDetailService],
    }).compile();

    service = module.get<MembershipDetailService>(MembershipDetailService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
