import { Injectable, Inject } from '@nestjs/common';
import { MembershipDetailsModel } from '../../models/membership-detail.model';
import { CreateDetailDto } from '../../dto/create-detail.dto';
import { UpdateMembershipDetailDto } from '../../dto/update-membership-detail.dto';

@Injectable()
export class MembershipDetailService {
    constructor(@Inject('MembershipDetailsModel') private readonly detailModel: typeof MembershipDetailsModel) {}

    async create(detail: CreateDetailDto){
        const createDetail = new MembershipDetailsModel(detail);
        return await createDetail.save();
    }

    async list() {
        return await this.detailModel.findAll();
    }

    async detail(param){
        return await this.detailModel.findById(param.id);
    }
 
    async update(idMembershipsDetail: string, membershipsUpdate: Partial<UpdateMembershipDetailDto>){
        return await this.detailModel.update(membershipsUpdate, {where: {id: idMembershipsDetail}});
    }

    async delete(id: string){
        return this.detailModel.destroy({where: {id}});
    }

}
