import { Test, TestingModule } from '@nestjs/testing';
import { UserMembershipsService } from './user-memberships.service';

describe('UserMembershipsService', () => {
  let service: UserMembershipsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserMembershipsService],
    }).compile();

    service = module.get<UserMembershipsService>(UserMembershipsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
