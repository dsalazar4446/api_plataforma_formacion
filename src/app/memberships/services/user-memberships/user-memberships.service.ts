import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { UserMembershipsModel } from '../../models/user-memberships.model';
import { CreateUserMembershipDto } from '../../dto/create-user-membership.dto';
import { Op } from 'sequelize';
import { MembershipPricingService } from '../membership-pricing/membership-pricing.service';
import { UserMembershipsHasPricingsService } from '../user-memberships-has-pricings/user-memberships-has-pricings.service';
import { MembershipPricingsModel } from '../../models/membership-pricings.model';
import * as moment from 'moment'
import { MembershipComissionsService } from '../membership-comissions/membership-comissions.service';
import { MembershipsService } from '../membership/membership.service';
import { CreateUserMembershipsHasComissionsService } from '../create-user-memberships-has-comissions/create-user-memberships-has-comissions.service';
import { MembershipsComissionsModel } from '../../models/membership-comissions.model';
import { UserMembershipActivationPaymentService } from '../user-membership-activation-payment/user-membership-activation-payment.service';
import { CreateActivationPaymentDto } from '../../dto/create-activation-payments.dto';
import { UpdateActivationPaymentDto } from '../../dto/update-activation-payments.dto';
import { UpdateUserMembershipDto } from '../../dto/update-user-membership.dto';
import { UserMembershipActivationPaymentsModel } from '../../models/user-membership-activation-payments.model';
@Injectable()
export class UserMembershipsService {
    constructor(
        @Inject('UserMembershipsModel') private readonly userMembeshipModel: typeof UserMembershipsModel,
        private readonly pricing: MembershipPricingService,
        private readonly comissions: MembershipComissionsService,
        private readonly userMembershipPricing: UserMembershipsHasPricingsService,
        private readonly userMembershipComissions: CreateUserMembershipsHasComissionsService,
        private readonly userMembershipActivationPaymentServices: UserMembershipActivationPaymentService,
    ) {}

    async create(body: CreateUserMembershipDto) {
        console.log(body)
        // creando registro user membership
        const userMembership = new UserMembershipsModel(body, 
            { include: [MembershipPricingsModel, MembershipsComissionsModel]
        });
        // hora actual                                                                                                                                                                                                                                                                                                                                                                                          
        const currentDate = new Date();

        //inicializando tiempos
        userMembership.membershipPaymentDate = currentDate;
        userMembership.membershipStartDate = currentDate;
        userMembership.membershipEndDate = currentDate;

        // tipo inicial de membrecia 'first time'
        userMembership.membershipType = "0";
        userMembership.membershipStatus = true;

        const result = await userMembership.save();
                                                                                                                     
        const pricing = await this.pricing.findByMembershipId(userMembership.membershipId);
        
        if(!pricing){
            throw new HttpException('no se pudo encontrar el precio de la membresia', HttpStatus.UNPROCESSABLE_ENTITY);
        }
        const comission = await this.comissions.findComissionByMembershipId(userMembership.membershipId);
        if (!comission) {
            throw new HttpException('no se pudo encontrar el precio de la comision', HttpStatus.UNPROCESSABLE_ENTITY);
        }
        if(pricing){
            result.$add('membershipPricings',[pricing.id]).then(
                item => {console.log('userMemebership/membershipPricings success')}
            ).catch(
                err => {
                    console.log(err)
                }
            );
        }
        if(comission){
            result.$add('membershipComissions', [comission.id]).then(
                item => { console.log('userMemebership/membershipComissions success') }
            )
            .catch(
                err => {
                    console.log(err)
                }
            );
        }
        // if(pricing){
        //     const union = await this.userMembershipPricing.create({
        //         userMembershipsId: result.id,
        //         membershipsPricingsId: pricing.id
        //     });
        //     if(!union){
        //         throw new HttpException('Imposible procesar recurso userMemebership/pricing', HttpStatus.UNPROCESSABLE_ENTITY)
        //     }
        // }   
        // if (comission) {
        //     const union = await this.userMembershipComissions.create({
        //         userMembershipsId: result.id,
        //         membershipsComissionsId: comission.id
        //     });
        //     if (!union) {
        //         throw new HttpException('Imposible procesar recurso userMemebership/comissions', HttpStatus.UNPROCESSABLE_ENTITY)
        //     }
        // } 
        return result;
    }

    async list(membershipStartDateStart?, membershipStartDateEnd?, membershipEndDateStart?, membershipEndDateEnd?, type?){
        
        if (!membershipStartDateStart &&
            !membershipStartDateEnd &&
            !membershipEndDateStart &&
            !membershipEndDateEnd &&
            !type){
            return this.userMembeshipModel.findAll({
                include: [MembershipPricingsModel, MembershipsComissionsModel, UserMembershipActivationPaymentsModel]
            });
        }
        
        return await this.userMembeshipModel.findAll({
            include: [MembershipPricingsModel, MembershipsComissionsModel, UserMembershipActivationPaymentsModel],
            where: {
            [Op.or]: {
                membershipStartDate: {
                    [Op.between]: [membershipStartDateStart, membershipStartDateEnd ],
                },
                membershipEndDate: {
                    [Op.between]: [ membershipEndDateStart, membershipEndDateEnd ],
                },
                membershipType: type,
            },
        },
        order: [
            ['membershipPaymentDate', 'DESC'],
            ['membershipStartDate', 'DESC'],
            ['membershipEndDate', 'DESC'],
            ['membershipType', 'DESC'],
        ]
    });
    }

    async detail(id: string) {
        return await this.userMembeshipModel.findByPk(id, {
            include: [MembershipPricingsModel, MembershipsComissionsModel, UserMembershipActivationPaymentsModel]
        })
    }
    async findByUserId(userId: string){
        return this.userMembeshipModel.findOne({
            include: [MembershipPricingsModel, MembershipsComissionsModel, UserMembershipActivationPaymentsModel],
            where: {
                userId
            }
        })
    }
    async updateSuccess(id: string, param: UpdateUserMembershipDto) {
        // busca el user-membership
        const user =  await this.userMembeshipModel.findOne({
            include: [MembershipPricingsModel, MembershipsComissionsModel, UserMembershipActivationPaymentsModel],
            where:{id}
        })
        //busca el precio de la membresia 
        const pricing = user.membershipPricings.find(element => {
            return element.membershipId == user.membershipId;
        })
        // setea el start date
        user.membershipStartDate = new Date();
        
        // setea el end date
        const dateEnd = moment(user.membershipStartDate).add(pricing.membershipPricingMonths,'months');
        user.membershipEndDate = new Date(dateEnd.toISOString())
        
        if(pricing.membershipPricingMonths > 1){
            for (let i = 1; i < pricing.membershipPricingMonths; i++){
                const body: CreateActivationPaymentDto = {
                    userMembershipsId: id,
                    paymentAmmount: pricing.membershipPricingMonthlyActivationValue,
                    paymentSchedule: new Date(moment(user.membershipStartDate).add(i, 'months').toISOString()),
                    paymentStatus: 0,
                }
                this.userMembershipActivationPaymentServices.create(body);
            }
        }

        return user.save()
    }

    async checkPayments(id: string){
        const user = await this.userMembeshipModel.findOne({
            include: [MembershipPricingsModel, MembershipsComissionsModel, UserMembershipActivationPaymentsModel],
            where: { id }
        })

        const payment = user.activationPayments.find(payment =>{
            return  payment.paymentSchedule >= new Date() && payment.paymentStatus == 0
        })

        if(!payment){
            return false;
        }

        return true;
    }

    delete(id: string){
        this.userMembeshipModel.destroy({where:{id}});
    }
}
