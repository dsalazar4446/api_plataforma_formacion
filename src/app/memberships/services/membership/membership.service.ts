import { Injectable, Inject } from '@nestjs/common';
import { CONFIG } from '../../../../config/index';
import { CreateMembershipDto } from '../../dto/create-membeship.dto';
import { UpdateMembershipDto } from '../../dto/update-membership.dto';
import { MembershipModel } from '../../models/membership.model';
import * as ShortUniqueId from 'short-unique-id';
import { completeUrl } from '../../../shared/utils/completUrl';
import * as path from 'path';
import * as fs from 'fs';
import { MembershipDetailsModel } from '../../models/membership-detail.model';
import { MembershipPricingsModel } from '../../models/membership-pricings.model';
import { MembershipsComissionsModel } from '../../models/membership-comissions.model';
import { ProgramsModel } from '../../../programs/models/programs.model';

 
@Injectable()
export class MembershipsService {

    constructor(
        @Inject('MembershipModel') private readonly membershipModel: typeof MembershipModel,
    ) { }
    async findAll(): Promise<MembershipModel[]> {
        const result = await this.membershipModel.findAll({
            include: [MembershipDetailsModel, MembershipPricingsModel, MembershipsComissionsModel,ProgramsModel]
        });
        result.map(
            (membership) => {
                membership.dataValues.membershipIcon = completeUrl('memberships', membership.dataValues.membershipIcon);
                membership.dataValues.membershipImage = completeUrl('memberships', membership.dataValues.membershipImage);
            }
        )
        return  result;
    }
    async findById(id: string): Promise<MembershipModel> {
        const result =  await this.membershipModel.findById<MembershipModel>(id,{
            include:[MembershipDetailsModel, MembershipPricingsModel,MembershipsComissionsModel]
        });
        result.dataValues.membershipIcon = completeUrl('memberships', result.dataValues.membershipImage);;
        result.dataValues.membershipImage = completeUrl('memberships', result.dataValues.membershipImage);;
        return result;
    }
    async create(memberships: any): Promise<MembershipModel> {
        
        const uid = new ShortUniqueId();
        memberships.membershipCode = uid.randomUUID(9);
        const result = await this.membershipModel.create<MembershipModel>(memberships, {
            returning: true,
        });
        
        result.dataValues.membershipIcon = completeUrl('memberships', result.dataValues.membershipIcon);
        result.dataValues.membershipImage = completeUrl('memberships', result.dataValues.membershipImage);
        return result;
    }
    async update(idMemberships: string, membershipsUpdate: Partial<UpdateMembershipDto>){

        if( membershipsUpdate.membershipImage ){
            await this.deleteImg(idMemberships);
        }

        const result: any = await this.membershipModel.update(membershipsUpdate, {
            where: {
                id: idMemberships,
            },
            returning: true,
        });
        
        result[1][0].dataValues.membershipIcon = completeUrl('memberships', result[1][0].dataValues.membershipIcon);
        result[1][0].dataValues.membershipImage = completeUrl('memberships', result[1][0].dataValues.membershipImage);
        return result;
    }
    async deleted(idMemberships: string): Promise<any> {

        await this.deleteImg(idMemberships);
        return await this.membershipModel.destroy({
            where: {
                id: idMemberships,
            },
        });
    }

    private async deleteImg(id:string){
        const img = await this.membershipModel.findByPk(id).then(item => item.membershipImage).catch(err => err);
        const pathImagen = path.resolve(__dirname, `../../../../../uploads/memberships/${img}`);
    
        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }
}
