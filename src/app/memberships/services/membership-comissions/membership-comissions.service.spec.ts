import { Test, TestingModule } from '@nestjs/testing';
import { MembershipComissionsService } from './membership-comissions.service';

describe('MembershipComissionsService', () => {
  let service: MembershipComissionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MembershipComissionsService],
    }).compile();

    service = module.get<MembershipComissionsService>(MembershipComissionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
