import { Injectable, Inject } from '@nestjs/common';
import * as Bluebird from 'bluebird';
import { MembershipsComissionsModel } from '../../models/membership-comissions.model';
import { CreateMembershipComissionsDto } from '../../dto/create-membership-comissions.dto';
import { UpdateMembershipComissionsDto } from '../../dto/update-membership-comissions.dto';

@Injectable()
export class MembershipComissionsService {
    constructor(@Inject('MembershipsComissionsModel') private readonly comissions: typeof MembershipsComissionsModel) {}

    async create(param: CreateMembershipComissionsDto) {
        if(param.comissionStatus){
            const comission = await this.comissions.findAll({where:{
                membershipsId: param.membershipsId
            }})
            if(comission.length){
                comission.forEach(async element => {
                    element.comissionStatus = false
                    element.save()
                });
            }
        }
        const activation = new MembershipsComissionsModel(param);
        return await activation.save();
    }

    async list(){
        return await this.comissions.findAll();
    }

    async detail(id: string){
        return await this.comissions.findById(id)
    }
    async findComissionByMembershipId(id: string){
        return this.comissions.findOne({
            where: { membershipsId: id }
        })
    }
    async update(id, param: UpdateMembershipComissionsDto) {
        if(param.comissionStatus){
            const comission = await this.comissions.findAll({where:{
                membershipsId: param.membershipsId
            }})
            if(comission.length){
                comission.forEach(async element => {
                    element.comissionStatus = false
                    element.save()
                });
            }
        }
        return await this.comissions.update(param, {where: {id}});
    }

    delete(id){
        this.comissions.destroy({where:{id}});
    }
}
