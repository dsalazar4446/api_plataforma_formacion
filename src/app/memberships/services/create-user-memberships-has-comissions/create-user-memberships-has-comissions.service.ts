import { Injectable, Inject } from '@nestjs/common';
import { UserMembershipsComissionsModel } from '../../models/user-memberships-has-comissions.model';
import { CreateUserMembershipsComissionsDto } from '../../dto/create-user-memberships-has-comissions.dto';

@Injectable()
export class CreateUserMembershipsHasComissionsService {
    constructor(@Inject('UserMembershipsComissionsModel') private readonly userMembershipComissions: typeof UserMembershipsComissionsModel ){

    }

    async create(body: CreateUserMembershipsComissionsDto){
        const createUserMembershipsComissions = new UserMembershipsComissionsModel(body)
        return await createUserMembershipsComissions.save();
    }

    async delete(userMembershipsId: string){
        return await this.userMembershipComissions.destroy({ where: { userMembershipsId }});
    }
}
