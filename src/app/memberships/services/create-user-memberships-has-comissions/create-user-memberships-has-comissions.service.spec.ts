import { Test, TestingModule } from '@nestjs/testing';
import { CreateUserMembershipsHasComissionsService } from './create-user-memberships-has-comissions.service';

describe('CreateUserMembershipsHasComissionsService', () => {
  let service: CreateUserMembershipsHasComissionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CreateUserMembershipsHasComissionsService],
    }).compile();

    service = module.get<CreateUserMembershipsHasComissionsService>(CreateUserMembershipsHasComissionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
