import { Test, TestingModule } from '@nestjs/testing';
import { UserMembershipActivationPaymentService } from './user-membership-activation-payment.service';

describe('UserMembershipActivationPaymentService', () => {
  let service: UserMembershipActivationPaymentService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserMembershipActivationPaymentService],
    }).compile();

    service = module.get<UserMembershipActivationPaymentService>(UserMembershipActivationPaymentService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
