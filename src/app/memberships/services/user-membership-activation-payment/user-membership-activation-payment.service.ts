import { Injectable, Inject } from '@nestjs/common';
import { UserMembershipActivationPaymentsModel } from '../../models/user-membership-activation-payments.model';
import * as Bluebird from 'bluebird';
import { CreateActivationPaymentDto } from '../../dto/create-activation-payments.dto';
import { UpdateActivationPaymentDto } from '../../dto/update-activation-payments.dto';
import { Op } from 'sequelize';
import { TransactionItemsModel } from '../../../transactions/models/transaction-items.model';
import { UsersService } from '../../../user/services/users.service';
import { UserMembershipsService } from '../user-memberships/user-memberships.service';
import { UserMembershipsModel } from '../../models/user-memberships.model';
@Injectable()
export class UserMembershipActivationPaymentService {
    constructor(
        @Inject('UserMembershipActivationPaymentsModel') private readonly activationPayment: typeof UserMembershipActivationPaymentsModel,
        @Inject('UserMembershipsModel') private readonly userMembeshipModel: typeof UserMembershipsModel,
        private readonly usersService: UsersService,
    ) {}

    async create(param: CreateActivationPaymentDto) {
        const activation = new UserMembershipActivationPaymentsModel(param);
        return await activation.save();
    }

    // tslint:disable-next-line: max-line-length
    async list(perPage= 10, page= 1, paymentAmountStart?,paymentAmountEnd?,paymentScheduleStart?,paymentScheduleEnd?, status?){
        const count = await this.activationPayment.findAndCountAll()
        const pages = Math.ceil(count.count / perPage);
        let offset = 0;
        let result;
        offset = perPage * (page -1);
        if (
            !paymentAmountStart &&
            !paymentAmountEnd &&
            !paymentScheduleStart &&
            !paymentScheduleEnd &&
            !status
        ){
            result = await this.activationPayment.findAll({
                limit: perPage,
                offset,
            });
        }else{
            result = await this.activationPayment.findAll({
                where: {
                    [Op.or]: {
                        paymentAmmount: { [Op.between]: [paymentAmountStart, paymentAmountEnd] },
                        paymentSchedule: { [Op.between]: [paymentScheduleStart, paymentScheduleEnd] },
                        paymentStatus: status,
                    },
                },
                limit: perPage,
                offset,
            });
        }
        
        return {
            data: result,
            current: page,
            pages,
        };
    }

    async detail(id: string) {
        return await this.activationPayment.findById(id, {
            include: [
                {
                    model: TransactionItemsModel
                }
            ]
        })
    }


    async getStatus(id: string){
        return await this.activationPayment.findByPk(id,{
            attributes: ['payment_status'],
        })
    }

    async update(id, param: UpdateActivationPaymentDto) {
        return this.activationPayment.update(param, {where: {id}});
    }

    delete(id){
        this.activationPayment.destroy({where: {id}});
    }
}
