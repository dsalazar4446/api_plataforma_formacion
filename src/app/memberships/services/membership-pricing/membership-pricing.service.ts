import { Injectable, Inject } from '@nestjs/common';
import { MembershipPricingsModel } from '../../models/membership-pricings.model';
import { CreatePricingDto } from '../../dto/create-pricing.dto';
import * as Bluebird from 'bluebird';
import { TransactionItemsModel } from '../../../transactions/models/transaction-items.model';
import { UserMembershipsModel } from '../../models/user-memberships.model';

@Injectable()
export class MembershipPricingService {
    constructor(
        @Inject('MembershipPricingsModel') private readonly pricingModel: typeof MembershipPricingsModel
    ) {}

    async create(pricing: CreatePricingDto) {
        if(pricing.membershipPricingStatus){
            const precios = await this.pricingModel.findAll({
                where: {
                    membershipId: pricing.membershipId
                }
            })
            if(precios.length) {
                precios.forEach(async element => {
                    element.membershipPricingStatus = false;
                    await element.save()
                })
            }
        }
        const precio = new MembershipPricingsModel(pricing);
        return await precio.save();
    }
    async list(mount?, maxMount? , pricingStatus?) {
        // const data = await this.pricingModel.findAndCountAll()
        // const pages = Math.ceil(data.count / perPage)
        // let offset = 0;
        // offset = perPage * (page -1);
        if (!mount && !maxMount && !pricingStatus){
            return await this.pricingModel.findAll({
                include: [
                    {
                        model: UserMembershipsModel
                    },
                    {
                        model: TransactionItemsModel
                    }
                ],
            })    
        }
        return await this.pricingModel.findAll({
            include: [
                {
                    model: UserMembershipsModel
                },
                {
                    model: TransactionItemsModel
                }
            ],
            where:{
                $or:[
                    {
                        membershipPricingAmmount: mount,
                    },
                    {
                        membershipPricingAmmount: {
                            $gte: mount,
                            $lte: maxMount,
                        },
                    },
                    {
                        membershipPricingStatus: pricingStatus,
                    },
                ],
            },
            // limit: perPage,
            // offset,
        });
        // return {
        //     result,
        //     pages,
        //     current: page,
        // }
    }

    async detail(param: any) {
        return await this.pricingModel.findById(param.id,{
            include: [
                {
                    model: UserMembershipsModel
                },
                {
                    model: TransactionItemsModel
                }
            ]
        });
    }
    async findByMembershipId(membershipId:string){
        return await this.pricingModel.findOne({
            where:{membershipId}
        });
    }
    async update(param) {
        if(param.membershipPricingStatus){
            const precios = await this.pricingModel.findAll({
                where: {
                    membershipId: param.membershipId
                }
            })
            if(precios.length) {
                precios.forEach(async element => {
                    element.membershipPricingStatus = false;
                    await element.save()
                })
            }
        }
        this.pricingModel.update(param, {where: {id: param.id}});
    }
    
    async delete(id) {
        return this.pricingModel.destroy({
            where: {id},
        });
    }
    
}
