import { Test, TestingModule } from '@nestjs/testing';
import { UserMembershipsHasPricingsService } from './user-memberships-has-pricings.service';

describe('UserMembershipsHasPricingsService', () => {
  let service: UserMembershipsHasPricingsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserMembershipsHasPricingsService],
    }).compile();

    service = module.get<UserMembershipsHasPricingsService>(UserMembershipsHasPricingsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
