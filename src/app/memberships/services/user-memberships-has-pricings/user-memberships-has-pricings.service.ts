import { Injectable, Inject } from '@nestjs/common';
import { UserMembershipPricingModel } from '../../models/user-memberships-has-pricings.model';
import { CreateUserMembershipsPricingsDto } from '../../dto/user-memberships-has-pricings.dto';

@Injectable()
export class UserMembershipsHasPricingsService {
    constructor(@Inject('UserMembershipPricingModel') private readonly userMembershipPricingModel: typeof UserMembershipPricingModel) {}

    async create(body: CreateUserMembershipsPricingsDto){
        const instance = new UserMembershipPricingModel(body);
        return instance.save();
    }

    async delete(id: string){
        return this.userMembershipPricingModel.destroy({ where: { userMembershipsId:id}});
    }
}
