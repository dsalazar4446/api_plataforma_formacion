import { IsUUID } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class membershipsPrograms {
    @ApiModelProperty()
    @IsUUID('4')
    programsId: string;
    @ApiModelProperty()
    @IsUUID('4')
    membershipsId: string;
}