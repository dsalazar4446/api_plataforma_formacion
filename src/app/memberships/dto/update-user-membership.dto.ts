import { CreateUserMembershipDto } from './create-user-membership.dto';
import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';

export class UpdateUserMembershipDto extends CreateUserMembershipDto{
    @IsUUID('4')
    @ApiModelProperty()
    id: string;
}
