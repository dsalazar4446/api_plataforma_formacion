import { ApiModelProperty } from "@nestjs/swagger";
import { IsBoolean, IsUUID, IsNumber, IsString } from "class-validator";

export class CreatePricingDto {

    @ApiModelProperty()
    @IsString()
    public membershipId: string;

    @ApiModelProperty()
    @IsNumber()
    public membershipPricingMonths: number;

    @ApiModelProperty()
    @IsNumber()
    public membershipPricingAmmount: number;

    @ApiModelProperty()
    @IsNumber()
    public membershipPricingMonthlyActivationValue: number;

    @ApiModelProperty()
    @IsNumber()
    public membershipPricingTaxes: number;

    @ApiModelProperty()
    @IsBoolean()
    public membershipPricingStatus: boolean;

    @ApiModelProperty()
    @IsNumber()
    public membershipOddoId: number;

    @ApiModelProperty()
    @IsBoolean()
    public membershipRenawal: boolean;

    @ApiModelProperty()
    @IsBoolean()
    public membershipUpdate: boolean;

    @ApiModelProperty()
    @IsBoolean()
    public membershipRankings: boolean;

}