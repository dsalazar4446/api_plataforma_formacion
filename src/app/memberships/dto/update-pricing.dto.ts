import { CreatePricingDto } from "./create-pricing.dto";
import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID } from "class-validator";

export class UpdatePricingDto extends CreatePricingDto {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}