import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";
import { IsOptional } from "class-validator";

export class CreateMembershipDto {

    @ApiModelPropertyOptional()
    membershipCode: string;

    @ApiModelProperty()
    @IsOptional()
    membershipIcon?: string;

    @ApiModelProperty()
    @IsOptional()
    membershipImage?: string;

    @ApiModelProperty()
    // membershipLevel: number;
    membershipStatus: boolean;

    @ApiModelProperty()
    membershipTarget: string;

    @ApiModelProperty()
    membershipAdvisores: number;

    @ApiModelProperty()
    membershipLevel: number;
}