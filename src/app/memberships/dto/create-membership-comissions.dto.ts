import { ApiModelProperty } from "@nestjs/swagger";
import { IsNumber, IsUUID, IsString, IsBoolean } from "class-validator";

export class CreateMembershipComissionsDto {
     @ApiModelProperty()
     @IsUUID('4')
     membershipsId: string;

     @ApiModelProperty()
     @IsNumber()
     level: number;

     @ApiModelProperty()
     @IsNumber()
     comissionAmmount: number;

     @ApiModelProperty()
     @IsNumber()
     comissionActivationAmmount: number;

     @ApiModelProperty()
     @IsNumber()
     comissionActivationBroker: number;

     @ApiModelProperty()
     @IsString()
     comissionType: string;
     
     @ApiModelProperty()
     @IsBoolean()
     comissionPayable: boolean;

     @ApiModelProperty()
     @IsBoolean()
     comissionStatus: boolean;

}