import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsUUID, IsOptional, IsBoolean, IsDate } from "class-validator";

export class CreateUserMembershipDto {

    @ApiModelProperty()
    @IsUUID('4')
    userId: string;
    
    @IsUUID('4')
    @ApiModelProperty()
    membershipId: string

    @IsDate()
    @IsOptional()
    @ApiModelProperty()
    membershipPaymentDate: Date;

    @IsDate()
    @IsOptional()
    @ApiModelProperty()
    membershipStartDate: Date;
    
    @IsDate()
    @IsOptional()
    @ApiModelProperty()
    membershipEndDate: Date;
    
    @IsString()
    @IsOptional()
    @ApiModelProperty()
    membershipType: string;
    
    @IsBoolean()
    @IsOptional()
    @ApiModelProperty()
    membershipStatus: boolean;
}