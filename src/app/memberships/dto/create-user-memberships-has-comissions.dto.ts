import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID } from "class-validator";

export class CreateUserMembershipsComissionsDto {
    @ApiModelProperty()
    @IsUUID('4')
    userMembershipsId: string
    @ApiModelProperty()
    @IsUUID('4')
    membershipsComissionsId: string
}