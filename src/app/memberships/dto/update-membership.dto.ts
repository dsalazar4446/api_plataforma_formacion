import { CreateMembershipDto } from './create-membeship.dto';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateMembershipDto extends CreateMembershipDto {
    @ApiModelProperty()
    id: string;
}