import { ApiModelProperty } from '@nestjs/swagger';
import { CreateDetailDto } from './create-detail.dto';

export class UpdateMembershipDetailDto extends CreateDetailDto {
    @ApiModelProperty()
    id: string;
}