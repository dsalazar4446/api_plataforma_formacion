import { CreateMembershipComissionsDto } from './create-membership-comissions.dto';
import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';

export class UpdateMembershipComissionsDto extends CreateMembershipComissionsDto {
   @IsUUID('4')
   @ApiModelProperty()
   id: string;
}
