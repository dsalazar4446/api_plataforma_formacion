import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsUUID, IsOptional } from "class-validator";

export class CreateDetailDto {
    @ApiModelProperty()
    @IsUUID('4')
    membershipId: string;
    @ApiModelProperty()
    @IsString()
    membershipName: string;
    @ApiModelProperty()
    @IsString()
    membershipsBenefits: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    languageType: string;
}