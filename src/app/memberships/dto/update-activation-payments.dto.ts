import { CreateActivationPaymentDto } from './create-activation-payments.dto';
import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';

export class UpdateActivationPaymentDto extends CreateActivationPaymentDto{
  @IsUUID('4')
  @ApiModelProperty()
  id: string;
}