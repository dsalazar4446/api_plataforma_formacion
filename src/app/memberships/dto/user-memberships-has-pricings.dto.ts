import { ApiModelProperty } from "@nestjs/swagger";

export class CreateUserMembershipsPricingsDto {
    @ApiModelProperty()
    userMembershipsId: string 
    @ApiModelProperty()
    membershipsPricingsId: string 
}