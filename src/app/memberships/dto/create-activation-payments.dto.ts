import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsNumber, IsDate, IsBoolean } from "class-validator";

export class CreateActivationPaymentDto {
     @ApiModelProperty()
     @IsUUID('4')
     userMembershipsId: string;

     @ApiModelProperty()
     @IsNumber()
     paymentAmmount: number;

     @ApiModelProperty()
     @IsDate()
     paymentSchedule: Date;

     @IsNumber()
     @ApiModelProperty()
     paymentStatus: number;

}