import { Table, Model, Column, DataType, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { MembershipModel } from '../models/membership.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'membership_details',
})
export class MembershipDetailsModel extends Model<MembershipDetailsModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        autoIncrement:true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => MembershipModel)
    @Column({
        type: DataType.STRING,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'membership_id',
    })
    public membershipId: string;
    @ApiModelPropertyOptional()
    @BelongsTo(() => MembershipModel)
    membership: MembershipModel
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_name',
    })
    public membershipName: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_benefits',
    })
    public membershipsBenefits: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM(['es', 'en', 'it', 'pr']),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    public languageType: string;

    // tslint:disable-next-line: variable-name
    @ApiModelPropertyOptional()
    @CreatedAt public created_at: Date;
    // tslint:disable-next-line: variable-name
    @ApiModelPropertyOptional()
    @UpdatedAt public updated_at: Date;
}