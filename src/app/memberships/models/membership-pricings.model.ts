    import { Column, DataType, Table, Model, ForeignKey, BelongsTo, CreatedAt, UpdatedAt, BelongsToMany, HasMany } from 'sequelize-typescript';
import { MembershipModel} from './membership.model';
import { TransactItemsHMemberPricings } from '../../transactions/models/transact-items-h-member-pricings.model';
import { TransactionItemsModel } from '../../transactions/models/transaction-items.model';
import { UserMembershipsModel } from './user-memberships.model';
import { UserMembershipPricingModel } from './user-memberships-has-pricings.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
@Table({
    tableName: 'membership_pricings',
})

/**
 * @author Daniel Salazar
 * @description Modelo de la tabla membership pricings
 */
export class MembershipPricingsModel extends Model<MembershipPricingsModel> {
    
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        autoIncrement: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => MembershipModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_id',
    })
    public membershipId: string;
    @ApiModelPropertyOptional()
    @BelongsTo(() => MembershipModel)
    membership: MembershipModel

    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_pricing_monts',
    })
    public membershipPricingMonths: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.NUMERIC,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_pricing_ammount',
    })
    public membershipPricingAmmount: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.NUMERIC,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'mebership_pricing_monthly_activation_value',
    })
    public membershipPricingMonthlyActivationValue: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.NUMERIC,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_pricing_taxes',
    })
    public membershipPricingTaxes: number   ;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_pricing_status',
    })
    public membershipPricingStatus: boolean;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_oddo_id',
    })
    public membershipOddoId: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_renawal',
    })
    public membershipRenawal: boolean;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_update',
    })
    public membershipUpdate: boolean;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_rankings',
    })
    public membershipRankings: boolean;

        /**
     * RELACIONES
     * MembershipPricingsModel pertenece a muchos TransactionItemsModel
     */

    // @BelongsToMany(() => TransactionItemsModel , 'transact_items_h_member_pricings', 'transaction_item_id', 'membership_pricing_id')
    // transactionItemsModel: TransactionItemsModel;

    @ApiModelPropertyOptional({ type: UserMembershipsModel, isArray: true })
    @BelongsToMany(() => UserMembershipsModel, () => UserMembershipPricingModel)
    userMemberships: UserMembershipsModel[];
    @ApiModelPropertyOptional({ type: TransactionItemsModel,isArray:true})
    @BelongsToMany(() => TransactionItemsModel, () => TransactItemsHMemberPricings)
    transactionItemsModel: TransactionItemsModel[];
    
    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}