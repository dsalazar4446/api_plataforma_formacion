import { Table, Model, Column, DataType, ForeignKey, CreatedAt, UpdatedAt, BelongsTo, BelongsToMany } from 'sequelize-typescript';
import { UserMembershipsModel } from './user-memberships.model';
import { UserMemActPaymHTranItems } from '../../transactions/models/user-mem-a-pay.model';
import { TransactionItemsModel } from '../../transactions/models/transaction-items.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { TransactionIteArrayExample } from 'src/app/transactions/examples/transaction-items.example';


@Table({
    tableName: 'user_memberships_activations_payments',
})

export class UserMembershipActivationPaymentsModel extends Model<UserMembershipActivationPaymentsModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserMembershipsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field:'user_memberships_id',
    })
    public userMembershipsId: string;
    
    @BelongsTo(() => UserMembershipsModel)
    public userMemberships: UserMembershipsModel;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'payment_ammount',
    })
    public paymentAmmount: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field:'payment_schedule',
    })
    public paymentSchedule: Date;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TINYINT,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'payment_status',
    })
    public paymentStatus: number;


    /**
     * RELACIONES
     * UserMembershipActivationPaymentsModel pertenece a  TransactionItemsModel
     */

    // @BelongsToMany(() => TransactionItemsModel , 'user_mem_act_paym_h_tran_items', 'transaction_item_id', 'user_mem_a_payments_id')
    // transactionItemsModel: TransactionItemsModel;
    @ApiModelPropertyOptional({ example: TransactionIteArrayExample })
    @BelongsToMany(() => TransactionItemsModel, () => UserMemActPaymHTranItems)
    transactionItemsModel: TransactionItemsModel[];

    // tslint:disable-next-line: variable-name
    @CreatedAt public created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt public updated_at: Date;
}