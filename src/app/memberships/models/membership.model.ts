import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, BelongsToMany, HasMany } from 'sequelize-typescript';
import { ProgramsModel } from '../../programs/models/programs.model';
import { MembershipsPrograms } from './memberships-has-programs.model';
import { MembershipPricingsModel } from './membership-pricings.model';
import { MembershipsComissionsModel } from './membership-comissions.model';
import { MembershipDetailsModel } from './membership-detail.model';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
@Table({
    tableName: 'memberships',
})
export class MembershipModel extends Model<MembershipModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        autoIncrement:true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'membership_code',
    })
    public membershipCode: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_icon',
    })
    public membershipIcon: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'mermership_image',
    })
    public membershipImage: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_status',
    })
    membershipStatus: boolean;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM(['1', '2']),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_target',
    })
    membershipTarget: string;
    @ApiModelProperty()
    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_level',
    })
    membershipLevel: number;
    @ApiModelProperty()
    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        field: 'membership_advisores',
    })
    membershipAdvisores: number;
    @ApiModelProperty({
        type:MembershipPricingsModel,
        isArray: true,
        example: [
            {
                "id": "string",
                "membershipId": "string",
                "membershipPricingMonths": 0,
                "membershipPricingAmmount": 0,
                "membershipPricingMonthlyActivationValue": 0,
                "membershipPricingTaxes": 0,
                "membershipPricingStatus": true,
                "membershipOddoId": 0,
                "membershipRenawal": true,
                "membershipUpdate": true,
                "membershipRankings": true
              }
        ]
    })
    @HasMany(() => MembershipPricingsModel) 
    membershipPricings: MembershipPricingsModel[];
    @ApiModelProperty(
        {
            type: MembershipsComissionsModel,
            isArray: true,
            example: [
                {
                    "id": "string",
                    "membershipsId": "string",
                    "level": 0,
                    "comissionAmmount": 0,
                    "comissionActivationAmmount": 0,
                    "comissionActivationBroker": 0,
                    "comissionType": "string",
                    "comissionPayable": true,
                    "comissionStatus": true
                  }
            ]
        }
    )
    @HasMany(() => MembershipsComissionsModel)
    membershipsComissions: MembershipsComissionsModel[];
    @ApiModelProperty({
        type: MembershipDetailsModel,
        isArray: true,
        example: [
            {
                "id": "string",
                "membershipsId": "string",
                "level": 0,
                "comissionAmmount": 0,
                "comissionActivationAmmount": 0,
                "comissionActivationBroker": 0,
                "comissionType": "string",
                "comissionPayable": true,
                "comissionStatus": true
              }
        ]
    })
    @HasMany(() => MembershipDetailsModel)
    membershipDetails: MembershipDetailsModel[];
    @ApiModelProperty({
        type: ProgramsModel,
        isArray: true,
        example: [
            {
                "id":"string",
                "categoriesId": "string",
                "programCode": "string",
                "programStatus": "string",
                "programBackground": "string",
                "programIcon": "string",
                "fbPromotionImg": "string"
            }
        ]
    })
    @BelongsToMany(() => ProgramsModel, () => MembershipsPrograms)
    programs: ProgramsModel[]

    // tslint:disable-next-line: variable-name
    @ApiModelPropertyOptional()
    @CreatedAt public created_at: Date;
    // tslint:disable-next-line: variable-name
    @ApiModelPropertyOptional()
    @UpdatedAt public updated_at: Date;
}
