import { UserMembershipsModel } from "./user-memberships.model";
import { Table, Model, ForeignKey, Column, DataType, BelongsTo } from "sequelize-typescript";
import { MembershipPricingsModel } from "./membership-pricings.model";

@Table({tableName: 'user_memberships_has_pricings'})

export class UserMembershipPricingModel extends Model<UserMembershipPricingModel>{
    @ForeignKey(() => UserMembershipsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_memberships_id',
    })
    userMembershipsId: string 

    @BelongsTo(() => UserMembershipsModel)
    userMemberships: UserMembershipsModel
    
    @ForeignKey(() => MembershipPricingsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
        field: 'memberships_pricings_id',
    })
    membershipsPricingsId: string 

    @BelongsTo(() => MembershipPricingsModel)
    membershipPricings: MembershipPricingsModel
}