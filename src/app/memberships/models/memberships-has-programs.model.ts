import { Model, Table, Column, DataType, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from "sequelize-typescript";
import { ProgramsModel } from "../../programs/models/programs.model";
import { MembershipModel } from "./membership.model";

@Table({tableName:'membership_has_programs'})
export class MembershipsPrograms extends Model<MembershipsPrograms>{
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;

    @ForeignKey(() => ProgramsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'program_id'
    })
    public programsId: string;
    @BelongsTo(() => ProgramsModel)
    programs: ProgramsModel
    
    @ForeignKey(() => MembershipModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'membership_id'
    })
    public membershipsId: string;
    @BelongsTo(() => MembershipModel)
    membership: MembershipModel

    // tslint:disable-next-line: variable-name
    @CreatedAt public created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt public updated_at: Date;

}