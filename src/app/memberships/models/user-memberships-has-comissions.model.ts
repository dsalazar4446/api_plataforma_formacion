import { Table, ForeignKey, Column, DataType, BelongsTo, Model } from "sequelize-typescript";
import { UserMembershipsModel } from "./user-memberships.model";
import { MembershipsComissionsModel } from "./membership-comissions.model";

@Table({tableName:'user_memberships_has_comissions'})
export class UserMembershipsComissionsModel extends Model<UserMembershipsComissionsModel> {
    @ForeignKey(() => UserMembershipsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_memberships_id',
    })
    userMembershipsId: string

    @BelongsTo(() => UserMembershipsModel)
    userMemberships: UserMembershipsModel

    @ForeignKey(() => MembershipsComissionsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
        field: 'memberships_comissions_id',
    })
    membershipsComissionsId: string

    @BelongsTo(() => MembershipsComissionsModel)
    membershipsComissions: MembershipsComissionsModel
}