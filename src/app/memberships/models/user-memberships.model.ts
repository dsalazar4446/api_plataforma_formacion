import { Table, Model, Column, DataType, ForeignKey, CreatedAt, UpdatedAt, BelongsToMany, HasMany, BelongsTo } from 'sequelize-typescript';
import { MembershipModel } from './membership.model';
import { MembershipPricingsModel } from './membership-pricings.model';
import { MembershipsComissionsModel } from './membership-comissions.model';
import { UserModel } from '../../user/models/user.Model';
import { UserMembershipPricingModel } from './user-memberships-has-pricings.model';
import { UserMembershipActivationPaymentsModel } from './user-membership-activation-payments.model';
import { UserMembershipsComissionsModel } from './user-memberships-has-comissions.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { MembershipPricingExample, MembershipPricingArrayExample } from '../examples/membership-pricing.example';
import { UserMembActPayArrayExample } from '../examples/user-memb-act-pay.example';

@Table({
    tableName: 'user_memberships',
})

export class UserMembershipsModel extends Model<UserMembershipsModel> {
    
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => MembershipModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
        field: 'membership_id',
    })
    public membershipId: string;

    @BelongsTo(() => MembershipModel)
    membership: MembershipModel
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    public userId: string;

    @BelongsTo(() => UserModel)
    user: UserModel
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TIME,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'membership_payment_date',
    })
    public membershipPaymentDate: Date;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TIME,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'membership_start_date',
    })
    public membershipStartDate: Date;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TIME,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'membership_end_date',
    })
    public membershipEndDate: Date;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2', '3'),
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'membership_type',
    })
    public membershipType: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'membership_status',
    })
    public membershipStatus: boolean;

    // @HasMany(() => UserMembershipActivationPaymentsModel)
    // userMembershipActivationPayments: UserMembershipActivationPaymentsModel[];

    @ApiModelPropertyOptional({example:MembershipPricingArrayExample})
    @BelongsToMany(() => MembershipPricingsModel, () => UserMembershipPricingModel)
    membershipPricings: MembershipPricingsModel[];
    @ApiModelPropertyOptional({example: MembershipPricingArrayExample})
    @BelongsToMany(() => MembershipsComissionsModel, () => UserMembershipsComissionsModel)
    membershipComissions: MembershipsComissionsModel[];
    @ApiModelPropertyOptional({example: UserMembActPayArrayExample})
    @HasMany(() => UserMembershipActivationPaymentsModel)
    public activationPayments: UserMembershipActivationPaymentsModel[];

    // tslint:disable-next-line: variable-name
    @CreatedAt public created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt public updated_at: Date;
}
