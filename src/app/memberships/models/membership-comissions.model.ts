import { Model, Table, Column, DataType, ForeignKey, CreatedAt, UpdatedAt, BelongsToMany, BelongsTo, HasMany } from 'sequelize-typescript';
import { MembershipModel } from './membership.model';
import { UserMembershipsModel } from './user-memberships.model';
import { UserMembershipsComissionsModel } from './user-memberships-has-comissions.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'membership_comissions',
})

export class MembershipsComissionsModel extends Model<MembershipsComissionsModel> {
    
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => MembershipModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'membership_id',
    })
    public membershipsId: string;
    @BelongsTo(() => MembershipModel)
    memberships: MembershipModel;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'level',
    })
    public level: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'comission_ammount',
    })
    public comissionAmmount: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field:'comission_activation_ammount',
    })
    public comissionActivationAmmount: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field:'comission_activation_broker',
    })
    public comissionActivationBroker: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('percentage', 'money'),
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field:'comission_type',
    })
    public comissionType: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'comission_payable',
    })
    comissionPayable: boolean;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field:'comission_status',
    })
    public comissionStatus: boolean;

    @ApiModelPropertyOptional({ type: UserMembershipsModel, isArray: true})
    @BelongsToMany(() => UserMembershipsModel, () => UserMembershipsComissionsModel)
    userMembership: UserMembershipsModel[];

    // tslint:disable-next-line: variable-name
    @CreatedAt public created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt public updated_at: Date;
}
