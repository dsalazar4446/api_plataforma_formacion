import { Test, TestingModule } from '@nestjs/testing';
import { MembershipPricingController } from './membersip-pricing.controller';

describe('MembersipPricing Controller', () => {
  let controller: MembershipPricingController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MembershipPricingController],
    }).compile();

    controller = module.get<MembershipPricingController>(MembershipPricingController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
