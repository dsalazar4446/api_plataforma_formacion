import { Controller, Body, Post, Get, Param, Delete, Put, UseInterceptors, UsePipes, UseFilters, UseGuards } from '@nestjs/common';
import { MembershipPricingService } from '../../services/membership-pricing/membership-pricing.service';
import { CreatePricingDto } from '../../dto/create-pricing.dto';
import * as Bluebird from 'bluebird'
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { UpdatePricingDto } from '../../dto/update-pricing.dto';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { MembershipPricingsModel } from '../../models/membership-pricings.model';

@ApiUseTags('Memberships')
@Controller('membership-pricing')
@UseFilters(new AppExceptionFilter())
// // @UseGuards(new JwtAuthGuard())
@UseInterceptors(new AppResponseInterceptor())
export class MembershipPricingController {
    constructor(private readonly pricingService: MembershipPricingService) {}

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: MembershipPricingsModel })
    async create(@Body() body: CreatePricingDto){
        return await this.pricingService.create(body);
    }
    
    @Get('list/:mount?/:maxMount?/:pricingStatus?')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: MembershipPricingsModel})
    list(@Param() param) {
        return this.pricingService.list(param.mount, param.maxMount, param.pricingStatus);
    }
    @Get('detail/:id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: MembershipPricingsModel})
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    detail(@Param() param) {
        return this.pricingService.detail(param);
    }

    @Put()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: MembershipPricingsModel})
    update(@Body() body: UpdatePricingDto) {
        return this.pricingService.update(body);
    }

    @Delete(':id')
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    delete(@Param() param) {
        return this.pricingService.delete(param.id);
    }
}
