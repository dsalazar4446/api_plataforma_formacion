import { Controller, Post, UsePipes, UseInterceptors, Body, Get, Param, Put, Delete, UseFilters, UseGuards } from '@nestjs/common';
import { UpdateActivationPaymentDto } from '../../dto/update-activation-payments.dto';
import { CreateActivationPaymentDto } from '../../dto/create-activation-payments.dto';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { UserMembershipActivationPaymentService } from '../../services/user-membership-activation-payment/user-membership-activation-payment.service';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { UserMembershipActivationPaymentsModel } from '../../models/user-membership-activation-payments.model';

@ApiUseTags('Memberships')
@UseFilters(new AppExceptionFilter())
// // @UseGuards(new JwtAuthGuard())
@Controller('memberships/user-membership-activation-payment')
export class UserMembershipActivationPaymentController {
    constructor(private readonly activationPayment: UserMembershipActivationPaymentService) {}

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: UserMembershipActivationPaymentsModel })
    @UseInterceptors(new AppResponseInterceptor())
    async create(@Body() body: CreateActivationPaymentDto) {
        return await this.activationPayment.create(body);
    }
    @Get('list/:perPage/:page/:paymentAmountStart?/:paymentAmountEnd?/:paymentScheduleStart?/:paymentScheduleEnd?/:status?')
    @ApiImplicitHeader({ name: 'language', required: true})
    @ApiResponse({ status: 200, type: UserMembershipActivationPaymentsModel ,isArray: true})
    @UseInterceptors(new AppResponseInterceptor())
    async list(@Param() params) {
        // tslint:disable-next-line: max-line-length
        return await this.activationPayment.list(params.perPage,params.page, params.paymentAmountStart, params.paymentAmountEnd, params.paymentScheduleStart, params.paymentScheduleEnd, params.status);
    }
    @Get('detail/:id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserMembershipActivationPaymentsModel})
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    @UseInterceptors(new AppResponseInterceptor())
    async detail(@Param('id') id: string)
    {
        return await this.activationPayment.detail(id);
    }
    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserMembershipActivationPaymentsModel})
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    async update(
        @Body()
        body: UpdateActivationPaymentDto,
        @Param('id') id,
    ) {
        return this.update(body, id);
    }

    @Delete(':id')
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    @UseInterceptors(new AppResponseInterceptor())
    delete(@Param('id') id: string){
        this.activationPayment.delete(id);
    }
}
