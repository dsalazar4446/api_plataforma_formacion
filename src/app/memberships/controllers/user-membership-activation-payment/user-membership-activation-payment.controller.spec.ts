import { Test, TestingModule } from '@nestjs/testing';
import { UserMembershipActivationPaymentController } from './user-membership-activation-payment.controller';

describe('UserMembershipActivationPayment Controller', () => {
  let controller: UserMembershipActivationPaymentController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserMembershipActivationPaymentController],
    }).compile();

    controller = module.get<UserMembershipActivationPaymentController>(UserMembershipActivationPaymentController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
