import { Controller, Post, UsePipes, UseInterceptors, Body, Get, Param, Put, Delete, UseFilters, UseGuards } from '@nestjs/common';
import { MembershipComissionsService } from '../../services/membership-comissions/membership-comissions.service';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { CreateMembershipComissionsDto } from '../../dto/create-membership-comissions.dto';
import { UpdateMembershipComissionsDto } from '../../dto/update-membership-comissions.dto';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { MembershipsComissionsModel } from '../../models/membership-comissions.model';

@ApiUseTags('Memberships')
@UseFilters(new AppExceptionFilter())
// // @UseGuards(new JwtAuthGuard())
@Controller('memberships/memberships-comissions')
export class MembershipsComissionsController {
    constructor(private readonly comissions: MembershipComissionsService) {}

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: MembershipsComissionsModel, isArray: true })
    @UseInterceptors(new AppResponseInterceptor())
    async create(@Body() body: CreateMembershipComissionsDto) {
        return await this.comissions.create(body);
    }
    @Get('list')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: MembershipsComissionsModel, isArray: true })
    @UseInterceptors(new AppResponseInterceptor())
    async list() {
        return await this.comissions.list();
    }
    @Get('detail/:id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: MembershipsComissionsModel, isArray: true })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(new AppResponseInterceptor())
    async detail(@Param('id') id: string)
    {
        return await this.comissions.detail(id);
    }
    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: MembershipsComissionsModel, isArray: true })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    async update(
        @Body()
        body: UpdateMembershipComissionsDto,
        @Param('id') id,
    ) {
        return this.update(body, id);
    }

    @Delete(':id')
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    @UseInterceptors(new AppResponseInterceptor())
    delete(@Param('id') id: string){
        this.comissions.delete(id);
    }
}
