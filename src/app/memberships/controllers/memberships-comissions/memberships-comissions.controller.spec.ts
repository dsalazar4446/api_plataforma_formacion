import { Test, TestingModule } from '@nestjs/testing';
import { MembershipsComissionsController } from './memberships-comissions.controller';

describe('MembershipsComissions Controller', () => {
  let controller: MembershipsComissionsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MembershipsComissionsController],
    }).compile();

    controller = module.get<MembershipsComissionsController>(MembershipsComissionsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
