import { Controller, Post, Get, Put, Delete, Body, Param, UseInterceptors, UseFilters, UseGuards, Inject, HttpStatus, Req } from '@nestjs/common';
import { MembershipDetailService } from '../../services/membership-detail/membership-detail.service';
import { CreateDetailDto } from '../../dto/create-detail.dto';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { MembershipDetailsModel } from '../../models/membership-detail.model';
import { AppUtilsService } from '../../../shared/services/app-utils.service';
import { MembershipModel } from '../../models/membership.model';
import { UpdateMembershipDetailDto } from '../../dto/update-membership-detail.dto';

@ApiUseTags('Memberships')
// // @UseGuards(new JwtAuthGuard())
@Controller('membreships/membership-detail')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(new AppResponseInterceptor())
export class MembershipDetailController {
    constructor(
      private readonly detailService: MembershipDetailService,
      @Inject('MembershipModel') private readonly membershipModel: typeof MembershipModel,
      @Inject('MembershipDetailsModel') private readonly membershipDetailsModel: typeof MembershipDetailsModel,
      private readonly appUtilsService: AppUtilsService,
    ) {}
    @ApiResponse({
        status: 200,
        type: MembershipDetailsModel,
      })
    @Post()
    @ApiImplicitHeader({
      name:'language',
      required: true,
  })
    async create(@Body() body: CreateDetailDto, @Req() req) {
      const data3 = await this.membershipModel.findByPk(body.membershipId);
      if(data3 == null){
          throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
              messageCode: 'EA12',
              languageType:  req.headers.language,
          });
      }
      return this.detailService.create(body);
    }
    @Get('list')
    @ApiImplicitHeader({
      name:'language',
      required: true,
  })
    @ApiResponse({
        status: 200,
        type: MembershipDetailsModel,
        isArray:true
      })
    list() {
        return this.detailService.list();
    }
    @Get('detail/:id')
    @ApiImplicitHeader({
      name:'language',
      required: true,
  })
    @ApiResponse({
        status: 200,
        type: MembershipDetailsModel,
      })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    
    detail(@Param() param) {
        return this.detailService.detail(param);
    }
    @Put()
    @ApiImplicitHeader({
      name:'language',
      required: true,
  })
    @ApiResponse({
        status: 200,
        type: MembershipDetailsModel,
      })
    async update(
      @Body() body : Partial<UpdateMembershipDetailDto>, 
      @Req() req,
      @Param() idMembershipDetail
    ) {
     
      if(body.membershipId){
        const data3 = await this.membershipModel.findByPk(body.membershipId);
        if(data3 == null){
          throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
              messageCode: 'EA12',
              languageType:  req.headers.language,
          });
        }
      }
      const data2 = await this.membershipDetailsModel.findByPk(idMembershipDetail);
      if(data2 == null){
          throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
              messageCode: 'EA13',
              languageType:  req.headers.language,
          });
      }
      return await this.detailService.update(idMembershipDetail, body);
    }
    @Delete(':id')
    @ApiImplicitHeader({
      name:'language',
      required: true,
  })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    
    delete(@Param() param) {
        this.detailService.delete(param.id);
    }
}
