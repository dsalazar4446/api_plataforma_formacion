import { Test, TestingModule } from '@nestjs/testing';
import { MembershipDetailController } from './membership-detail.controller';

describe('MembershipDetail Controller', () => {
  let controller: MembershipDetailController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MembershipDetailController],
    }).compile();

    controller = module.get<MembershipDetailController>(MembershipDetailController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
