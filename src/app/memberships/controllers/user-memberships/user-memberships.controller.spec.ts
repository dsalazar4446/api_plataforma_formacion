import { Test, TestingModule } from '@nestjs/testing';
import { UserMembershipsController } from './user-memberships.controller';

describe('UserMemberships Controller', () => {
  let controller: UserMembershipsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserMembershipsController],
    }).compile();

    controller = module.get<UserMembershipsController>(UserMembershipsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
