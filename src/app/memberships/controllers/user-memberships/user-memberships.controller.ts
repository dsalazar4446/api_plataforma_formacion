import { Controller, Get, Put, Delete, Post, UseInterceptors, UsePipes, Body, Param, UseFilters, UseGuards, HttpCode, HttpStatus } from '@nestjs/common';
import { UserMembershipsService } from '../../services/user-memberships/user-memberships.service';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { CreateUserMembershipDto } from '../../dto/create-user-membership.dto';
import { UpdateUserMembershipDto } from '../../dto/update-user-membership.dto';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { UserMembershipsModel } from '../../models/user-memberships.model';

@ApiUseTags('Memberships')
@Controller('memberships/user-memberships')
@UseFilters(new AppExceptionFilter())
// // @UseGuards(new JwtAuthGuard())
@UseInterceptors(new AppResponseInterceptor())
export class UserMembershipsController {
    constructor(private readonly userMembership: UserMembershipsService) {}

    @Post()
    @HttpCode(HttpStatus.CREATED)
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserMembershipsModel})
    async create(@Body() body: CreateUserMembershipDto) {
        return await this.userMembership.create(body);
    }
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserMembershipsModel, isArray: true })
    @ApiImplicitParam({ name: 'membershipStartDateStart', required: false, type: 'string' })
    @ApiImplicitParam({ name: 'membershipStartDateEnd', required: false, type: 'string' })
    @ApiImplicitParam({ name: 'membershipEndDateStart', required: false, type: 'string' })
    @ApiImplicitParam({ name: 'membershipEndDateEnd', required: false, type: 'string' })
    @ApiImplicitParam({ name: 'type', required: false, type: 'string' })
    @HttpCode(HttpStatus.OK)
    @Get('list/:membershipStartDateStart?/:membershipStartDateEnd?/:membershipEndDateStart?/:membershipEndDateEnd?/:type?')
    async list(@Param() params) {
        // tslint:disable-next-line: max-line-length
        console.log(params);
        
        return await this.userMembership.list(params.membershipStartDateStart,params.membershipStartDateEnd,params.membershipEndDateStart,params.membershipEndDateEnd,params.type);
    }

    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserMembershipsModel })
    @HttpCode(HttpStatus.OK)
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    async detail(@Param('id') id: string){
        return await this.userMembership.detail(id);
    }

    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserMembershipsModel, isArray: true })
    @ApiImplicitParam({name:'id', required: true})
    @HttpCode(HttpStatus.CREATED)
    async update(@Body() body: UpdateUserMembershipDto, @Param('id') id: string) {
        // console.log(id)
        return await this.userMembership.updateSuccess(id, body);
    }


    @Delete(':id')
    @HttpCode(HttpStatus.OK)
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    delete(@Param('id') id: string) {
        this.userMembership.delete(id);
    }
}
