import { Test, TestingModule } from '@nestjs/testing';
import { CreateUserMembershipsHasComissionsController } from './create-user-memberships-has-comissions.controller';

describe('CreateUserMembershipsHasComissions Controller', () => {
  let controller: CreateUserMembershipsHasComissionsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CreateUserMembershipsHasComissionsController],
    }).compile();

    controller = module.get<CreateUserMembershipsHasComissionsController>(CreateUserMembershipsHasComissionsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
