import { Controller, Post, Delete, Param } from '@nestjs/common';
import { CreateUserMembershipsHasComissionsService } from '../../services/create-user-memberships-has-comissions/create-user-memberships-has-comissions.service';
import { CreateUserMembershipsComissionsDto } from '../../dto/create-user-memberships-has-comissions.dto';

@Controller('create-user-memberships-has-comissions')
export class CreateUserMembershipsHasComissionsController {
    constructor(private readonly userMembershipsComissionsService: CreateUserMembershipsHasComissionsService){}

    @Post()
    create(body: CreateUserMembershipsComissionsDto){
        return this.userMembershipsComissionsService.create(body);
    }

    @Delete('id')
    delete(@Param('id') id: string){
        this.userMembershipsComissionsService.delete(id);
    }
}
