    // tslint:disable-next-line: max-line-length
import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, FileFieldsInterceptor, UseInterceptors, UseFilters, UploadedFiles, UseGuards, HttpStatus, Req, Inject } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { extname } from 'path';
import { MembershipsService } from '../../services/membership/membership.service';
import { CreateMembershipDto } from '../../dto/create-membeship.dto';
import { UpdateMembershipDto } from '../../dto/update-membership.dto';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiUseTags, ApiConsumes, ApiImplicitFile, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { localOptions, arrayFiles } from '../../multer.options';
import { MembershipModel } from '../../models/membership.model';
import { AppUtilsService } from 'src/app/shared/services/app-utils.service';
@ApiUseTags('Memberships')
@Controller('memberships')
@UseFilters(new AppExceptionFilter()) 
@UseInterceptors(AppResponseInterceptor)

export class MembershipController {
    constructor(
        private readonly membershipsService: MembershipsService,
        @Inject('MembershipModel') private readonly membershipModel: typeof MembershipModel,
        private readonly appUtilsService: AppUtilsService,
    ) { }
    @Post()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: MembershipModel,
      })
    @UseInterceptors(FileFieldsInterceptor(arrayFiles,localOptions))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'membershipImage', required: true, description: 'Imagen de membresia' })
    @ApiImplicitFile({ name: 'membershipIcon', required: true, description: 'Icono de membresia' })
    // // @UseGuards(new JwtAuthGuard())
    async create(@Body() membership: CreateMembershipDto) {
        return await this.membershipsService.create(membership);
    }
    @Get()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: MembershipModel,
        isArray: true
      })
    // // @UseGuards(new JwtAuthGuard())
    async getAll() {
        return await this.membershipsService.findAll();
    }
    @Get(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: MembershipModel,
      })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    async findById(@Param('id') idMembership) {
        return await this.membershipsService.findById(idMembership);
    }
    @Put(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: MembershipModel,
      })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    // // @UseGuards(new JwtAuthGuard())
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'membershipImage', required: true, description: 'Imagen de membresia' })
    @ApiImplicitFile({ name: 'membershipIcon', required: true, description: 'Icono de membresia' })
    async update(
        @Body()
        updateMembership: Partial<UpdateMembershipDto>,
        @Param('id') idMembership,
        @Req() req
    ) {
        const data3 = await this.membershipModel.findByPk(idMembership);
        if(data3 == null){
            throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA12',
                languageType:  req.headers.language,
            });
        }
        return await this.membershipsService.update(idMembership, updateMembership);
    }
    @Delete(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    // // @UseGuards(new JwtAuthGuard())
    async delete(@Param('id') idMembership) {
        return await this.membershipsService.deleted(idMembership);
    }
}
