import { Controller, Post, Body, Delete, Param, UsePipes } from '@nestjs/common';
import { UserMembershipsHasPricingsService } from '../../services/user-memberships-has-pricings/user-memberships-has-pricings.service';
import { CreateUserMembershipsPricingsDto } from '../../../memberships/dto/user-memberships-has-pricings.dto';
import { ApiUseTags, ApiImplicitParam } from '@nestjs/swagger';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
@ApiUseTags('Memberships')
@Controller('user-memberships-has-pricings')
export class UserMembershipsHasPricingsController {
    constructor(private readonly userMembershipPricing: UserMembershipsHasPricingsService){}

    @Post()
    create(@Body() body: CreateUserMembershipsPricingsDto){
        return this.userMembershipPricing.create(body);
    }
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    delete(@Param('id') id: string){
        return this.userMembershipPricing.delete(id);
    }
}
