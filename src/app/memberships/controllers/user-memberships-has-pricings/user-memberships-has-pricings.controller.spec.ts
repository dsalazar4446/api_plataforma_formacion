import { Test, TestingModule } from '@nestjs/testing';
import { UserMembershipsHasPricingsController } from './user-memberships-has-pricings.controller';

describe('UserMembershipsHasPricings Controller', () => {
  let controller: UserMembershipsHasPricingsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserMembershipsHasPricingsController],
    }).compile();

    controller = module.get<UserMembershipsHasPricingsController>(UserMembershipsHasPricingsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
