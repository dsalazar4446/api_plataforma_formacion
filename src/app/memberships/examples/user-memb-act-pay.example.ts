export const UserMembActPayExample = 
{
    "id": "string",
    "userMembershipsId": "string",
    "paymentAmmount": "number",
    "paymentSchedule": "Date",
    "paymentStatus": "number"
}

export const UserMembActPayArrayExample = 
[
    UserMembActPayExample
]
