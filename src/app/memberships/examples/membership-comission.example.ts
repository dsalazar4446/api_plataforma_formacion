export const MembershipComissionsExample = 
{
    "id": "string",
    "membershipsId": "string",
    "level": "number",
    "comissionAmmount": "number",
    "comissionActivationAmmount": "number",
    "comissionActivationBroker": "number",
    "comissionType": "string",
    "comissionPayable": "boolean",
    "comissionStatus": "boolean"
}


export const MembershipComissionsArrayExample = 
[
    MembershipComissionsExample
]
