export const MembershipPricingExample = 
{
     "id": "string",
     "membershipId": "string",
     "membershipPricingMonths": "number",
     "membershipPricingMonthlyActivationValue": "number",
     "membershipPricingTaxes": "number"   ,
     "membershipPricingStatus": "boolean",
     "membershipOddoId": "number",
     "membershipRenawal": "boolean",
     "membershipUpdate": "boolean",
     "membershipRankings": "boolean"
}


export const MembershipPricingArrayExample = 
[
     MembershipPricingExample
]
