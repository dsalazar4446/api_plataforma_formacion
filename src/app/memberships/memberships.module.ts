import { Module, HttpModule, RequestMethod, forwardRef } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { UserMembershipsService } from './services/user-memberships/user-memberships.service';
import { MembershipComissionsService } from './services/membership-comissions/membership-comissions.service';
import { UserMembershipActivationPaymentService } from './services/user-membership-activation-payment/user-membership-activation-payment.service';
import {MembershipController,} from './controllers/membership/membership.controller';
import {UserMembershipsController} from './controllers/user-memberships/user-memberships.controller';
import {MembershipsComissionsController} from './controllers/memberships-comissions/memberships-comissions.controller';
import {UserMembershipActivationPaymentController} from './controllers/user-membership-activation-payment/user-membership-activation-payment.controller';
import {MembershipsComissionsModel} from './models/membership-comissions.model';
import {MembershipDetailsModel} from './models/membership-detail.model';
import { MembershipPricingsModel} from './models/membership-pricings.model';
import { MembershipModel} from './models/membership.model';
import {UserMembershipActivationPaymentsModel} from './models/user-membership-activation-payments.model';
import {UserMembershipsModel} from './models/user-memberships.model';
import { MembershipsService } from './services/membership/membership.service';
import { MembershipPricingController } from './controllers/membersip-pricing/membersip-pricing.controller';
import { MembershipPricingService } from './services/membership-pricing/membership-pricing.service';
import { UserMembershipsHasPricingsService } from './services/user-memberships-has-pricings/user-memberships-has-pricings.service';
import { UserMembershipsHasPricingsController } from './controllers/user-memberships-has-pricings/user-memberships-has-pricings.controller';
import { UserMembershipPricingModel } from './models/user-memberships-has-pricings.model';
import { CreateUserMembershipsHasComissionsService } from './services/create-user-memberships-has-comissions/create-user-memberships-has-comissions.service';
import { CreateUserMembershipsHasComissionsController } from './controllers/create-user-memberships-has-comissions/create-user-memberships-has-comissions.controller';
import { UserMembershipsComissionsModel } from './models/user-memberships-has-comissions.model';
import { MembershipsPrograms } from './models/memberships-has-programs.model';
import { MembershipDetailController } from './controllers/membership-detail/membership-detail.controller';
import { MembershipDetailService } from './services/membership-detail/membership-detail.service';
import { UserModule } from '../user/user.module';

const models = [
  MembershipsComissionsModel,
  MembershipDetailsModel,
  MembershipPricingsModel,
  MembershipModel,
  MembershipsPrograms,
  UserMembershipActivationPaymentsModel,
  UserMembershipsComissionsModel,
  UserMembershipPricingModel,
  UserMembershipsModel,
];
@Module({
  imports: [
    forwardRef(()=>SharedModule),
    forwardRef(()=>UserModule),
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [
    MembershipController,
    UserMembershipsController,
    MembershipsComissionsController,
    UserMembershipActivationPaymentController,
    MembershipPricingController,
    UserMembershipsHasPricingsController,
    CreateUserMembershipsHasComissionsController,
    MembershipDetailController
  ],
  providers: [
    MembershipsService,
    UserMembershipsService,
    MembershipComissionsService,
    UserMembershipActivationPaymentService,
    MembershipPricingService,
    UserMembershipsHasPricingsService,
    CreateUserMembershipsHasComissionsService,
    MembershipDetailService
  ],
  exports: [
    UserMembershipsService,
    UserMembershipActivationPaymentService,
    MembershipsService
  ],
})
export class MembershipsModule{}
