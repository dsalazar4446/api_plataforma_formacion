import { Injectable, Inject } from '@nestjs/common';
import { DeviceSaveJson, DeviceUpdateJson } from '../interfaces/device.interface';
import { DeviceModel } from '../models/device.model';
import { Sequelize } from 'sequelize-typescript';
const Op = Sequelize.Op;
@Injectable()
export class DeviceService {
    constructor(
        @Inject('DeviceModel') private readonly deviceModel: typeof DeviceModel
    ) { }

// tslint:disable-next-line: max-line-length
    async findAll(uuidDevice: string, ipAddress: string, limit: number, page: number): Promise<any> {
        const tagsFilters = ['os', 'device_status', 'user_id'];
        const counter = await this.deviceModel.findAndCountAll({
            where: {
                uuidDevice,
                ipAddress,
            },

        });
        const pages = Math.ceil(counter.count / limit);
        const offset = limit * (page - 1);
        const device = await this.deviceModel.findAll({
            attributes: tagsFilters,
            where: {
                uuidDevice,
                ipAddress,
            },
            limit,
            offset,
            order: [
                ['uuid_device', 'ASC'],
                ['ip_address', 'ASC'],
                ['os', 'ASC'],
            ],
        });
        const dataResult = {
            result: device,
            count: counter.count,
            pages,
        };
        return dataResult;
    }

    async findById(id: string): Promise<DeviceModel> {
        return await this.deviceModel.findById<DeviceModel>(id);
    }
    async findByUserId(userId: string){
        return await this.deviceModel.findOne({where:{userId}});
    }

    async create(device: DeviceSaveJson): Promise<DeviceModel> {
        const newDevice = new DeviceModel(device);
        return await newDevice.save() 
        // return await this.deviceModel.create<DeviceModel>(device, {
        //     returning: true,
        // });
    }

    async update(idDevice: string, deviceUpdate: Partial<DeviceUpdateJson>){
        return  await this.deviceModel.update(deviceUpdate, {
            where: {
                id: idDevice,
            },
            returning: true,
        });
    }

    async deleted(idDevice: string): Promise<any> {
        return await this.deviceModel.destroy({
            where: {
                id: idDevice,
            },
        });
    }
}
