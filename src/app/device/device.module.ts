import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { DeviceController } from './controllers/device.controller';
import { DeviceService } from './services/device.service';
import { DeviceModel } from './models/device.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
const models = [
  DeviceModel,
];
@Module({
  imports: [
    forwardRef(() => SharedModule),
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [DeviceController],
  providers: [DeviceService],
  exports: [DeviceService]
})
export class DeviceModule {}
