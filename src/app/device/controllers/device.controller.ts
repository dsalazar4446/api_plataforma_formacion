import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, HttpStatus, Res, Req} from '@nestjs/common';
import { DeviceService } from '../services/device.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { DeviceSaveJson, DeviceUpdateJson } from '../interfaces/device.interface';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { DeviceModel } from '../models/device.model';
import { UserModel } from '../../user/models/user.Model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
@Controller('device') 
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class DeviceController {
    constructor(
        private readonly deviceService: DeviceService,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('DeviceModel') private readonly deviceModel: typeof DeviceModel,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        @ApiResponse({
            status: 200, 
            type: DeviceModel,
          })
        @Post()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async create(@Body() device: DeviceSaveJson, @Req() req) {
            const data3 = await this.userModel.findByPk(device.userId);
            if(data3 == null){
                throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA4',
                    languageType:  req.headers.language,
                });
            }
            return await this.deviceService.create(device);
        }

        @ApiImplicitParam({ name: 'uuidDevice', required: true, type: 'string' })
        @ApiImplicitParam({ name: 'ipAddress', required: true, type: 'string' })
        @ApiImplicitParam({ name: 'limit', required: true, type: 'number' })
        @ApiImplicitParam({ name: 'page', required: true, type: 'number' })
        @ApiResponse({
            status: 200,
            type: DeviceModel,
            isArray: true
          })
        @Get(':uuid_device/:ip_address/:limit/:page')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async getAll(
            @Param('uuid_device') uuidDevice,
            @Param('ip_address') ipAddress,
            @Param('limit') limit,
            @Param('page') page,
        ) {
            return await this.deviceService.findAll(uuidDevice, ipAddress, limit, page);
        }
        // DETAIL SESSION
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @ApiResponse({
            status: 200,
            type: DeviceModel,
          })
        @Get(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async findById(@Param('id') idDevice) {
            return await this.deviceService.findById(idDevice);
        }
        // UPDATE SESSION
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @ApiResponse({
            status: 200,
            type: DeviceModel,
          })
        @Put(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async update(
            @Body()
            updateSessions: Partial<DeviceUpdateJson>, 
            @Param('id') idDevice,
            @Req() req
        ) {
            const data3 = await this.userModel.findByPk(updateSessions.userId);
            if(data3 == null){
                throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA4',
                    languageType:  req.headers.language,
                });
            }
            const data2 = await this.deviceModel.findByPk(idDevice);
            if(data2 == null){
                throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA6',
                    languageType:  req.headers.language,
                });
            }
            return await this.deviceService.update(idDevice, updateSessions);
        }
        // DELETE SESSION
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Delete(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async delete(@Param('id') idDevice) {
            return await this.deviceService.deleted(idDevice);
        }
}
