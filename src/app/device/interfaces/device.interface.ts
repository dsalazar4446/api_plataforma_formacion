import { ApiModelProperty } from '@nestjs/swagger';
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID} from "class-validator";
export class DeviceSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    userId: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    uuidDevice: string;
    @ApiModelProperty()
    @IsInt()
    @IsNotEmpty()
    os: number;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    ipAddress: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    longitude: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    latitude: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    playerId: string;
    @ApiModelProperty()
    @IsBoolean()
    @IsNotEmpty()
    mobileVibration: boolean;
    @ApiModelProperty()
    @IsBoolean()
    @IsNotEmpty()
    deviceStatus: boolean;
    @ApiModelProperty()
    @IsBoolean()
    @IsNotEmpty()
    mobileNotification: boolean;
}
// tslint:disable-next-line: max-classes-per-file
export class DeviceUpdateJson extends DeviceSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}