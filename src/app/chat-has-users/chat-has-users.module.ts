
import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { ChatHasUsersController } from './controllers/chat-has-users.controller';
import { ChatHasUsersService } from './services/chat-has-users.service';
import { ChatHasUsersModel } from './models/chat-has-users.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { ChatModel } from '../chat/models/chat.model';
import { UserModel } from '../user/models/user.Model';

const models = [
  ChatHasUsersModel,
  ChatModel,
  UserModel,
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [ChatHasUsersController],
  providers: [ChatHasUsersService]
})
export class ChatHasUsersModule {}
