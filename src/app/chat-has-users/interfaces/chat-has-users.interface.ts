import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class ChatHasUsersSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    chatId: string;
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    userId: string; 
}

// tslint:disable-next-line: max-classes-per-file
export class ChatHasUsersUpdateJson extends ChatHasUsersSaveJson{
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}