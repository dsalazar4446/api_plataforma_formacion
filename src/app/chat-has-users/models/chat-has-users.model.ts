import { Column, DataType, Table, Model,ForeignKey, BelongsTo, BelongsToMany, CreatedAt, UpdatedAt } from 'sequelize-typescript';
import { ApiModelProperty } from '@nestjs/swagger';
import { UserModel } from '../../user/models/user.Model';
import { ChatModel } from '../../chat/models/chat.model';
@Table({
    tableName: 'chat_has_users',
})
export class ChatHasUsersModel extends Model<ChatHasUsersModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'user_id',
    })
    userId: string; 
    @ApiModelProperty()
    @ForeignKey(() => ChatModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'chat_id',
    })
    chatId: string;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
