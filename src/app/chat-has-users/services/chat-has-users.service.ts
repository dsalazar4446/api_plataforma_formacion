import { Injectable, Inject } from '@nestjs/common';
import { ChatHasUsersSaveJson, ChatHasUsersUpdateJson} from '../interfaces/chat-has-users.interface';
import { ChatHasUsersModel } from '../models/chat-has-users.model';
import { ChatModel } from '../../chat/models/chat.model';
import { UserModel } from '../../user/models/user.Model';
import { completeUrl } from '../../shared/utils/completUrl';

@Injectable()
export class ChatHasUsersService {
    constructor(
// tslint:disable-next-line: max-line-length
        @Inject('ChatHasUsersModel') private readonly chatHasUsersModel: typeof ChatHasUsersModel,
        @Inject('ChatModel') private readonly chatModel: typeof ChatModel,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        
    ) { }
    async findAll(): Promise<ChatHasUsersModel[]> {
        const data = await this.chatHasUsersModel.findAll();
        const result: any[] = [];
        await Promise.all(
            await data.map(
                async( item: any)=>{
                    const dataChat = await this.chatModel.findByPk(item.dataValues.chatId);
                    const dataUser = await this.userModel.findByPk(item.dataValues.userId);
                    item.dataValues.chatModel = dataChat;
                    item.dataValues.userModel = dataUser;
                    item.dataValues.userModel.dataValues.pwd = undefined;
                    item.dataValues.userModel.dataValues.avatar = completeUrl('user/avatar',item.dataValues.userModel.dataValues.avatar);
                    result.push(item.dataValues);
                }
            )
        )
        return result;
    }
    async findById(id: string): Promise<ChatHasUsersModel[]> {
        const result:any = await this.chatHasUsersModel.findByPk(id);
        const dataChat = await this.chatModel.findByPk(result.dataValues.chatId);
        const dataUser = await this.userModel.findByPk(result.dataValues.userId);
        result.dataValues.chatModel = dataChat;
        result.dataValues.userModel = dataUser;
        result.dataValues.userModel.dataValues.pwd = undefined;
        result.dataValues.userModel.dataValues.avatar = completeUrl('user/avatar',result.dataValues.userModel.dataValues.avatar);
        return result;
    }
    async create(broadcastMessageHasCategories: ChatHasUsersSaveJson): Promise<ChatHasUsersModel> {
// tslint:disable-next-line: max-line-length
        const result:any =  await this.chatHasUsersModel.create<ChatHasUsersModel>(broadcastMessageHasCategories, {
            returning: true,
        });
        const dataChat = await this.chatModel.findByPk(result.dataValues.chatId);
        const dataUser = await this.userModel.findByPk(result.dataValues.userId);
        result.dataValues.chatModel = dataChat;
        result.dataValues.userModel = dataUser;
        result.dataValues.userModel.dataValues.pwd = undefined;
        result.dataValues.userModel.dataValues.avatar = completeUrl('user/avatar',result.dataValues.userModel.dataValues.avatar);
        return result;
    }
    async update(idBroadcast: string, broadcastUpdate: Partial<ChatHasUsersUpdateJson>){
        try {
            const result: any =  await this.chatHasUsersModel.update(broadcastUpdate, {
                where: { 
                    id: idBroadcast,
                },
                returning: true,
            });
            const dataChat = await this.chatModel.findByPk(result.dataValues.chatId);
            const dataUser = await this.userModel.findByPk(result.dataValues.userId);
            result[1][0].dataValues.chatModel = dataChat;
            result[1][0].dataValues.userModel = dataUser;
            result[1][0].dataValues.userModel.dataValues.pwd = undefined;
            result[1][0].dataValues.userModel.dataValues.avatar = completeUrl('user/avatar',result.dataValues.userModel.dataValues.avatar);
            return result;
        } catch (error) {
            return error.data;
        }
    }
    async delete(id: string) {
        return await this.chatHasUsersModel.destroy({
            where: {
                id,
            },
        });
    }
}
