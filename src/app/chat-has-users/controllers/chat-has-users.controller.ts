import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, Req} from '@nestjs/common';
import { ChatHasUsersService } from '../services/chat-has-users.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { ChatHasUsersSaveJson, ChatHasUsersUpdateJson } from '../interfaces/chat-has-users.interface';
import { ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { JwtAuthGuard } from '../../auths/guards/jwt-auth.guard';
import { ChatHasUsersModel } from '../models/chat-has-users.model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { ChatModel } from '../../chat/models/chat.model';
import { UserModel } from '../../user/models/user.Model';

@Controller('chat-has-users')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// @UseGuards(new JwtAuthGuard())
export class ChatHasUsersController {
    constructor(
        private readonly chatHasUsersService: ChatHasUsersService,
        @Inject('ChatModel') private readonly chatModel: typeof ChatModel,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        @ApiResponse({
            status: 200,
            type: ChatHasUsersModel,
          })
        @Post()
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        async create(@Body() ChatHasUsers: ChatHasUsersSaveJson, @Req() req) {
          const data2= await this.userModel.findByPk(ChatHasUsers.userId);
          if(data2 == null){
            throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA4',
                languageType:  req.headers.language,
            });
          } 
          const data3= await this.chatModel.findByPk(ChatHasUsers.chatId);
          if(data3 == null){
                throw this.appUtilsService.httpCommonError('Chat does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA43',
                    languageType:  req.headers.language,
                });
          }
          return await this.chatHasUsersService.create(ChatHasUsers);
        }
        @ApiResponse({
            status: 200,
            type: ChatHasUsersModel,
            isArray: true
          })
        @Get()
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
      async getAll() {
        return await this.chatHasUsersService.findAll();
      }

      @ApiResponse({
        status: 200,
        type: ChatHasUsersModel,
      })
      @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
      @Get(':id')
      @ApiImplicitHeader({
          name:'language',
          required: true,
      })
      async findById(@Param('id') id) {
          return await this.chatHasUsersService.findById(id);
      }



      // UPDATE SESSION
      @ApiResponse({
        status: 200,
        type: ChatHasUsersModel,
      })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Put(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async update(
        @Body()
        updateChatHasUsers: Partial<ChatHasUsersUpdateJson>,
        @Param('id') id,
        @Req() req,
    ) {
      return await this.chatHasUsersService.update(id, updateChatHasUsers);
    }



    // DELETE SESSION
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Delete(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async delete(@Param('id') id) {
        return await this.chatHasUsersService.delete(id);
    }
}
