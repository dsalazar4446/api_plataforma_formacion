import { Controller, UseInterceptors, UseFilters, UseGuards, Post, UsePipes, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';
import { UserCoursesService } from '../../services/user-courses/user-courses.service';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { CreateUserCoursesDto, UpdateUserCoursesDto } from '../../dto/user-courses.dto';
import { UserCoursesModel } from '../../models/user-courses.model';

@ApiUseTags('Courses')
@Controller('user-courses')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // @UseGuards(new JwtAuthGuard())
export class UserCoursesController {
    constructor(private readonly userCoursesService: UserCoursesService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: UserCoursesModel })
    create(@Body() body: CreateUserCoursesDto) {
        return this.userCoursesService.create(body);
    }
    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserCoursesModel, isArray:true })
    list() {
        return this.userCoursesService.list();
    }
    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserCoursesModel })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    detail(@Param('id') id: string) {
        return this.userCoursesService.detail(id);
    }
    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserCoursesModel })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    update(
        @Param('id') id: string,
        @Body()
        body: Partial<UpdateUserCoursesDto>,
    ) {
        return this.userCoursesService.update(id, body);
    }
    @Delete(':id')
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    delete(@Param('id') id) {
        return this.userCoursesService.delete(id);
    }
}
