import {
  Controller,
  UseInterceptors,
  UseFilters,
  UseGuards,
  Post,
  UsePipes,
  Body,
  Get,
  Param,
  Put,
  Delete,
} from '@nestjs/common';
 
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { CoursesService } from '../../services/courses/courses.service';
import { CreateCourseDto, UpdateCourseDto } from '../../dto/courses.dto';
import { CoursesModel } from '../../models/courses.model';

@ApiUseTags('Courses')
@Controller('courses')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // @UseGuards(new JwtAuthGuard())
export class CoursesController {
  constructor(private readonly coursesServices: CoursesService) {}

  @Post()
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 201, type: CoursesModel})
  create(@Body() body: CreateCourseDto) {
    return this.coursesServices.create(body);
  }
  @Get()
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: CoursesModel, isArray: true })
  list() {
    return this.coursesServices.list();
  }
  @Get('admin')
  @ApiResponse({ status: 200, type: CoursesModel, isArray: true })
  listAdmin() {
    return this.coursesServices.listAdmin();
  }
  @Get(':id')
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: CoursesModel})
  @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
  detail(@Param('id') id: string) {
    return this.coursesServices.detail(id);
  }
  @Get('admin/:id')
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 200, type: CoursesModel})
  @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
  detailAdmin(
    @Param('id') id: string,
  ) {
    return this.coursesServices.detailAdmin(id);
  }
  @Put(':id')
  @ApiImplicitHeader({ name: 'language', required: true })
  @ApiResponse({ status: 201, type: CoursesModel, isArray: true })
  @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
  update(
    @Param('id') id: string,
    @Body()
    body: Partial<UpdateCourseDto>,
  ) {
    return this.coursesServices.update(id, body);
  }
  @Delete(':id')
  @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
  delete(@Param('id') id) {
    return this.coursesServices.delete(id);
  }
}
