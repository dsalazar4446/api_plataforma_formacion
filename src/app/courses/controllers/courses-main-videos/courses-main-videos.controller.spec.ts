import { Test, TestingModule } from '@nestjs/testing';
import { CoursesMainVideosController } from './courses-main-videos.controller';

describe('CoursesMainVideos Controller', () => {
  let controller: CoursesMainVideosController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CoursesMainVideosController],
    }).compile();

    controller = module.get<CoursesMainVideosController>(
      CoursesMainVideosController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
