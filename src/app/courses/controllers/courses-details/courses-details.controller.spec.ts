import { Test, TestingModule } from '@nestjs/testing';
import { CoursesDetailsController } from './courses-details.controller';

describe('CoursesDetails Controller', () => {
  let controller: CoursesDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CoursesDetailsController],
    }).compile();

    controller = module.get<CoursesDetailsController>(CoursesDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
