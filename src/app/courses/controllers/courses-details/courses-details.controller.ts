import { Controller, UseInterceptors, UseFilters, UseGuards, Post, UsePipes, Body, Get, Param, Put, Delete, FileFieldsInterceptor, Req } from '@nestjs/common';
import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import * as path from 'path';
import { diskStorage } from 'multer';
import { ApiUseTags, ApiImplicitParam, ApiConsumes, ApiImplicitFile, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { arrayFiles, localOptions } from '../../multer.options';
import { CoursesDetailsService } from '../../services/courses-details/courses-details.service';
import { CreateCoursesDetails, UpdateCoursesDetails } from '../../dto/courses-details.dto';
import { CourseDetailsModel } from '../../models/courses-details.model';

@ApiUseTags('Courses')
@Controller('courses-details')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // @UseGuards(new JwtAuthGuard())
export class CoursesDetailsController {
    constructor(private readonly coursesDetailsService: CoursesDetailsService) { }

    @Post()
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'courseImage', required: true, description: 'Imagen de course' })
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: CourseDetailsModel })
    create(@Body() body: CreateCoursesDetails, @Req() req) {
        body.languageType = req.headers.language;
        return this.coursesDetailsService.create(body);
    }
    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: CourseDetailsModel, isArray: true })
    list() {
        return this.coursesDetailsService.list();
    }
    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: CourseDetailsModel})
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    detail(@Param('id') id: string) {
        return this.coursesDetailsService.detail(id);
    }
    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: CourseDetailsModel, isArray: true })
    @ApiImplicitParam({name:'id', required: true})
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'courseImage', required: true, description: 'Imagen de course' })
    update(
        @Param('id') id: string,
        @Body()
        body: Partial<UpdateCoursesDetails>,
        @Req() req
    ) {
        body.languageType = req.headers.language;
        return this.coursesDetailsService.update(id, body);
    }
    @Delete(':id')
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    delete(@Param('id') id) {
        return this.coursesDetailsService.delete(id);
    }
}
