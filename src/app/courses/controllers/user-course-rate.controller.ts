import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param, Req } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { UserCourseRatesService } from '../services/user-course.rate.service';
import { UserCourseRatesSaveDto } from '../dto/user-course.rate.dto';
import { UserCourseRates } from '../models/user-course.rate.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller UserCourseRatesController
 * @Creado 13 de Mayo 2019
 */

@ApiUseTags('Course')
@Controller('user-course-rates')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class UserCourseRatesController {

    constructor(private readonly _userCourseRatesService: UserCourseRatesService,
        private readonly appUtilsService: AppUtilsService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: UserCourseRates})
    async create(@Req() req, @Body() bodyUserCourseRates: UserCourseRatesSaveDto) {

        if (req.headers.language) {
            bodyUserCourseRates.languageType = req.headers.language;
        } else {
            bodyUserCourseRates.languageType = 'es';
        }

        const createUserCourseRates = await this._userCourseRatesService.saveUserCourseRates(bodyUserCourseRates);

        if (!createUserCourseRates) {
            return this.appUtilsService.httpCommonError('UserCourseRates does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return createUserCourseRates;

    }

    @Get('list')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserCourseRates, isArray: true })
    async showAll() {
        return await this._userCourseRatesService.showAllUserCourseRates();
    }

    @Get('details/:id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserCourseRates })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idUserCourseRates: string) {

        const fetchUserCourseRates = await this._userCourseRatesService.getUserCourseRates(idUserCourseRates);

        if (!fetchUserCourseRates) {
            throw new NotFoundException('UserCourseRates does not exist!');
        }

        return fetchUserCourseRates;
    }

    @Get()
    async showAllUserCourseRates(@Req() req) {

        const fetch = await this._userCourseRatesService.showAll(req.headers.language);
        if (!fetch) {
            throw new NotFoundException('Language does not exist!');
        }

        return fetch;
    }


    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserCourseRates})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateUserCourseRates(@Req() req, @Param('id') id: string, @Body() bodyUserCourseRates: Partial<UserCourseRatesSaveDto>) {

        if (req.headers.language) bodyUserCourseRates.languageType = req.headers.language;

        const updateUserCourseRates = await this._userCourseRatesService.updateUserCourseRates(id, bodyUserCourseRates);

        if (!updateUserCourseRates) {
            throw new NotFoundException('UserCourseRates does not exist!');
        }

        return updateUserCourseRates;
    }

    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteUserCourseRates(@Param('id') idUserCourseRates: string) {

        const deleteUserCourseRates = await this._userCourseRatesService.destroyUserCourseRates(idUserCourseRates);

        if (!deleteUserCourseRates) {
            throw new NotFoundException('UserCourseRates does not exist!');
        }

        return deleteUserCourseRates;
    }
}
