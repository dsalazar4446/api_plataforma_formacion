
import { ApiModelProperty } from "@nestjs/swagger";
import { IsNumber, IsUUID, IsBoolean } from "class-validator";

export class CreateUserCoursesDto {
  @ApiModelProperty()
  @IsUUID('4')
  usersId: string;
  @ApiModelProperty()
  @IsUUID('4')
  courseId: string;
  @ApiModelProperty()
  @IsBoolean()
  courseStatus: boolean;
}

export class UpdateUserCoursesDto extends CreateUserCoursesDto {
  @ApiModelProperty()
  @IsUUID('4')
  id: string;
}
