import { ApiModelProperty } from "@nestjs/swagger";
import { IsNumber, IsUUID, IsBoolean, IsNotEmpty, IsString, IsOptional } from "class-validator";

export class UserCourseRatesSaveDto {

  @ApiModelProperty()
  @IsUUID('4')
  @IsNotEmpty()
  userCourseId: string;

  @ApiModelProperty()
  @IsNumber()
  @IsNotEmpty()
  userCourseRate: number;

  @ApiModelProperty()
  @IsString()
  @IsOptional()
  userCourseComments: string;

  @ApiModelProperty()
  @IsBoolean()
  @IsNotEmpty()
  userCourseStatus: boolean;

  @ApiModelProperty()
  @IsString()
  @IsOptional()
  languageType: string;
}

export class UserCourseRatesUpdateDto extends UserCourseRatesSaveDto {
  @ApiModelProperty()
  @IsUUID('4')
  id: string;
}
