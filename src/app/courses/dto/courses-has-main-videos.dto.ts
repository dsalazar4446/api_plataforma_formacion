import { IsUUID } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateCoursesMainVideosDto {
  @ApiModelProperty()
  @IsUUID('4')
  coursesId: string;
  @ApiModelProperty()
  @IsUUID('4')
  mainVideosId: string;
}
