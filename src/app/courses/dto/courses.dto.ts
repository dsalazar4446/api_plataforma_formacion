import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString, IsNumber } from "class-validator";

export class CreateCourseDto {
  @ApiModelProperty()
  @IsUUID('4')
  userId: string;
  @ApiModelProperty()
  @IsString()
  courseDate: string;
  @ApiModelProperty()
  @IsString()
  hours: string;
  @ApiModelProperty()
  @IsString()
  courseStatus: string;
  @ApiModelProperty()
  @IsString()
  courseType: string;
  @ApiModelProperty()
  @IsNumber()
  premiumCreexPrice: number;
  @ApiModelProperty()
  @IsNumber()
  premiumExternalPrice: number;
  @ApiModelProperty()
  @IsNumber()
  vipPrice: number;
  @ApiModelProperty()
  @IsNumber()
  courseOdooId: number;
}
export class UpdateCourseDto extends CreateCourseDto {
  @ApiModelProperty()
  @IsUUID('4')
  id: string;
}
