import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString, IsOptional } from "class-validator";

export class CreateCoursesDetails {
  @ApiModelProperty()
  @IsUUID('4')
  courseId: string;
  @ApiModelProperty()
  @IsString()
  courseTitle: string;
  @ApiModelProperty()
  @IsString()
  courseDescription: string;
  @ApiModelProperty()
  @IsString()
  @IsOptional()
  courseImage?: string;
  // @ApiModelProperty()
  // @IsString()
  // @IsOptional()
  // courseImagecertificate: string;
  @ApiModelProperty()
  @IsString()
  @IsOptional()
  languageType: string;
}
export class UpdateCoursesDetails extends CreateCoursesDetails {
  @ApiModelProperty()
  @IsUUID('4')
  id: string;
}
