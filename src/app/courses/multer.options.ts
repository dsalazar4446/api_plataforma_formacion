import {  MulterOptions } from "@nestjs/common/interfaces/external/multer-options.interface";
import { diskStorage } from 'multer';
import { extname } from "path";
import { config } from 'bluebird';


export const arrayFiles = [
    { name: 'course_image', maxCount: 1 },
];

export const localOptions: MulterOptions = {
            storage: diskStorage({
                destination: './uploads/courses'
                , filename: (req, file, cb) => {
                    let extValidas = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG'];

                    if (extValidas.indexOf(extname(file.originalname)) < 0) {
                        cb('valid extensions: ' + extValidas.join(', '));
                        return;
                    }
                    const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                    if (file.fieldname === 'courseImage') {
                    req.body.courseImage = `${randomName}${extname(file.originalname)}`;
                    }
                    cb(null, `${randomName}${extname(file.originalname)}`)
                },
            }),
        }
