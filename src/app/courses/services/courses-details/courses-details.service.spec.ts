import { Test, TestingModule } from '@nestjs/testing';
import { CoursesDetailsService } from './courses-details.service';

describe('CoursesDetailsService', () => {
  let service: CoursesDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CoursesDetailsService],
    }).compile();

    service = module.get<CoursesDetailsService>(CoursesDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
