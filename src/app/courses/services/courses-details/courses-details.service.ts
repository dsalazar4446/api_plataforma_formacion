import { Injectable, Inject } from '@nestjs/common';
import { CourseDetailsModel } from '../../models/courses-details.model';
import {
  UpdateCoursesDetails,
  CreateCoursesDetails,
} from '../../dto/courses-details.dto';
import * as path from 'path';
import * as fs from 'fs';
import { CONFIG } from '../../../../config';

@Injectable()
export class CoursesDetailsService {
  constructor(
    @Inject('CourseDetailsModel')
    private readonly courseDetail: typeof CourseDetailsModel,
  ) { }

  async create(body: CreateCoursesDetails) {
    const createCoursesDetails = new CourseDetailsModel(body);
    return await createCoursesDetails.save();
  }
  async list() {
    const result = await this.courseDetail.findAll();
    result.map(element => {
      element.courseImage = CONFIG.storage.server+'courses/'+element.courseImage
    })
    return result
  }
  async detail(id: string) {
    const result = await this.courseDetail.findByPk(id);
    result.courseImage =  CONFIG.storage.server+'courses/'+result.courseImage
    return result;
  }
  async update(id: string, body: Partial<UpdateCoursesDetails>) {

    if (body.courseImage) {
      await this.deleteImg(id);
    }
    const result = await this.courseDetail.update(body, { where: { id }, returning: true });
    result[1][0].courseImage = CONFIG.storage.server+'courses/'+result[1][0].courseImage;
    return result
  }

  async delete(id: string) {
    await this.deleteImg(id);
    return await this.courseDetail.destroy({ where: { id } });
  }

  private async deleteImg(id: string) {
    const img = await this.courseDetail.findByPk(id).then(item => item.courseImage).catch(err => err);
    const pathImagen = path.resolve(__dirname, `../../../../../uploads/courses/${img}`);
    if (fs.existsSync(pathImagen)) {
      fs.unlinkSync(pathImagen);
    }
  }
}
