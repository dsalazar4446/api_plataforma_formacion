import { Injectable, Inject } from '@nestjs/common';
import { CoursesModel } from '../../models/courses.model';
import { CreateCourseDto, UpdateCourseDto } from '../../dto/courses.dto';
import { CourseDetailsModel } from '../../models/courses-details.model';
import { UserCoursesModel } from '../../models/user-courses.model';
import { MainVideosModel } from '../../../main-videos/models/main-videos.model';
import { ProgramsModel } from '../../../programs/models/programs.model';
import { LessonsModel } from '../../../lessons/models/lessons.model';
import { LessonDetailsModel } from '../../../lessons/models/lesson-details.model';
import { UserLessonsModel } from '../../../lessons/models/user-lessons.model';
import { FormationClassesModel } from '../../../classes/models/formation-classes.model';
import { ClassSequencesModel } from '../../../classes/models/class-sequences.model';
import { ClassAttachmentsModel } from '../../../classes/models/class-attachments.model';
import { ClassHomeworksModel } from '../../../classes/models/class-homeworks.model';
import { ClassTextModel } from '../../../classes/models/class-text.model';
import { FormationTest } from '../../../test/models/formation-test.models';
import { UserClassesViewsModel } from '../../../classes/models/user-classes-views.model';
import { CONFIG } from '../../../../config';

const include = [
  CourseDetailsModel,
  UserCoursesModel,
  MainVideosModel,
  ProgramsModel,
  { 
    model: LessonsModel,
    include:[
      LessonDetailsModel,
      UserLessonsModel,
      {
        model: FormationClassesModel,
      }
      
    ]
  }
  
]
@Injectable()
export class CoursesService {
  
  constructor(
    @Inject('CoursesModel') private readonly courses: typeof CoursesModel,
  ) {}


  concatUrl(result: CoursesModel[]){

  }

  async create(body: CreateCourseDto) {
    const createCourse = new CoursesModel(body);
    return await createCourse.save();
  }
  async list() {
    const result = await this.courses.findAll({
      include,
      where: {courseStatus:'2'}
    });
    result.map(element => {
      element.Programs.map(program => {
        program.programIcon = CONFIG.storage.server+'courses/'+program.programIcon
        program.programBackground = CONFIG.storage.server+'courses/'+program.programBackground
        program.fbPromotionImg = CONFIG.storage.server+'courses/'+program.fbPromotionImg
      })
      element.courseDetails.map(subElement =>{
        subElement.courseImage = CONFIG.storage.server+'courses/'+subElement.courseImage 
      })
    })
    return result 
  }

  async listAdmin(){
    const result = await this.courses.findAll({
      include,
    });
    result.map(element => {
      element.Programs.map(program => {
        program.programIcon = CONFIG.storage.server+'courses/'+program.programIcon
        program.programBackground = CONFIG.storage.server+'courses/'+program.programBackground
        program.fbPromotionImg = CONFIG.storage.server+'courses/'+program.fbPromotionImg
      })
      element.courseDetails.map(subElement =>{
        subElement.courseImage = CONFIG.storage.server+'courses/'+subElement.courseImage 
      })
    })
    return result;
  }

  async detail(id: string) {
    const result =  await this.courses.findByPk(id,{
      include,
      where: {courseStatus:'2'}
    });
    result.Programs.map(program => {
      program.programIcon = CONFIG.storage.server+'programs/'+program.programIcon
      program.programBackground = CONFIG.storage.server+'programs/'+program.programBackground
      program.fbPromotionImg = CONFIG.storage.server+'programs/'+program.fbPromotionImg
    })
    result.courseDetails.map(subElement =>{
        subElement.courseImage = CONFIG.storage.server+'courses/'+subElement.courseImage 
    })
    return result
  }
  async detailAdmin(id:string,){
    const result =  await this.courses.findByPk(id,{
      include,
    });
    result.Programs.map(program => {
      program.programIcon = CONFIG.storage.server+'programs/'+program.programIcon
      program.programBackground = CONFIG.storage.server+'programs/'+program.programBackground
      program.fbPromotionImg = CONFIG.storage.server+'programs/'+program.fbPromotionImg
    })
    result.courseDetails.map(subElement =>{
        subElement.courseImage = CONFIG.storage.server+'courses/'+subElement.courseImage 
    })
    return result
  }
  async update(id: string, body: Partial<UpdateCourseDto>) {
    return await this.courses.update(body, {
      where: { id }, returning: true,
    });
  }
  async delete(id: string) {
    return await this.courses.destroy({ where: { id } });
  }
}
