import { Injectable, Inject } from '@nestjs/common';
import { UserCourseRates } from '../models/user-course.rate.model';
import { UserCourseRatesSaveDto, UserCourseRatesUpdateDto } from '../dto/user-course.rate.dto';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service UserCourseRatesService
 * @Creado 13 de Mayo 2019
 */

@Injectable()
export class UserCourseRatesService {

    private language = ['es', 'en', 'it', 'pr'];
    
    constructor(
        @Inject('UserCourseRates') private readonly _userCourseRates: typeof UserCourseRates
    ) { }

    async saveUserCourseRates(bodyUserCourseRates: UserCourseRatesSaveDto): Promise<UserCourseRatesUpdateDto> {
        return await new this._userCourseRates(bodyUserCourseRates).save();
    }

    async showAllUserCourseRates(): Promise<UserCourseRatesUpdateDto[]> {
        return await this._userCourseRates.findAll();
    }

    async showAll(languageType: string): Promise<UserCourseRatesUpdateDto[]> {
        if (this.language.indexOf(languageType) >= 0) {
            return await this._userCourseRates.findAll({
                where: {
                    languageType
                }
            });
        }
        return;
    }

    async getUserCourseRates(userCourseRatesID: string): Promise<UserCourseRatesUpdateDto> {
        return await this._userCourseRates.findByPk(userCourseRatesID);
    }

    async updateUserCourseRates(id: string, bodyUserCourseRates: Partial<UserCourseRatesUpdateDto>): Promise<[number, Array<UserCourseRatesUpdateDto>]> {
        return await this._userCourseRates.update(bodyUserCourseRates, {
            where: { id },
            returning: true
        });
    }

    async destroyUserCourseRates(userCourseRatesID: string): Promise<number> {
        return await this._userCourseRates.destroy({
            where: { id: userCourseRatesID }
        });
    }

}
