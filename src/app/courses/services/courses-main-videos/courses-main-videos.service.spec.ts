import { Test, TestingModule } from '@nestjs/testing';
import { CoursesMainVideosService } from './courses-main-videos.service';

describe('CoursesMainVideosService', () => {
  let service: CoursesMainVideosService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CoursesMainVideosService],
    }).compile();

    service = module.get<CoursesMainVideosService>(CoursesMainVideosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
