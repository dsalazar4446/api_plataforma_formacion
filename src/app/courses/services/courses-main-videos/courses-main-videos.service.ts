import { Injectable, Inject } from '@nestjs/common';
import { CoursesMainVideoModel } from '../../models/courses-has-main-videos.model';
import { CreateCoursesMainVideosDto } from '../../dto/courses-has-main-videos.dto';

@Injectable()
export class CoursesMainVideosService {
  constructor(
    @Inject('CoursesMainVideoModel')
    private readonly coursesMainVideo: typeof CoursesMainVideoModel,
  ) {}

  async create(body: CreateCoursesMainVideosDto) {
    const createCourseMainVideo = new CoursesMainVideoModel(body);
    return await createCourseMainVideo.save();
  }

  async delete(id: string) {
    return await this.coursesMainVideo.destroy({
      where: { coursesId: id },
    });
  }
}
