import { Injectable, Inject } from '@nestjs/common';
import { UserCoursesModel } from '../../models/user-courses.model';
import {
  UpdateUserCoursesDto,
  CreateUserCoursesDto,
} from '../../dto/user-courses.dto';

@Injectable()
export class UserCoursesService {
  constructor(
    @Inject('UserCoursesModel')
    private readonly userCourse: typeof UserCoursesModel,
  ) {}

  async create(body: CreateUserCoursesDto) {
    const createUserCourses = new UserCoursesModel(body);
    return await createUserCourses.save();
  }
  async list() {
    return await this.userCourse.findAll();
  }
  async detail(id: string) {
    return await this.userCourse.findByPk(id);
  }
  async update(id: string, body: Partial<UpdateUserCoursesDto>) {
    return await this.userCourse.update(body, { where: { id }, returning:true });
  }
  async delete(id: string) {
    return await this.userCourse.destroy({ where: { id } });
  }
}
