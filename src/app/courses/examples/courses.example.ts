export const CoursesExample = 
{
    "id": "string",
    "userId": "string",
    "courseDate": "string",
    "hours": "string",
    "courseStatus": "string",
    "courseType": "string",
    "premiumCreexPrice": "number",
    "premiumExternalPrice": "number",
    "vipPrice": "number"

}

export const CoursesArrayExample = 
[
    CoursesExample
]
