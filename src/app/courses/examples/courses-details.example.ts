export const CoursesDetailsExample = 
{
  "id": "string",
  "courseId": "string",
  "courseTitle": "string",
  "courseDescription": "string",
  "courseImage": "string",
  // "courseImagecertificate": "string",
  "languageType": "string"
}

export const CoursesDetailsArrayExample = 
[
    CoursesDetailsExample
]
