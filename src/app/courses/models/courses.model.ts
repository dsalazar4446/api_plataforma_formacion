// tslint:disable-next-line: max-line-length
import {
  Model,
  Table,
  Column,
  DataType,
  ForeignKey, 
  CreatedAt,
  UpdatedAt,
  HasMany,
  BelongsTo,
  BelongsToMany,
} from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { CourseDetailsModel } from './courses-details.model';
import { UserCoursesModel } from './user-courses.model';
import { MainVideosModel } from '../../main-videos/models/main-videos.model';
import { CoursesMainVideoModel } from './courses-has-main-videos.model';
import { SurveysHasCourses } from '../../surveys/models/surveys-has-courses.model';
import { Surveys } from '../../surveys/models/surveys.models';
import { CoursesHasFormationTests } from '../../test/models/courses-has-formation-test.model';
import { FormationTest } from '../../test/models/formation-test.models';
import { SurveysHasCoursesServices } from '../../surveys/services/surveys-has-courses.service';
import { ProgramHasCoursesModel } from 'src/app/programs/models/program-has-courses';
import { ProgramsModel } from '../../programs/models/programs.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { CoursesDetailsArrayExample } from '../examples/courses-details.example';
import { UserCourseArrayExample } from '../examples/user-course.example';
import { MainVideosArrayExample } from '../../main-videos/example/main-videos.example';
import { ProgramsArrayExample } from '../../programs/examples/progrmas.example';
import { FormationTestArrayExample } from '../../test/examples/formation-test.example';
import { SurviesArrayExample } from '../../surveys/examples/survies.example';
import { LessonsModel } from '../../lessons/models/lessons.model';
import { LessonsDetailsArrayExample } from '../../lessons/examples/lessons-details.example';
@Table({ tableName: 'courses' })
export class CoursesModel extends Model<CoursesModel> {
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    autoIncrement: true,
    unique: true,
    primaryKey: true,
    validate: {
      notEmpty: true,
    },
  })
  id: string;
  @ApiModelPropertyOptional()
  @ForeignKey(() => UserModel)
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    unique: true,
    validate: {
      notEmpty: true,
    },
    field: 'user_id',
  })
  userId: string;

  @BelongsTo(() => UserModel)
  users: UserModel;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'course_date',
  })
  courseDate: string;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.DOUBLE,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'hours',
  })
  hours: string;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.ENUM('1', '2', '3'),
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'course_status',
  })
  courseStatus: string;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.ENUM('membership', 'premium', 'vip'),
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'course_type',
  })
  courseType: string;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.DOUBLE,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'premium_creex_price',
  })
  premiumCreexPrice: number;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.DOUBLE,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'premium_external_price',
  })
  premiumExternalPrice: number;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.DOUBLE,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'vip_price',
  })
  vipPrice: number;

  @ApiModelPropertyOptional()
  @Column({
    type: DataType.DOUBLE,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'course_odoo_id',
  })
  courseOdooId: number;
  @ApiModelPropertyOptional({ example: SurviesArrayExample })
  @BelongsToMany(() => Surveys, {
    through: {
      model: () => SurveysHasCourses,
      unique: true,
    }
  })
  surveys: Surveys[];
  @ApiModelPropertyOptional({example: FormationTestArrayExample})
  @BelongsToMany(() => FormationTest, {    through: {
      model: () => CoursesHasFormationTests,
      unique: true,
    }
  })
  formationTest: FormationTest[];
  @ApiModelPropertyOptional({ example: CoursesDetailsArrayExample})
  @HasMany(() => CourseDetailsModel)
  courseDetails: CourseDetailsModel[];
  @ApiModelPropertyOptional({example: UserCourseArrayExample})
  @HasMany(() => UserCoursesModel)
  userCourses: UserCoursesModel[];
  @ApiModelPropertyOptional({ example: MainVideosArrayExample})
  @BelongsToMany(() => MainVideosModel, () => CoursesMainVideoModel)
  mainVideos: MainVideosModel[];
  @ApiModelPropertyOptional({example: ProgramsArrayExample})
  @BelongsToMany(() => ProgramsModel, () => ProgramHasCoursesModel)
  Programs: ProgramsModel[];
  @ApiModelPropertyOptional({ example: LessonsDetailsArrayExample })
  @HasMany(() => LessonsModel)
  lessons: LessonsModel[];

  // @HasMany(() => UserCourseRates)
  // userCourseRates: UserCourseRates[];
  // @ApiModelPropertyOptional({type:Date})
  // tslint:disable-next-line: variable-name
  
  @CreatedAt created_at: Date;
  // @ApiModelPropertyOptional({type:Date})
  // tslint:disable-next-line: variable-name
  @UpdatedAt updated_at: Date;
}
