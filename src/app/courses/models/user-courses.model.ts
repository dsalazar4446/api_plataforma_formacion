// tslint:disable-next-line: max-line-length
import {
  Model,
  Table,
  Column,
  PrimaryKey,
  AutoIncrement,
  AllowNull,
  Validate,
  Unique,
  DataType,
  ForeignKey,
  CreatedAt,
  UpdatedAt,
  HasMany,
  BelongsTo,
  BelongsToMany,
} from 'sequelize-typescript';
import { CoursesModel } from './courses.model';
import { UserModel } from './../../user/models/user.Model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { UserCourseRates } from './user-course.rate.model';

@Table({ tableName: 'user_courses' })
export class UserCoursesModel extends Model<UserCoursesModel> {
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    autoIncrement: true,
    unique: true,
    primaryKey: true,
    validate: {
      notEmpty: true,
    },
  })
  id: string;
  @ApiModelPropertyOptional()
  @ForeignKey(() => UserModel)
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    unique: true,
    validate: {
      notEmpty: true,
    },
    field: 'user_id',
  })
  usersId: string;

  @BelongsTo(() => UserModel)
  users: UserModel[];
  @ApiModelPropertyOptional()
  @ForeignKey(() => CoursesModel)
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    unique: true,
    validate: {
      notEmpty: true,
    },
    field: 'course_id',
  })
  courseId: string;

  @BelongsTo(() => CoursesModel)
  courses: CoursesModel[];
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'course_status',
  })
  courseStatus: boolean;

  @HasMany(() => UserCourseRates)
  userCourseRates: UserCourseRates[];

  // tslint:disable-next-line: variable-name
  @CreatedAt created_at: Date;
  // tslint:disable-next-line: variable-name
  @UpdatedAt updated_at: Date;
}
