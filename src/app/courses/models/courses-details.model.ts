// tslint:disable-next-line: max-line-length
import {
  Model,
  Table,
  Column,
  PrimaryKey,
  AutoIncrement,
  AllowNull,
  Validate,
  Unique,
  DataType,
  ForeignKey,
  CreatedAt,
  UpdatedAt,
  HasMany,
  BelongsTo,
  BelongsToMany,
} from 'sequelize-typescript';
import { CoursesModel } from './courses.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({ tableName: 'course_details' })
export class CourseDetailsModel extends Model<CourseDetailsModel> {
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    autoIncrement: true,
    unique: true,
    primaryKey: true,
    validate: {
      notEmpty: true,
    },
  })
  id: string;
  @ApiModelPropertyOptional()
  @ForeignKey(() => CoursesModel)
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    unique: true,
    validate: {
      notEmpty: true,
    },
    field: 'course_id',
  })
  courseId: string;

  @BelongsTo(() => CoursesModel)
  courses: CoursesModel[];
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'course_title',
  })
  courseTitle: string;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'course_description',
  })
  courseDescription: string;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.STRING,
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'course_image',
  })
  courseImage: string;
  // @ApiModelPropertyOptional()
  // @Column({
  //   type: DataType.STRING,
  //   allowNull: false,
  //   validate: {
  //     notEmpty: true,
  //   },
  //   field: 'course_image_certificate',
  // })
  // courseImagecertificate: string;
  @ApiModelPropertyOptional()
  @Column({
    type: DataType.ENUM('es', 'en', 'it', 'pr'),
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'language_type',
  })
  languageType: string;

  // tslint:disable-next-line: variable-name
  @CreatedAt created_at: Date;
  // tslint:disable-next-line: variable-name
  @UpdatedAt updated_at: Date;
}
