import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { UserCoursesModel } from './user-courses.model';

@Table({
    tableName: 'user_course_rates',
})
export class UserCourseRates extends Model<UserCourseRates>{
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserCoursesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_course_id',
    })
    userCourseId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.NUMERIC,
        defaultValue: 0.0,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_course_rate',
    })
    userCourseRate: number;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: true,
        field: 'user_course_comments',
    })
    userCourseComments: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        defaultValue: true,
        allowNull: false,
        validate: {
            notEmpty: false,
        },
        field: 'user_course_status',
    })
    userCourseStatus: boolean;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        allowNull: false,
        validate: {
            notEmpty: false,
        },
        field: 'language_type',
    })
    languageType: string;

    @BelongsTo(() => UserCoursesModel)
    userCoursesModel: UserCoursesModel;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
