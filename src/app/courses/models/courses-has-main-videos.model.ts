// tslint:disable-next-line: max-line-length
import {
  Model,
  Table,
  Column,
  PrimaryKey,
  AutoIncrement,
  AllowNull,
  Validate,
  Unique,
  DataType,
  ForeignKey,
  CreatedAt,
  UpdatedAt,
  HasMany,
  BelongsTo,
  BelongsToMany,
} from 'sequelize-typescript';
import { MainVideosModel } from './../../main-videos/models/main-videos.model';
import { CoursesModel } from './courses.model';
@Table({ tableName: 'courses_has_main_videos' })
export class CoursesMainVideoModel extends Model<CoursesMainVideoModel> {
  @ForeignKey(() => CoursesModel)
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    unique: true,
    validate: {
      notEmpty: true,
    },
    field: 'courses_id',
  })
  coursesId: string;

  @BelongsTo(() => CoursesModel)
  courses: CoursesModel;

  @ForeignKey(() => MainVideosModel)
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    unique: true,
    validate: {
      notEmpty: true,
    },
    field: 'main_videos_id',
  })
  mainVideosId: string;

  @BelongsTo(() => MainVideosModel)
  mainVideos: MainVideosModel;

  // tslint:disable-next-line: variable-name
  @CreatedAt created_at: Date;
  // tslint:disable-next-line: variable-name
  @UpdatedAt updated_at: Date;
}
