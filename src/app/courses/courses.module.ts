import { SharedModule } from '../shared/shared.module';
import { Module } from '@nestjs/common';
import { CoursesService } from './services/courses/courses.service';
import { CoursesDetailsService } from './services/courses-details/courses-details.service';
import { UserCoursesService } from './services/user-courses/user-courses.service';
import { CoursesMainVideosService } from './services/courses-main-videos/courses-main-videos.service';
import { DatabaseModule } from '../database/database.module';
import { CoursesMainVideoModel } from './models/courses-has-main-videos.model';
import { CoursesModel } from './models/courses.model';
import { CourseDetailsModel } from './models/courses-details.model';
import { UserCoursesModel } from './models/user-courses.model';
import { CoursesController } from './controllers/courses/courses.controller';
import { CoursesDetailsController } from './controllers/courses-details/courses-details.controller';
import { CoursesMainVideosController } from './controllers/courses-main-videos/courses-main-videos.controller';
import { UserCoursesController } from './controllers/user-courses/user-courses.controller';
import { UserCourseRatesController } from './controllers/user-course-rate.controller';
import { UserCourseRates } from './models/user-course.rate.model';
import { UserCourseRatesService } from './services/user-course.rate.service';

const models = [
  CoursesModel,
  CourseDetailsModel,
  UserCoursesModel,
  CoursesMainVideoModel,
  UserCourseRates
];
@Module({
  imports: [
    SharedModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: false,
    }),
  ],
  providers: [
    CoursesService,
    CoursesDetailsService,
    UserCoursesService,
    CoursesMainVideosService,
    UserCourseRatesService
  ],
  controllers: [
    CoursesController,
    CoursesDetailsController,
    CoursesMainVideosController,
    UserCoursesController,
    UserCourseRatesController
  ],
  exports: [
    CoursesService,
  ]
})
export class CoursesModule {}
