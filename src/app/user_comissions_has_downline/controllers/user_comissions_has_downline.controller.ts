import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, UsePipes, UseInterceptors, UseFilters, UseGuards} from '@nestjs/common';
import { UserComissionsHasDownlineService } from '../services/user_comissions_has_downline.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { UserComissionsHasDownlineSaveJson } from '../interfaces/user_comissions_has_downline.interface';
import { JwtAuthGuard } from '../../auths/guards/jwt-auth.guard';
import { UserComissionsHasDownlineModel } from '../models/user_comissions_has_downline.model';
import { ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
@Controller('user-comissions-has-downlines')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// @UseGuards(new JwtAuthGuard())
export class UserComissionsHasDownlineController {
    constructor(
        private readonly userComissionsHasDownlineService: UserComissionsHasDownlineService,
        ) {}
        @ApiResponse({
            status: 200,
            type: UserComissionsHasDownlineModel,
          })
          @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @Post()
        async create(@Body() userComissionsHasDownline: UserComissionsHasDownlineSaveJson) {
            return await this.userComissionsHasDownlineService.create(userComissionsHasDownline);
        }
        @ApiResponse({
            status: 200,
            type: UserComissionsHasDownlineModel,
            isArray: true
          })
        @Get()
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        async getAll() {
            return await this.userComissionsHasDownlineService.findAll();
        }
}
