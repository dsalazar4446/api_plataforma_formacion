import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class UserComissionsHasDownlineSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    userComissionsId: string;
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty() 
    downlinesId: string; 
}
