
import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { UserComissionsHasDownlineController } from './controllers/user_comissions_has_downline.controller';
import { UserComissionsHasDownlineService } from './services/user_comissions_has_downline.service';
import {UserComissionsHasDownlineModel } from './models/user_comissions_has_downline.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { Downlines } from '../downlines/models/downlines.model';
import { UserComissionModel } from '../user-comission/models/user-comission.model';

const models = [
  UserComissionsHasDownlineModel,
  UserComissionModel,
  Downlines
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [UserComissionsHasDownlineController],
  providers: [UserComissionsHasDownlineService]
})
export class UserComissionsHasDownlineModule {}
