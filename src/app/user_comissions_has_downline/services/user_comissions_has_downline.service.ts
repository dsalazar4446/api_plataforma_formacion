import { Injectable, Inject } from '@nestjs/common';
import { UserComissionsHasDownlineSaveJson } from '../interfaces/user_comissions_has_downline.interface';
import { UserComissionsHasDownlineModel } from '../models/user_comissions_has_downline.model';

@Injectable()
export class UserComissionsHasDownlineService {
    constructor(
// tslint:disable-next-line: max-line-length
// tslint:disable-next-line: no-shadowed-variable
// tslint:disable-next-line: max-line-length
        @Inject('UserComissionsHasDownlineModel') private readonly userComissionsHasDownlineModel: typeof UserComissionsHasDownlineModel,
    ) { }
    async findAll(): Promise<UserComissionsHasDownlineModel[]> {
        return  await this.userComissionsHasDownlineModel.findAll();
    }
// tslint:disable-next-line: max-line-length
    async create(UserComissionsHasDownline: UserComissionsHasDownlineSaveJson): Promise<UserComissionsHasDownlineModel> {
// tslint:disable-next-line: max-line-length
        return await this.userComissionsHasDownlineModel.create<UserComissionsHasDownlineModel>(UserComissionsHasDownline, {
            returning: true,
        });
    }

}
