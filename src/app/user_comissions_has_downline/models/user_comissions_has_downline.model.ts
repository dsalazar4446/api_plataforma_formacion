import { Column, DataType, Table, Model,ForeignKey, BelongsTo } from 'sequelize-typescript';
import { UserComissionModel } from '../../user-comission/models/user-comission.model';
import { Downlines } from 'src/app/downlines/models/downlines.model';
import { ApiModelProperty } from '@nestjs/swagger';


// FALTA RELACIONAR CON CATEGORY
@Table({
    tableName: 'user_comissions_has_downline',
})
export class UserComissionsHasDownlineModel extends Model<UserComissionsHasDownlineModel> {
    @ApiModelProperty()
    @ForeignKey(() => UserComissionModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'user_comissions_id',
    })
    userComissionsId: string;
    @ApiModelProperty()
    @ForeignKey(() => Downlines)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'downline_id',
    })
    downlinesId: string;
    @ApiModelProperty()
    @BelongsTo(() => UserComissionModel)
    userComission: UserComissionModel;
    @ApiModelProperty()
    @BelongsTo(() => Downlines)
    downline: Downlines;
}
