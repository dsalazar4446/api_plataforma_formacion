import { Module, HttpModule, Provider } from "@nestjs/common";
import { DatabaseModule } from "../database/database.module";
import { SharedModule } from "../shared/shared.module";

import { TradersBrokersService } from './services/trader-broker.service';
import { TraderTransactionsService } from './services/trader-transactions.service';
import { TraderUsersService } from './services/trader-user.service';
import { TraderviewUserRequestService } from './services/traderview-user-request.service';

import { TradersBrokers } from './models/trader-broker.model';
import { TraderTransactions } from './models/trader-transactions.model';
import { TraderUsers } from './models/trader-users.model';
import { TraderviewUserRequest } from './models/traderview-user-request.model';

import { TradersBrokersController } from './controllers/trader-broker.controller';
import { TraderTransactionsController } from './controllers/trader-transactions.controller';
import { TraderUsersController } from './controllers/trader-user.controller';
import { TraderviewUserRequestController } from './controllers/traderview-user-request.controller';

const models = [
    TradersBrokers,
    TraderTransactions,
    TraderUsers,
    TraderviewUserRequest
];
const providers: Provider[] = [
    TradersBrokersService,
    TraderTransactionsService,
    TraderUsersService,
    TraderviewUserRequestService,
];
const controllers = [
    TradersBrokersController,
    TraderTransactionsController,
    TraderUsersController,
    TraderviewUserRequestController
];


@Module({
    imports: [
        SharedModule,
        HttpModule,
        DatabaseModule.forRoot({
            sequelize: {
                models
            },
            loki: true,
        }),
    ],
    providers,
    controllers,
})
export class TraderModule { }
