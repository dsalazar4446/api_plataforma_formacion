export const TraderViewUserRequestExample = {
    "id": "string",
    "traderUserId": "string",
    "traderUserRequest": "string",
    "traderUserRequestStatus": "string",
    "observations": "string",
    "traderAmmount": "number"
}

export const TraderViewUserRequestArrayExample = [
    TraderViewUserRequestExample
]