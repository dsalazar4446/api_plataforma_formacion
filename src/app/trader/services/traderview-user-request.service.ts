import { Injectable, Inject } from '@nestjs/common';
import { TraderviewUserRequest } from '../models/traderview-user-request.model';
import { TraderviewUserRequestSaveJson, TraderviewUserRequestUpdateJson } from '../interfaces/traderview-user-request.interface';
import { TraderUsers } from '../models/trader-users.model';
/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service TraderviewUserRequestService
 * @Creado 30 de Abril 2019
 */

@Injectable()
export class TraderviewUserRequestService {

    private include = {
        include: [TraderUsers]
    };

    constructor(
        @Inject('TraderviewUserRequest') private readonly _traderviewUserRequest: typeof TraderviewUserRequest
    ) { }

    async saveTraderviewUserRequest(bodyTraderviewUserRequest: TraderviewUserRequestSaveJson): Promise<TraderviewUserRequestUpdateJson> {
        return await new this._traderviewUserRequest(bodyTraderviewUserRequest).save();
    }

    async showAllTraderviewUserRequest(): Promise<TraderviewUserRequestUpdateJson[]> {
        return await this._traderviewUserRequest.findAll(this.include);
    }

    async getTraderviewUserRequestDetails(traderviewUserRequestId: string): Promise<TraderviewUserRequestUpdateJson> {
        return await this._traderviewUserRequest.findByPk(traderviewUserRequestId, this.include);
    }

    async updateTraderviewUserRequest(id: string, bodyTraderviewUserRequest: Partial<TraderviewUserRequestSaveJson>): Promise<[number, Array<TraderviewUserRequestUpdateJson>]> {
        return await this._traderviewUserRequest.update(bodyTraderviewUserRequest, {
            where: { id },
            returning: true
        });
    }

    async destroyTraderviewUserRequest(traderviewUserRequestId: string): Promise<number> {
        return await this._traderviewUserRequest.destroy({
            where: { id: traderviewUserRequestId },
        });
    }

}
