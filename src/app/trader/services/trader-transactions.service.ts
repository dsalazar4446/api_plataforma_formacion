import { Injectable, Inject } from '@nestjs/common';
import { UserModel } from '../../user/models/user.Model';
import { TraderTransactions } from '../models/trader-transactions.model';
import { TraderTransactionsSaveJson, TraderTransactionsUpdateJson } from '../interfaces/trader-transactions.interface';
/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service TraderTransactionsService
 * @Creado 30 de Abril 2019
 */

@Injectable()
export class TraderTransactionsService {

    private include = {
        include: [UserModel]
    };

    constructor(
        @Inject('TraderTransactions') private readonly _traderTransactions: typeof TraderTransactions
    ) { }

    async saveTraderTransactions(bodyTraderTransactions: TraderTransactionsSaveJson): Promise<TraderTransactionsUpdateJson> {
        return await new this._traderTransactions(bodyTraderTransactions).save();
    }

    async showAllTraderTransactions(): Promise<TraderTransactionsUpdateJson[]> {
        return await this._traderTransactions.findAll(this.include);
    }

    async getTraderTransactionsDetails(traderTransactionsId: string): Promise<TraderTransactionsUpdateJson> {
        return await this._traderTransactions.findByPk(traderTransactionsId, this.include);
    }

    async updateTraderTransactions(id: string, bodyTraderTransactions: Partial<TraderTransactionsSaveJson>): Promise<[number, Array<TraderTransactionsUpdateJson>]> {
        return await this._traderTransactions.update(bodyTraderTransactions, {
            where: { id },
            returning: true
        });
    }

    async destroyTraderTransactions(traderTransactionsId: string): Promise<number> {
        return await this._traderTransactions.destroy({
            where: { id: traderTransactionsId },
        });
    }

}
