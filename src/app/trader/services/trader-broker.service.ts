import { Injectable, Inject } from '@nestjs/common';
import { UserModel } from '../../user/models/user.Model';
import { TradersBrokersSaveJson, TradersBrokersUpdateJson } from '../interfaces/trader-broker.interdace';
import { TradersBrokers } from '../models/trader-broker.model';
/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service TradersBrokersService
 * @Creado 30 de Abril 2019
 */

@Injectable()
export class TradersBrokersService {

    private include = {
        include: [
            {   
                as: 'trader',
                model: UserModel
            },
            {   
                as: 'broker',
                model: UserModel
            }
        ]
    };

    constructor(
        @Inject('TradersBrokers') private readonly _tradersBrokers: typeof TradersBrokers
    ) { }

    async saveTradersBrokers(bodyTradersBrokers: TradersBrokersSaveJson): Promise<TradersBrokersUpdateJson> {
        return await new this._tradersBrokers(bodyTradersBrokers).save();
    }

    async showAllTradersBrokers(): Promise<TradersBrokersUpdateJson[]> {
        return await this._tradersBrokers.findAll(this.include);
    }

    async getTradersBrokersDetails(tradersBrokersId: string): Promise<TradersBrokersUpdateJson> {
        return await this._tradersBrokers.findByPk(tradersBrokersId, this.include);
    }

    async updateTradersBrokers(id: string, bodyTradersBrokers: Partial<TradersBrokersSaveJson>): Promise<[number, Array<TradersBrokersUpdateJson>]> {
        return await this._tradersBrokers.update(bodyTradersBrokers, {
            where: { id },
            returning: true
        });
    }

    async destroyTradersBrokers(tradersBrokersId: string): Promise<number> {
        return await this._tradersBrokers.destroy({
            where: { id: tradersBrokersId },
        });
    }

}
