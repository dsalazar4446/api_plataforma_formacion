import { Injectable, Inject } from '@nestjs/common';
import { UserModel } from '../../user/models/user.Model';
import { TraderUsers } from '../models/trader-users.model';
import { TraderUsersSaveJson, TraderUsersUpdateJson } from '../interfaces/trader.users.interface';
/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service TraderUsersService
 * @Creado 30 de Abril 2019
 */

@Injectable()
export class TraderUsersService {

    private include = {
        include: [
            {   
                as: 'trader',
                model: UserModel
            },
            {   
                as: 'user',
                model: UserModel
            }
        ]
    };

    constructor(
        @Inject('TraderUsers') private readonly _traderUsers: typeof TraderUsers
    ) { }

    async saveTraderUsers(bodyTraderUsers: TraderUsersSaveJson): Promise<TraderUsersUpdateJson> {
        return await new this._traderUsers(bodyTraderUsers).save();
    }

    async showAllTraderUsers(): Promise<TraderUsersUpdateJson[]> {
        return await this._traderUsers.findAll(this.include);
    }

    async getTraderUsersDetails(traderUsersId: string): Promise<TraderUsersUpdateJson> {
        return await this._traderUsers.findByPk(traderUsersId, this.include);
    }

    async updateTraderUsers(id: string, bodyTraderUsers: Partial<TraderUsersSaveJson>): Promise<[number, Array<TraderUsersUpdateJson>]> {
        return await this._traderUsers.update(bodyTraderUsers, {
            where: { id },
            returning: true
        });
    }

    async destroyTraderUsers(traderUsersId: string): Promise<number> {
        return await this._traderUsers.destroy({
            where: { id: traderUsersId },
        });
    }

}
