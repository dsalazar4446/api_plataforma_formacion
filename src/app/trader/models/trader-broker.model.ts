import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo, HasMany, BelongsToMany } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'traders_brokers',
})
export class TradersBrokers extends Model<TradersBrokers>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'trader_id',
    })
    traderId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'broker_id',
    })
    brokerId: string;

    
    @ApiModelPropertyOptional({
        type: UserModel,
        isArray: false,
        example: {
            "traderId": "UserModel"
        }
    })
    @BelongsTo(() => UserModel, 'traderId')
    trader: UserModel;
    
    @ApiModelPropertyOptional({
        type: UserModel,
        isArray: false,
        example: {
            "brokerId": "UserModel"
        }
    })
    @BelongsTo(() => UserModel, 'brokerId')
    broker: UserModel;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
