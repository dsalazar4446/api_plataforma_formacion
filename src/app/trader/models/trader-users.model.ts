import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo, HasMany, BelongsToMany } from 'sequelize-typescript';
import { UserModel } from './../../user/models/user.Model';
import { TraderviewUserRequest } from './traderview-user-request.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { TraderViewUserRequestArrayExample } from '../examples/trader-view-user-request.example';

@Table({
    tableName: 'trader_users',
})
export class TraderUsers extends Model<TraderUsers>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'trader_id',
    })
    traderId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'myfxbook_email',
    })
    myfxbookEmail: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'myfxbook_id',
    })
    myfxbookId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'trader_view_id',
    })
    traderViewId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        defaultValue: true,
        field: 'trader_user_status',
    })
    traderUserStatus: boolean;

    
    @ApiModelPropertyOptional({
        type: UserModel,
        isArray: false,
        example: {
            "traderId": "UserModel"
        }
    })
    @BelongsTo(() => UserModel, 'traderId')
    trader: UserModel;
    
    @ApiModelPropertyOptional({
        type: UserModel,
        isArray: false,
        example: {
            "userId": "UserModel"
        }
    })
    @BelongsTo(() => UserModel, 'userId')
    user: UserModel;
    
    @ApiModelPropertyOptional({
        example: TraderViewUserRequestArrayExample
    })
    @HasMany(() => TraderviewUserRequest)
    traderviewUserRequest: TraderviewUserRequest;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
