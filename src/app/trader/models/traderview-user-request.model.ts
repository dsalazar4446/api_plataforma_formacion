import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { TraderUsers } from './trader-users.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'tradeview_user_request',
})
export class TraderviewUserRequest extends Model<TraderviewUserRequest>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => TraderUsers)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'trader_user_id',
    })
    traderUserId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('connect', 'desconect', 'other'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'trader_user_request',
    })
    traderUserRequest: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('pending', 'on_progress', 'finished'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'tradeview_user_request_status',
    })
    traderUserRequestStatus: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'observations',
    })
    observations: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.NUMERIC,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        defaultValue: 0,
        field: 'trader_ammount',
    })
    traderAmmount: number;

    

    @BelongsTo(() => TraderUsers)
    traderUsers: TraderUsers;


    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
