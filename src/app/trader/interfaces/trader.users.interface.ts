import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsNotEmpty, IsString, IsBoolean } from 'class-validator';
export class TraderUsersSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'traderId property must a be uuid'})
    @IsNotEmpty({message: 'traderId property not must null'})
    traderId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    userId: string;

    @ApiModelProperty()
    @IsString({message: 'myfxbookEmail property must a be string'})
    @IsNotEmpty({message: 'myfxbookEmail property not must null'})
    myfxbookEmail: string;

    @ApiModelProperty()
    @IsString({message: 'myfxbookId property must a be string'})
    @IsNotEmpty({message: 'myfxbookId property not must null'})
    myfxbookId: string;

    @ApiModelProperty()
    @IsString({message: 'traderViewId property must a be string'})
    @IsNotEmpty({message: 'traderViewId property not must null'})
    traderViewId: string;

    @ApiModelProperty()
    @IsBoolean({message: 'traderUserStatus property must a be boolean'})
    @IsNotEmpty({message: 'traderUserStatus property not must null'})
    traderUserStatus: boolean;

}
export class TraderUsersUpdateJson extends TraderUsersSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
