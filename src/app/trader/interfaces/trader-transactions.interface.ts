import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsNotEmpty, IsString, IsNumber, IsBoolean, IsDateString } from 'class-validator';

export class TraderTransactionsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    userId: string;

    @ApiModelProperty()
    @IsString({message: 'traderReferal property must a be string'})
    @IsNotEmpty({message: 'traderReferal property not must null'})
    traderReferal: string;

    @ApiModelProperty()
    @IsString({message: 'transactionDate property must a be Date'})
    @IsNotEmpty({message: 'transactionDate property not must null'})
    transactionDate: string;

    @ApiModelProperty()
    @IsNumber(null, {message: 'profitSum property must a be number'})
    @IsNotEmpty({message: 'profitSum property not must null'})
    profitSum: number;

    @ApiModelProperty()
    @IsNumber(null, {message: 'swapsOrRollover property must a be number'})
    @IsNotEmpty({message: 'swapsOrRollover property not must null'})
    swapsOrRollover: number;

    @ApiModelProperty()
    @IsNumber(null, {message: 'profitPercentage property must a be number'})
    @IsNotEmpty({message: 'profitPercentage property not must null'})
    profitPercentage: number;

    @ApiModelProperty()
    @IsNumber(null, {message: 'netIncome property must a be number'})
    @IsNotEmpty({message: 'netIncome property not must null'})
    netIncome: number;

    @ApiModelProperty()
    @IsNumber(null, {message: 'floating property must a be number'})
    @IsNotEmpty({message: 'floating property not must null'})
    floating: number;

    @ApiModelProperty()
    @IsString({message: 'lastCollectionDate property must a be Date'})
    @IsNotEmpty({message: 'lastCollectionDate property not must null'})
    lastCollectionDate: string;

    @ApiModelProperty()
    @IsString({message: 'profitLosess property must a be string'})
    @IsNotEmpty({message: 'profitLosess property not must null'})
    profitLosess: string;

    @ApiModelProperty()
    @IsBoolean({message: 'payableOrNot property must a be boolean'})
    @IsNotEmpty({message: 'payableOrNot property not must null'})
    payableOrNot: boolean;

    @ApiModelProperty()
    @IsNumber(null, {message: 'managementCosts property must a be number'})
    @IsNotEmpty({message: 'managementCosts property not must null'})
    managementCosts: number;

    @ApiModelProperty()
    @IsNumber(null, {message: 'currentBalance property must a be number'})
    @IsNotEmpty({message: 'currentBalance property not must null'})
    currentBalance: number;

    @ApiModelProperty()
    @IsBoolean({message: 'equityCheck property must a be boolean'})
    @IsNotEmpty({message: 'equityCheck property not must null'})
    equityCheck: boolean;

    @ApiModelProperty()
    @IsNumber(null, {message: 'creexFee property must a be number'})
    @IsNotEmpty({message: 'creexFee property not must null'})
    creexFee: number;

    @ApiModelProperty()
    @IsNumber(null, {message: 'parentFee property must a be number'})
    @IsNotEmpty({message: 'parentFee property not must null'})
    parentFee: number;

    @ApiModelProperty()
    @IsNumber(null, {message: 'deposit property must a be number'})
    @IsNotEmpty({message: 'deposit property not must null'})
    deposit: number;
}
export class TraderTransactionsUpdateJson extends TraderTransactionsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
