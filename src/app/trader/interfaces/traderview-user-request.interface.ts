import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsNotEmpty, IsString, IsNumber } from 'class-validator';
export class TraderviewUserRequestSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'traderUserId property must a be uuid'})
    @IsNotEmpty({message: 'traderUserId property not must null'})
    traderUserId: string;

    @ApiModelProperty()
    @IsString({message: 'traderUserRequest property must a be string'})
    @IsNotEmpty({message: 'traderUserRequest property not must null'})
    traderUserRequest: string;

    @ApiModelProperty()
    @IsString({message: 'traderUserRequestStatus property must a be string'})
    @IsNotEmpty({message: 'traderUserRequestStatus property not must null'})
    traderUserRequestStatus: string;

    @ApiModelProperty()
    @IsString({message: 'observations property must a be string'})
    @IsNotEmpty({message: 'observations property not must null'})
    observations: string;

    @ApiModelProperty()
    @IsNumber(null, {message: 'traderAmmount property must a be number'})
    @IsNotEmpty({message: 'traderAmmount property not must null'})
    traderAmmount: number;

}
export class TraderviewUserRequestUpdateJson extends TraderviewUserRequestSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
