import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsNotEmpty } from 'class-validator';
export class TradersBrokersSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'traderId property must a be uuid'})
    @IsNotEmpty({message: 'traderId property not must null'})
    traderId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'brokerId property must a be uuid'})
    @IsNotEmpty({message: 'brokerId property not must null'})
    brokerId: string;

}
export class TradersBrokersUpdateJson extends TradersBrokersSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
