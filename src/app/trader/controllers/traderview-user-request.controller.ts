import { Controller, UseFilters, UseInterceptors, Post, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';

import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { TraderviewUserRequestService } from '../services/traderview-user-request.service';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { TraderviewUserRequestSaveJson } from '../interfaces/traderview-user-request.interface';
import { TraderviewUserRequest } from '../models/traderview-user-request.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller TraderviewUserRequestController
 * @Creado 30 de Abril 2019
 */

@ApiUseTags('Module Traderview User Request')
@Controller('traderview-user-request')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class TraderviewUserRequestController {

    constructor(private readonly _traderviewUserRequestService: TraderviewUserRequestService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: TraderviewUserRequest,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyTraderviewUserRequest: TraderviewUserRequestSaveJson) {

        const createTraderviewUserRequest = await this._traderviewUserRequestService.saveTraderviewUserRequest(bodyTraderviewUserRequest);

        if (!createTraderviewUserRequest) {
            throw this.appUtilsService.httpCommonError('Traderview User Request no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM53',
                languageType: 'es',
            });
        }

        return createTraderviewUserRequest;
    }


    @ApiResponse({
        status: 200,
        type: TraderviewUserRequest,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAll() {
        return await this._traderviewUserRequestService.showAllTraderviewUserRequest();
    }


    @ApiResponse({
        status: 200,
        type: TraderviewUserRequest,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async getTraderviewUserRequestDetails(@Param('id') idTraderviewUserRequest: string) {

        const fetchTraderviewUserRequest = await this._traderviewUserRequestService.getTraderviewUserRequestDetails(idTraderviewUserRequest);

        if (!fetchTraderviewUserRequest) {
            throw this.appUtilsService.httpCommonError('Traderview User Request no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM54',
                languageType: 'es',
            });
        }

        return fetchTraderviewUserRequest;
    }


    @ApiResponse({
        status: 200,
        type: TraderviewUserRequest,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateTraderviewUserRequest(@Param('id') id: string, @Body() bodyTraderviewUserRequest: Partial<TraderviewUserRequestSaveJson>) {

        const updateTraderviewUserRequest = await this._traderviewUserRequestService.updateTraderviewUserRequest(id, bodyTraderviewUserRequest);

        if (!updateTraderviewUserRequest[1][0]) {
            throw this.appUtilsService.httpCommonError('Traderview User Request no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM54',
                languageType: 'es',
            });
        }

        return updateTraderviewUserRequest;
    }



    @ApiResponse({
        status: 200,
        type: TraderviewUserRequest,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteTransaction(@Param('id') idTraderviewUserRequest: string) {

        const deleteTraderviewUserRequest = await this._traderviewUserRequestService.destroyTraderviewUserRequest(idTraderviewUserRequest);

        if (!deleteTraderviewUserRequest) {
            throw this.appUtilsService.httpCommonError('Traderview User Request no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM54',
                languageType: 'es',
            });
        }
        return deleteTraderviewUserRequest;
    }
}
