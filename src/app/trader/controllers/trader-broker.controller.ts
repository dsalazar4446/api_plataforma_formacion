import { Controller, UseFilters, UseInterceptors, Post, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus } from '@nestjs/common';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { TradersBrokersService } from '../services/trader-broker.service';
import { TradersBrokersSaveJson, TradersBrokersUpdateJson } from '../interfaces/trader-broker.interdace';
import { TradersBrokers } from '../models/trader-broker.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller TradersBrokersController
 * @Creado 30 de Abril 2019
 */

@ApiUseTags('Module Traders Brokers')
@Controller('traders-brokers')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class TradersBrokersController {

    constructor(private readonly _tradersBrokersService: TradersBrokersService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: TradersBrokers,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyTradersBrokers: TradersBrokersSaveJson) {

        const createTradersBrokers = await this._tradersBrokersService.saveTradersBrokers(bodyTradersBrokers);

        if (!createTradersBrokers) {
            throw this.appUtilsService.httpCommonError('Traders Brokers no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM59',
                languageType: 'es',
            });
        }

        return createTradersBrokers;
    }


    @ApiResponse({
        status: 200,
        type: TradersBrokers,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAll() {
        return await this._tradersBrokersService.showAllTradersBrokers();
    }


    @ApiResponse({
        status: 200,
        type: TradersBrokers,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async getTradersBrokersDetails(@Param('id') idTradersBrokers: string) {

        const fetchTradersBrokers = await this._tradersBrokersService.getTradersBrokersDetails(idTradersBrokers);

        if (!fetchTradersBrokers) {
            throw this.appUtilsService.httpCommonError('Traders Brokers no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM60',
                languageType: 'es',
            });
        }

        return fetchTradersBrokers;
    }



    @ApiResponse({
        status: 200,
        type: TradersBrokers,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateTradersBrokers(@Param('id') id: string, @Body() bodyTradersBrokers: Partial<TradersBrokersSaveJson>) {

        const updateTradersBrokers = await this._tradersBrokersService.updateTradersBrokers(id, bodyTradersBrokers);

        if (!updateTradersBrokers[1][0]) {
            throw this.appUtilsService.httpCommonError('Traders Brokers no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM60',
                languageType: 'es',
            });
        }

        return updateTradersBrokers;
    }


    @ApiResponse({
        status: 200,
        type: TradersBrokers,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteTransaction(@Param('id') idTradersBrokers: string) {

        const deleteTradersBrokers = await this._tradersBrokersService.destroyTradersBrokers(idTradersBrokers);

        if (!deleteTradersBrokers) {
            throw this.appUtilsService.httpCommonError('Traders Brokers no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM60',
                languageType: 'es',
            });
        }
        return deleteTradersBrokers;
    }
}
