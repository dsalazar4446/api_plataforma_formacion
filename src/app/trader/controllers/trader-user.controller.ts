import { Controller, UseFilters, UseInterceptors, Post, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus } from '@nestjs/common';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { TraderUsersService } from '../services/trader-user.service';
import { TraderUsersSaveJson } from '../interfaces/trader.users.interface';
import { TraderUsers } from '../models/trader-users.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller TraderUsersController
 * @Creado 30 de Abril 2019
 */

@ApiUseTags('Module Trader Users')
@Controller('trader-users')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class TraderUsersController {

    constructor(private readonly _traderUsersService: TraderUsersService,
        private readonly appUtilsService: AppUtilsService) { }



    @ApiResponse({
        status: 200,
        type: TraderUsers,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyTraderUsers: TraderUsersSaveJson) {

        const createTraderUsers = await this._traderUsersService.saveTraderUsers(bodyTraderUsers);

        if (!createTraderUsers) {
            throw this.appUtilsService.httpCommonError('Trader Users no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM55',
                languageType: 'es',
            });
        }

        return createTraderUsers;
    }



    @ApiResponse({
        status: 200,
        type: TraderUsers,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAll() {
        return await this._traderUsersService.showAllTraderUsers();
    }



    @ApiResponse({
        status: 200,
        type: TraderUsers,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async getTraderUsersDetails(@Param('id') idTraderUsers: string) {

        const fetchTraderUsers = await this._traderUsersService.getTraderUsersDetails(idTraderUsers);

        if (!fetchTraderUsers) {
            throw this.appUtilsService.httpCommonError('Trader Users no existe', HttpStatus.NOT_FOUND, {
                messageCode: 'EM56',
                languageType: 'es',
            });
        }

        return fetchTraderUsers;
    }




    @ApiResponse({
        status: 200,
        type: TraderUsers,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateTraderUsers(@Param('id') id: string, @Body() bodyTraderUsers: Partial<TraderUsersSaveJson>) {

        const updateTraderUsers = await this._traderUsersService.updateTraderUsers(id, bodyTraderUsers);

        if (!updateTraderUsers[1][0]) {
            throw this.appUtilsService.httpCommonError('Trader Users no existe', HttpStatus.NOT_FOUND, {
                messageCode: 'EM56',
                languageType: 'es',
            });
        }

        return updateTraderUsers;
    }



    @ApiResponse({
        status: 200,
        type: TraderUsers,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteTransaction(@Param('id') idTraderUsers: string) {

        const deleteTraderUsers = await this._traderUsersService.destroyTraderUsers(idTraderUsers);

        if (!deleteTraderUsers) {
            throw this.appUtilsService.httpCommonError('Trader Users no existe', HttpStatus.NOT_FOUND, {
                messageCode: 'EM56',
                languageType: 'es',
            });
        }
        return deleteTraderUsers;
    }
}
