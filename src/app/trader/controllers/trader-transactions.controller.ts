import { Controller, UseFilters, UseInterceptors, Post, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus } from '@nestjs/common';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { TraderTransactionsService } from '../services/trader-transactions.service';
import { TraderTransactionsSaveJson } from '../interfaces/trader-transactions.interface';
import { TraderTransactions } from '../models/trader-transactions.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller TraderTransactionsController
 * @Creado 30 de Abril 2019
 */

@ApiUseTags('Module Trader Transactions')
@Controller('trader-transactions')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class TraderTransactionsController {

    constructor(private readonly _traderTransactionsService: TraderTransactionsService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: TraderTransactions,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyTraderTransactions: TraderTransactionsSaveJson) {

        const createTraderTransactions = await this._traderTransactionsService.saveTraderTransactions(bodyTraderTransactions);

        if (!createTraderTransactions) {
            throw this.appUtilsService.httpCommonError('Trader Transactions no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM57',
                languageType: 'es',
            });
        }

        return createTraderTransactions;
    }


    @ApiResponse({
        status: 200,
        type: TraderTransactions,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAll() {
        return await this._traderTransactionsService.showAllTraderTransactions();
    }


    @ApiResponse({
        status: 200,
        type: TraderTransactions,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async getTraderTransactionsDetails(@Param('id') idTraderTransactions: string) {

        const fetchTraderTransactions = await this._traderTransactionsService.getTraderTransactionsDetails(idTraderTransactions);

        if (!fetchTraderTransactions) {
            throw this.appUtilsService.httpCommonError('Trader Transactions no existe', HttpStatus.NOT_FOUND, {
                messageCode: 'EM58',
                languageType: 'es',
            });
        }

        return fetchTraderTransactions;
    }



    @ApiResponse({
        status: 200,
        type: TraderTransactions,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateTraderTransactions(@Param('id') id: string, @Body() bodyTraderTransactions: Partial<TraderTransactionsSaveJson>) {

        const updateTraderTransactions = await this._traderTransactionsService.updateTraderTransactions(id, bodyTraderTransactions);

        if (!updateTraderTransactions[1][0]) {
            throw this.appUtilsService.httpCommonError('Trader Transactions no existe', HttpStatus.NOT_FOUND, {
                messageCode: 'EM58',
                languageType: 'es',
            });
        }

        return updateTraderTransactions;
    }


    @ApiResponse({
        status: 200,
        type: TraderTransactions,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteTransaction(@Param('id') idTraderTransactions: string) {

        const deleteTraderTransactions = await this._traderTransactionsService.destroyTraderTransactions(idTraderTransactions);

        if (!deleteTraderTransactions) {
            throw this.appUtilsService.httpCommonError('Trader Transactions no existe', HttpStatus.NOT_FOUND, {
                messageCode: 'EM58',
                languageType: 'es',
            });
        }
        return deleteTraderTransactions;
    }
}
