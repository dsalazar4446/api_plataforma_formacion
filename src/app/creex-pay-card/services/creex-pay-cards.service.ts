import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { CreexPayCards } from '../models/creex-pay-cards.model';
import { CreexPayCardsSaveJson, CreexPayCardsUpdateJson } from '../interfaces/creex-pay-cards.interface';
import * as ShortUniqueId from 'short-unique-id';
import { UserModel } from '../../user/models/user.Model';
import { UserLocationModel } from '../../user-location/models/user-location.model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
const uid = new ShortUniqueId();

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller CreexPayCardsService
 * @Creado 23 de Marzo 2019
 */

@Injectable()
export class CreexPayCardsService {

    constructor(
        @Inject('CreexPayCards') private readonly _creexPayCards: typeof CreexPayCards,
        private readonly appUtilsService: AppUtilsService
    ) { }

    async saveCreexPayCards(bodyCreex: CreexPayCardsSaveJson): Promise<CreexPayCardsUpdateJson> {

        if(!bodyCreex.cardLastNumbers) bodyCreex.cardLastNumbers = '0000';
        bodyCreex.cardToken = uid.randomUUID(17) + uid.randomUUID(17);

        return await new this._creexPayCards(bodyCreex).save();

    }

    async showAllCreexPayCards(): Promise<CreexPayCardsUpdateJson[]> {

        return await this._creexPayCards.findAll();

    }

    async getCreexPayCards(creexId): Promise<CreexPayCardsUpdateJson> {

        return await this._creexPayCards.findByPk(creexId, {
            include: [
                {
                    model: UserModel
                },
                {
                    model: UserLocationModel
                }
            ]
        });
    }

    async updateCreexPayCards(id: string, bodyCreex: Partial<CreexPayCardsSaveJson>): Promise<[number, Array<any>]> {

        if(!bodyCreex.cardLastNumbers || bodyCreex.cardLastNumbers.length !== 4){
            throw this.appUtilsService.httpCommonError('could not update!', HttpStatus.NOT_ACCEPTABLE, {
                ok: false,
                message: 'Last four numbers card: ' + bodyCreex.cardLastNumbers + '. Invalid length',
            });
        }

        return await this._creexPayCards.update(bodyCreex, { 
            where: { id },
            returning: true
        });

    }

    async destroyCreexPayCards(creexId): Promise<number> {

        return await this._creexPayCards.destroy({
            where: {id: creexId},
        });
    }
}
