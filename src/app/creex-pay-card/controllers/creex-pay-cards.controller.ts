// tslint:disable-next-line: max-line-length
import { Controller, UseFilters, UseInterceptors, Post, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { CreexPayCardsService } from '../services/creex-pay-cards.service';
import { CreexPayCardsSaveJson, CreexPayCardsUpdateJson } from '../interfaces/creex-pay-cards.interface';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { CreexPayCards } from '../models/creex-pay-cards.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller CreexPayCardsController
 * @Creado 23 de Marzo 2019
 */

@ApiUseTags('Module-Creex-Pay-Card')
@Controller('creex-pay-cards')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class CreexPayCardsController {

    constructor(
        private readonly _creexPayCardServcs: CreexPayCardsService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: CreexPayCards,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyCreex: CreexPayCardsSaveJson) {

        const createCreexPayCards = await this._creexPayCardServcs.saveCreexPayCards(bodyCreex);

        if (!createCreexPayCards) {
            throw this.appUtilsService.httpCommonError('Creex Pay Cards no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM29',
                languageType: 'es',
            });
        }

        return createCreexPayCards;
    }


    @ApiResponse({
        status: 200,
        type: CreexPayCards,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllCreexPayCards() {

        return await this._creexPayCardServcs.showAllCreexPayCards();
    }


    @ApiResponse({
        status: 200,
        type: CreexPayCards,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') creexId) {

        const fetchCreexPayCards = await this._creexPayCardServcs.getCreexPayCards(creexId);

        if (!fetchCreexPayCards) {
            throw this.appUtilsService.httpCommonError('Creex Pay Cards no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM30',
                languageType: 'es',
            });
        }

        return fetchCreexPayCards;
    }


    @ApiResponse({
        status: 200,
        type: CreexPayCards,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateCreexPayCards(@Param('id') id: string, @Body() bodyCreex: Partial<CreexPayCardsSaveJson>) {

        const updateCreexPayCards = await this._creexPayCardServcs.updateCreexPayCards(id, bodyCreex);

        if (!updateCreexPayCards[1][0]) {
            throw this.appUtilsService.httpCommonError('Creex Pay Cards no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM30',
                languageType: 'es',
            });
        }

        return updateCreexPayCards;
    }


    @ApiResponse({
        status: 200,
        type: CreexPayCards,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteCreexPayCards(@Param('id') creexId) {

        const deleteCreexPayCards = await this._creexPayCardServcs.destroyCreexPayCards(creexId);

        if (!deleteCreexPayCards) {
            throw this.appUtilsService.httpCommonError('Creex Pay Cards no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM30',
                languageType: 'es',
            });
        }

        return deleteCreexPayCards;
    }



}
