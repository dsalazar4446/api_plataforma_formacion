import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsDateString, IsOptional, Length, IsDate } from 'class-validator';
export class CreexPayCardsSaveJson {
    @ApiModelProperty()
    @IsUUID('4',{message: 'usersId property must a be uuid'})
    @IsNotEmpty({message: 'usersId property not must null'})
    usersId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'userLocationsId property must a be uuid'})
    @IsNotEmpty({message: 'userLocationsId property not must null'})
    userLocationsId: string;

    @ApiModelProperty()
    cardToken?: string;

    @ApiModelProperty()
    @IsString({message: 'cardLastNumbers property must a be string'})
    @Length(4, 4)
    @IsNotEmpty({message: 'cardLastNumbers property not must null'})
    cardLastNumbers: string;

    @ApiModelProperty()
    @IsDate()
    @IsNotEmpty({message: 'cardDateRequest property not must null'})
    cardDateRequest: Date

    @ApiModelProperty()
    @IsDate()
    @IsNotEmpty({message: 'cardDateSend property not must null'})
    cardDateSend: Date

    @ApiModelProperty()
    @IsDate()
    @IsNotEmpty({message: 'cardDateRecieved property not must null'})
    cardDateRecieved: Date

}
export class CreexPayCardsUpdateJson extends CreexPayCardsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
