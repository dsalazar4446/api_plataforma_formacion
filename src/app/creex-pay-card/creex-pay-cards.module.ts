import { Module, HttpModule, Provider } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { CreexPayCardsController } from './controllers/creex-pay-cards.controller';
import { CreexPayCards} from './models/creex-pay-cards.model';
import { CreexPayCardsService} from './services/creex-pay-cards.service';

const models = [
    CreexPayCards 
];

const controllers = [
    CreexPayCardsController
];

const providers: Provider[] = [
    CreexPayCardsService
];

@Module({
    imports: [
        SharedModule,
        HttpModule,
        DatabaseModule.forRoot({
            sequelize: {
                models,
            },
            loki: true,
        }),
    ],
    providers,
    controllers
})

@Module({})
export class CreexPayCardModule { }


