import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo, HasMany, BelongsToMany } from 'sequelize-typescript';
import { UserModel } from './../../user/models/user.Model';
import { UserLocationModel } from '../../user-location/models/user-location.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'creex_pay_cards',
})
export class CreexPayCards extends Model<CreexPayCards>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'users_id',
    })
    usersId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserLocationModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_locations_id',
    })
    userLocationsId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(36),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'card_token',
    })
    cardToken: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(4),
        allowNull: true,
        validate: {
            notEmpty: false,
            len: [4, 4]
        },
        field: 'card_last_numbers',
    })
    cardLastNumbers: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'card_date_request',
    })
    cardDateRequest: Date;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'card_date_send',
    })
    cardDateSend: Date;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'card_date_recieved',
    })
    cardDateRecieved: Date;

    /**
     * RELACIONES
     * CreexPayCards pertenece a UserModel
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => UserModel)
    userModel: UserModel;
    
    @ApiModelPropertyOptional()
    @BelongsTo(() => UserLocationModel)
    userLocationModel: UserLocationModel;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
