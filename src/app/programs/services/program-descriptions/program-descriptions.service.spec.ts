import { Test, TestingModule } from '@nestjs/testing';
import { ProgramDescriptionsService } from './program-descriptions.service';

describe('ProgramDescriptionsService', () => {
  let service: ProgramDescriptionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProgramDescriptionsService],
    }).compile();

    service = module.get<ProgramDescriptionsService>(ProgramDescriptionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
