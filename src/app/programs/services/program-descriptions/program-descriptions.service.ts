import { Injectable, Inject } from '@nestjs/common';
import { ProgramDescriptionModel } from '../../models/program-descriptions.model';
import { CreateProgramDescriptionDto, UpdateProgramDescriptionDto } from '../../dto/program-description.dto';

@Injectable()
export class ProgramDescriptionsService {
    constructor(@Inject('ProgramDescriptionModel') private readonly programDescription: typeof ProgramDescriptionModel) {}

    async create(body: CreateProgramDescriptionDto): Promise<ProgramDescriptionModel> {
        const createDescription = new ProgramDescriptionModel(body);
        return await createDescription.save();
    }

    async list(languageType: string) {
        return await this.programDescription.findAll({where:{
            languageType
        }});
    }

    async detail(id: string, languageType:string) {
        return await this.programDescription.findByPk(id, {where:{languageType}});
    }

    async update(id: string, body: Partial<UpdateProgramDescriptionDto>) {
        return await this.programDescription.update(body, {where: {id,},returning:true});
    }

    async delete(id: string){
        return await this.programDescription.destroy({where: {id}});
    }
}
