import { Injectable, Inject } from '@nestjs/common';
import { UserProgramsModel } from '../../models/user-programs.model';
import { CreateUserProgramsDto, UpdateUserProgramsDto } from '../../dto/user-programs.dto';

@Injectable()
export class UserProgramService {
    constructor(@Inject('UserProgramsModel') private readonly userPrograms: typeof UserProgramsModel) {}

    async create(body: CreateUserProgramsDto) {
        const createUserProgram = new UserProgramsModel(body);
        return await createUserProgram.save();
    }
    async list() {
        return await this.userPrograms.findAll();
    }
    async detail(id: string) {
        return await this.userPrograms.find({where: {id}});
    }
    async update(id: string, body: Partial<UpdateUserProgramsDto>) {
        return await this.userPrograms.update(body, {where: {id}, returning: true})
    }
    async delete(id: string) {
        return await this.userPrograms.destroy({ where: { id }});
    }
}
