import { Test, TestingModule } from '@nestjs/testing';
import { ProgramHasCoursesService } from './program-has-courses.service';

describe('ProgramHasCoursesService', () => {
  let service: ProgramHasCoursesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProgramHasCoursesService],
    }).compile();

    service = module.get<ProgramHasCoursesService>(ProgramHasCoursesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
