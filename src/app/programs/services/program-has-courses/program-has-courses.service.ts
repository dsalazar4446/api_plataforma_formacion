import { Injectable, Inject } from '@nestjs/common';
import { CreateProgramHasCourses } from '../../dto/program-has-courses.dto';
import { ProgramHasCoursesModel } from '../../models/program-has-courses';

@Injectable()
export class ProgramHasCoursesService {
    constructor(@Inject('ProgramHasCoursesModel') private readonly programCourses: typeof ProgramHasCoursesModel) {}

    async create(body: CreateProgramHasCourses) {
        const createProgramCourse = new ProgramHasCoursesModel(body);
        return await createProgramCourse.save();
    }
    async delete(id: string) {
        return await this.programCourses.destroy({where: {id}});
    }
}
