import { Injectable, Inject } from '@nestjs/common';
import { ProgramsModel } from '../../models/programs.model';
import { CreateProgramDto, UpdateProgramDto } from '../../dto/program.dto';
import { ProgramDescriptionModel } from '../../models/program-descriptions.model';
import { Sequelize } from 'sequelize-typescript';
import { Surveys } from '../../../surveys/models/surveys.models';
import { CONFIG } from '../../../../config';
import * as ShortUniqueId from 'short-unique-id';
import { CoursesModel } from '../../../courses/models/courses.model';
import { MainVideosModel } from '../../../main-videos/models/main-videos.model';
import { UserProgramsModel } from '../../models/user-programs.model';
import * as path from 'path';
import * as fs from 'fs'
import { UserProgramRatesModel } from '../../models/user-program-rates.model';
import { Certificates } from '../../../certificateAcademies/models/certificates.models';
import sequelize = require('sequelize');

const include = [
    {
        model: ProgramDescriptionModel,
        attributes: [
            'programTitle',
            'programDetail',
        ],

    },
    Surveys,
    CoursesModel,
    MainVideosModel,
    Certificates,
    {
        model: UserProgramsModel,
        include: [
            {
                model: UserProgramRatesModel
            }
        ]
    }
]

@Injectable()
export class ProgramsService {
    limit: number = 50;
    constructor(
        @Inject('ProgramsModel') private readonly programsModel: typeof ProgramsModel,
        @Inject('UserProgramsModel') private readonly userProgramsModel: typeof UserProgramsModel,
        @Inject('UserProgramRatesModel') private readonly userProgramRatesModel: typeof UserProgramRatesModel
    ) { }
    includes(language: string, programTitle?: string, programStatus?:string) {  
            return [
                {
                    model: ProgramDescriptionModel,
                    where: { language_type: language}
                },
                Surveys,
                CoursesModel,
                MainVideosModel,
                Certificates,
                {
                    model: UserProgramsModel,
                    include: [
                        {
                            model: UserProgramRatesModel
                        }
                    ],
                }
            ]
        
    }
    async create(program: CreateProgramDto) {
        
        const uid = new ShortUniqueId();
        program.programCode = uid.randomUUID(8);
        const createProgram = new ProgramsModel(program);
        return await createProgram.save();
    }

    async list(language: string = '1', page: string, programTitle?: string, programStatus?: string) {
        let offset = 0
        let currentPage = parseInt(page);
        let pages: number; 
        offset = this.limit * (currentPage - 1)
        let result: { rows:ProgramsModel[] ,count:number};        
            result = await this.programsModel.findAndCountAll({
                include: this.includes(language),
                where: { programStatus: '1' },
                limit: this.limit,
                offset
            })
        
        pages = Math.ceil(result.count / this.limit)
        let students = 0;
        let userP: UserProgramsModel[];
        let promRate =  0
        if (result.rows.length > 0) {
            result.rows.forEach(element => {
                if(element.dataValues.userPrograms){
                    userP = element.userPrograms.filter(userProgram =>{
                        return userProgram.userProgramRates.filter.length > 0
                    })
                }
            });
            let userRate: UserProgramRatesModel[]
            if(userP){
                userP.forEach(element => {
                    if(element.dataValues.userProgramRates){
                        userRate = element.userProgramRates.filter(element => {return element.userProgramRateStatus == true})
                    }
                })
            }
           if(userRate){
               userRate.forEach(element => {
                   promRate += element.dataValues.userProgramRate
               })
           }
            if(userRate){
                promRate = promRate/userRate.length
            }
        }
        
        let array = []
        result.rows.forEach(program => {
            let programa = {
                id: program.id,
                categoriesId: program.categoriesId,
                programCode: program.programCode,
                programStatus: program.programStatus,
                programBackground: program.programBackground,
                programIcon: program.programIcon,
                fbPromotionImg: program.fbPromotionImg,
                cantStudents: program.userPrograms.length,
                rates: promRate,
                programDescription:program.programDescription,
                userPrograms:program.userPrograms,
                surveys:program.surveys,
                mainVideos:program.mainVideos,
                courses:program.courses,
                certificates:program.certificates

            }
            array.push(programa)
       
        })
        array.map((role) => {
            role.fbPromotionImg = `${CONFIG.storage.server}programs/${role.fbPromotionImg}`
            role.programIcon = `${CONFIG.storage.server}programs/${role.programIcon}`
            role.programBackground = `${CONFIG.storage.server}programs/${role.programBackground}`
        })
        return { currentPage: page, perPage: this.limit, pages, students,  programs: array }
    }
    
    async listAdmin(page: string = '1', language: string, programTitle?: string) {

    
        let offset = 0
        let currentPage = parseInt(page);
        let pages: number; 
        offset = this.limit * (currentPage - 1)
        let result: { rows:ProgramsModel[] ,count:number};
    
            result = await this.programsModel.findAndCountAll({
                include: this.includes(language),
                limit:this.limit,
                offset
            })
        
        pages = Math.ceil(result.count / this.limit)
        let students = 0;
        let userP: UserProgramsModel[];
        let promRate =  0
        if (result.rows.length > 0) {
            result.rows.forEach(element => {
                userP = element.userPrograms.filter(userProgram =>{
                    return userProgram.userProgramRates.filter.length > 0
                })
            });
            let userRate: UserProgramRatesModel[]
            userP.forEach(element => {
                userRate = element.userProgramRates.filter(element => {return element.userProgramRateStatus == true})
            })           
            userRate.forEach(element => {
                promRate += element.userProgramRate
            })
            if(userRate.length){
                promRate = promRate/userRate.length
            }
        }
        
        let array = []
        result.rows.forEach(program => {
            let programa = {
                id: program.id,
                categoriesId: program.categoriesId,
                programCode: program.programCode,
                programStatus: program.programStatus,
                programBackground: program.programBackground,
                programIcon: program.programIcon,
                fbPromotionImg: program.fbPromotionImg,
                cantStudents: program.userPrograms.length,
                rates: promRate,
                programDescription:program.programDescription,
                userPrograms:program.userPrograms,
                surveys:program.surveys,
                mainVideos:program.mainVideos,
                courses:program.courses,
                certificates:program.certificates

            }
            array.push(programa)
       
        })
        
        array.map((role) => {
            role.fbPromotionImg = `${CONFIG.storage.server}programs/${role.fbPromotionImg}`
            role.programIcon = `${CONFIG.storage.server}programs/${role.programIcon}`
            role.programBackground = `${CONFIG.storage.server}programs/${role.programBackground}`
        })
        return { currentPage: page, perPage: this.limit, pages, students,  programs: array }
    }

    async detail(id: string, language: string) {
        const result = await this.programsModel.findOne({
            include: this.includes(language),
            where: {
                id,
                programStatus: '1'
            },
        });
        if (result) {
            result.fbPromotionImg = `${CONFIG.storage.server}programs/${result.fbPromotionImg}`
            result.programIcon = `${CONFIG.storage.server}programs/${result.programIcon}`
            result.programBackground = `${CONFIG.storage.server}programs/${result.programBackground}`
        }

        return result;
    }

    async detailAdmin(id: string, language: string) {
        const result = await this.programsModel.findOne({
            include: this.includes(language),
            where: {
                id,
                programStatus: status,
            },
        });
        if (result) {
            result.fbPromotionImg = `${CONFIG.storage.server}programs/${result.fbPromotionImg}`
            result.programIcon = `${CONFIG.storage.server}programs/${result.programIcon}`
            result.programBackground = `${CONFIG.storage.server}programs/${result.programBackground}`
        }

        return result;
    }

    async update(id: string, program: Partial<UpdateProgramDto>) {

        if (program.fbPromotionImg && program.programBackground && program.programIcon) {
            await this.deleteImg(id)
        }
        return await this.programsModel.update(program, { where: { id }, returning: true });
    }

    async delete(id: string) {
        await this.deleteImg(id)
        return await this.programsModel.destroy({ where: { id } });
    }

    async charts(userId: string) {
        const totalPrograms = await this.userProgramsModel.findAndCountAll({where: {usersId:userId}})
        const programOnProgress = await this.userProgramsModel.findAndCountAll({where: {usersId:userId, userProgramStatus: '1'}})
        const programFinish = await this.userProgramsModel.findAndCountAll({where: {usersId:userId, userProgramStatus: '2'}})

        return {
            completed: (programOnProgress.count / totalPrograms.count) * 100,
            finished: (programFinish.count / totalPrograms.count) * 100
        }
    }

    private async deleteImg(id: string) {
        let img = await this.programsModel.findByPk(id).then(item => item).catch(err => err);

        let pathImagen = path.resolve(__dirname, `../../../../../uploads/programs/${img.fbPromotionImg}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }

        pathImagen = path.resolve(__dirname, `../../../../../uploads/programs/${img.programIcon}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }

        pathImagen = path.resolve(__dirname, `../../../../../uploads/programs/${img.programBackground}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }

}
