import { Injectable, Inject } from '@nestjs/common';
import { ProgramHasVideosModel } from '../../models/programs-has-videos.model';
import { CreateProgramHasVideos } from '../../dto/programs-has-videos.dto';

@Injectable()
export class ProgramHasVideosService {
    constructor(@Inject('ProgramHasVideosModel') private readonly programVideos: typeof ProgramHasVideosModel) {}

    async create(body: CreateProgramHasVideos) {
        const createProgramVideo = new ProgramHasVideosModel(body);
        return await createProgramVideo.save();
    }
    async delete(id: string) {
        return await this.programVideos.destroy({where: {id}});
    }
}

