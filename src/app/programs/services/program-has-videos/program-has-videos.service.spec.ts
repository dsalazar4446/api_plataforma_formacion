import { Test, TestingModule } from '@nestjs/testing';
import { ProgramHasVideosService } from './program-has-videos.service';

describe('ProgramHasVideosService', () => {
  let service: ProgramHasVideosService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProgramHasVideosService],
    }).compile();

    service = module.get<ProgramHasVideosService>(ProgramHasVideosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
