import { Test, TestingModule } from '@nestjs/testing';
import { UserProgramRatesService } from './user-program-rates.service';

describe('UserProgramRatesService', () => {
  let service: UserProgramRatesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserProgramRatesService],
    }).compile();

    service = module.get<UserProgramRatesService>(UserProgramRatesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
