import { Injectable, Inject } from '@nestjs/common';
import { UserProgramRatesModel } from '../../models/user-program-rates.model';
import { CreateUserProgramRatesDto, UpdateUserProgramsRatesDto } from '../../dto/user-program-rates';

@Injectable()
export class UserProgramRatesService {
    constructor(@Inject('UserProgramRatesModel') private readonly userProgramsRates: typeof UserProgramRatesModel) {}

    async create(body: CreateUserProgramRatesDto) {
        const createUserProgram = new UserProgramRatesModel(body);
        return await createUserProgram.save();
    }
    async list(languageType: string) {
        return await this.userProgramsRates.findAll({where:{languageType}});
    }
    async detail(id: string, languageType: string) {
        return await this.userProgramsRates.findByPk(id,{where:{languageType}});
    }
    async update(id: string, body: Partial<UpdateUserProgramsRatesDto>) {
        return await this.userProgramsRates.update(body, {where: {id}, returning: true});
    }
    async delete(id: string) {
        return await this.userProgramsRates.destroy({where: {id}});
    }
}
