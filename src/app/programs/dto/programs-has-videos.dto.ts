import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID } from "class-validator";

export class CreateProgramHasVideos {
    @ApiModelProperty()
    @IsUUID('4')
    programsId: string;
    @ApiModelProperty()
    @IsUUID('4')
    mainVideosId: string;
}