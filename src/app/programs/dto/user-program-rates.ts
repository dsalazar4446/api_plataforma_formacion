import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString, IsNumber, IsBoolean, IsOptional } from "class-validator";

export class CreateUserProgramRatesDto {
    @ApiModelProperty()
    @IsUUID('4')
    userProgramsId: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    languageType: string;
    @ApiModelProperty()
    @IsNumber()
    userProgramRate: number;
    @ApiModelProperty()
    @IsString()
    userProgramComments: string;
    @ApiModelProperty()
    @IsBoolean()
    userProgramRateStatus: boolean;
}
// tslint:disable-next-line: max-classes-per-file
export class UpdateUserProgramsRatesDto extends  CreateUserProgramRatesDto{
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}