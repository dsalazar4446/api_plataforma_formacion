import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString, IsBoolean } from "class-validator";

export class CreateUserProgramsDto {
    @ApiModelProperty()
    @IsUUID('4')
    usersId: string;
    @ApiModelProperty()
    @IsString()
    programsId: string;
    @ApiModelProperty()
    @IsString()
    userProgramStatus: string;
    @ApiModelProperty()
    @IsBoolean()
    userProgramFavorite: boolean;
}

// tslint:disable-next-line: max-classes-per-file
export class UpdateUserProgramsDto extends  CreateUserProgramsDto{
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}