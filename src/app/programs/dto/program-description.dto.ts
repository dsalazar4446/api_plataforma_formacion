    import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString, IsOptional } from "class-validator";

export class CreateProgramDescriptionDto {
    @ApiModelProperty()
    @IsUUID('4')
    programsId: string;
    @ApiModelProperty()
    @IsString()
    programTitle: string;
    @ApiModelProperty()
    @IsString()
    programDetail: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    languageType: string;

}

// tslint:disable-next-line: max-classes-per-file
export class UpdateProgramDescriptionDto extends CreateProgramDescriptionDto {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}
