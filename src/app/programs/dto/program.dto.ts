import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";
import { IsUUID, IsString, IsOptional, IsNotEmpty } from "class-validator";

export class CreateProgramDto {
    @ApiModelProperty()
    @IsString()
    categoriesId?: string;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    programCode?: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    programStatus?: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    programBackground?: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    programIcon?: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    fbPromotionImg?: string;

}

// tslint:disable-next-line: max-classes-per-file
export class UpdateProgramDto extends CreateProgramDto {
    @IsString()
    @ApiModelProperty()
    id: string;
}
