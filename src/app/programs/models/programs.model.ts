// tslint:disable-next-line: max-line-length
import { Model, Table, Column, DataType, ForeignKey, CreatedAt, UpdatedAt, HasMany, BelongsTo, BelongsToMany } from 'sequelize-typescript';
import { ProgramDescriptionModel } from './program-descriptions.model';
import { SurveysHasPrograms } from './../../surveys/models/surveys-has-programs.models';
import { Categories } from './../../categories/models/categories.model';
import { Surveys } from '../../surveys/models/surveys.models';
import { CoursesModel } from '../../courses/models/courses.model';
import { MainVideosModel } from '../../main-videos/models/main-videos.model';
import { ProgramHasCoursesModel } from './program-has-courses';
import { ProgramHasVideosModel } from './programs-has-videos.model';
import { UserProgramsModel } from './user-programs.model';
import { Certificates } from '../../certificateAcademies/models/certificates.models';
import { ProgramsHasCertificates } from '../../certificateAcademies/models/program-has-certificates.model';
import { ApiModelProperty } from '@nestjs/swagger';
import { MainVideosArrayExample } from '../../main-videos/example/main-videos.example';
import { CoursesArrayExample } from '../../courses/examples/courses.example';
import { CertificatesArrayExample } from '../../certificateAcademies/examples/certificates.example';
import { SurviesArrayExample } from '../../surveys/examples/survies.example';

@Table({tableName: 'programs'})
export class ProgramsModel extends Model<ProgramsModel> {

    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelProperty()
    @ForeignKey(() => Categories)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'category_id',
    })
    categoriesId: string;

    @ApiModelProperty()
    @HasMany(() => ProgramDescriptionModel)
    programDescription: ProgramDescriptionModel[];
    
    @ApiModelProperty()
    @HasMany(() => UserProgramsModel)
    userPrograms: UserProgramsModel[];

    @ApiModelProperty()
    @Column({
        type: DataType.STRING,
        allowNull: true,
        validate: {
            notEmpty: true,
        },
        field: 'program_code',
    })
    programCode: string;

    @ApiModelProperty()
    @Column({
        type: DataType.ENUM('1', '2', '3'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'program_status',
    })
    programStatus: string;

    @ApiModelProperty()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'program_background',
    })
    programBackground: string;

    @ApiModelProperty()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'program_icon',
    })
    programIcon: string;

    @ApiModelProperty()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'fb_promotion_img',
    })
    fbPromotionImg: string;

    /**
     * RELACIONES
     * Programs tiene muchos Surveys
     */
    @ApiModelProperty({example: SurviesArrayExample})
    @BelongsToMany(()=> Surveys, () => SurveysHasPrograms)
    surveys: Surveys[];

/**
     * RELACIONES
     * Programs pertenece a Categories
    */
    
    @BelongsTo(() => Categories)
    categories: Categories;

    @ApiModelProperty({example: MainVideosArrayExample})
    @BelongsToMany(() => MainVideosModel, () => ProgramHasVideosModel)
    mainVideos: MainVideosModel[];
    
    @ApiModelProperty({example: CoursesArrayExample})
    @BelongsToMany(() => CoursesModel, () => ProgramHasCoursesModel)
    courses: CoursesModel[];
    @ApiModelProperty({ example: CertificatesArrayExample})
    @BelongsToMany(() => Certificates, {through: { model: () => ProgramsHasCertificates, unique: true}})
    certificates: Certificates[];

    // tslint:disable-next-line: variable-name
    @ApiModelProperty()
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @ApiModelProperty()
    @UpdatedAt updated_at: Date;
}
