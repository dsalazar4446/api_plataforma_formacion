import { Table, Model, ForeignKey, AllowNull, Validate, Unique, Column, DataType, CreatedAt, UpdatedAt } from 'sequelize-typescript';
import { ProgramsModel } from './programs.model';
import { MainVideosModel } from '../../main-videos/models/main-videos.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({ tableName: 'programs_has_main_videos' })
export class ProgramHasVideosModel extends Model<ProgramHasVideosModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => ProgramsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'program_id',
    })
    programsId: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => MainVideosModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'main_videos_id'
    })
    mainVideosId: string;

    // tslint:disable-next-line: variable-name
    @CreatedAt public created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt public updated_at: Date;

}