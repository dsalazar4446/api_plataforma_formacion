// tslint:disable-next-line: max-line-length
import { Table, Model, PrimaryKey, AutoIncrement, AllowNull, Validate, Unique, Column, DataType, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from "sequelize-typescript";
import { ProgramsModel } from './programs.model';
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({ tableName: 'program_descriptions' })

export class ProgramDescriptionModel extends Model<ProgramDescriptionModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => ProgramsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'program_id',
    })
    programsId: string;

    @BelongsTo(() => ProgramsModel)
    programs: ProgramsModel;

    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'program_title',
    })
    programTitle: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'program_detail',
    })
    programDetail: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;
}