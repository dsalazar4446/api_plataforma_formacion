// tslint:disable-next-line: max-line-length
import { Table, Model, PrimaryKey, AutoIncrement, AllowNull, Validate, Unique, Column, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt, HasMany } from 'sequelize-typescript';
import { ProgramsModel } from './programs.model';
import { UserModel } from './../../user/models/user.Model';
import { UserProgramRatesModel } from './user-program-rates.model';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { UserProgrmasRatesArrayExample } from '../examples/user-programs-rates.example';

@Table({tableName: 'user_programs'})

export class UserProgramsModel extends Model<UserProgramsModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    usersId: string;

    @BelongsTo(() => UserModel)
    user: UserModel;

    @ApiModelPropertyOptional()
    @ForeignKey(() => ProgramsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
        field: 'program_id',
    })
    programsId: string;

    @BelongsTo(() => ProgramsModel)
    program: ProgramsModel;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2', '3'),
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_program_status',
    })
    userProgramStatus: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_program_favorite',
    })
    userProgramFavorite: boolean;

    @ApiModelPropertyOptional({ example: UserProgrmasRatesArrayExample })
    @HasMany(() => UserProgramRatesModel)
    userProgramRates: UserProgramRatesModel[];
     // tslint:disable-next-line: variable-name
     @CreatedAt created_at: Date;
     // tslint:disable-next-line: variable-name
     @UpdatedAt updated_at: Date;
}