// tslint:disable-next-line: max-line-length
import { Table, Model, PrimaryKey, AutoIncrement, AllowNull, Validate, Unique, Column, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt } from 'sequelize-typescript';
import { UserProgramsModel } from './user-programs.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({tableName: 'user_program_rates'})

export class UserProgramRatesModel extends Model<UserProgramRatesModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserProgramsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_program_id'
    })
    userProgramsId: string;

    @BelongsTo(() => UserProgramsModel, 'user_program_id')
    userPrograms: UserProgramsModel;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_program_rate',
    })
    userProgramRate: number;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_program_comments',
    })
    userProgramComments: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_program_status',
    })
    userProgramRateStatus: boolean;

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;
}
