import { Table, Model, ForeignKey, AllowNull, Validate, Unique, Column, DataType, CreatedAt, UpdatedAt, Sequelize } from 'sequelize-typescript';
import { ProgramsModel } from './programs.model';
import { CoursesModel } from '../../courses/models/courses.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({ tableName: 'programs_has_courses' })
export class ProgramHasCoursesModel extends Model<ProgramHasCoursesModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => ProgramsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        // references: {
        //     model: typeof ProgramsModel,
        //     key: 'id',
        //     deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
        // },
        field: 'program_id',
    })
    programsId: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => CoursesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        // references: {
        //     model: typeof CoursesModel,
        //     key: 'id',
        //     deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
        // },
        field: 'course_id',
    })
    coursesId: string;

    // tslint:disable-next-line: variable-name
    @CreatedAt public created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt public updated_at: Date;

}