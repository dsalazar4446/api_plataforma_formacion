import { Module, forwardRef } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { ProgramsModel } from './models/programs.model';
import { ProgramsService } from './services/programs/programs.service';
import { ProgramDescriptionsService } from './services/program-descriptions/program-descriptions.service';
import { ProgramDescriptionsController } from './controllers/program-descriptions/program-descriptions.controller';
import { ProgramsController } from './controllers/programs/programs.controller';
import { ProgramDescriptionModel } from './models/program-descriptions.model';
import { ProgramHasCoursesService } from './services/program-has-courses/program-has-courses.service';
import { ProgramHasVideosService } from './services/program-has-videos/program-has-videos.service';
import { UserProgramService } from './services/user-program/user-program.service';
import { UserProgramRatesService } from './services/user-program-rates/user-program-rates.service';
import { ProgramHasCoursesController } from './controllers/program-has-courses/program-has-courses.controller';
import { ProgramHasVideosController } from './controllers/program-has-videos/program-has-videos.controller';
import { UserProgramsController } from './controllers/user-programs/user-programs.controller';
import { UserProgramRatesController } from './controllers/user-program-rates/user-program-rates.controller';
import { ProgramHasCoursesModel } from './models/program-has-courses';
import { ProgramHasVideosModel } from './models/programs-has-videos.model';
import { UserProgramRatesModel } from './models/user-program-rates.model';
import { UserProgramsModel } from './models/user-programs.model';


const models = [
    ProgramsModel,
    ProgramDescriptionModel,
    ProgramHasCoursesModel,
    ProgramHasVideosModel,
    UserProgramRatesModel,
    UserProgramsModel,
];

@Module({
    imports: [
        DatabaseModule.forRoot({
            sequelize: {
                models,
            },
            loki: true,
        }),
    ],
    providers: [ProgramsService, ProgramDescriptionsService, ProgramHasCoursesService, ProgramHasVideosService, UserProgramService, UserProgramRatesService],
    controllers: [ProgramsController, ProgramDescriptionsController, ProgramHasCoursesController, ProgramHasVideosController, UserProgramsController, UserProgramRatesController],
})
export class ProgramsModule { }
