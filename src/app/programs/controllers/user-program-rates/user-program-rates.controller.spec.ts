import { Test, TestingModule } from '@nestjs/testing';
import { UserProgramRatesController } from './user-program-rates.controller';

describe('UserProgramRates Controller', () => {
  let controller: UserProgramRatesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserProgramRatesController],
    }).compile();

    controller = module.get<UserProgramRatesController>(UserProgramRatesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
