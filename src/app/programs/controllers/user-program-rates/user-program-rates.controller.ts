import { Controller, UseInterceptors, UseFilters, UseGuards, Post, UsePipes, Body, Get, Param, Put, Delete, Req } from '@nestjs/common';
import { UpdateUserProgramsRatesDto, CreateUserProgramRatesDto } from '../../dto/user-program-rates';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { UserProgramRatesService } from '../../services/user-program-rates/user-program-rates.service';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { UserProgramRatesModel } from '../../models/user-program-rates.model';

@ApiUseTags('Programs')
@Controller('user-program-rates')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // // @UseGuards(new JwtAuthGuard())
export class UserProgramRatesController {
    constructor(private readonly userProgramRatesServices: UserProgramRatesService) {}

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: UserProgramRatesModel })
    create(@Body() body: CreateUserProgramRatesDto, @Req() req) {``
        body.languageType = req.headers.language;
        return this.userProgramRatesServices.create(body);
    }
    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserProgramRatesModel, isArray: true })
    list(@Req() req) {
        return this.userProgramRatesServices.list(req.headers.language);
    }
    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserProgramRatesModel })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    detail(@Param('id') id,@Req() req) {
        return this.userProgramRatesServices.detail(id, req.headers.language);
    }
    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserProgramRatesModel })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    update(@Param('id') id: string, @Body() body: Partial<UpdateUserProgramsRatesDto>, @Req() req) {
        body.languageType = req.headers.language;
        return this.userProgramRatesServices.update(id, body);
    }
    @Delete(':id')
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    delete(@Param('id') id: string) {
        return this.userProgramRatesServices.delete(id);
    }
}
