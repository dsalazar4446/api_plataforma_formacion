import { Controller, UseInterceptors, UseFilters, UseGuards, Put, Delete, Post, Get, UsePipes, Body, Param, Req } from '@nestjs/common';
import { ProgramDescriptionsService } from '../../services/program-descriptions/program-descriptions.service';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { CreateProgramDescriptionDto, UpdateProgramDescriptionDto } from '../../dto/program-description.dto';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { ProgramDescriptionModel } from '../../models/program-descriptions.model';
import { UserProgramsModel } from '../../models/user-programs.model';
import { language } from 'googleapis/build/src/apis/language';

@ApiUseTags('Programs')
@Controller('program-descriptions')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // // @UseGuards(new JwtAuthGuard())
export class ProgramDescriptionsController {
    constructor( private readonly descriptionService: ProgramDescriptionsService) {}

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: ProgramDescriptionModel })
    @ApiResponse({ status: 201, type: UserProgramsModel })
    create(@Body() body: CreateProgramDescriptionDto, @Req() req) {
        body.languageType = req.headers.language;
        return this.descriptionService.create(body);
    }
    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: ProgramDescriptionModel, isArray: true })
    list(@Req() req) {
        return this.descriptionService.list(req.headers.language);
    }
    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: ProgramDescriptionModel })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    detail(@Param('id') id: string, @Req() req) {
        return this.descriptionService.detail(id, req.headers.language);
    }

    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: ProgramDescriptionModel })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    update(@Param('id') id: string, @Body() body: Partial<UpdateProgramDescriptionDto>, @Req() req){
        body.languageType = req.headers.language;
        return this.descriptionService.update(id, body);
    }
    @Delete(':id')
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    delete(@Param('id') id) {
        return this.descriptionService.delete(id);
    }

}
