import { Test, TestingModule } from '@nestjs/testing';
import { ProgramDescriptionsController } from './program-descriptions.controller';

describe('ProgramDescriptions Controller', () => {
  let controller: ProgramDescriptionsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProgramDescriptionsController],
    }).compile();

    controller = module.get<ProgramDescriptionsController>(ProgramDescriptionsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
