import { Controller, Body, UsePipes, Post, UseInterceptors, FileFieldsInterceptor, Param, Get, Put, Delete, UseFilters, UseGuards, Headers, Header, Query } from '@nestjs/common';
import { ProgramsService } from '../../services/programs/programs.service';
import { CreateProgramDto, UpdateProgramDto } from '../../dto/program.dto';
import { extname } from 'path';
import { diskStorage } from 'multer';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiConsumes, ApiImplicitFile, ApiResponse, ApiResponseModelProperty, ApiImplicitHeader, ApiImplicitHeaders, ApiImplicitQuery } from '@nestjs/swagger';
import { ProgramsModel } from '../../models/programs.model';
import { Surveys } from '../../../surveys/models/surveys.models';
import { language } from 'googleapis/build/src/apis/language';

@ApiUseTags('Programs')
@Controller('programs')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // // @UseGuards(new JwtAuthGuard())
export class ProgramsController {
    constructor(private readonly programsService: ProgramsService) {}

    @Post()
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'programBackground', description: 'Background del programa', required: true })
    @ApiImplicitFile({ name: 'programIcon', description: 'icono del programa', required: true })
    @ApiImplicitFile({ name: 'fbPromotionImg', description: 'promocion de facebook del programa', required: true })
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'programBackground', maxCount: 1 },
        { name: 'programIcon', maxCount: 1 },
        { name: 'fbPromotionImg', maxCount: 1 },
        ], {
        storage: diskStorage({ 
        destination: './uploads'
            , filename: (req, file, cb) => {
                let extValidas = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG'];

                if (extValidas.indexOf(extname(file.originalname)) < 0) {
                    cb('valid extensions: ' + extValidas.join(', '));
                    return
                }
                const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                if (file.fieldname === 'programBackground'){
                    req.body.programBackground = `${randomName}${extname(file.originalname)}`;
                }
                if (file.fieldname === 'programIcon'){
                    req.body.programIcon = `${randomName}${extname(file.originalname)}`;
                }
                if (file.fieldname === 'fbPromotionImg'){
                    req.body.fbPromotionImg = `${randomName}${extname(file.originalname)}`;
                }
                cb(null, `${randomName}${extname(file.originalname)}`);
            },
        }),
    }))
    async create(@Body() body: CreateProgramDto) {
        return await this.programsService.create(body);
    }

    @Get()
    @ApiResponse({ status: 200, type: ProgramsModel, isArray: true})
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiImplicitQuery({name: 'page', required: true})
    async list(@Query() query ,@Headers('language') language: string) {
        return this.programsService.list(language,query.page, query.programTitle, query.programStatus);
    }
    @Get('admin')
    @ApiResponse({status: 200,type:ProgramsModel, isArray: true})
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiImplicitQuery({name: 'page', required: true})
    async listAdmin(@Query() query,@Headers('language') language: string) {
        return this.programsService.listAdmin(query.page, language, query.programTitle);
    }
    @Get(':id')
    @ApiResponse({status: 200,type:ProgramsModel})
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponseModelProperty()
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    async detail(@Param('id') id: string, @Headers('language') language : string){
        return this.programsService.detail(id, language);
    }
    @Get('admin/:id/')
    @ApiResponse({status: 200,type:ProgramsModel})
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detailAdmin(@Param('id') id: string, @Headers('language') language: string) {
        return this.programsService.detailAdmin(id, language);
    }
    @Get('charts/:userId')
    @ApiResponse({status: 200,type:ProgramsModel})
    charts(@Param('userId') userId: string){
        return this.programsService.charts(userId);
    }
    @Put(':id')
    @ApiResponse({status: 200,type:ProgramsModel})
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'programBackground', description: 'Background del programa', required: true })
    @ApiImplicitFile({ name: 'programIcon', description: 'icono del programa', required: true })
    @ApiImplicitFile({ name: 'fbPromotionImg', description: 'promocion de facebook del programa', required: true })
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'programBackground', maxCount: 1 },
        { name: 'programIcon', maxCount: 1 },
        { name: 'fbPromotionImg', maxCount: 1 },
        ], {
        storage: diskStorage({
        destination: './uploads/programs'
            , filename: (req, file, cb) => {
                let extValidas = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG'];

                if (extValidas.indexOf(extname(file.originalname)) < 0) {
                    cb('valid extensions: ' + extValidas.join(', '));
                    return
                }
                const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                if(file.fieldname === 'programBackground') {
                    req.body.programBackground = `${randomName}${extname(file.originalname)}`;
                }
                if(file.fieldname === 'programIcon') {
                    req.body.programIcon = `${randomName}${extname(file.originalname)}`;
                }
                if(file.fieldname === 'fbPromotionImg') {
                    req.body.fbPromotionImg = `${randomName}${extname(file.originalname)}`;
                }
                cb(null, `${randomName}${extname(file.originalname)}`);
            },
        }),
    }))
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateProgramDto>) {
        return await this.programsService.update(id, body);
    }

    @Delete(':id')
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    delete(@Param('id') id: string) {
        return this.programsService.delete(id);
    }


}
