import { Controller, UseInterceptors, UseFilters, UseGuards, Post, UsePipes, Body, Delete, Param } from '@nestjs/common';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { ProgramHasVideosService } from '../../services/program-has-videos/program-has-videos.service';
import { CreateProgramHasVideos } from '../../dto/programs-has-videos.dto';

@Controller('programs/program-has-videos')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // // @UseGuards(new JwtAuthGuard())
export class ProgramHasVideosController {
    constructor(private readonly programVideosService: ProgramHasVideosService) {}

    @Post()
    create(@Body() body: CreateProgramHasVideos){
        return this.programVideosService.create(body);
    }

    @Delete(':id')
    delete(@Param('id') id: string){
        return this.programVideosService.delete(id);
    }
}
