import { Test, TestingModule } from '@nestjs/testing';
import { ProgramHasVideosController } from './program-has-videos.controller';

describe('ProgramHasVideos Controller', () => {
  let controller: ProgramHasVideosController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProgramHasVideosController],
    }).compile();

    controller = module.get<ProgramHasVideosController>(ProgramHasVideosController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
