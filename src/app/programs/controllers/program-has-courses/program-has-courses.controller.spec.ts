import { Test, TestingModule } from '@nestjs/testing';
import { ProgramHasCoursesController } from './program-has-courses.controller';

describe('ProgramHasCourses Controller', () => {
  let controller: ProgramHasCoursesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProgramHasCoursesController],
    }).compile();

    controller = module.get<ProgramHasCoursesController>(ProgramHasCoursesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
