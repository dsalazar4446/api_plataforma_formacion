import { Controller, UseInterceptors, UseFilters, UseGuards, UsePipes, Post, Body, Delete, Param } from '@nestjs/common';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { CreateProgramHasCourses } from '../../dto/program-has-courses.dto';
import { ProgramHasCoursesService } from '../../services/program-has-courses/program-has-courses.service';

@Controller('programs/program-has-courses')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // // @UseGuards(new JwtAuthGuard())
export class ProgramHasCoursesController {
    constructor(private readonly programCoursesService: ProgramHasCoursesService) {}

    @Post()
    create(@Body() body: CreateProgramHasCourses){
        return this.programCoursesService.create(body);
    }

    @Delete(':id')
    delete(@Param('id') id: string){
        return this.programCoursesService.delete(id);
    }
}
