import { Test, TestingModule } from '@nestjs/testing';
import { UserProgramsController } from './user-programs.controller';

describe('UserPrograms Controller', () => {
  let controller: UserProgramsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserProgramsController],
    }).compile();

    controller = module.get<UserProgramsController>(UserProgramsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
