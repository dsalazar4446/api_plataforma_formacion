import { Controller, Post, Body, UsePipes, Param, Get, Put, Delete, UseInterceptors, UseFilters, UseGuards } from '@nestjs/common';
import { UserProgramService } from '../../services/user-program/user-program.service';
import { CreateUserProgramsDto, UpdateUserProgramsDto } from '../../dto/user-programs.dto';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { UserProgramsModel } from '../../models/user-programs.model';

@ApiUseTags('Programs')
@Controller('user-programs')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // @UseGuards(new JwtAuthGuard())
export class UserProgramsController {
    constructor(private readonly userProgramServices: UserProgramService) {}

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: UserProgramsModel })
    create(@Body() body: CreateUserProgramsDto) {
        return this.userProgramServices.create(body);
    }
    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserProgramsModel, isArray: true })
    list() {
        return this.userProgramServices.list();
    }
    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserProgramsModel})
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    detail(@Param('id') id) {
        return this.userProgramServices.detail(id);
    }
    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserProgramsModel})
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    update(@Param('id') id: string, @Body() body: Partial<UpdateUserProgramsDto>) {
        return this.userProgramServices.update(id, body);
    }
    @Delete(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserProgramsModel, isArray: true })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    delete(@Param('id') id: string) {
        return this.userProgramServices.delete(id);
    }
}
