export const UserProgrmasRatesExample = 
{
    "id": "string",
    "userProgramsId": "string",
    "languageType": "string",
    "userProgramRate": "number",
    "userProgramComments": "string",
    "userProgramRateStatus": "boolean"
}

export const UserProgrmasRatesArrayExample = 
[
    UserProgrmasRatesExample
]
