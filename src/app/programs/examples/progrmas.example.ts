export const ProgramsExample = 
{
    "id": "string",
    "categoriesId": "string",
    "programCode": "string",
    "programStatus": "string",
    "programBackground": "string",
    "programIcon": "string",
    "fbPromotionImg": "string",
}

export const ProgramsArrayExample = 
[
    ProgramsExample
]
