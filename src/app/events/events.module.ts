import { Module, HttpModule, Provider } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { EventAttachments } from './models/event-attachments.model';
import { EventAttendances } from './models/event-attendances.model'
import { EventDetails} from './models/event-details.model'
import { Events } from './models/events.model'


import { EventAttachmentsServices} from './services/event-attachments.services';
import { EventAttendancesServices} from './services/event-attendances.services';
import { EventDetailsService} from './services/event-details.services';
import { EventsServices } from './services/events.services';


import { EventAttachmentsController } from './controllers/event-attachments.controller';
import { EventAttendancesController} from './controllers/event-attendances.controller';
import { EventDetailsController } from './controllers/event-details.controller';
import { EventsController } from './controllers/events.controller';

const models = [
  EventAttachments, EventAttendances, EventDetails, Events
];

const providers: Provider[] = [
  EventAttachmentsServices, EventAttendancesServices, EventDetailsService, EventsServices
];

const controllers = [
  EventAttachmentsController, EventAttendancesController, EventDetailsController, EventsController
];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers,
  exports: [EventsServices],
})
export class EventsModule { }
