import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsBoolean, IsNumber, IsString, IsOptional, IsDate } from 'class-validator';
export class EventsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    createByUserId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'cityId property must a be uuid'})
    @IsOptional()
    cityId?: string;

    @ApiModelProperty()
    @IsBoolean({message: 'requiredRegistration property must a be boolean'})
    @IsNotEmpty({message: 'requiredRegistration property not must null'})
    requiredRegistration: boolean;

    @ApiModelProperty()
    @IsBoolean({message: 'requiredPayment property must a be boolean'})
    @IsNotEmpty({message: 'requiredPayment property not must null'})
    requiredPayment: boolean;

    @ApiModelProperty()
    @IsNumber(null,{message: 'eventValue property must a be number'})
    @IsOptional()
    eventValue: number;

    @ApiModelProperty()
    @IsString({message: 'eventType property must a be string'})
    @IsNotEmpty({message: 'eventType property not must null'})
    eventType: string;

    @ApiModelProperty()
    @IsDate()
    @IsNotEmpty({message: 'eventTimeStart property not must null'})
    eventTimeStart: Date;

    @ApiModelProperty()
    @IsDate()
    @IsNotEmpty({message: 'eventTimeEnd property not must null'})
    eventTimeEnd: Date;

    @ApiModelProperty()
    @IsString({message: 'eventLatitude property must a be string'})
    @IsOptional()
    eventLatitude?: string;

    @ApiModelProperty()
    @IsString({message: 'eventLongitude property must a be string'})
    @IsOptional()
    eventLongitude?: string;

    @ApiModelProperty()
    @IsString({message: 'eventAddress1 property must a be string'})
    @IsOptional()
    eventAddress1?: string;

    @ApiModelProperty()
    @IsString({message: 'eventAddress2 property must a be string'})
    @IsOptional()
    eventAddress2?: string;

    @ApiModelProperty()
    @IsString({message: 'eventStatus property must a be string'})
    @IsNotEmpty({message: 'eventStatus property not must null'})
    eventStatus: string;
    
    @ApiModelProperty()
    @IsNumber()
    @IsNotEmpty({ message: 'eventStatus property not must null' })
    eventOdooId: number;

}
export class EventsUpdateJson extends EventsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
