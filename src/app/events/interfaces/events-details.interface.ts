import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional } from 'class-validator';
export class EventDetailsSaveJson {
    
    @ApiModelProperty()
    @IsUUID('4',{message: 'eventId property must a be uuid'})
    @IsNotEmpty({message: 'eventId property not must null'})
    eventId: string;

    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsString({message: 'languageType property must a be string'})
    @IsOptional()
    languageType: string;

    @ApiModelProperty()
    @IsString({message: 'eventTitle property must a be string'})
    @IsNotEmpty({message: 'eventTitle property not must null'})
    eventTitle: string;

    @ApiModelProperty()
    @IsString({message: 'eventDescription property must a be string'})
    @IsNotEmpty({message: 'eventDescription property not must null'})
    eventDescription: string;
}
export class EventDetailsUpdateJson extends EventDetailsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
