import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString } from 'class-validator';
export class EventAttendancesSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    userId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'eventId property must a be uuid'})
    @IsNotEmpty({message: 'eventId property not must null'})
    eventId: string;

    @ApiModelProperty()
    @IsString({message: 'eventAttendancesStatus property must a be string'})
    @IsNotEmpty({message: 'eventAttendancesStatus property not must null'})
    eventAttendancesStatus: string;
}
export class EventAttendancesUpdateJson extends EventAttendancesSaveJson{
    
    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
