import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional } from 'class-validator';
export class EventAttachmentsSaveJson {

    @ApiModelProperty()

    @IsUUID('4',{message: 'eventId property must a be uuid'})
    @IsNotEmpty({message: 'eventId property not must null'})
    eventId: string;

    @ApiModelProperty()
    @IsString({message: 'eventAttachmentType property must a be string'})
    @IsNotEmpty({message: 'eventAttachmentType property not must null'})
    eventAttachmentType: string;

    @ApiModelProperty()
    @IsString({message: 'eventAttachment property must a be string'})
    @IsNotEmpty({message: 'eventAttachment property not must null'})
    eventAttachment: string;
}
export class EventAttachmentsUpdateJson extends EventAttachmentsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
