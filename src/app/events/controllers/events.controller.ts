import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, Param, NotFoundException, Put, Delete, Req } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { EventsServices } from '../services/events.services';
import { EventsSaveJson } from '../interfaces/events.interface';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { Events } from "../models/events.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller EventsController
 * @Creado 02 Abril 2019
 */

@ApiUseTags('Module-Events')
@Controller('events')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class EventsController {

    constructor(private readonly _eventsServcs: EventsServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: Events,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyEvents: EventsSaveJson) {

        const createEvents = await this._eventsServcs.saveEvents(bodyEvents);

        if (!createEvents) {
            throw this.appUtilsService.httpCommonError('Event no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM41',
                languageType: 'es',
            });
        }

        return createEvents;

    }


    @ApiResponse({
        status: 200,
        type: Events,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAll() {
        const fetchAll = await this._eventsServcs.find();
        if (!fetchAll[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetchAll;
    }


    @ApiResponse({
        status: 200,
        type: Events,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idEvents: string) {

        const fetchEvents = await this._eventsServcs.getDetails(idEvents);

        if (!fetchEvents) {
            throw this.appUtilsService.httpCommonError('Event Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM42',
                languageType: 'es',
            });
        }

        return fetchEvents;
    }


    @ApiResponse({
        status: 200,
        type: Events,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get(':eventStatus/:eventType?')
    @ApiImplicitParam({ name: 'eventType', required: false, type: 'string' })
    @ApiImplicitParam({ name: 'eventStatus', required: true, type: 'string' })
    async showAllNews(@Req() req, @Param('eventStatus') eventStatus: string, @Param('eventType') eventType?: string) {

        const fetchAll = await this._eventsServcs.showAllEvents(req.headers.language, eventStatus, eventType);

        if (!fetchAll[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetchAll;
    }


    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateEvents(@Param('id') id: string, @Body() bodyEvents: Partial<EventsSaveJson>) {

        const updateEvents = await this._eventsServcs.updateEvents(id, bodyEvents);

        if (!updateEvents[1][0]) {
            throw this.appUtilsService.httpCommonError('Event Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM41',
                languageType: 'es',
            });
        }

        return updateEvents;
    }

    
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteEvents(@Param('id') idEvents: string) {

        const deleteEvents = await this._eventsServcs.destroyEvents(idEvents);

        if (!deleteEvents) {
            throw this.appUtilsService.httpCommonError('Event Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM41',
                languageType: 'es',
            });
        }

        return deleteEvents;
    }
}