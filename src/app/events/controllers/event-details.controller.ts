import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, NotFoundException, Put, Delete, Param, Req } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { EventDetailsService } from "../services/event-details.services";
import { EventDetailsSaveJson } from "../interfaces/events-details.interface";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { EventDetails } from "../models/event-details.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller EventDetailsController
 * @Creado 02 Abril 2019
 */

@ApiUseTags('Module-Events')
@Controller('event-details')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class EventDetailsController {

    constructor(private readonly _eventDetailsServcs: EventDetailsService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: EventDetails,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Req() req, @Body() bodyeDetails: EventDetailsSaveJson) {

        if (req.headers.language) {
            bodyeDetails.languageType = req.headers.language;
        } else {
            bodyeDetails.languageType = 'es';
        }

        const createeDetails = await this._eventDetailsServcs.saveeDetails(bodyeDetails);

        if (!createeDetails) {
            throw this.appUtilsService.httpCommonError('Event Details no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM39',
                languageType: 'es',
            });
        }

        return createeDetails;

    }


    @ApiResponse({
        status: 200,
        type: EventDetails,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAll() {
        const fetchAll = await this._eventDetailsServcs.find();
        if (!fetchAll) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetchAll;
    }


    @ApiResponse({
        status: 200,
        type: EventDetails,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') ideDetails: string) {

        const fetcheDetails = await this._eventDetailsServcs.getDetails(ideDetails);

        if (!fetcheDetails) {
            throw this.appUtilsService.httpCommonError('Event Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM40',
                languageType: 'es',
            });
        }

        return fetcheDetails;
    }


    @ApiResponse({
        status: 200,
        type: EventDetails,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAlleDeatils(@Req() req) {

        const fetch = await this._eventDetailsServcs.showAllEDetails(req.headers.language);

        if (!fetch) {
            throw this.appUtilsService.httpCommonError('Language does not exist!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM3',
                languageType: 'es',
            });
        }

        return fetch;
    }


    @ApiResponse({
        status: 200,
        type: EventDetails,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateeDetails(@Req() req, @Param('id') id: string, @Body() bodyeDetails: Partial<EventDetailsSaveJson>) {

        if (req.headers.language) bodyeDetails.languageType = req.headers.language;

        const updateeDetails = await this._eventDetailsServcs.updateeDetails(id, bodyeDetails);

        if (!updateeDetails[1][0]) {
            throw this.appUtilsService.httpCommonError('Event Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM40',
                languageType: 'es',
            });
        }

        return updateeDetails;
    }


    @ApiResponse({
        status: 200,
        type: EventDetails,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteeDetails(@Param('id') ideDetails: string) {

        const deleteeDetails = await this._eventDetailsServcs.destroyeDetails(ideDetails);

        if (!deleteeDetails) {
            throw this.appUtilsService.httpCommonError('Event Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM40',
                languageType: 'es',
            });
        }

        return deleteeDetails;
    }
}