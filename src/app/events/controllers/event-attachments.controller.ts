import { Controller, UseFilters, UseInterceptors, Post, Body, HttpStatus, Get, NotFoundException, Put, Delete, FileFieldsInterceptor, Param } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { EventAttachmentsServices } from "../services/event-attachments.services";
import { EventAttachmentsSaveJson } from "../interfaces/events-attachments.interface";
import { CONFIG } from "../../../config";
import { ApiUseTags, ApiConsumes, ApiImplicitParam, ApiImplicitFile, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import * as file from './infoFile';
import { EventAttachments } from "../models/event-attachments.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller EventAttachmentsController
 * @Creado 26 de Marzo 2019
 */

@ApiUseTags('Module-Events')
@Controller('event-attachments')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class EventAttachmentsController {

    constructor(private readonly _attachServcs: EventAttachmentsServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: EventAttachments,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'eventAttachment', description: 'Event Attachment File', required: true })
    async create(@Body() bodyAttach: EventAttachmentsSaveJson) {

        const createAttach = await this._attachServcs.saveAttachments(bodyAttach);

        if (!createAttach) {
            throw this.appUtilsService.httpCommonError('Event Attachments no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM35',
                languageType: 'es',
            });
        }

        if (createAttach.eventAttachmentType == '1') return createAttach;
        createAttach.eventAttachment = `${CONFIG.storage.server}eventAttachment/${createAttach.eventAttachment}`;
        return createAttach;

    }


    @ApiResponse({
        status: 200,
        type: EventAttachments,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAll() {

        const fetchAll = await this._attachServcs.find();

        if (!fetchAll) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        fetchAll.forEach(fetch => {
            if (fetch.eventAttachmentType == '1') return;
            fetch.eventAttachment = `${CONFIG.storage.server}eventAttachment/${fetch.eventAttachment}`;
        })

        return fetchAll;
    }


    @ApiResponse({
        status: 200,
        type: EventAttachments,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idAttach: string) {

        const fetchAttach = await this._attachServcs.getDetails(idAttach);

        if (!fetchAttach) {
            throw this.appUtilsService.httpCommonError('Event Attachments no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM36',
                languageType: 'es',
            });
        }
        if (fetchAttach.eventAttachmentType == '1') return fetchAttach;
        fetchAttach.eventAttachment = `${CONFIG.storage.server}eventAttachment/${fetchAttach.eventAttachment}`;

        return fetchAttach;
    }


    @ApiResponse({
        status: 200,
        type: EventAttachments,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get(':eventAttachmentType')
    @ApiImplicitParam({ name: 'eventAttachmentType', required: false, type: 'string' })
    async showAllAttach(@Param('eventAttachmentType') eventAttachmentType: string) {

        const fetch = await this._attachServcs.showAllAttach(eventAttachmentType);
        if (!fetch) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        fetch.forEach(fetch => {

            if (fetch.eventAttachmentType == '1') return;
            fetch.eventAttachment = `${CONFIG.storage.server}eventAttachment/${fetch.eventAttachment}`;
        });

        return fetch;
    }


    @ApiResponse({
        status: 200,
        type: EventAttachments,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'eventAttachment', description: 'Event Attachment File', required: true })
    async updateAttach(@Param('id') id: string, @Body() bodyAttach: Partial<EventAttachmentsSaveJson>) {

        const updateAttach = await this._attachServcs.updateAttach(id, bodyAttach);

        if (!updateAttach[1][0]) {
            throw this.appUtilsService.httpCommonError('Event Attachments no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM36',
                languageType: 'es',
            });
        }
        if (updateAttach[1][0].eventAttachmentType == '1') return updateAttach;
        updateAttach[1][0].eventAttachment = `${CONFIG.storage.server}eventAttachment/${updateAttach[1][0].eventAttachment}`;

        return updateAttach;
    }


    @ApiResponse({
        status: 200,
        type: EventAttachments,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteAttach(@Param('id') idAttach: string) {

        const deleteAttach = await this._attachServcs.destroyAttach(idAttach);

        if (!deleteAttach) {
            throw this.appUtilsService.httpCommonError('Event Attachments no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM36',
                languageType: 'es',
            });
        }

        return deleteAttach;
    }
}