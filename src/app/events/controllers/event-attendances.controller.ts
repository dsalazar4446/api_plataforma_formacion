import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, NotFoundException, Put, Delete, Param } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { EventAttendancesServices } from "../services/event-attendances.services";
import { EventAttendancesSaveJson } from "../interfaces/events-attendances.interface";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { EventAttendances } from "../models/event-attendances.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller EventAttendancesController
 * @Creado 26 de Marzo 2019
 */

@ApiUseTags('Module-Events')
@Controller('event-attendances')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class EventAttendancesController {

    constructor(private readonly _attenServcs: EventAttendancesServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: EventAttendances,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyAtten: EventAttendancesSaveJson) {

        const createNews = await this._attenServcs.saveEventAttendances(bodyAtten);

        if (!createNews) {
            throw this.appUtilsService.httpCommonError('Event Attendances no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM37',
                languageType: 'es',
            });
        }

        return createNews;

    }


    @ApiResponse({
        status: 200,
        type: EventAttendances,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAll() {
        const fetchAll = await this._attenServcs.find();

        if (!fetchAll) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetchAll;
    }


    @ApiResponse({
        status: 200,
        type: EventAttendances,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idAtten: string) {

        const fetchAtten = await this._attenServcs.getDetails(idAtten);

        if (!fetchAtten) {
            throw this.appUtilsService.httpCommonError('Event Attendances no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM38',
                languageType: 'es',
            });
        }

        return fetchAtten;
    }


    @ApiResponse({
        status: 200,
        type: EventAttendances,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get(':eventAttendancesStatus')
    @ApiImplicitParam({ name: 'eventAttendancesStatus', required: false, type: 'string' })
    async showAllAtten(@Param('eventAttendancesStatus') eventAttendancesStatus: string) {

        const fetch = await this._attenServcs.showAllAttendances(eventAttendancesStatus);
        if (!fetch) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return fetch;
    }


    @ApiResponse({
        status: 200,
        type: EventAttendances,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateAtten(@Param('id') id: string, @Body() bodyAtten: Partial<EventAttendancesSaveJson>) {

        const updateAtten = await this._attenServcs.updateAttendances(id, bodyAtten);

        if (!updateAtten[1][0]) {
            throw this.appUtilsService.httpCommonError('Event Attendances no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM38',
                languageType: 'es',
            });
        }

        return updateAtten;
    }


    @ApiResponse({
        status: 200,
        type: EventAttendances,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteAtten(@Param('id') idAtten: string) {

        const deleteAtten = await this._attenServcs.destroyAttendances(idAtten);

        if (!deleteAtten) {
            throw this.appUtilsService.httpCommonError('Event Attendances no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM38',
                languageType: 'es',
            });
        }

        return deleteAtten;
    }


}