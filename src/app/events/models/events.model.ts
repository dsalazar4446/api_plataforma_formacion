import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo, HasMany, BelongsToMany } from 'sequelize-typescript';
import { UserModel } from './../../user/models/user.Model';
import { EventDetails } from './event-details.model';
import { EventAttachments } from './event-attachments.model';
import { EventAttendances } from './event-attendances.model';
import { TransactionsItemsHasEvents } from '../../transactions/models/transactions-items-has-events.model';
import { TransactionItemsModel } from '../../transactions/models/transaction-items.model';
import { CityModel } from '../../city/models/city.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'events',
})
export class Events extends Model<Events>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    createByUserId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => CityModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: true,
        validate: {
            notEmpty: false,
        },
        field: 'city_id',
    })
    cityId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'required_registration',
    })
    requiredRegistration: boolean;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'required_payment',
    })
    requiredPayment: boolean;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.FLOAT,
        defaultValue: 0,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'event_value',
    })
    eventValue: number;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'event_type',
    })
    eventType: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'event_time_start',
    })
    eventTimeStart: Date;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'event_time_end',
    })
    eventTimeEnd: Date;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(128),
        allowNull: true,
        validate: {
            notEmpty: false,
        },
        field: 'event_latitude',
    })
    eventLatitude: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(128),
        allowNull: true,
        validate: {
            notEmpty: false,
        },
        field: 'event_longitude',
    })
    eventLongitude: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        validate: {
            notEmpty: false,
        },
        field: 'event_address_1',
    })
    eventAddress1: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        validate: {
            notEmpty: false,
        },
        field: 'event_address_2',
    })
    eventAddress2: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2', '3', '4', '5', '6'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'event_status',
    })
    eventStatus: string;
    
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'event_odoo_id',
    })
    eventOdooId: number;
    /**
     * RELACIONES
     * Events pertenece a UserModel
     * Events tiene muchos EventDetails
     * Events tiene muchos EventAttachments
     * Events tiene muchos EventAttendances
     * Events Pertenece a muchos TransactionItemsModel
     */


    @ApiModelPropertyOptional({
        type: () => EventDetails,
        isArray: true
    })
    @HasMany(() => EventDetails)
    eventDetails: EventDetails[];

    @ApiModelPropertyOptional({
        type: () => EventAttachments,
        isArray: true
    })
    @HasMany(() => EventAttachments)
    eventAttachments: EventAttachments[];

    @ApiModelPropertyOptional({
        type: () => EventAttendances,
        isArray: true
    })
    @HasMany(() => EventAttendances)
    eventAttendances: EventAttendances[];

    @ApiModelPropertyOptional({
        type: TransactionItemsModel,
        isArray: false,
        example: [
            {
                "id":"UUID",
                "billingTransactionsId":"UUID",
                "userId":"UUID",
                "transactionItemType":"ENUM",
                "transactionItemCant":"number",
                "transactionItemAmmount":"number",
                "transactionItemTaxes":"number",
                "transactionItemTotal":"number"
            }
        ]
    })
    @BelongsToMany(() => TransactionItemsModel, () => TransactionsItemsHasEvents)
    transactionItemsModel: TransactionItemsModel[];

    @BelongsTo(() => UserModel)
    userModel: UserModel;

    @BelongsTo(() => CityModel)
    cityModel: CityModel;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
