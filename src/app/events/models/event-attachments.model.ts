import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Events } from './events.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'event_attachments',
})
export class EventAttachments extends Model<EventAttachments>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Events)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'event_id',
    })
    eventId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1','2','3'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'event_attachment_type',
    })
    eventAttachmentType: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'event_attachment',
    })
    eventAttachment: string;

    /**
     * RELACIONES
     * EventAttachments pertenece a Events
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => Events)
    events: Events;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}