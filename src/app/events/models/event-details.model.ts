import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Events } from './events.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'event_details',
})
export class EventDetails extends Model<EventDetails>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Events)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'event_id',
    })
    eventId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es','en','it','pr'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'event_title',
    })
    eventTitle: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'event_description',
    })
    eventDescription: string;

    /**
     * RELACIONES
     * EventDetails pertenece a Events
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => Events)
    events: Events;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}