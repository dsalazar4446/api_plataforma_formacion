import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Events } from './events.model';
import { UserModel } from './../../user/models/user.Model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'event_attendances',
})
export class EventAttendances extends Model<EventAttendances>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Events)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'event_id',
    })
    eventId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1','2','3','4'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'event_attendande_status',
    })
    eventAttendancesStatus: string;

    /**
     * RELACIONES
     * EventAttendances pertenece a Events
     * EventAttendances pertenece a UserModel
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => Events)
    events: Events;
    
    @ApiModelPropertyOptional()
    @BelongsTo(() => UserModel)
    userModel: UserModel;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}