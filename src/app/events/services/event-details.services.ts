import { Injectable, Inject } from "@nestjs/common";
import { EventDetails } from "../models/event-details.model";
import { EventDetailsUpdateJson, EventDetailsSaveJson } from "../interfaces/events-details.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services EventDetailsService
 * @Creado 02 Abril 2019
 */

@Injectable()
export class EventDetailsService {

    private language = ['es', 'en', 'it', 'pr'];

    constructor(
        @Inject('EventDetails') private readonly _eventDetails: typeof EventDetails
    ) { }

    async saveeDetails(bodyEDetails: EventDetailsSaveJson): Promise<EventDetailsUpdateJson> {

        return await new this._eventDetails(bodyEDetails).save();
    }

    async showAllEDetails(languageType: string): Promise<EventDetailsUpdateJson[]> {

        if (this.language.indexOf(languageType) >= 0) {

            return await this._eventDetails.findAll({
                where: {
                    languageType
                }
            });
        }

        return

    }

    async getDetails(eDetailsId: string){

        if (eDetailsId === undefined) {

            return await this.find();
        }

        return await this._eventDetails.findByPk(eDetailsId);
    }

    async updateeDetails(id: string, bodyEDetails: Partial<EventDetailsUpdateJson>): Promise<[number, Array<any>]> {

        return await this._eventDetails.update(bodyEDetails, {
            where: { id },
            returning: true
        });

    }

    async destroyeDetails(eDetailsId: string): Promise<number> {

        return await this._eventDetails.destroy({
            where: { id: eDetailsId },
        });
    }

    async find() {

        return await this._eventDetails.findAll();

    }
}