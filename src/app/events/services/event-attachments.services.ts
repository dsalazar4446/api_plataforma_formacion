import { Injectable, Inject, HttpStatus } from "@nestjs/common";
import { EventAttachments } from "../models/event-attachments.model";
import { EventAttachmentsUpdateJson, EventAttachmentsSaveJson } from "../interfaces/events-attachments.interface";
import * as path from 'path';
import * as fs from 'fs';
import { AppUtilsService } from "../../shared/services/app-utils.service";
/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services EventAttachmentsServices
 * @Creado 26 de Marzo 2019
 */

@Injectable()
export class EventAttachmentsServices {

    private options = ['1', '2', '3'];
    private pattern = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;

    constructor(
        @Inject('EventAttachments') private readonly _eventAttachmentsServcs: typeof EventAttachments,
        private readonly appUtilsService: AppUtilsService
    ) { }

    async saveAttachments(bodyAttach: EventAttachmentsSaveJson): Promise<EventAttachmentsUpdateJson> {

        if (bodyAttach.eventAttachmentType === '1') {
            if (!bodyAttach.eventAttachment.match(this.pattern)) {
                throw this.appUtilsService.httpCommonError('Event Attachments does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    ok: false,
                    message: 'Event Attachment: ' + bodyAttach.eventAttachment + ' it is not video url Youtube',
                    types: '1: video, 2: images, 3: audios'
                });
            }
        }
        return await new this._eventAttachmentsServcs(bodyAttach).save();
    }

    async showAllAttach(eventAttachmentType: string): Promise<EventAttachmentsUpdateJson[]> {

        if (this.options.indexOf(eventAttachmentType) >= 0) {

            return await this._eventAttachmentsServcs.findAll({
                where: {
                    eventAttachmentType
                }
            });
        }

    }

    async getDetails(attachId: string) {

        return await this._eventAttachmentsServcs.findByPk(attachId);
    }

    async updateAttach(id: string, bodyAttach: Partial<EventAttachmentsUpdateJson>): Promise<[number, Array<any>]> {

        if (bodyAttach.eventAttachmentType === '1') {
            if (!bodyAttach.eventAttachment.match(this.pattern)) {
                throw this.appUtilsService.httpCommonError('Event Attachments does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    ok: false,
                    message: 'Event Attachment: ' + bodyAttach.eventAttachment + ' it is not video url Youtube',
                    types: '1: video, 2: images, 3: audios'
                });
            }
        }

        if (bodyAttach.eventAttachment) {
            await this.deleteImg(id);
        }

        return await this._eventAttachmentsServcs.update(bodyAttach, {
            where: { id },
            returning: true
        });

    }

    async destroyAttach(attachId: string): Promise<number> {

        await this.deleteImg(attachId);
        return await this._eventAttachmentsServcs.destroy({
            where: { id: attachId },
        });
    }

    async find() {

        return await this._eventAttachmentsServcs.findAll();

    }

    private async deleteImg(id: string) {
        const img = await this._eventAttachmentsServcs.findByPk(id).then(item => item.eventAttachment).catch(err => err);

        const pathImagen = path.resolve(__dirname, `../../../../uploads/eventAttachment/${img}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }

}