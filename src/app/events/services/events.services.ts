import { Injectable, Inject, NotFoundException } from "@nestjs/common";
import { Events } from "../models/events.model";
import { EventsSaveJson, EventsUpdateJson } from "../interfaces/events.interface";
import { EventDetails } from "../models/event-details.model";
import { TransactionItemsModel } from "../../transactions/models/transaction-items.model";
import { CityModel } from "../../city/models/city.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services EventsServices
 * @Creado 01 Abril 2019
 */

@Injectable()
export class EventsServices {

    private eventStatus = ['5', '6', '3', '4', '1', '2'];
    private eventType = ['1', '2'];
    private include = [
        {
            model: EventDetails
        },
        {
            model: CityModel
        },
        {
            model: TransactionItemsModel
        }
    ];

    constructor(
        @Inject('Events') private readonly _events: typeof Events
    ) { }

    async saveEvents(bodyEvents: EventsSaveJson): Promise<EventsUpdateJson> {

        return await new this._events(bodyEvents).save();
    }

    async showAllEvents(languageType: string, eventStatus: string, eventType?: string): Promise<EventsUpdateJson[]> {

        if (this.eventStatus.indexOf(eventStatus) < 0) throw new NotFoundException('Events Status does not exist! 1, 2, 3, 4, 5, 6');
        if (eventType) if (this.eventType.indexOf(eventType) < 0) throw new NotFoundException('Events Status does not exist! 1, 2');

        if (eventType) {
            return await this._events.findAll({
                include: [
                    {
                        model: EventDetails,
                        where: { languageType }
                    },
                    {
                        model: CityModel
                    },
                    {
                        model: TransactionItemsModel
                    }
                ],
                where: {
                    eventStatus,
                    eventType
                }
            });
        }

        return await this._events.findAll({
            include: [
                {
                    model: EventDetails,
                    where: { languageType }
                },
                {
                    model: CityModel
                },
                {
                    model: TransactionItemsModel
                }
            ],
            where: {
                eventStatus
            }
        });

    }

    async getDetails(eventsId: string) {

        return await this._events.findByPk(eventsId, { include: this.include });
    }

    async updateEvents(id: string, bodyEvents: Partial<EventsUpdateJson>): Promise<[number, Array<any>]> {

        return await this._events.update(bodyEvents, {
            where: { id },
            returning: true
        });

    }

    async destroyEvents(eventsId: string): Promise<number> {

        return await this._events.destroy({
            where: { id: eventsId },
        });
    }

    async find() {
        return await this._events.findAll({
            include: [
                {
                    model: EventDetails
                },
                {
                    model: CityModel
                },
                {
                    model: TransactionItemsModel
                }
            ]
        });
    }

}