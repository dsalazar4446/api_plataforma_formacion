import { Injectable, Inject } from "@nestjs/common";
import { EventAttendances } from "../models/event-attendances.model";
import { EventAttendancesUpdateJson, EventAttendancesSaveJson } from "../interfaces/events-attendances.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services EventAttendancesServices
 * @Creado 26 de Marzo 2019
 */

@Injectable()
export class EventAttendancesServices {

    private options = ['1', '2', '3', '4'];

    constructor(
        @Inject('EventAttendances') private readonly _eventAttendancesServcs: typeof EventAttendances
    ) { }

    async saveEventAttendances(bodyAttendances: EventAttendancesSaveJson): Promise<EventAttendancesUpdateJson> {

        return await new this._eventAttendancesServcs(bodyAttendances).save();
    }

    async showAllAttendances(eventAttendancesStatus: string): Promise<EventAttendancesUpdateJson[]> {

        if (this.options.indexOf(eventAttendancesStatus) >= 0) {

            return await this._eventAttendancesServcs.findAll({
                where: {
                    eventAttendancesStatus
                }
            });
        }

    }

    async getDetails(attendancesId: string){

        if (attendancesId === undefined) {

            return await this.find();
        }

        return await this._eventAttendancesServcs.findByPk(attendancesId);
    }
    
    async updateAttendances(id: string, bodyAttendances: Partial<EventAttendancesUpdateJson>): Promise<[number, Array<any>]> {
        
        return await this._eventAttendancesServcs.update(bodyAttendances, {
            where: { id },
            returning: true
        });
        
    }
    
    async destroyAttendances(attendancesId: string): Promise<number> {
        
        return await this._eventAttendancesServcs.destroy({
            where: { id: attendancesId },
        });
    }

    async find() {

        return await this._eventAttendancesServcs.findAll();

    }
}