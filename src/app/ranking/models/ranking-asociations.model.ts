import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { RankingModel } from './ranking.model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'ranking_asociations',
})
export class RankingAsociations extends Model<RankingAsociations>{

    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelProperty()
    @ForeignKey(() => RankingModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'ranking_parent_id',
    })
    rankingParentId: string;
    @ApiModelProperty()
    @ForeignKey(() => RankingModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'ranking_children_id',
    })
    rankingChildrenId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.INTEGER,
        defaultValue: 0,
        validate: {
            notEmpty: true,
        },
        field: 'ranking_association_cant',
    })
    rankingAssociationCant: number;

    /**
     * RELACIONES
     * NewsAttachments pertenece a RankingModel
     */
    @ApiModelProperty()
    @BelongsTo(() => RankingModel, 'rankingParentId')
    rankingParent: RankingModel;
    @ApiModelProperty()
    @BelongsTo(() => RankingModel, 'rankingChildrenId')
    rankingChildren: RankingModel;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;

}