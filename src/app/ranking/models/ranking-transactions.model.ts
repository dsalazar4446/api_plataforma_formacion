import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { BillingTransactionsModel } from '../../transactions/models/billing-transactions.model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'ranking_transactios',
})
export class RankingTransactions extends Model<RankingTransactions>{

    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'parent_user_id',
    })
    parentUserId: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'children_user_id',
    })
    childrenUserId: string;
    @ApiModelProperty()
    @ForeignKey(() => BillingTransactionsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'billing_transactions_id',
    })
    billingTransactionsId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.NUMERIC,
        defaultValue: 0,
        validate: {
            notEmpty: true,
        },
        field: 'ranking_transaction_ammount',
    })
    rankingTransactionAmmount: number;
    @ApiModelProperty()
    @BelongsTo(() => UserModel, 'parentUserId')
    parentUser: UserModel;
    @ApiModelProperty()
    @BelongsTo(() => UserModel, 'childrenUserId')
    childrenUser: UserModel;
    @ApiModelProperty()
    @BelongsTo(() => BillingTransactionsModel)
    billingTransactions: BillingTransactionsModel;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;

}