import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey } from 'sequelize-typescript';
import { BillingTransactionsModel } from '../../transactions/models/billing-transactions.model';
import { UserRankings } from './user-ranking.model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'user_rankings_has_transactions',
})
export class UserRankingsHasTransactions extends Model<UserRankingsHasTransactions>{
    @ApiModelProperty()
    @ForeignKey(() => UserRankings)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_ranking_id',
    })
    userRankingId: string;
    @ApiModelProperty()
    @ForeignKey(() => BillingTransactionsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'billing_transaction_id',
    })
    billingTransactionId: string;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;

}