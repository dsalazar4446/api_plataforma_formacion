import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo, BelongsToMany } from 'sequelize-typescript';
import { RankingModel } from './ranking.model';
import { UserModel } from '../../user/models/user.Model';
import { BillingTransactionsModel } from '../../transactions/models/billing-transactions.model';
import { UserRankingsHasTransactions } from './user-ranking-has-transactions.model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'user_rankings',
})
export class UserRankings extends Model<UserRankings>{

    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelProperty()
    @ForeignKey(() => RankingModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'ranking_id',
    })
    rankingId: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;
    @ApiModelProperty()
    @BelongsTo(() => UserModel)
    userModel: UserModel;
    @ApiModelProperty()
    @BelongsTo(() => RankingModel)
    rankingModel: RankingModel;
    @ApiModelProperty()
    @BelongsToMany(() => BillingTransactionsModel, {
        through: {
            model: () => UserRankingsHasTransactions,
            unique: true,
        }
    })
    billingTransactionsModel: BillingTransactionsModel[];
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;

}