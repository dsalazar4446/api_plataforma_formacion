import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { RankingModel } from './ranking.model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'ranking_details',
})
export class RankingDetails extends Model<RankingDetails>{

    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelProperty()
    @ForeignKey(() => RankingModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'ranking_id',
    })
    rankingId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(75),
        validate: {
            notEmpty: true,
        },
        field: 'ranking_title',
    })
    rankingTitle: string;
    @ApiModelProperty()
    @Column({
        type: DataType.TEXT,
        validate: {
            notEmpty: true,
        },
        field: 'ranking_requirements',
    })
    rankingRequirements: string;
    @ApiModelProperty()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;

    /**
     * RELACIONES
     * NewsAttachments pertenece a RankingModel
     */
    @ApiModelProperty()
    @BelongsTo(() => RankingModel)
    RankingModel: RankingModel;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;

}