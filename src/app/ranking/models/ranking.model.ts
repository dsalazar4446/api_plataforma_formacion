import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey, HasMany, BelongsToMany } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { PrebuiltUserModel } from '../../prebuilt-users/models/prebuilt-users.model';
import { RankingDetails } from './ranking-details.model';
import { RankingAsociations } from './ranking-asociations.model';
import { UserRankings } from './user-ranking.model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'rankings',
})
export class RankingModel extends Model<RankingModel> {

    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;

    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.INTEGER,
        defaultValue: 0,
        allowNull: false,
        field: 'ranking_timing',
    })
    rankingTiming: number;

    @ApiModelProperty()
    @Column({
        type: DataType.INTEGER,
        defaultValue: 0,
        allowNull: false,
        field: 'billing_goal',
    })
    billingGoal: number;

    @ApiModelProperty()
    @Column({
        type: DataType.INTEGER,
        defaultValue: 0,
        allowNull: false,
        field: 'active_member_bonus_value',
    })
    activeMemberBonusValue: number;

    @ApiModelProperty()
    @Column({
        type: DataType.INTEGER,
        defaultValue: 0,
        allowNull: false,
        field: 'event_bonus_value',
    })
    eventBonusValue: number;

    @ApiModelProperty()
    @Column({
        type: DataType.INTEGER,
        defaultValue: 0,
        allowNull: false,
        field: 'corporate_bonus_value',
    })
    corporateBonusValue: number;

    @ApiModelProperty()
    @Column({
        type: DataType.STRING(6),
        allowNull: false,
        field: 'ranking_color_1',
    })
    rankingColor1: string;

    @ApiModelProperty()
    @Column({
        type: DataType.STRING(6),
        allowNull: false,
        field: 'ranking_color_2',
    })
    rankingColor2: string;

    @ApiModelProperty()
    @Column({
        type: DataType.STRING(75),
        allowNull: false,
        field: 'ranking_icon',
    })
    rankingIcon: string;

    @ApiModelProperty()
    @Column({
        type: DataType.NUMERIC,
        allowNull: false,
        field: 'active_direc_member_bonus_value',
    })
    activeDirecMemberBonusValue: number;

    @ApiModelProperty()
    @Column({
        type: DataType.STRING(75),
        defaultValue: 0,
        allowNull: false,
        field: 'ranking_img_bar',
    })
    rankingImgBar: string;

    @ApiModelProperty({
        example: [
            {
                "id": "UUID",
                "userName": "string",
                "userLastName": "string",
                "rankingId": "string",
                "avatar": "string",
                "ranking": "RankingModel"
            }
        ]
    })
    @HasMany(() => PrebuiltUserModel)
    prebuiltUserModel: PrebuiltUserModel[];


    @ApiModelProperty({
        type: RankingDetails,
        isArray: true
    })
    @HasMany(() => RankingDetails)
    rankingDetails: RankingDetails[];


    @ApiModelProperty({
        type: RankingAsociations,
        isArray: true
    })
    @HasMany(() => RankingAsociations, 'rankingParentId')
    rankingP: RankingAsociations[];


    @ApiModelProperty({
        type: RankingAsociations,
        isArray: true
    })
    @HasMany(() => RankingAsociations, 'rankingChildrenId')
    rankingC: RankingAsociations[];


    @BelongsToMany(() => UserModel, {
        through: {
            model: () => UserRankings,
            unique: true,
        }
    })
    userModel: UserModel[];

    
    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
