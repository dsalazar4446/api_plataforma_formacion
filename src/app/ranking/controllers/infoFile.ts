import { diskStorage } from 'multer';
import * as path from 'path';

export const nameFile =[
    { name: 'rankingIcon', maxCount: 1 },
    { name: 'rankingImgBackground', maxCount: 1 },
    { name: 'rankingImgBar', maxCount: 1 }
];

export const validationFile ={
    storage: diskStorage({
        limits:{
            files: 3
        },
        filename: (req, file, cb) => {

            let extValidas = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG'];

            if (extValidas.indexOf(path.extname(file.originalname)) < 0) {
                cb(`valid extensions:${extValidas.join(', ')} `);
                return;
            }
            const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
            if (file.fieldname === 'rankingIcon') {
                req.body.rankingIcon = `${randomName}${path.extname(file.originalname)}`;
            }
            if (file.fieldname === 'rankingImgBackground') {
                req.body.rankingImgBackground = `${randomName}${path.extname(file.originalname)}`;
            }
            if (file.fieldname === 'rankingImgBar') {
                req.body.rankingImgBar = `${randomName}${path.extname(file.originalname)}`;
            }
            cb(null, `${randomName}${path.extname(file.originalname)}`);
        },
        destination: './uploads/rankingImgs',
    }),
}