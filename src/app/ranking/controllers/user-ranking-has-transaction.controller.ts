import { Controller, UseFilters, UseInterceptors, Post, Body, NotFoundException, Delete, HttpStatus, Param } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { UserRankingHasTransactionService } from '../services/user-ranking-has-transaction.service';
import { UserRankingHasTransactionSaveJson } from '../interfaces/user-ranking-has-transactions.interface';
import { UserRankingsHasTransactions } from '../models/user-ranking-has-transactions.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller UserRankingHasControllerController
 * @Creado 23 de Abril 2019
 */

@ApiUseTags('Module-Ranking')
@Controller('user-ranking-has-transaction')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class UserRankingHasControllerController {

    constructor(private readonly _userRankingHasTransactionService: UserRankingHasTransactionService,
        private readonly appUtilsService: AppUtilsService) { }

    @Post()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type:   UserRankingsHasTransactions,
      })
    async create(@Body() bodyRanking: UserRankingHasTransactionSaveJson) {
        const createURanking = await this._userRankingHasTransactionService.save(bodyRanking);
        if (!createURanking) {
            return this.appUtilsService.httpCommonError('UserRankingHasTransactionService does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return createURanking;
    }

    @Delete(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteUserRanking(@Param('id') idRanking: string) {
        const deleteUserRanking = await this._userRankingHasTransactionService.destroyRanking(idRanking);
        if (!deleteUserRanking) {
            throw new NotFoundException('UserRankingHasTransactionService does not exist!');
        }
        return deleteUserRanking;
    }
}
