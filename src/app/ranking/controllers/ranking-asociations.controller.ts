import { Controller, UseFilters, UseInterceptors, Post, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param } from '@nestjs/common';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { RankingAsociationsSaveJson } from '../interfaces/ranking-asociations.interface';
import { RankingAsociationsService } from '../services/ranking-asociations.service';
import { RankingAsociations } from '../models/ranking-asociations.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller RankingAsociationsController
 * @Creado 26 de Marzo 2019
 */

@ApiUseTags('Module-Ranking')
@Controller('ranking-asociations')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class RankingAsociationsController {

    constructor(private readonly _rankingAsociationsServcs: RankingAsociationsService,
    private readonly appUtilsService: AppUtilsService) { }

    
    @ApiResponse({
        status: 200,
        type: RankingAsociations,
      })
    @Post()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async create(@Body() bodyRankingAsociations: RankingAsociationsSaveJson) {

        const createRankingAsociations = await this._rankingAsociationsServcs.save(bodyRankingAsociations);

        if (!createRankingAsociations) {
            return this.appUtilsService.httpCommonError('Ranking asociations does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return createRankingAsociations;

    }


    @ApiResponse({
        status: 200,
        type: RankingAsociations,
        isArray: true
      })
    @Get()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async showAll() {
            return await this._rankingAsociationsServcs.showAll();
    }


    @ApiResponse({
        status: 200,
        type: RankingAsociations,
      })
    @Get('details/:id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idRankingAsociations: string) {

        const fetchRankingAsociations = await this._rankingAsociationsServcs.getDetails(idRankingAsociations);

        if (!fetchRankingAsociations) {
            throw new NotFoundException('Ranking asociations does not exist!');
        }

        return fetchRankingAsociations;
    }


    @ApiResponse({
        status: 200,
        type: RankingAsociations,
      })
    @Put(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateRankingAsociations(@Param('id') id: string, @Body() bodyRankingAsociations: Partial<RankingAsociationsSaveJson>) {

        const updateRankingAsociations = await this._rankingAsociationsServcs.update(id, bodyRankingAsociations);

        if (!updateRankingAsociations) {
            throw new NotFoundException('Ranking asociations does not exist!');
        }

        return updateRankingAsociations;
    }

    
    @Delete(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteRankingAsociations(@Param('id') idRankingAsociations: string) {

        const deleteRankingAsociations = await this._rankingAsociationsServcs.destroyRanking(idRankingAsociations);

        if (!deleteRankingAsociations) {
            throw new NotFoundException('Ranking asociations does not exist!');
        }

        return deleteRankingAsociations;
    }
}
