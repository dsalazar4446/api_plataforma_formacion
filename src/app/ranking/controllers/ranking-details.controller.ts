import { Controller, UseFilters, UseInterceptors, Post, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param, Req } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { RankingDetailsService } from '../services/ranking-details.service';
import { RankingDetailsSaveJson } from '../interfaces/ranking-details.interface';
import { RankingDetails } from '../models/ranking-details.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller RankingDetailsController
 * @Creado 26 de Marzo 2019
 */

@ApiUseTags('Module-Ranking')
@Controller('ranking-details')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class RankingDetailsController {

    constructor(private readonly _rankingDetailsServcs: RankingDetailsService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: RankingDetails,
    })
    @Post()
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    async create(@Req() req, @Body() bodyRankingDetails: RankingDetailsSaveJson) {

        if (req.headers.language) {
            bodyRankingDetails.languageType = req.headers.language;
        } else {
            bodyRankingDetails.languageType = 'es';
        }

        const createRankingDetails = await this._rankingDetailsServcs.save(bodyRankingDetails);

        if (!createRankingDetails) {
            return this.appUtilsService.httpCommonError('Ranking Details does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return createRankingDetails;

    }


    @ApiResponse({
        status: 200,
        type: RankingDetails,
        isArray: true
    })
    @Get('list')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    async showAll() {
        return await this._rankingDetailsServcs.find();
    }


    @ApiResponse({
        status: 200,
        type: RankingDetails,
    })
    @Get('details/:id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idRankingDetails: string) {

        const fetchRankingDetails = await this._rankingDetailsServcs.getDetails(idRankingDetails);

        if (!fetchRankingDetails) {
            throw new NotFoundException('Ranking Details does not exist!');
        }

        return fetchRankingDetails;
    }


    @ApiResponse({
        status: 200,
        type: RankingDetails,
    })
    @Get()
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    async showAllRankingDetails(@Req() req) {

        const fetch = await this._rankingDetailsServcs.showAll(req.headers.language);
        if (!fetch) {
            throw new NotFoundException('Language does not exist!');
        }

        return fetch;
    }


    @ApiResponse({
        status: 200,
        type: RankingDetails,
    })
    @Put(':id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateRankingDetails(@Req() req, @Param('id') id: string, @Body() bodyRankingDetails: Partial<RankingDetailsSaveJson>) {

        if (req.headers.language) {
            bodyRankingDetails.languageType = req.headers.language;
        }

        const updateRankingDetails = await this._rankingDetailsServcs.update(id, bodyRankingDetails);

        if (!updateRankingDetails) {
            throw new NotFoundException('Ranking Details does not exist!');
        }

        return updateRankingDetails;
    }


    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteRankingDetails(@Param('id') idRankingDetails: string) {

        const deleteRankingDetails = await this._rankingDetailsServcs.destroyRanking(idRankingDetails);

        if (!deleteRankingDetails) {
            throw new NotFoundException('Ranking Details does not exist!');
        }

        return deleteRankingDetails;
    }
}
