import { Controller, UseFilters, UseInterceptors, Post, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param, Req } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { RankingTransactionsService } from '../services/ranking-transactions.service';
import { RankingTransactionsSaveJson } from '../interfaces/ranking-transactions.interface';
import { RankingTransactions } from '../models/ranking-transactions.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller RankingTransactionsController
 * @Creado 26 de Marzo 2019
 */

@ApiUseTags('Module-Ranking')
@Controller('ranking-transactions')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class RankingTransactionsController {

    constructor(private readonly _rankingTransactionsService: RankingTransactionsService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: RankingTransactions,
    })
    @Post()
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    async create(@Req() req, @Body() bodyRankingTransactions: RankingTransactionsSaveJson) {

        const createRankingTransactions = await this._rankingTransactionsService.save(bodyRankingTransactions);

        if (!createRankingTransactions) {
            return this.appUtilsService.httpCommonError('Ranking Details does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return createRankingTransactions;
    }


    @ApiResponse({
        status: 200,
        type: RankingTransactions,
        isArray: true
    })
    @Get('list')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    async showAll() {
        return await this._rankingTransactionsService.showAll();
    }


    @ApiResponse({
        status: 200,
        type: RankingTransactions,
    })
    @Get('details/:id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idRankingTransactions: string) {

        const fetchRankingTransactions = await this._rankingTransactionsService.getDetails(idRankingTransactions);

        if (!fetchRankingTransactions) {
            throw new NotFoundException('Ranking Details does not exist!');
        }
        return fetchRankingTransactions;
    }


    @ApiResponse({
        status: 200,
        type: RankingTransactions,
    })
    @Get()
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    async showAllRankingTransactions() {

        const fetch = await this._rankingTransactionsService.showAll();
        if (!fetch) {
            throw new NotFoundException('Language does not exist!');
        }
        return fetch;
    }


    @ApiResponse({
        status: 200,
        type: RankingTransactions,
    })
    @Put(':id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateRankingTransactions(@Param('id') id: string, @Body() bodyRankingTransactions: Partial<RankingTransactionsSaveJson>) {

        const updateRankingTransactions = await this._rankingTransactionsService.update(id, bodyRankingTransactions);

        if (!updateRankingTransactions) {
            throw new NotFoundException('Ranking Details does not exist!');
        }
        return updateRankingTransactions;
    }


    @Delete(':id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteRankingTransactions(@Param('id') idRankingTransactions: string) {

        const deleteRankingTransactions = await this._rankingTransactionsService.destroyRanking(idRankingTransactions);
        if (!deleteRankingTransactions) {
            throw new NotFoundException('Ranking Details does not exist!');
        }
        return deleteRankingTransactions;
    }
}
