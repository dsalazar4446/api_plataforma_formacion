import { Controller, UseFilters, UseInterceptors, Post, Body, NotFoundException, Delete, HttpStatus, Param } from '@nestjs/common';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { UserRankingService } from '../services/user-ranking.service';
import { UserRankingsSaveJson } from '../interfaces/user-ranking.interface';
import { UserRankings } from '../models/user-ranking.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller UserRankingController
 * @Creado 23 de Abril 2019
 */

@ApiUseTags('Module-Ranking')
@Controller('user-ranking')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class UserRankingController {

    constructor(private readonly _userRankingService: UserRankingService,
    private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type:   UserRankings,
      })
    @Post()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async create(@Body() bodyRanking: UserRankingsSaveJson) {

        const createURanking = await this._userRankingService.save(bodyRanking);

        if (!createURanking) {
            return this.appUtilsService.httpCommonError('User Ranking does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return createURanking;

    }


    @Delete(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteUserRanking(@Param('id') idRanking: string) {

        const deleteUserRanking = await this._userRankingService.destroyRanking(idRanking);

        if (!deleteUserRanking) {
            throw new NotFoundException('User Ranking does not exist!');
        }

        return deleteUserRanking;
    }
}
