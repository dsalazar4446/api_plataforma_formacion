import { Controller, Post, Body, Get, Put, Delete, Param, UseInterceptors, UseFilters, UseGuards, FileFieldsInterceptor, Req } from '@nestjs/common';
import { RankingService } from '../services/ranking.service';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { RankingSaveJson } from '../interfaces/ranking.interface';
import { JwtAuthGuard } from '../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiUseTags, ApiConsumes, ApiImplicitFile, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import * as file from './infoFile';
import { RankingModel } from '../models/ranking.model';

@ApiUseTags('Module-Ranking')
@Controller('ranking')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class RankingController {
    constructor(
        private readonly rankingService: RankingService,
    ) { }


    @ApiResponse({
        status: 200,
        type: RankingModel,
    })
    @Post()
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'Files', description: 'Files Rankings', required: true })
    async create(@Body() ranking: RankingSaveJson) {

        return await this.rankingService.create(ranking);
    }


    @ApiResponse({ 
        status: 200,
        type: RankingModel,
        isArray: true
    })
    @Get()
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    async getAll(@Req() req) {
        return await this.rankingService.findAll(req.headers.language);
    }


    @ApiResponse({
        status: 200,
        type: RankingModel,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idRanking,@Req() req) {
        return await this.rankingService.findById(idRanking,req.headers.language);
    }


    @ApiResponse({
        status: 200,
        type: RankingModel,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'Files', description: 'Files Rankings', required: true })
    async update(
        @Body()
        updateRanking: Partial<RankingSaveJson>,
        @Param('id') idRanking
    ) {
        return await this.rankingService.update(idRanking, updateRanking);
    }


    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    async delete(@Param('id') idRanking) {
        return await this.rankingService.deleted(idRanking);
    }
}
