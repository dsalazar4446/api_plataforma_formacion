import { Injectable, Inject } from "@nestjs/common";
import { UserRankings } from "../models/user-ranking.model";
import { UserRankingsSaveJson, UserRankingsUpdateJson } from "../interfaces/user-ranking.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services UserRankingService
 * @Creado 23 de Abril 2019
 */

@Injectable()
export class UserRankingService {

    constructor(
        @Inject('UserRankings') private readonly _userRankings: typeof UserRankings
    ) { }

    async save(bodyRanking: UserRankingsSaveJson): Promise<UserRankingsUpdateJson> {
        return await new this._userRankings(bodyRanking).save();
    }

    async destroyRanking(rankingId: string): Promise<number> {
        return await this._userRankings.destroy({
            where: { id: rankingId },
        });
    }

}