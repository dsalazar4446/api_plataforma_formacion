import { Injectable, Inject } from "@nestjs/common";
import { RankingTransactionsSaveJson, RankingTransactionsUpdateJson } from "../interfaces/ranking-transactions.interface";
import { RankingTransactions } from "../models/ranking-transactions.model";
import { UserModel } from "../../user/models/user.Model";
import { BillingTransactionsModel } from "../../transactions/models/billing-transactions.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services RankingTransactionsService
 * @Creado 09 de Mayo 2019
 */

@Injectable()
export class RankingTransactionsService {

    private include = {
        include: [
            {   
                as: 'parentUser',
                model: UserModel
            },
            {   
                as: 'childrenUser',
                model: UserModel
            },
            {   
                model: BillingTransactionsModel
            }
        ]
    }

    constructor(
        @Inject('RankingTransactions') private readonly _rankingTransactions: typeof RankingTransactions
    ) { }

    async save(bodyRanking: RankingTransactionsSaveJson): Promise<RankingTransactionsUpdateJson> {
        return await new this._rankingTransactions(bodyRanking).save();
    }

    async showAll(): Promise<RankingTransactionsUpdateJson[]> {
            return await this._rankingTransactions.findAll(this.include);
    }

    async getDetails(rankingId: string){

        return await this._rankingTransactions.findByPk(rankingId, this.include);
    }

    async update(id: string, bodyRanking: Partial<RankingTransactionsSaveJson>): Promise<[number, Array<RankingTransactionsUpdateJson>]> {
        return await this._rankingTransactions.update(bodyRanking, {
            where: { id },
            returning: true
        });

    }

    async destroyRanking(rankingId: string): Promise<number> {
        return await this._rankingTransactions.destroy({
            where: { id: rankingId },
        });
    }

}