import { Injectable, Inject } from "@nestjs/common";
import { UserRankingsHasTransactions } from "../models/user-ranking-has-transactions.model";
import { UserRankingHasTransactionSaveJson } from "../interfaces/user-ranking-has-transactions.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services UserRankingHasTransactionService
 * @Creado 23 de Abril 2019
 */

@Injectable()
export class UserRankingHasTransactionService {

    constructor(
        @Inject('UserRankingsHasTransactions') private readonly _userRankingsHasTransactions: typeof UserRankingsHasTransactions
    ) { }

    async save(bodyRanking: UserRankingHasTransactionSaveJson): Promise<UserRankingHasTransactionSaveJson> {
        return await new this._userRankingsHasTransactions(bodyRanking).save();
    }

    async destroyRanking(rankingId: string): Promise<number> {
        return await this._userRankingsHasTransactions.destroy({
            where: { id: rankingId },
        });
    }

}