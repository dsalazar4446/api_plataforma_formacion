import { Injectable, Inject } from '@nestjs/common';
import { RankingSaveJson } from '../interfaces/ranking.interface';
import { RankingModel } from '../models/ranking.model';
import { Sequelize } from 'sequelize-typescript';
import { RankingDetails } from '../models/ranking-details.model';
import { completeUrl } from '../../shared/utils/completUrl';
import * as path from 'path';
import * as fs from 'fs';
const Op = Sequelize.Op;
@Injectable()
export class RankingService {

    private readonly default = 'default.jpeg';
    private readonly folder = 'rankingImgs';

    constructor(
        @Inject('RankingModel') private readonly rankingModel: typeof RankingModel
    ) { }

    // tslint:disable-next-line: max-line-length
    async findAll(language: string): Promise<any> {
        const data = await this.rankingModel.findAll({
            include: [{ model: RankingDetails }]
        });
        data.forEach(async item => {
            this.complete(item);
        });
        const result = data.map(
            (item) => {
                let res: any = [];
                item.dataValues.rankingDetails.map(
                    data2 =>{
                        if(data2.dataValues.languageType === language){
                            res.push(data2);
                        }
                    }
                )
                item.dataValues.rankingDetails = res;
                return item;
            }
        );
        return result;
    }

    async findById(id: string,language: string): Promise<RankingModel> {
        const data = await this.rankingModel.findByPk<RankingModel>(id, {
            include: [{ model: RankingDetails }]
        });
        this.complete(data);
        let res: any = [];
        data.dataValues.rankingDetails.map(
            data2 =>{
                if(data2.dataValues.languageType === language){
                    res.push(data2);
                }
            }
        )
        data.dataValues.rankingDetails = res;
        return data;
    }

    async create(ranking: RankingSaveJson): Promise<RankingModel> {

        if (!ranking.rankingIcon) ranking.rankingIcon = this.default;
        if (!ranking.rankingImgBar) ranking.rankingImgBar = this.default;
        // if (!ranking.rankingImgBackground) ranking.rankingImgBackground = this.default;

        const data = await this.rankingModel.create<RankingModel>(ranking, {
            returning: true,
        });

        this.complete(data);
        return data;
    }

    async update(idRanking: string, body: Partial<RankingSaveJson>) {

        if (body.rankingIcon) {
            let img = await this.rankingModel.findByPk(idRanking).then(item => item.rankingIcon).catch(err => err);
            this.deletePath(img);
        }
        if (body.rankingImgBar) {
            let img = await this.rankingModel.findByPk(idRanking).then(item => item.rankingImgBar).catch(err => err);
            this.deletePath(img);
        }
        // if (body.rankingImgBackground) {
        //     let img = await this.rankingModel.findByPk(idRanking).then(item => item.rankingImgBackground).catch(err => err);
        //     this.deletePath(img);
        // }

        const data = await this.rankingModel.update(body, {
            where: {
                id: idRanking,
            },
            returning: true,
        });

        data[1][0].rankingIcon = completeUrl(this.folder, data[1][0].rankingIcon);
        data[1][0].rankingImgBar = completeUrl(this.folder, data[1][0].rankingImgBar);
        // data[1][0].rankingImgBackground = completeUrl(this.folder, data[1][0].rankingImgBackground);

        return data;
    }

    async deleted(idRanking: string): Promise<any> {
        await this.deleteImg(idRanking);
        return await this.rankingModel.destroy({
            where: {
                id: idRanking,
            },
        });
    }


    private async deleteImg(id: string) {
        let img = await this.rankingModel.findByPk(id).then(item => item.rankingIcon).catch(err => err);
        this.deletePath(img);
        img = await this.rankingModel.findByPk(id).then(item => item.rankingImgBar).catch(err => err);
        this.deletePath(img);
        // img = await this.rankingModel.findByPk(id).then(item => item.rankingImgBackground).catch(err => err);
        // this.deletePath(img);
    }

    private async deletePath(img: string) {
        let pathImagen = path.resolve(__dirname, `../../../../uploads/${this.folder}/${img}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }
    async complete(data: RankingModel) {
        data.rankingIcon = completeUrl(this.folder, data.rankingIcon);
        data.rankingImgBar = completeUrl(this.folder, data.rankingImgBar);
        // data.rankingImgBackground = completeUrl(this.folder, data.rankingImgBackground);
    }
}
