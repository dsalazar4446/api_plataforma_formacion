import { Injectable, Inject } from "@nestjs/common";
import { RankingDetails } from "../models/ranking-details.model";
import { RankingDetailsSaveJson, RankingDetailsUpdateJson } from "../interfaces/ranking-details.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services RankingDetailsService
 * @Creado 23 de Abril 2019
 */

@Injectable()
export class RankingDetailsService {

    private language = ['es', 'en', 'it', 'pr'];

    constructor(
        @Inject('RankingDetails') private readonly _rankingDetails: typeof RankingDetails
    ) { }

    async save(bodyRanking: RankingDetailsSaveJson): Promise<RankingDetailsUpdateJson> {

        return await new this._rankingDetails(bodyRanking).save();
    }

    async showAll(languageType: string): Promise<RankingDetailsUpdateJson[]> {

        if (this.language.indexOf(languageType) >= 0) {

            return await this._rankingDetails.findAll({
                where: {
                    languageType
                }
            });
        }

        return;

    }

    async getDetails(rankingId: string){

        return await this._rankingDetails.findByPk(rankingId);
    }

    async update(id: string, bodyRanking: Partial<RankingDetailsSaveJson>): Promise<[number, Array<any>]> {

        return await this._rankingDetails.update(bodyRanking, {
            where: { id },
            returning: true
        });

    }

    async destroyRanking(rankingId: string): Promise<number> {

        return await this._rankingDetails.destroy({
            where: { id: rankingId },
        });
    }

    async find() {
        return await this._rankingDetails.findAll();
    }

}