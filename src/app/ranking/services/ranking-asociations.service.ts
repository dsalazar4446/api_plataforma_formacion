import { Injectable, Inject } from "@nestjs/common";
import { RankingAsociations } from "../models/ranking-asociations.model";
import { RankingAsociationsSaveJson, RankingAsociationsUpdateJson } from "../interfaces/ranking-asociations.interface";
import { RankingModel } from "../models/ranking.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services RankingAsociationsService
 * @Creado 23 de Abril 2019
 */

@Injectable()
export class RankingAsociationsService {

    private include = {
        include: [
            {
                as: 'rankingChildren',
                model: RankingModel
            },
            {
                as: 'rankingParent',
                model: RankingModel
            },
        ],
        attributes: ['id', 'ranking_association_cant']
    }

    constructor(
        @Inject('RankingAsociations') private readonly _rankingAsociations: typeof RankingAsociations
    ) { }

    async save(bodyRanking: RankingAsociationsSaveJson): Promise<RankingAsociationsUpdateJson> {
        return await new this._rankingAsociations(bodyRanking).save();
    }

    async showAll(): Promise<RankingAsociationsUpdateJson[]> {
            return await this._rankingAsociations.findAll(this.include);
    }

    async getDetails(rankingId: string){
        return await this._rankingAsociations.findByPk(rankingId,this.include);
    }

    async update(id: string, bodyRanking: Partial<RankingAsociationsSaveJson>): Promise<[number, Array<any>]> {
        return await this._rankingAsociations.update(bodyRanking, {
            where: { id },
            returning: true
        });
    }

    async destroyRanking(rankingId: string): Promise<number> {
        return await this._rankingAsociations.destroy({
            where: { id: rankingId },
        });
    }

}