import { ApiModelProperty } from "@nestjs/swagger";
import { IsInt, IsNotEmpty, IsUUID } from "class-validator";
export class RankingTransactionsSaveJson {

    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    parentUserId: string;

    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    childrenUserId: string;

    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    billingTransactionsId: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsInt()
    rankingTransactionAmmount: number;

}
// tslint:disable-next-line: max-classes-per-file
export class RankingTransactionsUpdateJson extends RankingTransactionsSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}