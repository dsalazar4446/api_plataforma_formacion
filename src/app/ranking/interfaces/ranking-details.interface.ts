import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty, IsUUID, IsOptional } from "class-validator";
export class RankingDetailsSaveJson {

    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    rankingId: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    rankingTitle: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    rankingRequirements: string;

    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsNotEmpty()
    @IsOptional()
    languageType: string;
}
// tslint:disable-next-line: max-classes-per-file
export class RankingDetailsUpdateJson extends RankingDetailsSaveJson {
    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    id: string;
}