import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsUUID, IsNumber } from "class-validator";
export class RankingAsociationsSaveJson {

    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    rankingParentId: string;

    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    rankingChildrenId: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    rankingAssociationCant: number;
}
// tslint:disable-next-line: max-classes-per-file
export class RankingAsociationsUpdateJson extends RankingAsociationsSaveJson {
    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    id: string;
}