import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum, IsIn, IsNumber} from "class-validator";
export class RankingSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsInt()
    rankingTiming: number;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsInt()
    billingGoal: number;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    activeMemberBonusValue: number;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    eventBonusValue: number;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    corporateBonusValue: number;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    rankingColor1: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    rankingColor2: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    rankingIcon: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    activeDirecMemberBonusValue: number;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    rankingImgBar: string;
}
// tslint:disable-next-line: max-classes-per-file
export class RankingUpdateJson extends RankingSaveJson {
    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    id: string;
}