import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsUUID } from "class-validator";
export class UserRankingHasTransactionSaveJson {

    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    userRankingId: string;

    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    billingTransactionId: string;
}