import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsUUID } from "class-validator";
export class UserRankingsSaveJson {

    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    rankingId: string;

    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    userId: string;
}
// tslint:disable-next-line: max-classes-per-file
export class UserRankingsUpdateJson extends UserRankingsSaveJson {
    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    id: string;
}