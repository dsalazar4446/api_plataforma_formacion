import { Module, HttpModule, forwardRef, Provider } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';

import { RankingService } from './services/ranking.service';
import { RankingAsociationsService } from './services/ranking-asociations.service';
import { RankingDetailsService } from './services/ranking-details.service';
import { UserRankingService } from './services/user-ranking.service';
import { RankingTransactionsService } from './services/ranking-transactions.service';

import { RankingModel } from './models/ranking.model';
import { RankingAsociations } from './models/ranking-asociations.model';
import { RankingDetails } from './models/ranking-details.model';
import { UserRankings } from './models/user-ranking.model';
import { UserRankingsHasTransactions } from './models/user-ranking-has-transactions.model';
import { RankingTransactions } from './models/ranking-transactions.model';

import { RankingController } from './controllers/ranking.controller';
import { RankingAsociationsController } from './controllers/ranking-asociations.controller';
import { RankingDetailsController } from './controllers/ranking-details.controller';
import { UserRankingController } from './controllers/user-ranking.controller';
import { RankingTransactionsController } from './controllers/ranking-transactions.controller';

const models = [
  RankingModel,
  RankingAsociations,
  RankingDetails,
  UserRankings,
  UserRankingsHasTransactions,
  RankingTransactions
];

const controllers = [
  RankingController,
  RankingAsociationsController,
  RankingDetailsController,
  UserRankingController,
  RankingTransactionsController
];

const providers: Provider[] = [
  RankingService,
  RankingAsociationsService,
  RankingDetailsService,
  UserRankingService,
  RankingTransactionsService
];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  controllers,
  providers
})
export class RankingModule { }