import { Table, Column, DataType, CreatedAt, UpdatedAt, Model, ForeignKey, BelongsTo } from "sequelize-typescript";
import { UserModel } from "../../user/models/user.Model";
import { Downlines } from "../../downlines/models/downlines.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({
    tableName: 'transferals',
})
export class Transferals extends Model<Transferals> {

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Downlines)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'downline_id',
    })
    downlineId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    originalParentId: string;
    
    @BelongsTo(() => Downlines)
    downlines: Downlines[];
    
    @BelongsTo(() => UserModel)
    originalParent: UserModel[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}