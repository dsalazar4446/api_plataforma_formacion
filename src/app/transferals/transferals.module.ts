import { Module, HttpModule, Provider } from "@nestjs/common";
import { SharedModule } from "../shared/shared.module";
import { DatabaseModule } from "../database/database.module";
import { TransferalsController } from './controllers/transferal.controller';
import { Transferals } from './models/transferal.model';
import { TransferalService } from './services/transferal.service';
import { DownlinesModule } from "../downlines/downlines.module";

const models = [Transferals];
const providers: Provider[] = [TransferalService];
const controllers = [TransferalsController];

@Module({
    imports: [
        SharedModule,
        HttpModule,
        DatabaseModule.forRoot({
            sequelize: {
                models,
            },
            loki: true,
        }),
        DownlinesModule
    ],
    providers,
    controllers,
})
export class TransferalsModule { }
