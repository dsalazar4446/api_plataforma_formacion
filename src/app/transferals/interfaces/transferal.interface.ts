import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsUUID } from "class-validator";
export class TransferalsSaveJson {

    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    downlineId: string;

    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    originalParentId: string;
}
// tslint:disable-next-line: max-classes-per-file
export class TransferalsUpdateJson extends TransferalsSaveJson {
    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    id: string;
}