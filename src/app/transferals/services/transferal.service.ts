import { Injectable, Inject } from "@nestjs/common";
import { Transferals } from "../models/transferal.model";
import { TransferalsSaveJson, TransferalsUpdateJson } from "../interfaces/transferal.interface";
import { Downlines } from "../../downlines/models/downlines.model";
import { UserModel } from "../../user/models/user.Model";
import { DownlinesService } from "src/app/downlines/services/downline.service";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller TransferalsController
 * @Creado 24 de Abril 2019
 */

@Injectable()
export class TransferalService {

    private include = {
        include: [
            {
                model: Downlines
            },
            {
                as: 'originalParent',
                model: UserModel,
            }
        ],
        attributes: ['id']
    };

    constructor(
        @Inject('Transferals') private readonly _transferals: typeof Transferals,
        private readonly downlines: DownlinesService,
    ) { }

    async save(bodyTransferals: TransferalsSaveJson): Promise<TransferalsUpdateJson> {
        const parent = await this.downlines.getFather(bodyTransferals.originalParentId);
        const parentDownline = await this.downlines.getDownlineByUserId(parent.parentUserId); 
        const current = await this.downlines.getDownlineByUserId(bodyTransferals.originalParentId);
        parentDownline.childrenUserId = current.childrenUserId;
        await parentDownline.save();
        await this.downlines.destroyRanking(current.id);
        return await new this._transferals(bodyTransferals).save();
    }

    async showAll(): Promise<TransferalsUpdateJson[]> {
        return await this._transferals.findAll(this.include);
    }

    async getDetails(transferalId: string){
        return await this._transferals.findByPk(transferalId, this.include);
    }

    async update(id: string, bodyTransferals: Partial<TransferalsSaveJson>): Promise<[number, Array<any>]> {

        return await this._transferals.update(bodyTransferals, {
            where: { id },
            returning: true
        });
    }

    async destroyTransferal(transferalId: string): Promise<number> {
        return await this._transferals.destroy({
            where: { id: transferalId },
        });
    }
}

