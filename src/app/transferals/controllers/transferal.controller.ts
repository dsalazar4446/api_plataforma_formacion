import { Controller, UseFilters, UseInterceptors, Post, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { TransferalsSaveJson } from '../interfaces/transferal.interface';
import { TransferalService } from '../services/transferal.service';
import { Transferals } from '../models/transferal.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller TransferalsController
 * @Creado 24 de Abril 2019
 */

@ApiUseTags('Module-Transferals')
@Controller('transferals')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class TransferalsController {

    constructor(private readonly _transferalsServcs: TransferalService,
        private readonly appUtilsService: AppUtilsService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: Transferals })
    async create(@Body() bodyTransferals: TransferalsSaveJson) {

        const createTransferals = await this._transferalsServcs.save(bodyTransferals);
        if (!createTransferals) {
            return this.appUtilsService.httpCommonError('Transferal does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return createTransferals;
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true})
    @ApiResponse({ status: 200, type: Transferals, isArray: true})
    async showAll() {
        return await this._transferalsServcs.showAll();
    }

    @Get('details/:id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: Transferals })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idTransferals: string) {

        const fetchTransferals = await this._transferalsServcs.getDetails(idTransferals);
        if (!fetchTransferals) {
            throw new NotFoundException('Transferal does not exist!');
        }
        return fetchTransferals;
    }

    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: Transferals })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateTransferals(@Param('id') id: string, @Body() bodyTransferals: Partial<TransferalsSaveJson>) {

        const updateTransferals = await this._transferalsServcs.update(id, bodyTransferals);
        if (!updateTransferals) {
            throw new NotFoundException('Transferal does not exist!');
        }
        return updateTransferals;
    }

    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteTransferals(@Param('id') idTransferals: string) {

        const deleteTransferals = await this._transferalsServcs.destroyTransferal(idTransferals);

        if (!deleteTransferals) {
            throw new NotFoundException('Transferal does not exist!');
        }

        return deleteTransferals;
    }

}