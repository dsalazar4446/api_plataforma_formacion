import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { EmailController } from './controllers/email.controller';
import { EmailService } from './services/email.service';
import { EmailModel } from './models/email.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
const models = [
  EmailModel,
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [EmailController],
  providers: [EmailService]
})
export class EmailModule {}
