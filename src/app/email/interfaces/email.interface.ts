import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class EmailSaveJson {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    @IsEmail()
    email: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    password: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    domain: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    smtp: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    @IsEmail()
    emailSecurity: string;
}
// tslint:disable-next-line: max-classes-per-file
export class EmailUpdateJson extends EmailSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    id: string;
}