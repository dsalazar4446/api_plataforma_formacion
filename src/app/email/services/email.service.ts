import { Injectable, Inject } from '@nestjs/common';
import { EmailSaveJson, EmailUpdateJson } from '../interfaces/email.interface';
import { EmailModel } from '../models/email.model';
import { Sequelize } from 'sequelize-typescript';
const Op = Sequelize.Op;
@Injectable()
export class EmailService {
    constructor(
        @Inject('EmailModel') private readonly emailModel: typeof EmailModel
    ) { }

// tslint:disable-next-line: max-line-length
    async findAll(): Promise<any> {
        return  await this.emailModel.findAll();
    }

    async findById(id: string): Promise<EmailModel> {
        return await this.emailModel.findById<EmailModel>(id);
    }

    async create(email: EmailSaveJson): Promise<EmailModel> {
        return await this.emailModel.create<EmailModel>(email, {
            returning: true,
        });
    }

    async update(idEmail: string, emailUpdate: Partial<EmailUpdateJson>){
        return  await this.emailModel.update(emailUpdate, {
            where: {
                id: idEmail,
            },
            returning: true,
        });
    }

    async deleted(idEmail: string): Promise<any> {
        return await this.emailModel.destroy({
            where: {
                id: idEmail,
            },
        });
    }
}
