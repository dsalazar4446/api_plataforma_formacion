import { Column, DataType, Table, Model, CreatedAt, UpdatedAt} from 'sequelize-typescript';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'emails',
})
export class EmailModel extends Model<EmailModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        field: 'email',
    })
    email: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        field: 'password',
    })
    password: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'domain',
    })
    domain: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(75),
        allowNull: false,
        field: 'smtp',
    })
    smtp: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(75),
        allowNull: false,
        field: 'email_security',
    })
    emailSecurity: string;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
