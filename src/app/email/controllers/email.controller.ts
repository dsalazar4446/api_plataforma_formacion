import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters, UseGuards} from '@nestjs/common';
import { EmailService } from '../services/email.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { EmailSaveJson, EmailUpdateJson } from '../interfaces/email.interface';
import { JwtAuthGuard } from '../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { EmailModel } from '../models/email.model';
@ApiUseTags('Email')
@Controller('email')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class EmailController {
    constructor(
        private readonly emailService: EmailService,
        ) {}
        @Post()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: EmailModel,
          })
        async create(@Body() email: EmailSaveJson) {
            return await this.emailService.create(email);
        }
        @Get()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: EmailModel,
            isArray: true
          })
        async getAll() {
            return await this.emailService.findAll();
        }
        @Get(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: EmailModel,
          })
        @ApiImplicitParam({name: 'id', required: true})
        async findById(@Param('id') idEmail) {
            return await this.emailService.findById(idEmail);
        }
        @Put(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: EmailModel,
          })
        @ApiImplicitParam({ name: 'id', required: true })
        async update(
            @Body()
            updateEmail: Partial<EmailUpdateJson>,
            @Param('id') idEmail
        ) {
            return await this.emailService.update(idEmail, updateEmail);
        }
        @Delete(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiImplicitParam({ name: 'id', required: true })
        async delete(@Param('id') idEmail) {
            return await this.emailService.deleted(idEmail);
        }
}
