import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class MagazinesHasUsersSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'magazinesId property must a be uuid'})
    @IsNotEmpty({message: 'magazinesId property not must null'})
    magazinesId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'usersId property must a be uuid'})
    @IsNotEmpty({message: 'usersId property not must null'})
    usersId: string;
}
