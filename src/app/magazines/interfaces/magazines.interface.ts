import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional, IsDate } from 'class-validator';
export class MagazinesSaveJson {

    @ApiModelProperty()
    @IsString({message: 'magazineTitle property must a be string'})
    @IsNotEmpty({message: 'magazineTitle property not must null'})
    magazineTitle: string;

    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsString({message: 'languageType property must a be string'})
    @IsOptional()
    languageType: string;

    @ApiModelProperty()
    @IsString({message: 'magazineDescription property must a be string'})
    @IsNotEmpty({message: 'magazineDescription property not must null'})
    magazineDescription: string;

    @ApiModelProperty()
    @IsDate()
    @IsNotEmpty({message: 'magazinePublishDate property not must null'})
    magazinePublishDate: Date;

    @ApiModelProperty()
    @IsString({message: 'magazinePdfAttachment property must a be PDF'})
    @IsNotEmpty({message: 'magazinePdfAttachment property not must null'})
    magazinePdfAttachment: string;

}
export class MagazinesUpdateJson extends MagazinesSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
