import { Injectable, Inject } from "@nestjs/common";
import { MagazinesHasUsers } from "../models/magazines-has-user.model";
import { MagazinesHasUsersSaveJson } from "../interfaces/magazines-has-user.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services MagazinesHasUsersServices
 * @Creado 04 de abril 2019
 */

@Injectable()
export class MagazinesHasUsersServices {

    constructor(
        @Inject('MagazinesHasUsers') private readonly _magazinesHasUsersSrvcs: typeof MagazinesHasUsers
    ) { }

    async save(body: MagazinesHasUsersSaveJson): Promise<MagazinesHasUsersSaveJson> {

        return await new this._magazinesHasUsersSrvcs(body).save();
    }

    async getAll(): Promise<MagazinesHasUsersSaveJson[]> {

        return await this._magazinesHasUsersSrvcs.findAll();
    }

    async destroy(id: string): Promise<number> {

        return await this._magazinesHasUsersSrvcs.destroy({
            where: { id },
        });
    }

}