import { Injectable, Inject } from "@nestjs/common";
import { Magazines } from "../models/magazines.model";
import { MagazinesSaveJson, MagazinesUpdateJson } from "../interfaces/magazines.interface";
import { UserModel } from "../../user/models/user.Model";
import * as path from 'path';
import * as fs from 'fs';
import { CONFIG } from "../../../config";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services MagazinesServices
 * @Creado 04 de abril 2019
 */

@Injectable()
export class MagazinesServices {

    constructor(
        @Inject('Magazines') private readonly _magazinesServcs: typeof Magazines
    ) { }

    async saveMagazines(bodyMagazines: MagazinesSaveJson): Promise<MagazinesUpdateJson> {

        const magazine =  await new this._magazinesServcs(bodyMagazines).save();

        magazine.magazinePdfAttachment = `${CONFIG.storage.server}magazinePdfAttachment/${magazine.magazinePdfAttachment}`;

        return magazine;
    }

    async showAllMagazines(): Promise<MagazinesUpdateJson[]> {

        const magazine = await this._magazinesServcs.findAll();

        magazine.forEach( item =>  item.magazinePdfAttachment =  `${CONFIG.storage.server}magazinePdfAttachment/${item.magazinePdfAttachment}`);

        return magazine;
    }

    async showAll(languageType : string): Promise<MagazinesUpdateJson[]> {

        const magazine = await this._magazinesServcs.findAll({
            where: {
                languageType
            }
        });

        magazine.forEach( item =>  item.magazinePdfAttachment =  `${CONFIG.storage.server}magazinePdfAttachment/${item.magazinePdfAttachment}`);

        return magazine;
    }

    async getDetails(magazinesId: string): Promise<MagazinesUpdateJson> {
        const magazine = await this._magazinesServcs.findByPk(magazinesId, {
            include: [
                {
                    model: UserModel
                }
            ]
        });
        magazine.magazinePdfAttachment = `${CONFIG.storage.server}magazinePdfAttachment/${magazine.magazinePdfAttachment}`;
        return magazine;
    }

    async updateMagazines(id: string, bodyMagazines: Partial<MagazinesUpdateJson>): Promise<[number, Array<any>]> {

        if( bodyMagazines.magazinePdfAttachment ){
            await this.deleteImg(id);
        }

        const magazine = await this._magazinesServcs.update(bodyMagazines, {
            where: { id },
            returning: true
        });

        magazine[1][0].magazinePdfAttachment = `${CONFIG.storage.server}magazinePdfAttachment/${magazine[1][0].magazinePdfAttachment}`;
        return magazine;

    }

    async destroyMagazines(magazinesId: string): Promise<number> {

        await this.deleteImg(magazinesId);
        return await this._magazinesServcs.destroy({
            where: { id: magazinesId },
        });
    }

    private async deleteImg(id: string){
        const img = await this._magazinesServcs.findByPk(id).then(item => item.magazinePdfAttachment).catch(err => err);
    
        const pathImagen = path.resolve(__dirname, `../../../../uploads/magazinePdfAttachment/${img}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }
}