import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { Magazines } from './magazines.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'magazines_has_users',
})
export class MagazinesHasUsers extends Model<MagazinesHasUsers>{

    @ApiModelPropertyOptional()
    @ForeignKey(() => Magazines)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'magazines_id',
    })
    magazinesId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'users_id',
    })
    usersId: string;
}