import { Table, Model, Column, DataType, CreatedAt, UpdatedAt, HasMany, BelongsToMany } from "sequelize-typescript";
import { UserModel } from "../../user/models/user.Model";
import { MagazinesHasUsers } from "./magazines-has-user.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({
    tableName: 'magazines',
})
export class Magazines extends Model<Magazines>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'magazine_title',
    })
    magazineTitle: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es','en','it','pr'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'magazine_description',
    })
    magazineDescription: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'magazine_publish_date',
    })
    magazinePublishDate: Date;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'magazine_pdf_attachment',
    })
    magazinePdfAttachment: string;


    
    @ApiModelPropertyOptional()
    @BelongsToMany(() => UserModel, {
        through: {
          model: () => MagazinesHasUsers,
          unique: true,
        }
      })
    userModel: UserModel[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}