import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, Param, NotFoundException, Put, Delete, FileFieldsInterceptor, Req } from "@nestjs/common";
import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { MagazinesServices } from "../services/magazines.service";
import { MagazinesSaveJson } from "../interfaces/magazines.interface";
import { ApiUseTags, ApiImplicitParam, ApiConsumes, ApiImplicitFile, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import * as file from './infoFile';
import { Magazines } from "../models/magazines.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller MagazinesController
 * @Creado 09 Abril 2019
 */

@ApiUseTags('Module-Magazines')
@Controller('magazines')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class MagazinesController {

    constructor(private readonly _magazinesServices: MagazinesServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: Magazines,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'magazinePdfAttachment', description: 'Magazine Pdf Attachment File', required: true })
    async create(@Req() req, @Body() bodyMagazines: MagazinesSaveJson) {

        if (req.headers.language) {
            bodyMagazines.languageType = req.headers.language;
        } else {
            bodyMagazines.languageType = 'es';
        }

        const createMagazines = await this._magazinesServices.saveMagazines(bodyMagazines);

        if (!createMagazines) {
            throw this.appUtilsService.httpCommonError('Magazines no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM111',
                languageType: 'es',
            });
        }

        return createMagazines;

    }


    @ApiResponse({
        status: 200,
        type: Magazines,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAllMagazines() {

        return await this._magazinesServices.showAllMagazines();
    }


    @ApiResponse({
        status: 200,
        type: Magazines,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idMagazines: string) {

        const fetchMagazines = await this._magazinesServices.getDetails(idMagazines);

        if (!fetchMagazines) {
            throw this.appUtilsService.httpCommonError('Magazines no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM112',
                languageType: 'es',
            });
        }

        return fetchMagazines;
    }


    @ApiResponse({
        status: 200,
        type: Magazines,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAll(@Req() req) {
        const fetch = await this._magazinesServices.showAll(req.headers.language);

        if (!fetch[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetch;
    }


    @ApiResponse({
        status: 200,
        type: Magazines,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'magazinePdfAttachment', description: 'Magazine Pdf Attachment File', required: true })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateMagazines(@Req() req, @Param('id') id: string, @Body() bodyMagazines: Partial<MagazinesSaveJson>) {

        if (req.headers.language) bodyMagazines.languageType = req.headers.language;

        const updateMagazines = await this._magazinesServices.updateMagazines(id, bodyMagazines);

        if (!updateMagazines[1][0]) {
            throw this.appUtilsService.httpCommonError('Magazines no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM112',
                languageType: 'es',
            });
        }

        return updateMagazines;
    }


    @ApiResponse({
        status: 200,
        type: Magazines,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteMagazines(@Param('id') id: string) {

        const deleteMagazines = await this._magazinesServices.destroyMagazines(id);

        if (!deleteMagazines) {
            throw this.appUtilsService.httpCommonError('Magazines no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM112',
                languageType: 'es',
            });
        }

        return deleteMagazines;
    }
}