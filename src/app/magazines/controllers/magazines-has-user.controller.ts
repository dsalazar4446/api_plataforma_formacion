import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { Controller, UseFilters, UseInterceptors, Post, Body, HttpStatus, Delete, Param, NotFoundException, Get } from "@nestjs/common";
import { MagazinesHasUsersServices } from "../services/magazines-has-user.service";
import { MagazinesHasUsersSaveJson } from "../interfaces/magazines-has-user.interface";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from "@nestjs/swagger";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller MagazinesHasUsersController
 * @Creado 09 Abril 2019
 */

@ApiUseTags('Module-Magazines')
@Controller('magazines-has-users')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class MagazinesHasUsersController {

    constructor(private readonly _magazinesHasUsersServices: MagazinesHasUsersServices,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyMagazines: MagazinesHasUsersSaveJson) {

        const createMagazines = await this._magazinesHasUsersServices.save(bodyMagazines);

        if (!createMagazines) {
            return this.appUtilsService.httpCommonError('MagazinesHasUsers no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM109',
                languageType: 'es',
            });
        }

        return createMagazines;

    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async getAll() {
        const data = await this._magazinesHasUsersServices.getAll();

        if (!data[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return data;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteMagazines(@Param('id') id: string) {

        const deleteMagazines = await this._magazinesHasUsersServices.destroy(id);

        if (!deleteMagazines) {
            return this.appUtilsService.httpCommonError('MagazinesHasUsers no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM110',
                languageType: 'es',
            });
        }

        return deleteMagazines;
    }

}