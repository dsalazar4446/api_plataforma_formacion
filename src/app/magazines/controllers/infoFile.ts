import { diskStorage } from 'multer';
import * as path from 'path';

export const nameFile =[
    { name: 'magazinePdfAttachment', maxCount: 1 }
];

export const validationFile ={
    storage: diskStorage({
        filename: (req, file, cb) => {

            let extValidas = ['.pdf', '.PDF'];

            if (extValidas.indexOf(path.extname(file.originalname)) < 0) {
                cb(`valid extensions:${extValidas.join(', ')} `);
                return;
            }
            const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
            if (file.fieldname === 'magazinePdfAttachment') {
                req.body.magazinePdfAttachment = `${randomName}${path.extname(file.originalname)}`;
            }
            cb(null, `${randomName}${path.extname(file.originalname)}`);
        },
        destination: './uploads/magazinePdfAttachment',
    }),
}