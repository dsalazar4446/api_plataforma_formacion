import { Module, HttpModule, Provider } from "@nestjs/common";
import { SharedModule } from "../shared/shared.module";
import { DatabaseModule } from "../database/database.module";
import { MagazinesHasUsersServices } from './services/magazines-has-user.service';
import { MagazinesServices } from './services/magazines.service';
import { MagazinesHasUsersController } from './controllers/magazines-has-user.controller';
import { MagazinesController } from './controllers/magazines.controller';
import { MagazinesHasUsers } from './models/magazines-has-user.model';
import { Magazines } from './models/magazines.model';


const models = [
  MagazinesHasUsers,
  Magazines
];
const controllers = [
  MagazinesHasUsersController,
  MagazinesController
];
const providers: Provider[] = [
  MagazinesHasUsersServices,
  MagazinesServices
];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers
})
export class MagazinesModule { }
