import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { BroadcastPrebuiltMessagesController } from './controllers/broadcast-prebuilt-message.controller';
import { BroadcastPrebuiltMessageService } from './services/broadcast-prebuilt-message.service';
import { BroadcastPrebuiltMessageModel } from './models/broadcast-prebuilt-message.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { PrebuiltUserModel } from '../prebuilt-users/models/prebuilt-users.model';
import { Categories } from '../categories/models/categories.model';
// tslint:disable-next-line: max-line-length
import { BroadcastPrebuiltMessageHasCategoriesModel } from '../broadcast-prebuilt-message-has-categories/models/broadcast-prebuilt-message-has-categories.model';
import { BroadcastModel } from '../broadcast/models/broadcast.model';
import { BroadcastMessageModel } from '../broadcast-message/models/broadcast-message.model';

const models = [
  BroadcastPrebuiltMessageModel,
  PrebuiltUserModel,
  Categories,
  BroadcastPrebuiltMessageHasCategoriesModel,
  BroadcastModel,
  BroadcastMessageModel
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [BroadcastPrebuiltMessagesController],
  providers: [BroadcastPrebuiltMessageService]
})
export class BroadcastPrebuiltMessageModule {}
