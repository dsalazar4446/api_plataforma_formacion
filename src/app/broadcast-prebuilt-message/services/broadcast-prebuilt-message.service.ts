import { Injectable, Inject } from '@nestjs/common';
import { BroadcastPrebuiltMessageSaveJson, BroadcastPrebuiltMessageUpdateJson } from '../interfaces/broadcast-prebuilt-message.interface';
import { BroadcastPrebuiltMessageModel } from '../models/broadcast-prebuilt-message.model';
import { PrebuiltUserModel } from '../../prebuilt-users/models/prebuilt-users.model';
import { Categories } from '../../categories/models/categories.model';
// tslint:disable-next-line: max-line-length
import { BroadcastPrebuiltMessageHasCategoriesModel } from '../../broadcast-prebuilt-message-has-categories/models/broadcast-prebuilt-message-has-categories.model';
import { completeUrl } from '../../shared/utils/completUrl';
@Injectable()
export class BroadcastPrebuiltMessageService {
    constructor(
        @Inject('BroadcastPrebuiltMessageModel') private readonly broadcastPrebuiltMessageModel: typeof BroadcastPrebuiltMessageModel,
        @Inject('PrebuiltUserModel') private readonly prebuiltUserModel: typeof PrebuiltUserModel,
        @Inject('Categories') private readonly categories: typeof Categories,
// tslint:disable-next-line: max-line-length
        @Inject('BroadcastPrebuiltMessageHasCategoriesModel') private readonly broadcastPrebuiltMessageHasCategoriesModel: typeof BroadcastPrebuiltMessageHasCategoriesModel,
    ) { }
    async findAll(): Promise<BroadcastPrebuiltMessageModel[]> {
        const result =  await this.broadcastPrebuiltMessageModel.findAll(
            {
                include: [
                    {
                        model: PrebuiltUserModel,
                    },
                    {
                        model: Categories
                    }
                ]
            }
        );
        result.map(
            (item)=>{
                item.dataValues.prebuiltUser.dataValues.avatar = completeUrl('prebuild/user', item.dataValues.prebuiltUser.dataValues.avatar)
            }
        )
        return result;
    }
    async findByCategoriesId(categories: string) {
        const category = categories.split(',');
        var result = [];
        await Promise.all(
            await category.map(
                async (auxCategory) =>{
                    const dataCategories = await this.categories.findByPk(auxCategory,{
                        include: [
                            {
                                model: BroadcastPrebuiltMessageHasCategoriesModel,
                            }
                        ]
                    });
                    const dataPrebuiltCategories: any =  dataCategories.dataValues.broadcastPrebuiltMessageHasCategoriesModel[0];
                    const dataPrebuiltMessages: any = await this.broadcastPrebuiltMessageModel.findAll(
                        {
                            include: [
                                {
                                    model: PrebuiltUserModel,
                                }
                            ],
                            where:{
                                id: dataPrebuiltCategories.dataValues.broadcastPrebuiltMes,
                            }
                        }
                    );  
                    dataPrebuiltMessages.map(
                        (item)=>{
// tslint:disable-next-line: max-line-length
                            item.dataValues.prebuiltUser.dataValues.avatar = completeUrl('prebuild/user', item.dataValues.prebuiltUser.dataValues.avatar)
                        }
                    )
                    const dataResult = {
                        'categoryId': dataCategories.dataValues.id,
                        'categoryName': dataCategories.dataValues.categoryName,
                        'prebuiltMessage': dataPrebuiltMessages,
                    }
                    result.push(dataResult);

                }
            )
        )
        return result;
    } 
    //TRAER LA INFORMACION DEL BROADCAST
    async findById(id: string): Promise<BroadcastPrebuiltMessageModel> {
        const result = await this.broadcastPrebuiltMessageModel.findByPk<BroadcastPrebuiltMessageModel>(id,
            {
                include: [
                    {
                        model: PrebuiltUserModel,
                    },
                    {
                        model: Categories
                    }
                ]
            }
        );
        result.dataValues.prebuiltUser.dataValues.avatar = completeUrl('prebuild/user', result.dataValues.prebuiltUser.dataValues.avatar);
        return result;
    } 
    async create(broadcastPrebuiltMessage: BroadcastPrebuiltMessageSaveJson): Promise<BroadcastPrebuiltMessageModel> {
        const result = await this.broadcastPrebuiltMessageModel.create<BroadcastPrebuiltMessageModel>(broadcastPrebuiltMessage, {
            returning: true,
        });
        const dataPrebuiltUser = await this.prebuiltUserModel.findByPk<PrebuiltUserModel>(result.dataValues.prebuiltUserId);
        dataPrebuiltUser.dataValues.avatar = completeUrl('prebuild/user', dataPrebuiltUser.dataValues.avatar);
        result.dataValues.prebuiltUser = dataPrebuiltUser.dataValues;
        return result;
    }
    async update(idBroadcastPrebuiltMessage: string, broadcastPrebuiltMessageUpdate: Partial<BroadcastPrebuiltMessageUpdateJson>){
        const result =  await this.broadcastPrebuiltMessageModel.update(broadcastPrebuiltMessageUpdate, {
            where: {
                id: idBroadcastPrebuiltMessage,
            },
            returning: true,
        });
        const dataPrebuiltUser = await this.prebuiltUserModel.findByPk<PrebuiltUserModel>(result[1][0].dataValues.prebuiltUserId);
        dataPrebuiltUser.dataValues.avatar = completeUrl('prebuild/user', dataPrebuiltUser.dataValues.avatar);
        result[1][0].dataValues.prebuiltUser = dataPrebuiltUser.dataValues;
        return result;
    }
    async deleted(idBroadcastPrebuiltMessage: string): Promise<any> {
        return await this.broadcastPrebuiltMessageModel.destroy({
            where: {
                id: idBroadcastPrebuiltMessage,
            },
        });
    }
}
