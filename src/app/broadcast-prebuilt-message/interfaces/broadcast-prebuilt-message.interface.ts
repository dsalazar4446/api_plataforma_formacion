import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum, IsOptional} from "class-validator";
export class BroadcastPrebuiltMessageSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    prebuiltUserId: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    broadcastPrebuiltMessage: string;
    @ApiModelProperty()
    @IsInt()
    @IsNotEmpty()
    broadcastPrebuiltMessageStatus: number;
    @IsOptional()
    prebuiltUser?: any;
}
// tslint:disable-next-line: max-classes-per-file
export class BroadcastPrebuiltMessageUpdateJson  extends BroadcastPrebuiltMessageSaveJson{
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}
