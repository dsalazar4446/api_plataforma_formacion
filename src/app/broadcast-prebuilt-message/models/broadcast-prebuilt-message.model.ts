import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey, BelongsTo, HasMany, BelongsToMany } from 'sequelize-typescript';
import { PrebuiltUserModel } from '../../prebuilt-users/models/prebuilt-users.model';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
import { BroadcastHasBroadcastPrebuiltMessagesModel } from '../../broadcasts_has_broadcast_prebuilt_messages/models/broadcasts_has_broadcast_prebuilt_messages.model';
import { Categories } from '../../categories/models/categories.model';
import { BroadcastPrebuiltMessageHasCategoriesModel } from '../../broadcast-prebuilt-message-has-categories/models/broadcast-prebuilt-message-has-categories.model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'broadcast_prebuilt_messages',
})
export class BroadcastPrebuiltMessageModel extends Model<BroadcastPrebuiltMessageModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => PrebuiltUserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'prebuilt_user_id',
    })
    prebuiltUserId: string;

    @ApiModelProperty()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        field: 'broadcast_prebuilt_message',
    })
    broadcastPrebuiltMessage: string;

    @ApiModelProperty()
    @Column({
        type: DataType.SMALLINT,
        allowNull: false,
        field: 'broadcast_prebuilt_message_status',
    })
    broadcastPrebuiltMessageStatus: number;

    @BelongsTo(() => PrebuiltUserModel)
    prebuiltUser: PrebuiltUserModel;

    @BelongsToMany(() => Categories, {
        through: {
            model: () => BroadcastPrebuiltMessageHasCategoriesModel,
            unique: true,
        }
    })
    categories: Categories[];

    @BelongsToMany(() => BroadcastPrebuiltMessageModel, {
        through: {
            model: () => BroadcastPrebuiltMessageHasCategoriesModel,
            unique: true,
        }
    })
    BroadcastPrebuiltMessageModel: BroadcastPrebuiltMessageModel[];

    @BelongsToMany(() => BroadcastModel, {
        through: {
            model: () => BroadcastHasBroadcastPrebuiltMessagesModel,
            unique: true,
        }
    })
    broadcastModel: BroadcastModel[];


    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;




}
