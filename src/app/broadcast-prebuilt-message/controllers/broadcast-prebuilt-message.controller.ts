import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, Req} from '@nestjs/common';
import { BroadcastPrebuiltMessageService } from '../services/broadcast-prebuilt-message.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { BroadcastPrebuiltMessageSaveJson, BroadcastPrebuiltMessageUpdateJson } from '../interfaces/broadcast-prebuilt-message.interface';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { BroadcastPrebuiltMessageModel } from '../models/broadcast-prebuilt-message.model';
import { UserModel } from '../../user/models/user.Model';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { BroadcastMessageModel } from '../../broadcast-message/models/broadcast-message.model';
import { PrebuiltUserModel } from '../../prebuilt-users/models/prebuilt-users.model';
@Controller('broadcast-prebuilt-messages')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// @UseGuards(new JwtAuthGuard())
export class BroadcastPrebuiltMessagesController {
    constructor(
        private readonly broadcastPrebuiltMessageService: BroadcastPrebuiltMessageService,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('BroadcastModel') private readonly broadcastModel: typeof BroadcastModel,
        @Inject('BroadcastMessageModel') private readonly broadcastMessageModel: typeof BroadcastMessageModel,
        @Inject('BroadcastPrebuiltMessageModel') private readonly broadcastPrebuiltMessageModel: typeof BroadcastPrebuiltMessageModel,
        @Inject('PrebuiltUserModel') private readonly prebuiltUserModel: typeof PrebuiltUserModel,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        @ApiResponse({
            status: 200,
            type: BroadcastPrebuiltMessageModel,
          })
        @Post()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async create(@Body() broadcastPrebuiltMessage: BroadcastPrebuiltMessageSaveJson, @Req() req) {
            const data = await this.prebuiltUserModel.findByPk(broadcastPrebuiltMessage.prebuiltUserId);
            if(data == null){
                  throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                      messageCode: 'EA29',
                      languageType:  req.headers.language,
                  });
            }
            return await this.broadcastPrebuiltMessageService.create(broadcastPrebuiltMessage);
        }
        @Get()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: BroadcastPrebuiltMessageModel,
            isArray: true
          })
        async getAll() {
            return await this.broadcastPrebuiltMessageService.findAll();
        }
        @ApiResponse({
            status: 200,
            type: BroadcastPrebuiltMessageModel,
          })
        @ApiImplicitParam({ name: 'categoriesId', required: true, type: 'string' })
        @Get('/categories/:categoriesId')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async findByCategoriesId(@Param('categoriesId') categoriesId) {
            return await this.broadcastPrebuiltMessageService.findByCategoriesId(categoriesId);
        }
        @ApiResponse({
            status: 200,
            type: BroadcastPrebuiltMessageModel,
          })
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Get(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async findById(@Param('id') idBroadcastPrebuiltMessageService) {
            return await this.broadcastPrebuiltMessageService.findById(idBroadcastPrebuiltMessageService);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Put(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: BroadcastPrebuiltMessageModel,
          })
        async update(
            @Body()
            updateBroadcastPrebuiltMessage: Partial<BroadcastPrebuiltMessageUpdateJson>, 
            @Param('id') idBroadcastPrebuiltMessage,
            @Req() req
        ) {
            if(updateBroadcastPrebuiltMessage.prebuiltUserId){
                const data = await this.prebuiltUserModel.findByPk(updateBroadcastPrebuiltMessage.prebuiltUserId);
                if(data == null){
                    throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                        messageCode: 'EA29',
                        languageType:  req.headers.language,
                    });
                }
            }
            const data2= await this.broadcastPrebuiltMessageModel.findByPk(idBroadcastPrebuiltMessage);
            if(data2 == null){
                  throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                      messageCode: 'EA30',
                      languageType:  req.headers.language,
                  });
            }
            
            return await this.broadcastPrebuiltMessageService.update(idBroadcastPrebuiltMessage, updateBroadcastPrebuiltMessage);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Delete(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async delete(@Param('id') idBroadcastPrebuiltMessage) {
            return await this.broadcastPrebuiltMessageService.deleted(idBroadcastPrebuiltMessage);
        }
}
