import { Test, TestingModule } from '@nestjs/testing';
import { RoleEndpointsService } from './role-endpoints.service';

describe('RoleEndpointsService', () => {
  let service: RoleEndpointsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RoleEndpointsService],
    }).compile();

    service = module.get<RoleEndpointsService>(RoleEndpointsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
