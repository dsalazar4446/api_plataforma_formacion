import * as Bluebird from 'bluebird';
import { CreateRoleEndpoints } from '../../dto/create-role-endpoint';
import { Inject, Injectable, HttpStatus } from '@nestjs/common';
import { RoleEndpointsModel } from '../../models/role-endpoints.model';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";

@Injectable()
export class RoleEndpointsService {
    constructor(
        @Inject('RoleEndpointsModel') private readonly roleEndpointsModel: typeof RoleEndpointsModel,
        private readonly appUtilsService: AppUtilsService,
    ) {}

    /**
     * @author Daniel Salazar
     * @description Crea un recurso
     * @method createRoleEndpoints
     * @memberof RoleEndpointsService
     * @param role
     */
    async createRoleEndpoints(role: CreateRoleEndpoints) {
        const rol = new RoleEndpointsModel(role);
        return await rol.save();
    }

    /**
     * @author Daniel Salazar
     * @description Busca todos los recursos
     * @method findRolesEndpoints
     * @memberof RoleEndpointsService
     */
    async findRolesEndpoints() {
        return await this.roleEndpointsModel.findAll();
    }
    /**
     * @author Daniel Salazar
     * @description Busca un recurso por id
     * @method findRoleEndpointsById
     * @memberof RoleEndpointsService
     * @param id
     */
    findRoleEndpointsById(param) {
        return this.roleEndpointsModel.findById(param.id);
    }
    /**
     * @author Daniel Salazar
     * @description Actualiza el recurso
     * @method updateRoleEndpoints
     * @memberof RoleEndpointsService
     * @param role
     */
    async updateRoleEndpoints(role: any) {
        await this.roleEndpointsModel.update(role, { where: { id: role.id } });
    }

    /**
     * @author Daniel Salazar
     * @description Elimina un recurso
     * @method deleteRoleEndpoints
     * @memberof RoleEndpointsService
     * @param id
     */

    deleteRoleEndpoints(id) {
        return this.roleEndpointsModel.destroy({
            where: { id },
        });
    }

    async checkRoleEndpoint(rolesId: string, roleEndpoint: string){
        const result = await this.roleEndpointsModel.find({
             where: {
                 rolesId,
                 roleEndpoint,
             }
        });
        if(!result){
            return  this.appUtilsService.httpCommonError('No puedes acceder a este endpoint', HttpStatus.UNAUTHORIZED);
        } else {
            return this.appUtilsService.httpCommonError('Acceso permitido', HttpStatus.OK);
        }
     }
}
