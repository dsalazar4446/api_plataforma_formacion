import { Test, TestingModule } from '@nestjs/testing';
import { RoleViewTextsService } from './role-view-texts.service';

describe('RoleViewTextsService', () => {
  let service: RoleViewTextsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RoleViewTextsService],
    }).compile();

    service = module.get<RoleViewTextsService>(RoleViewTextsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
