import { Injectable, Inject } from '@nestjs/common';
import { CreateRoleViewText } from '../../dto/create-role-view-text.dto';
import { RoleViewTextModel } from '../../models/role-view-text.model';

@Injectable()
export class RoleViewTextsService {
    constructor(@Inject('RoleViewTextModel') private readonly roleViewTextModel: typeof RoleViewTextModel) {}

    async create(text: CreateRoleViewText){
        const createText = new RoleViewTextModel(text);
        return await createText.save();
    }

    async list(languageType: string){
        return await this.roleViewTextModel.findAll({
            where: {languageType}
        });
    }
    
    async detail(param, languageType) {
        return await this.roleViewTextModel.findById(param.id, {
            where:{
                languageType: languageType,
            }
        });
    }

    async update(param){
        return await this.roleViewTextModel.update(param, {where:{id: param.id}});
    }
    delete(id) {
        this.roleViewTextModel.destroy({where: {id}});
    }
}
