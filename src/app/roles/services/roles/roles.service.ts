import * as Bluebird from 'bluebird';
import { CreateRoleDto } from '../../dto/create-role.dto';
import { Inject, Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { RolesModel } from '../../models/roles.models';
import { AuthGuard } from '@nestjs/passport';
import { CONFIG } from '../../../../config';
import * as ShortUniqueId from 'short-unique-id';
import * as path from 'path';
import * as fs from 'fs';
import { RoleViewsModel } from '../../models/role-views.model';
import { RoleEndpointsModel } from '../../models/role-endpoints.model';
@Injectable()
export class RolesService {
    constructor(
        @Inject('RolesModel') private readonly rolesModel: typeof RolesModel,
        ) {}
        /**
         * @author Daniel Salazar
         * @description Crea un recurso
         * @method createRole
         * @memberof RolesService
         * @param role
         */
        async createRole(role: CreateRoleDto ) {
            const uid = new ShortUniqueId();
            role.roleCode = uid.randomUUID(9);
            const result = await this.rolesModel.create(role);
            result.roleIcon = CONFIG.storage.server +'roles/'+ result.roleIcon;
            return result;
        }
        /**
         * @author Daniel Salazar
         * @description Busca todos los recursos
         * @method findRoles
         * @memberof RolesService
         */
        async findRoles() {
            const roles = await this.rolesModel.findAll({
                include:[RoleViewsModel, RoleEndpointsModel]
            });
            if(roles.length > 0){
                roles.map(
                    role => {
                        role.roleIcon = CONFIG.storage.server + 'roles/' + role.roleIcon;
                    },
                );
            }
            return roles;
        }
        /**
         * @author Daniel Salazar
         * @description Busca un recurso por id
         * @method findRoles
         * @memberof RolesService
         * @param id
         */
        async findRoleById(param) {
            const role = await this.rolesModel.findByPk(param.id,/** {include: [RoleViewsModel]}*/);
            if(role){
                role.roleIcon = CONFIG.storage.server + 'roles/' +  role.roleIcon;
            }
            return role;
        }
         /**
          * @author Daniel Salazar
          * @description Actualiza el recurso
          * @method updateRole
          * @memberof RolesService
          * @param role
          */

        async findStudentRoleId(){
            const role = await this.rolesModel.findOne({ where: {roleText: 'Student'}});
            if (role) {
                return role.id;
            }
            return null;
        }
        async updateRole(role: any) {

            if( role.roleIcon ){
                await this.deleteImg(role.id)
            }

            const result = await this.rolesModel.update(role, { where: { id: role.id, include: [RoleViewsModel, RoleEndpointsModel]}, returning: true});
            result[1][0].roleIcon = CONFIG.storage.server + 'roles/' +  result[1][0].roleIcon;
            return result[1][0];
        }

        /**
         * @author Daniel Salazar
         * @description Elimina un recurso
         * @method deleteRoles
         * @memberof RolesService
         * @param id
         */

        async deleteRole(id) {

            await this.deleteImg(id)
            return this.rolesModel.destroy({
                where: {id},
            });
        }

        private async deleteImg(id: string){
            const img = await this.rolesModel.findByPk(id).then(item => item.roleIcon).catch(err => err);
        
            const pathImagen = path.resolve(__dirname, `../../../../../uploads/roles/${img}`);
    
            if (fs.existsSync(pathImagen)) {
                fs.unlinkSync(pathImagen);
            }
        }

}
