import * as Bluebird from 'bluebird';
import { CreateRoleViewsDto } from './../../dto/create-role-views.dto';
import { Inject, Injectable, HttpCode, HttpStatus } from '@nestjs/common';
import { RoleViewsModel } from '../../models/role-views.model';
import { UserModel } from '../../../user/models/user.Model';
import { RolesModel } from '../../models/roles.models';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import * as fs from 'fs';
import * as path from 'path';
import { UpdateRoleViewsDto } from '../../dto/update-role-views.dto';
import { completeUrl } from '../../../shared/utils/completUrl';
import { RoleViewTextModel } from '../../models/role-view-text.model';

@Injectable()
export class RoleViewService {
    constructor(
        @Inject('RoleViewsModel') private readonly roleViewsModel: typeof RoleViewsModel,
        private readonly appUtilsService: AppUtilsService,
    ) {}

    /**
     * @author Daniel Salazar
     * @description Crea un recurso
     * @method createRole
     * @memberof RoleViewService
     * @param role
     */
    async createRoleViews(role: CreateRoleViewsDto) {
        const rol = new RoleViewsModel(role);
        const result = await rol.save();
        result.roleViewIcon = completeUrl('roles/roleView', result.roleViewIcon);
        result.roleViewImage = completeUrl('roles/roleView', result.roleViewImage);
        return result;
    }

    /**
     * @author Daniel Salazar
     * @description Busca todos los recursos
     * @method findRoles
     * @memberof findRolesViews
     */
    async findRolesViews() {
        return await this.roleViewsModel.findAll({
            include:[RoleViewTextModel]
        });
    }
    /**
     * @author Daniel Salazar
     * @description Busca un recurso por id
     * @method findRoleViewsById
     * @memberof RoleViewService
     * @param id
     */
    async findRoleViewsById(param) {
        const result =  await this.roleViewsModel.findById(param.id,{
            include: [RoleViewTextModel]
        });
        result.roleViewIcon = completeUrl('roles/roleView', result.roleViewIcon);
        result.roleViewImage = completeUrl('roles/roleView',result.roleViewImage);
    }
    /**
     * @author Daniel Salazar
     * @description Actualiza el recurso`
     * @method updateRoleViews
     * @memberof RoleViewService
     * @param role
     */
    async updateRoleViews(roleView: UpdateRoleViewsDto) {
        if (roleView.roleViewImage && roleView.roleViewIcon) {
            await this.deleteImg(roleView.id);
        }
        const result = await this.roleViewsModel.update(roleView, { where: { id: roleView.id }, returning: true });
        result[1][0].roleViewIcon = completeUrl('roles/roleView', result[1][0].roleViewIcon);
        result[1][0].roleViewImage = completeUrl('roles/roleView', result[1][0].roleViewImage);
    }

    /**
     * @author Daniel Salazar
     * @description Elimina un recurso
     * @method deleteRoleViwes
     * @memberof RoleViewService
     * @param id
     */

    async deleteRoleViwes(id) {
        await this.deleteImg(id);
        return await this.roleViewsModel.destroy({
            where: { id },
        });
    }
    async checkRoleView(rolesId: string, roleView: string){
       const result = await this.roleViewsModel.find({
            where: {
                rolesId,
                roleView,
            }
       });
       if(!result){
            return  this.appUtilsService.httpCommonError('No puedes acceder a esta vista', HttpStatus.UNAUTHORIZED);
       } else {
        return this.appUtilsService.httpCommonError('Acceso permitido', HttpStatus.OK);
       }
    }
    private async deleteImg(id: string) {
        let img = await this.roleViewsModel.findByPk(id).then(item => item.roleViewIcon).catch(err => err);
        let pathImagen = path.resolve(__dirname, `../../../../../uploads/roles/roleView/${img}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
        img = await this.roleViewsModel.findByPk(id).then(item => item.roleViewIcon).catch(err => err);
        pathImagen = path.resolve(__dirname, `../../../../../uploads/roles/roleView/${img}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }
}
