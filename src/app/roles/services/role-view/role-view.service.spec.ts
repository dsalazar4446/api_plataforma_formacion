import { Test, TestingModule } from '@nestjs/testing';
import { RoleViewService } from './role-view.service';

describe('RoleViewService', () => {
  let service: RoleViewService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RoleViewService],
    }).compile();

    service = module.get<RoleViewService>(RoleViewService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
