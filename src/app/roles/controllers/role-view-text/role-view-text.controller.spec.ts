import { Test, TestingModule } from '@nestjs/testing';
import { RoleViewTextController } from './role-view-text.controller';

describe('RoleViewText Controller', () => {
  let controller: RoleViewTextController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RoleViewTextController],
    }).compile();

    controller = module.get<RoleViewTextController>(RoleViewTextController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
