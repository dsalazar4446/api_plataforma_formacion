import { Controller, Post, Body, UseInterceptors, Get, Param, Put, UsePipes, UseFilters, UseGuards, Delete, FileFieldsInterceptor, Req } from '@nestjs/common';
import { RoleViewTextsService } from '../../services/role-view-texts/role-view-texts.service';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { CreateRoleViewText } from '../../dto/create-role-view-text.dto';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { RoleViewTextModel } from '../../models/role-view-text.model';
@UseFilters(new AppExceptionFilter())
@ApiUseTags('Roles')
// // @UseGuards(new JwtAuthGuard())
@Controller('role-view-text')
export class RoleViewTextController {
    constructor(private readonly roleViewText: RoleViewTextsService) {}

    @Post()
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: RoleViewTextModel})
    @UseInterceptors(new AppResponseInterceptor())
    create(@Body() body: CreateRoleViewText, @Req() req){

        body.languageType = req.headers.language
        return this.roleViewText.create(body);
    }

    @Get('list')
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: RoleViewTextModel})
    @UseInterceptors(new AppResponseInterceptor())
    list(@Body() body: CreateRoleViewText, @Req() req){
        return this.roleViewText.list(req.headers.language);
    }

    @Get(':id')
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: RoleViewTextModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(new AppResponseInterceptor())
    detail(@Param() param, @Req() req){
        return this.roleViewText.detail(param, req.headers.language);
    }

    @Put()
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: RoleViewTextModel})
    update(@Body() body, @Req() req){
        body.languageType = req.headers.language
        return this.roleViewText.update(body);
    }

    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(new AppResponseInterceptor())
    delete(@Param() param){
        return this.roleViewText.delete(param.id);
    }
}
