import { Test, TestingModule } from '@nestjs/testing';
import { RoleViewsController } from './role-views.controller';

describe('RoleViews Controller', () => {
  let controller: RoleViewsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RoleViewsController],
    }).compile();

    controller = module.get<RoleViewsController>(RoleViewsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
