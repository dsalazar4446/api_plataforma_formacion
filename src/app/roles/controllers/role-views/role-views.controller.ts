import { CreateRoleViewsDto } from './../../dto/create-role-views.dto';
import { RoleViewService } from './../../services/role-view/role-view.service';
import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    UseInterceptors,
    UsePipes,
    UseFilters,
    UseGuards,
    FileFieldsInterceptor,
    } from '@nestjs/common';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { UpdateRoleViewsDto } from '../../dto/update-role-views.dto';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiConsumes, ApiImplicitFile, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { diskStorage } from 'multer';
import { extname } from "path";// // @UseGuards(new JwtAuthGuard())
import { RoleViewsModel } from '../../models/role-views.model';
@ApiUseTags('Roles')
@Controller('role-views')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(new AppResponseInterceptor())
export class RoleViewsController {
    constructor(private readonly roleViewService: RoleViewService) {}

    /**
     * @author Daniel Salazar
     * @method createRoleViews
     * @description Crea un role view
     * @param role
     */

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: RoleViewsModel })
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'roleViewImage', required: true, description: 'Imagen del role view' })
    @ApiImplicitFile({ name: 'roleViewIcon', required: true, description: 'Icono del role view' })
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'roleViewImage', maxCount: 1 },
        { name: 'roleViewIcon', maxCount: 1 },
    ], {
            storage: diskStorage({
                destination: './uploads/roles/roleView'
                , filename: (req, file, cb) => {
                    let extValidas = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG'];
                    
                    if(extValidas.indexOf(extname(file.originalname)) < 0) {
                        cb('valid extensions: ' + extValidas.join(', '));
                        return;
                    }
                    const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                    if (file.fieldname === 'roleViewImage') {
                        req.body.roleIcon = `${randomName}${extname(file.originalname)}`;
                    }
                    if (file.fieldname === 'roleViewIcon') {
                        req.body.roleIcon = `${randomName}${extname(file.originalname)}`;
                    }
                    cb(null, `${randomName}${extname(file.originalname)}`)
                },
            }),
        }))
    async createRoleViews(@Body() role: CreateRoleViewsDto) {
        return await this.roleViewService.createRoleViews(role);
    }

    /**
     * @author Daniel Salazar
     * @method getRoleViews
     * @description Obtiene todos los role view
     * @param
     */
    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: RoleViewsModel })
    async getRoleViews() {
        return await this.roleViewService.findRolesViews();
    }
    /**
     * @author Daniel Salazar
     * @method getRoleViewsById
     * @description Obtiene un rol view mediante su id
     * @param param
     */
    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: RoleViewsModel })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async getRoleViewsById(@Param() param) {
        return await this.roleViewService.findRoleViewsById(param);
    }

    /**
     * @author Daniel Salazar
     * @method updateRole
     * @description Actualiza un role view
     * @param role
     */
    @Put()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: RoleViewsModel })
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'roleViewImage', required: true, description: 'Imagen del role view' })
    @ApiImplicitFile({ name: 'roleViewIcon', required: true, description: 'Icono del role view' })
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'roleViewImage', maxCount: 1 },
        { name: 'roleViewIcon', maxCount: 1 },
    ], {
            storage: diskStorage({
                destination: './uploads/roles/roleView'
                , filename: (req, file, cb) => {
                    let extValidas = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG'];
                    
                    if(extValidas.indexOf(extname(file.originalname)) < 0) {
                        cb('valid extensions: ' + extValidas.join(', '));
                        return;
                    }
                    const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                    if (file.fieldname === 'roleViewImage') {
                        req.body.roleIcon = `${randomName}${extname(file.originalname)}`;
                    }
                    if (file.fieldname === 'roleViewIcon') {
                        req.body.roleIcon = `${randomName}${extname(file.originalname)}`;
                    }
                    cb(null, `${randomName}${extname(file.originalname)}`)
                },
            }),
        }))
    async updateRoleViews(@Body() role: UpdateRoleViewsDto) {
        return await this.roleViewService.updateRoleViews(role);
    }

    /**
     * @author Daniel Salazar
     * @method deleteRole
     * @description Elimina un role view
     * @param param
     */
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    
    async deleteRoleViews(@Param() param) {
        return await this.roleViewService.deleteRoleViwes(param.id);
    }
    @Get('check/:id/:viewName')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: RoleViewsModel })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @ApiImplicitParam({ name: 'viewName', required: true, type: 'string' })
    
    check(@Param() params){
        return this.roleViewService.checkRoleView(params.id, params.viewName);
    }
}
