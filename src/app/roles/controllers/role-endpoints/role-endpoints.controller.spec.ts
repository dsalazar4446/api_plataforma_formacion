import { RoleEndpointsController } from '../role-endpoints/role-endpoints.controller';
import { Test, TestingModule } from '@nestjs/testing';

describe('RoleEndpoints Controller', () => {
  let controller: RoleEndpointsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RoleEndpointsController],
    }).compile();

    controller = module.get<RoleEndpointsController>(RoleEndpointsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
