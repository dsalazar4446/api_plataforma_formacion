import { CreateRoleEndpoints } from './../../dto/create-role-endpoint';
import { RoleEndpointsService } from './../../services/role-endpoints/role-endpoints.service';
import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    UseInterceptors,
    UsePipes,
    UseFilters,
    UseGuards,
    Headers,
    } from '@nestjs/common';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { RoleEndpointsModel } from '../../models/role-endpoints.model';
import { UpdateRoleEndopoints } from '../../dto/update-role-endpoint';
@UseFilters(new AppExceptionFilter())
@ApiUseTags('Roles')
// // @UseGuards(new JwtAuthGuard())
@Controller('role-endpoints')
export class RoleEndpointsController {
    constructor(private readonly roleEndpointsService: RoleEndpointsService) {}
    /**
     * @author Daniel Salazar
     * @method createRoleEndpoints
     * @description Crea un role endpoint
     * @param role
     */

    @Post()
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: RoleEndpointsModel})
    @UseInterceptors(new AppResponseInterceptor())
    async createRoleEndpoints(@Body() role: CreateRoleEndpoints) {
        return await this.roleEndpointsService.createRoleEndpoints(role);
    }

    /**
     * @author Daniel Salazar
     * @method getRoleEndpoints
     * @description Obtiene todos los role endpoint
     * @param
     */
    @Get()
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: RoleEndpointsModel})
    @UseInterceptors(new AppResponseInterceptor())
    async getRoleEndpoints() {
        return await this.roleEndpointsService.findRolesEndpoints();
    }
    /**
     * @author Daniel Salazar
     * @method getRoleEndpointsById
     * @description Obtiene un rol endpoint mediante su id
     * @param param
     */
    @Get(':id')
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: RoleEndpointsModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(new AppResponseInterceptor())
    async getRoleEndpointsById(@Param() param) {
        return await this.roleEndpointsService.findRoleEndpointsById(param);
    }

    /**
     * @author Daniel Salazar
     * @method updateRoleEndpoints
     * @description Actualiza un role endpoint
     * @param role
     */
    @Put()
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: RoleEndpointsModel})
    async updateRoleEndpoints(@Body() role:UpdateRoleEndopoints) {
        return await this.roleEndpointsService.updateRoleEndpoints(role);
    }

    /**
     * @author Daniel Salazar
     * @method deleteRoleEndpoints
     * @description Elimina un role endpoint
     * @param param
     */
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(new AppResponseInterceptor())
    async deleteRoleEndpoints(@Param() param) {
        return await this.roleEndpointsService.deleteRoleEndpoints(param.id);
    }

    @Get('check/:roleId/:endpoint')
    @ApiImplicitParam({ name: 'roleId', required: true, type: 'string' })
    @ApiImplicitParam({ name: 'endpoint', required: true, type: 'string' })
    @UseInterceptors(new AppResponseInterceptor())
    check(@Param() param){
        return this.roleEndpointsService.checkRoleEndpoint(param.roleId, param.endpoint)
    }
}
