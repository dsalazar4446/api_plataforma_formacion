import { CreateRoleDto } from './../../dto/create-role.dto';
import { RolesService } from './../../services/roles/roles.service';
import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    UsePipes,
    FileFieldsInterceptor,
    UseInterceptors,
    UseGuards,
    UseFilters,
    HttpException,
    HttpCode,
    HttpStatus,
    } from '@nestjs/common';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import * as path from 'path';
import { diskStorage } from 'multer';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiImplicitFile, ApiConsumes, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { RolesModel } from '../../models/roles.models';

@UseFilters(new AppExceptionFilter())
// @UseGuards(new JwtAuthGuard())
@ApiUseTags('Roles')
@Controller('roles')
export class RolesController {
    constructor(private readonly rolesService: RolesService) {}
    /**
     * @author Daniel Salazar
     * @method createRole
     * @description Crea un rol
     * @param role
     */
    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: RolesModel })
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({name:'roleIcon',description:'icono de rol',required: true})
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'roleIcon', maxCount: 1 },
    ], {
    storage: diskStorage({
        destination: './uploads/roles/roleIcon'
      , filename: (req, file, cb) => {
          let extValidas = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG'];

          if (extValidas.indexOf(path.extname(file.originalname)) < 0) {
              cb('valid extensions: ' + extValidas.join(', '));
              return
          }
          const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
          if(file.fieldname === 'roleIcon'){
            req.body.roleIcon = `${randomName}${path.extname(file.originalname)}`;
        }
          cb(null, `${randomName}${path.extname(file.originalname)}`)
      },
    }),
  }))
   
    async createRole(@Body() role: CreateRoleDto) {
        return await this.rolesService.createRole(role);
    }

    /**
     * @author Daniel Salazar
     * @method getRole
     * @description Obtiene todos los roles
     * @param
     */
    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: RolesModel })
    async getRole() {
        const roles = await this.rolesService.findRoles();
        if (roles.length < 0) {
            throw new HttpException('no se encontraron roles', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        return await this.rolesService.findRoles();

    }

    /**
     * @author Daniel Salazar
     * @method getRoleById
     * @description Obtiene un rol mediante su id
     * @param param
     */
    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: RolesModel })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async getRoleById(@Param() param) {
        const role = await this.rolesService.findRoleById(param);
        if(!role){
            throw new HttpException('Role no encontrado',HttpStatus.UNPROCESSABLE_ENTITY)
        }
        return role;
    }
    /**
     * @author Daniel Salazar
    * @method updateRole
     * @description Actualiza un role
     * @param role
     */
    @Put()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: RolesModel })
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'roleIcon', description: 'icono de rol', required: true })
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'roleIcon', maxCount: 1 },
    ], {
    storage: diskStorage({
        destination: './uploads/roles/roleIcon'
      , filename: (req, file, cb) => {
        let extValidas = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG'];
        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
        if(file.fieldname === 'roleIcon'){
            req.body.roleIcon = `${randomName}${path.extname(file.originalname)}`;
        }
        cb(null, `${randomName}${path.extname(file.originalname)}`)
      },
    }),
  }))
    async updateRole(@Body() role) {
        return await this.rolesService.updateRole(role);
    }

    /**
     * @author Daniel Salazar
     * @method deleteRole
     * @description Elimina un rol
     * @param param
     */
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteRole(@Param() param) {
        return await this.rolesService.deleteRole(param.id);
    }

}
