import { DatabaseModule } from '../database/database.module';
import { Module, forwardRef } from '@nestjs/common';
import { RoleEndpointsController } from './controllers/role-endpoints/role-endpoints.controller';
import { RoleEndpointsService } from './services/role-endpoints/role-endpoints.service';
import { RolesController } from './controllers/roles/roles.controller';
import { RolesService } from './services/roles/roles.service';
import { RoleViewsController } from './controllers/role-views/role-views.controller';
import { RoleViewService } from './services/role-view/role-view.service';
import { RoleViewTextsService } from './services/role-view-texts/role-view-texts.service';
import { RoleViewTextController } from './controllers/role-view-text/role-view-text.controller';
import { RolesModel } from './models/roles.models';
import { RoleEndpointsModel } from './models/role-endpoints.model';
import { RoleViewsModel } from './models/role-views.model';
import { RoleViewTextModel } from './models/role-view-text.model';
import { SharedModule } from '../shared/shared.module';

const models = [
  RolesModel,
  RoleEndpointsModel,
  RoleViewsModel,
  RoleViewTextModel,
];

@Module({
  imports: [
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
    forwardRef(() => SharedModule),
  ],
  providers: [
    RolesService,
    RoleEndpointsService,
    RoleViewService,
    RoleViewTextsService,
  ],
  controllers: [
    RolesController,
    RoleEndpointsController,
    RoleViewsController,
    RoleViewTextController,
  ],
  exports: [RoleEndpointsService, RoleViewService, RolesService]
})
export class RolesModule {}
