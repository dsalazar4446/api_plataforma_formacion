export const RoleViewExample = 
{
    "id": "string",
    "rolesId": "string",
    "roleView": "string",
    "roleViewWelcome": "boolean",
    "roleViewImage": "string",

}

export const RoleViewExampleArray = 
[
    RoleViewExample
]
