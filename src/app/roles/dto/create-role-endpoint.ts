import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsString } from 'class-validator';

export class CreateRoleEndpoints {
    // id: string;
    @ApiModelProperty()
    @IsUUID('4')
    rolesId: string;
    @ApiModelProperty()
    @IsString()
    roleEndpoint: string;
}
