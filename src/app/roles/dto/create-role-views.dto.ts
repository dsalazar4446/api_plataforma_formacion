import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString, IsBoolean } from "class-validator";

export class CreateRoleViewsDto {
    // id: string;
    @ApiModelProperty()
    @IsUUID('4')
    rolesId: string;
    @ApiModelProperty()
    @IsString()
    roleView: string;
    @ApiModelProperty()
    @IsBoolean()
    roleViewWelcome: boolean;
    @ApiModelProperty()
    @IsString()
    roleViewImage: string;
    @ApiModelProperty()
    @IsString()
    roleViewIcon: string;

}
