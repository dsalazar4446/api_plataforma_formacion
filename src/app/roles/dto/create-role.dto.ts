import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";
import { IsString, IsOptional } from "class-validator";

/**
 * @author Daniel Salazar
 * @description Objeto de transferencia de datos para roles
 * @class
 */
export class CreateRoleDto {
    // id: string;
    @ApiModelPropertyOptional()
    @IsOptional()
    roleCode?: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    roleIcon?: string;
    @ApiModelProperty()
    @IsString()
    roleText: string;
}
