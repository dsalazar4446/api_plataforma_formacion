import { CreateRoleEndpoints } from "./create-role-endpoint";
import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID } from "class-validator";


export class UpdateRoleEndopoints extends CreateRoleEndpoints{
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}
