import { CreateRoleViewText } from "./create-role-view-text.dto";
import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID } from "class-validator";

export class UpdateRoleViewText extends CreateRoleViewText {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}