import { CreateRoleViewsDto } from "./create-role-views.dto";
import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID } from "class-validator";

export class UpdateRoleViewsDto extends CreateRoleViewsDto {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}
