import { CreateRoleDto } from "./create-role.dto";
import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID } from "class-validator";

export class UpdateRoleDto extends CreateRoleDto {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}
 