import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsString, IsOptional } from 'class-validator';

export class CreateRoleViewText {
    @ApiModelProperty()
    @IsUUID('4')
    roleViewid: string;
    @ApiModelProperty()
    @IsString()
    roleViewText: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    languageType: string;
}