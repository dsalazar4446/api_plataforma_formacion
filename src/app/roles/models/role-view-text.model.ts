import { Table, Model, Column, DataType, ForeignKey, CreatedAt, UpdatedAt, BelongsTo } from 'sequelize-typescript';
import { RoleViewsModel } from './role-views.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'role_view_texts',
})
/**
 * @author Daniel Salazar
 * @description Modelo de conexion de la tabla role_view_text
 * @type class
 */
export class RoleViewTextModel extends Model<RoleViewTextModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'role_views_id',
    })
    public roleViewid: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => RoleViewsModel)
    @Column({
        type: DataType.STRING,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'role_view_text',
    })
    public roleViewText: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es','en','it','pr'),
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    public languageType: string;
    
    @BelongsTo(() => RoleViewsModel)
    roleViews: RoleViewsModel;
    
    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}