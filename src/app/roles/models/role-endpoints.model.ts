import {
    BelongsTo,
    Column,
    DataType,
    ForeignKey,
    Model,
    Table,
    CreatedAt,
    UpdatedAt,
} from 'sequelize-typescript';
import { RolesModel } from './roles.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'role_endpoints',
})
/**
 * @author Daniel Salazar
 * @description Modelo de conexion de la tabla role-epoint
 * @type class
 */
export class RoleEndpointsModel extends Model<RoleEndpointsModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        unique: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => RolesModel)
    @Column({
        type: DataType.STRING,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'roles_id',
    })
    public rolesId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'role_endpoint',
    })
    public roleEndpoint: string;
    
    @BelongsTo(() => RolesModel)
    roles: RolesModel;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
