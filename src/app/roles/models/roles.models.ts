import {
    Column,
    DataType,
    Model,
    Table,
    UpdatedAt,
    CreatedAt,
    HasMany,
    BelongsToMany,
} from 'sequelize-typescript';
import { RoleEndpointsModel } from './role-endpoints.model';
import { RoleViewsModel } from './role-views.model';
import { UserModel } from './../../user/models/user.Model';
import { Surveys } from '../../surveys/models/surveys.models';
import { SurveysHasRoles } from '../../surveys/models/surveys-has-roles.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { RoleEndpoindsArrayExample } from '../examples/role-endpoints.example';
import { RoleViewExampleArray } from '../examples/role-view.example';

@Table({
    tableName : 'roles',
})
/**
 * @author Daniel Salazar
 * @description Modelo de conexion de la tabla roles
 * @type class
 */
export class RolesModel extends Model<RolesModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;
    @ApiModelPropertyOptional()

    @Column({
        type: DataType.STRING,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'role_code',
    })
    public roleCode: string;
    @ApiModelPropertyOptional()

    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'role_icon',
    })
    public roleIcon: string;
    @ApiModelPropertyOptional()

    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'role_text',
    })
    public roleText: string;
    
    @ApiModelPropertyOptional({ example: RoleEndpoindsArrayExample})
    @HasMany(() => RoleEndpointsModel)
    roleEndpoints: RoleEndpointsModel[];
    
    @ApiModelPropertyOptional({example: RoleViewExampleArray})
    @HasMany(() => RoleViewsModel)
    roleViews: RoleViewsModel[];

    /*
     * RolesModel tiene muchos UserModel
     */
    @ApiModelPropertyOptional({type:UserModel ,isArray: true})
    @HasMany(() => UserModel, 'roleId')
    userModel: UserModel[];
    @ApiModelPropertyOptional({type:Surveys ,isArray: true})
    @BelongsToMany(() => Surveys, {
        through: {
          model: () => SurveysHasRoles,
          unique: true,
        }
      })
    surveys: Surveys[];

// tslint:disable-next-line: variable-name
    @CreatedAt public created_at: Date;
// tslint:disable-next-line: variable-name
    @UpdatedAt public updated_at: Date;
}
