import { RolesModel } from './roles.models';
import {
    Column,
    DataType,
    ForeignKey,
    Model,
    Table,
    CreatedAt,
    UpdatedAt,
    HasMany,
    BelongsTo,
} from 'sequelize-typescript';

import { RoleViewTextModel } from './role-view-text.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { RoleViewExampleArray } from '../examples/role-view.example';

@Table({
    tableName: 'role_views',
})
/**
 * @author Daniel Salazar
 * @description Modelo de conexion de la tabla roles-view
 * @type class
 */
export class RoleViewsModel extends Model<RoleViewsModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => RolesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'roles_id',
    })
    public rolesId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'role_view',
    })
    public roleView: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'role_view_welcome',
    })
    public roleViewWelcome: boolean;
    @ApiModelPropertyOptional()

    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'role_view_image',
    })
    public roleViewImage: string;
    @ApiModelPropertyOptional()

    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'role_view_icon',
    })

    public roleViewIcon: string;
    @ApiModelPropertyOptional({ example: RoleViewExampleArray })
    @HasMany(() => RoleViewTextModel)
    roleText: RoleViewTextModel[];

    @BelongsTo(() => RolesModel)
    roles: RolesModel;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}
