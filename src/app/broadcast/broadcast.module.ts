import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { BroadcastController } from './controllers/broadcast.controller';
import { BroadcastService } from './services/broadcast.service';
import { BroadcastModel } from './models/broadcast.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { YoutubeLiveStreamService } from './services/youtube-livestream.service';
import { YoutubeBroadCastService } from './services/youtube-broadcast.service';
import { YoutubeAuthService } from '../shared/services/youtubeauth.service';
import { UserModel } from 'src/app/user/models/user.Model';
import { BroadcastHasCategoriesModel } from '../broadcast-has-categories/models/broadcast-has-categories.model';
const models = [
  BroadcastModel,
  UserModel,
  BroadcastHasCategoriesModel
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [BroadcastController],
  providers: [BroadcastService, YoutubeLiveStreamService, YoutubeBroadCastService, YoutubeAuthService]
})
export class BroadCastModule {}
