import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey, HasMany, BelongsTo, BelongsToMany } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { BroadcastMessageModel } from '../../broadcast-message/models/broadcast-message.model';
import { BroadcastAttendanceModel } from '../../broadcast-attendance/models/broadcast-attendance.model';
import { Categories } from '../../categories/models/categories.model';
import { BroadcastHasCategoriesModel } from '../../broadcast-has-categories/models/broadcast-has-categories.model';
// tslint:disable-next-line: max-line-length
import { BroadcastHasBroadcastPrebuiltMessagesModel } from '../../broadcasts_has_broadcast_prebuilt_messages/models/broadcasts_has_broadcast_prebuilt_messages.model';
import { BroadcastPrebuiltMessageModel } from '../../broadcast-prebuilt-message/models/broadcast-prebuilt-message.model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'broadcasts',
})
export class BroadcastModel extends Model<BroadcastModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'broadcaster_id',
    })
    broadcasterId: string;
    @ApiModelProperty()
    @BelongsTo(() => UserModel,'broadcaster_id')
    broadcaster: UserModel
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'organizer_id',
    })
    organizerId: string;
    @ApiModelProperty()
    @BelongsTo(() => UserModel,'organizer_id')
    organizer: UserModel
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'broadcast_title',
    })
    broadcastTitle: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        field: 'url_transmition',
    })
    urlTransmition: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        field: 'key_transmition',
    })
    keyTransmition: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        field: 'url_shared',
    })
    urlShared: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'broadcast_detail',
    })
    broadcastDetail: string;
    @ApiModelProperty()
    @Column({
        type: DataType.ENUM(
            'complete',
            'created',
            'live',
            'liveStarting',
            'ready',
            'revoke',
            'testStarting',
            'testing',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'broadcast_status',
    })
    broadcastStatus: string;

    @ApiModelProperty()
    @Column({
        type: DataType.ENUM(
            'private',
            'public',
            'unlisted',
            'unlisted_new',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'broadcast_privacity',
    })
    broadcastPrivacity: string;

    @ApiModelProperty()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        field: 'scheduled_date',
    })
    scheduledDate: string;
    @ApiModelProperty()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        field: 'start_date',
    })
    startDate: string;
    @ApiModelProperty()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        field: 'end_date',
    })
    endDate: string;
    @ApiModelProperty()
    @Column({
        type: DataType.ENUM(
            'es',
            'en',
            'it',
            'pr',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'broadcast_youtube_id',
    })
    broadcastYoutubeId: string;
    
    @HasMany(() => BroadcastMessageModel)
    broadcastMessageModel: BroadcastMessageModel[];

    @BelongsToMany(() => UserModel, {
        through: {
          model: () => BroadcastAttendanceModel,
          unique: true,
        }
      })
    userModel: UserModel[];

    @BelongsToMany(() => Categories, {
        through: {
          model: () => BroadcastHasCategoriesModel,
          unique: true,
        }
      })
    broadcastHasCategoriesModel: BroadcastHasCategoriesModel[];

    @BelongsToMany(() => BroadcastPrebuiltMessageModel, {
        through: {
          model: () => BroadcastHasBroadcastPrebuiltMessagesModel,
          unique: true,
        }
      })
    broadcastPrebuiltMessageModel: BroadcastPrebuiltMessageModel[];
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
