import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, Req} from '@nestjs/common';
import { BroadcastService } from '../services/broadcast.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { BroadcastSaveJson, BroadcastUpdateJson } from '../interfaces/broadcast.interface';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiImplicitQuery, ApiResponse, ApiImplicitHeader, ApiUseTags, ApiOperation } from '@nestjs/swagger';
import { BroadcastModel } from '../models/broadcast.model';
import { UserModel } from '../../user/models/user.Model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
@ApiUseTags('Module-Broadcast')
@Controller('broadcast')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)

export class BroadcastController {
    constructor(
        private readonly broadcastService: BroadcastService,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('BroadcastModel') private readonly broadcastModel: typeof BroadcastModel,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        /***** CREAR  ****/
        @ApiResponse({
            status: 200,
            type: BroadcastModel,
          })
          @ApiImplicitQuery({ name: 'languajeType', enum: ['es','en','it','pr'] })
          @ApiImplicitQuery({ name: 'broadcastPrivacity', enum: ['private', 'public', 'unlisted', 'unlisted_new'] })
// tslint:disable-next-line: max-line-length
          @ApiImplicitQuery({ name: 'broadcastStatus', enum: ['complete', 'created', 'live', 'liveStarting','ready','revoke','testStarting','testing'] })
          @ApiImplicitQuery({ name: 'broadcastStatus', enum: ['complete', 'created', 'live', 'liveStarting','ready','revoke','testStarting','testing'] })
// tslint:disable-next-line: max-line-length
          @ApiOperation({ title: 'urlTransmition,keyTransmition,urlShared,broadcastYoutubeId son datos que se autogeneran solos por lo que no son requeridos.El atributo format no se almacena en la base de datos pero es requerido para la configuración de youtube para ver un poco acerca de este ver en la documentación el campo cdn.format en el siguiente enlace: https://developers.google.com/youtube/v3/live/docs/liveStreams#resource' })
          @Post()
        @ApiImplicitHeader({
          name:'language',
          required: true,
      }) 
        // @UseGuards(new JwtAuthGuard())
        async create(@Body() broadcast: BroadcastSaveJson, @Req() req) {
          const data = await this.userModel.findByPk(broadcast.broadcasterId);
          if(data == null){
                throw this.appUtilsService.httpCommonError('Broadcast does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA4',
                    languageType:  req.headers.language,
                });
          }
          const data2 = await this.userModel.findByPk(broadcast.organizerId);
          if(data2 == null){
                throw this.appUtilsService.httpCommonError('Broadcast does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA4',
                    languageType:  req.headers.language,
                });
          }
          return await this.broadcastService.create(broadcast);
        }
        /***** LISTAR  ****/
        @Get()
        @ApiResponse({
            status: 200,
            type: BroadcastModel,
            isArray: true
          })
          @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async getAll(@Req() req) {
            return await this.broadcastService.findAll(req.headers.language);
        }
         /***** BUSCADOR ****/
         @Get('search/title/:title')
         @ApiImplicitParam({ name: 'title', required: true, type: 'string' })
         @ApiResponse({
             status: 200,
             type: BroadcastModel,
             isArray: true
           })
           @ApiImplicitHeader({
             name:'language',
             required: true,
         })
         async searchBroadcast(@Param('title') title,@Req() req) {
             return await this.broadcastService.findByTitle(title, req.headers.language);
         }
         /***** BUSCADOR ****/
         @Get('search/category/:categoryId')
         @ApiImplicitParam({ name: 'categoryId', required: true, type: 'string' })
         @ApiResponse({
             status: 200,
             type: BroadcastModel,
             isArray: true
           })
           @ApiImplicitHeader({
             name:'language',
             required: true,
         })
         async searchBroadcastByCategory(@Param('categoryId') categoryId,@Req() req) {
             return await this.broadcastService.findByCategory(categoryId);
         }
          /***** BUSCADOR POR FECHAS ****/
          @Get('search/date/:dateStart/:dateEnd')
          @ApiImplicitParam({ name: 'dateStart', required: true, type: 'string' })
          @ApiImplicitParam({ name: 'dateEnd', required: true, type: 'string' })
          @ApiResponse({
              status: 200,
              type: BroadcastModel,
              isArray: true
            })
            @ApiImplicitHeader({
              name:'language',
              required: true,
          })
          async searchByDates(@Param('dateStart') dateStart,@Param('dateEnd') dateEnd,@Req() req) {
              return await this.broadcastService.findByIDates(dateStart,dateEnd,req.headers.language);
          }
        /***** DETALLE  ****/
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Get(':id')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        @ApiResponse({
            status: 200,
            type: BroadcastModel,
          })
        async findById(@Param('id') idBroadcast) {
            return await this.broadcastService.findById(idBroadcast);
        } 
        /***** DETALLE  ADMIN****/
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Get('detail-admin/:id')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        @ApiResponse({
            status: 200,
            type: BroadcastModel,
          })
        // @UseGuards(new JwtAuthGuard())
        async findByIdOnlyAdministrator(@Param('id') idBroadcast) {
            return await this.broadcastService.findByIdOnlyAdministrator(idBroadcast);
        } 
        /***** LISTAR ADMIN  ****/
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Get('list-admin/:status')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        @ApiResponse({
            status: 200,
            type: BroadcastModel,
          })
        // @UseGuards(new JwtAuthGuard())
        async getAllOnlyAdmin(@Param('status') status,@Req() req) {
            return await this.broadcastService.findAllOnlyAdministrator(status,req.headers.language);
        } 
        /***** ACTUALIZAR  ****/
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @ApiImplicitQuery({ name: 'languajeType', enum: ['es','en','it','pr'] })
        @ApiImplicitQuery({ name: 'broadcastPrivacity', enum: ['private', 'public', 'unlisted', 'unlisted_new'] })
// tslint:disable-next-line: max-line-length
        @ApiImplicitQuery({ name: 'broadcastStatus', enum: ['complete', 'created', 'live', 'liveStarting','ready','revoke','testStarting','testing'] })
        @Put(':id')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        @ApiResponse({
            status: 200,
            type: BroadcastModel,
          })
        // @UseGuards(new JwtAuthGuard())
// tslint:disable-next-line: max-line-length
        @ApiOperation({ title: 'urlTransmition,keyTransmition,urlShared son datos que se autogeneran solos por lo que no son requeridos. El dato broadcastYoutubeId es requerido para ejecutar la actualización en el api de youtube.El atributo format no se almacena en la base de datos pero es requerido para la configuración de youtube para ver un poco acerca de este ver en la documentación el campo cdn.format en el siguiente enlace: https://developers.google.com/youtube/v3/live/docs/liveStreams#resource' })
        async update(
            @Body()
            updateBroadcast: Partial<BroadcastUpdateJson>, 
            @Param('id') idBroadcast,
            @Req() req
        ) {
            if(updateBroadcast.broadcasterId){
              const data = await this.userModel.findByPk(updateBroadcast.broadcasterId);
              if(data == null){
                    throw this.appUtilsService.httpCommonError('Broadcast does not created!', HttpStatus.NOT_ACCEPTABLE, {
                        messageCode: 'EA4',
                        languageType:  req.headers.language,
                    });
              }
            }
            if(updateBroadcast.organizerId){
              const data2 = await this.userModel.findByPk(updateBroadcast.organizerId);
              if(data2 == null){
                    throw this.appUtilsService.httpCommonError('Broadcast does not created!', HttpStatus.NOT_ACCEPTABLE, {
                        messageCode: 'EA4',
                        languageType:  req.headers.language,
                    });
              }
            }
            const data3 = await this.broadcastModel.findByPk(idBroadcast);
            if(data3 == null){
                  throw this.appUtilsService.httpCommonError('Broadcast does not updated!', HttpStatus.NOT_ACCEPTABLE, {
                      messageCode: 'EA22',
                      languageType:  req.headers.language,
                  });
            }
            return await this.broadcastService.update(idBroadcast, updateBroadcast);
        }
        /***** ELIMINAR  ****/
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Delete(':id')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        // @UseGuards(new JwtAuthGuard())
        async delete(@Param('id') idBroadcast) {
            return await this.broadcastService.deleted(idBroadcast);
        }
}
