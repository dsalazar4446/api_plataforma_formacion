import { Injectable } from '@nestjs/common';
import { YoutubeAuthService } from './../../shared/services/youtubeauth.service';
import { CONFIG } from '../../../config';
import {google} from 'googleapis';
@Injectable()
export class YoutubeBroadCastService {
    youtubeUrl: string = 'https://www.googleapis.com/youtube/v3';
    apiKey: string = CONFIG.youtube.apiKey;
    channelId: string =  CONFIG.youtube.channelId;
    constructor(
        private youtubeAuthService: YoutubeAuthService,
    ) { } 

    /********************* PLAYLISTS **************************/
    async createBroadCast(playList: any){
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.liveBroadcasts.insert({
          auth,
          part: playList.part,
          requestBody: {
            snippet: {
              title: playList.requestBody.snippet.title,
              description: playList.requestBody.snippet.description,
              scheduledStartTime: playList.requestBody.snippet.scheduledStartTime,
              isDefaultBroadcast: playList.requestBody.snippet.isDefaultBroadcast,
            },
            status: {
              privacyStatus: playList.requestBody.status.privacyStatus,
              lifeCycleStatus: playList.requestBody.status.lifeCycleStatus,
            },
            etag: playList.requestBody.etag,
          }}, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response.data);
        });
      });
    }
    async deleteBroadCast(id: string) {
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.liveBroadcasts.delete({
          auth,
          id,
          }, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response);
        });
      });
    }

    async listBroadCast(status: string,type: string){
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.liveBroadcasts.list({
          auth,
          part: 'id,snippet,contentDetails,status',
          broadcastStatus: status,
          broadcastType: type,
        }, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response);
        });
      });
    }

    async dataBroadCast(id: string){
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.liveBroadcasts.list({
          id,
          auth,
          part: 'id,snippet,contentDetails,status',
          broadcastStatus: 'all',
          broadcastType: 'all',
        }, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response);
        });
      });
    }

    async updateBroadCast(id: string, playList: any){
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.liveBroadcasts.update({
          auth,
          part: playList.part,
          requestBody: {
            id,
            snippet: {
              title: playList.requestBody.snippet.title,
              description: playList.requestBody.snippet.description,
              scheduledStartTime: playList.requestBody.snippet.scheduledStartTime,
              isDefaultBroadcast: playList.requestBody.snippet.isDefaultBroadcast,
            },
            status: {
              privacyStatus: playList.requestBody.status.privacyStatus,
              lifeCycleStatus: playList.requestBody.status.lifeCycleStatus,
            },
            etag: playList.requestBody.etag,
          }}, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response);
        });
      });
    }
}
