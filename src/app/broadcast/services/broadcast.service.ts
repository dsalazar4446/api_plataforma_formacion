import { Injectable, Inject } from '@nestjs/common';
import { BroadcastSaveJson, BroadcastUpdateJson } from '../interfaces/broadcast.interface';
import { BroadcastModel } from '../models/broadcast.model';
import { BroadcastMessageModel } from '../../broadcast-message/models/broadcast-message.model';
import { UserModel } from '../../user/models/user.Model';
import { YoutubeLiveStreamService } from './youtube-livestream.service';
import { YoutubeBroadCastService } from './youtube-broadcast.service';
import { Categories } from '../../categories/models/categories.model';
import { BroadcastHasCategoriesModel } from '../../broadcast-has-categories/models/broadcast-has-categories.model';
import { completeUrl } from '../../shared/utils/completUrl';
var Sequelize = require('sequelize');
var Op = Sequelize.Op;
@Injectable()
export class BroadcastService {
    constructor( 
        @Inject('BroadcastModel') private readonly broadcastModel: typeof BroadcastModel,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('BroadcastHasCategoriesModel') private readonly broadcastHasCategoriesModel: typeof BroadcastHasCategoriesModel,
        private readonly youtubeBroadCastService: YoutubeBroadCastService,
        private readonly youtubeLiveStreamService: YoutubeLiveStreamService,
    ) { }
    async findAll(language: string): Promise<BroadcastModel[]> {
        const result =  await this.broadcastModel.findAll({
            where:{
                languageType: language,
            },
            include: [Categories]
        });
        await Promise.all(
            await result.map(
                async (data) => {
                    const darUserBroadcaster =  await this.userModel.findByPk<UserModel>(data.dataValues.broadcasterId);
                    const darUserOrganizer =  await this.userModel.findByPk<UserModel>(data.dataValues.organizerId);
                    darUserBroadcaster.dataValues.pwd = undefined;
                    darUserOrganizer.dataValues.pwd = undefined;
                    darUserBroadcaster.dataValues.avatar = completeUrl('user/avatar',darUserBroadcaster.dataValues.avatar);
                    darUserOrganizer.dataValues.avatar = completeUrl('user/avatar',darUserOrganizer.dataValues.avatar);
                    data.dataValues.organizer = darUserOrganizer.dataValues;
                    data.dataValues.broadcaster = darUserBroadcaster.dataValues;
                }
            )
        )
        return result;
    }
    async findAllOnlyAdministrator(status,language): Promise<BroadcastModel[]> {
        const result =  await this.broadcastModel.findAll({
            where:{
                broadcastStatus: status,
                languageType: language,
            }
        });
        await Promise.all(
            await result.map(
                async (data) => {
                    const darUserBroadcaster =  await this.userModel.findByPk<UserModel>(data.dataValues.broadcasterId);
                    const darUserOrganizer =  await this.userModel.findByPk<UserModel>(data.dataValues.organizerId);
                    darUserBroadcaster.dataValues.pwd = undefined;
                    darUserOrganizer.dataValues.pwd = undefined;
                    darUserBroadcaster.dataValues.avatar = completeUrl('user/avatar',darUserBroadcaster.dataValues.avatar);
                    darUserOrganizer.dataValues.avatar = completeUrl('user/avatar',darUserOrganizer.dataValues.avatar);
                    data.dataValues.organizer = darUserOrganizer.dataValues;
                    data.dataValues.broadcaster = darUserBroadcaster.dataValues;
                }
            )
        )
        return result;
    }

    async findByTitle(title: string,language: string): Promise<BroadcastModel> {
        const result =  await this.broadcastModel.find<BroadcastModel>({
            where:{
                [Op.or]: {
                    broadcastTitle: {
                        [Op.iLike]: '%' + title + '%'
                    },
                },
                languageType: language,
            },
            include:[
                Categories
            ]
        });
        const dataUserBroadcaster =  await this.userModel.findByPk<UserModel>(result.dataValues.broadcasterId);
        const dataUserOrganizer =  await this.userModel.findByPk<UserModel>(result.dataValues.organizerId);
        dataUserBroadcaster.dataValues.pwd = undefined;
        dataUserOrganizer.dataValues.pwd = undefined;
        dataUserBroadcaster.dataValues.avatar = completeUrl('user/avatar',dataUserBroadcaster.dataValues.avatar);
        dataUserOrganizer.dataValues.avatar = completeUrl('user/avatar',dataUserOrganizer.dataValues.avatar);
        result.dataValues.organizer = dataUserOrganizer.dataValues;
        result.dataValues.broadcaster = dataUserBroadcaster.dataValues;
        return result;
    }
    async findByCategory(id: string):Promise<BroadcastModel[]>{
        const dataCM = await this.broadcastHasCategoriesModel.findAll<BroadcastHasCategoriesModel>({
            where:{
                categoriesId: id,
            }
        });
        const result: BroadcastModel[] = [];
        await Promise.all(
            await dataCM.map(
                async (item)=>{
                    const data: any = await this.broadcastModel.findByPk(item.dataValues.broadcastId);
                    const dataUserBroadcaster =  await this.userModel.findByPk<UserModel>(data.dataValues.broadcasterId);
                    const dataUserOrganizer =  await this.userModel.findByPk<UserModel>(data.dataValues.organizerId);
                    dataUserBroadcaster.dataValues.pwd = undefined;
                    dataUserOrganizer.dataValues.pwd = undefined;
                    dataUserBroadcaster.dataValues.avatar = completeUrl('user/avatar',dataUserBroadcaster.dataValues.avatar);
                    dataUserOrganizer.dataValues.avatar = completeUrl('user/avatar',dataUserOrganizer.dataValues.avatar);
                    data.dataValues.organizer = dataUserOrganizer.dataValues;
                    data.dataValues.broadcaster = dataUserBroadcaster.dataValues;
                    result.push(data.dataValues);
                }
            )
        )
        return result;
    }
    async findByIDates(start:string,end:string,language: string): Promise<BroadcastModel> {
        const result =  await this.broadcastModel.find<BroadcastModel>({
            where:{
                [Op.or]: {
                    startDate: {
                        [Op.between]: [start, end]
                    },
                },
                languageType: language,
            },
            include:[
                Categories
            ]
        });
        const dataUserBroadcaster =  await this.userModel.findByPk<UserModel>(result.dataValues.broadcasterId);
        const dataUserOrganizer =  await this.userModel.findByPk<UserModel>(result.dataValues.organizerId);
        dataUserBroadcaster.dataValues.pwd = undefined;
        dataUserOrganizer.dataValues.pwd = undefined;
        dataUserBroadcaster.dataValues.avatar = completeUrl('user/avatar',dataUserBroadcaster.dataValues.avatar);
        dataUserOrganizer.dataValues.avatar = completeUrl('user/avatar',dataUserOrganizer.dataValues.avatar);
        result.dataValues.organizer = dataUserOrganizer.dataValues;
        result.dataValues.broadcaster = dataUserBroadcaster.dataValues;
        return result;
    }

    async findById(id: string): Promise<BroadcastModel> {
        const result =  await this.broadcastModel.findByPk<BroadcastModel>(id,{
            include:[
                Categories
            ]
        });
        const dataUserBroadcaster =  await this.userModel.findByPk<UserModel>(result.dataValues.broadcasterId);
        const dataUserOrganizer =  await this.userModel.findByPk<UserModel>(result.dataValues.organizerId);
        dataUserBroadcaster.dataValues.pwd = undefined;
        dataUserOrganizer.dataValues.pwd = undefined;
        dataUserBroadcaster.dataValues.avatar = completeUrl('user/avatar',dataUserBroadcaster.dataValues.avatar);
        dataUserOrganizer.dataValues.avatar = completeUrl('user/avatar',dataUserOrganizer.dataValues.avatar);
        result.dataValues.organizer = dataUserOrganizer.dataValues;
        result.dataValues.broadcaster = dataUserBroadcaster.dataValues;
        return result;
    }
    async findByIdOnlyAdministrator(id: string): Promise<BroadcastModel> {
        return  await this.broadcastModel.findByPk<BroadcastModel>(id,{
            include:[
                Categories
            ]
        });
    }
    async create(broadcast: BroadcastSaveJson): Promise<any> {
        const dataLiveStream = {
            part: 'id,snippet,cdn,contentDetails,status',
            requestBody: {
                cdn: {
                  format: broadcast.format,
                  ingestionType: 'rtmp',
                },
                snippet: {
                  title: broadcast.broadcastTitle,
                  description: broadcast.broadcastDetail,
                },
                status: {
                    privacyStatus: broadcast.broadcastPrivacity,
                    lifeCycleStatus: broadcast.broadcastStatus,
                }
            }
        }
        const dataBroadcast = {
            part: 'id,snippet,contentDetails,status',
            requestBody:{
                snippet: {
                  scheduledStartTime: broadcast.scheduledDate,
                  title: broadcast.broadcastTitle,
                  isDefaultBroadcast: false,
                  description: broadcast.broadcastDetail,
                },
                status: {
                    privacyStatus: broadcast.broadcastPrivacity,
                    lifeCycleStatus: broadcast.broadcastStatus,
                },
                etag: 'transmisión',
              }
        }
        const broadCast: any = await  this.youtubeBroadCastService.createBroadCast(dataBroadcast);
        const liveStream: any = await  this.youtubeLiveStreamService.createLiveStream(dataLiveStream);
        broadcast.keyTransmition = liveStream.cdn.ingestionInfo.streamName;
        broadcast.urlTransmition = liveStream.cdn.ingestionInfo.ingestionAddress;
        broadcast.broadcastYoutubeId = broadCast.id;
        broadcast.urlShared = `https://www.youtube.com/embed/${broadCast.id}?autoplay=1`;
        const result = await this.broadcastModel.create<BroadcastModel>(broadcast, {
            returning: true,
        });
        const dataUserBroadcaster =  await this.userModel.findByPk<UserModel>(result.dataValues.broadcasterId);
        const dataUserOrganizer =  await this.userModel.findByPk<UserModel>(result.dataValues.organizerId);
        dataUserBroadcaster.dataValues.pwd = undefined;
        dataUserOrganizer.dataValues.pwd = undefined;
        dataUserBroadcaster.dataValues.avatar = completeUrl('user/avatar',dataUserBroadcaster.dataValues.avatar);
        dataUserOrganizer.dataValues.avatar = completeUrl('user/avatar',dataUserOrganizer.dataValues.avatar);
        result.dataValues.organizer = dataUserOrganizer.dataValues;
        result.dataValues.broadcaster = dataUserBroadcaster.dataValues;
        return result;
    }
    async update(idBroadcast: string, broadcastUpdate: Partial<BroadcastUpdateJson>){
        try {
            const dataBroadcast = {
                part: 'id,snippet,status',
                requestBody:{
                    id: broadcastUpdate.broadcastYoutubeId,
                    snippet: {
                      scheduledStartTime: broadcastUpdate.scheduledDate,
                      title: broadcastUpdate.broadcastTitle,
                      isDefaultBroadcast: false,
                      description: broadcastUpdate.broadcastDetail,
                    },
                    status: {
                        privacyStatus: broadcastUpdate.broadcastPrivacity,
                        lifeCycleStatus: broadcastUpdate.broadcastStatus,
                    }
                }
            }
            await  this.youtubeBroadCastService.updateBroadCast(broadcastUpdate.broadcastYoutubeId,dataBroadcast);
            const result: any =  await this.broadcastModel.update(broadcastUpdate, {
                where: { 
                    id: idBroadcast,
                },
                returning: true,
            });
            const dataUserBroadcaster =  await this.userModel.findByPk<UserModel>(result[1][0].dataValues.broadcaster_id);
            const dataUserOrganizer =  await this.userModel.findByPk<UserModel>(result[1][0].dataValues.organizer_id);
            dataUserBroadcaster.dataValues.pwd = undefined;
            dataUserOrganizer.dataValues.pwd = undefined;
            dataUserBroadcaster.dataValues.avatar = completeUrl('user/avatar',dataUserBroadcaster.dataValues.avatar);
            dataUserOrganizer.dataValues.avatar = completeUrl('user/avatar',dataUserOrganizer.dataValues.avatar);
            result[1][0].dataValues.organizer = dataUserOrganizer.dataValues;
            result[1][0].dataValues.broadcaster = dataUserBroadcaster.dataValues;
            return result;
        } catch (error) {
            return error.data;
        }
    }
    async deleted(idBroadcast: string): Promise<any> {
        const dataBroadcast = await this.broadcastModel.findByPk(idBroadcast);
        await  this.youtubeBroadCastService.deleteBroadCast(dataBroadcast.dataValues.broadcastYoutubeId);
        return await this.broadcastModel.destroy({
            where: {
                id: idBroadcast,
            },
        });
    }
}
