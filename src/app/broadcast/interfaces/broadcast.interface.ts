import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum, IsEmpty, IsOptional, IsDate, IsISO8601} from "class-validator";
export class BroadcastSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    broadcasterId: string;
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    organizerId: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    broadcastTitle: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    broadcastDetail: string;
    @ApiModelProperty({ enum: ['complete', 'created', 'live', 'liveStarting','ready','revoke','testStarting','testing']})
    @IsString()
    @IsNotEmpty()
    broadcastStatus: string;
    @ApiModelProperty({ enum: ['private', 'public', 'unlisted', 'unlisted_new']})
    @IsString()
    @IsNotEmpty()
    broadcastPrivacity: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    scheduledDate: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    startDate: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    endDate: string;
    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsNotEmpty()
    @IsString()
    languageType: string;
    @ApiModelProperty({
        required: false,
        description: 'Este campo es opcional debido a que se autogenera desde el api de youtube',
    })
    @IsString()
    @IsOptional()
    urlTransmition?: string;
    @ApiModelProperty({
        required: false,
        description: 'Este campo es opcional debido a que se autogenera desde el api de youtube',
    })
    @IsOptional()
    @IsString()
    keyTransmition?: string;
    @ApiModelProperty({
        required: false,
        description: 'Este campo es opcional debido a que se autogenera desde el api de youtube',
    })
    @IsOptional()
    @IsString()
    urlShared?: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    format: string;
    @ApiModelProperty({
        required: false,
        description: 'Este campo se genera automaticamente con el api de youtube y es requerido solo en las actualizaciones y eliminaciones.',
    })
    @IsString()
    @IsOptional()
    broadcastYoutubeId?: string;
}
// tslint:disable-next-line: max-classes-per-file
export class BroadcastUpdateJson{
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    broadcasterId: string;
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    organizerId: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    broadcastTitle: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    broadcastDetail: string;
    @ApiModelProperty({ enum: ['complete', 'created', 'live', 'liveStarting','ready','revoke','testStarting','testing']})
    @IsString()
    @IsNotEmpty()
    broadcastStatus: string;
    @ApiModelProperty({ enum: ['private', 'public', 'unlisted', 'unlisted_new']})
    @IsString()
    @IsNotEmpty()
    broadcastPrivacity: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    scheduledDate: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    startDate: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    endDate: string;
    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsNotEmpty()
    @IsString()
    languageType: string;
    @ApiModelProperty({
        required: false,
        description: 'Este campo es opcional debido a que se autogenera desde el api de youtube',
    })
    @IsString()
    @IsNotEmpty()
    urlTransmition?: string;
    @ApiModelProperty({
        required: false,
        description: 'Este campo es opcional debido a que se autogenera desde el api de youtube',
    })
    @IsOptional()
    @IsString()
    keyTransmition?: string;
    @ApiModelProperty({
        required: false,
        description: 'Este campo es opcional debido a que se autogenera desde el api de youtube',  
    })
    @IsOptional()
    @IsString()
    urlShared?: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    format: string;
    @ApiModelProperty({
        description: 'Este campo solo es requerido para la actualización y eliminaciones y es generado por el api de youtube'
    })
    @IsString()
    @IsNotEmpty()
    broadcastYoutubeId: string;
}
