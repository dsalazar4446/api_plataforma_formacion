import { Module, Provider, HttpModule } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { CalendarsController } from './controllers/calendars.controller';
import { Calendars } from './models/calendars.model';
import { CalendarsService } from './services/calendars.servide';

const models = [Calendars];
const controllers = [CalendarsController];
const providers: Provider[] = [CalendarsService];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers
})
export class CalendarsModule { }
