import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsUUID, IsNumber, IsString, IsOptional, IsDate, IsBoolean } from "class-validator";
export class CalendarsSaveJson {

    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    userId: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    title: string;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    description: string;

    @ApiModelProperty()
    @IsDate()
    @IsNotEmpty()
    dateStart: Date;

    @ApiModelProperty()
    @IsDate()
    @IsNotEmpty()
    dateEnd: Date;

    @ApiModelProperty()
    @IsString()
    @IsOptional()
    location: string;

    @ApiModelProperty()
    @IsString()
    @IsOptional()
    latitude: string;

    @ApiModelProperty()
    @IsString()
    @IsOptional()
    longitude: string;

    @ApiModelProperty()
    @IsBoolean()
    @IsNotEmpty()
    makePublic: boolean;

    @ApiModelProperty()
    @IsBoolean()
    @IsNotEmpty()
    counsulting: boolean;

    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsString()
    @IsNotEmpty()
    languageType: string;
}
// tslint:disable-next-line: max-classes-per-file
export class CalendarsUpdateJson extends CalendarsSaveJson {

    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}