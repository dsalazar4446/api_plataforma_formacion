import { Injectable, Inject } from '@nestjs/common';
import { UserModel } from '../../user/models/user.Model';
import { Calendars } from '../models/calendars.model';
import { CalendarsSaveJson, CalendarsUpdateJson } from '../interfaces/calendars.interface';
import * as users from '../../shared/utils/users.json';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services CalendarsService
 * @Creado 10 de Mayo 2019
 */

@Injectable()
export class CalendarsService {

    private include = {
        include: [
            {
                model: UserModel
            }
        ]
    }

    constructor(
        @Inject('Calendars') private readonly _calendars: typeof Calendars
    ) { }

    async saveCalendars(bodyCalendars: CalendarsSaveJson): Promise<CalendarsUpdateJson> {
        return await new this._calendars(bodyCalendars).save();
    }

    async showAllCalendars(): Promise<CalendarsUpdateJson[]> {
        return await this._calendars.findAll(this.include);
    }

    async getDetails(CalendarsId: string) {
        return await this._calendars.findByPk(CalendarsId, this.include);
    }

    async updateCalendars(id: string, bodyCalendars: Partial<CalendarsUpdateJson>): Promise<[number, Array<CalendarsUpdateJson>]> {
        return await this._calendars.update(bodyCalendars, {
            where: { id },
            returning: true
        });

    }

    async destroyCalendars(CalendarsId: string): Promise<number> {
        return await this._calendars.destroy({
            where: { id: CalendarsId },
        });
    }
}
