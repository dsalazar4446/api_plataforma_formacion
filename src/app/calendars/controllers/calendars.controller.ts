import { Controller, UseFilters, UseInterceptors, Post, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { CalendarsService } from '../services/calendars.servide';
import { CalendarsSaveJson } from '../interfaces/calendars.interface';
import { Calendars } from '../models/calendars.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller CalendarsController
 * @Creado 10 de Mayo 2019
 */

@ApiUseTags('Module-Calendars')
@Controller('calendars')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class CalendarsController {

    constructor(private readonly _calendarsService: CalendarsService,
        private readonly appUtilsService: AppUtilsService) { }

    @ApiResponse({
        status: 200,
        type: Calendars,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyCalendars: CalendarsSaveJson) {
        const createCalendars = await this._calendarsService.saveCalendars(bodyCalendars);
        if (!createCalendars) {
            throw this.appUtilsService.httpCommonError('Calendars does not created!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM5',
                languageType: 'es',
            });
        }
        return createCalendars;
    }


    @ApiResponse({
        status: 200,
        type: Calendars,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idCalendars: string) {

        const fetchCalendars = await this._calendarsService.getDetails(idCalendars);

        if (!fetchCalendars) {
            throw this.appUtilsService.httpCommonError('Calendars does not created!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM6',
                languageType: 'es',
            });
        }
        return fetchCalendars;
    }


    @ApiResponse({
        status: 200,
        type: Calendars,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllCalendars() {

        const fetch = await this._calendarsService.showAllCalendars();
        if (!fetch) {
            throw this.appUtilsService.httpCommonError('Calendars does not created!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM6',
                languageType: 'es',
            });
        }
        return fetch;
    }


    @ApiResponse({
        status: 200,
        type: Calendars,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateCalendars(@Param('id') id: string, @Body() bodyCalendars: Partial<CalendarsSaveJson>) {

        const updateCalendars = await this._calendarsService.updateCalendars(id, bodyCalendars);
        if (!updateCalendars[1][0]) {
            throw this.appUtilsService.httpCommonError('Calendars does not created!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM6',
                languageType: 'es',
            });
        }
        return updateCalendars;
    }


    @ApiResponse({
        status: 200,
        type: Calendars,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteCalendars(@Param('id') idCalendars: string) {

        const deleteCalendars = await this._calendarsService.destroyCalendars(idCalendars);
        if (!deleteCalendars) {
            throw this.appUtilsService.httpCommonError('Calendars does not created!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM6',
                languageType: 'es',
            });
        }
        return deleteCalendars;
    }
}
