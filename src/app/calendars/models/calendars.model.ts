import { Table, Model, Column, DataType, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from "sequelize-typescript";
import { UserModel } from "../../user/models/user.Model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({
    tableName: 'calendars',
})
export class Calendars extends Model<Calendars>{


    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'title',
    })
    title: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'description',
    })
    description: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'date_start',
    })
    dateStart: Date;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'date_end',
    })
    dateEnd: Date;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(45),
        allowNull: true,
        field: 'location',
    })
    location: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(128),
        allowNull: true,
        field: 'latitude',
    })
    latitude: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(128),
        allowNull: true,
        field: 'longitude',
    })
    longitude: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        defaultValue: false,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'make_public',
    })
    makePublic: boolean;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'counsulting',
    })
    counsulting: boolean;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'language_type'
    })
    languageType: string; 
    
    @BelongsTo(() => UserModel)
    userModel: UserModel;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}