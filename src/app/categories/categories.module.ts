import { Module, Provider, HttpModule } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { Categories } from './models/categories.model';
import { CashMovements} from './models/cash-movements.model';
import { Budgets } from './models/budgets.model';


import { CashMovementsServices } from './services/cash-movements.services';
import { CategoriesServices } from './services/categories.services';
import { BudgetsServices } from './services/budgets.services';

import { CashMovementsController } from './controllers/cash-movements.controller';
import { CategoriesController } from './controllers/categories.controller';
import { BudgetsController } from './controllers/budgets.controller';


const models = [Categories, CashMovements, Budgets];
const controllers = [CashMovementsController, BudgetsController,CategoriesController];
const providers: Provider[] = [CashMovementsServices, CategoriesServices, BudgetsServices];

@Module({
    imports: [
      SharedModule,
      HttpModule,
      DatabaseModule.forRoot({
        sequelize: {
          models,
        },
        loki: true,
      }),
    ],
    providers,
    controllers
  })
export class CategoriesModule {}
