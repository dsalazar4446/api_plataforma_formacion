import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, BelongsTo, ForeignKey } from 'sequelize-typescript';
import { Categories } from './categories.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'cash_movements',
})
export class CashMovements extends Model<CashMovements>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => Categories)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'categories_id'
    })
    categoriesId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'cash_ammount'
    })
    cashAmmount: number;

    /**
     * RELACIONES
     * CashMovements pertenece a Categories
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => Categories)
    categories: Categories;

    @CreatedAt created_at: Date;
    @UpdatedAt updated_at: Date;

}