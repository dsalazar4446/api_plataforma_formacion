import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, BelongsTo, ForeignKey } from 'sequelize-typescript';
import { Categories } from './categories.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'budgets',
})
export class Budgets extends Model<Budgets>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => Categories)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'categories_id'
    })
    categoriesId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'budget_start'
    })
    budgetStart: Date;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'budget_end'
    })
    budgetEnd: Date;

    /**
     * RELACIONES
     * Budgets pertenece a Categories
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => Categories)
    categories: Categories;

    @CreatedAt created_at: Date;
    @UpdatedAt updated_at: Date;

}