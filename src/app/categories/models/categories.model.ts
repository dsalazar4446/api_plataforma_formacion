import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany, BelongsToMany } from 'sequelize-typescript';
import { CashMovements } from './cash-movements.model';
import { Budgets } from './budgets.model';
import { ProgramsModel } from './../../programs/models/programs.model';
import { Forums } from './../../forums/models/forums.model';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
import { BroadcastHasCategoriesModel } from '../../broadcast-has-categories/models/broadcast-has-categories.model';
// tslint:disable-next-line: max-line-length
import { BroadcastPrebuiltMessageHasCategoriesModel } from '../../broadcast-prebuilt-message-has-categories/models/broadcast-prebuilt-message-has-categories.model';
import { BroadcastPrebuiltMessageModel } from '../../broadcast-prebuilt-message/models/broadcast-prebuilt-message.model';
import { NotesModel } from '../../classes/models/notes.model';
import { NotesHasCategoriesModel } from '../../classes/models/notes-has-categories.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'categories',
})  
export class Categories extends Model<Categories>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(75),
        allowNull: false,
        validate: {
            notEmpty: true
        },
        field: 'category_name'
    })
    categoryName: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language'
    })
    language: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('formation', 'financial'),
        allowNull: false,
        validate: {
            notEmpty: true
        },
        defaultValue: 'formation',
        field: 'category_target'
    })
    categoryTarget: string;


    @ApiModelPropertyOptional({
        type: CashMovements,
        isArray: false
    })
    @HasMany(() => CashMovements)
    cashMovements: CashMovements[];


    @ApiModelPropertyOptional({
        type: Budgets,
        isArray: false
    })
    @HasMany(() => Budgets)
    budgets: Budgets[];


    @ApiModelPropertyOptional({
        example: [{
            "id": "UUID",
            "categoriesId": "UUID",
            "programCode": "string",
            "programStatus": "string",
            "programBackground": "string",
            "programIcon": "string",
            "fbPromotionImg": "string"
        }]
    })
    @HasMany(() => ProgramsModel)
    programsModel: ProgramsModel[];


    @ApiModelPropertyOptional({
        example: [{
            "id": "UUID",
            "moderatorUserId": "UUID",
            "categoryId": "UUID",
            "forumTitle": "string",
            "formationDescription": "string",
            "languageType": "string",
        }]
    })
    @HasMany(() => Forums)
    forums: Forums[];

    @ApiModelPropertyOptional({
        example: [{
            "id":"UUID",
            "broadcasterId":"UUID",
            "organizerId":"UUID",
            "broadcastTitle":"string",
            "urlTransmition":"string",
            "keyTransmition":"string",
            "urlShared":"string",
            "broadcastDetail":"string",
            "broadcastStatus":"string",
            "scheduledDate":"string",
            "startDate":"string",
            "endDate":"string",
            "languageType":"string",
            "broadcastYoutubeId":"string"
        }]
    })
    @BelongsToMany(() => BroadcastModel, {
        through: {
            model: () => BroadcastHasCategoriesModel,
            unique: true,
        }
    })
    broadcastModel: BroadcastModel[];

    @ApiModelPropertyOptional({
        example: [{
            "id":"UUID",
            "prebuiltUserId":"UUID",
            "broadcastPrebuiltMessage":"string",
            "broadcastPrebuiltMessageStatus":"boolean",

        }]
    })
    @BelongsToMany(() => BroadcastPrebuiltMessageModel, {
        through: {
            model: () => BroadcastPrebuiltMessageHasCategoriesModel,
            unique: true,
        }
    })
    broadcastPrebuiltMessageModel: BroadcastPrebuiltMessageModel[];

    @HasMany(() => BroadcastPrebuiltMessageHasCategoriesModel)
    broadcastPrebuiltMessageHasCategoriesModel: BroadcastPrebuiltMessageHasCategoriesModel[];


    @BelongsToMany(() => NotesModel, () => NotesHasCategoriesModel)
    notes: NotesModel

    @CreatedAt created_at: Date;
    @UpdatedAt updated_at: Date;
}
