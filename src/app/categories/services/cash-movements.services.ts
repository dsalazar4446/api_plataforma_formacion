import { Injectable, Inject, HttpStatus } from "@nestjs/common";
import { CashMovementsSaveJson, CashMovementsUpdateJson } from "../interfaces/cash-movements.interface";
import { CashMovements } from "../models/cash-movements.model";
import { ValidateUUID } from "../../shared/utils/validateUUID";
import { Categories } from "../models/categories.model";
import { AppUtilsService } from "../../shared/services/app-utils.service";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services CashMovementsServices
 * @Creado 29 de Marzo 2019
 */

@Injectable()
export class CashMovementsServices {

    constructor(
        @Inject('CashMovements') private readonly _cashMovementsServcs: typeof CashMovements,
        private readonly _validateUUID: ValidateUUID,
        @Inject('Categories') private readonly _categories: typeof Categories,
        private readonly appUtilsService: AppUtilsService,
    ) { }

    async saveCashMove(bodyCashMove: CashMovementsSaveJson): Promise<CashMovementsUpdateJson> {

        const categories = await this._categories.findByPk(bodyCashMove.categoriesId);

        if (!categories) {
            throw this.appUtilsService.httpCommonError('Categories Id no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM142',
                languageType: 'es',
            });
        }
        return await new this._cashMovementsServcs(bodyCashMove).save();
    }

    async showAllCashMove(): Promise<CashMovementsUpdateJson[]> {
        return await this._cashMovementsServcs.findAll();
    }

    async getDetails(cashMoveId: string) {

        this._validateUUID.validateUUIID(cashMoveId);
        return await this._cashMovementsServcs.findByPk(cashMoveId);
    }

    async updateCashMove(id: string, bodyCashMove: Partial<CashMovementsUpdateJson>): Promise<[number, Array<any>]> {

        this._validateUUID.validateUUIID(id);
        return await this._cashMovementsServcs.update(bodyCashMove, {
            where: { id },
            returning: true
        });

    }

    async destroyCashMove(cashMoveId: string): Promise<number> {

        this._validateUUID.validateUUIID(cashMoveId);
        return await this._cashMovementsServcs.destroy({
            where: { id: cashMoveId },
        });
    }
}