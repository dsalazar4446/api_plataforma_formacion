import { Injectable, Inject, HttpStatus } from "@nestjs/common";
import { BudgetsSaveJson, BudgetsUpdateJson } from "../interfaces/budgets.interface";
import { Budgets } from "../models/budgets.model";
import { ValidateUUID } from "../../shared/utils/validateUUID";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { Categories } from "../models/categories.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services BudgetsServices
 * @Creado 29 de Marzo 2019
 */

@Injectable()
export class BudgetsServices {

    constructor(
        @Inject('Budgets') private readonly _budgetsServcs: typeof Budgets,
        @Inject('Categories') private readonly _categories: typeof Categories,
        private readonly appUtilsService: AppUtilsService,
        private readonly _validateUUID: ValidateUUID,
    ) { }

    async saveForumsM(bodyBudgets: BudgetsSaveJson): Promise<BudgetsUpdateJson> {

        const categories = await this._categories.findByPk(bodyBudgets.categoriesId);

        if (!categories) {
            throw this.appUtilsService.httpCommonError('Categories Id no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM142',
                languageType: 'es',
            });
        }

        return await new this._budgetsServcs(bodyBudgets).save();
    }

    async showAllBugets(): Promise<BudgetsUpdateJson[]> {

        return await this._budgetsServcs.findAll();
    }

    async getDetails(budgetsId: string) {
        this._validateUUID.validateUUIID(budgetsId);
        return await this._budgetsServcs.findByPk(budgetsId);
    }

    async updateBudgets(id: string, bodyBudgets: Partial<BudgetsUpdateJson>): Promise<[number, Array<any>]> {

        this._validateUUID.validateUUIID(id);
        return await this._budgetsServcs.update(bodyBudgets, {
            where: { id },
            returning: true
        });

    }

    async destroyBudgets(budgetsId: string): Promise<number> {

        this._validateUUID.validateUUIID(budgetsId);
        return await this._budgetsServcs.destroy({
            where: { id: budgetsId },
        });
    }


}