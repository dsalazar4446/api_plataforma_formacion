import { Injectable, Inject, NotFoundException } from "@nestjs/common";
import { Categories } from "../models/categories.model";
import { CashMovements} from "../models/cash-movements.model";
import { Budgets } from "../models/budgets.model";
import { CategoriesSaveJson, CategoriesUpdateJson } from "../interfaces/categories.inteface";
import { Sequelize } from 'sequelize-typescript';
import { ValidateUUID } from "../../shared/utils/validateUUID";
import { NotesModel } from "../../classes/models/notes.model";
const Op = Sequelize.Op;

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services CategoriesServices
 * @Creado 29 de Marzo 2019
 */

@Injectable()
export class CategoriesServices {

    private categoryTarget = ['formation', 'financial'];
    private language = ['es', 'en', 'it', 'pr'];

    private includeModels = [
        {
            model: CashMovements
        },
        {
            model: Budgets
        },
        {
            model: NotesModel
        }
    ];

    constructor(
        @Inject('Categories') private readonly _categoriesServcs: typeof Categories,
        private readonly _validateUUID: ValidateUUID
    ) { }

    async saveNews(bodyCategories: CategoriesSaveJson): Promise<CategoriesUpdateJson> {

        return await new this._categoriesServcs(bodyCategories).save();
    }

    async showAllForums(language: string, categoryTarget: string): Promise<CategoriesUpdateJson[]> {

        if(language && this.language.indexOf(language) < 0) throw new NotFoundException('language does not exist! es, en, it, pr');
        if(categoryTarget && this.categoryTarget.indexOf(categoryTarget) < 0) throw new NotFoundException('Category Target does not exist! formation, financial');

        if (language && categoryTarget) {
            return await this._categoriesServcs.findAll({
                where: {
                    [Op.and]: [
                        {
                            language
                        },
                        {
                            categoryTarget
                        }
                    ]
                },
                include: this.includeModels
            })
        }

        if (language && !categoryTarget) {

            return await this._categoriesServcs.findAll({
                where: {
                    language
                },
                include: this.includeModels
            });
        }

        if (!language && categoryTarget) {
            return await this._categoriesServcs.findAll({
                where: {
                    categoryTarget
                },
                include: this.includeModels
            });
        }

        return;
    }

    async getDetails(categoriesId: string) {

        this._validateUUID.validateUUIID(categoriesId);
        return await this._categoriesServcs.findByPk(categoriesId, {
            include: this.includeModels
        });
    }

    async updateCategories(id: string, categoriesId: Partial<CategoriesUpdateJson>): Promise<[number, Array<any>]> {

        this._validateUUID.validateUUIID(id);
        return await this._categoriesServcs.update(categoriesId, {
            where: { id },
            returning: true
        });

    }

    async destroyCategories(categoriesId: string): Promise<number> {

        this._validateUUID.validateUUIID(categoriesId);
        return await this._categoriesServcs.destroy({
            where: { id: categoriesId },
        });
    }

    async find() {

        return await this._categoriesServcs.findAll();

    }
}