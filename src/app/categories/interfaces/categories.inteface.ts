import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsUUID } from 'class-validator';

export class CategoriesSaveJson {
    @ApiModelProperty()
    @IsString({message: 'categoryName property must a be string'})
    @IsNotEmpty({message: 'categoryName property not must null'})
    categoryName: string;

    @ApiModelProperty()
    @IsString({message: 'language property must a be string'})
    @IsNotEmpty({message: 'language property not must null'})
    language: string;

    @ApiModelProperty()
    @IsString({message: 'categoryTarget property must a be string'})
    @IsNotEmpty({message: 'categoryTarget property not must null'})
    categoryTarget: string;
}
export class CategoriesUpdateJson extends CategoriesSaveJson{
    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
