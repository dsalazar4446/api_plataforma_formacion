import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsNotEmpty, IsNumber } from 'class-validator';

export class CashMovementsSaveJson {
    @ApiModelProperty()
    @IsUUID('4',{message: 'categoriesId property must a be uuid'})
    @IsNotEmpty({message: 'categoriesId property not must null'})
    categoriesId: string;

    @ApiModelProperty()
    @IsNumber(null, {message: 'categoriesId property must a be number'})
    @IsNotEmpty({message: 'categoriesId property not must null'})
    cashAmmount: number;
}
export class CashMovementsUpdateJson extends CashMovementsSaveJson{
    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
