import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsNotEmpty, IsString, IsDate } from 'class-validator';

export class BudgetsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'categoriesId property must a be uuid'})
    @IsNotEmpty({message: 'categoriesId property not must null'})
    categoriesId: string;

    @ApiModelProperty()
    @IsDate()
    @IsNotEmpty({message: 'budgetStart property not must null'})
    budgetStart: Date;

    @ApiModelProperty()
    @IsDate()
    @IsNotEmpty({message: 'budgetEnd property not must null'})
    budgetEnd: Date;
}
export class BudgetsUpdateJson extends BudgetsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
