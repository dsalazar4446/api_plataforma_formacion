import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, NotFoundException, Delete, Put, Param, Req } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { CategoriesServices } from "../services/categories.services";
import { CategoriesSaveJson, CategoriesUpdateJson } from "../interfaces/categories.inteface";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { Categories } from "../models/categories.model";
/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller CategoriesController
 * @Creado 29 de Marzo 2019
 */
@ApiUseTags('Module-Categories')
@Controller('categories')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class CategoriesController {

    constructor(private readonly _categoriesServcs: CategoriesServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: Categories,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Req() req, @Body() bodyCategories: CategoriesSaveJson) {

        if (req.headers.language) {
            bodyCategories.language = req.headers.language;
        } else {
            bodyCategories.language = 'es';
        }

        const createCategories = await this._categoriesServcs.saveNews(bodyCategories);

        if (!createCategories) {
            throw this.appUtilsService.httpCommonError('Categorías no creadas!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM16',
                languageType: 'es',
            });
        }

        return createCategories;

    }


    @ApiResponse({
        status: 200,
        type: Categories,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idCategories: string) {

        const fetchCategories = await this._categoriesServcs.getDetails(idCategories);

        if (!fetchCategories) {
            throw this.appUtilsService.httpCommonError('Categoría no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM17',
                languageType: 'es',
            });
        }

        return fetchCategories;
    }


    @ApiResponse({
        status: 200,
        type: Categories,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async findByAll() {

        const fetchAll = await this._categoriesServcs.find();

        if (!fetchAll) {
            throw this.appUtilsService.httpCommonError('Categoría no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM17',
                languageType: 'es',
            });
        }

        return fetchAll;
    }


    @ApiResponse({
        status: 200,
        type: Categories,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get(':categoryTarget?')
    @ApiImplicitParam({ name: 'categoryTarget', required: false, type: 'string' })
    async showAllCategories(@Req() req, @Param('categoryTarget') categoryTarget?: string) {

        const info = await this._categoriesServcs.showAllForums((req.headers.language), categoryTarget);

        if (!info) {
            throw this.appUtilsService.httpCommonError('Las categorías no existen! categoryTarget ?: formacion o financiera!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM18',
                languageType: 'es',
            });
        }

        return info;
    }


    @ApiResponse({
        status: 200,
        type: Categories,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateCategories(@Req() req, @Param('id') id: string, @Body() bodyCategories: Partial<CategoriesUpdateJson>) {

        if (req.headers.language) bodyCategories.language = req.headers.language;

        const updateCategories = await this._categoriesServcs.updateCategories(id, bodyCategories);

        if (!updateCategories[1][0]) {
            throw this.appUtilsService.httpCommonError('Categoría no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM17',
                languageType: 'es',
            });
        }

        return updateCategories;
    }


    @ApiResponse({
        status: 200,
        type: Categories,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteCategories(@Param('id') idCategories: string) {

        const deleteCategories = await this._categoriesServcs.destroyCategories(idCategories);

        if (!deleteCategories) {
            throw this.appUtilsService.httpCommonError('Categoría no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM17',
                languageType: 'es',
            });
        }

        return deleteCategories;
    }
}