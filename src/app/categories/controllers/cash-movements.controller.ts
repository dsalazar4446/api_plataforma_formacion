import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, NotFoundException, Delete, Put, Param } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { CashMovementsServices } from "../services/cash-movements.services";
import { CashMovementsSaveJson, CashMovementsUpdateJson } from "../interfaces/cash-movements.interface";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { CashMovements } from "../models/cash-movements.model";
/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller CashMovementsController
 * @Creado 29 de Marzo 2019
 */
@ApiUseTags('Module-Categories')
@Controller('cash-movements')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class CashMovementsController {

    constructor(private readonly _cashMovementsServices: CashMovementsServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: CashMovements,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyCashM: CashMovementsSaveJson) {

        const createCashM = await this._cashMovementsServices.saveCashMove(bodyCashM);

        if (!createCashM) {
            throw this.appUtilsService.httpCommonError('Movimientos de efectivo no creados!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM11',
                languageType: 'es',
            });
        }

        return createCashM;

    }


    @ApiResponse({
        status: 200,
        type: CashMovements,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllCashMove() {

        return await this._cashMovementsServices.showAllCashMove();
    }


    @ApiResponse({
        status: 200,
        type: CashMovements,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idCashMove: string) {

        const fetchCashMove = await this._cashMovementsServices.getDetails(idCashMove);

        if (!fetchCashMove) {
            throw this.appUtilsService.httpCommonError('Los movimientos de efectivo no existen.!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM12',
                languageType: 'es',
            });
        }

        return fetchCashMove;
    }


    @ApiResponse({
        status: 200,
        type: CashMovements,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateCashMove(@Param('id') id: string, @Body() bodyCashM: Partial<CashMovementsUpdateJson>) {

        const updateCashMove = await this._cashMovementsServices.updateCashMove(id, bodyCashM);

        if (!updateCashMove[1][0]) {
            throw this.appUtilsService.httpCommonError('Los movimientos de efectivo no existen.!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM12',
                languageType: 'es',
            });
        }

        return updateCashMove;
    }


    @ApiResponse({
        status: 200,
        type: CashMovements,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteCashMove(@Param('id') idCashMove: string) {

        const deleteCashMove = await this._cashMovementsServices.destroyCashMove(idCashMove);

        if (!deleteCashMove) {
            throw this.appUtilsService.httpCommonError('Los movimientos de efectivo no existen.!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM12',
                languageType: 'es',
            });
        }

        return deleteCashMove;
    }


}