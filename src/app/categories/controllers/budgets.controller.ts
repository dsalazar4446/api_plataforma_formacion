import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, NotFoundException, Put, Delete, Param } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { BudgetsSaveJson, BudgetsUpdateJson } from "../interfaces/budgets.interface";
import { BudgetsServices } from "../services/budgets.services";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { Budgets } from "../models/budgets.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller BudgetsController
 * @Creado 29 de Marzo 2019
 */
@ApiUseTags('Module-Categories')
@Controller('budgets')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class BudgetsController {

    constructor(private readonly _budgetsServices: BudgetsServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: Budgets,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyBudgets: BudgetsSaveJson) {

        const createBudgets = await this._budgetsServices.saveForumsM(bodyBudgets);

        if (!createBudgets) {
            throw this.appUtilsService.httpCommonError('Presupuestos no creados!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM7',
                languageType: 'es',
            });
        }

        return createBudgets;

    }


    @ApiResponse({
        status: 200,
        type: Budgets,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllBudgets() {

        return await this._budgetsServices.showAllBugets();
    }


    @ApiResponse({
        status: 200,
        type: Budgets,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idBudgets: string) {

        const fetchBudgets = await this._budgetsServices.getDetails(idBudgets);

        if (!fetchBudgets) {
            throw this.appUtilsService.httpCommonError('Presupuestos Los presupuestos no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM8',
                languageType: 'es',
            });
        }

        return fetchBudgets;
    }


    @ApiResponse({
        status: 200,
        type: Budgets,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateBudgets(@Param('id') id: string, @Body() bodyBudgets: Partial<BudgetsUpdateJson>) {

        const updateBudgets = await this._budgetsServices.updateBudgets(id, bodyBudgets);

        if (!updateBudgets[1][0]) {
            throw this.appUtilsService.httpCommonError('Presupuestos Los presupuestos no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM8',
                languageType: 'es',
            });
        }

        return updateBudgets;
    }


    @ApiResponse({
        status: 200,
        type: Budgets,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteBudgets(@Param('id') idBudgets: string) {

        const deleteBudgets = await this._budgetsServices.destroyBudgets(idBudgets);

        if (!deleteBudgets) {
            throw this.appUtilsService.httpCommonError('Presupuestos Los presupuestos no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM8',
                languageType: 'es',
            });
        }

        return deleteBudgets;
    }
}