import { Injectable, Inject } from '@nestjs/common';
import { UserActionsSaveJson, UserActionsUpdateJson } from '../interfaces/user-actions.interface';
import { UserActionsModel } from '../models/user-actions.model';

@Injectable()
export class UserActionsService {
    constructor(
        @Inject('UserActionsModel') private readonly userActionsModel: typeof UserActionsModel,
    ) { }
    async findAll(): Promise<UserActionsModel[]> {
        return  await this.userActionsModel.findAll();
    }
    async findById(id: string): Promise<UserActionsModel> {
        return await this.userActionsModel.findById<UserActionsModel>(id);
    }
    async create(userActions: UserActionsSaveJson): Promise<UserActionsModel> {
        return await this.userActionsModel.create<UserActionsModel>(userActions, {
            returning: true,
        });
    }
    async update(idUserActions: string, userActionsUpdate: Partial<UserActionsUpdateJson>){
        return  await this.userActionsModel.update(userActionsUpdate, {
            where: {
                id: idUserActions,
            },
            returning: true,
        });
    }
    async deleted(idUserActions: string): Promise<any> {
        return await this.userActionsModel.destroy({
            where: {
                id: idUserActions,
            },
        });
    }
}
