import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class UserActionsSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    userId: string;
    @ApiModelProperty()
    @IsEnum({enum:['profile','wallet','comission']})
    actionType: string;
}
// tslint:disable-next-line: max-classes-per-file
export class UserActionsUpdateJson extends UserActionsSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    id: string;
}
