import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { ApiModelProperty } from '@nestjs/swagger';
// Como aun no se dispone del modulo citie no se pudo agregar el foreign key con cities
@Table({
    tableName: 'user_actions',
})
export class UserActionsModel extends Model<UserActionsModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'user_id',
    })
    userId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.ENUM(
            'profile',
            'wallet',
            'comission',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'action_type',
    })
    actionType: string;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @CreatedAt public updated_at: Date;
}
