import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { UserActionsController } from './controllers/user-actions.controller';
import { UserActionsService } from './services/user-actions.service';
import { UserActionsModel } from './models/user-actions.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { DeviceModel } from '../device/models/device.model';
const models = [
  UserActionsModel,
  DeviceModel
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [UserActionsController],
  providers: [UserActionsService]
})
export class UserActionsModule {}
