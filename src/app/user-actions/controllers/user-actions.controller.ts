import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, Req, HttpStatus} from '@nestjs/common';
import { UserActionsService } from '../services/user-actions.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { UserActionsSaveJson, UserActionsUpdateJson } from '../interfaces/user-actions.interface';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiImplicitQuery, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { UserActionsModel } from '../models/user-actions.model';
import { UserModel } from '../../user/models/user.Model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { DeviceModel } from '../../device/models/device.model';
UserModel
@Controller('user-actions')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class UserActionsController {
    constructor(
        private readonly userActionsService: UserActionsService,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('UserActionsModel') private readonly userActionsModel: typeof UserActionsModel,
        @Inject('DeviceModel') private readonly deviceModel: typeof DeviceModel,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        @ApiResponse({
            status: 200,
            type: UserActionsModel,
          })
        @ApiImplicitQuery({ name: 'actionType', enum: ['profile','wallet','comission'] })
        @Post()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async create(@Body() userActions: UserActionsSaveJson, @Req() req) {
            const data3 = await this.userModel.findByPk(userActions.userId);
            if(data3 == null){
                throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA4',
                    languageType:  req.headers.language,
                });
            }
            return await this.userActionsService.create(userActions);
        }
        @Get()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: UserActionsModel,
            isArray: true
          })
        async getAll() {
            return await this.userActionsService.findAll();
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })

        @ApiResponse({
            status: 200,
            type: UserActionsModel,
          })
        @Get(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async findById(@Param('id') idUserActions) {
            return await this.userActionsService.findById(idUserActions);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @ApiImplicitQuery({ name: 'actionType', enum: ['profile','wallet','comission'] })
        @ApiResponse({
            status: 200,
            type: UserActionsModel,
          })
        @Put(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async update(
            @Body()
            updateUserActions: Partial<UserActionsUpdateJson>, 
            @Param('id') idUserActions,
            @Req() req
        ) {
            const data3 = await this.userModel.findByPk(updateUserActions.userId);
            if(data3 == null){
                throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA4',
                    languageType:  req.headers.language,
                });
            }
            const data2 = await this.userActionsModel.findByPk(idUserActions);
            if(data2 == null){
                throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA9',
                    languageType:  req.headers.language,
                });
            }
            return await this.userActionsService.update(idUserActions, updateUserActions);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Delete(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async delete(@Param('id') idUserActions) {
            return await this.userActionsService.deleted(idUserActions);
        }
}
