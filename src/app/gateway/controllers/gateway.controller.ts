import { ApiUseTags, ApiConsumes, ApiImplicitFile, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { Controller, UseFilters, UseInterceptors, Post, FileFieldsInterceptor, Body, HttpStatus, Get, NotFoundException, Param, Put, Delete } from "@nestjs/common";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { GatewaysServices } from "../services/gateway.service";
import * as file from './infoFile';
import { GatewaysSaveJson } from "../interfaces/gateway.interface";
import { CONFIG } from '../../../config';
import { Gateways } from "../models/gateway.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller GatewaysController
 * @Creado 02 Abril 2019
 */

@ApiUseTags('Module-Gateways')
@Controller('gateways')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class GatewaysController {

    constructor(private readonly _gatewaysServices: GatewaysServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: Gateways,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'gatewayImg', description: 'Gateway Img File', required: true })
    async create(@Body() bodyGateways: GatewaysSaveJson) {

        const createGateways = await this._gatewaysServices.saveGateways(bodyGateways);

        if (!createGateways) {
            throw this.appUtilsService.httpCommonError('Gateways no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM49',
                languageType: 'es',
            });
        }
        createGateways.gatewayImg = `${CONFIG.storage.server}gatewayImg/${createGateways.gatewayImg}`;


        return createGateways;
    }


    @ApiResponse({
        status: 200,
        type: Gateways,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async find() {

        const fetchAll: any = await this._gatewaysServices.listAllGateways();

        if (!fetchAll) {
            throw new NotFoundException('Gateways does not exist!');
        }

        fetchAll.forEach(fetch => fetch.gatewayImg = `${CONFIG.storage.server}gatewayImg/${fetch.gatewayImg}`);

        return fetchAll;
    }


    @ApiResponse({
        status: 200,
        type: Gateways,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idGateways: string) {

        const fetchGateways = await this._gatewaysServices.getGateways(idGateways);

        if (!fetchGateways) {
            throw this.appUtilsService.httpCommonError('Gateways no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM50',
                languageType: 'es',
            });
        }

        fetchGateways.gatewayImg = `${CONFIG.storage.server}gatewayImg/${fetchGateways.gatewayImg}`;

        return fetchGateways;
    }


    @ApiResponse({
        status: 200,
        type: Gateways,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'gatewayImg', description: 'Gateway Img File', required: true })
    async update(@Param('id') id: string, @Body() bodyGateways: Partial<GatewaysSaveJson>) {

        const updateGateways = await this._gatewaysServices.updateGateways(id, bodyGateways);

        if (!updateGateways[1][0]) {
            throw this.appUtilsService.httpCommonError('Gateways no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM50',
                languageType: 'es',
            });
        }

        updateGateways[1][0].gatewayImg = `${CONFIG.storage.server}gatewayImg/${updateGateways[1][0].gatewayImg}`;

        return updateGateways;
    }


    @ApiResponse({
        status: 200,
        type: Gateways,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteGateways(@Param('id') idGateways: string) {

        const deleteGateways = await this._gatewaysServices.destroyGateways(idGateways);

        if (!deleteGateways) {
            throw this.appUtilsService.httpCommonError('Gateways no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM50',
                languageType: 'es',
            });
        }

        return deleteGateways;
    }


}