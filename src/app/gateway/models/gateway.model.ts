import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany, ForeignKey } from 'sequelize-typescript';
import { TransactionsPayments } from '../../transactions/models/transactions-payments.models';
import { GroupGateways } from '../../group-gateway/models/group-gateway.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'gateways',
})
export class Gateways extends Model<Gateways>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(75),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'gateway_name',
    })
    gatewayName: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'gateway_img',
    })
    gatewayImg: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => GroupGateways)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'group_gateways_id',
    })
    groupGatewaysId: string;

    /**
     * RELACIONES
     * Gateways pertenece a TransactionsPayments
     */    
    @ApiModelPropertyOptional()
    @HasMany(() => TransactionsPayments)
    transactionsPayments: TransactionsPayments[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}