import { Module, HttpModule, Provider } from "@nestjs/common";
import { SharedModule } from "../shared/shared.module";
import { DatabaseModule } from "../database/database.module";
import { Gateways } from "./models/gateway.model";
import { GatewaysController } from "./controllers/gateway.controller";
import { GatewaysServices } from "./services/gateway.service";



const models = [ Gateways ];
const controllers = [ GatewaysController ];
const providers: Provider[] = [ GatewaysServices ];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers,
  exports: [
    GatewaysServices
  ]
})
export class GatewayModule { }
