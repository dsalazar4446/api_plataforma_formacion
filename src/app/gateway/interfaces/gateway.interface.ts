import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional } from 'class-validator';
export class GatewaysSaveJson {

    @ApiModelProperty()
    @IsString({message: 'gatewayName property must a be string'})
    @IsNotEmpty({message: 'gatewayName property not must null'})
    gatewayName: string;

    @ApiModelProperty()
    @IsString({message: 'gatewayImg property must a be string'})
    @IsOptional()
    gatewayImg?: string;

    @ApiModelProperty()
    @IsString({message: 'groupGatewaysId property must a be string'})
    @IsOptional()
    groupGatewaysId;
}
// tslint:disable-next-line: max-classes-per-file
export class GatewaysUpdateJson extends GatewaysSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
