import { Injectable, Inject } from "@nestjs/common";
import { Gateways } from "../models/gateway.model";
import { GatewaysSaveJson, GatewaysUpdateJson } from "../interfaces/gateway.interface";
import { TransactionsPayments } from "../../transactions/models/transactions-payments.models";
import * as path from 'path';
import * as fs from 'fs';
import { Op } from 'sequelize'

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services GatewaysServices
 * @Creado 17 Abril 2019
 */

@Injectable()
export class GatewaysServices {

    constructor(
        @Inject('Gateways') private readonly _gateways: typeof Gateways
    ) { }

    async saveGateways(bodyGateways: GatewaysSaveJson): Promise<GatewaysUpdateJson> {

        return await new this._gateways(bodyGateways).save();
    }

    async listAllGateways() {

        return await this._gateways.findAll();
    }

    async getGateways(id): Promise<GatewaysUpdateJson> {

        return await this._gateways.findByPk(id, {
            include: [
                {
                    model: TransactionsPayments
                }
            ]
        });
    }

    async findByGatewayName(name: string){
        return await this._gateways.findOne({
            where: {
                gatewayName:{ 
                    [Op.iLike]: '%'+name
                }
                
            }
        })
    }

    async updateGateways(id: string, bodyGateways: Partial<GatewaysUpdateJson>): Promise<[number, Array<any>]> {

        if( bodyGateways.gatewayImg ){
            await this.deleteImg(id);
        }

        return await this._gateways.update(bodyGateways, {
            where: { id },
            returning: true
        });
    }

    async destroyGateways(id: string): Promise<number> {
        
        await this.deleteImg(id);
        return await this._gateways.destroy({
            where: { id },
        });
    }

    private async deleteImg(id: string){
        const img = await this._gateways.findByPk(id).then(item => item.gatewayImg).catch(err => err);
    
        const pathImagen = path.resolve(__dirname, `../../../../uploads/gatewayImg/${img}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }

}