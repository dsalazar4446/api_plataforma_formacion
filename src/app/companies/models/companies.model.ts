import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { CityModel } from '../../city/models/city.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'companies',
})
export class Companies extends Model<Companies>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => CityModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'city_id',
    })
    cityId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'company_name',
    })
    companyName: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'company_identification',
    })
    companyIdentification: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'company_email',
    })
    companyEmail: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'company_logo',
    })
    companyLogo: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'company_phone',
    })
    companyPhone: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'company_address',
    })
    companyAddress: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'company_postal_code',
    })
    companyPostalCode: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'company_website',
    })
    companyWebsite: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('unilateral', 'bilateral', 'trilateral'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        defaultValue: 'unilateral',
        field: 'mlm_type',
    })
    mlmType: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('short', 'long', 'both'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        defaultValue: 'short',
        field: 'mlm_payment_side',
    })
    mlmPaymentSide: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        defaultValue: false,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'mlm_choosed_by_user',
    })
    mlmChoosedByUser: boolean;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'bank_peru_name',
    })
    bankPeruName: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'bank_peru_account_number',
    })
    bankPeruAccountNumber: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'bank_international_name',
    })
    bankInternationalName: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'bank_international_isbn',
    })
    bankInternationalIsbn: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'bank_international_account_number',
    })
    bankInternationalAccountNumber: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'host_odoo',
    })
    hostOdoo: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'db_odoo',
    })
    dbOdoo: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'username_odoo',
    })
    usernameOdoo: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'password_odoo',
    })
    passwordOdoo: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'value_document_foreign_id',
    })
    valueDocumentForeignId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'value_document_peruvian_id',
    })
    valueDocumentPeruvianId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.INTEGER,
        defaultValue: 0,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'ranking_time_renawal',
    })
    rankingTimeRenawal: number;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'value_document_peruvian_ruc_id',
    })
    valueDocumentPeruvianRucId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'type_operation_peruvian_id',
    })
    typeOperationPeruvianId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'type_operation_foreign_id',
    })
    typeOperationForeignId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'journal_id_bill',
    })
    journalIdBill: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'journal_id_ticket',
    })
    journalIdTicket: string;

    /**
     * RELACIONES
     * Companies pertenece a CityModel
     */

    @BelongsTo(() => CityModel)
    cityModel: CityModel;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}