import { Controller, UseFilters, UseInterceptors, Post, FileFieldsInterceptor, Body, HttpStatus, Get, NotFoundException, Put, Delete, Param } from "@nestjs/common";
import { CompaniesServices } from "../services/companies.service";
import { diskStorage } from 'multer';
import { extname } from "path";
import { CompaniesSaveJson, CompaniesUpdateJson } from "../interfaces/companies.interface";
import { CONFIG } from "../../../config";
import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiConsumes, ApiUseTags, ApiImplicitParam, ApiImplicitFile, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { Companies } from "../models/companies.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller CompaniesController
 * @Creado 02 Abril 2019
 */

@ApiUseTags('Module-Companies')
@Controller('companies')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class CompaniesController {

    constructor(private readonly _companiesServices: CompaniesServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: Companies,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    @UseInterceptors(FileFieldsInterceptor(
        [
            { name: 'companyLogo', maxCount: 1 }
        ],
        {
            storage: diskStorage({
                filename: (req, file, cb) => {

                    let extValidas = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG'];

                    if (extValidas.indexOf(extname(file.originalname)) < 0) {
                        cb(`valid extensions:${extValidas.join(', ')} `);
                        return;
                    }
                    const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                    if (file.fieldname === 'companyLogo') {
                        req.body.companyLogo = `${randomName}${extname(file.originalname)}`;
                    }
                    cb(null, `${randomName}${extname(file.originalname)}`);
                },
                destination: './uploads/companyLogo',
            }),
        }))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'companyLogo', description: 'Company Logo File', required: true })
    async create(@Body() bodyCompanies: CompaniesSaveJson) {

        const createCompanies = await this._companiesServices.saveCompanies(bodyCompanies);

        if (!createCompanies) {
            throw this.appUtilsService.httpCommonError('Compañia no ha sido creada!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM25',
                languageType: 'es',
            });
        }
        createCompanies.companyLogo = `${CONFIG.storage.server}companyLogo/${createCompanies.companyLogo}`;

        return createCompanies;

    }


    @ApiResponse({
        status: 200,
        type: Companies,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllComapnies() {

        const fetchAll: CompaniesUpdateJson[] = await this._companiesServices.showAllCompanies();

        if (!fetchAll) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        fetchAll.forEach(fetch => fetch.companyLogo = `${CONFIG.storage.server}companyLogo/${fetch.companyLogo}`);
        return fetchAll;
    }


    @ApiResponse({
        status: 200,
        type: Companies,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idCompanies: string) {

        const fetchCompanies = await this._companiesServices.getDetails(idCompanies);

        if (!fetchCompanies) {
            throw this.appUtilsService.httpCommonError('Compañia no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM26',
                languageType: 'es',
            });
        }
        fetchCompanies.companyLogo = `${CONFIG.storage.server}companyLogo/${fetchCompanies.companyLogo}`;

        return fetchCompanies;
    }


    @ApiResponse({
        status: 200,
        type: Companies,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(FileFieldsInterceptor(
        [
            { name: 'companyLogo', maxCount: 1 }
        ],
        {
            storage: diskStorage({
                filename: (req, file, cb) => {

                    let extValidas = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG'];

                    if (extValidas.indexOf(extname(file.originalname)) < 0) {
                        cb(`valid extensions:${extValidas.join(', ')} `);
                        return;
                    }
                    const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                    if (file.fieldname === 'companyLogo') {
                        req.body.companyLogo = `${randomName}${extname(file.originalname)}`;
                    }
                    cb(null, `${randomName}${extname(file.originalname)}`);
                },
                destination: './uploads/companyLogo',
            })
        }))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'companyLogo', description: 'Company Logo File', required: true })
    async updateCompanies(@Param('id') id: string, @Body() bodyCompanies: Partial<CompaniesSaveJson>) {

        const updateCompanies = await this._companiesServices.updateCompanies(id, bodyCompanies);

        if (!updateCompanies[1][0]) {
            throw this.appUtilsService.httpCommonError('Compañia no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM26',
                languageType: 'es',
            });
        }
        updateCompanies[1][0].companyLogo = `${CONFIG.storage.server}companyLogo/${updateCompanies[1][0].companyLogo}`;
        return updateCompanies;
    }


    @ApiResponse({
        status: 200,
        type: Companies,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteCompanies(@Param('id') idCompanies: string) {

        const deleteCompanies = await this._companiesServices.destroyCompanies(idCompanies);

        if (!deleteCompanies) {
            throw this.appUtilsService.httpCommonError('Compañia no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM26',
                languageType: 'es',
            });
        }

        return deleteCompanies;
    }
}