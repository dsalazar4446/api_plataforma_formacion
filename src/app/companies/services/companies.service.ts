import { Injectable, Inject } from "@nestjs/common";
import { Companies } from "../models/companies.model";
import { CompaniesSaveJson, CompaniesUpdateJson } from "../interfaces/companies.interface";
import { CityModel } from "../../city/models/city.model";
import * as path from 'path';
import * as fs from 'fs';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services CompaniesServices
 * @Creado 05 Abril 2019
 */

@Injectable()
export class CompaniesServices {

    constructor(
        @Inject('Companies') private readonly _companies: typeof Companies
    ) { }

    async saveCompanies(bodyCompanies: CompaniesSaveJson): Promise<CompaniesUpdateJson> {

        if (!bodyCompanies.companyLogo) bodyCompanies.companyLogo = 'default.jpeg';

        return await new this._companies(bodyCompanies).save();
    }

    async showAllCompanies(): Promise<CompaniesUpdateJson[]> {

        return await this._companies.findAll();
    }

    async getDetails(companiesId: string) {

        return await this._companies.findByPk(companiesId, {
            include: [
                {
                    model: CityModel
                }
            ]
        });
    }

    async updateCompanies(id: string, bodyCompanies: Partial<CompaniesUpdateJson>): Promise<[number, Array<any>]> {

        if( bodyCompanies.companyLogo ){
            await this.deleteImg(id);
        }

        return await this._companies.update(bodyCompanies, {
            where: { id },
            returning: true
        });

    }

    async destroyCompanies(companiesId: string): Promise<number> {

        await this.deleteImg(companiesId);
        return await this._companies.destroy({
            where: { id: companiesId },
        });
    }

    private async deleteImg(id: string) {

        const img = await this._companies.findByPk(id).then(item => item.companyLogo).catch(err => err);

        if(img === 'default.jpeg') return;

        const pathImagen = path.resolve(__dirname, `../../../../uploads/companyLogo/${img}`);
        
        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }
    

}