import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional, IsBoolean, IsNumber } from 'class-validator';
export class CompaniesSaveJson {
    @ApiModelProperty()
    @IsUUID('4',{message: 'cityId property must a be uuid'})
    @IsNotEmpty({message: 'cityId property not must null'})
    cityId: string;

    @ApiModelProperty()
    @IsString({message: 'companyName property must a be string'})
    @IsNotEmpty({message: 'companyName property not must null'})
    companyName: string;

    @ApiModelProperty()
    @IsString({message: 'companyIdentification property must a be string'})
    @IsNotEmpty({message: 'companyIdentification property not must null'})
    companyIdentification: string;

    @ApiModelProperty()
    @IsString({message: 'companyEmail property must a be string'})
    @IsNotEmpty({message: 'companyEmail property not must null'})
    companyEmail: string;

    @ApiModelProperty()
    @IsString({message: 'companyLogo property must a be string'})
    @IsOptional()
    companyLogo?: string;

    @ApiModelProperty()
    @IsString({message: 'companyPhone property must a be string'})
    @IsNotEmpty({message: 'companyPhone property not must null'})
    companyPhone: string;

    @ApiModelProperty()
    @IsString({message: 'companyAddress property must a be string'})
    @IsNotEmpty({message: 'companyAddress property not must null'})
    companyAddress: string;

    @ApiModelProperty()
    @IsString({message: 'companyPostalCode property must a be string'})
    @IsNotEmpty({message: 'companyPostalCode property not must null'})
    companyPostalCode: string;

    @ApiModelProperty()
    @IsString({message: 'companyWebsite property must a be string'})
    @IsNotEmpty({message: 'companyWebsite property not must null'})
    companyWebsite: string;

    @ApiModelProperty()
    @IsString({message: 'mlmType property must a be string'})
    @IsOptional()
    mlmType: string;

    @ApiModelProperty()
    @IsString({message: 'mlmPaymentSide property must a be string'})
    @IsOptional()
    mlmPaymentSide: string;

    @ApiModelProperty()
    @IsBoolean({message: 'mlmChoosedByUser property must a be boolean'})
    @IsOptional()
    mlmChoosedByUser: boolean;

    @ApiModelProperty()
    @IsString({message: 'bankPeruName property must a be string'})
    @IsNotEmpty({message: 'bankPeruName property not must null'})
    bankPeruName: string;

    @ApiModelProperty()
    @IsString({message: 'bankPeruAccountNumber property must a be string'})
    @IsNotEmpty({message: 'bankPeruAccountNumber property not must null'})
    bankPeruAccountNumber: string;

    @ApiModelProperty()
    @IsString({message: 'bankInternationalName property must a be string'})
    @IsNotEmpty({message: 'bankInternationalName property not must null'})
    bankInternationalName: string;

    @ApiModelProperty()
    @IsString({message: 'bankInternationalIsbn property must a be string'})
    @IsNotEmpty({message: 'bankInternationalIsbn property not must null'})
    bankInternationalIsbn: string;

    @ApiModelProperty()
    @IsString({message: 'bankInternationalAccountNumber property must a be string'})
    @IsNotEmpty({message: 'bankInternationalAccountNumber property not must null'})
    bankInternationalAccountNumber: string;

    @ApiModelProperty()
    @IsString({message: 'hostOdoo property must a be string'})
    @IsNotEmpty({message: 'hostOdoo property not must null'})
    hostOdoo: string;

    @ApiModelProperty()
    @IsString({message: 'dbOdoo property must a be string'})
    @IsNotEmpty({message: 'dbOdoo property not must null'})
    dbOdoo: string;

    @ApiModelProperty()
    @IsString({message: 'usernameOdoo property must a be string'})
    @IsNotEmpty({message: 'usernameOdoo property not must null'})
    usernameOdoo: string;

    @ApiModelProperty()
    @IsString({message: 'passwordOdoo property must a be string'})
    @IsNotEmpty({message: 'passwordOdoo property not must null'})
    passwordOdoo: string;

    @ApiModelProperty()
    @IsString({message: 'valueDocumentForeignId property must a be string'})
    @IsNotEmpty({message: 'valueDocumentForeignId property not must null'})
    valueDocumentForeignId: string;

    @ApiModelProperty()
    @IsString({message: 'valueDocumentPeruvianId property must a be string'})
    @IsNotEmpty({message: 'valueDocumentPeruvianId property not must null'})
    valueDocumentPeruvianId: string;

    @ApiModelProperty()
    @IsString({message: 'valueDocumentPeruvianRucId property must a be string'})
    @IsNotEmpty({message: 'valueDocumentPeruvianRucId property not must null'})
    valueDocumentPeruvianRucId: string;

    @ApiModelProperty()
    @IsString({message: 'typeOperationPeruvianId property must a be string'})
    @IsNotEmpty({message: 'typeOperationPeruvianId property not must null'})
    typeOperationPeruvianId: string;

    @ApiModelProperty()
    @IsString({message: 'typeOperationForeignId property must a be string'})
    @IsNotEmpty({message: 'typeOperationForeignId property not must null'})
    typeOperationForeignId: string;

    @ApiModelProperty()
    @IsString({message: 'journalIdBill property must a be string'})
    @IsNotEmpty({message: 'journalIdBill property not must null'})
    journalIdBill: string;

    @ApiModelProperty()
    @IsString({message: 'journalIdTicket property must a be string'})
    @IsNotEmpty({message: 'journalIdTicket property not must null'})
    journalIdTicket: string;

    @ApiModelProperty()
    @IsNumber(null, {message: 'rankingTimeRenawal property must a be number'})
    @IsOptional()
    rankingTimeRenawal: number;
}
// tslint:disable-next-line: max-classes-per-file
export class CompaniesUpdateJson extends CompaniesSaveJson {

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}