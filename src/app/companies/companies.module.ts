import { Module, HttpModule, Provider } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { Companies } from './models/companies.model';
import { CompaniesServices } from './services/companies.service';
import { CompaniesController } from './controllers/companies.controller';

const models = [Companies];

const providers: Provider[] = [CompaniesServices];

const controllers = [CompaniesController];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers
})
export class CompaniesModule { }
