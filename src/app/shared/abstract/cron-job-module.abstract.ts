import { CronJobService } from '../services/cron-job.service';
import { AppCronJob } from "../interfaces/app-cron-job.interface";
import { CronJobParameters } from 'cron';

export abstract class CronJobModule {
    constructor(
        protected readonly cronJobService: CronJobService,
        protected readonly jobs: any[],
    ) {
        this.jobs.forEach((job) => {
            
            const options: CronJobParameters = {
                cronTime: job.cronTime,
                onTick: job.onTick.bind(job),
                onComplete: job.onComplete ? job.onComplete.bind(job) : undefined,
                start: job.start,
                timeZone: job.timeZone,
                context: job.context,
                runOnInit: job.runOnInit,
                utcOffset: job.utcOffset,
                unrefTimeout: job.unrefTimeout,
            };
            this.cronJobService.createJob(job.jobName, job.description, options);
            if (!job.start) {
                this.cronJobService.startJob(job.jobName);
            }
        });
    }
}
