import * as Ajv from 'ajv';
import * as TJS from 'typescript-json-schema';
import { PipeTransform, ArgumentMetadata, HttpException } from '@nestjs/common';
import * as path from 'path';
import { TranslateService } from '../services/translate.service';
import { CONFIG } from '../../../config';
const settings: TJS.PartialArgs = {
  required: true,
};

const compilerOptions: TJS.CompilerOptions = {
  strictNullChecks: true,
};

const ajv = new Ajv();

export class AjvValidationPipe implements PipeTransform {
  private static files: string[] = [];
  private static generator: TJS.JsonSchemaGenerator;
  private validate: any;
  private type: string;
  private jsonSchema: TJS.Definition;

  constructor(
    interfaceFile: string,
    type: string,
    basePath: string,
  ) {
    basePath = path.resolve(`${__dirname}/../../${basePath}`);
    if (CONFIG.useDefinitionExt) {
      interfaceFile = interfaceFile.replace(/\.ts$/, '.d.ts');
    }
    // const fullInterfacePath = `${basePath}/${interfaceFile}`;
    // if (AjvValidationPipe.files.indexOf(fullInterfacePath) > -1) {
    //   AjvValidationPipe.files.push(fullInterfacePath);
    // }
    // this.type = type;
  }

  static compile() {
    const program = TJS.getProgramFromFiles(this.files, compilerOptions);
    AjvValidationPipe.generator = TJS.buildGenerator(program, settings);
  }

  transform(value: any, metadata: ArgumentMetadata) {
    if (!this.validate) {

      this.jsonSchema = AjvValidationPipe.generator.getSchemaForSymbol(this.type);
      delete this.jsonSchema.$schema;
      this.validate = ajv.compile(this.jsonSchema);
    }
    const valid = this.validate(value);
    if (valid) {
      return value;
    }
    throw new HttpException({
      description: TranslateService.translate('Error en la validación ajv del esquema json'),
      details: this.validate.errors,
    }, 400);
  }
}

// import * as Ajv from 'ajv';
// import * as TJS from 'typescript-json-schema';
// import { PipeTransform, ArgumentMetadata, HttpException } from '@nestjs/common';
// import * as path from 'path';
// import { TranslateService } from '../services/translate.service';
// import { CONFIG } from '../../../config';
// const settings: TJS.PartialArgs = {
//   required: true,
// };

// const compilerOptions: TJS.CompilerOptions = {
//   strictNullChecks: true,
// };

// const ajv = new Ajv();

// export class AjvValidationPipe implements PipeTransform {

//   private validate: any;
//   private jsonSchema: TJS.Definition;

//   constructor(
//     interfaceFile: string,
//     type: string,
//     basePath: string,
//   ) {
    
//     basePath = path.resolve(`${__dirname}/../../${basePath}`);
    
//     if (CONFIG.useDefinitionExt) {
//       interfaceFile = interfaceFile.replace(/\.ts$/, '.d.ts');
//     }

//     const program = TJS.getProgramFromFiles([`${basePath}/${interfaceFile}`], compilerOptions, basePath);
//     const generator = TJS.buildGenerator(program, settings);
//     this.jsonSchema = generator.getSchemaForSymbol(type);
//     delete this.jsonSchema.$schema;
//     this.validate = ajv.compile(this.jsonSchema);

//   }

//   transform(value: any, metadata: ArgumentMetadata) {
//     const valid = this.validate(value);
//     if (valid) {
//       return value;
//     }
//     throw new HttpException({
//       description: TranslateService.translate('Error en la validación ajv del esquema json'),
//       details: this.validate.errors,
//     }, 400);
//   }
// }
