import { CONFIG } from "../../../config";

export const completeUrl = (folder: string, img: string) => {
    return `${CONFIG.storage.server}${folder}/${img}`;
}
