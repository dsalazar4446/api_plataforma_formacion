import { AppUtilsService } from '../../shared/services/app-utils.service';
import { HttpStatus, Injectable } from '@nestjs/common';

@Injectable()
export class ValidateUUID {

    private readonly pattern = /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i;
    constructor(
        private readonly appUtilsService: AppUtilsService
    ) { }

    validateUUIID(id: string): any {

        if (!id.match(this.pattern)) {
            throw this.appUtilsService.httpCommonError('UUID No Valid!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA148',
                languageType: 'es'
            });
        }
    }

}
