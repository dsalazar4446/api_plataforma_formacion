import { Injectable, NestInterceptor, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Response<T> {
  data: T;
}

@Injectable()
export class AppResponseInterceptor<T>
  implements NestInterceptor<T, Response<T>> {
  intercept(
    context: ExecutionContext,
    call$: Observable<T>,
  ): Observable<Response<T>> {
    const request = context.getArgByIndex(1);
    return call$.pipe(
      map(
        data => {
          return {
            statusCode: request.statusCode,
            error: false,
            data
          };
        },
      ),
    );
  }
}
