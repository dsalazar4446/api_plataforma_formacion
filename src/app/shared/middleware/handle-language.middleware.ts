import { Injectable, NestMiddleware, MiddlewareFunction } from '@nestjs/common';
import { TranslateService } from '../services/translate.service';
import { ConfigService} from '../services/config.service';
import { AppLanguages } from '../services/translate.service';

@Injectable()
export class HandleLanguageMiddleware implements NestMiddleware {
    private defaultLng: AppLanguages;
    private languageHeader: AppLanguages;
    constructor(
        private readonly translateService: TranslateService,
        private readonly configService: ConfigService,
    ) {
        this.defaultLng = this.configService.get('i18next.defaultLng');
        this.languageHeader = this.configService.get('headers.language');
    }
    resolve(...args: any[]): MiddlewareFunction {
        return (req, res, next) => {
            
            let language: AppLanguages = req.header(this.languageHeader);
            if (!(language && AppLanguages[language.toUpperCase()])) {
                language = this.defaultLng;
            }
            if (language === this.translateService.getCurrentLang()) {
                next();
            } else {
                this.translateService.changeLanguage(language).then(() => {
                    next();
                });
            }

        };
    }
}
