import { Injectable, MiddlewareFunction, NestMiddleware } from '@nestjs/common';
import { AuthsService } from '../../auths/services/auths.service';
import { RoleEndpointsService } from './../../roles/services/role-endpoints/role-endpoints.service';
import { RoleViewService } from './../../roles/services/role-view/role-view.service';
// import { JwtService } from '@nestjs/jwt';


@Injectable()
export class RoleMiddleware implements NestMiddleware {
  constructor(
    private readonly authsService: AuthsService,
    private readonly roleEndpointsService: RoleEndpointsService,
    private readonly roleViewService: RoleViewService,

    ) {}
  resolve(...args: any[]): MiddlewareFunction {
    const unautorize = {
      status: 'error',
      statusCode: 401,
      error: 'Unauthorized',
    };
    return async (req, res, next) => {
      let token: string;
      if (req.header.authorization) {
        token = req.header.authorization.split(' ')[1];
        if(token) {
          const infoUser = this.authsService.decode(token);
          const user = await this.authsService.validateUser(infoUser.email);
          if (user) {
            const roleEndpoint = await this.roleEndpointsService.checkRoleEndpoint(user.roleId, req.endpoint);
            const roleView = await this.roleViewService.checkRoleView(user.roleId, req.roleEndpoint);

            if (roleEndpoint){
              if (roleEndpoint.getStatus() == 401) {
                return res.send(roleEndpoint);
              } else {
                next();
              }
            }

            if (roleView){
              if (roleView.getStatus() == 401) {
                return res.send(roleView);
              } else {
                next();
              }
            }

          } else {
            res.send(unautorize);
          }
        } else{
          res.send(unautorize);
        }
      } else {
        res.send(unautorize);
      }
      next();
    }
  }
}

