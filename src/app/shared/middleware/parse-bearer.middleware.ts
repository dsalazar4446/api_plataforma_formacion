import { Injectable, NestMiddleware, MiddlewareFunction } from '@nestjs/common';

@Injectable()
export class ParseBearerMiddleware implements NestMiddleware {
    resolve(...args: any[]): MiddlewareFunction {
        return (req, res, next) => {
            const bearer = req.header('authorization');
            const token = bearer ? bearer.replace('Bearer ', '') : null;
            req.auth = {
                token,
            };
            next();
        };
    }
}