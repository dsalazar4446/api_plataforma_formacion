import { Injectable, Inject } from '@nestjs/common';
import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';
import { APP_LOGGER } from '../../../logger';
import ApiMessageModel from '../models/sequelize/api-message.model';

@Catch()
export class AppExceptionFilter implements ExceptionFilter {

  async getApiMessage(languageType: string, messageCode: string, httpErrorCode: number) {
// tslint:disable-next-line: triple-equals
    if (languageType == "es" || languageType == "en" || languageType == "pr" || languageType == "it") {
      const data = await ApiMessageModel.findOne({
        where: {
          languageType,
          messageCode,
        },
      });
      return {
        'description': data.dataValues.messageText,
        'details': {
          'messageCode': data.dataValues.messageCode,
          'message': data.dataValues.messageText,
          'languageType': data.dataValues.languageType,
          'httpErrorCode': httpErrorCode,
        },
      };
    } else {
      return {
        'description': 'Enter a valid language option',
        'details': {
          'message': 'Enter a valid language option',
          'languageType': languageType,
          'httpErrorCode': httpErrorCode,
        },
      };
    }


  }
  async catch(exception: HttpException | Error, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    let statusCode: number;
    let data: any;
    let auxData: any;
    if (exception instanceof HttpException) {
      const exep = exception as HttpException;
      statusCode = exep.getStatus();
      auxData = exep.message;
      if(auxData.details == undefined || auxData.details == null){
        data = auxData;
      }else{
        data = await this.getApiMessage(auxData.details.languageType, auxData.details.messageCode, statusCode);
      }
    } else {
      const exep = exception as Error;
      statusCode = 500;
      data = {
        description: 'Error inesperado en creex api',
      };
      APP_LOGGER.error('Error stringify failed', exep);
    }

    response
      .status(statusCode)
      .json({
        statusCode,
        error: true,
        path: request.url,
        timestamp: new Date(),
        data,
      });
  }
}
