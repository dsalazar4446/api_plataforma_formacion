import { CronJob, CronJobParameters } from 'cron';

export interface AppCronJobStoreDto {
    job: CronJob;
    description?: string;

}

export interface AppCronJob extends CronJobParameters {
    jobName: string;
    description?: string;
}
