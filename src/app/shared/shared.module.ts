import { Module, Provider, forwardRef } from '@nestjs/common';
import { ConfigService } from './services/config.service';
import { NodemailerService } from './services/nodemailer.service';
import { AppUtilsService } from './services/app-utils.service';
import { CronJobService } from './services/cron-job.service';
import { DatabaseModule } from '../database/database.module';
import { TranslateService } from './services/translate.service';
import ApiMessageModel  from './models/sequelize/api-message.model';
import { AuthsModule } from '../auths/auths.module';
import { RolesModule } from '../roles/roles.module';
import { ValidateUUID } from './utils/validateUUID';
import { PassportModule } from '@nestjs/passport';
import { LogsModule } from '../logs/logs.module';
import { CityModule } from '../city/city.module';
// import { JwtModule } from '@nestjs/jwt';

const services: Provider[] = [
    ConfigService,
    AppUtilsService,
    // Used like it, because of a bug when providers of the same module are resolved
    {
        provide: 'CronJobService',
        useClass: CronJobService,
    },
    TranslateService,
    NodemailerService,
    ValidateUUID
];
const models = [ApiMessageModel];
@Module({
    imports: [
        // PassportModule.register({ defaultStrategy: 'jwt', session: true }),
        DatabaseModule.forRoot({
            sequelize: {
                models
            },
        }),
        forwardRef(() => RolesModule),
        forwardRef(() => AuthsModule),
        forwardRef(() => LogsModule),
        forwardRef(() => CityModule),
    ],
    controllers: [],
    providers: services,
    exports: services,
})
export class SharedModule { }
