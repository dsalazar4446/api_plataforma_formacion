import { Injectable, Inject } from '@nestjs/common';
import * as i18next from 'i18next';
import * as BBPromise from 'bluebird';
import { ConfigService } from './config.service';
import  ApiMessageModel  from '../models/sequelize/api-message.model';

export enum AppLanguages {
    EN = 'en',
    ES = 'es',
    IT = 'it',
    PR = 'pr',
}

@Injectable()
export class TranslateService {
    private static currentLang: AppLanguages;
    constructor(
        @Inject('ApiMessageModel') private readonly apiMessageModel: typeof ApiMessageModel,
        private readonly configService: ConfigService,
    ) { }

    /**
     * this is used when app is starting
     * stores the database keys in i18next framework resources
     */
    async _init(): Promise<void> {
        TranslateService.currentLang = this.configService.get('i18next.defaultLng');
        const languageValues = Object.keys(AppLanguages);
        // fetching the languages from Database
        const i18NextResources: i18next.default.Resource = {};
        await BBPromise.reduce(languageValues, async (object, key) => {
            const languageCode = AppLanguages[key];
            const languageKeys = await this.apiMessageModel.findAll({
                where: {
                    languageType: languageCode,
                },
            });
            const objectOfLanguageKeys = languageKeys.reduce((langObject, value) => {
                langObject[value.messageCode] = value.messageText;
                return langObject;
            }, {}) || {};
            object[languageCode] = {
                translation: objectOfLanguageKeys,
            };
            return object;
        }, i18NextResources);
        i18NextResources[AppLanguages.ES].translation = {
            test: 'domo',
        };
        i18NextResources[AppLanguages.EN].translation = {
            test: 'ingles',
        };
        // initializing i18next
        await i18next.default.init({
            lng: TranslateService.currentLang,
            fallbackLng: this.configService.get('i18next.fallbackLng'),
            resources: i18NextResources,
        });
    }

    async changeLanguage(lang: AppLanguages): Promise<void> {
        TranslateService.currentLang = lang;
        await i18next.default.changeLanguage(lang);
    }

    translate(
        key: string | string[],
        options?: string | i18next.default.TOptions<i18next.default.StringMap>,
    ): string {
        return i18next.default.t(key, options);
    }

    static translate(
        key: string | string[],
        options?: string | i18next.default.TOptions<i18next.default.StringMap>,
    ): string {
        return i18next.default.t(key, options);
    }

    getCurrentLang(): AppLanguages {
        return TranslateService.currentLang;
    }

}
