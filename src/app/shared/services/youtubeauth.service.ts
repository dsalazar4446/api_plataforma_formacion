import { Injectable} from '@nestjs/common';
import * as fs from 'fs';
import * as readline from 'readline';
import {google} from 'googleapis';
import { APP_LOGGER } from '../../../logger';
@Injectable()
export class YoutubeAuthService {
    private TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE) + '/.credentials/';
    private TOKEN_PATH = this.TOKEN_DIR + 'crex-credentials.json';
    private SCOPES = [
      'https://www.googleapis.com/auth/youtube',
      'https://www.googleapis.com/auth/youtube.upload',
      'https://www.googleapis.com/auth/youtubepartner',
      'https://www.googleapis.com/auth/youtube.force-ssl'
    ];
    constructor() { }
    async auth(){
      return new Promise((resolve, reject) =>{
        const routeClienteSecret = (__dirname).replace('/src/app/shared/services', '/config/client_secret.json');
        fs.readFile(routeClienteSecret, (err, content: any) => {
            if (err) {
              APP_LOGGER.error('Error loading client secret file: ', err);
              reject(err);
            }
            // Authorize a client with the loaded credentials, then call the YouTube API.
            this.authorize(JSON.parse(content), (authData) => { 
              APP_LOGGER.info('Youtube Auth Success');
              resolve(authData);
            });
        });
      });
    }
    async authorize(credentials, callback) {
        const clientSecret = credentials.web.client_secret;
        const clientId = credentials.web.client_id;
        const redirectUrl = credentials.web.redirect_uris[0];
        const  OAuth2 = google.auth.OAuth2;
        const oauth2Client = new OAuth2(clientId, clientSecret, redirectUrl);
        // Check if we have previously stored a token.
        fs.readFile(this.TOKEN_PATH, (err, token: any) => {
          if (err) {
            this.getNewToken(oauth2Client, callback);
          } else {
            oauth2Client.credentials = JSON.parse(token);
            callback(oauth2Client);
          }
        });
    }
    async getNewToken(oauth2Client, callback) {
        const authUrl = oauth2Client.generateAuthUrl({
          access_type: 'offline',
          approval_prompt: 'force',
          scope: this.SCOPES,
        });
        APP_LOGGER.info('Authorize this app by visiting this url: ' + authUrl);
        var rl = readline.createInterface({
          input: process.stdin,
          output: process.stdout
        });
        rl.question('Enter the code from that page here: ', (code) => {
          rl.close();
          oauth2Client.getToken(code, (err, token) => {
            if (err) {
              APP_LOGGER.error('Error while trying to retrieve access token', err);
              return;
            }
            oauth2Client.credentials = token;
            this.storeToken(token);
            callback(oauth2Client);
          });
        });
      }
      async storeToken(token) {
        try {
          fs.mkdirSync(this.TOKEN_DIR);
        } catch (err) {
          if (err.code != 'EEXIST') {
            throw err;
          }
        }
        fs.writeFile(this.TOKEN_PATH, JSON.stringify(token), (err) => {
          if (err) throw err;
          APP_LOGGER.info('Token stored to ' + this.TOKEN_PATH);
        });
        APP_LOGGER.info('Token stored to ' + this.TOKEN_PATH);
      }
}
