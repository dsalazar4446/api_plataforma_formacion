import { Injectable, HttpException } from '@nestjs/common';
import { TranslateService } from './translate.service';

@Injectable()
export class AppUtilsService {

    constructor(
        private readonly translateService: TranslateService,
    ) {

    }

    requestPromiseHttpError(description: string, requestData: any) {
        return new HttpException({
            description: this.translateService.translate(description),
            details: requestData.error,
        }, requestData.response.statusCode);
    }

    httpCLienHttpError(description: string, requestData: any) {
        return new HttpException({
            description: this.translateService.translate(description),
            details: requestData.response.data,
        }, requestData.response.status);
    }

    httpCommonError(description: string, responseCode: number, details?: any) {
        return new HttpException({
            description: this.translateService.translate(description),
            details,
        }, responseCode);
    }
}
