import { Injectable, Inject, Optional } from '@nestjs/common';
import { CronJob, CronJobParameters } from 'cron';
import { AppUtilsService } from './app-utils.service';
import { AppCronJobStoreDto } from '../interfaces/app-cron-job.interface';

@Injectable()
export class CronJobService {

    private static readonly jobsStore: {
        [key: string]: AppCronJobStoreDto,
    } = {};

    constructor(
        private readonly appUtilsService: AppUtilsService,
    ) { }

    private noJobError(jobName: string) {
        return this.appUtilsService.httpCommonError(
            `El job con nombre: ${jobName}, no existe`,
            404,
        );
    }

    createJob(name: string, description: string = null, options: CronJobParameters): void {
        const storedJob = CronJobService.jobsStore[name];
        if (storedJob) {
            throw this.appUtilsService.httpCommonError(
                `El job con nombre: ${name}, ya existe`,
                409,
            );
        }
        CronJobService.jobsStore[name] = {
            job: new CronJob(options),
            description,
        };
    }

    stopJob(name: string): void {
        const storedJob = CronJobService.jobsStore[name];
        if (!storedJob) {
            throw this.noJobError(name);
        }
        storedJob.job.stop();
    }

    startJob(name: string): void {
        const storedJob = CronJobService.jobsStore[name];
        if (!storedJob) {
            throw this.noJobError(name);
        }
        storedJob.job.start();
    }

    getJob(name: string): AppCronJobStoreDto {
        const storedJob = CronJobService.jobsStore[name];
        if (!storedJob) {
            throw this.noJobError(name);
        }
        return storedJob;
    }

    destroyJob(name: string): void {
        const storedJob = CronJobService.jobsStore[name];
        if (!storedJob) {
            throw this.noJobError(name);
        }
        storedJob.job.stop();
        delete CronJobService.jobsStore[name];
    }
}
