import { Injectable } from '@nestjs/common';
import { CONFIG } from '../../../config';
import * as _ from 'lodash';

@Injectable()
export class ConfigService {

    private config: { [key: string]: any };

    constructor() {
        this.config = CONFIG;
    }

    get(def: string | string[]) {
        return _.get(this.config, def);
    }
}
