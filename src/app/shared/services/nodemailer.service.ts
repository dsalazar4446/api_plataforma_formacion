import { Injectable } from '@nestjs/common';
import * as Nodemailer from 'nodemailer';
import { nodemailer } from '../../../config/nodemailer.config';
import * as Email from 'email-templates';
const email = new Email();

@Injectable()
export class NodemailerService {

    async sendMail(from: string, to: string[], subject: string, text: string, html: string) {
        let transporter = Nodemailer.createTransport(nodemailer);

        let info = await transporter.sendMail({
            from, // sender address
            to, // list of receivers
            subject, // Subject line
            text, // plain text body 
            html
        });
        console.log("Message sent: %s", info.messageId);
        console.log("Preview URL: %s", Nodemailer.getTestMessageUrl(info));
    }

    async sendEmail(template: string, options: any) {

        const htmlMail = await email
            .render(`../src/app/shared/templates/${template}.pug`, options)
            .then(item => item)
            .catch(console.error);

        this.sendMail('noreplay@grupocreex.com', [`${options.email}`], options.subject, options.text, htmlMail).catch(console.error);
    }
}