import { Table, Model, DataType, Column, CreatedAt, UpdatedAt } from 'sequelize-typescript';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'api_messages',
})
export default class ApiMessageModel extends Model<ApiMessageModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM(
            'es',
            'en',
            'it',
            'pr',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(100),
        field: 'message_code',
        allowNull: false,
        validate: {
            notEmpty: true,
        },
    })
    messageCode: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        field: 'message_text',
        allowNull: false,
        validate: {
            notEmpty: true,
        },
    })
    messageText: string;

    // tslint:disable-next-line: variable-name
    @CreatedAt public created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt public updated_at: Date;
}
