import { Table, Model, DataType, Column, CreatedAt, UpdatedAt } from 'sequelize-typescript';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'tools',
})
export class Tools extends Model<Tools>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(75),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'tool_name',
    })
    toolName: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'tool_description',
    })
    toolDescription: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        defaultValue: 'default.jpeg',
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'tool_avatar',
    })
    toolAvatar: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'tool_relative_url',
    })
    toolRelativeUrl: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        defaultValue: 'es',
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
