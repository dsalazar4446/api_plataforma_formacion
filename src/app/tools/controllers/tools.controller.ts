import { Controller, UseFilters, UseInterceptors, Post, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param, Req, FileFieldsInterceptor } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiUseTags, ApiImplicitParam, ApiConsumes, ApiImplicitFile, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { ToolsService } from '../services/tools.service';
import { ToolsSaveJson } from '../insterfaces/tools.interface';
import * as file from './infoFile';
import { CONFIG } from '../../../config';
import { Tools } from '../models/tools.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller ToolsController
 * @Creado 26 de Marzo 2019
 */

@ApiUseTags('Module-Tools')
@Controller('tools')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class ToolsController {

    constructor(private readonly _toolsService: ToolsService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: Tools,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'toolAvatar', description: 'Tool Avatar File', required: true })
    async create(@Req() req, @Body() bodyTools: ToolsSaveJson) {

        if (req.headers.language) {
            bodyTools.languageType = req.headers.language;
        } else {
            bodyTools.languageType = 'es';
        }

        const createTools = await this._toolsService.saveTools(bodyTools);

        if (!createTools) {
            throw this.appUtilsService.httpCommonError('Tools no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM61',
                languageType: 'es',
            });
        }

        createTools.toolAvatar = `${CONFIG.storage.server}toolAvatar/${createTools.toolAvatar}`;

        return createTools;

    }


    @ApiResponse({
        status: 200,
        type: Tools,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAll() {
        const fetchall = await this._toolsService.showAllTools();
        if (!fetchall) throw this.appUtilsService.httpCommonError('Tools no existe!', HttpStatus.NOT_FOUND, {
            messageCode: 'EM62',
            languageType: 'es',
        });

        fetchall.forEach(fetch => {

            fetch.toolAvatar = `${CONFIG.storage.server}toolAvatar/${fetch.toolAvatar}`;
        });

        return fetchall;
    }


    @ApiResponse({
        status: 200,
        type: Tools,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idTools: string) {

        const fetchTools = await this._toolsService.getDetails(idTools);

        if (!fetchTools) {
            throw this.appUtilsService.httpCommonError('Tools no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM62',
                languageType: 'es',
            });
        }

        fetchTools.toolAvatar = `${CONFIG.storage.server}toolAvatar/${fetchTools.toolAvatar}`;

        return fetchTools;
    }


    @ApiResponse({
        status: 200,
        type: Tools,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllTools(@Req() req) {

        const fetch = await this._toolsService.showAll(req.headers.language);
        if (!fetch) {
            throw new NotFoundException('Language does not exist!');
        }

        fetch.forEach(fetch => {
            fetch.toolAvatar = `${CONFIG.storage.server}toolAvatar/${fetch.toolAvatar}`;
        });

        return fetch;
    }



    @ApiResponse({
        status: 200,
        type: Tools,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateTools(@Body() bodyTools: Partial<ToolsSaveJson>, @Req() req, @Param('id') id: string) {

        if (req.headers.language) bodyTools.languageType = req.headers.language;

        const updateTools = await this._toolsService.updateTools(id, bodyTools);

        if (!updateTools[1][0]) {
            throw this.appUtilsService.httpCommonError('Tools no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM62',
                languageType: 'es',
            });
        }

        updateTools[1][0].toolAvatar = `${CONFIG.storage.server}toolAvatar/${updateTools[1][0].toolAvatar}`;

        return updateTools;
    }


    @ApiResponse({
        status: 200,
        type: Tools,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteTools(@Param('id') idTools: string) {

        const deleteTools = await this._toolsService.destroyTools(idTools);

        if (!deleteTools) {
            throw this.appUtilsService.httpCommonError('Tools no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM62',
                languageType: 'es',
            });
        }

        return deleteTools;
    }
}
