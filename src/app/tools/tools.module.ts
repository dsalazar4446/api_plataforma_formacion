import { Module, HttpModule } from "@nestjs/common";
import { DatabaseModule } from "../database/database.module";
import { SharedModule } from "../shared/shared.module";
import { ToolsController } from './controllers/tools.controller';
import { Tools } from './models/tools.model';
import { ToolsService } from './services/tools.service';


const controllers = [ToolsController];
const models = [Tools];
const providers = [ToolsService];

@Module({
    imports: [
        SharedModule,
        HttpModule,
        DatabaseModule.forRoot({
            sequelize: {
                models,
            },
            loki: true,
        }),
    ],
    providers,
    controllers,
})
export class ToolsModule { }
