import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional } from 'class-validator';
export class ToolsSaveJson {

    @ApiModelProperty()
    @IsString({message: 'toolName property must a be uuid'})
    @IsNotEmpty({message: 'toolName property not must null'})
    toolName: string;

    @ApiModelProperty()
    @IsString({message: 'toolDescription property must a be string'})
    @IsNotEmpty({message: 'toolDescription property not must null'})
    toolDescription: string;

    @ApiModelProperty()
    @IsString({message: 'toolAvatar property must a be string'})
    @IsNotEmpty({message: 'toolAvatar property not must null'})
    toolAvatar: string;

    @ApiModelProperty()
    @IsString({message: 'toolRelativeUrl property must a be string'})
    @IsNotEmpty({message: 'toolRelativeUrl property not must null'})
    toolRelativeUrl: string;

    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsString({message: 'languageType property must a be string'})
    @IsOptional()
    languageType: string;
}
export class ToolsUpdateJson extends ToolsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
