import { Injectable, Inject } from '@nestjs/common';
import { Tools } from '../models/tools.model';
import { ToolsSaveJson, ToolsUpdateJson } from '../insterfaces/tools.interface';
import * as path from 'path';
import * as fs from 'fs';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller ToolsService
 * @Creado 09 de Mayo 2019
 */

@Injectable()
export class ToolsService {

    private language = ['es', 'en', 'it', 'pr']

    constructor(
        @Inject('Tools') private readonly _tools: typeof Tools
    ) { }

    async saveTools(bodyTools: ToolsSaveJson): Promise<ToolsUpdateJson> {
        return await new this._tools(bodyTools).save();
    }

    async showAllTools(): Promise<ToolsUpdateJson[]> {
        return await this._tools.findAll();
    }

    async showAll(languageType: string): Promise<ToolsUpdateJson[]> {
        if (this.language.indexOf(languageType) >= 0) {

            return await this._tools.findAll({
                where: {
                    languageType
                }
            });
        }

        return;
    }

    async getDetails(ToolsId: string) {
        return await this._tools.findByPk(ToolsId);
    }

    async updateTools(id: string, bodyTools: Partial<ToolsSaveJson>): Promise<[number, Array<ToolsUpdateJson>]> {

        if (bodyTools.toolAvatar) {
            await this.deleteImg(id);
        }
        return await this._tools.update(bodyTools, {
            where: { id },
            returning: true
        });
    }

    async destroyTools(ToolsId: string): Promise<number> {
        await this.deleteImg(ToolsId);

        return await this._tools.destroy({
            where: { id: ToolsId },
        });
    }

    private async deleteImg(id: string) {
        const img = await this._tools.findByPk(id).then(item => item.toolAvatar).catch(err => err);

        const pathImagen = path.resolve(__dirname, `../../../../uploads/toolAvatar/${img}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }

}
