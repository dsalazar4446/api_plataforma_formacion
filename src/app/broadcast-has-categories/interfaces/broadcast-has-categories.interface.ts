import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class BroadcastHasCategoriesSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    broadcastId: string;
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    categoriesId: string;
}
// tslint:disable-next-line: max-classes-per-file
export class BroadcastHasCategoriesUpdateJson  extends BroadcastHasCategoriesSaveJson{
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}
