import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, Req} from '@nestjs/common';
import { BroadcastHasCategoriesService } from '../services/broadcast-has-categories.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { BroadcastHasCategoriesSaveJson, BroadcastHasCategoriesUpdateJson } from '../interfaces/broadcast-has-categories.interface';
import { ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { JwtAuthGuard } from '../../auths/guards/jwt-auth.guard';
import { BroadcastHasCategoriesModel } from '../models/broadcast-has-categories.model';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
import { Categories } from '../../categories/models/categories.model';
import { AppUtilsService } from 'src/app/shared/services/app-utils.service';

@Controller('broadcast-has-categories')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// @UseGuards(new JwtAuthGuard())
export class BroadcastHasCategoriesController {
    constructor(
        private readonly broadcastHasCategoriesService: BroadcastHasCategoriesService,
        @Inject('BroadcastHasCategoriesModel') private readonly broadcastHasCategoriesModel: typeof BroadcastHasCategoriesModel,
        @Inject('BroadcastModel') private readonly broadcastModel: typeof BroadcastModel,
        @Inject('Categories') private readonly categories: typeof Categories,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        @ApiResponse({ 
            status: 200,
            type: BroadcastHasCategoriesModel,
          })
        @Post()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async create(@Body() broadcastHasCategories: BroadcastHasCategoriesSaveJson, @Req() req) {
            const data = await this.broadcastModel.findByPk(broadcastHasCategories.broadcastId);
            if(data == null){
                  throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                      messageCode: 'EA23',
                      languageType:  req.headers.language,
                  });
            }
            const data2 = await this.categories.findByPk(broadcastHasCategories.categoriesId);
            if(data2 == null){
                  throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                      messageCode: 'EA24',
                      languageType:  req.headers.language,
                  });
            }
            return await this.broadcastHasCategoriesService.create(broadcastHasCategories);
        }
        @Get()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: BroadcastHasCategoriesModel,
            isArray: true
          })
        async getAll() {
            return await this.broadcastHasCategoriesService.findAll();
        }
        @Get(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: BroadcastHasCategoriesModel,
          })
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        async findById(@Param('id') idBroadcastHasCategories) {
            return await this.broadcastHasCategoriesService.findById(idBroadcastHasCategories);
        }
        @Put(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: BroadcastHasCategoriesModel,
          })
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        async update(
            @Body()
            updateBroadcastHasCategories: Partial<BroadcastHasCategoriesUpdateJson>, 
            @Param('id') idBroadcastHasCategories,
            @Req() req
        ) {
            if(updateBroadcastHasCategories.broadcastId){
                const data = await this.broadcastModel.findByPk(updateBroadcastHasCategories.broadcastId);
                if(data == null){
                    throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                        messageCode: 'EA23',
                        languageType:  req.headers.language,
                    });
                }
            }
            if(updateBroadcastHasCategories.categoriesId){
                const data2 = await this.categories.findByPk(updateBroadcastHasCategories.categoriesId);
                if(data2 == null){
                    throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                        messageCode: 'EA24',
                        languageType:  req.headers.language,
                    });
                }
            }
            const data3 = await this.broadcastHasCategoriesModel.findByPk(idBroadcastHasCategories);
            if(data3 == null){
                  throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                      messageCode: 'EA25',
                      languageType:  req.headers.language,
                  });
            }
            return await this.broadcastHasCategoriesService.update(idBroadcastHasCategories, updateBroadcastHasCategories);
        }
        @Delete(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        async delete(@Param('id') idBroadcastHasCategories) {
            return await this.broadcastHasCategoriesService.deleted(idBroadcastHasCategories);
        }
}
