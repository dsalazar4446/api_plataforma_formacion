import { Injectable, Inject } from '@nestjs/common';
import { BroadcastHasCategoriesSaveJson, BroadcastHasCategoriesUpdateJson } from '../interfaces/broadcast-has-categories.interface';
import { BroadcastHasCategoriesModel } from '../models/broadcast-has-categories.model';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
import { Categories } from '../../categories/models/categories.model';
@Injectable()
export class BroadcastHasCategoriesService {
    constructor(
        @Inject('BroadcastHasCategoriesModel') private readonly broadcastHasCategoriesModel: typeof BroadcastHasCategoriesModel
    ) { }
    async findAll(): Promise<BroadcastHasCategoriesModel[]> {
        return  await this.broadcastHasCategoriesModel.findAll({
            include:[BroadcastModel,Categories]
        });
    }
    async findById(id: string): Promise<BroadcastHasCategoriesModel> {
        return await this.broadcastHasCategoriesModel.findById<BroadcastHasCategoriesModel>(id,{
            include:[BroadcastModel,Categories]
        });
    }
    async create(broadcastHasCategories: BroadcastHasCategoriesSaveJson): Promise<BroadcastHasCategoriesModel> {
        return await this.broadcastHasCategoriesModel.create<BroadcastHasCategoriesModel>(broadcastHasCategories, {
            returning: true,
        });
    }
    async update(idBroadcastHasCategories: string, broadcastHasCategoriesUpdate: Partial<BroadcastHasCategoriesUpdateJson>){
        return  await this.broadcastHasCategoriesModel.update(broadcastHasCategoriesUpdate, {
            where: {
                id: idBroadcastHasCategories,
            },
            returning: true,
        });
    }
    async deleted(idBroadcastHasCategories: string): Promise<any> {
        return await this.broadcastHasCategoriesModel.destroy({
            where: {
                id: idBroadcastHasCategories,
            },
        });
    }
}
