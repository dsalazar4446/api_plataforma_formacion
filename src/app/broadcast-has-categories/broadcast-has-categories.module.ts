
import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { BroadcastHasCategoriesController } from './controllers/broadcast-has-categories.controller';
import { BroadcastHasCategoriesService } from './services/broadcast-has-categories.service';
import { BroadcastHasCategoriesModel } from './models/broadcast-has-categories.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { Categories } from '../categories/models/categories.model';
import { BroadcastModel } from '../broadcast/models/broadcast.model';

const models = [
  BroadcastHasCategoriesModel,
  Categories,
  BroadcastModel,
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [BroadcastHasCategoriesController],
  providers: [BroadcastHasCategoriesService]
})
export class BroadcastHasCategoriesModule {}
