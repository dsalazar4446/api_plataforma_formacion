import { Column, DataType, Table, Model, ForeignKey, BelongsTo} from 'sequelize-typescript';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
import { Categories } from '../../categories/models/categories.model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    timestamps: false,
    tableName: 'broadcasts_has_categories',
})
export class BroadcastHasCategoriesModel extends Model<BroadcastHasCategoriesModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => BroadcastModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'broadcasts_id', 
    })
    broadcastId: string;
    @ApiModelProperty()
    @ForeignKey(() =>Categories)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'categories_id',
    })
    categoriesId: string;
    
    @BelongsTo(() => BroadcastModel)
    broadcast: BroadcastModel;


    @BelongsTo(() => Categories)
    categories: Categories;
}
