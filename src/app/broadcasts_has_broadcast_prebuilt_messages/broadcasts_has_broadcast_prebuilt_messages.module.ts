
import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { BroadcastHasBroadcastPrebuiltMessagesController } from './controllers/broadcasts_has_broadcast_prebuilt_messages.controller';
import { BroadcastHasBroadcastPrebuiltMessagesService } from './services/broadcasts_has_broadcast_prebuilt_messages.service';
import {BroadcastHasBroadcastPrebuiltMessagesModel } from './models/broadcasts_has_broadcast_prebuilt_messages.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { BroadcastPrebuiltMessageModel } from '../broadcast-prebuilt-message/models/broadcast-prebuilt-message.model';
import { BroadcastModel } from '../broadcast/models/broadcast.model';
const models = [
  BroadcastHasBroadcastPrebuiltMessagesModel,
  BroadcastPrebuiltMessageModel,
  BroadcastModel
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [BroadcastHasBroadcastPrebuiltMessagesController],
  providers: [BroadcastHasBroadcastPrebuiltMessagesService]
})
export class BroadcastHasBroadcastPrebuiltMessagesModule {}
