import { Column, DataType, Table, Model,ForeignKey, BelongsTo } from 'sequelize-typescript';
import { BroadcastPrebuiltMessageModel } from '../../broadcast-prebuilt-message/models/broadcast-prebuilt-message.model';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
import { ApiModelProperty } from '@nestjs/swagger';

// FALTA RELACIONAR CON CATEGORY
@Table({
    tableName: 'broadcasts_has_broadcast_prebuilt_messages',
})
export class BroadcastHasBroadcastPrebuiltMessagesModel extends Model<BroadcastHasBroadcastPrebuiltMessagesModel> {
    @ApiModelProperty()
    @ForeignKey(() => BroadcastPrebuiltMessageModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'broadcast_prebuilt_messages_id',
    })
    broadcastPrebuiltMessageId: string;
    @ApiModelProperty()
    @ForeignKey(() => BroadcastModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'broadcast_id',
    })
    broadcastId: string;
    
    @BelongsTo(() => BroadcastPrebuiltMessageModel)
    broadcastPrebuiltMessageModel: BroadcastPrebuiltMessageModel;
    
    @BelongsTo(() =>BroadcastModel)
    broadcast: BroadcastModel;
}
