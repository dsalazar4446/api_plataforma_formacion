import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, Req} from '@nestjs/common';
import { BroadcastHasBroadcastPrebuiltMessagesService } from '../services/broadcasts_has_broadcast_prebuilt_messages.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { BroadcastHasBroadcastPrebuiltMessagesSaveJson } from '../interfaces/broadcasts_has_broadcast_prebuilt_messages.interface';
import { JwtAuthGuard } from '../../auths/guards/jwt-auth.guard';
import { BroadcastHasBroadcastPrebuiltMessagesModel } from '../models/broadcasts_has_broadcast_prebuilt_messages.model';
import { ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { BroadcastPrebuiltMessageModel } from '../../broadcast-prebuilt-message/models/broadcast-prebuilt-message.model';

@Controller('broadcast-has-broadcast-prebuilt-messages')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// @UseGuards(new JwtAuthGuard())
export class BroadcastHasBroadcastPrebuiltMessagesController {
    constructor(
        private readonly broadcastHasBroadcastPrebuiltMessageService: BroadcastHasBroadcastPrebuiltMessagesService,
        @Inject('BroadcastPrebuiltMessageModel') private readonly broadcastPrebuiltMessageModel: typeof BroadcastPrebuiltMessageModel,
        @Inject('BroadcastModel') private readonly broadcastModel: typeof BroadcastModel,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        @Post()
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        @ApiResponse({
            status: 200,
            type: BroadcastHasBroadcastPrebuiltMessagesModel,
          })
        async create(@Body() broadcastHasBroadcastPrebuiltMessage: BroadcastHasBroadcastPrebuiltMessagesSaveJson, @Req() req) {
          const data2= await this.broadcastPrebuiltMessageModel.findByPk(broadcastHasBroadcastPrebuiltMessage.broadcastPrebuiltMessageId);
          if(data2 == null){
                throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA30',
                    languageType:  req.headers.language,
                });
          }
          const data3= await this.broadcastModel.findByPk(broadcastHasBroadcastPrebuiltMessage.broadcastId);
          if(data3 == null){
                throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA22',
                    languageType:  req.headers.language,
                });
          }
          return await this.broadcastHasBroadcastPrebuiltMessageService.create(broadcastHasBroadcastPrebuiltMessage);
        }
        @Get()
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        @ApiResponse({
            status: 200,
            type: BroadcastHasBroadcastPrebuiltMessagesModel,
            isArray: true
          })
        async getAll() {
            return await this.broadcastHasBroadcastPrebuiltMessageService.findAll();
        }
}
