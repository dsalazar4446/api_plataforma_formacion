import { Injectable, Inject } from '@nestjs/common';
import { BroadcastHasBroadcastPrebuiltMessagesSaveJson } from '../interfaces/broadcasts_has_broadcast_prebuilt_messages.interface';
import { BroadcastHasBroadcastPrebuiltMessagesModel } from '../models/broadcasts_has_broadcast_prebuilt_messages.model';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
import { BroadcastPrebuiltMessageModel } from '../../broadcast-prebuilt-message/models/broadcast-prebuilt-message.model';
@Injectable()
export class BroadcastHasBroadcastPrebuiltMessagesService {
    constructor(
// tslint:disable-next-line: max-line-length
// tslint:disable-next-line: no-shadowed-variable
// tslint:disable-next-line: max-line-length
        @Inject('BroadcastHasBroadcastPrebuiltMessagesModel') private readonly broadcastHasBroadcastPrebuiltMessageModel: typeof BroadcastHasBroadcastPrebuiltMessagesModel,
        @Inject('BroadcastModel') private readonly broadcastModel: typeof BroadcastModel,
        @Inject('BroadcastPrebuiltMessageModel') private readonly broadcastPrebuiltMessageModel: typeof BroadcastPrebuiltMessageModel,
    ) { }
    async findAll(): Promise<any[]> {
        const data =  await this.broadcastHasBroadcastPrebuiltMessageModel.findAll();
        const result: any[] = [];
        await Promise.all(
            await data.map(
                async (item)=>{
                    const dataBPM = await this.broadcastPrebuiltMessageModel.findByPk(item.dataValues.broadcastPrebuiltMessageId);
                    const dataB = await this.broadcastModel.findByPk(item.dataValues.broadcastId);
                    item.dataValues.broadcast = dataB.dataValues;
                    item.dataValues.broadcastPrebuiltMessageModel = dataBPM;
                    result.push(item.dataValues);
                }
            )
        )
        return result;
    }
// tslint:disable-next-line: max-line-length
    async create(BroadcastHasBroadcastPrebuiltMessage: BroadcastHasBroadcastPrebuiltMessagesSaveJson): Promise<BroadcastHasBroadcastPrebuiltMessagesModel> {
// tslint:disable-next-line: max-line-length
        return await this.broadcastHasBroadcastPrebuiltMessageModel.create<BroadcastHasBroadcastPrebuiltMessagesModel>(BroadcastHasBroadcastPrebuiltMessage, {
            returning: true,
        });
    }

}
