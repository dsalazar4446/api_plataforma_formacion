import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { NotificationsMsgController } from './controllers/notifications-msg.controller';
import { NotificationMsgService } from './services/notifications-msg.service';
import { NotificationMsgModel } from './models/notifications-msg.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { NotificationModel } from '../notifications/models/notifications.model';
const models = [
  NotificationMsgModel,
  NotificationModel
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [NotificationsMsgController],
  providers: [NotificationMsgService]
})
export class NotificationsMsgModule {}
