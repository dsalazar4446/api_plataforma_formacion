import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, Req} from '@nestjs/common';
import { NotificationMsgService } from '../services/notifications-msg.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { NotificationMsgSaveJson, NotificationMsgUpdateJson } from '../interfaces/notification-msg.interface';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiImplicitQuery, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { NotificationMsgModel } from '../models/notifications-msg.model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { NotificationModel } from '../../notifications/models/notifications.model';

@Controller('notifications-msg')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class NotificationsMsgController {
    constructor(
        private readonly notificationMsgService: NotificationMsgService,
        @Inject('NotificationMsgModel') private readonly notificationMsgModel: typeof NotificationMsgModel,
        @Inject('NotificationModel') private readonly notificationModel: typeof NotificationModel,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        // @ApiImplicitQuery({ name: 'notificationTarget', enum: ['app','web'] })
        @ApiImplicitQuery({ name: 'languageType', enum: ['es','en','it','pr'] })
        @Post()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: NotificationMsgModel,
          })
        async create(@Body() notificationMsg: NotificationMsgSaveJson, @Req() req) {
            const data3 = await this.notificationModel.findByPk(notificationMsg.notificationId);
            if(data3 == null){
                throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA14',
                    languageType:  req.headers.language,
                });
            }
            return await this.notificationMsgService.create(notificationMsg);
        }
        @Get()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: NotificationMsgModel,
            isArray: true
          })
        async getAll() {
            return await this.notificationMsgService.findAll();
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Get(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: NotificationMsgModel,
          })
        async findById(@Param('id') idNotigicationMsg) {
            return await this.notificationMsgService.findById(idNotigicationMsg);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        // @ApiImplicitQuery({ name: 'notificationTarget', enum: ['app','web'] })
        @ApiImplicitQuery({ name: 'languageType', enum: ['es','en','it','pr'] })
        @Put(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: NotificationMsgModel,
          })
        async update(
            @Body()
            updateNotificationMsg: Partial<NotificationMsgUpdateJson>,
            @Param('id') idNotificationMsg,
            @Req() req
        ) {
            const data3 = await this.notificationModel.findByPk(updateNotificationMsg.notificationId);
            if(data3 == null){
                throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA14',
                    languageType:  req.headers.language,
                });
            }
            const data2 = await this.notificationMsgModel.findByPk(idNotificationMsg);
            if(data2 == null){
                throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA15',
                    languageType:  req.headers.language,
                });
            }
            return await this.notificationMsgService.update(idNotificationMsg, updateNotificationMsg);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Delete(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async delete(@Param('id') idNotificationMsg) {
            return await this.notificationMsgService.deleted(idNotificationMsg);
        }
}
