import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class NotificationMsgSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    notificationId: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    notificationMsgHeader: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    notificationMsgBody: string;
    // @ApiModelProperty()
    // @IsNotEmpty()
    // @IsEnum({enum:['app','web']})
    // notificationTarget: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsEnum({enum:['es','en','it','pr']})
    languageType: string;
}

// tslint:disable-next-line: max-classes-per-file
export class NotificationMsgUpdateJson extends NotificationMsgSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}