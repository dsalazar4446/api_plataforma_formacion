import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey } from 'sequelize-typescript';
import { NotificationModel } from '../../notifications/models/notifications.model';
import { ApiModelProperty } from '@nestjs/swagger';

@Table({
    tableName: 'notification_msgs',
})
export class NotificationMsgModel extends Model<NotificationMsgModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => NotificationModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'notification_id',
    })
    notificationId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'notification_msg_header',
    })
    notificationMsgHeader: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'notification_msg_body',
    })
    notificationMsgBody: string;
    // @ApiModelProperty()
    // @Column({
    //     type: DataType.ENUM(
    //         'app',
    //         'web',
    //     ),
    //     allowNull: false,
    //     validate: {
    //         notEmpty: true,
    //     },
    //     field: 'notification_target',
    // })
    // notificationTarget: string;

    @ApiModelProperty()
    @Column({
        type: DataType.ENUM(
            'es',
            'en',
            'it',
            'pr',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
        get() {
            return parseInt(this.getDataValue('languageType'), 10);
        },
        set(value: number) {
            this.setDataValue('languageType', value.toString());
        },
    })
    languageType: string;

    

    
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
