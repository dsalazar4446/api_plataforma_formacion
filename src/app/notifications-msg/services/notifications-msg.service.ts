import { Injectable, Inject } from '@nestjs/common';
import { NotificationMsgSaveJson, NotificationMsgUpdateJson } from '../interfaces/notification-msg.interface';
import { NotificationMsgModel } from '../models/notifications-msg.model';

@Injectable()
export class NotificationMsgService {
    constructor(
        @Inject('NotificationMsgModel') private readonly notificationMsgModel: typeof NotificationMsgModel,
    ) { }
    async findAll(): Promise<NotificationMsgModel[]> {
        return  await this.notificationMsgModel.findAll();
    }
    async findById(id: string): Promise<NotificationMsgModel> {
        return await this.notificationMsgModel.findById<NotificationMsgModel>(id);
    }

    async create(notificationMsg: NotificationMsgSaveJson): Promise<NotificationMsgModel> {
        return await this.notificationMsgModel.create<NotificationMsgModel>(notificationMsg, {
            returning: true,
        });
    }
    async update(idNotificationMsg: string, notificationUpdate: Partial<NotificationMsgUpdateJson>){
        return  await this.notificationMsgModel.update(notificationUpdate, {
            where: {
                id: idNotificationMsg,
            },
            returning: true,
        });
    }
    async deleted(idNotificationMsg: string): Promise<any> {
        return await this.notificationMsgModel.destroy({
            where: {
                id: idNotificationMsg,
            },
        });
    }
}
