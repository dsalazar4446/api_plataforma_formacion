import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class DocumentHasCountriesSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'documentTypeId property must a be uuid'})
    @IsNotEmpty({message: 'documentTypeId property not must null'})
    documentTypeId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'country_id property must a be uuid'})
    @IsNotEmpty({message: 'country_id property not must null'})
    country_id: string;
}
