import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty, IsUUID } from "class-validator";

export class DocumentSaveJson {

    @ApiModelProperty()
    @IsString({message: "The documentType variable must be a string"})
    @IsNotEmpty({message: "The documentType variable can not be null"})
    documentType: string;
}

export class DocumentUpdateJson extends DocumentSaveJson {

    @ApiModelProperty()
    @IsUUID('4', {message: "The id variable must be a uuid"})
    @IsNotEmpty({message: "The id variable can not be null"})
    id: string;
}
