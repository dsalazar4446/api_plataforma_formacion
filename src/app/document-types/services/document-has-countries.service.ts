import { Injectable, Inject } from "@nestjs/common";
import { DocumentTypesHasCountries } from "../models/document-has-countries.model";
import { DocumentHasCountriesSaveJson } from "../interfaces/document-has-countries.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services DocumentHasCountriesServices
 * @Creado 20 de abril 2019
 */

@Injectable()
export class DocumentHasCountriesServices {

    constructor(
        @Inject('DocumentTypesHasCountries') private readonly _documentTypesHasCountries: typeof DocumentTypesHasCountries
    ) { }

    async save(body: DocumentHasCountriesSaveJson): Promise<DocumentHasCountriesSaveJson> {

        return await new this._documentTypesHasCountries(body).save();
    }

    async getAll(): Promise<DocumentHasCountriesSaveJson[]> {

        return await this._documentTypesHasCountries.findAll();
    }

    async destroy(id: string): Promise<number> {

        return await this._documentTypesHasCountries.destroy({
            where: { id },
        });
    }

}