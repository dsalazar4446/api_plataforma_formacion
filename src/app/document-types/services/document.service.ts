import { Injectable, Inject } from "@nestjs/common";
import { DocumentTypes } from "../models/document.model";
import { DocumentSaveJson, DocumentUpdateJson } from "../interfaces/document.interface";
import { Countries } from "../../city/models/countries.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services DocumentService
 * @Creado 20 de Abril 2019
 */

@Injectable()
export class DocumentService {
    constructor(
        @Inject('DocumentTypes') private readonly _documentTypes: typeof DocumentTypes,
    ) { }

    async saveDocument(bodyDocument: DocumentSaveJson): Promise<DocumentUpdateJson> {

        return await new this._documentTypes(bodyDocument).save();
    }

    async showAllDocument(): Promise<DocumentSaveJson[]> {

        return await this._documentTypes.findAll();
    }

    async getDocument(idDocument: string): Promise<DocumentUpdateJson> {

        return await this._documentTypes.findByPk(idDocument,{
            include: [
                {
                    model: Countries
                }
            ]
        });
    }

    async updateDocument(id: string, bodyDocument: Partial<DocumentSaveJson>): Promise<[number, Array<DocumentUpdateJson>]> {

        return await this._documentTypes.update(bodyDocument, {
            where: { id },
            returning: true
        });
    }

    async destroyDocument(id: string): Promise<number> {
        return await this._documentTypes.destroy({
            where: { id },
        });
    }

}