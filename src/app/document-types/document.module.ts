import { SharedModule } from "../shared/shared.module";
import { HttpModule, Module, Provider } from "@nestjs/common";
import { DatabaseModule } from "../database/database.module";
import { DocumentHasCountriesController } from './controllers/document-has-countries.controller';
import { DocumentController } from './controllers/document.controller';
import { DocumentHasCountriesServices } from './services/document-has-countries.service';
import { DocumentService } from './services/document.service';
import { DocumentTypesHasCountries } from './models/document-has-countries.model';
import { DocumentTypes } from './models/document.model';

const models = [DocumentTypesHasCountries,DocumentTypes];
const controllers = [DocumentHasCountriesController,DocumentController];
const providers: Provider[] = [DocumentHasCountriesServices,DocumentService];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  controllers,
  providers
})
export class DocumentModule { }
