import { Controller, UseFilters, UseInterceptors, Post, Body, HttpStatus, Get, Param, NotFoundException, Put, Delete } from "@nestjs/common";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from "@nestjs/swagger";
import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { DocumentHasCountriesServices } from "../services/document-has-countries.service";
import { DocumentHasCountriesSaveJson } from "../interfaces/document-has-countries.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller DocumentHasCountriesController
 * @Creado 29 de Marzo 2019
 */
@ApiUseTags('Module-Document-Types')
@Controller('document-has-countries')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class DocumentHasCountriesController {

    constructor(private readonly _documentHasCountriesServices: DocumentHasCountriesServices,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyDocumentHasCountries: DocumentHasCountriesSaveJson) {

        const createDocumentHasCountries = await this._documentHasCountriesServices.save(bodyDocumentHasCountries);

        if (!createDocumentHasCountries) {
            throw this.appUtilsService.httpCommonError('DocumentHasCountries no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM31',
                languageType: 'es',
            });
        }

        return createDocumentHasCountries;

    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async getAll() {
        const data = await this._documentHasCountriesServices.getAll();

        if (!data) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return data;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteDocumentHasCountries(@Param('id') idDocumentHasCountries: string) {

        const deleteDocumentHasCountries = await this._documentHasCountriesServices.destroy(idDocumentHasCountries);

        if (!deleteDocumentHasCountries) {
            throw this.appUtilsService.httpCommonError('DocumentHasCountries no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM32',
                languageType: 'es',
            });
        }

        return deleteDocumentHasCountries;
    }

}