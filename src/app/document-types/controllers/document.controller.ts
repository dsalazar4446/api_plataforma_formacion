import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { Controller, UseFilters, UseInterceptors, Post, Body, HttpStatus, Get, Param, NotFoundException, Put, Delete } from "@nestjs/common";
import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { DocumentService } from "../services/document.service";
import { DocumentSaveJson } from "../interfaces/document.interface";
import { DocumentTypes } from "../models/document.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller DocumentController
 * @Creado 29 de Marzo 2019
 */
@ApiUseTags('Module-Document-Types')
@Controller('document-types')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class DocumentController {

    constructor(private readonly _documentService: DocumentService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: DocumentTypes,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyDocument: DocumentSaveJson) {

        const createDocument = await this._documentService.saveDocument(bodyDocument);

        if (!createDocument) {
            throw this.appUtilsService.httpCommonError('Documento no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM33',
                languageType: 'es',
            });
        }

        return createDocument;

    }


    @ApiResponse({
        status: 200,
        type: DocumentTypes,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idDocument: string) {

        const fetchDocument = await this._documentService.getDocument(idDocument);

        if (!fetchDocument) {
            throw this.appUtilsService.httpCommonError('Documento no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM34',
                languageType: 'es',
            });
        }

        return fetchDocument;
    }


    @ApiResponse({
        status: 200,
        type: DocumentTypes,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async findByAll() {

        const fetchAll = await this._documentService.showAllDocument();

        if (!fetchAll) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetchAll;
    }


    @ApiResponse({
        status: 200,
        type: DocumentTypes,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateDocument(@Param('id') id: string, @Body() bodyDocument: Partial<DocumentSaveJson>) {

        const updateDocument = await this._documentService.updateDocument(id, bodyDocument);

        if (!updateDocument[1][0]) {
            throw this.appUtilsService.httpCommonError('Documento no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM34',
                languageType: 'es',
            });
        }

        return updateDocument;
    }


    @ApiResponse({
        status: 200,
        type: DocumentTypes,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteDocument(@Param('id') idDocument: string) {

        const deleteDocument = await this._documentService.destroyDocument(idDocument);

        if (!deleteDocument) {
            throw this.appUtilsService.httpCommonError('Documento no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM34',
                languageType: 'es',
            });
        }

        return deleteDocument;
    }

}

