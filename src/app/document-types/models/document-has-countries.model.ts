import { Table, Column, DataType, Model, ForeignKey } from "sequelize-typescript";
import { DocumentTypes } from "./document.model";
import { Countries } from "../../city/models/countries.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({
    tableName: 'document_types_has_countries',
})
export class DocumentTypesHasCountries extends Model<DocumentTypesHasCountries> {

    @ApiModelPropertyOptional()
    @ForeignKey(() => DocumentTypes)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'document_type_id',
    })
    documentTypeId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Countries)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'country_id',
    })
    country_id: string;

}