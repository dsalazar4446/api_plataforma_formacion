import { Table, Column, DataType, CreatedAt, UpdatedAt, Model, HasMany, BelongsToMany } from "sequelize-typescript";
import { Countries } from "../../city/models/countries.model";
import { DocumentTypesHasCountries } from "./document-has-countries.model";
import { UserModel } from "../../user/models/user.Model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({
    tableName: 'document_types',
})
export class DocumentTypes extends Model<DocumentTypes> {

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'document_type',
    })
    documentType: string;

    @ApiModelPropertyOptional()
    @BelongsToMany(() => Countries, {
        through: {
          model: () => DocumentTypesHasCountries,
          unique: true,
        }
      })
    countries: Countries[];
    
    @ApiModelPropertyOptional()
    @HasMany(() => UserModel)
    userModel: UserModel[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}