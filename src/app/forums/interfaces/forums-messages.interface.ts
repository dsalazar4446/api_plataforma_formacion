import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsUUID } from 'class-validator';
export class ForumsMessagesSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    senderUserId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'forumId property must a be uuid'})
    @IsNotEmpty({message: 'forumId property not must null'})
    forumId: string;

    @ApiModelProperty()
    @IsString({message: 'forumsMessage property must a be string'})
    @IsNotEmpty({message: 'forumsMessage property not must null'})
    forumsMessage: string;

    @ApiModelProperty()
    @IsString({message: 'forumsMessageStatus property must a be string'})
    @IsNotEmpty({message: 'forumsMessageStatus property not must null'})
    forumsMessageStatus: string;
}
export class ForumsMessagesUpdateJson extends ForumsMessagesSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
1