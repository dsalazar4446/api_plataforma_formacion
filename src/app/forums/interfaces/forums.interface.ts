import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional } from 'class-validator';
export class ForumsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    moderatorUserId: string;

    @ApiModelProperty()
    @IsString({message: 'categoryId property must a be string'})
    @IsNotEmpty({message: 'categoryId property not must null'})
    categoryId: string;

    @ApiModelProperty()
    @IsString({message: 'forumTitle property must a be string'})
    @IsNotEmpty({message: 'forumTitle property not must null'})
    forumTitle: string;

    @ApiModelProperty()
    @IsString({message: 'formationDescription property must a be string'})
    @IsNotEmpty({message: 'formationDescription property not must null'})
    formationDescription: string;
    
    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsString({message: 'languageType property must a be string'})
    @IsOptional()
    languageType: string;
}
export class ForumsUpdateJson extends ForumsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
