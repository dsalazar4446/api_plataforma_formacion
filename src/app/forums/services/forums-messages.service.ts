import { Injectable, Inject } from "@nestjs/common";
import { ForumsMessages } from "../models/forums_messages.model";
import { ForumsMessagesSaveJson, ForumsMessagesUpdateJson } from "../interfaces/forums-messages.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services ForumsMessagesServices
 * @Creado 29 de Marzo 2019
 */

@Injectable()
export class ForumsMessagesServices {

    constructor(
        @Inject('ForumsMessages') private readonly _forumsMessagesServcs: typeof ForumsMessages
    ) { }

    async saveForumsM(bodyForumsM: ForumsMessagesSaveJson): Promise<ForumsMessagesUpdateJson> {

        return await new this._forumsMessagesServcs(bodyForumsM).save();
    }

    async showAllForumsM(): Promise<ForumsMessagesUpdateJson[]> {

        return await this._forumsMessagesServcs.findAll();

    }

    async getDetails(forumsMId: string){
        return await this._forumsMessagesServcs.findByPk(forumsMId);
    }

    async updateForumsM(id: string, bodyForumsM: Partial<ForumsMessagesUpdateJson>): Promise<[number, Array<any>]> {

        return await this._forumsMessagesServcs.update(bodyForumsM, {
            where: { id },
            returning: true
        });

    }

    async destroyForumsM(forumsMId: string): Promise<number> {

        return await this._forumsMessagesServcs.destroy({
            where: { id: forumsMId },
        });
    }
}