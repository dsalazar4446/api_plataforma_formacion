import { Injectable, Inject } from "@nestjs/common";
import { Forums} from "../models/forums.model";
import { ForumsMessages } from "../models/forums_messages.model";
import { ForumsSaveJson, ForumsUpdateJson } from "../interfaces/forums.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services ForumsServices
 * @Creado 29 de Marzo 2019
 */

@Injectable()
export class ForumsServices {

    private language = ['es', 'en', 'it', 'pr'];

    constructor(
        @Inject('Forums') private readonly _forumsServcs: typeof Forums
    ) { }

    async saveForums(bodyForums: ForumsSaveJson): Promise<ForumsUpdateJson> {

        return await new this._forumsServcs(bodyForums).save();
    }

    async showAllForums(languageType: string): Promise<ForumsUpdateJson[]> {

        if (this.language.indexOf(languageType) >= 0) {

            return await this._forumsServcs.findAll({
                where: {
                    languageType
                }
            });
        }

        return;

    }

    async getDetails(forumsId: string) {

        if (forumsId === undefined) {

            return await this.find();
        }

        return await this._forumsServcs.findByPk(forumsId, {
            include: [
                {
                    model: ForumsMessages
                }
            ]
        });
    }

    async updateForums(id: string, forumsId: Partial<ForumsUpdateJson>): Promise<[number, Array<any>]> {

        return await this._forumsServcs.update(forumsId, {
            where: { id },
            returning: true
        });

    }

    async destroyForums(forumsId: string): Promise<number> {

        return await this._forumsServcs.destroy({
            where: { id: forumsId },
        });
    }

    async find() {

        return await this._forumsServcs.findAll({
            include: [
                {
                    model: ForumsMessages
                }
            ]
        });

    }
}