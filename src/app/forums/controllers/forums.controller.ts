import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, NotFoundException, Put, Delete, Param, Req } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ForumsServices } from "../services/forums.service";
import { ForumsSaveJson, ForumsUpdateJson } from "../interfaces/forums.interface";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { Forums } from "../models/forums.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller ForumsController
 * @Creado 29 de Marzo 2019
 */

@ApiUseTags('Module-Forums')
@Controller('forums')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class ForumsController {

    constructor(private readonly _forumsServices: ForumsServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: Forums,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Req() req, @Body() bodyForums: ForumsSaveJson) {

        if (req.headers.language) {
            bodyForums.languageType = req.headers.language;
        } else {
            bodyForums.languageType = 'es';
        }

        const createForums = await this._forumsServices.saveForums(bodyForums)

        if (!createForums) {
            throw this.appUtilsService.httpCommonError('Forums no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM47',
                languageType: 'es',
            });
        }

        return createForums;

    }


    @ApiResponse({
        status: 200,
        type: Forums,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAll() {
        return await this._forumsServices.find();
    }


    @ApiResponse({
        status: 200,
        type: Forums,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idForums: string) {

        const fetchForums = await this._forumsServices.getDetails(idForums);

        if (!fetchForums) {
            throw this.appUtilsService.httpCommonError('Forums no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM48',
                languageType: 'es',
            });
        }

        return fetchForums;
    }


    @ApiResponse({
        status: 200,
        type: Forums,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showLanguage(@Req() req) {

        const fetch = await this._forumsServices.showAllForums(req.headers.language);

        if (!fetch) {
            throw this.appUtilsService.httpCommonError('Language does not exist!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM3',
                languageType: 'es',
            });
        }

        return fetch;
    }


    @ApiResponse({
        status: 200,
        type: Forums,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateForums(@Req() req, @Param('id') id: string, @Body() bodyForums: Partial<ForumsSaveJson>) {

        if (req.headers.language) bodyForums.languageType = req.headers.language;

        const updateForums = await this._forumsServices.updateForums(id, bodyForums);

        if (!updateForums[1][0]) {
            throw this.appUtilsService.httpCommonError('Forums no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM48',
                languageType: 'es',
            });
        }

        return updateForums;
    }


    @ApiResponse({
        status: 200,
        type: Forums,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteForums(@Param('id') idForums: string) {

        const deleteForums = await this._forumsServices.destroyForums(idForums);

        if (!deleteForums) {
            throw this.appUtilsService.httpCommonError('Forums no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM48',
                languageType: 'es',
            });
        }

        return deleteForums;
    }
}