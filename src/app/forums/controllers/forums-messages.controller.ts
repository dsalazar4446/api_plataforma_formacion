import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, NotFoundException, Put, Delete, Param } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ForumsMessagesServices } from "../services/forums-messages.service";
import { ForumsMessagesSaveJson, ForumsMessagesUpdateJson } from "../interfaces/forums-messages.interface";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { ForumsMessages } from "../models/forums_messages.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller ForumsMessagesController
 * @Creado 29 de Marzo 2019
 */

@ApiUseTags('Module-Forums')
@Controller('forums-messages')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class ForumsMessagesController {

    constructor(private readonly _newsServcs: ForumsMessagesServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: ForumsMessages,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyForumsM: ForumsMessagesSaveJson) {

        const createForumsM = await this._newsServcs.saveForumsM(bodyForumsM);

        if (!createForumsM) {
            throw this.appUtilsService.httpCommonError('Forums Messages no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM45',
                languageType: 'es',
            });
        }

        return createForumsM;

    }


    @ApiResponse({
        status: 200,
        type: ForumsMessages,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllNews() {
        return await this._newsServcs.showAllForumsM();
    }


    @ApiResponse({
        status: 200,
        type: ForumsMessages,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idForumsM: string) {

        const fetchForumsM = await this._newsServcs.getDetails(idForumsM);

        if (!fetchForumsM) {
            throw this.appUtilsService.httpCommonError('Forums Messages no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM46',
                languageType: 'es',
            });
        }

        return fetchForumsM;
    }


    @ApiResponse({
        status: 200,
        type: ForumsMessages,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateForumsM(@Param('id') id: string, @Body() bodyForumsM: Partial<ForumsMessagesSaveJson>) {

        const updateForumsM = await this._newsServcs.updateForumsM(id, bodyForumsM);

        if (!updateForumsM[1][0]) {
            throw this.appUtilsService.httpCommonError('Forums Messages no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM46',
                languageType: 'es',
            });
        }

        return updateForumsM;
    }


    @ApiResponse({
        status: 200,
        type: ForumsMessages,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteForumsM(@Param('id') idForumsM: string) {

        const deleteForumsM = await this._newsServcs.destroyForumsM(idForumsM);

        if (!deleteForumsM) {
            throw this.appUtilsService.httpCommonError('Forums Messages no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM46',
                languageType: 'es',
            });
        }

        return deleteForumsM;
    }
}