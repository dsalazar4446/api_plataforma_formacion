import { Module, HttpModule, Provider } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { Forums } from './models/forums.model';
import { ForumsMessages } from './models/forums_messages.model';

import { ForumsMessagesServices } from './services/forums-messages.service';
import { ForumsServices } from './services/forums.service';

import { ForumsController } from './controllers/forums.controller';
import { ForumsMessagesController } from './controllers/forums-messages.controller';

const models = [Forums, ForumsMessages];
const controllers = [ForumsController, ForumsMessagesController];
const providers: Provider[] = [ForumsMessagesServices, ForumsServices];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers
})
export class ForumsModule { }
