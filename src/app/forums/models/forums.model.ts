import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { UserModel } from './../../user/models/user.Model';
import { ForumsMessages } from './forums_messages.model';
import { Categories } from './../../categories/models/categories.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'forums',
})
export class Forums extends Model<Forums>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id'
    })
    moderatorUserId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Categories)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'category_id'
    })
    categoryId: string;


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'forum_title'
    })
    forumTitle: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'formation_description'
    })
    formationDescription: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es','en','it','pr'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type'
    })
    languageType: string;

    /**
     * RELACIONES
     * Forums pertenece a UserModel
     * Forums pertenece a Category
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => UserModel)
    userModel: UserModel;
    
    @ApiModelPropertyOptional()
    @BelongsTo(() => Categories)
    categories: Categories;

    /**
     * RELACIONES
     * Forums pertenece a ForumsMessages
     */    
    @ApiModelPropertyOptional({
        type: () => ForumsMessages,
        isArray: true
    })
    @HasMany(() => ForumsMessages)
    forumsMessages: ForumsMessages[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}