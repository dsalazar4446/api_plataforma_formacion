import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { UserModel } from './../../user/models/user.Model';
import { Forums } from './forums.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'form_messages',
})
export class ForumsMessages extends Model<ForumsMessages>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_id'
    })
    senderUserId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Forums)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'forum_id'
    })
    forumId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
        field: 'formu_message'
    })
    forumsMessage: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('0', '1', '2'),
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'forum_message_status'
    })
    forumsMessageStatus: string;

    /**
     * RELACIONES
     * ForumsMessages pertenece a  UserModel
     * ForumsMessages pertenece a  Forums
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => UserModel)
    userModel: UserModel;
    
    @ApiModelPropertyOptional()
    @BelongsTo(() => Forums)
    forums: Forums;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}