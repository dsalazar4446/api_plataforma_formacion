import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters, UseGuards } from '@nestjs/common';
import { MessageService } from '../services/messages.service';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { MessageSaveJson, MessageUpdateJson } from '../interfaces/messages.interface';
import { JwtAuthGuard } from '../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiImplicitQuery, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { MessageModel } from '../models/messages.model';

@Controller('messages')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class MessageController {
    constructor(
        private readonly messageService: MessageService,
    ) { }
    @ApiImplicitQuery({ name: 'messageStatus', enum: ['1','2','3'] })
    @Post()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: MessageModel,
      })
    async create(@Body() message: MessageSaveJson) {
        return await this.messageService.create(message);
    }
    @Get()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: MessageModel,
        isArray: true
      })
    async getAll() {
        return await this.messageService.findAll();
    }
    @Get('chat/:chatId')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: MessageModel,
        isArray: true
      })
    @ApiImplicitParam({ name: 'chatId', required: true, type: 'string' })
    async getAllByChatId(@Param('chatId') chatId) {
        return await this.messageService.findByChatId(chatId);
    }
    @Get(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: MessageModel,
      })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idMessage) {
        return await this.messageService.findById(idMessage);
    }
    @Put(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: MessageModel,
      })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @ApiImplicitQuery({ name: 'messageStatus', enum: ['1','2','3'] })
    async update(
        @Body()
        updateMessage: Partial<MessageUpdateJson>,
        @Param('id') idMessage
    ) {
        return await this.messageService.update(idMessage, updateMessage);
    }
    @Delete(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') idMessage) {
        return await this.messageService.deleted(idMessage);
    }
}
