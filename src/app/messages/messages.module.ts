import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { MessageController } from './controllers/messages.controller';
import { MessageService } from './services/messages.service';
import { MessageModel } from './models/messages.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { ChatModel } from '../chat/models/chat.model';
import { UserModel } from '../user/models/user.Model';
import { MessageAttachmentModel } from '../message-attachments/models/message-attachments.model';
const models = [
  MessageModel,
  ChatModel,
  UserModel,
  MessageAttachmentModel,
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [MessageController],
  providers: [MessageService]
})
export class MessagesModule {}
