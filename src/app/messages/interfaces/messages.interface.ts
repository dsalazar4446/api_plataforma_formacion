import { ApiModelProperty } from '@nestjs/swagger';
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class MessageSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    chatId: string;
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    userId: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    message: string;
    @ApiModelProperty({ enum: ['1','2','3']})
    @IsNotEmpty()
    @IsString()
    messageStatus: string;
}
// tslint:disable-next-line: max-classes-per-file
export class MessageUpdateJson extends MessageSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}