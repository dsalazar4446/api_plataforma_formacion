import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey} from 'sequelize-typescript';
import { ChatModel } from '../../chat/models/chat.model';
import { UserModel } from '../../user/models/user.Model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'messages',
})
export class MessageModel extends Model<MessageModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => ChatModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'chat_id',
    })
    chatId: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'user_id',
    })
    userId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'message',
    })
    message: string;
    @ApiModelProperty()
    @Column({
        type: DataType.ENUM(
            '1',
            '2',
            '3',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'message_status',
    })
    messageStatus: string;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
