import { Injectable, Inject } from '@nestjs/common';
import { MessageSaveJson, MessageUpdateJson } from '../interfaces/messages.interface';
import { MessageModel } from '../models/messages.model';
import { ChatModel } from '../../chat/models/chat.model';
import { UserModel } from '../../user/models/user.Model';
import { completeUrl } from '../../shared/utils/completUrl';
import { MessageAttachmentModel } from '../../message-attachments/models/message-attachments.model';
@Injectable() 
export class MessageService {
    constructor(
        @Inject('MessageModel') private readonly messageModel: typeof MessageModel,
        @Inject('ChatModel') private readonly chatModel: typeof ChatModel,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('MessageAttachmentModel') private readonly messageAttachmentModel: typeof MessageAttachmentModel,
    ) { }

// tslint:disable-next-line: max-line-length
    async findAll(): Promise<any> {
        const data = await this.messageModel.findAll();
        const result: any[]= [];
        await Promise.all(
            await data.map(
                async (item: any)=>{
                    const dataUser = await this.userModel.findByPk(item.dataValues.userId);
                    const dataChat = await this.chatModel.findByPk(item.dataValues.chatId);
                    const dataAttachment = await this.messageAttachmentModel.findAll({
                        where:{
                            messageId: item.dataValues.id
                        }
                    });
                    dataAttachment.map(
                        (data)=>{
                            return data.dataValues.messageAttachment = completeUrl('user/avatar',data.dataValues.messageAttachment );
                        }
                    )
                    item.dataValues.messageAttachmentModel = dataAttachment;
                    item.dataValues.userModel = dataUser.dataValues;
                    item.dataValues.chatModel = dataChat.dataValues;
                    item.dataValues.userModel.pwd = undefined;
                    item.dataValues.userModel.avatar = completeUrl('user/avatar',item.dataValues.userModel.avatar );
                    item.dataValues.chatModel.chatAvatar = completeUrl('chat/avatar',item.dataValues.chatModel.chatAvatar);
                    result.push(item.dataValues);
                }
            )
        )
        return result;
    }

    async findById(id: string): Promise<MessageModel> {
        const result: any = await this.messageModel.findByPk<MessageModel>(id);
        const dataUser = await this.userModel.findByPk(result.dataValues.userId);
        const dataChat = await this.chatModel.findByPk(result.dataValues.chatId);
        result.dataValues.userModel = dataUser.dataValues;
        result.dataValues.chatModel = dataChat.dataValues;
        result.dataValues.userModel.pwd = undefined;
        result.dataValues.userModel.avatar = completeUrl('user/avatar',result.dataValues.userModel.avatar);
        result.dataValues.chatModel.chatAvatar = completeUrl('chat/avatar',result.dataValues.chatModel.chatAvatar);
        const dataAttachment = await this.messageAttachmentModel.findAll({
            where:{
                messageId: result.dataValues.id
            }
        });
        dataAttachment.map(
            (data)=>{
                return data.dataValues.messageAttachment = completeUrl('user/avatar',data.dataValues.messageAttachment );
            }
        )
        result.dataValues.messageAttachmentModel = dataAttachment;
        return result;
    }

    async findByChatId(chatId: string): Promise<any> {
        const data = await this.messageModel.findAll({
            where: {
                chatId
            }
        });
        const result: any[]= [];
        await Promise.all(
            await data.map(
                async (item: any)=>{
                    const dataUser = await this.userModel.findByPk(item.dataValues.userId);
                    const dataChat = await this.chatModel.findByPk(item.dataValues.chatId);
                    const dataAttachment = await this.messageAttachmentModel.findAll({
                        where:{
                            messageId: item.dataValues.id
                        }
                    });
                    dataAttachment.map(
                        (data)=>{
                            return data.dataValues.messageAttachment = completeUrl('user/avatar',data.dataValues.messageAttachment );
                        }
                    )
                    item.dataValues.messageAttachmentModel = dataAttachment;
                    item.dataValues.userModel = dataUser.dataValues;
                    item.dataValues.chatModel = dataChat.dataValues;
                    item.dataValues.userModel.pwd = undefined;
                    item.dataValues.userModel.avatar = completeUrl('user/avatar',item.dataValues.userModel.avatar );
                    item.dataValues.chatModel.chatAvatar = completeUrl('chat/avatar',item.dataValues.chatModel.chatAvatar);
                    result.push(item.dataValues);
                }
            )
        )
        return result;
    }

    async create(message: MessageSaveJson): Promise<MessageModel> {
        const result: any = await this.messageModel.create<MessageModel>(message, {
            returning: true,
        });
        const dataUser = await this.userModel.findByPk(result.dataValues.userId);
        const dataChat = await this.chatModel.findByPk(result.dataValues.chatId);
        result.dataValues.userModel = dataUser.dataValues;
        result.dataValues.chatModel = dataChat.dataValues;
        result.dataValues.userModel.pwd = undefined;
        result.dataValues.userModel.avatar = completeUrl('user/avatar',result.dataValues.userModel.avatar);
        result.dataValues.chatModel.chatAvatar = completeUrl('chat/avatar',result.dataValues.chatModel.chatAvatar);
        const dataAttachment = await this.messageAttachmentModel.findAll({
            where:{
                messageId: result.dataValues.id
            }
        });
        dataAttachment.map(
            (data)=>{
                return data.dataValues.messageAttachment = completeUrl('user/avatar',data.dataValues.messageAttachment );
            }
        )
        result.dataValues.messageAttachmentModel = dataAttachment;
        return result;
    }
    async update(idMessage: string, messageUpdate: Partial<MessageUpdateJson>){
        const result: any =  await this.messageModel.update(messageUpdate, {
            where: {
                id: idMessage,
            },
            returning: true,
        });
        const dataUser = await this.userModel.findByPk(result[1][0].dataValues.userId);
        const dataChat = await this.chatModel.findByPk(result[1][0].dataValues.chatId);
        result[1][0].dataValues.userModel = dataUser.dataValues;
        result[1][0].dataValues.chatModel = dataChat.dataValues;
        result[1][0].dataValues.userModel.pwd = undefined;
        result[1][0].dataValues.userModel.avatar = completeUrl('user/avatar',result[1][0].dataValues.userModel.avatar);
        result[1][0].dataValues.chatModel.chatAvatar = completeUrl('chat/avatar',result[1][0].dataValues.chatModel.chatAvatar);
        const dataAttachment = await this.messageAttachmentModel.findAll({
            where:{
                messageId: result[1][0].dataValues.id
            }
        });
        dataAttachment.map(
            (data)=>{
                return data.dataValues.messageAttachment = completeUrl('user/avatar',data.dataValues.messageAttachment );
            }
        )
        result[1][0].dataValues.messageAttachmentModel = dataAttachment;
        return result;
    }
    async deleted(idMessage: string): Promise<any> {
        return await this.messageModel.destroy({
            where: {
                id: idMessage,
            },
        });
    }
}
