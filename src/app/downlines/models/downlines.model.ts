import { Table, Column, DataType, CreatedAt, UpdatedAt, Model, HasMany, BelongsToMany, ForeignKey, BelongsTo } from "sequelize-typescript";
import { UserModel } from "../../user/models/user.Model";
import { Transferals } from "../../transferals/models/transferal.model";
import { UserComissionsHasDownlineModel } from "../../user_comissions_has_downline/models/user_comissions_has_downline.model";
import { UserComissionModel } from "../../user-comission/models/user-comission.model";
import { ApiModelProperty } from "@nestjs/swagger";

@Table({
    tableName: 'downlines',
})
export class Downlines extends Model<Downlines> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'parent_user_id',
    })
    parentUserId: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'children_user_id',
    })
    childrenUserId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.SMALLINT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'downline_level',
    })
    downlineLevel: number;
    @ApiModelProperty()
    @Column({
        type: DataType.ENUM('left', 'center', 'right'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'downline_side',
    })
    downlineSide: string;
    @ApiModelProperty()
    @BelongsToMany(() => UserComissionModel, {
        through: {
            model: () => UserComissionsHasDownlineModel,
            unique: true,
        },
        as: 'DownlinesUserComissionHasDownline',
    })


    @BelongsTo(() => UserModel, 'parentUserId')
    parentUser: UserModel[];
    @ApiModelProperty()
    @BelongsTo(() => UserModel, 'childrenUserId')
    childrenUser: UserModel[];

    @HasMany(() => Transferals,)
    transferals: Transferals[];
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;

}