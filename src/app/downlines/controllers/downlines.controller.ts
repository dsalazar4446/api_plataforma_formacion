import { Controller, UseFilters, UseInterceptors, Post, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param, Inject, Req } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { DownlinesService } from '../services/downline.service';
import { DownlinesSaveJson } from '../inerfaces/downlines.interface';
import { Downlines } from '../models/downlines.model';
import { UserModel } from '../../user/models/user.Model';
import { UserComissionModel } from '../../user-comission/models/user-comission.model';
import { UserMembershipsModel } from '../../memberships/models/user-memberships.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller DownlinesController
 * @Creado 26 de Marzo 2019
 */

@ApiUseTags('Module-Downlines')
@Controller('downlines')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class DownlinesController {
    
    constructor(
        private readonly _downlinesService: DownlinesService,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        private readonly appUtilsService: AppUtilsService,
        @Inject('UserComissionModel') private readonly userComissionModel: typeof UserComissionModel,
        @Inject('UserMembershipsModel') private readonly userMembershipsModel: typeof UserMembershipsModel,
    ) { }
    @ApiResponse({
        status: 200,
        type: Downlines,
      })
    @Post()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async create(@Body() bodyDownlines: DownlinesSaveJson,@Req() req) {
        const data2 = await this.userModel.findByPk(bodyDownlines.parentUserId);
        if (data2 == null) {
            throw this.appUtilsService.httpCommonError('Parent not exists!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA3',
                languageType: req.headers.language,
            });
        }
        const data3 = await this.userModel.findByPk(bodyDownlines.childrenUserId);
        if (data3 == null) {
            throw this.appUtilsService.httpCommonError('Children not exists', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA3',
                languageType: req.headers.language,
            });
        }
        const userMembership = await this.userMembershipsModel.findOne({
            where:{
                userId: bodyDownlines.childrenUserId,
                membershipStatus: true
            }
        })
        // console.log(userMembership)
        if(userMembership == null || userMembership == undefined){
          throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
              messageCode: 'EA27',
              languageType:  req.headers.language,
          });
        }
        const createDownlines = await this._downlinesService.save(bodyDownlines);

        if (!createDownlines) {
            return this.appUtilsService.httpCommonError('Downlines does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return createDownlines;

    }
    @ApiResponse({
        status: 200,
        type: Downlines,
        isArray: true
      })
    @Get()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async showAll() {
        return await this._downlinesService.showAll();
    }

    /*@Get('mTree/:id')
    async mTree(@Param('id') id: string) {
        return await this._downlinesService.getbyId(id);
    }*/
    @ApiResponse({
        status: 200,
        type: Downlines,
      })
    @Get('family/:parentId')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'parentId', required: true, type: 'string' })
    async getFamily(@Param('parentId') id: string) {
        return await this._downlinesService.getAllFamily(id);
    }
    @ApiResponse({
        status: 200,
        type: Downlines,
        isArray: true
      })
    @Get('details/:id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: Downlines,
      })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idDownlines: string) {

        const fetchDownlines = await this._downlinesService.getDetails(idDownlines);

        if (!fetchDownlines) {
            throw new NotFoundException('Downlines does not exist!');
        }

        return fetchDownlines;
    }

    @Put(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: Downlines,
      })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateDownlines(@Param('id') id: string, @Body() bodyDownlines: Partial<DownlinesSaveJson>) {

        const updateDownlines = await this._downlinesService.update(id, bodyDownlines);

        if (!updateDownlines) {
            throw new NotFoundException('Downlines does not exist!');
        }

        return updateDownlines;
    }

    @Delete(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteDownlines(@Param('id') idDownlines: string) {

        const deleteDownlines = await this._downlinesService.destroyRanking(idDownlines);

        if (!deleteDownlines) {
            throw new NotFoundException('Downlines does not exist!');
        }

        return deleteDownlines;
    }
}
