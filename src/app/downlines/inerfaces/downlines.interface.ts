import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsUUID, IsNumber, IsString, IsOptional } from "class-validator";
export class DownlinesSaveJson {

    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    parentUserId: string;

    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    childrenUserId: string;

    @ApiModelProperty()
    @IsOptional()
    @IsNumber()
    downlineLevel?: number;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    downlineSide: string;
}
// tslint:disable-next-line: max-classes-per-file
export class DownlinesUpdateJson extends DownlinesSaveJson {
    @ApiModelProperty()
    @IsUUID()
    @IsNotEmpty()
    id: string;
}