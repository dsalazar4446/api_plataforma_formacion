import { Injectable, Inject, HttpStatus } from "@nestjs/common";
import { Downlines } from "../models/downlines.model";
import { DownlinesUpdateJson, DownlinesSaveJson } from "../inerfaces/downlines.interface";
import { UserModel } from "../../user/models/user.Model";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { DownlineTree, Nodo } from "./downlineTree";
import { UserComissionModel } from "../../user-comission/models/user-comission.model";
import { MembershipModel } from "../../memberships/models/membership.model";
import { MembershipsComissionsModel } from "../../memberships/models/membership-comissions.model";
import { MembershipPricingsModel } from "../../memberships/models/membership-pricings.model";
import { UserComissionStatusModel } from "../../user-comission-status/models/user-comission-status.model";
import { UserMembershipsModel } from "../../memberships/models/user-memberships.model";
import { UserComissionsHasDownlineModel } from "../../user_comissions_has_downline/models/user_comissions_has_downline.model";
import { UserComissionService } from '../../user-comission/services/user-comission.service';

var mTree = new DownlineTree();

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services DownlinesService
 * @Creado 23 de Abril 2019
 */

@Injectable()
export class DownlinesService {
    arrayFamily: any[] = [];
    private nodo: Nodo;
    private hijo: Nodo;
    private include = {
        include: [
            {
                as: 'parentUser',
                model: UserModel
            },
            {
                as: 'childrenUser',
                model: UserModel
            },
        ]
    }

    constructor(
        @Inject('Downlines') private readonly _downlines: typeof Downlines,
        private readonly appUtilsService: AppUtilsService,
        private readonly userComissionService: UserComissionService,
        @Inject('UserComissionModel') private readonly userComissionModel: typeof UserComissionModel,
        @Inject('UserMembershipsModel') private readonly userMembershipsModel: typeof UserMembershipsModel,
        @Inject('MembershipModel') private readonly membershipModel: typeof MembershipModel,
        @Inject('MembershipsComissionsModel') private readonly membershipsComissionsModel: typeof MembershipsComissionsModel,
        @Inject('MembershipPricingsModel') private readonly membershipPricingsModel: typeof MembershipPricingsModel,
        @Inject('UserComissionStatusModel') private readonly userComissionStatusModel: typeof UserComissionStatusModel,
        @Inject('UserComissionsHasDownlineModel') private readonly userComissionsHasDownlineModel: typeof UserComissionsHasDownlineModel,
        
    ) { }

    async getFather(id: string) {
        const result = await this._downlines.findOne({
            where: {
                childrenUserId: id
            }
        })
        if (result === null) {
            return null;
        }
        return {
            "parentUserId": result.dataValues.parentUserId,
            "parentUserSide": result.dataValues.downlineSide,
        }
    }
    async getDownlineByUserId(userId){
        return await this._downlines.findOne({
            where:{
                parentUserId: userId,
            }
        })
    }
    async getFamily(father: string, side: string) {
        try {
            this.arrayFamily.push({
                "parentUserId": father,
                "parentUserSide": side,
            });
            const existParent = await this.getFather(father);
            if (existParent != null) {
                await this.getFamily(existParent.parentUserId, existParent.parentUserSide);
            }
            return this.arrayFamily;
        } catch (error) {
            return error;
        }
    }
    save(bodyDownlines: DownlinesSaveJson) {
        return new Promise(async (resolve, reject) => {
            const dataResult: any = [];
            try {
                const family = await this.getFamily(bodyDownlines.parentUserId, bodyDownlines.downlineSide);
                this.arrayFamily = [];
                await Promise.all(
                    await family.map(
                        async (data, index) => {
                            bodyDownlines.parentUserId = data.parentUserId;
                            bodyDownlines.downlineSide = data.parentUserSide;
                            bodyDownlines.downlineLevel = index + 1;
                            const result = await this._downlines.create(bodyDownlines, {
                                returning: true,
                            });
                            dataResult.push(result.dataValues);
                        }
                    )
                )
                this.userComissionService.createUserComissionByMembership(bodyDownlines.childrenUserId);
                resolve(dataResult);
            } catch (error) {
                reject(error);
            }
        });
    }

    async getAllFamily(id: string){
        return await this._downlines.findAll({
            where:{
                parentUserId: id
            },
            order: [
                ['downline_level', 'ASC'],
            ],
        });
    }
    async showAll(): Promise<DownlinesUpdateJson[]> {
        return await this._downlines.findAll(this.include);
    }

    async getDetails(downlineId: string) {
        return await this._downlines.findByPk(downlineId, this.include);
    }

    async update(id: string, bodyDownlines: Partial<DownlinesSaveJson>): Promise<[number, Array<any>]> {
        return await this._downlines.update(bodyDownlines, {
            where: { id },
            returning: true
        });
    }

    async destroyRanking(downlineId: string): Promise<number> {
        return await this._downlines.destroy({
            where: { id: downlineId },
        });
    }

    async getbyId(parentUserId: string, nodoPadre?: Nodo) {

        const exist = await this._downlines.findAll({ where: { parentUserId } });

        if (exist.length === 0) {
            throw this.appUtilsService.httpCommonError(`Usuario ${parentUserId} no es Papa de nadie o es una hoja`, HttpStatus.FORBIDDEN);
        }

        exist.forEach(async item => {


            if (this.nodo.parent === item.parentUserId) {
                console.log('soy el mismo padre!');

                const children = await this._downlines.findOne({ where: { childrenUserId: item.childrenUserId } });
                this.nodo = {
                    value: children.parentUserId,
                    parent: children.parentUserId,
                    children: children.childrenUserId,
                    level: children.downlineLevel,
                    side: children.downlineSide
                }
                mTree.add(this.nodo, this.nodo.value);
            }


            this.nodo = {
                value: item.parentUserId,
                parent: item.parentUserId,
                children: item.childrenUserId,
                level: item.downlineLevel,
                side: item.downlineSide
            }

            if (!mTree.root) {
                mTree.add(this.nodo);
            }

            // if (nodoPadre) {
            //     if (nodoPadre.value === this.nodo.value) {
            //         mTree.add(this.nodo, nodoPadre.value);
            //     }
            // }

            console.log(`usuario ${item.parentUserId} es papa de ${item.childrenUserId} en la rama ${item.downlineLevel}`);
            console.log(mTree.root);

        });
        return await this.getbyId(this.nodo.children, this.nodo);


    }

  
}