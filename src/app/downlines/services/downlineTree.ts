export interface Nodo{

    value: string,
    parent: string,
    children: string,
    side: string,
    level?: number
}

export class Node {

    public value;
    public children = [];
    public side;
    public parent;
    public level;

    constructor(val: Nodo) {
        this.value = val.value;
        this.parent = val.parent;
        this.side = val.side;
        this.level = val.level
    }
}

export class DownlineTree {

    root: Nodo;

    constructor(){
        this.root = undefined; // referencia al nodo raíz

    }

    findBFS = function (value) {
        var queue = [this.root];
        while (queue.length) {
            var node = queue.shift();
            if (node.value === value) {
                return node;
            }
            for (var i = 0; i < node.children.length; i++) {
                queue.push(node.children[i]);
            }
        }
        return null;
    };

    add = function (value: Nodo, toNodeValue?) {
        var node = new Node(value);
        var parent = toNodeValue ? this.findBFS(toNodeValue) : null;
        if (parent) {
            parent.children.push(node);
        }
        else if (!this.root) {
            this.root = node;
        }
        else {
            throw new Error('Root node is already assigned');
        }
    };

    remove = function (value) {
        // caso especial: si el valor está en el
        // nodo raíz reseteamos el árbol
        if (this.root.value === value) {
            this.root = null;
        }
        var queue = [this.root];
        while (queue.length) {
            var node = queue.shift();
            for (var i = 0; i < node.children.length; i++) {
                if (node.children[i].value === value) {
                    node.children.splice(i, 1);
                }
                else {
                    queue.push(node.children[i]);
                }
            }
        }
    };

    traverseBFS = function (fn) {
        var queue = [this.root];
        while (queue.length) {
            var node = queue.shift();
            fn && fn(node);
            for (var i = 0; i < node.children.length; i++) {
                queue.push(node.children[i]);
            }
        }
    };

    traverseDFS = function (fn, method) {
        var current = this.root;
        if (method) {
            this['_' + method](current, fn);
        }
        else {
            this._preOrder(current, fn);
        }
    };

    preOrder = function (node, fn) {
        if (!node) {
            return;
        }
        fn && fn(node);
        for (var i = 0; i < node.children.length; i++) {
            this._preOrder(node.children[i], fn);
        }
    };

    postOrder = function (node, fn) {
        if (!node) {
            return;
        }
        for (var i = 0; i < node.children.length; i++) {
            this._postOrder(node.children[i], fn);
        }
        fn && fn(node);
    };
}