import { Module, HttpModule, Provider } from "@nestjs/common";
import { SharedModule } from "../shared/shared.module";
import { DatabaseModule } from "../database/database.module";
import { DownlinesController } from './controllers/downlines.controller';
import { DownlinesService } from './services/downline.service';
import { Downlines } from './models/downlines.model';
import { UserComissionStatusModel } from "../user-comission-status/models/user-comission-status.model";
import { UserComissionModel } from "../user-comission/models/user-comission.model";
import { MembershipModel } from '../memberships/models/membership.model';
import { MembershipsComissionsModel } from '../memberships/models/membership-comissions.model';
import { MembershipPricingsModel } from '../memberships/models/membership-pricings.model';
import { UserMembershipsModel } from '../memberships/models/user-memberships.model';
import { UserComissionsHasDownlineModel } from "../user_comissions_has_downline/models/user_comissions_has_downline.model";
import { UserModel } from '../user/models/user.Model';
import { UserComissionService } from "../user-comission/services/user-comission.service";

const models = [
    Downlines,
    UserComissionStatusModel,
    UserComissionModel,
    MembershipModel,
    MembershipsComissionsModel,
    MembershipPricingsModel,
    UserMembershipsModel,
    UserComissionsHasDownlineModel,
    UserModel,
];
const providers: Provider[] = [DownlinesService,UserComissionService];
const controllers = [ DownlinesController ];

@Module({
    imports: [
        SharedModule,
        HttpModule,
        DatabaseModule.forRoot({
            sequelize: {
                models,
            },
            loki: true,
        }),
    ],
    providers,
    controllers,
    exports: [DownlinesService]
})
export class DownlinesModule { }
