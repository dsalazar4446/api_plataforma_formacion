import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty, IsUUID, IsDate } from "class-validator";

export class ConsultingsSaveJson {

    @ApiModelProperty()
    @IsUUID('4', {message: "The userId variable must be a uuid"})
    @IsNotEmpty({message: "The userId variable can not be null"})
    userId: string;

    @ApiModelProperty()
    @IsUUID('4', {message: "The advisorUserId variable must be a uuid"})
    @IsNotEmpty({message: "The advisorUserId variable can not be null"})
    advisorUserId: string;

    @ApiModelProperty()
    @IsDate()
    @IsNotEmpty({message: "The advisorUserId variable can not be null"})
    consultingScheduleDate: Date;

    @ApiModelProperty()
    @IsDate()
    @IsNotEmpty({message: "The advisorUserId variable can not be null"})
    consultingDate: Date;
}

export class ConsultingsUpdateJson extends ConsultingsSaveJson {

    @ApiModelProperty()
    @IsUUID('4', {message: "The id variable must be a uuid"})
    @IsNotEmpty({message: "The id variable can not be null"})
    id: string;
}
