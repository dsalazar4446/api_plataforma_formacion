import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { Consultings } from '../models/consultings.model';
import { ConsultingsSaveJson, ConsultingsUpdateJson } from '../interfaces/consultings.interface';
import { UserModel } from '../../user/models/user.Model';
import { UserMembershipsModel } from '../../memberships/models/user-memberships.model';
import { MembershipModel } from '../../memberships/models/membership.model';
import * as moment from 'moment';
import { AppUtilsService } from '../../shared/services/app-utils.service';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services ConsultingsService
 * @Creado 08 de Mayo 2019
 */

@Injectable()
export class ConsultingsService {

    private include = {
        include: [
            {
                as: 'userConsulting',
                model: UserModel
            },
            {
                as: 'advisorUser',
                model: UserModel
            }
        ]
    }

    constructor(
        @Inject('Consultings') private readonly _consultings: typeof Consultings,
        @Inject('UserMembershipsModel') private readonly _userMemberships: typeof UserMembershipsModel,
        private readonly appUtilsService: AppUtilsService
    ) { }

    async saveConsultings(bodyConsultings: ConsultingsSaveJson): Promise<any> {

        let today = `${moment().format('YYYY')}-${moment().format('MM')}-${moment().format('DD')}`;
        let fecha = new Date(today);

        const countConsulting = await this._consultings.findAndCountAll({
            where: { userId: bodyConsultings.userId },
        });

        const userM = await this._userMemberships.findOne({
            where: { userId: bodyConsultings.userId },
            include: [MembershipModel]
        });

        if(userM){
            if(new Date(userM.membershipEndDate) >= fecha){
                if(countConsulting.count > userM.membership.membershipAdvisores){
                    throw this.appUtilsService.httpCommonError(`Su membresia tiene como maximo ${userM.membership.membershipAdvisores} consultas y ya ha alcanzado el limite`, HttpStatus.UNAUTHORIZED,{
                        messageCode: 'EM145',
                        languageType: 'es'
                    });
                }

                return await new this._consultings(bodyConsultings).save();

            }else{
                throw this.appUtilsService.httpCommonError(`Su membresia venció el ${userM.membershipEndDate}`, HttpStatus.UNAUTHORIZED, {
                    messageCode: 'EM143',
                    languageType: 'es'
                });
            }
        }else{
            throw this.appUtilsService.httpCommonError('Debe obtener una Membresia para poder consultar!', HttpStatus.UNAUTHORIZED, {
                messageCode: 'EM143',
                languageType: 'es',
            });
        }
    }

    async showAllConsultings(): Promise<ConsultingsUpdateJson[]> {
        return await this._consultings.findAll(this.include);
    }

    async getDetails(ConsultingsId: string) {
        return await this._consultings.findByPk(ConsultingsId, this.include);
    }

    async updateConsultings(id: string, bodyConsultings: Partial<ConsultingsUpdateJson>): Promise<[number, Array<ConsultingsUpdateJson>]> {
        return await this._consultings.update(bodyConsultings, {
            where: { id },
            returning: true
        });

    }

    async destroyConsultings(ConsultingsId: string): Promise<number> {
        return await this._consultings.destroy({
            where: { id: ConsultingsId },
        });
    }
}
