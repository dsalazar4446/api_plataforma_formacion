import { Table, Column, DataType, CreatedAt, UpdatedAt, Model, HasMany, ForeignKey, BelongsTo } from "sequelize-typescript";
import { UserModel } from "../../user/models/user.Model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({
    tableName: 'consultings',
})
export class Consultings extends Model<Consultings> {

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'advisor_user_id',
    })
    advisorUserId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'consulting_schedule_date',
    })
    consultingScheduleDate: Date;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'consulting_date',
    })
    consultingDate: Date;
    
    @ApiModelPropertyOptional()
    @BelongsTo(() => UserModel, 'userId')
    userConsulting: UserModel[];
    
    @ApiModelPropertyOptional()
    @BelongsTo(() => UserModel, 'advisorUserId')
    advisorUser: UserModel[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}