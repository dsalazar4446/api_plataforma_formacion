import { SharedModule } from "../shared/shared.module";
import { HttpModule, Module, Provider } from "@nestjs/common";
import { DatabaseModule } from "../database/database.module";
import { Consultings } from './models/consultings.model'
import { ConsultingsController } from './controllers/consultings.controller';
import { ConsultingsService } from './services/consultings.service';
import { MembershipsModule } from '../memberships/memberships.module';

const models = [Consultings];
const controllers = [ConsultingsController];
const providers: Provider[] = [ConsultingsService];

@Module({
  imports: [
    SharedModule,
    MembershipsModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  controllers,
  providers
})
export class ConsultingsModule { }
