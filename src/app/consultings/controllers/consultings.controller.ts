import { Controller, UseFilters, UseInterceptors, Post, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { ConsultingsService } from '../services/consultings.service';
import { ConsultingsSaveJson } from '../interfaces/consultings.interface';
import { Consultings } from '../models/consultings.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller ConsultingsController
 * @Creado 07 de Mayo 2019
 */

@ApiUseTags('Module-Consultings')
@Controller('consultings')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class ConsultingsController {

    constructor(private readonly _consultingsService: ConsultingsService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: Consultings,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyConsultings: ConsultingsSaveJson) {
        const createConsultings = await this._consultingsService.saveConsultings(bodyConsultings);
        if (!createConsultings) {
            throw this.appUtilsService.httpCommonError('Consultas no creadas!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM27',
                languageType: 'es',
            });
        }
        return createConsultings;
    }


    @ApiResponse({
        status: 200,
        type: Consultings,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idConsultings: string) {

        const fetchConsultings = await this._consultingsService.getDetails(idConsultings);

        if (!fetchConsultings) {
            throw this.appUtilsService.httpCommonError('La consulta no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM28',
                languageType: 'es',
            });
        }
        return fetchConsultings;
    }


    @ApiResponse({
        status: 200,
        type: Consultings,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllConsultings() {

        const fetch = await this._consultingsService.showAllConsultings();
        if (!fetch) {
            throw this.appUtilsService.httpCommonError('La consulta no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM28',
                languageType: 'es',
            });
        }
        return fetch;
    }



    @ApiResponse({
        status: 200,
        type: Consultings,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateConsultings(@Param('id') id: string, @Body() bodyConsultings: Partial<ConsultingsSaveJson>) {

        const updateConsultings = await this._consultingsService.updateConsultings(id, bodyConsultings);
        if (!updateConsultings[1][0]) {
            throw this.appUtilsService.httpCommonError('La consulta no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM28',
                languageType: 'es',
            });
        }
        return updateConsultings;
    }


    @ApiResponse({
        status: 200,
        type: Consultings,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteConsultings(@Param('id') idConsultings: string) {

        const deleteConsultings = await this._consultingsService.destroyConsultings(idConsultings);
        if (!deleteConsultings) {
            throw this.appUtilsService.httpCommonError('La consulta no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM28',
                languageType: 'es',
            });
        }
        return deleteConsultings;
    }
}
