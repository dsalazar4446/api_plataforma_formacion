import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'auths',
})
export class AuthsModel extends Model<AuthsModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.STRING(36),
        allowNull: false,
        field: 'user_id',
    })
    userId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(128),
        allowNull: false,
        field: 'pwd',
    })
    pwd: string;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
