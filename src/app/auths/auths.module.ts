import { Module, forwardRef } from '@nestjs/common';
import { AuthsController } from './controllers/auths.controller';
import { AuthsService } from './services/auths.service';
import { AuthsModel } from './models/auths.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { CONFIG } from './../../config';
import { UserModule } from '../user/user.module';
import { JwtStrategy } from './jwt.strategy';
import { MembershipsModule } from '../memberships/memberships.module';
import { DeviceModule } from '../device/device.module';
import { SessionsModule } from '../sessions/sessions.module';
import { UserLocationModule } from '../user-location/user-location.module';

const models = [
  AuthsModel,
];
@Module({
  imports: [
    forwardRef(() => SharedModule),
    forwardRef(() => UserModule),
    forwardRef(() => MembershipsModule),
    forwardRef(() => SessionsModule),
    forwardRef(() => DeviceModule),
    forwardRef(() => UserLocationModule),
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
    PassportModule.register({ defaultStrategy: 'jwt', session: true }),
    JwtModule.register({
      secretOrPrivateKey: CONFIG.jwt.secret,
      signOptions: {
        expiresIn: '180 days',
      },
    }),
  ],
  controllers: [AuthsController],
  providers: [AuthsService, JwtStrategy],
  exports: [PassportModule, AuthsService ]
})
export class AuthsModule {}

