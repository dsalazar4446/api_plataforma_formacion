import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsNumber} from "class-validator";
export class JwtPayload {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    @IsEmail()
    email: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    password: string;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    device?: string;

    @ApiModelProperty()
    @IsNumber()
    @IsNotEmpty()
    os?: number;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    ipAddress?: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    longitude?: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    latitude?: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    playerId?: string;
    @ApiModelProperty()
    @IsBoolean()
    @IsNotEmpty()
    mobileVibration?: boolean;
    @ApiModelProperty()
    @IsBoolean()
    @IsNotEmpty()
    deviceStatus?: boolean;
    @ApiModelProperty()
    @IsBoolean()
    @IsNotEmpty()
    mobileNotification?: boolean;
}


// tslint:disable-next-line: max-classes-per-file
export class UserLoginModel{
    @ApiModelProperty()
    roleId: string;
    @ApiModelProperty()
    documentTypeId: string;
    @ApiModelProperty()
    clientCode: string;
    @ApiModelProperty()
    pwd: string;
    @ApiModelProperty()
    email: string;
    @ApiModelProperty()
    cellphone: number;
    @ApiModelProperty()
    username: string;
    @ApiModelProperty()
    avatar: string;
    @ApiModelProperty()
    firstName: string;
    @ApiModelProperty()
    lastName: string;
    @ApiModelProperty()
    phone: number;
    @ApiModelProperty()
    gender: string;
    @ApiModelProperty()
    birthDate: Date;
    @ApiModelProperty()
    telegram: string;
    @ApiModelProperty()
    facebook: string;
    @ApiModelProperty()
    whatsapp: number;
    @ApiModelProperty()
    skype: string;
    @ApiModelProperty()
    registerStep: number;
    @ApiModelProperty()
    advisor: boolean;
    @ApiModelProperty()
    mlmSide: string;
    @ApiModelProperty()
    oddoUserId: number;
    @ApiModelProperty()
    tradeSystem: string;
    @ApiModelProperty()
    operationMethod: string;
    @ApiModelProperty()
    tradingProfile: string;
    @ApiModelProperty()
    masterRootAccount: string;
    @ApiModelProperty()
    riskBenefitAverageRatio: string;
    @ApiModelProperty()
    maximumOperationsPerDay: string;
    @ApiModelProperty()
    maximumLostPercentagePerDay: string;
    @ApiModelProperty()
    percentageOfAssertiveness: string;
    @ApiModelProperty()
    maximumOfStraightNegativeOperations: string;
    @ApiModelProperty()
    maximumLosess: string;
    @ApiModelProperty()
    maximumEarnings: string;
    @ApiModelProperty()
    eficientOperationDays: string;
}

// tslint:disable-next-line: max-classes-per-file
export class LoginModel{
    @ApiModelProperty()
    user: UserLoginModel;
    @ApiModelProperty()
    token: string;
}