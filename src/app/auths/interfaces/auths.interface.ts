import { ApiModelProperty } from '@nestjs/swagger';
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class AuthsSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    userId: string;
    @IsString()
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    pwd: string;
}

export class AuthsUpdateJson extends AuthsSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}
 export class logoutJson {
     @ApiModelProperty()
     @IsUUID('4')
     @IsNotEmpty()
     userId: string;
 }