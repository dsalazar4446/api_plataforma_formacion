import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { AuthsSaveJson, AuthsUpdateJson } from '../interfaces/auths.interface';
import { AuthsModel } from '../models/auths.model';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from '../interfaces/jwt.interface';
import * as bcrypt from 'bcrypt';
import { UsersService } from '../../user/services/users.service';
import { UserMembershipsService } from '../../memberships/services/user-memberships/user-memberships.service';
import { PaymentCheckService } from '../../payment/services/payment-check.service';



@Injectable()
export class AuthsService {
    constructor(
        @Inject('AuthsModel') private readonly authsModel: typeof AuthsModel,
        private readonly jwtService: JwtService,
        @Inject(forwardRef(() => UsersService)) private readonly userService: UsersService,
        private readonly userMembershipService: UserMembershipsService,
        
    ) { }
    async findAll(): Promise<AuthsModel[]> {
        return await this.authsModel.findAll();
    }
    async findById(id: string): Promise<AuthsModel> {
        return await this.authsModel.findById<AuthsModel>(id);
    }
    async create(auth: AuthsSaveJson): Promise<AuthsModel> {
        return await this.authsModel.create<AuthsModel>(auth, {
            returning: true,
        });
    }
    async update(idAuth: Partial<AuthsUpdateJson>, authUpdate: Partial<AuthsUpdateJson>) {
        return await this.authsModel.update(authUpdate, {
            where: {
                id: idAuth,
            },
            returning: true,
        });
    }
    async deleted(idAuth: string): Promise<any> {
        return await this.authsModel.destroy({
            where: {
                id: idAuth,
            },
        });
    }
    async validateUser(email: string) {
        return await this.userService.validateUser('email', email);

    }
    async validateUserByEmail(email: string, password: string) {
        const user = await this.userService.validateUserByEmail(email);
        let pwd = null;
        if (user) {
            pwd = await this.authsModel.findOne({
                attributes: ['pwd'],
                where: {
                    userId: user.id,
                },
            });
            if (bcrypt.compareSync(password, pwd.pwd)) {
                return user;
            }
        }
        return null;
    }

    async verifyUserMemActpayment(userId: string){
        const userMembership = await this.userMembershipService.detail(userId);
        if(userMembership)
        userMembership.activationPayments.forEach(activation =>{
            if(activation.paymentSchedule >= new Date() && activation.paymentStatus == 0    ){

            }
        })
    }

    async sigin(payload: JwtPayload) {
        return this.jwtService.sign(payload);
    }

    async currentUser(token: string) {
        const verify = this.jwtService.verify(token)
        const user = await this.userService.validateUserByEmail(verify.email);
        if (user) {
            return user;
        }
        return null;

    }

    decode(token: string) {
        return this.jwtService.verify(token);
    }

}
