// tslint:disable-next-line: max-line-length
import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, FileFieldsInterceptor, UseInterceptors, UseFilters, UploadedFiles, HttpStatus, UnauthorizedException, Req, HttpException, UseGuards, Inject } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { AuthsService } from '../services/auths.service';
import { AuthsSaveJson, AuthsUpdateJson, logoutJson } from '../interfaces/auths.interface';
import { JwtPayload, UserLoginModel, LoginModel } from '../interfaces/jwt.interface';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { JwtAuthGuard } from '../guards/jwt-auth.guard';
import { AuthsModel } from '../models/auths.model';
import { SessionsService } from '../../sessions/services/sessions.service';
import { DeviceService } from '../../device/services/device.service';
import moment = require('moment');
import { DeviceModel } from '../../device/models/device.model';
import { DeviceSaveJson } from '../../device/interfaces/device.interface';
import { UserLocationService } from '../../user-location/services/user-location.service';
import { CONFIG } from '../../../config';
@ApiUseTags('Auths')
@Controller('auths')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class AuthsController {
    constructor(
        private readonly authsService: AuthsService,
        private readonly appUtilsService: AppUtilsService,
        @Inject('AuthsModel') private readonly authsModel: typeof AuthsModel,
        private readonly session: SessionsService,
        private readonly device: DeviceService,
        private readonly userLocation: UserLocationService,
        ) {}
        @ApiResponse({
            status: 200,
            type: AuthsModel,
          })
        @Post()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async create(@Body() auths: AuthsSaveJson, @Req() req) {
            const data3 = await this.authsModel.findByPk(auths.userId);
            if(data3 == null){
                throw this.appUtilsService.httpCommonError('Auths does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA4',
                    languageType:  req.headers.language,
                });
            }
            return await this.authsService.create(auths);
        }
        @ApiResponse({
            status: 200,
            type: LoginModel,
          })
        @Post('login')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async login(@Body() body: JwtPayload) {
            const validate = await this.authsService.validateUserByEmail(body.email, body.password);
            if(!validate){
                throw new UnauthorizedException();
            } else{
                const token = await this.authsService.sigin(body);
                const user = {
                    id: validate.id,
                    roleId: validate.roleId,
                    documentTypeId: validate.documentTypeId,
                    documentNumber: validate.documentNumber,
                    clientCode: validate.clientCode,
                    email: validate.email,
                    cellphone: validate.cellphone,
                    username: validate.username,
                    avatar: CONFIG.storage.server + 'user/avatar/' + validate.avatar,
                    firstName: validate.firstName,
                    lastName: validate.lastName,
                    phone: validate.phone,
                    gender: validate.gender,
                    birthDate: validate.birthDate,
                    telegram: validate.telegram,
                    facebook: validate.facebook,
                    whatsapp: validate.whatsapp,
                    skype: validate.skype,
                    registerStep: validate.registerStep,
                    advisor: validate.advisor,
                    mlmSide: validate.mlmSide,
                    oddoUserId: validate.oddoUserId,
                    tradeSystem: validate.tradeSystem,
                    operationMethod: validate.operationMethod,
                    tradingProfile: validate.tradingProfile,
                    masterRootAccount: validate.masterRootAccount,
                    riskBenefitAverageRatio: validate.riskBenefitAverageRatio,
                    maximumOperationsPerDay: validate.maximumOperationsPerDay,
                    maximumLostPercentagePerDay: validate.maximumLostPercentagePerDay,
                    percentageOfAssertiveness: validate.percentageOfAssertiveness,
                    maximumOfStraightNegativeOperations: validate.maximumOfStraightNegativeOperations,
                    maximumLosess: validate.maximumLosess,
                    maximumEarnings: validate.maximumEarnings,
                    eficientOperationDays: validate.eficientOperationDays,
                    userCountryId: validate.userCountryId,
                    userCityId: validate.userCityId,
                    resetPasswordCode: validate.resetPasswordCode,
                    passwordCodeTime: validate.passwordCodeTime
                }
                                   
                const deviceData: DeviceSaveJson = {
                    userId: validate.id,
                    uuidDevice:body.device,
                    os: body.os,
                    ipAddress: body.device,
                    longitude: body.longitude,
                    latitude: body.latitude,
                    playerId: body.playerId,
                    mobileVibration: body.mobileVibration,
                    deviceStatus: body.deviceStatus,
                    mobileNotification: body.mobileNotification,
                }
                    // console.log('creando dispositivo')
                let device = await this.device.create(deviceData)
                if(!device){
                    throw new HttpException('no se pudo crear dispositivo',HttpStatus.UNPROCESSABLE_ENTITY);
                }        
                let session;
                if(device){
                    // console.log('creando session')
                    session = await this.session.create({
                        userId: validate.id,
                        deviceId: device.id,
                        sessionStatus: '1',
                        sessionDate: `${moment().format('YYYY-MM-DD hh:mm:ss')}`
                    })
                }
                

                if(!session){
                    console.log('sesion no creada')
                }
                return {
                    user,
                    token,
                };
            }
        }
        @Post('logout')
        @ApiResponse({
            status: 200,
            type: DeviceModel,
        })
        async logout(@Body() body: logoutJson){
            const device = await this.device.findByUserId(body.userId)
            return await this.session.create({
                userId: body.userId,
                deviceId: device.id,
                sessionStatus: '0',
                sessionDate: `${moment(moment().format())}`
            })
        }
    @ApiResponse({
        status: 200,
        type: UserLoginModel,
    })
    @Post('me')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    // @UseGuards(new JwtAuthGuard())
    async me(@Req() req) {
        const token = req.headers.authorization.split(' ')[1];
        if (token) {
            const validate = await this.authsService.currentUser(token);
            const user = {
                id: validate.id,
                roleId: validate.roleId,
                documentTypeId: validate.documentTypeId,
                documentNumber: validate.documentNumber,
                clientCode: validate.clientCode,
                email: validate.email,
                cellphone: validate.cellphone,
                username: validate.username,
                avatar: CONFIG.storage.server + 'user/avatar/' + validate.avatar,
                firstName: validate.firstName,
                lastName: validate.lastName,
                phone: validate.phone,
                gender: validate.gender,
                birthDate: validate.birthDate,
                telegram: validate.telegram,
                facebook: validate.facebook,
                whatsapp: validate.whatsapp,
                skype: validate.skype,
                registerStep: validate.registerStep,
                advisor: validate.advisor,
                mlmSide: validate.mlmSide,
                oddoUserId: validate.oddoUserId,
                tradeSystem: validate.tradeSystem,
                operationMethod: validate.operationMethod,
                tradingProfile: validate.tradingProfile,
                masterRootAccount: validate.masterRootAccount,
                riskBenefitAverageRatio: validate.riskBenefitAverageRatio,
                maximumOperationsPerDay: validate.maximumOperationsPerDay,
                maximumLostPercentagePerDay: validate.maximumLostPercentagePerDay,
                percentageOfAssertiveness: validate.percentageOfAssertiveness,
                maximumOfStraightNegativeOperations: validate.maximumOfStraightNegativeOperations,
                maximumLosess: validate.maximumLosess,
                maximumEarnings: validate.maximumEarnings,
                eficientOperationDays: validate.eficientOperationDays,
                userCountryId: validate.userCountryId,
                userCityId: validate.userCityId,
                resetPasswordCode: validate.resetPasswordCode,
                passwordCodeTime: validate.passwordCodeTime
            }
            return user;
        }
        throw new HttpException('Recurso no encontrado', HttpStatus.UNAUTHORIZED)
    }
    
    @ApiResponse({
        status: 200,
        type: AuthsModel,
        isArray: true
    })
    @Get()
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    async getAll() {
        return await this.authsService.findAll();
    }
    @ApiResponse({
        status: 200,
        type: AuthsModel,
    })
    @Get(':id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idAuth) {
        return await this.authsService.findById(idAuth);
    }
    @ApiResponse({
        status: 200,
        type: AuthsModel,
    })
    @Put(':id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(
        @Body()
        updateAuth: Partial<AuthsUpdateJson>,
        @Param('id') idAuth,
        @Req() req
    ) {
        const data3 = await this.authsModel.findByPk(idAuth);
        if (data3 == null) {
            throw this.appUtilsService.httpCommonError('Auths does not updated!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA5',
                languageType: req.headers.language,
            });
        }
        return await this.authsService.update(idAuth, updateAuth);
    }
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Delete(':id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    async delete(@Param('id') idAuth) {
        return await this.authsService.deleted(idAuth);
    }

}
