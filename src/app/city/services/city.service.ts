import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { CitySaveJson, CityUpdateJson } from '../interfaces/city.interface';
import { CityModel } from '../models/city.model';
import { Sequelize } from 'sequelize-typescript';
import { Companies } from '../../companies/models/companies.model';
import { Events } from '../../events/models/events.model';
import { Countries } from '../models/countries.model';
import { AppUtilsService } from "../../shared/services/app-utils.service";
const Op = Sequelize.Op;
@Injectable()
export class CityService {
    constructor(
        @Inject('CityModel') private readonly cityModel: typeof CityModel,
        @Inject('Countries') private readonly countries: typeof Countries,
        private readonly appUtilsService: AppUtilsService
    ) { }

    async findAll(): Promise<any> {
        return await this.cityModel.findAll({
            include: [
                {
                    model: Companies
                },
                {
                    model: Events
                }
            ]
        });
    }

    async findById(id: string): Promise<CityModel> {
        return await this.cityModel.findById<CityModel>(id, {
            include: [
                {
                    model: Companies
                },
                {
                    model: Events
                }
            ]
        });
    }

    async create(city: CitySaveJson): Promise<CityModel> {
        return await this.cityModel.create<CityModel>(city, {
            returning: true,
            include: [
                {
                    model: Companies
                },
                {
                    model: Events
                }
            ]
        });
    }

    async update(idCity: string, cityUpdate: Partial<CityUpdateJson>){
        return  await this.cityModel.update(cityUpdate, {
            where: {
                id: idCity,
            },
            returning: true,
        });
    }

    async deleted(idCity: string): Promise<any> {
        return await this.cityModel.destroy({
            where: {
                id: idCity,
            },
        });
    }
}
