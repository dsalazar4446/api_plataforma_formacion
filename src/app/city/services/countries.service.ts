import { Injectable, Inject } from "@nestjs/common";
import { Countries } from "../models/countries.model";
import { CountriesSaveJson, CountriesUpdateJson } from "../interfaces/countries.interface";
import { CityModel } from "../models/city.model";
import { DocumentTypes } from "../../document-types/models/document.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services CountriesService
 * @Creado 20 de Abril 2019
 */

@Injectable()
export class CountriesService {
    constructor(
        @Inject('Countries') private readonly _countries: typeof Countries,
    ) { }

    async saveCountry(bodyCountries: CountriesSaveJson): Promise<CountriesUpdateJson> {

        return await new this._countries(bodyCountries).save();
    }

    async showAllCountries(): Promise<CountriesUpdateJson[]> {

        return await this._countries.findAll({
            include: [ CityModel, DocumentTypes ]
        });
    }

    async getCountry(idCountry: string): Promise<CountriesUpdateJson> {

        return await this._countries.findByPk(idCountry, {
            include: [
                {
                    model: CityModel
                }
            ]
        });
    }

    async updateCountry(id: string, bodyCountries: Partial<CountriesSaveJson>): Promise<[number, Array<CountriesUpdateJson>]> {

        return await this._countries.update(bodyCountries, {
            where: { id },
            returning: true
        });
    }

    async destroyCountry(id: string): Promise<number> {
        return await this._countries.destroy({
            where: { id },
        });
    }

}