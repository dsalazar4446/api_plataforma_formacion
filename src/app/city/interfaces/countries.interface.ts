import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsUUID } from 'class-validator';

export class CountriesSaveJson {

    @ApiModelProperty()
    @IsString({
        message: 'The countryName variable must be a string'
    })
    @IsNotEmpty({
        message: 'The countryName variable can not be null'
    })
    @ApiModelProperty()
    @IsString()
    countryName: string;

    @ApiModelProperty()
    @IsString({
        message: 'The countryAbreviation variable must be a string'
    })
    @IsNotEmpty({
        message: 'The countryAbreviation variable can not be null'
    })
    countryAbreviation: string;

    @ApiModelProperty()
    @IsString({ 
        message: 'The countryFlag variable must be a string'
    })
    @IsNotEmpty({
        message: 'The countryFlag variable can not be null'
    })
    countryFlag: string;

    @ApiModelProperty()
    @IsString({
        message: 'The countryPhoneCod variable must be a string'
    })
    @IsNotEmpty({
        message: 'The countryPhoneCod variable can not be null'
    })
    countryPhoneCode: string;
}

// tslint:disable-next-line: max-classes-per-file
export class CountriesUpdateJson extends CountriesSaveJson {

    @ApiModelProperty()
    @IsUUID('4', {
        message: 'The id variable must be a uuid'
    })
    @IsNotEmpty({
        message: 'The id variable can not be null'
    })
    id: string;
}
