import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty, IsUUID} from "class-validator";
export class CitySaveJson {

    @ApiModelProperty()
    @IsUUID('4',{
        message: "The countryId variable must be a uuid"
     })
    @IsNotEmpty({
        message: "The countryId variable can not be null"
     })
    countryId: string;
    
    @ApiModelProperty()
    @IsString({
        message: "The cityName variable must be a string"
     })
    @IsNotEmpty({
        message: "The cityName variable can not be null"
     })
    cityName: string;
}
// tslint:disable-next-line: max-classes-per-file
export class CityUpdateJson extends CitySaveJson {
    @ApiModelProperty()
    @IsUUID('4',{
        message: "The id variable must be a uuid"
     })
    @IsNotEmpty({
        message: "The id variable can not be null"
     })
    id: string;
}
