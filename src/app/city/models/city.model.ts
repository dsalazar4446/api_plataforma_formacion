import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, HasMany, BelongsTo, ForeignKey } from 'sequelize-typescript';
import { Companies } from '../../companies/models/companies.model';
import { Events } from '../../events/models/events.model';
import { Countries } from './countries.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { UserModel } from '../../user/models/user.Model';
@Table({
    tableName: 'cities',
})
export class CityModel extends Model<CityModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => Countries)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'country_id'
    })
    countryId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        field: 'city_name',
    })
    cityName: string;

    /**
     * RELACIONES
     * CityModel tiene muchos Companies
     */

    @HasMany(() => UserModel)
    user: UserModel;

    @ApiModelPropertyOptional({
        type: () => Companies,
        isArray: true,
    })
    @HasMany(() => Companies) 
    companies: Companies[];

    @ApiModelPropertyOptional({
        type: () => Events,
        isArray: true
    })
    @HasMany(() => Events)
    events: Events[];

    @BelongsTo(() => Countries)
    countries: Countries[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
