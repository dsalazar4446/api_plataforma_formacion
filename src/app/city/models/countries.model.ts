import { Table, Column, DataType, CreatedAt, UpdatedAt, Model, HasMany, BelongsToMany } from "sequelize-typescript";
import { CityModel } from "./city.model";
import { DocumentTypes } from "../../document-types/models/document.model";
import { DocumentTypesHasCountries } from "../../document-types/models/document-has-countries.model";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";
import { UserModel } from "../../user/models/user.Model";

@Table({
    tableName: 'countries',
})
export class Countries extends Model<Countries> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'country_name',
    })
    countryName: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(4),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'country_abreviation',
    })
    countryAbreviation: string; 
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'country_flag',
    })
    countryFlag: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'country_phone_code',
    })
    countryPhoneCode: string;

    @ApiModelPropertyOptional({
        type: () => CityModel,
        isArray: true
    })
    @HasMany(() => CityModel)
    cityModel: CityModel[];

    @ApiModelPropertyOptional({
        type: () => DocumentTypes
    })
    @BelongsToMany(() => DocumentTypes, {
        through: {
            model: () => DocumentTypesHasCountries,
            unique: true,
        }
    })
    documentTypes: DocumentTypes[];

    @HasMany(() => UserModel)
    user: UserModel;
    
    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}