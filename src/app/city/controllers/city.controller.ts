import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, HttpStatus, Req } from '@nestjs/common';
import { CityService } from '../services/city.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { CitySaveJson, CityUpdateJson } from '../interfaces/city.interface';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiResponse, ApiOperation, ApiUseTags, ApiImplicitParam, ApiCreatedResponse, ApiProduces, ApiImplicitHeader } from '@nestjs/swagger';
import { CityModel } from '../models/city.model';
import { Countries } from '../models/countries.model';
import { AppUtilsService } from "../../shared/services/app-utils.service";
@ApiUseTags('city')
@Controller('city')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class CityController {
    constructor(
        private readonly cityService: CityService,
        @Inject('CityModel') private readonly cityModel: typeof CityModel,
        @Inject('Countries') private readonly countries: typeof Countries,
        private readonly appUtilsService: AppUtilsService
        ) {}
        @ApiOperation({ title: 'Create city' })
        @ApiResponse({
            status: 201,
            description: 'The city has been successfully created.',
        })
        @ApiResponse({ status: 403, description: 'Forbidden.' })
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @Post()
        @ApiResponse({
            status: 200,
            type: CityModel,
          })
        async create(@Body() city: CitySaveJson, @Req() req) {
            const data = await this.countries.findByPk(city.countryId);
            if(data == null){
                throw this.appUtilsService.httpCommonError('City does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA20',
                    languageType:  req.headers.language,
                });
            }
            return await this.cityService.create(city);
        }
        // LIST SESSION
        @Get()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: CityModel,
            isArray: true
          })
        async getAll() {
            return await this.cityService.findAll();
        }
        // DETAIL SESSION
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @ApiResponse({
            status: 200,
            type: CityModel,
          })
        @Get(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async findById(@Param('id') idCity) {
            return await this.cityService.findById(idCity);
        }
        // UPDATE SESSION
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Put(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: CityModel,
          })
        async update(
            @Body()
             updateCity: Partial<CityUpdateJson>,
             @Param('id') idCity,
             @Req() req
        ) {
            const data = await this.cityModel.findByPk(idCity);
            if(data == null){
                throw this.appUtilsService.httpCommonError('City does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA21',
                    languageType:  req.headers.language,
                });
            }
            if(updateCity.countryId){
                const data2 = await this.countries.findByPk(updateCity.countryId);
                if(data2 == null){
                    throw this.appUtilsService.httpCommonError('City does not created!', HttpStatus.NOT_ACCEPTABLE, {
                        messageCode: 'EA20',
                        languageType:  req.headers.language,
                    });
                }
            }
            return await this.cityService.update(idCity, updateCity);
        }
        // DELETE SESSION
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Delete(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async delete(@Param('id') idCity) {
            return await this.cityService.deleted(idCity);
        }
}
