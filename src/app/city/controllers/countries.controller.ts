import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from "@nestjs/swagger";
import { Controller, UseFilters, UseInterceptors, Post, Body, HttpStatus, Get, Param, NotFoundException, Put, Delete } from "@nestjs/common";
import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { CountriesService } from "../services/countries.service";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { CountriesSaveJson, CountriesUpdateJson } from "../interfaces/countries.interface";
import { Countries } from "../models/countries.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller CountriesController
 * @Creado 20 de Abril 2019
 */
@ApiUseTags('Module-Cities')
@Controller('countries')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class CountriesController {

    constructor(private readonly _countriesService: CountriesService,
    private readonly appUtilsService: AppUtilsService) { }
    @ApiResponse({
        status: 200,
        type: Countries,
      })
      @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyCountry: CountriesSaveJson) {

        const createCountry = await this._countriesService.saveCountry(bodyCountry);

        if (!createCountry) {
            throw this.appUtilsService.httpCommonError('Country does not created!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM160',
                languageType: 'es',
            });
        }

        return createCountry;

    }

    @Get('details/:id')
    @ApiResponse({
        status: 200,
        type: Countries,
      })
      @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idCountry: string) {

        const fetchCountry = await this._countriesService.getCountry(idCountry);

        if (!fetchCountry) {
            throw this.appUtilsService.httpCommonError('Country does not created!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM156',
                languageType: 'es',
            });
        }

        return fetchCountry;
    }

    @Get()
    @ApiResponse({
        status: 200,
        type: Countries,
        isArray: true
      })
      @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async findByAll() {

        const fetchAll = await this._countriesService.showAllCountries();

        if (!fetchAll) {
            throw this.appUtilsService.httpCommonError('Country does not created!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM156',
                languageType: 'es',
            });
        }

        return fetchAll;
    }

    @Put(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: Countries,
      })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateCountry(@Param('id') id: string, @Body() bodyCountry: Partial<CountriesUpdateJson>) {

        const updateCountry = await this._countriesService.updateCountry(id, bodyCountry);

        if (!updateCountry) {
            throw this.appUtilsService.httpCommonError('Country does not created!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM156',
                languageType: 'es',
            });
        }

        return updateCountry;
    }

    @Delete(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteCountry(@Param('id') idCountry: string) {

        const deleteCountry = await this._countriesService.destroyCountry(idCountry);

        if (!deleteCountry) {
            throw this.appUtilsService.httpCommonError('Country does not created!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM156',
                languageType: 'es',
            });
        }

        return deleteCountry;
    }

}