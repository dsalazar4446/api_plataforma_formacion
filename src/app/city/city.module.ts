import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { CityController } from './controllers/city.controller';
import { CityService } from './services/city.service';
import { CityModel } from './models/city.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { CountriesController } from './controllers/countries.controller';
import { Countries } from './models/countries.model';
import { CountriesService } from './services/countries.service';
const models = [
  CityModel,
  Countries
];
@Module({
  imports: [
    forwardRef(() => SharedModule),
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  controllers: [CityController, CountriesController],
  providers: [CityService, CountriesService],
  exports: [CityService, CountriesService]
})
export class CityModule { }
