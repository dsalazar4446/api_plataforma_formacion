import { Module, HttpModule, Provider, forwardRef } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { LogsController } from './controllers/logs.controller';
import { Logs } from './models/logs.model';
import { LogsService } from './services/logs.service';
import { UserModule } from '../user/user.module';

const models = [Logs];
const controllers = [LogsController];
const providers: Provider[] = [LogsService];

@Module({
    imports: [
        forwardRef(() => SharedModule),
        HttpModule,
        DatabaseModule.forRoot({
            sequelize: {
                models,
            },
            loki: true,
        }),
    ],
    exports: [LogsService],
    providers,
    controllers
})

@Module({})
export class LogsModule { }


