import { Injectable, Inject } from '@nestjs/common';
import { Logs } from '../models/logs.model';
import { LogsSaveJson, LogsUpdateJson } from '../interfaces/logs.interface';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services LogsService
 * @Creado 11 de Mayo 2019
 */

@Injectable()
export class LogsService {

    constructor(
        @Inject('Logs') private readonly _logs: typeof Logs
    ) { }

    async saveLogs(bodyLogs: LogsSaveJson): Promise<LogsUpdateJson> {
        return await new this._logs(bodyLogs).save();
    }

    async showAllLogs(): Promise<LogsUpdateJson[]> {
        return await this._logs.findAll();
    }

    async getDetails(logsId: string) {
        return await this._logs.findByPk(logsId);
    }

    async updateLogs(id: string, bodyLogs: Partial<LogsUpdateJson>): Promise<[number, Array<LogsUpdateJson>]> {
        return await this._logs.update(bodyLogs, {
            where: { id },
            returning: true
        });

    }

    async destroyLogs(logsId: string): Promise<number> {
        return await this._logs.destroy({
            where: { id: logsId },
        });
    }

    async logs(userId: string, nickname: string, logMsg: string, logStatus: string){
        return await this.saveLogs({
            userId,
            nickname,
            logMsg,
            logStatus
        }).then().catch(err => err);
    }
}
