import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'logs',
})
export class Logs extends Model<Logs>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(75),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'nickname',
    })
    nickname: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('profile', 'pwd', 'wallet_create', 'wallet_update', 'wallet_delete', 'comission_request', 'comission_payment'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'log_msg',
    })
    logMsg: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'log_status',
    })
    logStatus: string;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
