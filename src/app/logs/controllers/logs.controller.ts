import { Controller, UseFilters, UseInterceptors, Post, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { LogsService } from '../services/logs.service';
import { LogsSaveJson } from '../interfaces/logs.interface';
import { Logs } from '../models/logs.model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller LogsController
 * @Creado 13 de Mayo 2019
 */

@ApiUseTags('Module-Logs')
@Controller('logs')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class LogsController {

    constructor(private readonly _logsService: LogsService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: Logs,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyLogs: LogsSaveJson) {
        const createLogs = await this._logsService.saveLogs(bodyLogs);
        if (!createLogs) {
            throw this.appUtilsService.httpCommonError('logs no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM51',
                languageType: 'es',
            });
        }
        return createLogs;
    }


    @ApiResponse({
        status: 200,
        type: Logs,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idLogs: string) {

        const fetchLogs = await this._logsService.getDetails(idLogs);

        if (!fetchLogs) {
            throw this.appUtilsService.httpCommonError('logs no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM52',
                languageType: 'es',
            });
        }
        return fetchLogs;
    }


    @ApiResponse({
        status: 200,
        type: Logs,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllLogs() {

        const fetch = await this._logsService.showAllLogs();
        if (!fetch[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return fetch;
    }



    @ApiResponse({
        status: 200,
        type: Logs,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateLogs(@Param('id') id: string, @Body() bodyLogs: Partial<LogsSaveJson>) {

        const updateLogs = await this._logsService.updateLogs(id, bodyLogs);
        if (!updateLogs[1][0]) {
            throw this.appUtilsService.httpCommonError('logs no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM52',
                languageType: 'es',
            });
        }
        return updateLogs;
    }


    @ApiResponse({
        status: 200,
        type: Logs,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteLogs(@Param('id') idLogs: string) {

        const deleteLogs = await this._logsService.destroyLogs(idLogs);
        if (!deleteLogs) {
            throw this.appUtilsService.httpCommonError('logs no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM52',
                languageType: 'es',
            });
        }
        return deleteLogs;
    }
}
