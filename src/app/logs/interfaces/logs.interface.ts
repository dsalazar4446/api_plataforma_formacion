import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString } from 'class-validator';
export class LogsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    userId: string;

    @ApiModelProperty()
    @IsString({message: 'nickname property must a be srting'})
    @IsNotEmpty({message: 'nickname property not must null'})
    nickname: string;

    @ApiModelProperty({ enum: ['profile', 'pwd', 'wallet_create', 'wallet_update', 'wallet_delete', 'comission_request', 'comission_payment']})
    @IsString({message: 'logMsg property must a be srting'})
    @IsNotEmpty({message: 'logMsg property not must null'})
    logMsg: string;

    @ApiModelProperty({ enum: ['1','2']})
    @IsString({message: 'logStatus property must a be srting'})
    @IsNotEmpty({message: 'logStatus property not must null'})
    logStatus: string;

}
export class LogsUpdateJson extends LogsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
