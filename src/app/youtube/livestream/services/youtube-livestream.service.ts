import { Injectable } from '@nestjs/common';
import { YoutubeAuthService } from './../../../shared/services/youtubeauth.service';
import { ILiveStreamSaveJson } from '../interfaces/youtube-livestream.interface';
import { CONFIG } from '../../../../config';
import {google} from 'googleapis';
@Injectable()
export class YoutubeLiveStreamService {
    youtubeUrl: string = 'https://www.googleapis.com/youtube/v3';
    apiKey: string = CONFIG.youtube.apiKey;
    channelId: string =  CONFIG.youtube.channelId;
    constructor(
        private youtubeAuthService: YoutubeAuthService,
    ) { }

    /********************* PLAYLISTS **************************/
    async createLiveStream(playList: any){
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.liveStreams.insert({
          auth,
          part: playList.part,
          requestBody: {
            cdn: {
              ingestionType: playList.requestBody.cdn.ingestionType,
              format: playList.requestBody.cdn.format,
            },
            snippet: {
              title: playList.requestBody.snippet.title,
              description: playList.requestBody.snippet.description,
            },
            status: {
              streamStatus: playList.requestBody.status.streamStatus,
            },
          }}, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response.data);
        });
      });
    }
    async deleteLiveStream(id: string) {
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.liveStreams.delete({
          auth,
          id,
          }, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response);
        });
      });
    }

    async listLiveStream(){
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.liveStreams.list({
          auth,
          part: 'id,snippet,cdn,status',
          mine: true,
        }, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response);
        });
      });
    }

    async dataLiveStream(id: string){
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.liveStreams.list({
          id,
          auth,
          part: 'id,snippet,cdn,status',
          mine: true,
        }, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response);
        });
      });
    }

    async updateLiveStream(id: string, playList: any){
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.liveStreams.update({
          auth,
          part: playList.part,
          requestBody: {
            id,
            cdn: {
              ingestionType: playList.requestBody.cdn.ingestionType,
              format: playList.requestBody.cdn.format,
            },
            snippet: {
              title: playList.requestBody.snippet.title,
              description: playList.requestBody.snippet.description,
            },
            status: {
              streamStatus: playList.requestBody.status.streamStatus,
            },
          }}, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response);
        });
      });
    }
}
