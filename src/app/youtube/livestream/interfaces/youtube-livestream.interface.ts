import { ApiModelProperty } from "@nestjs/swagger";

export class ILiveStreamSaveJson {
  @ApiModelProperty()
  part: string;
  @ApiModelProperty()
  requestBody: {
    cdn: {
      ingestionType: string,
      format: string,
    },
    snippet: {
      title: string,
      description: string,
    },
    status: {
      streamStatus: string,
    }
  }
}
