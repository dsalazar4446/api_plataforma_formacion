import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters} from '@nestjs/common';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { YoutubeLiveStreamService } from '../services/youtube-livestream.service';
import { ILiveStreamSaveJson } from '../interfaces/youtube-livestream.interface';
import { ApiImplicitParam } from '@nestjs/swagger';
@Controller('youtube')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class YoutubeLiveStreamController {
    constructor(
        private readonly youtubeLiveStreamService: YoutubeLiveStreamService,
    ) {}
    
    @Post('livestream')
    async createPlayList(@Body() playlist: ILiveStreamSaveJson) {
        return await this.youtubeLiveStreamService.createLiveStream(playlist);
    }
    @Get('livestream')
    async listPlayList() {
        return await this.youtubeLiveStreamService.listLiveStream();
    }
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Get('livestream/:id')
    async dataPlayList(@Param('id') idPlayList) {
        return await this.youtubeLiveStreamService.dataLiveStream(idPlayList);
    }
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Delete('livestream/:id')
    async deleteList(@Param('id') idPlayList) {
        return await this.youtubeLiveStreamService.deleteLiveStream(idPlayList);
    }
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Put('livestream/:id')
    async updateList(@Param('id') idPlayList, @Body() playlistItem: ILiveStreamSaveJson) {
        return await this.youtubeLiveStreamService.updateLiveStream(idPlayList,playlistItem);
    }
}
