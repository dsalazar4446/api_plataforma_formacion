import { Module, HttpModule } from '@nestjs/common';
import { SharedModule } from '../../shared/shared.module';
import { YoutubeLiveStreamService } from './services/youtube-livestream.service';
import { YoutubeAuthService } from '../../shared/services/youtubeauth.service';
import { YoutubeLiveStreamController } from './controllers/youtube-livestream.controller';
@Module({
  imports: [
    SharedModule,
    HttpModule,
  ],
  controllers: [YoutubeLiveStreamController],
  providers: [YoutubeLiveStreamService, YoutubeAuthService]
})
export class YoutubeLiveStreamModule {}
