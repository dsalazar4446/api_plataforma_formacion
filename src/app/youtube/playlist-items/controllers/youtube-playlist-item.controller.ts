import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters} from '@nestjs/common';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { YoutubePlayListItemService } from '../services/youtube-playlist-item.service';
import { IPlayListItemSaveJson } from '../interfaces/youtube-playlist-item.interface';
import { ApiImplicitParam } from '@nestjs/swagger';
@Controller('youtube')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class YoutubePlayListItemController {
    constructor(
        private readonly youtubeService: YoutubePlayListItemService,
    ) {}

    @Post('playlist-item')
    async createPlayList(@Body() playlistItem: IPlayListItemSaveJson) {
        return await this.youtubeService.createPlayListItems(playlistItem);
    }
    @Get('playlist-item/:playListId/:id')
    async dataPlayList(@Param(':playListId') playListId, @Param('id') idPlayListItem) {
        return await this.youtubeService.dataPlayListItems( playListId, idPlayListItem);
    }
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Delete('playlist-item/:id')
    async deleteList(@Param('id') idPlayList) {
        return await this.youtubeService.deletePlayListItems(idPlayList);
    }
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Put('playlist-item/:id')
    async updateList(@Param('id') idPlayList) {
        return await this.youtubeService.updatePlayListItems(idPlayList);
    }
}
