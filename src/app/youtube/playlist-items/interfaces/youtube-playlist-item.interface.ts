import { ApiModelProperty } from "@nestjs/swagger";

export class IPlayListItemSaveJson {
  @ApiModelProperty()
    part: string;
    @ApiModelProperty()
    requestBody: {
      id: string,
      snippet:{
        playlistId: string,
        resourceId: {
          playlistId: string,
          channelId: string,
          videoId: string,
        },
      },
    };
}
