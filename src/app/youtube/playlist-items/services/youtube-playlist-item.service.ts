import { Injectable, Inject } from '@nestjs/common';
import { YoutubeAuthService } from './../../../shared/services/youtubeauth.service';
import { IPlayListItemSaveJson } from '../interfaces/youtube-playlist-item.interface';
import { CONFIG } from '../../../../config';
import {google} from 'googleapis';

@Injectable()
export class YoutubePlayListItemService {
    youtubeUrl: string = 'https://www.googleapis.com/youtube/v3';
    apiKey: string = CONFIG.youtube.apiKey;
    channelId: string =  CONFIG.youtube.channelId;
    constructor(
        private youtubeAuthService: YoutubeAuthService,
    ) { }

    /********************* PLAYLISTS **************************/
    async createPlayListItems(playListItems: IPlayListItemSaveJson){
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.playlistItems.insert({
          auth,
          part: playListItems.part,
          requestBody: {
            id: playListItems.requestBody.id,
            snippet: {
              playlistId: playListItems.requestBody.snippet.playlistId,
              resourceId: {
                playlistId: playListItems.requestBody.snippet.resourceId.playlistId,
                channelId: playListItems.requestBody.snippet.resourceId.channelId,
                videoId: playListItems.requestBody.snippet.resourceId.videoId,
                kind: 'youtube#video',
              }
            }
          },
        }, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response.data);
        });
      });
    }
    async deletePlayListItems(id: string) {
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.playlistItems.delete({
          auth,
          id,
          }, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response);
        });
      });
    }

    async dataPlayListItems(playListId: string, id: string) {
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.playlistItems.list({
          id,
          auth,
          part: 'snippet',
          playlistId: playListId,
        }, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response);
        });
      });
    }

    async updatePlayListItems(playListItems: IPlayListItemSaveJson){
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.playlistItems.update({
          auth,
          part: playListItems.part,
          requestBody: {
            id: playListItems.requestBody.id,
            snippet: {
              playlistId: playListItems.requestBody.snippet.playlistId,
              resourceId: {
                playlistId: playListItems.requestBody.snippet.resourceId.playlistId,
                channelId: playListItems.requestBody.snippet.resourceId.channelId,
                videoId: playListItems.requestBody.snippet.resourceId.videoId,
                kind: 'youtube#video',
              }
            }
          },
        }, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response);
        });
      });
    }
}
