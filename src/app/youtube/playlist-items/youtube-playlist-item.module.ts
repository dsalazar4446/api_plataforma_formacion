import { Module, HttpModule } from '@nestjs/common';
import { SharedModule } from '../../shared/shared.module';
import { YoutubePlayListItemController } from './controllers/youtube-playlist-item.controller';
import { YoutubeAuthService } from '../../shared/services/youtubeauth.service';
import { YoutubePlayListItemService } from './services/youtube-playlist-item.service';
@Module({
  imports: [
    SharedModule,
    HttpModule,
  ],
  controllers: [YoutubePlayListItemController],
  providers: [YoutubePlayListItemService, YoutubeAuthService]
})
export class YoutubePlayListItemModule {}
