import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters} from '@nestjs/common';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { YoutubePlayListService } from '../services/youtube-playlist.service';
import { IPlayListSaveJson } from '../interfaces/youtube-playlist.interface';
import { ApiImplicitParam } from '@nestjs/swagger';
@Controller('youtube')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class YoutubeController {
    constructor(
        private readonly youtubePlayListService: YoutubePlayListService,
    ) {}
    @Post('playlist')
    async createPlayList(@Body() playlist: IPlayListSaveJson) {
        return await this.youtubePlayListService.createPlayList(playlist);
    }
    @Get('playlist')
    async listPlayList() {
        return await this.youtubePlayListService.listPlayList();
    }
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Get('playlist/:id')
    async dataPlayList(@Param('id') idPlayList) {
        return await this.youtubePlayListService.dataPlayList(idPlayList);
    }
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Delete('playlist/:id')
    async deleteList(@Param('id') idPlayList) {
        return await this.youtubePlayListService.deletePlayList(idPlayList);
    }
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Put('playlist/:id')
    async updateList(@Param('id') idPlayList, @Body() playlistItem: IPlayListSaveJson) {
        return await this.youtubePlayListService.updatePlayList(idPlayList,playlistItem);
    }
}
