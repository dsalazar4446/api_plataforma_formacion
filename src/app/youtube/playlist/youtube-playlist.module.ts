import { Module, HttpModule } from '@nestjs/common';
import { SharedModule } from '../../shared/shared.module';
import { YoutubePlayListService } from './services/youtube-playlist.service';
import { YoutubeController } from './controllers/youtube-playlist.controller';
import { YoutubeAuthService } from '../../shared/services/youtubeauth.service';
@Module({
  imports: [
    SharedModule,
    HttpModule,
  ],
  controllers: [YoutubeController],
  providers: [YoutubePlayListService, YoutubeAuthService]
})
export class YoutubePlayListModule {}
