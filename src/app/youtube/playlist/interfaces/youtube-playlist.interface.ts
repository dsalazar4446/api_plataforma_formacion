import { ApiModelProperty } from "@nestjs/swagger";

export class IPlayListSaveJson {
  @ApiModelProperty()
  part: string;
  @ApiModelProperty()
  requestBody: {
    snippet: {
      title: string,
      description: string,
    },
    status: {
      privacyStatus: string,
    },
    etag: string,
  }
}
