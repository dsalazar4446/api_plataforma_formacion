import { Injectable } from '@nestjs/common';
import { YoutubeAuthService } from './../../../shared/services/youtubeauth.service';
import { IPlayListSaveJson } from '../interfaces/youtube-playlist.interface';
import { CONFIG } from '../../../../config';
import {google} from 'googleapis';
import { stringify } from 'querystring';
@Injectable()
export class YoutubePlayListService {
    youtubeUrl: string = 'https://www.googleapis.com/youtube/v3';
    apiKey: string = CONFIG.youtube.apiKey;
    channelId: string =  CONFIG.youtube.channelId;
    constructor(
        private youtubeAuthService: YoutubeAuthService,
    ) { }

    /********************* PLAYLISTS **************************/
    async createPlayList(playList: IPlayListSaveJson){
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.playlists.insert({
          auth,
          part: playList.part,
          requestBody: {
            snippet: {
              title: playList.requestBody.snippet.title,
              description: playList.requestBody.snippet.description,
            },
            status: {
              privacyStatus: playList.requestBody.status.privacyStatus,
            },
            etag: playList.requestBody.etag,
          }}, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response.data);
        });
      });
    }
    async deletePlayList(id: string) {
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.playlists.delete({
          auth,
          id,
          }, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response);
        });
      });
    }

    async listPlayList(){
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.playlists.list({
          auth,
          part: 'snippet',
          channelId: this.channelId,
          maxResults: 10
        }, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response);
        });
      });
    }

    async dataPlayList(id: string){
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.playlists.list({
          id,
          auth,
          part: 'snippet',
          channelId: this.channelId,
        }, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response);
        });
      });
    }

    async updatePlayList(id: string, playList: IPlayListSaveJson){
      return new Promise( async (resolve,reject)=>{
        const auth: any = await this.youtubeAuthService.auth();
        const service = google.youtube('v3');
        service.playlists.update({
          auth,
          part: playList.part,
          requestBody: {
            id,
            snippet: {
              title: playList.requestBody.snippet.title,
              description: playList.requestBody.snippet.description,
            },
            status: {
              privacyStatus: playList.requestBody.status.privacyStatus,
            },
            etag: playList.requestBody.etag,
          }}, (err, response) => {
          if (err) {
            reject(err);
          }
          resolve(response);
        });
      });
    }
}
