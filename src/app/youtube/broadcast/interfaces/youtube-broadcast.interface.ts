import { ApiModelProperty } from "@nestjs/swagger";

export class IBroadcastSaveJson {
  @ApiModelProperty()
  part: string;
  @ApiModelProperty()
  requestBody:{
    snippet: {
      scheduledStartTime: string,
      title: string,
      isDefaultBroadcast: boolean,
      description: string,
    },
    status: {
      privacyStatus: string,
    },
    etag: string,
  };
}
