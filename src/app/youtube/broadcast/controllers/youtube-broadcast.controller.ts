import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters} from '@nestjs/common';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { YoutubeBroadCastService } from '../services/youtube-broadcast.service';
import { IBroadcastSaveJson } from '../interfaces/youtube-broadcast.interface';
import { ApiImplicitParam } from '@nestjs/swagger';
@Controller('youtube')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class YoutubeBroadCastController {
    constructor(
        private readonly youtubeBroadCastService: YoutubeBroadCastService,
    ) {}
    
    @Post('broadcast')
    async createPlayList(@Body() playlist: IBroadcastSaveJson) {
        return await this.youtubeBroadCastService.createBroadCast(playlist);
    }
    @ApiImplicitParam({ name: 'status', required: true, type: 'string' })
    @ApiImplicitParam({ name: 'type', required: true, type: 'string' })
    @Get('broadcast/:status/:type')
    async listPlayList(@Param('status') status, @Param('type') type) {
        return await this.youtubeBroadCastService.listBroadCast(status,type);
    }
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Get('broadcast/:id')
    async dataPlayList(@Param('id') idPlayList) {
        return await this.youtubeBroadCastService.dataBroadCast(idPlayList);
    }
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Delete('broadcast/:id')
    async deleteList(@Param('id') idPlayList) {
        return await this.youtubeBroadCastService.deleteBroadCast(idPlayList);
    }
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Put('broadcast/:id')
    async updateList(@Param('id') idPlayList, @Body() playlistItem: IBroadcastSaveJson) {
        return await this.youtubeBroadCastService.updateBroadCast(idPlayList,playlistItem);
    }
}
