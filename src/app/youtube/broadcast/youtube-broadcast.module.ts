import { Module, HttpModule } from '@nestjs/common';
import { SharedModule } from '../../shared/shared.module';
import { YoutubeBroadCastService } from './services/youtube-broadcast.service';
import { YoutubeBroadCastController } from './controllers/youtube-broadcast.controller';
import { YoutubeAuthService } from '../../shared/services/youtubeauth.service';
@Module({
  imports: [
    SharedModule,
    HttpModule,
  ],
  controllers: [YoutubeBroadCastController],
  providers: [YoutubeBroadCastService, YoutubeAuthService]
})
export class YoutubeBroadCastModule {}
