import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { UserLocationController } from './controllers/user-location.controller';
import { UserLocationService } from './services/user-location.service';
import { UserLocationModel } from './models/user-location.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
const models = [
  UserLocationModel,
];
@Module({
  imports: [
    forwardRef(() => SharedModule),
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [UserLocationController],
  providers: [UserLocationService],
  exports: [UserLocationService]
})
export class UserLocationModule {}
