import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey, HasMany, BelongsTo } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { CityModel } from '../../city/models/city.model';
import { CreexPayCards } from '../../creex-pay-card/models/creex-pay-cards.model';
import { ApiModelProperty } from '@nestjs/swagger';

// Como aun no se dispone del modulo citie no se pudo agregar el foreign key con cities
@Table({
    tableName: 'user_locations',
})
export class UserLocationModel extends Model<UserLocationModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'user_id',
    })
    userId: string;
    @ApiModelProperty()
    @ForeignKey(() => CityModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'city_id',
    })
    cityId: string;
    @ApiModelProperty()
    @BelongsTo(() => CityModel)
    City: CityModel;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(128),
        allowNull: false,
        field: 'latitude',
    })
    latitude: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(128),
        allowNull: false,
        field: 'longitude',
    })
    longitude: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        field: 'address_1',
    })
    address1: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        field: 'address_2',
    })
    address2: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'phone',
    })
    phone: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        field: 'user_location_reference',
    })
    userLocationReference: string;
    @HasMany(() => CreexPayCards)
    creexPayCards: CreexPayCards[];
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
