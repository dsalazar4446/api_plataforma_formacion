import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, Req, HttpStatus} from '@nestjs/common';
import { UserLocationService } from '../services/user-location.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { UserLocationSaveJson, UserLocationUpdateJson } from '../interfaces/user-location.interface';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { UserLocationModel } from '../models/user-location.model';
import { UserModel } from '../../user/models/user.Model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
@Controller('user-location')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class UserLocationController {
    constructor(
        private readonly userLocationService: UserLocationService,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('UserLocationModel') private readonly userLocationModel: typeof UserLocationModel,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        @ApiResponse({
            status: 200,
            type: UserLocationModel,
          })
          @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @Post()
        async create(@Body() userLocation: UserLocationSaveJson, @Req() req) {
          const data3 = await this.userModel.findByPk(userLocation.userId);
          if(data3 == null){
              throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
                  messageCode: 'EA4',
                  languageType:  req.headers.language,
              });
            }
            return await this.userLocationService.create(userLocation);
        }
        @ApiResponse({
            status: 200,
            type: UserLocationModel,
            isArray: true
          })
        @Get()
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        async getAll() {
            return await this.userLocationService.findAll();
        } 
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @ApiResponse({
            status: 200,
            type: UserLocationModel,
          })
        @Get(':id')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        async findById(@Param('id') idUserLocation) {
            return await this.userLocationService.findById(idUserLocation);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Put(':id')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        @ApiResponse({
            status: 200,
            type: UserLocationModel,
          })
        async update(
            @Body()
            updateUserLocation: Partial<UserLocationUpdateJson>,
             @Param('id') idUserLocation,
             @Req() req
            ) {
              const data3 = await this.userModel.findByPk(updateUserLocation.userId);
              if(data3 == null){
                throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA4',
                    languageType:  req.headers.language,
                });
              }
              const data2 = await this.userLocationModel.findByPk(idUserLocation);
              if(data2 == null){
                throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA11',
                    languageType:  req.headers.language,
                });
              }
            return await this.userLocationService.update(idUserLocation, updateUserLocation);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Delete(':id')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        async delete(@Param('id') idUserLocation) {
            return await this.userLocationService.deleted(idUserLocation);
        }
}
