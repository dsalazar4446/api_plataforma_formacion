import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class UserLocationSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    userId: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    cityId: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    latitude: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    longitude: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    address1: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    address2: string;
    @IsString()
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    phone: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    userLocationReference: string;
}

// tslint:disable-next-line: max-classes-per-file
export class UserLocationUpdateJson extends UserLocationSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    id: string;
}
