import { Injectable, Inject } from '@nestjs/common';
import { UserLocationSaveJson, UserLocationUpdateJson } from '../interfaces/user-location.interface';
import { UserLocationModel } from '../models/user-location.model';
import { CityModel } from '../../city/models/city.model';

@Injectable()
export class UserLocationService {
    constructor(
        @Inject('UserLocationModel') private readonly userLocationModel: typeof UserLocationModel,
    ) { }
    async findAll(): Promise<UserLocationModel[]> {
        return  await this.userLocationModel.findAll({
            include:[
                {
                    model: CityModel
    
                }
            ]
        });
    }
    async findById(id: string): Promise<UserLocationModel> {
        return await this.userLocationModel.findById<UserLocationModel>(id);
    }
    async create(userLocation: UserLocationSaveJson): Promise<UserLocationModel> {
        return await this.userLocationModel.create<UserLocationModel>(userLocation, {
            returning: true,
        });
    }
    async update(idUserLocation: string, userLocationUpdate: Partial<UserLocationUpdateJson>){
        return  await this.userLocationModel.update(userLocationUpdate, {
            where: {
                id: idUserLocation,
            },
            returning: true,
        });
    }
    async findByUserId(userId){
        return this.userLocationModel.findOne({
            where: {
                userId
            }
        })
    }
    async deleted(idUserLocation: string): Promise<any> {
        return await this.userLocationModel.destroy({
            where: {
                id: idUserLocation,
            },
        });
    }
}
