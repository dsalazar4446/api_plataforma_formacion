import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey, BelongsTo} from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { InboxModel } from '../../inbox/models/inbox.model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'inbox_recievers',
})
export class InboxRecieversModel extends Model<InboxRecieversModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'user_id',
    })
    userId: string;
    @ApiModelProperty()
    @ForeignKey(() => InboxModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'inbox_id',
    })
    inboxId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        field: 'viewed',
    })
    viewed: string;

    @BelongsTo(() => UserModel)
    UserModel: UserModel;

    @BelongsTo(() => InboxModel)
    InboxModel: InboxModel;
    
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
