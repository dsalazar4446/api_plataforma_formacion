import { Injectable, Inject } from '@nestjs/common';
import { InboxRecieversSaveJson, InboxRecieversUpdateJson } from '../interfaces/inbox-recievers.interface';
import { InboxRecieversModel } from '../models/inbox-recievers.model';
import { InboxModel } from '../../inbox/models/inbox.model';
@Injectable() 
export class InboxRecieversService {
    constructor(
        @Inject('InboxRecieversModel') private readonly inboxRecieversModel: typeof InboxRecieversModel
    ) { }

// tslint:disable-next-line: max-line-length
    async findAll(): Promise<any> {
        return await this.inboxRecieversModel.findAll();
    }

    async findById(id: string): Promise<InboxRecieversModel> {
        return await this.inboxRecieversModel.findById<InboxRecieversModel>(id,{
            include: [
                {
                    model: InboxModel
                }
            ]
        });
    }

    async create(inbox: InboxRecieversSaveJson): Promise<InboxRecieversModel> {
        return await this.inboxRecieversModel.create<InboxRecieversModel>(inbox, {
            returning: true,
        });
    }
    async update(idInbox: string, inboxUpdate: Partial<InboxRecieversUpdateJson>){
        return   await this.inboxRecieversModel.update(inboxUpdate, {
            where: {
                id: idInbox,
            },
            returning: true,
        });
    }
    async deleted(idInbox: string): Promise<any> {
        return await this.inboxRecieversModel.destroy({
            where: {
                id: idInbox,
            },
        });
    }
}
