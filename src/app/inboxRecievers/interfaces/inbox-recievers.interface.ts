import { ApiModelProperty } from '@nestjs/swagger';
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class InboxRecieversSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    inboxId: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    userId: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    viewed: string;
}
// tslint:disable-next-line: max-classes-per-file
export class InboxRecieversUpdateJson extends InboxRecieversSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    id: string;
}