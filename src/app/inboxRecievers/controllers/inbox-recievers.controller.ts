import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, Req, HttpStatus } from '@nestjs/common';
import { InboxRecieversService } from '../services/inbox-recievers.service';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { InboxRecieversSaveJson, InboxRecieversUpdateJson } from '../interfaces/inbox-recievers.interface';
import { JwtAuthGuard } from '../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { InboxRecieversModel } from '../models/inbox-recievers.model';
import { UserModel } from '../../user/models/user.Model';
import { InboxModel } from '../../inbox/models/inbox.model';
import { AppUtilsService } from '../../shared/services/app-utils.service';

@ApiUseTags('Module-Inbox-Recievers')
@Controller('inbox-recievers')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class InboxRecieversController {
    constructor(
        private readonly inboxRecieversService: InboxRecieversService,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('InboxModel') private readonly inboxModel: typeof InboxModel,
        @Inject('InboxRecieversModel') private readonly inboxRecieversModel: typeof InboxRecieversModel,
        private readonly appUtilsService: AppUtilsService,
    ) { }
    @ApiResponse({
        status: 200,
        type: InboxRecieversModel,
      })
    @Post()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async create(@Body() inbox: InboxRecieversSaveJson, @Req() req) {
        const data2 = await this.userModel.findByPk(inbox.userId);
        if (data2 == null) {
            throw this.appUtilsService.httpCommonError('Parent not exists!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA3',
                languageType: req.headers.language,
            });
        }

        const data3 = await this.inboxModel.findByPk(inbox.inboxId);
        if (data3 == null) {
            throw this.appUtilsService.httpCommonError('Parent not exists!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA31',
                languageType: req.headers.language,
            });
        }
        return await this.inboxRecieversService.create(inbox);
    }
    @ApiResponse({
        status: 200,
        type: InboxRecieversModel,
        isArray: true
      })
    @Get()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async getAll() {
        return await this.inboxRecieversService.findAll();
    }
    @Get(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: InboxRecieversModel,
      })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idInbox) {
        return await this.inboxRecieversService.findById(idInbox);
    }
    @Put(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: InboxRecieversModel,
      })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(
        @Body()
        updateInbox: Partial<InboxRecieversUpdateJson>,
        @Param('id') idInbox,
        @Req() req
    ) {

        if(updateInbox.userId){
            const data2 = await this.userModel.findByPk(updateInbox.userId);
            if (data2 == null) {
                throw this.appUtilsService.httpCommonError('Parent not exists!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA3',
                    languageType: req.headers.language,
                });
            }
        }
        if(updateInbox.inboxId){
            const data3 = await this.inboxModel.findByPk(updateInbox.inboxId);
            if (data3 == null) {
                throw this.appUtilsService.httpCommonError('Parent not exists!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA31',
                    languageType: req.headers.language,
                });
            }
        }
        const data4 = await this.inboxRecieversModel.findByPk(idInbox);
        if (data4 == null) {
            throw this.appUtilsService.httpCommonError('Parent not exists!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA32',
                languageType: req.headers.language,
            });
        }
        return await this.inboxRecieversService.update(idInbox, updateInbox);
    }
    @Delete(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') idInbox) {
        return await this.inboxRecieversService.deleted(idInbox);
    }
}
