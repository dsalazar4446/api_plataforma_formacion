import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { InboxRecieversController } from './controllers/inbox-recievers.controller';
import { InboxRecieversService } from './services/inbox-recievers.service';
import { InboxRecieversModel } from './models/inbox-recievers.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { InboxModel } from '../inbox/models/inbox.model';

const models = [
  InboxRecieversModel,
  InboxModel
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [InboxRecieversController],
  providers: [InboxRecieversService]
})
export class InboxRecieversModule {}
