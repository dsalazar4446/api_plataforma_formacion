import { Controller, UseInterceptors, UseFilters, UseGuards, Post, UsePipes, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';
import { ApiLogsService } from '../../services/api-logs/api-logs.service';
import { CreateApiLogsDto, UpdateApiLogsDto } from '../../dto/api-logs.dto';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { ApiLogsModel } from '../../models/api-logs.model';

@ApiUseTags('Support')
@Controller('support/api-logs')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // // @UseGuards(new JwtAuthGuard())
export class ApiLogsController {
    constructor(private readonly apiLogsService: ApiLogsService) { }

    @Post()
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: ApiLogsModel})
    async create(@Body() body: CreateApiLogsDto) {
        return this.apiLogsService.create(body);
    }

    @Get()
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: ApiLogsModel, isArray: true})
    async list() {
        return this.apiLogsService.list();
    }

    @Get(':id')
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: ApiLogsModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detail(@Param('id') id) {
        return this.apiLogsService.detail(id);
    }

    @Put(':id')
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: ApiLogsModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(@Param('id') id: string, @Body() body: Partial<UpdateApiLogsDto>) {
        return await this.apiLogsService.update(id, body);
    }

    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') id: string) {
        return this.apiLogsService.delete(id);
    }
}
