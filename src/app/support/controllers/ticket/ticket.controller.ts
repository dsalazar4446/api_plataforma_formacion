import { Controller, UseFilters, UseInterceptors, UseGuards, UsePipes, Post, Body, Get, Param, Put, Delete, Req } from '@nestjs/common';
import { TicketService } from '../../services/ticket/ticket.service';
import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';
import { UpdateTicketDto, CreateTicketDto } from '../../dto/tickets.dto';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { TicketModel } from '../../models/tickets.model';

@ApiUseTags('Support')
@Controller('support/ticket')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // // @UseGuards(new JwtAuthGuard())
export class TicketController {
    constructor(private readonly ticketService: TicketService) {}

    @Post()
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: TicketModel})
    async create(@Body() body: CreateTicketDto, @Req() req) {
        body.languageType = req.headers.language
        return await this.ticketService.create(body);
    }
    @Get()
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: TicketModel, isArray: true})
    async list() {
        return await this.ticketService.list();
    }
    @Get(':id')
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: TicketModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detail(@Param('id') id) {
        return await this.ticketService.detail(id);
    }
    @Put(':id')
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: TicketModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(@Body()
        body: Partial<UpdateTicketDto>,
        @Param('id') id,
        @Req() req,
        ) {
            body.languageType = req.headers.language
        return await this.ticketService.update(id, body);
    }
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') id) {
        return await this.ticketService.delete(id);
    }
}
