
import { Controller, UseInterceptors, UseFilters, UseGuards, Post, UsePipes, Body, Get, Param, Put, Delete, Req } from '@nestjs/common';
import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';
import { UpdateApiMessagesDto, CreateApiMessagesDto } from '../../dto/api-messages.dto';
import { ApiMessagesService } from '../../services/api-messages/api-messages.service';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import ApiMessageModel from '../../../shared/models/sequelize/api-message.model';

@ApiUseTags('Support')
@Controller('api-messages')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // // @UseGuards(new JwtAuthGuard())
export class ApiMessagesController {
    constructor(private readonly apiMessagesService: ApiMessagesService) { }

    @Post()
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({status: 200,type: ApiMessageModel})
    async create(@Body() body: CreateApiMessagesDto, @Req() req) {
        body.languageType = req.headers.language
        return this.apiMessagesService.create(body);
    }

    @Get()
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({ status: 200, type: ApiMessageModel, isArray: true})
    async list() {
        return this.apiMessagesService.list();
    }

    @Get(':id')
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({ status: 200, type: ApiMessageModel})
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    async detail(@Param('id') id) {
        return this.apiMessagesService.detail(id);
    }

    @Put(':id')
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({ status: 200, type: ApiMessageModel})
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateApiMessagesDto>, @Req() req) {
        body.languageType = req.headers.language
        return await this.apiMessagesService.update(id, body);
    }

    @Delete(':id')
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    async delete(@Param('id') id: string) {
        return this.apiMessagesService.delete(id);
    }
}
