import { Injectable, Inject } from '@nestjs/common';
import { TicketModel } from '../../models/tickets.model';
import { CreateTicketDto, UpdateTicketDto } from '../../dto/tickets.dto';

@Injectable()
export class TicketService {
    constructor(@Inject('TicketModel') private readonly ticket: typeof TicketModel) {}

    async create(body: CreateTicketDto){
        const createTicket = new TicketModel(body)
        return await createTicket.save();
    }

    async list(){
        return await this.ticket.findAll();
    }

    async detail(id: string) {
        return await this.ticket.find({where:{id}});
    }

    async update(id: string, body: Partial<UpdateTicketDto>) {
        return await this.ticket.update(body, {where:{id}});
    }

    async delete(id: string) {
        return await this.ticket.destroy({where: {id}});
    }

}
