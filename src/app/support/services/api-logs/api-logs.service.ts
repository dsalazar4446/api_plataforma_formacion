import { Injectable, Inject } from '@nestjs/common';
import { ApiLogsModel } from '../../models/api-logs.model';
import { CreateApiLogsDto, UpdateApiLogsDto } from '../../dto/api-logs.dto';

@Injectable()
export class ApiLogsService {
    constructor(@Inject('ApiLogsModel') private readonly apiMessages: typeof ApiLogsModel) { }

    async create(body: CreateApiLogsDto) {
        const createApiLogs = new ApiLogsModel(body)
        return await createApiLogs.save();
    }

    async list() {
        return await this.apiMessages.findAll();
    }

    async detail(id: string) {
        return await this.apiMessages.find({ where: { id } });
    }

    async update(id: string, body: Partial<UpdateApiLogsDto>) {
        return await this.apiMessages.update(body, { where: { id } });
    }

    async delete(id: string) {
        return await this.apiMessages.destroy({ where: { id } });
    }
}
