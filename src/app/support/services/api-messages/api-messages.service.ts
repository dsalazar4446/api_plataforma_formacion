import { Injectable, Inject } from '@nestjs/common';
import { CreateApiMessagesDto, UpdateApiMessagesDto } from '../../dto/api-messages.dto';
import  ApiMessageModel  from '../../../shared/models/sequelize/api-message.model';

@Injectable()
export class ApiMessagesService {

    constructor(@Inject('ApiMessageModel') private readonly apiMessages: typeof ApiMessageModel) { }

    async create(body: CreateApiMessagesDto) {
        const createApiMessages = new ApiMessageModel(body)
        return await createApiMessages.save();
    }

    async list() {
        return await this.apiMessages.findAll();
    }

    async detail(id: string) {
        return await this.apiMessages.find({ where: { id } });
    }

    async update(id: string, body: Partial<UpdateApiMessagesDto>) {
        return await this.apiMessages.update(body, { where: { id }, returning: true });
    }

    async delete(id: string) {
        return await this.apiMessages.destroy({ where: { id } });
    }

}
