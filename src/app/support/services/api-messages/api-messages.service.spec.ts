import { Test, TestingModule } from '@nestjs/testing';
import { ApiMessagesService } from './api-messages.service';

describe('ApiMessagesService', () => {
  let service: ApiMessagesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ApiMessagesService],
    }).compile();

    service = module.get<ApiMessagesService>(ApiMessagesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
