import { Module } from '@nestjs/common';
import { TicketService } from './services/ticket/ticket.service';
import { TicketController } from './controllers/ticket/ticket.controller';
import { ApiMessagesController } from './controllers/api-messages/api-messages.controller';
import { ApiMessagesService } from './services/api-messages/api-messages.service';
import { ApiLogsController } from './controllers/api-logs/api-logs.controller';
import { ApiLogsService } from './services/api-logs/api-logs.service';
import { ApiLogsModel } from './models/api-logs.model';
import { TicketModel } from './models/tickets.model';
import { DatabaseModule } from '../database/database.module';
import ApiMessageModel  from '../shared/models/sequelize/api-message.model';
const models = [
  ApiLogsModel,
  TicketModel,
  ApiMessageModel
]

@Module({
  imports:[
    DatabaseModule.forRoot({
      sequelize: {
        models,
      }
    })
  ],
  providers: [TicketService, ApiMessagesService, ApiLogsService],
  controllers: [TicketController, ApiMessagesController, ApiLogsController]
})
export class SupportModule {}
