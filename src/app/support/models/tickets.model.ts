import { Model, Table, PrimaryKey, AutoIncrement, AllowNull, Validate, Unique, Column, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt } from "sequelize-typescript";
import { UserModel } from "../../user/models/user.Model";
import { RoleViewsModel } from "../../roles/models/role-views.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";
@Table({tableName: 'tickets'})
export class TicketModel extends Model<TicketModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => RoleViewsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'role_views_id',
    })
    roleViewsId: string;

    @BelongsTo(() => RoleViewsModel)
    roleViews: RoleViewsModel;
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'users_id',
    })
    usersId: string;

    @BelongsTo(() => UserModel)
    user: UserModel;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'ticket_description',
    })
    ticketDescription: string;

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;
}