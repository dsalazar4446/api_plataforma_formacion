import { Model, Table, PrimaryKey, AutoIncrement, AllowNull, Validate, Unique, Column, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt } from "sequelize-typescript";
import { UserModel } from "../../user/models/user.Model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";
@Table({tableName: 'api_logs'})
export class ApiLogsModel extends Model<ApiLogsModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;

    @BelongsTo(() => UserModel)
    user: UserModel;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'nickname',
    })
    nickname: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'log_msg',
    })
    logMsg: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2'),
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'log_status',
    })
    logStatus: string;

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;
}