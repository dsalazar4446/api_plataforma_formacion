import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString, IsOptional } from "class-validator";

export class CreateTicketDto {
    @ApiModelProperty()
    @IsUUID('4')
    roleViewsId: string;
    @ApiModelProperty()
    @IsUUID('4')
    usersId: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    languageType: string;
    @ApiModelProperty()
    @IsString()
    ticketDescription: string;
}

export class UpdateTicketDto extends CreateTicketDto {
    @ApiModelProperty()
    @IsUUID('4')
    id: string
}