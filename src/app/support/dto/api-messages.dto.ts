import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsUUID, IsOptional } from "class-validator";

export class CreateApiMessagesDto {
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    languageType: string;
    @ApiModelProperty()
    @IsString()
    messageCode: string;
    @ApiModelProperty()
    @IsString()
    messageText: string;
}

export class UpdateApiMessagesDto extends CreateApiMessagesDto{
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}