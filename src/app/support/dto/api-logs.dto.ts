import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString } from "class-validator";

export class CreateApiLogsDto {
    @ApiModelProperty()
    @IsUUID('4')
    categoriesId: string;
    @ApiModelProperty()
    @IsString()
    nickname: string;
    @ApiModelProperty()
    @IsString()
    logMsg: string;
    @ApiModelProperty()
    @IsString()
    logStatus: string;
}
export class UpdateApiLogsDto extends CreateApiLogsDto {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}