import {IsUUID, IsBoolean, IsNumber, IsString, IsNotEmpty, IsOptional} from "class-validator";
import { ApiModelProperty } from '@nestjs/swagger';
export class PromotionAttachmentsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'promotionId property must a be uuid'})
    @IsNotEmpty({message: 'promotionId property not must null'})
    promotionId: string;

    @ApiModelProperty()
    @IsString({message: 'promotionAttachment property must a be string'})
    @IsOptional()
    promotionAttachment?: string;

    @ApiModelProperty()
    @IsString({message: 'promotionAttachmentType property must a be string'})
    @IsNotEmpty({message: 'promotionAttachmentType property not must null'})
    promotionAttachmentType: string;
}
export class PromotionAttachmentsUpdateJson extends PromotionAttachmentsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
