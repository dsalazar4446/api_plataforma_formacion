import {IsUUID, IsBoolean, IsNumber, IsString, IsNotEmpty} from "class-validator";
import { ApiModelProperty } from '@nestjs/swagger';
export class PromotionsHasTransactionsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'promotionId property must a be uuid'})
    @IsNotEmpty({message: 'promotionId property not must null'})
    promotionId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'promotionId property must a be uuid'})
    @IsNotEmpty({message: 'promotionId property not must null'})
    billingTransactionId: string;
}
export class PromotionsHasTransactionsUpdateJson extends PromotionsHasTransactionsSaveJson{
    
    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
