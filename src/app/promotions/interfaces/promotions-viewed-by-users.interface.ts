import {IsUUID, IsBoolean, IsNumber, IsString, IsNotEmpty} from "class-validator";
import { ApiModelProperty } from '@nestjs/swagger';
export class PromotionsViewedByUsersSaveJson {
    
    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    userId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'promotionId property must a be uuid'})
    @IsNotEmpty({message: 'promotionId property not must null'})
    promotionId: string;
}
export class PromotionsViewedByUsersUpdateJson extends PromotionsViewedByUsersSaveJson{
    
    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
