import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsUUID, IsBoolean, IsNumber, IsNotEmpty, IsDate } from 'class-validator';
export class PromotionsSaveJson {

    @ApiModelProperty()
    promotionCode?: string;

    @ApiModelProperty()
    @IsString({message: 'promotionType property must a be string'})
    @IsNotEmpty({message: 'promotionType property not must null'})
    promotionType: string;

    @ApiModelProperty()
    @IsNumber(null,{message: 'promotionPercentageDiscount property must a be number'})
    @IsNotEmpty({message: 'promotionPercentageDiscount property not must null'})
    promotionPercentageDiscount: number;

    @ApiModelProperty()
    @IsNumber(null,{message: 'promotionAvailableCant property must a be number'})
    @IsNotEmpty({message: 'promotionAvailableCant property not must null'})
    promotionAvailableCant: number;
    
    @ApiModelProperty()
    @IsDate()
    promotionValidTill: Date

    @ApiModelProperty()
    @IsBoolean({message: 'promotionStatus property must a be boolean'})
    @IsNotEmpty({message: 'promotionStatus property not must null'})
    promotionStatus: boolean;
}
export class PromotionsUpdateJson extends PromotionsSaveJson{
    
    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
