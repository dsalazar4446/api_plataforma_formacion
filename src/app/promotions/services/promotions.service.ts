import { Injectable, Inject } from "@nestjs/common";
import { Promotions } from "../models/promotions.model";
import { PromotionAttachments } from "../models/promotion-attachments.model";
import { PromotionsUpdateJson, PromotionsSaveJson } from "../interfaces/promotions.interface";
import { UserModel } from "../../user/models/user.Model";
import { BillingTransactionsModel } from "../../transactions/models/billing-transactions.model";
import * as ShortUniqueId from 'short-unique-id';
const uid = new ShortUniqueId();

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller PromotionsService
 * @Creado 04 de Marzo 2019
 */

@Injectable()
export class PromotionsService {

    private promotionType = ['1', '2', '3', '4'];
    private include = {
        include: [
            {
                model: PromotionAttachments
            },
            {
                model: UserModel
            },
            {
                model: BillingTransactionsModel
            }
        ]
    }

    constructor(
        @Inject('Promotions') private readonly _promotions: typeof Promotions
    ) { }

    async savePromotions(bodyPromotions: PromotionsSaveJson): Promise<PromotionsUpdateJson> {

        bodyPromotions.promotionCode = uid.randomUUID(9);

        return await new this._promotions(bodyPromotions).save();
    }

    async showAllPromotions(promotionType: string): Promise<PromotionsUpdateJson[]> {

        if (this.promotionType.indexOf(promotionType) >= 0) {
            return await this._promotions.findAll({
                where: {
                    promotionType
                },
                include: [
                    {
                        model: PromotionAttachments
                    },
                    {
                        model: UserModel
                    },
                    {
                        model: BillingTransactionsModel
                    }
                ]
            });
        }
        return;
    }

    async getDetails(promotionsId: string) {
        return await this._promotions.findByPk(promotionsId, this.include);
    }

    async updatePromotions(id, bodyPromotions: Partial<PromotionsUpdateJson>): Promise<[number, Array<any>]> {

        return await this._promotions.update(bodyPromotions, {
            where: { id },
            returning: true
        });

    }

    async destroyPromotions(promotionsId: string): Promise<number> {

        return await this._promotions.destroy({
            where: { id: promotionsId },
        });
    }

    async find() {

        return await this._promotions.findAll(this.include);

    }

    async findByCode(code){
        return  await this._promotions.findOne({where:{
            promotionCode: code
        }})
    }
}