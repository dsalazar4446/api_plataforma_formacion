import { Injectable, Inject } from "@nestjs/common";
import { PromotionAttachments } from "../models/promotion-attachments.model";
import { PromotionAttachmentsSaveJson, PromotionAttachmentsUpdateJson } from "../interfaces/promotion-attachments.interface";
import * as path from 'path';
import * as fs from 'fs';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller PromotionAttachmentsService
 * @Creado 04 de Marzo 2019
 */

@Injectable()
export class PromotionAttachmentsService {

    private promotionAttachmentType = ['1', '2', '3']

    constructor(
        @Inject('PromotionAttachments') private readonly _promotionAttachments: typeof PromotionAttachments
    ) { }

    async savePromotionsAttach(bodyPromotionAttach: PromotionAttachmentsSaveJson): Promise<PromotionAttachmentsUpdateJson> {

        return await new this._promotionAttachments(bodyPromotionAttach).save();
    }

    async showAllPromotionAttach(promotionAttachmentType: string): Promise<PromotionAttachmentsUpdateJson[]> {

        if (this.promotionAttachmentType.indexOf(promotionAttachmentType) >= 0) {
            return await this._promotionAttachments.findAll({
                where: {
                    promotionAttachmentType
                }
            });
        }
    }

    async getDetails(promotionAttachId: string) {

        return await this._promotionAttachments.findByPk(promotionAttachId);
    }

    async updatePromotionAttach(id: string, bodyPromotionAttach: Partial<PromotionAttachmentsUpdateJson>): Promise<[number, Array<any>]> {

        if( bodyPromotionAttach.promotionAttachment ){
            await this.deleteImg(id);
        }

        return await this._promotionAttachments.update(bodyPromotionAttach, {
            where: { id },
            returning: true
        });

    }

    async destroyPromotionAttach(promotionsId: string): Promise<number> {

        await this.deleteImg(promotionsId);
        return await this._promotionAttachments.destroy({
            where: { id: promotionsId },
        });
    }

    async find() {

        return await this._promotionAttachments.findAll();

    }

    private async deleteImg(id: string){
        const img = await this._promotionAttachments.findByPk(id).then(item => item.promotionAttachment).catch(err => err);
    
        const pathImagen = path.resolve(__dirname, `../../../../uploads/promotionAttachment/${img}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }

}