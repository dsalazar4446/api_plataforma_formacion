import { Injectable, Inject } from "@nestjs/common";
import { PromotionsHasTransactions } from "../models/promotions-has-transactions.model";
import { PromotionsHasTransactionsSaveJson, PromotionsHasTransactionsUpdateJson } from "../interfaces/promotions-has-transactions.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller PromotionsHasTransactionsService
 * @Creado 04 de Marzo 2019
 */

@Injectable()
export class PromotionsHasTransactionsService {

    constructor(
        @Inject('PromotionsHasTransactions') private readonly _promotionsHasTransactions: typeof PromotionsHasTransactions
    ) { }

    async save(body: PromotionsHasTransactionsSaveJson): Promise<PromotionsHasTransactionsUpdateJson> {

        return await new this._promotionsHasTransactions(body).save();
    }

    async getAll(): Promise<PromotionsHasTransactionsSaveJson[]> {

        return await this._promotionsHasTransactions.findAll();
    }

    async destroy(id: string): Promise<number> {

        return await this._promotionsHasTransactions.destroy({
            where: { id },
        });
    }
}