import { Injectable, Inject } from "@nestjs/common";
import { PromotionsViewedByUsers } from "../models/promotions-viewed-by-users.model";
import { PromotionsViewedByUsersSaveJson, PromotionsViewedByUsersUpdateJson } from "../interfaces/promotions-viewed-by-users.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller PromotionsViewedByUsersService
 * @Creado 04 de Marzo 2019
 */

@Injectable()
export class PromotionsViewedByUsersService {

    constructor(
        @Inject('PromotionsViewedByUsers') private readonly _promotionsViewedByUsers: typeof PromotionsViewedByUsers
    ) { }

    async save(body: PromotionsViewedByUsersSaveJson): Promise<PromotionsViewedByUsersUpdateJson> {

        return await new this._promotionsViewedByUsers(body).save();
    }

    async getAll(): Promise<PromotionsViewedByUsersUpdateJson[]> {

        return await this._promotionsViewedByUsers.findAll();
    }

    async destroy(id: string): Promise<number> {

        return await this._promotionsViewedByUsers.destroy({
            where: { id },
        });
    }
}