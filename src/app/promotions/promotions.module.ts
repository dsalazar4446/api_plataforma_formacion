import { Module, HttpModule } from "@nestjs/common";
import { DatabaseModule } from "../database/database.module";
import { PromotionAttachments } from './models/promotion-attachments.model';
import { PromotionsHasTransactions } from './models/promotions-has-transactions.model';
import { PromotionsViewedByUsers } from './models/promotions-viewed-by-users.model';
import { Promotions } from './models/promotions.model';

import { PromotionsViewedByUsersService } from './services/promotions-viewed-by-users.service';
import { PromotionsService } from './services/promotions.service';
import { PromotionsHasTransactionsService } from './services/promotions-has-transactions.service';
import { PromotionAttachmentsService} from './services/promotion-attachments.service';

import { PromotionsViewedByUsersController } from './controllers/promotions-viewed-by-users.controller';
import { PromotionsHasTransactionsController } from './controllers/promotions-has-transactions.controller';
import { PromotionsController } from './controllers/promotions.controller';
import { PromotionAttachmentsController } from './controllers/promotion-attachments.controller';
import { SharedModule } from "../shared/shared.module";


const models = [
    PromotionAttachments, Promotions, PromotionsViewedByUsers, PromotionsHasTransactions
];
const providers = [
    PromotionAttachmentsService, PromotionsHasTransactionsService, PromotionsService, PromotionsViewedByUsersService
];
const controllers = [
    PromotionAttachmentsController, PromotionsController, PromotionsHasTransactionsController, PromotionsViewedByUsersController
];


@Module({
    imports: [
        SharedModule,
        HttpModule,
        DatabaseModule.forRoot({
            sequelize: {
                models,
            },
            loki: true,
        }),
    ],
    providers,
    controllers,
    exports: [
        PromotionsService
    ]
})
export class PromotionsModule { }
