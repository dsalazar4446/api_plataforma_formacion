import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, NotFoundException, Delete, Put, Param } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { PromotionsService } from "../services/promotions.service";
import { PromotionsSaveJson } from "../interfaces/promotions.interface";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { Promotions } from "../models/promotions.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller PromotionsController
 * @Creado 04 de Abril 2019
 */

@ApiUseTags('Module-Promotions')
@Controller('promotions')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class PromotionsController {

    constructor(private readonly _promotionsService: PromotionsService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: Promotions,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyPromotions: PromotionsSaveJson) {

        const createPromotions = await this._promotionsService.savePromotions(bodyPromotions);

        if (!createPromotions) {
            return this.appUtilsService.httpCommonError('Promotions no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM119',
                languageType: 'es',
            });
        }

        return createPromotions;

    }


    @ApiResponse({
        status: 200,
        type: Promotions,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAll() {

        const list = await this._promotionsService.find();

        if (!list) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return list;
    }


    @ApiResponse({
        status: 200,
        type: Promotions,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idPromotions: string) {

        const fetchPromotions = await this._promotionsService.getDetails(idPromotions);

        if (!fetchPromotions) {
            throw this.appUtilsService.httpCommonError('Promotions no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM120',
                languageType: 'es',
            });
        }

        return fetchPromotions;
    }


    @ApiResponse({
        status: 200,
        type: Promotions,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get(':promotionType')
    @ApiImplicitParam({ name: 'promotionType', required: true, type: 'string' })
    async showAllPromotions(@Param('promotionType') promotionType: string) {

        const promotionTypes = await this._promotionsService.showAllPromotions(promotionType);
        if (!promotionTypes) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return promotionTypes;
    }

    @ApiResponse({
        status: 200,
        type: Promotions,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updatePromotions(@Param('id') id: string, @Body() bodyPromotions: Partial<PromotionsSaveJson>) {

        const updatePromotions = await this._promotionsService.updatePromotions(id, bodyPromotions);

        if (!updatePromotions) {
            throw this.appUtilsService.httpCommonError('Promotions no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM120',
                languageType: 'es',
            });
        }

        return updatePromotions;
    }


    @ApiResponse({
        status: 200,
        type: Promotions,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deletePromotions(@Param('id') idPromotions: string) {

        const deletePromotions = await this._promotionsService.destroyPromotions(idPromotions);

        if (!deletePromotions) {
            throw this.appUtilsService.httpCommonError('Promotions no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM120',
                languageType: 'es',
            });
        }

        return deletePromotions;
    }
}