import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, NotFoundException, Delete, Param, Get } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { PromotionsHasTransactionsService } from "../services/promotions-has-transactions.service";
import { PromotionsHasTransactionsSaveJson } from "../interfaces/promotions-has-transactions.interface";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from "@nestjs/swagger";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller PromotionsHasTransactionsController
 * @Creado 04 de Abril 2019
 */

@ApiUseTags('Module-Promotions')
@Controller('promotions-has-transactions')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class PromotionsHasTransactionsController {


    constructor(private readonly _promotionsHasTransactionsService: PromotionsHasTransactionsService,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() body: PromotionsHasTransactionsSaveJson) {

        const create = await this._promotionsHasTransactionsService.save(body);

        if (!create) {
            throw this.appUtilsService.httpCommonError('Promotions Has Transactions no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM115',
                languageType: 'es',
            });
        }

        return create;

    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async getAll() {
        const data = await this._promotionsHasTransactionsService.getAll();

        if (!data[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return data;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') id: string) {

        const deletePromotionsHasTransactions = await this._promotionsHasTransactionsService.destroy(id);

        if (!deletePromotionsHasTransactions) {
            throw this.appUtilsService.httpCommonError('Promotions Has Transactions no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM116',
                languageType: 'es',
            });
        }

        return deletePromotionsHasTransactions;
    }
}