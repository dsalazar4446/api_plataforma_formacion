import { Controller, UseFilters, UseInterceptors, Post, Body, HttpStatus, Get, NotFoundException, Put, Delete, FileFieldsInterceptor, Param } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { PromotionAttachmentsService } from "../services/promotion-attachments.service";
import { PromotionAttachmentsSaveJson } from "../interfaces/promotion-attachments.interface";
import { CONFIG } from "../../../config";
import { ApiUseTags, ApiConsumes, ApiImplicitParam, ApiImplicitFile, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import * as file from './infoFile';
import { PromotionAttachments } from "../models/promotion-attachments.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller PromotionAttachmentsController
 * @Creado 04 de Abril 2019
 */

@ApiUseTags('Module-Promotions')
@Controller('promotion-attachments')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class PromotionAttachmentsController {

    constructor(private readonly _promotionAttachmentsService: PromotionAttachmentsService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: PromotionAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'promotionAttachment', description: 'Promotion Attachment File', required: true })
    async create(@Body() bodyPromotions: PromotionAttachmentsSaveJson) {

        const createPromotionAttach = await this._promotionAttachmentsService.savePromotionsAttach(bodyPromotions);

        if (!createPromotionAttach) {
            throw this.appUtilsService.httpCommonError('Promotion Attachments no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM113',
                languageType: 'es',
            });
        }

        createPromotionAttach.promotionAttachment = `${CONFIG.storage.server}promotionAttachment/${createPromotionAttach.promotionAttachment}`;

        return createPromotionAttach;

    }


    @ApiResponse({
        status: 200,
        type: PromotionAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idPromotionAttach: string) {

        const fetchPromotionAttach = await this._promotionAttachmentsService.getDetails(idPromotionAttach);

        if (!fetchPromotionAttach) {
            throw this.appUtilsService.httpCommonError('Promotion Attachments no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM114',
                languageType: 'es',
            });
        }

        fetchPromotionAttach.promotionAttachment = `${CONFIG.storage.server}promotionAttachment/${fetchPromotionAttach.promotionAttachment}`;

        return fetchPromotionAttach;
    }


    @ApiResponse({
        status: 200,
        type: PromotionAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAll() {

        const list = await this._promotionAttachmentsService.find();

        if (!list[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return list;
    }


    @ApiResponse({
        status: 200,
        type: PromotionAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get(':promotionAttachmentType')
    @ApiImplicitParam({ name: 'promotionAttachmentType', required: true, type: 'string' })
    async showAllPromotions(@Param('promotionAttachmentType') promotionAttachmentType: string) {

        const fetchAll = await this._promotionAttachmentsService.showAllPromotionAttach(promotionAttachmentType);

        if (!fetchAll[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        fetchAll.forEach(fetch => fetch.promotionAttachment = `${CONFIG.storage.server}promotionAttachment/${fetch.promotionAttachment}`);

        return fetchAll;
    }


    @ApiResponse({
        status: 200,
        type: PromotionAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'promotionAttachment', description: 'Promotion Attachment File', required: true })
    async updatePromotionAttach(@Param('id') id: string, @Body() bodyPromotions: Partial<PromotionAttachmentsSaveJson>) {

        const updatePromotionAttach = await this._promotionAttachmentsService.updatePromotionAttach(id, bodyPromotions);

        if (!updatePromotionAttach) {
            throw this.appUtilsService.httpCommonError('Promotion Attachments no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM114',
                languageType: 'es',
            });
        }

        updatePromotionAttach[1][0].promotionAttachment = `${CONFIG.storage.server}promotionAttachment/${updatePromotionAttach[1][0].promotionAttachment}`;

        return updatePromotionAttach;
    }


    @ApiResponse({
        status: 200,
        type: PromotionAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deletePromotionAttach(@Param('id') idPromotionAttach: string) {

        const deletePromotionAttach = await this._promotionAttachmentsService.destroyPromotionAttach(idPromotionAttach);

        if (!deletePromotionAttach) {
            throw this.appUtilsService.httpCommonError('Promotion Attachments no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM114',
                languageType: 'es',
            });
        }

        return deletePromotionAttach;
    }


}