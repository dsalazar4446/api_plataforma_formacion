import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Delete, NotFoundException, Param, Get } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { PromotionsViewedByUsersService } from "../services/promotions-viewed-by-users.service";
import { PromotionsViewedByUsersSaveJson } from "../interfaces/promotions-viewed-by-users.interface";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from "@nestjs/swagger";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller PromotionsViewedByUsersController
 * @Creado 04 de Abril 2019
 */

@ApiUseTags('Module-Promotions')
@Controller('promotions-viewed-by-users')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class PromotionsViewedByUsersController {

    constructor(private readonly _promotionsViewedByUsersService: PromotionsViewedByUsersService,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() body: PromotionsViewedByUsersSaveJson) {

        const create = await this._promotionsViewedByUsersService.save(body);

        if (!create) {
            return this.appUtilsService.httpCommonError('Promotions Viewed By Users no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM117',
                languageType: 'es',
            });
        }

        return create;

    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async getAll() {
        const data = await this._promotionsViewedByUsersService.getAll();

        if (!data[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return data;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deletePromotions(@Param('id') id: string) {

        const deletePromotionsViewedByUsers = await this._promotionsViewedByUsersService.destroy(id);

        if (!deletePromotionsViewedByUsers) {
            throw this.appUtilsService.httpCommonError('Promotions Viewed By Users no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM118',
                languageType: 'es',
            });
        }

        return deletePromotionsViewedByUsers;
    }
}