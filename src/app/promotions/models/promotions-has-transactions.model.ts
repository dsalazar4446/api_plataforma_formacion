import { Table, Model, Column, DataType, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from "sequelize-typescript";
import { BillingTransactionsModel } from "../../transactions/models/billing-transactions.model";
import { Promotions } from "./promotions.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({
    tableName: 'promotions_has_transactions',
})
export class PromotionsHasTransactions extends Model<PromotionsHasTransactions>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Promotions)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'promotion_id',
    })
    promotionId: string;
    
    @ApiModelPropertyOptional()
    @BelongsTo(() => Promotions)
    promotions: Promotions;
    
    @ApiModelPropertyOptional()
    @ForeignKey(() => BillingTransactionsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'billing_transaction_id',
    })
    billingTransactionId: string;
    
    @ApiModelPropertyOptional()
    @BelongsTo(() => BillingTransactionsModel)
    billingTransactions: Promotions;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}