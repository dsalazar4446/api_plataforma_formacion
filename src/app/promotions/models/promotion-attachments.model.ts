import { Table, Model, Column, DataType, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from "sequelize-typescript";
import { Promotions } from "./promotions.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({
    tableName: 'promotion_attachments',
})
export class PromotionAttachments extends Model<PromotionAttachments>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Promotions)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'promotion_id',
    })
    promotionId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'promotion_attachment',
    })
    promotionAttachment: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1','2','3'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'promotion_attachment_type',
    })
    promotionAttachmentType: string;

    /**
     * RELACIONES
     * PromotionAttachments tiene muchos Promotions
     */
        
    @BelongsTo(() => Promotions)
    promotions: Promotions;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}