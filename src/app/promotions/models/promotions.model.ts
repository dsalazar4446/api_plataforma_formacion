import { Table, Model, Column, DataType, CreatedAt, UpdatedAt, HasMany, BelongsToMany } from "sequelize-typescript";
import { PromotionAttachments } from "./promotion-attachments.model";
import { PromotionsViewedByUsers } from "./promotions-viewed-by-users.model";
import { UserModel } from "../../user/models/user.Model";
import { BillingTransactionsModel } from "../../transactions/models/billing-transactions.model";
import { PromotionsHasTransactions } from "./promotions-has-transactions.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({
    tableName: 'promotions',
})
export class Promotions extends Model<Promotions>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(9),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'promotion_code',
    })
    promotionCode: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2', '3', '4'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'promotion_type',
    })
    promotionType: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.NUMERIC,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'promotion_percentage_discount',
    })
    promotionPercentageDiscount: number;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'promotion_available_cant',
    })
    promotionAvailableCant: number;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TIME,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'promotion_valid_till',
    })
    promotionValidTill: Date;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'promotion_status',
    })
    promotionStatus: boolean;

    /**
     * RELACIONES
     * Promotions tiene muchos PromotionAttachments
     * Promotions tiene muchos UserModel
     */

    @ApiModelPropertyOptional({
        type: PromotionAttachments,
        isArray: true
    })
    @HasMany(() => PromotionAttachments)
    promotionAttachments: PromotionAttachments[];

    @ApiModelPropertyOptional({
        type: UserModel,
        isArray: false,
        example: [
            {
                "User": "UserModel"
            }
        ]
    })
    @BelongsToMany(() => UserModel, () => PromotionsViewedByUsers)
    userModel: UserModel[];

    @ApiModelPropertyOptional({
        type: BillingTransactionsModel,
        isArray: false,
        example: [
            {
                "id":"UUID",
                "userId":"UUID",
                "userLocationId":"UUID",
                "transactionCode":"string",
                "transactionType":"ENUM",
                "transactionAmmount":"number",
                "transactionShippment":"number",
                "transactionTaxes":"number",
                "transactionTotal":"number",
                "transactionStatus":"ENUM",
                "transactionTryouts":"number",
                "transactionObservations":"string",
                "transactionsUserObservations":"string",
                "transactionDiscount":"number",
                "billingTransactionOddoId":"number",
            }
        ]
    })
    @BelongsToMany(() => BillingTransactionsModel, () => PromotionsHasTransactions)
    billingTransactionsModel: BillingTransactionsModel[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
    static TransacItemsHProductPricings: any;

}