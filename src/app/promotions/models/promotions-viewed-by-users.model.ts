import { Table, Model, Column, DataType, CreatedAt, UpdatedAt, ForeignKey } from "sequelize-typescript";
import { Promotions } from "./promotions.model";
import { UserModel } from "../../user/models/user.Model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({
    tableName: 'promotions_viewed_by_users',
})
export class PromotionsViewedByUsers extends Model<PromotionsViewedByUsers>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Promotions)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'promotion_id',
    })
    promotionId: string;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}