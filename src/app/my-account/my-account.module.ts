import { Module } from '@nestjs/common';
import { UserWalletsService } from './services/user-wallets/user-wallets.service';
import { UserWalletsController } from './controllers/user-wallets/user-wallets.controller';
import { DatabaseModule } from '../database/database.module';
import { UserWalletsModel } from './models/user-wallets.model';
import { LogsModule } from '../logs/logs.module';

const models = [
  UserWalletsModel
]

@Module({
  imports: [
    LogsModule,
    DatabaseModule.forRoot({
      sequelize:{
        models,
      }
    })
  ],
  providers: [UserWalletsService],
  controllers: [UserWalletsController]
})
export class MyAccountModule {}
