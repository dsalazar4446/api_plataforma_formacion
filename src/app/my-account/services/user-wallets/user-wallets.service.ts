import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { CreateUserWalletDto } from '../../dto/user-wallets.dto';
import { UserWalletsModel } from '../../models/user-wallets.model';
import { LogsService } from '../../../logs/services/logs.service';
import { AppUtilsService } from '../../../shared/services/app-utils.service';
import { UserModel } from 'src/app/user/models/user.Model';

@Injectable()
export class UserWalletsService {
    constructor(@Inject('UserWalletsModel') private readonly userWallets: typeof UserWalletsModel,
        private readonly _logsService: LogsService,
        private readonly appUtilsService: AppUtilsService) { }

    async create(body: CreateUserWalletDto) {

        const userW = await new this.userWallets(body).save({ returning: true });

        if (!userW) {
            throw this.appUtilsService.httpCommonError('User Wallet No creado', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        let userName = await this.detail(userW.id).then(item => item.dataValues.user.username);

        await this.logs(userW.usersId, userName, 'wallet_create', '2');

        return userW;
    }

    async list() {
        return await this.userWallets.findAll();
    }

    async detail(id: string) {
        return await this.userWallets.findByPk(id, { include: [UserModel] });
    }

    async update(id: string, body: Partial<CreateUserWalletDto>) {

        const userW = await this.userWallets.update(body, { where: { id }, returning: true });

        if (!userW) {
            throw this.appUtilsService.httpCommonError('User Wallet No creado', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        let userName = await this.detail(userW[1][0].dataValues.id).then(item => item.dataValues.user.username);

        await this.logs(userW[1][0].usersId, userName, 'wallet_update', '2');

        return userW;
    }

    async delete(id: string) {

        let userWallet = await this.detail(id);

        await this.logs(userWallet.dataValues.usersId, userWallet.dataValues.user.username, 'wallet_delete', '2');
        return await this.userWallets.destroy({ where: { id } });
    }

    /**Guardar en la tabla logs las actualizaciones del usuario */
    async logs(userId: string, nickname: string, logMsg: string, logStatus: string) {
        await this._logsService.saveLogs({
            userId,
            nickname,
            logMsg,
            logStatus
        });
    }
}
