
import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString } from "class-validator";

export class CreateUserWalletDto {
    @ApiModelProperty()
    @IsUUID('4')
    usersId: string;
    @ApiModelProperty()
    @IsString()
    walletToken: string;
    @ApiModelProperty()
    @IsString()
    mainWallet: string;
}

export class UpdateUserWalletDto extends CreateUserWalletDto{
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}