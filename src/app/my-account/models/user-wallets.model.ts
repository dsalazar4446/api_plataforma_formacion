import { Model, Table, PrimaryKey, AutoIncrement, AllowNull, Validate, Unique, Column, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt } from "sequelize-typescript";
import { UserModel } from "../../user/models/user.Model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";
@Table({ tableName:'user_wallets' })
export class UserWalletsModel extends Model<UserWalletsModel>{
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'users_id',
    })
    usersId: string;

    @BelongsTo(() => UserModel)
    user: UserModel;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'wallet_token',
    })
    walletToken: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TINYINT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'main_wallet',
    })
    mainWallet: string;

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;
}