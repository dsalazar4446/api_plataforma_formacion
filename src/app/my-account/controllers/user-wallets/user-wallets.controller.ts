import { Controller, UseInterceptors, UseFilters, UseGuards, Post, UsePipes, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';
import { UserWalletsService } from '../../services/user-wallets/user-wallets.service';
import { UpdateUserWalletDto, CreateUserWalletDto } from '../../dto/user-wallets.dto';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { UserWalletsModel } from '../../models/user-wallets.model';

@ApiUseTags('My account')
@Controller('my-account/user-wallets')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // // @UseGuards(new JwtAuthGuard())
export class UserWalletsController {
    constructor(private readonly userWalletsService: UserWalletsService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: UserWalletsModel, isArray: true})
    async create(@Body() body: CreateUserWalletDto) {
        return this.userWalletsService.create(body);
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserWalletsModel })
    async list() {
        return this.userWalletsService.list();
    }

    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserWalletsModel })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    async detail(@Param('id') id) {
        return this.userWalletsService.detail(id);
    }

    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserWalletsModel })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    async update(@Param('id') id: string, @Body() body: Partial<CreateUserWalletDto>) {
        return await this.userWalletsService.update(id, body);
    }

    @Delete(':id')
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    async delete(@Param('id') id: string) {
        return this.userWalletsService.delete(id);
    }
}
