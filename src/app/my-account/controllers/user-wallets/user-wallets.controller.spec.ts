import { Test, TestingModule } from '@nestjs/testing';
import { UserWalletsController } from './user-wallets.controller';

describe('UserWallets Controller', () => {
  let controller: UserWalletsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserWalletsController],
    }).compile();

    controller = module.get<UserWalletsController>(UserWalletsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
