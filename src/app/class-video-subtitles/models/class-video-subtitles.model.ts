import { Column, DataType, Table, Model, CreatedAt, UpdatedAt } from 'sequelize-typescript';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'class_video_subtitles',
})
export class ClassVideoSubtitlesModel extends Model<ClassVideoSubtitlesModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'video_title',
    })
    videoTitle: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'video_description',
    })
    videoDescription: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'class_video_id',
    })
    classVideoId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.ENUM(
            'es',
            'en',
            'it',
            'pr',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
