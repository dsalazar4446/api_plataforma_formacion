import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, UsePipes, FileFieldsInterceptor, UseInterceptors, UseFilters, Inject, Req} from '@nestjs/common';
import { ClassVideoSubtitlesService } from '../services/class-videos-subtitles.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { ClassVideoSubtitlesSaveJson, ClassVideoSubtitlesUpdateJson } from '../interfaces/class-video-subtitles.interface';
import { ApiUseTags, ApiImplicitParam, ApiImplicitQuery, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { MainVideosModel } from '../../main-videos/models/main-videos.model';
import { ClassVideoSubtitlesModel } from '../models/class-video-subtitles.model';
import { AppUtilsService } from '../../shared/services/app-utils.service';

@ApiUseTags('Class video subtitles')
@Controller('class-video-subtitle')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class ClassVideoSubtitlesController {
    constructor(
        private readonly classVideoSubtitlesService: ClassVideoSubtitlesService,
        @Inject('MainVideosModel') private readonly  mainVideosModel: typeof  MainVideosModel,
        @Inject('ClassVideoSubtitlesModel') private readonly classVideoSubtitlesModel: typeof ClassVideoSubtitlesModel,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        @ApiImplicitQuery({ name: 'languajeType', enum: ['es','en','it','pr'] })
        @Post()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: ClassVideoSubtitlesModel
          })
        async create(@Body() classVideoSubtitles: ClassVideoSubtitlesSaveJson, @Req() req) {
            const data3 = await this.mainVideosModel.findByPk(classVideoSubtitles.classVideoId);
            if(data3 == null){
                throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA16',
                    languageType:  req.headers.language,
                });
            }
            return await this.classVideoSubtitlesService.create(classVideoSubtitles);
        }
        @Get()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: ClassVideoSubtitlesModel,
            isArray: true
          })
        async getAll() {
            return await this.classVideoSubtitlesService.findAll();
        }
        @Get(':id')
        @ApiResponse({
            status: 200,
            type: ClassVideoSubtitlesModel
          })
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiImplicitParam({name:'id', required: true})
        async findById(@Param('id') idClassVideoSubtitles) {
            return await this.classVideoSubtitlesService.findById(idClassVideoSubtitles);
        }
        @Get('csv/:idVideo')
        @ApiResponse({
            status: 200,
            type: ClassVideoSubtitlesModel
          })
        @ApiImplicitParam({ name: 'idVideo', required: true })
        async generateCSV(@Param('idVideo') idVideo) {
            return await this.classVideoSubtitlesService.findById(idVideo);
        }
        @Put(':id')
        @ApiResponse({
            status: 200,
            type: ClassVideoSubtitlesModel
          })
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiImplicitQuery({ name: 'languajeType', enum: ['es','en','it','pr'] })
        @ApiImplicitParam({ name: 'id', required: true })
        async update(
            @Body()
            classVideoSubtitles: Partial<ClassVideoSubtitlesUpdateJson>,
            @Param('id') idClassVideoSubtitles,
            @Req() req,
        ) {
            if(classVideoSubtitles.classVideoId){
                const data3 = await this.mainVideosModel.findByPk(classVideoSubtitles.classVideoId);
                if(data3 == null){
                    throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                        messageCode: 'EA16',
                        languageType:  req.headers.language,
                    });
                }
            }
            const data2 = await this.classVideoSubtitlesModel.findByPk(idClassVideoSubtitles);
            if(data2 == null){
                throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA17',
                    languageType:  req.headers.language,
                });
            }
            return await this.classVideoSubtitlesService.update(idClassVideoSubtitles, classVideoSubtitles);
        }
        @Delete(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiImplicitParam({ name: 'id', required: true })
        async delete(@Param('id') idClassVideoSubtitles) {
            return await this.classVideoSubtitlesService.deleted(idClassVideoSubtitles);
        }
}
