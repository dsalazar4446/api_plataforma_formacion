import { Injectable, Inject } from '@nestjs/common';
import { ClassVideoSubtitlesSaveJson, ClassVideoSubtitlesUpdateJson } from '../interfaces/class-video-subtitles.interface';
import { ClassVideoSubtitlesModel } from '../models/class-video-subtitles.model';
import * as  fs from 'fs';
import * as  path from 'path';
import * as  os from 'os';
import { CONFIG } from '../../../config';

@Injectable()
export class ClassVideoSubtitlesService {
    constructor(
        @Inject('ClassVideoSubtitlesModel') private readonly classVideoSubtitlesModel: typeof ClassVideoSubtitlesModel,
    ) { }
    async findAll(): Promise<ClassVideoSubtitlesModel[]> {
        return  await this.classVideoSubtitlesModel.findAll();
    }
    async findById(id: string): Promise<ClassVideoSubtitlesModel> {
        return await this.classVideoSubtitlesModel.findById<ClassVideoSubtitlesModel>(id);
    }
    async create(classVideoSubtitles: ClassVideoSubtitlesSaveJson): Promise<ClassVideoSubtitlesModel> {
        return await this.classVideoSubtitlesModel.create<ClassVideoSubtitlesModel>(classVideoSubtitles, {
            returning: true,
        });
    }
    async update(idClassVideoSubtitles: string, mainVideosUpdate: Partial<ClassVideoSubtitlesUpdateJson>){
        return  await this.classVideoSubtitlesModel.update(mainVideosUpdate, {
            where: {
                id: idClassVideoSubtitles,
            },
            returning: true,
        });
    }
    async deleted(idClassVideoSubtitles: string): Promise<any> {
        return await this.classVideoSubtitlesModel.destroy({
            where: {
                id: idClassVideoSubtitles,
            },
        });
    }
    async generateCSV(idVideo: string): Promise<any>{
        return new Promise(async (resolve,reject)=>{
            try {
                const route = (__dirname).replace('/src/app/class-video-subtitles/', 'public/');
                const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');
                const filename = path.join(route, `${randomName}.csv`);
                const output = [];
                const data = await this.classVideoSubtitlesModel.findAll<ClassVideoSubtitlesModel>({
                    where:{
                        classVideoId : idVideo,
                    }
                });
                data.forEach((d) => {
                    const row = [];
                    row.push(d.id);
                    row.push(d.classVideoId);
                    row.push(d.languageType);
                    row.push(d.created_at);
                    row.push(d.updated_at);
                    output.push(row.join());
                });
                fs.writeFileSync(filename, async()=>{
                    await output.join(os.EOL);
                    resolve(`${CONFIG.storage}${randomName}.csv`);
                });
            } catch (error) {
                reject(error);
            }
        });
    }
}
