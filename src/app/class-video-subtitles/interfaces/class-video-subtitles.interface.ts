import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class ClassVideoSubtitlesSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    classVideoId: string;
    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsString()
    @IsNotEmpty()
    videoTitle: string;
    @IsString()
    @IsNotEmpty()
    videoDescription: string;
    @IsString()
    @IsNotEmpty()
    languageType: string;
}
// tslint:disable-next-line: max-classes-per-file
export class ClassVideoSubtitlesUpdateJson  extends ClassVideoSubtitlesSaveJson{
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}
