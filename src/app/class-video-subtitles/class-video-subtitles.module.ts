import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { ClassVideoSubtitlesModel } from './models/class-video-subtitles.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { ClassVideoSubtitlesController } from './controllers/class-videos-subtitles.controller';
import { ClassVideoSubtitlesService } from './services/class-videos-subtitles.service';
import { MainVideosModel } from '../main-videos/models/main-videos.model';

const models = [
  ClassVideoSubtitlesModel,
  MainVideosModel
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [ClassVideoSubtitlesController],
  providers: [ClassVideoSubtitlesService]
})
export class ClassVideoSubtitlesModule {}
