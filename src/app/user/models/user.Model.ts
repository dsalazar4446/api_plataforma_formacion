import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey, HasMany, BelongsToMany, BelongsTo } from 'sequelize-typescript';
import { RolesModel } from '../../roles/models/roles.models';
import { CreexPayCards } from './../../creex-pay-card/models/creex-pay-cards.model';
import { News } from './../../news/models/news.models';
import { NewsHasUsers } from './../../news/models/news-has-users.models';
import { Resources } from './../../resources/models/resources.models';
import { UserTest } from './../../test/models/user-tests.models';
import { Events } from './../../events/models/events.model';
import { EventAttendances } from './../../events/models/event-attendances.model';
import { BillingTransactionsModel } from '../../transactions/models/billing-transactions.model';
import { PaymentAuthorizations } from '../../payment/models/payment-authorizations.model';
import { MessageModel } from '../../messages/models/messages.model';
import { InboxModel } from '../../inbox/models/inbox.model';
import { Announcements } from '../../anouncements/models/anouncements.model';
import { AnnouncementsHasUsers } from '../../anouncements/models/announcements-has-users.model';
import { Magazines } from '../../magazines/models/magazines.model';
import { MagazinesHasUsers } from '../../magazines/models/magazines-has-user.model';
import { InboxRecieversModel } from '../../inboxRecievers/models/inbox-recievers.model';
import { BroadcastMessageModel } from '../../broadcast-message/models/broadcast-message.model';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
import { UserMembershipsModel } from '../../memberships/models/user-memberships.model';
import { DocumentTypes } from '../../document-types/models/document.model';
import { Surveys } from '../../surveys/models/surveys.models';
import { SurveysHasUsers } from '../../surveys/models/surveys-has-users.model';
import { UserRankings } from '../../ranking/models/user-ranking.model';
import { RankingModel } from '../../ranking/models/ranking.model';
import { Downlines } from '../../downlines/models/downlines.model';
import { Transferals } from '../../transferals/models/transferal.model';
import { NotesResponses } from '../../classes/models/notes-responses.model';
import { TraderTransactions } from '../../trader/models/trader-transactions.model';
import { TradersBrokers } from '../../trader/models/trader-broker.model';
import { TraderUsers } from '../../trader/models/trader-users.model';
import { ClassProblemsModel } from '../../classes/models/class-problems.model';
import { UserComissionModel } from '../../user-comission/models/user-comission.model';
import { UserClassQuestionsAnswersModel } from '../../classes/models/user-class-questions-answers.model';
import { ClassSequencesModel } from '../../classes/models/class-sequences.model';
import { UserVideoRecordsModel } from '../../classes/models/user-video-records.model';
import { UserClassesViewsModel } from '../../classes/models/user-classes-views.model';
import { UserVideoFavoritesModel } from '../../classes/models/user-video-favorites.model';
import { Consultings } from '../../consultings/models/consultings.model';
import { RankingTransactions } from '../../ranking/models/ranking-transactions.model';
import { Calendars } from '../../calendars/models/calendars.model';
import { Followers } from '../../followers/models/followers.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { CityModel } from '../../city/models/city.model';
import { Countries } from '../../city/models/countries.model';
import { UserProgramsModel } from '../../programs/models/user-programs.model';
import { UserCoursesModel } from '../../courses/models/user-courses.model';
@Table({
    tableName: 'users',
})
export class UserModel extends Model<UserModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => RolesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'role_id',
    })
    roleId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => DocumentTypes)
    @Column({
        type: DataType.UUIDV4,
        allowNull: true,
        field: 'document_type_id',
    })
    documentTypeId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'document_number',
    })
    documentNumber: string;

    @Column({
        type: DataType.STRING,
        unique: true,
        allowNull: false,
        field: 'client_code',
    })
    clientCode: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        unique: true,
        allowNull: false,
        field: 'pwd',
    })
    pwd: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        unique: true,
        allowNull: false,
        field: 'email',
    })
    email: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(25),
        unique: true,
        allowNull: false,
        field: 'cellphone',
    })
    cellphone: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(75),
        unique: true,
        allowNull: true,
        field: 'username',
    })
    username: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'avatar',
        defaultValue: "default.jpeg",
    })
    avatar: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(75),
        allowNull: true,
        field: 'first_name',
    })
    firstName: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(75),
        allowNull: true,
        field: 'last_name',
    })
    lastName: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(25),
        allowNull: true,
        field: 'phone',
    })
    phone: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('male','female'),
        allowNull: true,
        field: 'gender',
        defaultValue: 'male',
    })
    gender: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: true,
        field: 'birth_date',
    })
    birthDate: Date;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'telegram',
    })
    telegram: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'facebook',
    })
    facebook: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(25),
        allowNull: true,
        field: 'whatsapp',
    })
    whatsapp: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'skype',
    })
    skype: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        field: 'advisor',
        defaultValue: true
    })
    advisor: boolean;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('all','student'),
        allowNull: false,
        field: 'notifications_authorization',
        defaultValue: 'all',
    })
    notificationsAuthorization: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        field: 'user_status',
        defaultValue: true,
    })
    userStatus: boolean;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.SMALLINT,
        allowNull: false,
        field: 'register_step',
        defaultValue: 1,
    })
    registerStep: number;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('left', 'center', 'right'),
        defaultValue: 'center',
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'mlm_side',
    })
    mlmSide: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.INTEGER,
        defaultValue: 0,
        allowNull: true,
        field: 'oddo_user_id',
    })
    oddoUserId: number

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'trade_system',
        defaultValue: false,
    })
    tradeSystem: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'operation_method',
    })
    operationMethod: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'trading_profile',
    })
    tradingProfile: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'master_root_account',
    })
    masterRootAccount: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'risk_benefit_average_ratio',
    })
    riskBenefitAverageRatio: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'maximum_operations_per_day',
    })
    maximumOperationsPerDay: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'maximum_lost_percentage_per_day',
    })
    maximumLostPercentagePerDay: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'percentage_of_assertiveness',
    })
    percentageOfAssertiveness: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'maximum_of_straight_negavite_operations',
    })
    maximumOfStraightNegativeOperations: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'maximum_losses',
    })
    maximumLosess: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'maximum_earnings',
    })
    maximumEarnings: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'eficient_operation_days',
    })
    eficientOperationDays: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(45),
        allowNull: true,
        field: 'ruc',
    })
    ruc: string;

    @ForeignKey(() => Countries)
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUID,
        allowNull: true,
        field: 'user_country_id',
    })
    userCountryId: string;

    @ForeignKey(() => CityModel)
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUID,
        allowNull: true,
        field: 'user_city_id',
    })
    userCityId: string;

    @Column({
        type: DataType.STRING(45),
        allowNull: true,
        field: 'reset_password_code',
    })
    resetPasswordCode: string;

    @Column({
        type: DataType.DATE,
        allowNull: true,
        field: 'password_code_time',
    })
    passwordCodeTime: Date;

    /**
     * RELACIONES
     * UserModel pertenece a muchos News
     * UserModel tiene muchos News
     * UserModel Tiene muchos Resources
     * UserModel tiene muchos UserTest
     * UserModel Tiene muchos CreexPayCards
     * UserModel Pertenece a RolesModel
     * UserModel tiene muchos Events 
     * UserModel tiene muchos EventAttendances
     * *UserModel tiene muchos Messages
     * UserModel tiene muchos Inbox
     *  UserModel tiene muchos BillingTransactionsModel
     */

    @BelongsTo(() => CityModel)
    city: CityModel;

    @BelongsTo(() => Countries)
    countries: Countries;

    @HasMany(() => BillingTransactionsModel)
    billingTransactionsModel: BillingTransactionsModel[];
    
    @HasMany(() => Events)
    events: Events[];

    @HasMany(() => UserTest)
    userTest: UserTest[];

    @HasMany(() => Resources)
    resources: Resources[];

    @ApiModelPropertyOptional({
        type: RolesModel,
        isArray: false,
        example: {roleName: "string"}
    })
    @BelongsTo(() => RolesModel, 'roleId')
    rolName: RolesModel;

    @HasMany(() => CreexPayCards)
    creexPayCards: CreexPayCards[];

    @HasMany(() => EventAttendances)
    eventAttendances: EventAttendances[];

    @HasMany(() => MessageModel)
    MessageModel: MessageModel[];

    @HasMany(() => InboxModel)
    inboxModel: InboxModel[];

    @HasMany(() => InboxRecieversModel)
    inboxRecieversModel: InboxRecieversModel[];

    InboxModel: InboxModel[];

    @HasMany(() => BroadcastMessageModel)
    BroadcastMessageModel: BroadcastMessageModel[];

    @HasMany(() => BroadcastModel, 'broadcaster_id')
    broadcaster: BroadcastMessageModel[];

    @HasMany(() => BroadcastModel, 'organizer_id')
    organizer: BroadcastMessageModel[];

    @HasMany(() => UserMembershipsModel)
    userMemberships: UserMembershipsModel[];

    @HasMany(() => ClassProblemsModel)
    classProblems: ClassProblemsModel[];
    
    @HasMany(() => UserComissionModel)
    userReciever: UserComissionModel[];

    @BelongsToMany(() => News, () => NewsHasUsers)
    news: News[];

  
    @BelongsTo(() => DocumentTypes)
    documentTypes: DocumentTypes[];

    @HasMany(() => PaymentAuthorizations)
    paymentAuthorizations: PaymentAuthorizations[];

    @BelongsToMany(() => Announcements, {
        through: {
            model: () => AnnouncementsHasUsers,
            unique: true,
        }
    })
    announcements: Announcements[];

    @BelongsToMany(() => Magazines, {
        through: {
            model: () => MagazinesHasUsers,
            unique: true,
        }
    })
    magazines: Magazines[];

    @BelongsToMany(() => Surveys, {
        through: {
            model: () => SurveysHasUsers,
            unique: true,
        }
    })
    surveys: Surveys[];

    @BelongsToMany(() => RankingModel, {
        through: {
            model: () => UserRankings,
            unique: true,
        }
    })
    rankingModel: RankingModel[];

    @HasMany(() => Downlines, 'parentUserId')
    parentU: Downlines[];

    @HasMany(() => Downlines, 'childrenUserId')
    childrenU: Downlines[];

    @HasMany(() => Transferals,)
    transferals: Transferals[];

    @HasMany(() => NotesResponses,)
    noteResponses: NotesResponses[];

    @HasMany(() => TraderTransactions,)
    TraderTransactions: TraderTransactions[];

    @HasMany(() => TradersBrokers, 'traderId')
    traders: TradersBrokers[];

    @HasMany(() => TradersBrokers, 'brokerId')
    brokers: TradersBrokers[];

    @HasMany(() => TraderUsers, 'traderId')
    traderUser: TraderUsers[];

    @HasMany(() => TraderUsers, 'userId')
    userTraderUser: TraderUsers[];

    @HasMany(() => UserClassQuestionsAnswersModel)
    userClassQuestionsAnswers: UserClassQuestionsAnswersModel[];

    @HasMany(() => UserVideoRecordsModel)
    userVideoRecords: UserVideoRecordsModel[];
    
    @HasMany(() => UserClassesViewsModel)
    userClassesViews: UserClassesViewsModel[];

    @HasMany(() => UserVideoFavoritesModel)
    userVideoFavorites: UserVideoFavoritesModel[];

    @HasMany(() => Consultings, 'userId')
    userConsultings: Consultings[];


    @HasMany(() => Consultings, 'advisorUserId')
    advisorUsers: Consultings[];


    @HasMany(() => RankingTransactions, 'parentUserId')
    parentUserRanking: RankingTransactions[];


    @HasMany(() => RankingTransactions, 'childrenUserId')
    childrenUserRanking: RankingTransactions[];

    @HasMany(() => Calendars)
    calendars: Calendars;

    @HasMany(() => Followers, 'followerUserId')
    followerUsers: Followers[];

    @HasMany(() => Followers, 'followedUserId')
    followedUsers: Followers[];


    @HasMany(() => UserProgramsModel)
    userProgramsModel: UserProgramsModel[];

    @HasMany(() => UserCoursesModel)
    UserCoursesModel: UserCoursesModel[];

    @ApiModelPropertyOptional({
        type:Date
    })
    @CreatedAt public created_at: Date;
    @ApiModelPropertyOptional({
        type:Date
    })
    @UpdatedAt public updated_at: Date;
}
