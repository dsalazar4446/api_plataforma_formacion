export interface usersNew {
    Id?: string;
    email?: string;
    cellphone?: string;
    username?: string;
    avatar?: string;
    document_type?: string;
    Document_id?: string;
    first_name?: string;
    last_name?: string;
    phone?: string;
    gender?: string;
    birthday?: string;
    telegram?: string;
    facebook?: string;
    whatsapp?: string;
    skype?: string;
  }