import { ApiModelProperty } from '@nestjs/swagger';
import {IsInt,IsBoolean, IsString, IsNotEmpty, IsUUID, IsOptional, IsNumber, IsDate} from "class-validator";
export class UserSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsOptional()
    roleId?: string;

    @ApiModelProperty()
    @IsUUID('4')
    @IsOptional()
    userCountryId?: string;

    @ApiModelProperty()
    @IsUUID('4')
    @IsOptional()
    userCityId?: string;


    @ApiModelProperty()
    @IsUUID('4')
    @IsOptional()
    documentTypeId: string;

    @ApiModelProperty()
    @IsString()
    @IsOptional()
    documentNumber: string;

    @ApiModelProperty()
    @IsString()
    @IsOptional()
    clientCode?: string;

    @ApiModelProperty()
    @IsString()
    email: string;
    
    @ApiModelProperty()
    @IsString()
    cellphone: string;

    @ApiModelProperty()
    @IsString()
    pwd: string;

    @ApiModelProperty()
    @IsString()
    @IsOptional()
    username?: string;

    @ApiModelProperty()
    @IsString()
    @IsOptional()
    avatar?: string;

    @ApiModelProperty()
    @IsOptional()
    firstName: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    lastName: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    phone: string;

    @ApiModelProperty()
    @IsString()
    @IsOptional()
    gender: string;

    @ApiModelProperty()
    @IsOptional()
    @IsDate()
    birthDate?: Date;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    telegram: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    skype: string;

    @ApiModelProperty()
    @IsBoolean()
    @IsOptional()
    advisor : boolean;

    @ApiModelProperty()
    @IsOptional()
    notificationsAuthorization: string;

    @ApiModelProperty()
    @IsBoolean()
    @IsOptional()
    userStatus: boolean;

    @ApiModelProperty()
    @IsInt()
    @IsOptional()
    registerStep: number;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    whatsapp: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    facebook: string;

    @ApiModelProperty()
    @IsOptional()
    mlmSide: string;

    @ApiModelProperty()
    @IsOptional()
    @IsNumber()
    oddoUserId: number

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    tradeSystem: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    operationMethod: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    tradingProfile: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    masterRootAccount: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    riskBenefitAverageRatio: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    maximumOperationsPerDay: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    maximumLostPercentagePerDay: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    percentageOfAssertiveness: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    maximumOfStraightNegativeOperations: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    maximumLosess: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    maximumEarnings: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    ruc: string;

    @ApiModelProperty()
    @IsOptional()
    @IsString()
    eficientOperationDays: string;

    @IsOptional()
    @IsString()
    newPassword?: string;


    @IsOptional()
    @IsString()
    resetPasswordCode?: string;


    @IsOptional()
    @IsString()
    passwordCodeTime?: Date;

}

// tslint:disable-next-line: max-classes-per-file
export class UserUpdateJson extends UserSaveJson{
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}