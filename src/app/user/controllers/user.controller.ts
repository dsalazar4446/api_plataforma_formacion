import { Controller, Post, Body, Get, Put, Delete, Param, UseInterceptors, UseFilters, UseGuards, FileFieldsInterceptor, HttpStatus, Res, Inject, Req } from '@nestjs/common';
import { UsersService } from '../services/users.service';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { UserSaveJson, UserUpdateJson } from '../interfaces/user.interface';
import * as bcrypt from 'bcrypt';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiConsumes, ApiResponse, ApiImplicitHeader, ApiUseTags } from '@nestjs/swagger';
import * as file from './infoFile';
import { ValidateUUID } from "../../shared/utils/validateUUID";
import { UserModel } from '../models/user.Model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { RolesModel } from '../../roles/models/roles.models';
import { DocumentTypes } from 'src/app/document-types/models/document.model';
@ApiUseTags('Module-Users')
@Controller('users')
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
@UseFilters(new AppExceptionFilter())
export class UserController {

    private type = ['influencers', 'redeemables'];

    constructor(
        private readonly userService: UsersService,
        private readonly _validateUUID: ValidateUUID,
        @Inject('RolesModel') private readonly rolesModel: typeof RolesModel,
        @Inject('DocumentTypes') private readonly documentTypes: typeof DocumentTypes,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        private readonly appUtilsService: AppUtilsService
    ) { }
    // REGISTER SESSION
    @Post()
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: UserModel,
    })
    @ApiConsumes('multipart/form-data')
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    async create(@Body() user: UserSaveJson, @Req() req) {
        // const data = await this.rolesModel.findByPk(user.roleId);
        // if (data == null) {
        //     throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
        //         messageCode: 'EA1',
        //         languageType: req.headers.language,
        //     });
        // }

        user.pwd = bcrypt.hashSync(user.pwd, 10);

        const userNew = await this.userService.create(user);

        if (!userNew) {
            throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EA2',
                languageType: req.headers.language,
            });
        }

        return userNew;
    }

    /**CREA LOS USUARIOS OBTENIDOS DESDE EL ARCHIVO CSV */
    @Get('createUsersJson')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: UserModel,
    })
    async createUsersJson() {
        const userNew = await this.userService.createUsersJson();
        if (!userNew) {
            throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return userNew;
    }

    /**OBTIENE INFLUENCES O Redeemables !!!!! */
    @ApiImplicitParam({ name: 'type', required: true, type: 'string' })
    @ApiImplicitParam({ name: 'limit', required: true, type: 'number' })
    @ApiImplicitParam({ name: 'countryId', required: false, type: 'string' })
    @Get('top/:type/:limit/:countryId?')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: UserModel,
    })
    async getInfluencersOrRedeemables(
        @Param('type') type: string,
        @Param('limit') limit: number,
        @Param('countryId') countryId?: string,
    ) {
        if (countryId) this._validateUUID.validateUUIID(countryId);

        if (this.type.indexOf(type) < 0) {
            throw this.appUtilsService.httpCommonError('El tipo es influencers o redeemables!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM159',
                languageType: 'es',
            });
        }
        return await this.userService.getInfluencersOrRedeemables(type, limit, countryId);
    }

    /**OBTIENE LOS CUMPLEAÑEROS!!!!! */
    @Get('birthday')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: UserModel,
    })
    async cumpleaneros() {
        return await this.userService.cumpleaneros();
    }

    // LIST SESSION
    @ApiImplicitParam({ name: 'gender', required: true, type: 'number' })
    @ApiImplicitParam({ name: 'roleId', required: true, type: 'string' })
    @ApiImplicitParam({ name: 'birtDaystart', required: true, type: 'string' })
    @ApiImplicitParam({ name: 'birtDayEnd', required: true, type: 'string' })
    @ApiImplicitParam({ name: 'typeOrder', required: true, type: 'string' })
    @ApiImplicitParam({ name: 'limit', required: true, type: 'number' })
    @ApiImplicitParam({ name: 'page', required: true, type: 'number' })
    @Get(':gender/:role_id/:birtday_date_start/:birtday_date_end/:type_order/:limit/:page')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: UserModel,
        isArray: true
    })
    async getAll(
        @Param('gender') gender,
        @Param('role_id') roleId,
        @Param('birtday_start') birtDayStart,
        @Param('birtday_end') birtDayEnd,
        @Param('type_order') typeOrder,
        @Param('limit') limit,
        @Param('page') page,
    ) {
        return await this.userService.findAll(gender, roleId, birtDayStart, birtDayEnd, typeOrder, limit, page);
    }

    @ApiResponse({
        status: 200,
        type: UserModel,
    })
    /*UPDATE PASSWORD PERDIDA.. LUEGO QUE SE ENVIA EL CORREO, EL FRONT CONSUME ESTE ENDPOINT PARA ASIGNAR LA NUEVA CLAVE!!!!*/
    @Put('updatePassword')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    async updatePass(@Body() updateUser: Partial<UserUpdateJson>) {
        updateUser.pwd = bcrypt.hashSync(updateUser.pwd, 10);
        return await this.userService.updatePass(updateUser);
    }


    // UPDATE USER
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Put(':id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiConsumes('multipart/form-data')
    @ApiResponse({
        status: 200,
        type: UserModel,
    })
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    async update(
        @Body()
        updateUser: Partial<UserSaveJson>,
        @Param('id') idUser,
        @Req() req
    ) {
        // const data = await this.rolesModel.findByPk(updateUser.roleId);
        // if (data == null) {
        //     throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
        //         messageCode: 'EA1',
        //         languageType: req.headers.language,
        //     });
        // }
        // const data2 = await this.documentTypes.findByPk(updateUser.documentTypeId);
        // if (data2 == null) {
        //     throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
        //         messageCode: 'EA2',
        //         languageType: req.headers.language,
        //     });
        // }
        const data3 = await this.userModel.findByPk(idUser);
        if (data3 == null) {
            throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA3',
                languageType: req.headers.language,
            });
        }
        if (updateUser.pwd) updateUser.pwd = await bcrypt.hashSync(updateUser.pwd, 10);
        return await this.userService.update(idUser, updateUser);
    }
    // DELETE USER
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Delete(':id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    async delete(@Param('id') idUser) {
        return await this.userService.deleted(idUser);
    }

    /***********************************Metodos admin, Retornan todas las propiedades del objeto users!!! */

    /*ESTE ENDPOINT SE USA PARA CAMBIAR LA PASSWORD SABIENDO MI PASSWORD ACTUAL*/
    @Put('updatePassActual/:id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: UserModel,
    })
    async comparePass(@Param('id') idUser, @Body() updateUser: Partial<UserSaveJson>, @Req() req) {
        // const data3 = await this.userModel.findByPk(idUser);
        // if (data3 == null) {
        //     throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
        //         messageCode: 'EA3',
        //         languageType: req.headers.language,
        //     });
        // }
        this._validateUUID.validateUUIID(idUser);
        return await this.userService.comparePass(idUser, updateUser);
    }


    /*RESTAURAR PASSWORD!! RECIBE EL EMAIL Y MANDA CORREO AL USUARIO!!!!!!!!*/
    @Post('restore-password')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: UserModel,
    })
    async restorePassword(@Body() emailUser: Partial<UserSaveJson>) {
        return await this.userService.restorePassword(emailUser);
    }


    /**VEFIFICA LA VIGENCIA DEL TOKEN */
    @Get('vigencia-token-password/:token')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        description: 'Este endpoint recibe el token enviado al correo para verificar si aun es vigente'
        
    })
    async verificaTokenPassword(@Param('token') token: string) {
        return await this.userService.verificaTokenPassword(token);
    }


    // GET ALL USERS ADMIN
    @Get()
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: UserModel,
        isArray: true
    })
    async findAllUser() {
        return await this.userService.findAllUser();
    }


    /**para enviar correos a todos los usuarios de la plataforma */

    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: UserModel,
        isArray: true
    })
    @Get('sendMail')
    async sendMail() {
        return await this.userService.mailToRegisteredUsers();
    }




    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Get('admin/:id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: UserModel,
        isArray: true
    })
    async findByIdAdmin(@Param('id') idUser, @Req() req) {
        const data3 = await this.userModel.findByPk(idUser);
        if (data3 == null) {
            throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA3',
                languageType: req.headers.language,
            });
        }
        return await this.userService.findByIdAdmin(idUser);
    }

    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Get(':id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: UserModel,
        isArray: true
    })
    async findById(@Param('id') idUser, @Req() req) {
        const data3 = await this.userModel.findByPk(idUser);
        if (data3 == null) {
            throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA3',
                languageType: req.headers.language,
            });
        }
        return await this.userService.findById(idUser);
    }






    /**************************************PERMITE ACTUALIZAR TODAS LAS PROPIEDADES DEL USUARIO******************************************/


    // UPDATE ADMIN
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Put('admin/:id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: UserModel,
        isArray: true
    })
    @ApiConsumes('multipart/form-data')
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    async updateAdmin(
        @Body()
        updateUser: Partial<UserSaveJson>,
        @Param('id') idUser,
        @Req() req
    ) {
        const data3 = await this.userModel.findByPk(idUser);
        if (data3 == null) {
            throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA3',
                languageType: req.headers.language,
            });
        }
        if (updateUser.pwd) updateUser.pwd = await bcrypt.hashSync(updateUser.pwd, 10);
        return await this.userService.updateAll(idUser, updateUser);
    }

}
