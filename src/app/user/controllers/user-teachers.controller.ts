import { Controller, UseInterceptors, UseFilters, UseGuards, Get, Param, HttpStatus } from "@nestjs/common";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { UsersService } from "../services/users.service";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiImplicitParam, ApiImplicitHeader, ApiUseTags, ApiResponse } from "@nestjs/swagger";
import { UserModel } from "../models/user.Model";
// import { JwtAuthGuard } from "../../auths/guards/jwt-auth.guard";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller UserTeachersController
 * @Creado 07 de Abril 2019
 */
@ApiUseTags('Module-Users')
@Controller('user-search')
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
@UseFilters(new AppExceptionFilter())
export class UserTeachersController {

    private roles = ['influencer', 'payments', 'marketing', 'support', 'admin', 'student', 'teacher', 'organizer'];
    private roles1 = ['teacher', 'organizer'];

    constructor(private readonly userService: UsersService,
        private readonly appUtilsService: AppUtilsService) { }

    @ApiResponse({
        status: 200,
        type: UserModel,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('user/:termino/:status')
    async search(@Param('termino') termino: string,
        @Param('status') status: boolean,
        // @Param('limit') limit: number,
        // @Param('page') page: number, 
    ) {
        if (this.roles.includes(termino)) {
            return await this.userService.searchRol(termino);
        }
        return await this.userService.search(termino, status /*, limit, page*/);
    }

    @ApiResponse({
        status: 200,
        type: UserModel,
    })
    @Get('rol/:rol')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'rol', required: true, type: 'string' })
    async findRoles(@Param('rol') rol: string) {
        if (this.roles1.indexOf(rol.toLowerCase()) < 0) {
            return this.appUtilsService.httpCommonError(`Rol Invalid! ${this.roles1.join(', ')}`, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return await this.userService.searchRol(rol);
    }


}