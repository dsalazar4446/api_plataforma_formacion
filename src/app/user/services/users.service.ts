import { Injectable, Inject, HttpException, HttpStatus, forwardRef, HttpService } from '@nestjs/common';
import { UserModel } from '../../user/models/user.Model';
import { UserSaveJson, UserUpdateJson } from '../interfaces/user.interface';
import { Sequelize } from 'sequelize-typescript';
import { RolesModel } from '../../roles/models/roles.models';
import * as ShortUniqueId from 'short-unique-id';
import { RolesService } from '../../roles/services/roles/roles.service';
import { AuthsService } from '../../auths/services/auths.service';
import { AuthsModel } from '../../auths/models/auths.model';
import * as path from 'path';
import * as fs from 'fs';
import { CONFIG } from '../../../config';
import { DocumentTypes } from '../../document-types/models/document.model';
import * as moment from 'moment';
import { NodemailerService } from '../../shared/services/nodemailer.service';
import * as bcrypt from 'bcrypt';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { siteVerification } from 'googleapis/build/src/apis/siteVerification';
import * as users from '../../shared/utils/users.json';
import { usersNew } from '../interfaces/usersNews.interface';
import { LogsService } from '../../logs/services/logs.service';
import { LogsSaveJson } from '../../logs/interfaces/logs.interface';
import { CityModel } from '../../city/models/city.model';
import { Countries } from '../../city/models/countries.model';
import { UserComissionModel } from '../../user-comission/models/user-comission.model';
import { UserComissionStatusModel } from '../../user-comission-status/models/user-comission-status.model';
import { UserProgramsModel } from '../../programs/models/user-programs.model';
import { UserProgramRatesModel } from '../../programs/models/user-program-rates.model';
import { UserCourseRates } from '../../courses/models/user-course.rate.model';
import { validate } from 'class-validator';
import { UserCoursesModel } from '../../courses/models/user-courses.model';

const Op = Sequelize.Op;
const uid = new ShortUniqueId();

@Injectable()
export class UsersService {

    private rol = ['influencer', 'payments', 'marketing', 'support', 'admin', 'student', 'teacher', 'organizer'];

    private include = {
        include: [
            {
                as: 'rolName',
                model: RolesModel,
                attributes: ['role_text']
            },
            {
                model: Countries,
                include: [CityModel]
            }
        ]
    };

    constructor(
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('UserComissionModel') private readonly userComissionModel: typeof UserComissionModel,
        @Inject('UserComissionStatusModel') private readonly userComissionStatusModel: typeof UserComissionStatusModel,
        @Inject('AuthsModel') private readonly authsModel: typeof AuthsModel,
        @Inject('Countries') private readonly _countries: typeof Countries,
        @Inject('CityModel') private readonly _city: typeof CityModel,
        @Inject(forwardRef(() => AuthsService)) private readonly authsService: AuthsService,
        private readonly _nodemailerService: NodemailerService,
        private readonly rolesService: RolesService,
        private readonly appUtilsService: AppUtilsService,
        private readonly _httpService: HttpService,
        private readonly _logsService: LogsService
    ) { }

    async search(termino: string, status?: boolean /*, limit: number, page: number*/) {

        const data = await this.userModel.findAll({
            where: {
                [Op.or]: [
                    {
                        clientCode: {
                            [Op.iLike]: `%${termino}%`
                        }
                    },
                    {
                        email: {
                            [Op.iLike]: `%${termino}%`
                        }
                    },
                    {
                        cellphone: {
                            [Op.iLike]: `%${termino}%`
                        }
                    },
                    {
                        username: {
                            [Op.iLike]: `%${termino}%`
                        }
                    },
                    {
                        documentNumber: {
                            [Op.iLike]: `%${termino}%`
                        }
                    },
                    {
                        firstName: {
                            [Op.iLike]: `%${termino}%`
                        }
                    },
                    {
                        lastName: {
                            [Op.iLike]: `%${termino}%`
                        }
                    }
                ],
                [Op.and]: {
                    userStatus: status
                }
            }
        });

        data.forEach(user => {
            this.undefinedData(user);
        });

        return data;
    }

    // tslint:disable-next-line: max-line-length
    async findAll(gender: number, roleId: string, birthDayStart: string, birthDayEnd: string, typeOrder: string, limit: number, page: number): Promise<any> {
        const tagsFilters = ['client_code', 'email', 'cellphone', 'username', 'first_name', 'document_id', 'last_name'];
        const counter = await this.userModel.findAndCountAll({
            where: {
                gender,
                roleId,
                [Op.or]: {
                    sessionDate: {
                        [Op.between]: [birthDayStart, birthDayEnd],
                    },
                },
            },

        });

        const pages = Math.ceil(counter.count / limit);
        const offset = limit * (page - 1);
        const device = await this.userModel.findAll({
            attributes: tagsFilters,
            where: {
                gender,
                roleId,
                [Op.or]: {
                    sessionDate: {
                        [Op.between]: [birthDayStart, birthDayEnd],
                    },
                },
            },
            include: [
                {
                    model: Countries,
                    include: [CityModel]
                }
            ],
            limit,
            offset,
            order: [
                (typeOrder === 'client_code') ? ['client_code', 'ASC'] :
                    (typeOrder === 'email') ? ['email', 'ASC'] :
                        (typeOrder === 'cellphone') ? ['cellphone', 'ASC'] :
                            (typeOrder === 'username') ? ['username', 'ASC'] :
                                (typeOrder === 'document_id') ? ['document_id', 'ASC'] :
                                    (typeOrder === 'first_name') ? ['first_name', 'ASC'] :
                                        ['last_name', 'ASC']// typeOrder === 'last_name'
            ]
        });

        device.forEach(user => {
            this.undefinedData(user);
            user.avatar = CONFIG.storage.server + 'user/avatar/' + user.avatar
        })

        const dataResult = {
            result: device,
            count: counter.count,
            pages,
        };
        return dataResult;
    }



    async detail(id) {
        return await this.userModel.findByPk(id, {
            include: [DocumentTypes, Countries]
        });
    }




    async findById(id: string) {
        const user = await this.userModel.findByPk(id, {
            include: [DocumentTypes,
                {
                    model: Countries,
                    include: [CityModel]
                }]
        });
        let roleName = await this.rolesService.findRoleById({ id: user.roleId }).then(item => item.roleText);

        this.undefinedData(user);
        user.avatar = CONFIG.storage.server + 'user/avatar/' + user.avatar;

        return {
            roleName,
            ...user.dataValues
        }
    }



    async create(user: UserSaveJson, json?: boolean) {

        let country = 'Peru';
        let city = 'Lima';

        if (!user.avatar) user.avatar = 'default.jpeg';
        if (user.resetPasswordCode) delete user.resetPasswordCode;
        if (user.passwordCodeTime) delete user.passwordCodeTime;

        user.clientCode = uid.randomUUID(9);

        if (!user.roleId) {
            user.roleId = await this.rolesService.findStudentRoleId();
            if (!user.roleId) {
                throw this.appUtilsService.httpCommonError('No se pudo encontrar el rol Student', HttpStatus.UNPROCESSABLE_ENTITY, {
                    messageCode: 'EM158',
                    languageType: 'es',
                });
            }
        }

        if (user.userCountryId) {

            const countryInfo = await this._countries.findByPk(user.userCountryId)

            if (!countryInfo) {
                throw this.appUtilsService.httpCommonError('Country no existe!', HttpStatus.INTERNAL_SERVER_ERROR, {
                    messageCode: 'EM156',
                    languageType: 'es',
                });
            }

            country = countryInfo.dataValues.countryName;
        }

        if (user.userCityId) {

            const cityInfo = await this._city.findByPk(user.userCityId)

            if (!cityInfo) {
                throw this.appUtilsService.httpCommonError('City no existe!', HttpStatus.INTERNAL_SERVER_ERROR, {
                    messageCode: 'EM157',
                    languageType: 'es',
                });
            }

            city = cityInfo.dataValues.cityName;
        }

        if (user.username) {
            const validateUser = await this.validateUser('username', user.username);
            if (validateUser) {
                throw this.appUtilsService.httpCommonError('UserName duplicado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                    messageCode: 'EM163',
                    languageType: 'es',
                });
            }
        }

        if (user.documentNumber) {
            const validateUser = await this.validateUser('documentNumber', user.documentNumber);
            if (validateUser) {
                throw this.appUtilsService.httpCommonError('DocumentNumber duplicado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                    messageCode: 'EM164',
                    languageType: 'es',
                });
            }
        }

        const validateUser = await this.validateUser('email', user.email);
        if (validateUser !== null) {
            throw this.appUtilsService.httpCommonError('Email duplicado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM165',
                languageType: 'es',
            });
        }

        const userCreated = await this.userModel.create<UserModel>(user, { returning: true });

        if (userCreated) {
            const auths = await this.authsModel.create({ userId: userCreated.id, pwd: userCreated.pwd });
        }

        let token = await this.authsService.sigin({
            email: userCreated.email,
            password: userCreated.pwd,
        });

        if (token) {

            let template;

            /**Para traerme el role name! */
            var roleName = await this.rolesService.findRoleById({ id: userCreated.roleId }).then(item => item.roleText);
            userCreated.avatar = CONFIG.storage.server + 'user/avatar/' + userCreated.avatar;

            /**Para no retornar algunos valores */
            this.undefinedData(userCreated);

            /* Enviar correo de creacion de usuario*/
            let optionsEmail = {
                subject: 'Hello, Welcome ✔', /**ASUNTO */
                text: 'Hello, Welcome ✔',
                username: userCreated.username,
                firstName: userCreated.firstName,
                lastName: userCreated.lastName,
                email: userCreated.email /**Puede ser un string[] de destinatarios */
            }

            if (json) {
                template = 'reset-password';
            } else {
                template = 'bienvenida';
            }

            /*template es el nombre del template.pug a usar */
            this._nodemailerService.sendEmail(template, optionsEmail);
        }
        return {
            roleName,
            ...userCreated.dataValues,
            country,
            city,
            token
        };
    }




    async update(idUser: string, userUpdate: Partial<UserSaveJson>) {
        userUpdate.registerStep = 2;

        if (userUpdate.username) {
            const validateUser = await this.validateUser('username', userUpdate.username);
            if (validateUser && validateUser.id !== idUser) {
                throw this.appUtilsService.httpCommonError('UserName duplicado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                    messageCode: 'EM163',
                    languageType: 'es',
                });
            }
        }

        if (userUpdate.email) {
            const validateUser = await this.validateUser('email', userUpdate.email);
            if (validateUser && validateUser.id !== idUser) {
                throw this.appUtilsService.httpCommonError('Email duplicado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                    messageCode: 'EM165',
                    languageType: 'es',
                });
            }
        }

        if (userUpdate.documentNumber) {
            const validateUser = await this.validateUser('documentNumber', userUpdate.documentNumber);
            if (validateUser && validateUser.id !== idUser) {
                throw this.appUtilsService.httpCommonError('DocumentNumber duplicado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                    messageCode: 'EM164',
                    languageType: 'es',
                });
            }
        }

        if (userUpdate.avatar) {
            await this.deleteImg(idUser);
        }

        if (userUpdate.pwd) delete userUpdate.pwd;
        if (userUpdate.resetPasswordCode) delete userUpdate.resetPasswordCode;
        if (userUpdate.passwordCodeTime) delete userUpdate.passwordCodeTime;

        const user = await this.userModel.update(userUpdate, {
            where: {
                id: idUser,
            },
            returning: true,
        });

        if (!user) {
            throw this.appUtilsService.httpCommonError(`No se pudo actualizar el usuario`, HttpStatus.NOT_FOUND, {
                messageCode: 'EM153',
                languageType: 'es',
            });
        }

        await this.logs(user[1][0].id, user[1][0].username, 'profile', '2');

        let roleName = await this.rolesService.findRoleById({ id: user[1][0].roleId }).then(item => item.roleText);

        user[1][0].avatar = CONFIG.storage.server + 'user/avatar/' + user[1][0].avatar;

        this.undefinedData(user[1][0]);

        return {
            roleName,
            ...user[1][0].dataValues
        };
    }




    async deleted(idUser: string): Promise<any> {

        await this.deleteImg(idUser);
        return await this.userModel.destroy({
            where: {
                id: idUser,
            },
        });
    }



    async validateUser(propiedad: string, data: string) {
        return await this.userModel.findOne({
            where: { [propiedad]: data },
        });
    }



    async validateUserByEmail(email: string) {
        return await this.userModel.findOne({
            where: { email },
        });
    }


    /**************************************METODO PARA BORRAR IMG ANTIGUA******************************************/


    private async deleteImg(id: string) {
        const img = await this.userModel.findByPk(id).then(item => item.avatar).catch(err => err);
        if (img === 'default.jpeg') return;
        const pathImagen = path.resolve(__dirname, `../../../../uploads/user/avatar/${img}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }


    /**************************************METODO PARA searchRol!! SI ES TEACHERS, TRAE SUS ESTRELLAS OBTENIDAS EN LOS CURSOS************/

    async searchRol(rol: string) {

        const users = await this.userModel.findAll({
            include: [
                {
                    model: RolesModel,
                    where: {
                        roleText: {
                            [Op.iLike]: `%${rol}%`
                        }
                    },
                },
                {
                    model: UserProgramsModel,
                    include: [UserProgramRatesModel]
                },
                {
                    model: UserCoursesModel,
                    include: [UserCourseRates]
                }
            ]
        }).map(user => {

            let start0: number = 0; let start1: number = 0; let roleName: string;

            user.avatar = CONFIG.storage.server + 'user/avatar/' + user.avatar;
            roleName = user.rolName.dataValues.roleText;
            user.dataValues.rolName = undefined;
            this.undefinedData(user);

            if (!user.userProgramsModel[0] && !user.UserCoursesModel[0]) {
                user.dataValues.userProgramsModel = undefined;
                user.dataValues.UserCoursesModel = undefined;
                return {
                    ...user.dataValues,
                    roleName,
                    start: 0
                }
            }

            if (user.userProgramsModel[0]) {

                user.userProgramsModel.forEach(item => {

                    let count = item.dataValues.userProgramRates.length;
                    let startSum: number = 0;

                    item.dataValues.userProgramRates.forEach(rate => {
                        startSum += Number(rate.dataValues.userProgramRate);
                    });

                    start0 = Number((startSum / count).toFixed(2));
                });

                user.dataValues.userProgramsModel = undefined;
            }

            if (user.UserCoursesModel[0]) {

                user.UserCoursesModel.forEach(item => {

                    let count = item.dataValues.userCourseRates.length;
                    let startSum: number = 0;

                    item.dataValues.userCourseRates.forEach(rate => {
                        startSum += Number(rate.dataValues.userCourseRate);
                    });

                    start1 = Number((startSum / count).toFixed(2));
                });

                user.dataValues.UserCoursesModel = undefined;
            }

            if (start0 === 0 || start1 === 0) {
                return {
                    ...user.dataValues,
                    roleName,
                    start: start0 + start1
                }
            }

            return {
                ...user.dataValues,
                roleName,
                start: Number(((start0 + start1) / 2).toFixed(2))
            }

        });

        return users;
    }

    /**************************************METODO PARA changeStatusUser******************************************/


    async changeStatusUser(id: string, status: boolean) {
        const user = await this.userModel.findByPk(id);
        if (user) {
            user.userStatus = status
            return await user.save();
        }
        return false;
    }

    /***********************************OBTENER CUMPLEANEROS DEL DIA************************************************!!! */

    async cumpleaneros() {
        let birthDate = `${moment().format('MM')}-${moment().format('DD')}`;
        return await this.userModel.findAll().filter(item => String(item.birthDate).includes(birthDate));
    }



    /**************************************PERMITE RETORNAR TODAS LAS PROPIEDADES DE UN USUARIO******************************************/



    async findByIdAdmin(id: string): Promise<any> {
        const user = await this.userModel.findByPk(id, {
            include: [
                {
                    model: DocumentTypes
                },
                {
                    model: Countries,
                    include: [CityModel]
                }
            ]
        });
        let roleName = await this.rolesService.findRoleById({ id: user.roleId }).then(item => item.roleText);
        user.avatar = CONFIG.storage.server + 'user/avatar/' + user.avatar;
        return {
            roleName,
            ...user.dataValues
        }
    }


    /**************************************PERMITE ACTUALIZAR TODAS LAS PROPIEDADES DEL USUARIO******************************************/

    async updateAll(idUser: string, userUpdate: Partial<UserUpdateJson>) {

        if (userUpdate.avatar) {
            await this.deleteImg(idUser);
        }

        const user = await this.userModel.update(userUpdate, {
            where: {
                id: idUser,
            },
            returning: true,
        });
        user[1][0].avatar = CONFIG.storage.server + 'user/avatar/' + user[1][0].avatar;

        return user;
    }

    /**************************************RETORNA TODOS LOS USUARIOS********************************************/


    async findAllUser() {

        const data = await this.userModel.findAll(this.include);
        data.forEach(user => {
            user.avatar = CONFIG.storage.server + 'user/avatar/' + user.avatar
        });
        return data;
    }


    /**************************************CREAR Y MANDAR CORREOS A USUARIOS CREADOS DESDE JSON********************************************/

    async createUsersJson() {
        let user;

        return Promise.all(
            await users.map(async (userJson: usersNew, index) => {
                user = {
                    documentTypeId: '48d69629-77e3-4d66-ba26-03062e241959',
                    documentNumber: userJson.Document_id,
                    email: userJson.email,
                    pwd: bcrypt.hashSync('123456', 10),
                    cellphone: userJson.cellphone,
                    username: userJson.username,
                    firstName: userJson.first_name,
                    lastName: userJson.last_name,
                    phone: userJson.cellphone,
                    avatar: 'default.jpeg',
                    facebook: userJson.facebook || null,
                    whatsapp: userJson.whatsapp || null,
                    telegram: userJson.telegram || null,
                    skype: userJson.skype || null,
                    gender: 'male'
                }

                let data = await this.create(user, true);

                if (data) {
                    return {
                        ok: true,
                        message: `creados ${index} usuarios`,
                        data
                    }
                }
                throw this.appUtilsService.httpCommonError('UsersJson does not created!', HttpStatus.INTERNAL_SERVER_ERROR, {
                    messageCode: 'EM147',
                    languageType: 'es',
                });

            })
        );
    }

    /**************************************ASIGNAR UNDEFINED A PROPIEDADES************************************************/

    async undefinedData(obj: Partial<UserUpdateJson>) {

        obj.pwd = undefined;
        obj.tradeSystem = undefined;
        obj.operationMethod = undefined;
        obj.tradingProfile = undefined;
        obj.masterRootAccount = undefined;
        obj.riskBenefitAverageRatio = undefined;
        obj.maximumLostPercentagePerDay = undefined;
        obj.maximumOperationsPerDay = undefined;
        obj.percentageOfAssertiveness = undefined;
        obj.maximumOfStraightNegativeOperations = undefined;
        obj.maximumLosess = undefined;
        obj.maximumEarnings = undefined;
        obj.eficientOperationDays = undefined;
        obj.mlmSide = undefined;
        obj.oddoUserId = undefined;

    }







    /**************************************METODOS PARA RESTAURAR PASSWORD************************************************/

    /**ENVIA CORREO AL USUARIO PARA RESTAURAR SU CLAVE */
    async restorePassword(user: Partial<UserSaveJson>): Promise<any> {

        const validateUser = await this.validateUser('email', user.email);
        if (!validateUser) {
            throw this.appUtilsService.httpCommonError(`Cuenta desconocida (${user.email})`, HttpStatus.NOT_FOUND, {
                messageCode: 'EM152',
                languageType: 'es',
            });
        }

        let update = {
            resetPasswordCode: validateUser.resetPasswordCode = uid.randomUUID(10) + uid.randomUUID(10),
            passwordCodeTime: moment().add(1, 'days').format()
        }

        await this.userModel.update(update, {
            where: {
                id: validateUser.dataValues.id
            },
            returning: true,
        });

        let optionsEmail = {
            subject: '[creex.com] Password reseted', /**ASUNTO */
            text: '[creex.com] Password reseted',
            email: validateUser.email /**Puede ser un string[] de destinatarios */,
            id: validateUser.resetPasswordCode
        }
        this._nodemailerService.sendEmail('restore-password', optionsEmail);

        return {
            ok: true,
            message: 'Se le ha enviado un enlace para cambiar su contraseña por correo electrónico.'
        }
    }



    /**VERIFICA LA VIGENCIA DEL TOKEN PASSWORD */
    async verificaTokenPassword(resetPasswordCode: string) {

        const validateUser = await this.validateUser('resetPasswordCode', resetPasswordCode);
        if (!validateUser) {
            throw this.appUtilsService.httpCommonError('Token no existe!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM166',
                languageType: 'es',
            });
        }

        let fecha = moment().format();
        let hoy = new Date(fecha);

        if (validateUser.passwordCodeTime < hoy) {

            await this.userModel.update({ resetPasswordCode: null, passwordCodeTime: null }, {
                where: {
                    id: validateUser.id
                },
                returning: true,
            });
            return {
                validToken: false,
                message: 'Token Vencido'
            }
        }

        return {
            validToken: true,
            message: 'Token Vigente'
        }

    }



    /** ESTE ENDPOINT ES PARA GUARDAR LA NUEVA PASSWORD LLUEGO DE QUE EL USUARIO RECIBIO EL LINK Y MANDO CLAVES NUEVAS */
    async updatePass(userUpdate: Partial<UserUpdateJson>) {

        let { resetPasswordCode } = userUpdate;
        delete userUpdate.resetPasswordCode;

        const data = await this.userModel.findOne({ where: { resetPasswordCode } });

        if (!data) {
            throw this.appUtilsService.httpCommonError('Token no existe!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM166',
                languageType: 'es',
            });
        }

        userUpdate.resetPasswordCode = null;
        userUpdate.passwordCodeTime = null;

        const user = await this.userModel.update(userUpdate, {
            where: {
                id: data.id
            },
            returning: true,
        });

        if (!user) {
            throw this.appUtilsService.httpCommonError('Password no Actualizada', HttpStatus.FORBIDDEN, {
                messageCode: 'EM168',
                languageType: 'es',
            });
        }

        await this.logs(user[1][0].id, user[1][0].username, 'pwd', '2');

        /*POR EL ID DEL USERS ACTUALIZADO, BUSCO EN LA TABLA AUTHS PARA ACTUALIZAR TAMBIEN ALLÁ */
        const auth = await this.authsModel.findOne({ where: { userId: user[1][0].id } }).then(item => item.id);
        await this.authsModel.update({ userId: user[1][0].id, pwd: user[1][0].pwd }, {
            where: {
                id: auth
            }
        });

        /**Para no retornar algunos valores */
        this.undefinedData(user[1][0]);

        return {
            ...user[1][0].dataValues
        };
    }

    /* COMPARAR Y ACTUALIZAR PASSWORD SABIENDO LA PASSWORD ACTUAL Y ENVIANDO LA NUEVA*/
    async comparePass(idUser: string, user: Partial<UserSaveJson>) {

        /* VALIDO QUE HAYAN ESCRITO LAS CLAVES */
        if (!user.newPassword || !user.pwd) {
            throw this.appUtilsService.httpCommonError('Introduzca contraseña', HttpStatus.FORBIDDEN, {
                messageCode: 'EM150',
                languageType: 'es',
            });
        }

        /*VALIDO QUE EXISTA EL USUARIO EN LA DB */
        const userDB = await this.userModel.findByPk(idUser);
        if (!userDB) {
            throw this.appUtilsService.httpCommonError('No se ha podido encontrar el usuario', HttpStatus.NOT_FOUND, {
                messageCode: 'EM149',
                languageType: 'es',
            });
        }

        /* ...Y QUE LA PASSWORD ACTUAL COINCIDA */
        if (!bcrypt.compareSync(user.pwd, userDB.pwd)) {
            throw this.appUtilsService.httpCommonError('Contraseña no coincide', HttpStatus.CONFLICT, {
                messageCode: 'EM151',
                languageType: 'es',
            });
        }

        /*SI COINCIDE LA ACTUALIZO EN LA TABLA USERS */
        const userUpdated = await this.userModel.update({ pwd: bcrypt.hashSync(user.newPassword, 10) }, {
            where: {
                id: userDB.id
            },
            returning: true
        });

        await this.logs(userDB.id, userDB.username, 'pwd', '2');

        /*POR EL ID DEL USERS ACTUALIZADO, BUSCO EN LA TABLA AUTHS PARA ACTUALIZAR TAMBIEN ALLÁ */
        const auth = await this.authsModel.findOne({ where: { userId: userUpdated[1][0].id } }).then(item => item.id);
        await this.authsModel.update({ userId: userDB.id, pwd: userUpdated[1][0].pwd }, {
            where: {
                id: auth
            }
        });

        /*HAGO UNDEFINED LAS PROPIEDADES QUE NO DEBEN SER MOSTRADAS */
        this.undefinedData(userUpdated[1][0]);

        /*RETORNO */
        return {
            ok: true,
            message: 'Password Updated',
            userUpdated: {
                ...userUpdated[1][0].dataValues
            }
        };
    }


    /**ENVIAR CORREO A TODOS LOS USUARIOS DE LA PLATAFORMA.. SE DEBE CAMBIAR EL TEMPLATE*/

    async mailToRegisteredUsers() {
        let mailUsers: any[] = [];
        const users = await this.userModel.findAll();

        users.forEach(async user => {

            mailUsers.push({
                firstName: user.dataValues.firstName,
                lastName: user.dataValues.lastName,
                email: user.dataValues.email
            });

            let optionsEmail = {
                subject: '[creex.com] Password reseted', /**ASUNTO */
                text: '[creex.com] Password reseted',
                email: user.email /**Puede ser un string[] de destinatarios */,
                id: user.id
            };

            await this._nodemailerService.sendEmail('restore-password', optionsEmail);
        });

        return mailUsers;
    }


    /**Guardar en la tabla logs las actualizaciones del usuario */
    async logs(userId: string, nickname: string, logMsg: string, logStatus: string) {
        await this._logsService.saveLogs({
            userId,
            nickname,
            logMsg,
            logStatus
        });
    }

    async getInfluencersOrRedeemables(type: string, limit: number, countryId?: string) {

        let listUsers;
        if (!countryId) {
            listUsers = await this.userModel.findAll();
        } else {

            const country = await this._countries.findByPk(countryId);

            if (!country) {
                throw this.appUtilsService.httpCommonError('Country no existe!', HttpStatus.NOT_FOUND, {
                    messageCode: 'EM156',
                    languageType: 'es',
                });
            }

            listUsers = await this.userModel.findAll({
                where: {
                    userCountryId: countryId
                }
            });
        }
        let billingUsers: any = [];
        await Promise.all(
            await listUsers.map(
                async user => {
                    const dataUserComission: any = await this.userComissionModel.findAll({
                        where: {
                            usersRecieverId: user.dataValues.id,
                            comissionPayable: true
                        },
                        include: [UserComissionStatusModel]
                    });
                    if (dataUserComission != null) {
                        let amount = 0;
                        await dataUserComission.map(
                            async userComission => {
                                if (userComission.dataValues.userComissionStatusModel[0].dataValues.userComissionStatus == 'recieved') {
                                    amount = amount + (userComission.dataValues.comissionAmmount + userComission.dataValues.billingAmmount);
                                }
                            }
                        )
                        billingUsers.push({
                            userData: user.dataValues,
                            billing: amount
                        });
                    } else {
                        billingUsers.push({
                            userData: user.dataValues,
                            billing: 0
                        });
                    }
                }
            )
        );
        billingUsers = billingUsers.sort(this.sortByProperty('billing'));
        if (type == 'influencer') {
            billingUsers = billingUsers.reverse();
        }
        let responseData: any = [];
        if (billingUsers.length < limit) {
            for (let i = 0; i < limit; i++) {
                responseData.push(billingUsers[i]);
            }
        } else {
            responseData = billingUsers;
        }
        return responseData;
    }

    sortByProperty = function (property) {
        return function (x, y) {
            return ((x[property] === y[property]) ? 0 : ((x[property] > y[property]) ? 1 : -1));
        };
    };
}
