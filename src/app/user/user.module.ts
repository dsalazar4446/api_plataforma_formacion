import { SharedModule } from '../shared/shared.module';
import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { UserController } from './controllers/user.controller';
import { Category } from 'typescript-logging';
import { APP_LOGGER } from '../../logger';
import { DatabaseModule } from '../database/database.module';
import { UserModel } from './models/user.Model';
import { UsersService } from './services/users.service';
import { AuthsService } from '../auths/services/auths.service';
import { UserTeachersController } from './controllers/user-teachers.controller';
import { RolesModule } from '../roles/roles.module';
import { AuthsModule } from '../auths/auths.module';
import { UserComissionModel } from '../user-comission/models/user-comission.model';
import { DocumentTypes } from '../document-types/models/document.model';
import { LogsModule } from '../logs/logs.module';
import { CityModule } from '../city/city.module';
import { UserComissionStatusModel } from '../user-comission-status/models/user-comission-status.model';

const models = [
    UserModel,
    UserComissionModel,
    DocumentTypes,
    UserComissionStatusModel,
];

@Module({
    imports: [
        // SharedModule,
        HttpModule,
        forwardRef(() => RolesModule),
        forwardRef(() => AuthsModule),
        forwardRef(() => CityModule),
        LogsModule,
        DatabaseModule.forRoot({
            sequelize: {
                models,
            },
        }),
    ],
    controllers: [
        UserController,
        UserTeachersController
    ],
    providers: [
        AuthsService,
        UsersService,
        {
            provide: 'Logger',
            useValue: new Category('USER_MODULE', APP_LOGGER),
        },
    ],
    exports: [UsersService],
})
export class UserModule { }
