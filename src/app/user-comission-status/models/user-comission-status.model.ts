import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey, HasMany, BelongsTo, BelongsToMany } from 'sequelize-typescript';
import { ApiModelProperty } from '@nestjs/swagger';
import { UserComissionModel } from '../../user-comission/models/user-comission.model';
// Como aun no se dispone del modulo citie no se pudo agregar el foreign key con cities
@Table({
    tableName: 'user_comission_statuses',
})
export class UserComissionStatusModel extends Model<UserComissionStatusModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => UserComissionModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'user_comissions_id',
    })
    userComissionsId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.ENUM('pending', 'requested', 'aproved', 'rejected', 'recieved'),
        allowNull: false,
        validate: {
            notEmpty: false,
        },
        field: 'user_comission_status',
    })
    userComissionStatus: string;

    @BelongsTo( () => UserComissionModel )
    userComissionModel: UserComissionModel;


    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
