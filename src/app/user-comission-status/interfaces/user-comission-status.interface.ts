import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum, IsNumber, IsDate} from "class-validator";
export class UserComissionStatusSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    userComissionsId: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    userComissionStatus: string;
}

// tslint:disable-next-line: max-classes-per-file
export class UserComissionStatusUpdateJson extends UserComissionStatusSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    id: string;
}


