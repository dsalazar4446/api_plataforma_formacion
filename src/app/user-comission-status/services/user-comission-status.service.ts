import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { UserComissionStatusSaveJson, UserComissionStatusUpdateJson } from '../interfaces/user-comission-status.interface';
import { UserComissionStatusModel } from '../models/user-comission-status.model';
import { AppUtilsService } from 'src/app/shared/services/app-utils.service';
import { LogsService } from 'src/app/logs/services/logs.service';
import { UserComissionModel } from 'src/app/user-comission/models/user-comission.model';
import { UserComissionService } from 'src/app/user-comission/services/user-comission.service';

@Injectable()
export class UserComissionStatusService {
    constructor(
        @Inject('UserComissionStatusModel') private readonly userComissionStatusModel: typeof UserComissionStatusModel,
        private readonly appUtilsService: AppUtilsService,
        private readonly _logsService: LogsService,
        private readonly _userComissionService: UserComissionService
    ) { }
    async findAll(): Promise<UserComissionStatusModel[]> {
        return await this.userComissionStatusModel.findAll();
    }
    async findById(id: string): Promise<UserComissionStatusModel> {
        return await this.userComissionStatusModel.findById<UserComissionStatusModel>(id);
    }
    async create(userComissionStatus: UserComissionStatusSaveJson): Promise<UserComissionStatusModel> {
        const comission = await this.userComissionStatusModel.create<UserComissionStatusModel>(userComissionStatus, {
            returning: true,
        });

        if (!comission) {

            const userComission = await this._userComissionService.findById(userComissionStatus.userComissionsId);

            await this.logs(userComission.dataValues.usersRecieverId, userComission.dataValues.user.dataValues.username, 'comission_request', '1');

            throw this.appUtilsService.httpCommonError('User Comission no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM154',
                languageType: 'es',
            });

        }

        const userComission = await this._userComissionService.findById(comission.dataValues.userComissionsId);

        await this.logs(userComission.dataValues.usersRecieverId, userComission.dataValues.user.dataValues.username, 'comission_request', '2');

        return comission;
    }

    async update(idUserComissionStatus: string, userComissionStatusUpdate: Partial<UserComissionStatusUpdateJson>) {
        const comissionStatus = await this.userComissionStatusModel.update(userComissionStatusUpdate, {
            where: {
                id: idUserComissionStatus,
            },
            returning: true,
        });

        if (!comissionStatus[1][0]) {

            const comission = await this.userComissionStatusModel.findByPk(idUserComissionStatus, { include: [UserComissionModel] }).then(
                async item => {
                    return await this._userComissionService.findById(item.userComissionsId);
                });

            await this.logs(comission.user.id, comission.user.username, 'comission_payment', '1');

            throw this.appUtilsService.httpCommonError('User Comission no se ha actualizado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM155',
                languageType: 'es',
            });

        }

        if (comissionStatus[1][0].userComissionStatus && comissionStatus[1][0].userComissionStatus === 'aproved') {
            const userComission = await this._userComissionService.findById(comissionStatus[1][0].userComissionsId);

            await this.logs(userComission.dataValues.usersRecieverId, userComission.dataValues.user.dataValues.username, 'comission_payment', '2');
        }


        return comissionStatus;
    }

    async deleted(idUserComissionStatus: string): Promise<any> {
        return await this.userComissionStatusModel.destroy({
            where: {
                id: idUserComissionStatus,
            },
        });
    }

    async logs(userId: string, nickname: string, logMsg: string, logStatus: string) {
        await this._logsService.saveLogs({
            userId,
            nickname,
            logMsg,
            logStatus
        });
    }
}
