import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { UserComissionStatusController } from './controllers/user-comission-status.controller';
import { UserComissionStatusService } from './services/user-comission-status.service';
import { UserComissionStatusModel } from './models/user-comission-status.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { UserComissionModel } from '../user-comission/models/user-comission.model';
import { UserComissionModule } from '../user-comission/user-comission.module';
const models = [
  UserComissionStatusModel,
  UserComissionModel
];
@Module({
  imports: [
    UserComissionModule,
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [UserComissionStatusController],
  providers: [UserComissionStatusService]
})
export class UserComissionStatusModule {}
