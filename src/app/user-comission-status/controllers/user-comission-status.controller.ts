import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, HttpStatus, Res, Req } from '@nestjs/common';
import { UserComissionStatusService } from '../services/user-comission-status.service';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { UserComissionStatusSaveJson, UserComissionStatusUpdateJson } from '../interfaces/user-comission-status.interface';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiResponse, ApiImplicitHeader, ApiUseTags } from '@nestjs/swagger';
import { UserComissionStatusModel } from '../models/user-comission-status.model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { UserComissionModel } from '../../user-comission/models/user-comission.model';

@ApiUseTags('Module-User-Comission-Status')
@Controller('user-comission-status')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class UserComissionStatusController {
  constructor(
    private readonly userComissionStatusService: UserComissionStatusService,
    @Inject('UserComissionModel') private readonly userComissionModel: typeof UserComissionModel,
    @Inject('UserComissionStatusModel') private readonly userComissionStatusModel: typeof UserComissionStatusModel,
    private readonly appUtilsService: AppUtilsService,
  ) { }
  @Post()
  @ApiImplicitHeader({
    name: 'language',
    required: true,
  })
  @ApiResponse({
    status: 200,
    type: UserComissionStatusModel,
  })
  async create(@Body() userComissionStatus: UserComissionStatusSaveJson, @Req() req) {
    const data3 = await this.userComissionModel.findByPk(userComissionStatus.userComissionsId);
    if (data3 == null) {
      throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
        messageCode: 'EA10',
        languageType: req.headers.language,
      });
    }
    return await this.userComissionStatusService.create(userComissionStatus);
  }

        @ApiResponse({
            status: 200,
            type: UserComissionStatusModel,
            isArray: true
          })
        @Get()
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        async getAll() {
            return await this.userComissionStatusService.findAll();
        } 
        @ApiResponse({
            status: 200,
            type: UserComissionStatusModel,
          })
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Get(':id')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        async findById(@Param('id') idUserComission) {
            return await this.userComissionStatusService.findById(idUserComission);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @ApiResponse({
            status: 200,
            type: UserComissionModel,
          })
        @Put(':id')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        async update(
            @Body()
            updateUserComission: Partial<UserComissionStatusUpdateJson>,
             @Param('id') idUserComission,
             @Req() req
            ) {
            if(updateUserComission.userComissionsId){
                const data2 = await this.userComissionModel.findByPk(updateUserComission.userComissionsId);
                if(data2 == null){
                  throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
                      messageCode: 'EA10',
                      languageType:  req.headers.language,
                  });
                }
              }
            const data3 = await this.userComissionStatusModel.findByPk(idUserComission);
            if(data3 == null){
              throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
                  messageCode: 'EA19',
                  languageType:  req.headers.language,
              });
            }
            return await this.userComissionStatusService.update(idUserComission, updateUserComission);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Delete(':id')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
      async delete(@Param('id') idUserComission) {
            return await this.userComissionStatusService.deleted(idUserComission);
      }
}
