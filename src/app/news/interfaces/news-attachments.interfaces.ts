import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional } from 'class-validator';
export class NewsAttachmentsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'newId property must a be uuid'})
    @IsNotEmpty({message: 'newId property not must null'})
    newId: string;

    @ApiModelProperty()
    @IsString({message: 'newsAttachment property must a be string'})
    @IsNotEmpty()
    newsAttachment: string;

    @ApiModelProperty()
    @IsString({message: 'newAttachmentType property must a be string'})
    @IsNotEmpty({message: 'newAttachmentType property not must null'})
    newAttachmentType: string;
}
export class NewsAttachmentsUpdateJson extends NewsAttachmentsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
