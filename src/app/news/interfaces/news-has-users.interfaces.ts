import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class NewsHasUsersSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'newId property must a be uuid'})
    @IsNotEmpty({message: 'newId property not must null'})
    newId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    userId: string;
}
export class NewsHasUsersUpdateJson extends NewsHasUsersSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
