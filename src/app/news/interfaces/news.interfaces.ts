import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional } from 'class-validator';
export class NewsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    userId: string;

    @ApiModelProperty()
    @IsString({message: 'newTitle property must a be string'})
    @IsNotEmpty({message: 'newTitle property not must null'})
    newTitle: string;
    
    @ApiModelProperty()
    @IsString({message: 'newsDescription property must a be string'})
    @IsNotEmpty({message: 'newsDescription property not must null'})
    newsDescription: string;

    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsString({message: 'languageType property must a be string'})
    @IsOptional()
    languageType: string;
}
export class NewsUpdateJson extends NewsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
