import { Module, Provider, HttpModule } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { News } from './models/news.models';
import { NewsAttachments } from './models/news-attachments.models';
import { NewsHasUsers } from './models/news-has-users.models';

import { NewsService } from './services/news.service';
import { NewsAttachmentsService } from './services/news-attachments.service';
import { NewsHasUsersService } from './services/news-has-users.service';

import { NewsAttachmentsController } from './controllers/news-attachments.controller';
import { NewsController } from './controllers/news.controller';
import { NewsHasUsersController } from './controllers/news-has-users.controller';

const models = [News, NewsAttachments, NewsHasUsers];
const controllers = [NewsAttachmentsController, NewsController, NewsHasUsersController];
const providers: Provider[] = [NewsService, NewsAttachmentsService, NewsHasUsersService];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers
})
export class NewsModule { }
