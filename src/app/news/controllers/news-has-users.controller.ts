import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { NewsHasUsersSaveJson } from '../interfaces/news-has-users.interfaces';
import { NewsHasUsersService } from '../services/news-has-users.service';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from '@nestjs/swagger';


/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller NewsHasUsersController
 * @Creado 26 de Marzo 2019
 */

@ApiUseTags('Module-News')
@Controller('news-has-users')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class NewsHasUsersController {

    constructor(private readonly _newsHasUsers: NewsHasUsersService,
        private readonly appUtilsService: AppUtilsService
    ) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyNewsHasUsers: NewsHasUsersSaveJson) {

        const createNews = await this._newsHasUsers.saveNewsHasUsers(bodyNewsHasUsers);

        if (!createNews) {
            throw this.appUtilsService.httpCommonError('Surveys no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM134',
                languageType: 'es',
            });
        }

        return createNews;

    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllResource() {

        return await this._newsHasUsers.showAllNewsHasUsers();
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idNews: string) {

        const fetchNews = await this._newsHasUsers.getDetails(idNews);

        if (!fetchNews) {
            throw this.appUtilsService.httpCommonError('News Has Users no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM135',
                languageType: 'es',
            });
        }

        return fetchNews;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateNews(@Param('id') id: string, @Body() bodyNews: NewsHasUsersSaveJson) {

        const updateNews = await this._newsHasUsers.updateNewsHasUsers(id, bodyNews);

        if (!updateNews) {
            throw this.appUtilsService.httpCommonError('News Has Users no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM135',
                languageType: 'es',
            });
        }

        return updateNews;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteNews(@Param('id') idNews: string) {

        const deleteNews = await this._newsHasUsers.destroyNewsHasUsers(idNews);

        if (!deleteNews) {
            throw this.appUtilsService.httpCommonError('News Has Users no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM135',
                languageType: 'es',
            });
        }

        return deleteNews;
    }
}
