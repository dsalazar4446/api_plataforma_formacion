import { Controller, UseFilters, UseInterceptors, Post, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param, Req } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { NewsService } from '../services/news.service';
import { NewsSaveJson } from '../interfaces/news.interfaces';
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { News } from '../models/news.models';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller NewsController
 * @Creado 26 de Marzo 2019
 */

@ApiUseTags('Module-News')
@Controller('news')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class NewsController {

    constructor(private readonly _newsServcs: NewsService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: News,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Req() req, @Body() bodyNews: NewsSaveJson) {

        if (req.headers.language) {
            bodyNews.languageType = req.headers.language;
        } else {
            bodyNews.languageType = 'es';
        }

        const createNews = await this._newsServcs.saveNews(bodyNews);

        if (!createNews) {
            throw this.appUtilsService.httpCommonError('News no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM136',
                languageType: 'es',
            });
        }

        return createNews;

    }


    @ApiResponse({
        status: 200,
        type: News,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAll() {
        return await this._newsServcs.find();
    }


    @ApiResponse({
        status: 200,
        type: News,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idNews: string) {

        const fetchNews = await this._newsServcs.getDetails(idNews);

        if (!fetchNews) {
            throw this.appUtilsService.httpCommonError('News no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM137',
                languageType: 'es',
            });
        }

        return fetchNews;
    }


    @ApiResponse({
        status: 200,
        type: News,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllNews(@Req() req) {

        const fetch = await this._newsServcs.showAllNews(req.headers.language);
        if (!fetch[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetch;
    }


    @ApiResponse({
        status: 200,
        type: News,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateNews(@Req() req, @Param('id') id: string, @Body() bodyNews: Partial<NewsSaveJson>) {

        if (req.headers.language) bodyNews.languageType = req.headers.language;

        const updateNews = await this._newsServcs.updateNews(id, bodyNews);

        if (!updateNews[1][0]) {
            throw this.appUtilsService.httpCommonError('News no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM137',
                languageType: 'es',
            });
        }

        return updateNews;
    }


    @ApiResponse({
        status: 200,
        type: News,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteNews(@Param('id') idNews: string) {

        const deleteNews = await this._newsServcs.destroyNews(idNews);

        if (!deleteNews) {
            throw this.appUtilsService.httpCommonError('News no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM137',
                languageType: 'es',
            });
        }

        return deleteNews;
    }
}
