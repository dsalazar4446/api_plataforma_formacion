import { Controller, UseFilters, UseInterceptors, FileFieldsInterceptor, Post, UsePipes, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { NewsAttachmentsService } from '../services/news-attachments.service';
import { NewsAttachmentsSaveJson } from '../interfaces/news-attachments.interfaces';
import { CONFIG } from '../../../config';
import { ApiUseTags, ApiImplicitParam, ApiConsumes, ApiImplicitFile, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import * as file from './infoFile';
import { NewsAttachments } from '../models/news-attachments.models';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller NewsAttachmentsController
 * @Creado 26 de Marzo 2019
 */

@ApiUseTags('Module-News')
@Controller('news-attachments')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class NewsAttachmentsController {

    constructor(private readonly _newsAttachmentsService: NewsAttachmentsService,
        private readonly appUtilsService: AppUtilsService, ) { }


    @ApiResponse({
        status: 200,
        type: NewsAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'newsAttachments', description: 'News Attachments File', required: true })
    async create(@Body() bodyNewsAttach: NewsAttachmentsSaveJson) {

        const createNews = await this._newsAttachmentsService.saveNewsAttach(bodyNewsAttach);

        if (!createNews) {
            throw this.appUtilsService.httpCommonError('News Attachments no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM131',
                languageType: 'es',
            });
        }
        if (createNews.newAttachmentType == '1') return createNews;
        createNews.newsAttachment = `${CONFIG.storage.server}newsAttachment/${createNews.newsAttachment}`;

        return createNews;
    }


    @ApiResponse({
        status: 200,
        type: NewsAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllResource() {

        const fetchAll = await this._newsAttachmentsService.showAllNewsAttach();

        if (!fetchAll[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        fetchAll.forEach(fetch => {

            if (fetch.newAttachmentType == '1') return;
            fetch.newsAttachment = `${CONFIG.storage.server}newsAttachment/${fetch.newsAttachment}`;
        });

        return fetchAll;
    }


    @ApiResponse({
        status: 200,
        type: NewsAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idNews: string) {

        const fetchNews = await this._newsAttachmentsService.getDetails(idNews);

        if (!fetchNews) {
            throw this.appUtilsService.httpCommonError('News Attachments no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM132',
                languageType: 'es',
            });
        }

        if (fetchNews.newAttachmentType == '1') return fetchNews;
        fetchNews.newsAttachment = `${CONFIG.storage.server}newsAttachment/${fetchNews.newsAttachment}`;

        return fetchNews;
    }


    @ApiResponse({
        status: 200,
        type: NewsAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'newsAttachments', description: 'News Attachments File', required: true })
    async updateNews(@Param('id') id: string, @Body() bodyNews: Partial<NewsAttachmentsSaveJson>) {

        const updateNews = await this._newsAttachmentsService.updateNewsAttach(id, bodyNews);

        if (!updateNews[1][0]) {
            throw this.appUtilsService.httpCommonError('News Attachments no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM132',
                languageType: 'es',
            });
        }

        if (updateNews[1][0].newsAttachment == '1') return updateNews;
        updateNews[1][0].newsAttachment = `${CONFIG.storage.server}newsAttachment/${updateNews[1][0].newsAttachment}`;

        return updateNews;
    }


    @ApiResponse({
        status: 200,
        type: NewsAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteNews(@Param('id') idNews: string) {

        const deleteNews = await this._newsAttachmentsService.destroyNewsAttach(idNews);

        if (!deleteNews) {
            throw this.appUtilsService.httpCommonError('News Attachments no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM132',
                languageType: 'es',
            });
        }

        return deleteNews;
    }
}
