import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { News } from './news.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
@Table({
    tableName: 'news_attachments',
})
export class NewsAttachments extends Model<NewsAttachments>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => News)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'new_id',
    })
    newId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        validate: {
            notEmpty: true,
        },
        field: 'news_attachment',
    })
    newsAttachment: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2', '3'),
        validate: {
            notEmpty: true,
        },
        field: 'new_attachment_type',
    })
    newAttachmentType: string;

    /**
     * RELACIONES
     * NewsAttachments pertenece a News
     */    
    
    @BelongsTo(() => News)
    news: News;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}