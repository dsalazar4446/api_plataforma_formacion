import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo, BelongsToMany, HasMany } from 'sequelize-typescript';
import { UserModel } from './../../user/models/user.Model';
import { NewsHasUsers } from './news-has-users.models';
import { NewsAttachments } from './news-attachments.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'news',
})
export class News extends Model<News>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'new_title',
    })
    newTitle: string;


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'news_description',
    })
    newsDescription: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;

    /**
 * RELACIONES
 * News pertenece a  UserModel
 */

    @ApiModelPropertyOptional({
        type: UserModel,
        isArray: false,
        example: [
            {
                "User": "UserModel"
            }
        ]
    })
    @BelongsToMany(() => UserModel, () => NewsHasUsers)
    userModel: UserModel[];

    @ApiModelPropertyOptional({
        type: NewsAttachments,
        isArray: true
    })
    @HasMany(() => NewsAttachments)
    newsAttachments: NewsAttachments[];


    @BelongsTo(() => UserModel)
    userNews: UserModel;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}
