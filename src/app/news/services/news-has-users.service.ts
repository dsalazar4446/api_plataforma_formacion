import { Injectable, Inject } from '@nestjs/common';
import { NewsHasUsers} from '../models/news-has-users.models';
import { News } from '../models/news.models';
import { NewsHasUsersSaveJson, NewsHasUsersUpdateJson } from '../interfaces/news-has-users.interfaces';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services NewsHasUsersService
 * @Creado 26 de Marzo 2019
 */

@Injectable()
export class NewsHasUsersService {


    constructor(
        @Inject('NewsHasUsers') private readonly _newsHasUsersServcs: typeof NewsHasUsers
    ) { }

    async saveNewsHasUsers(bodyNews: NewsHasUsersSaveJson): Promise<NewsHasUsersUpdateJson> {

        return await new this._newsHasUsersServcs(bodyNews).save();
    }

    async showAllNewsHasUsers(): Promise<NewsHasUsersUpdateJson[]> {

        return await this._newsHasUsersServcs.findAll();
    }

    async getDetails(newsId: string) {

        if (newsId === undefined) {

            return await this._newsHasUsersServcs.findAll({
                include: [
                    {
                        model: News
                    }
                ]
            });
        }

        return await this._newsHasUsersServcs.findByPk(newsId, {
            include: [
                {
                    model: News
                }
            ]
        });
    }

    async updateNewsHasUsers(id: string, bodyNews: Partial<NewsHasUsersUpdateJson>): Promise<[number, Array<any>]> {

        return await this._newsHasUsersServcs.update(bodyNews, {
            where: { id },
            returning: true
        });

    }

    async destroyNewsHasUsers(newsId: string): Promise<number> {

        return await this._newsHasUsersServcs.destroy({
            where: { id: newsId },
        });
    }
}
