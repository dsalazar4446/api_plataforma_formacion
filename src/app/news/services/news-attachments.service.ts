import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { NewsAttachments } from '../models/news-attachments.models';
import { News } from '../models/news.models';
import { NewsAttachmentsSaveJson, NewsAttachmentsUpdateJson } from '../interfaces/news-attachments.interfaces';
import * as path from 'path';
import * as fs from 'fs';
import { AppUtilsService } from "../../shared/services/app-utils.service";


/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services NewsAttachmentsService
 * @Creado 26 de Marzo 2019
 */

@Injectable()
export class NewsAttachmentsService {

    private pattern = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;

    constructor(
        @Inject('NewsAttachments') private readonly _newsAttachmentsServcs: typeof NewsAttachments,
        private readonly appUtilsService: AppUtilsService
    ) { }

    async saveNewsAttach(bodyNewsAttach: NewsAttachmentsSaveJson): Promise<NewsAttachmentsUpdateJson> {

        if (bodyNewsAttach.newAttachmentType === '1') {
            if (!bodyNewsAttach.newsAttachment.match(this.pattern)) {
                
                throw this.appUtilsService.httpCommonError('News Attachment it is not video url Youtube', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EM133',
                    languageType: 'es',
                });
            }
        }
        
        return await new this._newsAttachmentsServcs(bodyNewsAttach).save();
    }

    async showAllNewsAttach(): Promise<NewsAttachmentsUpdateJson[]> {

        return await this._newsAttachmentsServcs.findAll();
    }

    async getDetails(newsAttachId: string) {

        return await this._newsAttachmentsServcs.findByPk(newsAttachId, {
            include: [
                {
                    model: News
                }
            ]
        });
    }

    async updateNewsAttach(id: string, bodyNewsAttach: Partial<NewsAttachmentsUpdateJson>): Promise<[number, Array<any>]> {

        if (bodyNewsAttach.newAttachmentType === '1') {
            if (!bodyNewsAttach.newsAttachment.match(this.pattern)) {
                throw this.appUtilsService.httpCommonError(`News Attachment it is not video url Youtube`, HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EM133',
                    languageType: 'es',
                });
            }
        }

        if (bodyNewsAttach.newsAttachment) {
            await this.deleteImg(id);
        }

        return await this._newsAttachmentsServcs.update(bodyNewsAttach, {
            where: { id },
            returning: true
        });

    }

    async destroyNewsAttach(newsId: string): Promise<number> {

        await this.deleteImg(newsId);
        return await this._newsAttachmentsServcs.destroy({
            where: { id: newsId },
        });
    }

    private async deleteImg(id: string) {
        const img = await this._newsAttachmentsServcs.findByPk(id).then(item => item.newsAttachment).catch(err => err);

        const pathImagen = path.resolve(__dirname, `../../../../uploads/newsAttachment/${img}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }
}
