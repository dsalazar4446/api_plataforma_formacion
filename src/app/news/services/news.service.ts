import { Injectable, Inject } from '@nestjs/common';
import { News } from '../models/news.models';
import { NewsSaveJson, NewsUpdateJson } from '../interfaces/news.interfaces';
import { UserModel } from './../../user/models/user.Model';
import { NewsAttachments } from '../models/news-attachments.models';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services NewsService
 * @Creado 26 de Marzo 2019
 */

@Injectable()
export class NewsService {

    private language = ['es', 'en', 'it', 'pr'];

    constructor(
        @Inject('News') private readonly _newsServices: typeof News
    ) { }

    async saveNews(bodyNews: NewsSaveJson): Promise<NewsUpdateJson> {

        return await new this._newsServices(bodyNews).save();
    }

    async showAllNews(languageType: string): Promise<NewsUpdateJson[]> {

        if (this.language.indexOf(languageType) >= 0) {

            return await this._newsServices.findAll({
                include: [
                    {
                        as: 'userNews',
                        model: UserModel
                    },
                    {
                        model: NewsAttachments
                    }
                ],
                where: {
                    languageType
                }
            });
        }

        return;

    }

    async getDetails(newsId: string) {

        return await this._newsServices.findByPk(newsId, {
            include: [
                {
                    as: 'userNews',
                    model: UserModel
                },
                {
                    model: NewsAttachments
                }
            ]
        });
    }

    async updateNews(id: string, bodyNews: Partial<NewsUpdateJson>): Promise<[number, Array<any>]> {

        return await this._newsServices.update(bodyNews, {
            where: { id },
            returning: true
        });

    }

    async destroyNews(newsId: string): Promise<number> {

        return await this._newsServices.destroy({
            where: { id: newsId },
        });
    }

    async find() {

        return await this._newsServices.findAll({
            include: [
                {
                    as: 'userNews',
                    model: UserModel
                },
                {
                    model: NewsAttachments
                }
            ]
        });

    }


}
