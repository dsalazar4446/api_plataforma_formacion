import { Injectable, Inject } from "@nestjs/common";
import { ProductCategoriesSaveJson, ProductCategoriesUpdateJson } from "../interfaces/product-categories.interface";
import { ProductCategories } from "../models/produc-categories.model";
import { Products } from "../models/products.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services ProductCategories
 * @Creado 02 Abril 2019
 */

@Injectable()
export class ProductCategoriesServices {

    constructor(
        @Inject('ProductCategories') private readonly _productCategories: typeof ProductCategories
    ) { }

    async savepCategories(bodypCategories: ProductCategoriesSaveJson): Promise<ProductCategoriesUpdateJson> {

        return await new this._productCategories(bodypCategories).save();
    }

    async showAllpCategories(): Promise<ProductCategoriesUpdateJson[]> {

        return await this._productCategories.findAll();

    }

    async getDetails(pCategoriesId: string){

        return await this._productCategories.findByPk(pCategoriesId,{
            include: [
                {
                    model: Products
                }
            ]
        });
    }

    async updatepCategories(id: string, bodypCategories: Partial<ProductCategoriesUpdateJson>): Promise<[number, Array<any>]> {

        return await this._productCategories.update(bodypCategories, {
            where: { id },
            returning: true
        });

    }

    async destroypCategories(pCategoriesId: string): Promise<number> {

        return await this._productCategories.destroy({
            where: { id: pCategoriesId },
        });
    }

}