import { Injectable, Inject } from "@nestjs/common";
import { ProductMovements } from "../models/product-movements.model";
import { ProductMovementsUpdateJson, ProductMovementsSaveJson } from "../interfaces/product-movements.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services ProductMovementsServices
 * @Creado 02 Abril 2019
 */

@Injectable()
export class ProductMovementsServices {

    private productType = ['1', '2'];

    constructor(
        @Inject('ProductMovements') private readonly _productMovements: typeof ProductMovements
    ) { }

    async savepMovements(bodypMovements: ProductMovementsSaveJson): Promise<ProductMovementsUpdateJson> {

        return await new this._productMovements(bodypMovements).save();
    }

    async showAllpMovements(productMovementType: string): Promise<ProductMovementsUpdateJson[]> {

        if (this.productType.indexOf(productMovementType) >= 0) {

            return await this._productMovements.findAll({
                where: {
                    productMovementType
                }
            });
        }

    }

    async getDetails(pMovementsId: string){

        if (pMovementsId === undefined) {

            return await this.find();
        }

        return await this._productMovements.findByPk(pMovementsId);
    }

    async updatepMovements(id: string, bodypMovements: Partial<ProductMovementsUpdateJson>): Promise<[number, Array<any>]> {

        return await this._productMovements.update(bodypMovements, {
            where: { id },
            returning: true
        });

    }

    async destroypMovements(pMovementsId: string): Promise<number> {

        return await this._productMovements.destroy({
            where: { id: pMovementsId },
        });
    }

    async find() {

        return await this._productMovements.findAll();

    }

}