import { Injectable, Inject, HttpException, HttpStatus } from "@nestjs/common";
import { Products } from "../models/products.model";
import { ProductDetails } from "../models/products-details.model";
import { ProductMovements } from "../models/product-movements.model";
import { ProductPricings } from "../models/product-pricings.model";
import { ProductsUpdateJson, ProductsSaveJson } from "../interfaces/products.interface";
import * as ShortUniqueId from 'short-unique-id';
import { Sequelize } from "sequelize-typescript";
const uid = new ShortUniqueId();

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services ProductsServices
 * @Creado 26 de Marzo 2019
 */

@Injectable()
export class ProductsServices {

    private include = {
        include: [
            {
                model: ProductDetails
            },
            {
                model: ProductMovements
            },
            {
                model: ProductPricings
            }
        ]
    };

    constructor(
        @Inject('Products') private readonly _products: typeof Products
    ) { }

    async saveProducts(bodyProd: ProductsSaveJson): Promise<ProductsUpdateJson> {

        bodyProd.productCode = uid.randomUUID(9);
        const nuevo = await new this._products(bodyProd).save().catch(Sequelize.ForeignKeyConstraintError,
            error => {
                throw new HttpException('Violacion de clave foranea', HttpStatus.UNPROCESSABLE_ENTITY);
            }
        )
        return nuevo 
    }

    async showAllProducts(): Promise<ProductsUpdateJson[]> {

        return await this._products.findAll(this.include);
    }

    async showAllProductsDetails(languageType: string): Promise<ProductsUpdateJson[]> {

        return await this._products.findAll({
            include: [
                {
                    model: ProductDetails,
                    where: {
                        languageType
                    }
                },
            ]
        });
    }

    async getDetails(productsId: string) {

        return await this._products.findByPk(productsId, this.include);
    }

    async updateProducts(id: string, bodyProd: Partial<ProductsUpdateJson>): Promise<[number, Array<any>]> {

        return await this._products.update(bodyProd, {
            where: { id },
            returning: true
        });

    }

    async destroyProducts(productsId: string): Promise<number> {

        return await this._products.destroy({
            where: { id: productsId }
        });
    }

}