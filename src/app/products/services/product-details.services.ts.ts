import { Injectable, Inject } from "@nestjs/common";
import { ProductDetails } from "../models/products-details.model";
import { ProductDetailsSaveJson, ProductDetailsUpdateJson } from "../interfaces/product-details.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services ProductDetailsService
 * @Creado 02 Abril 2019
 */

@Injectable()
export class ProductDetailsService {

    private language = ['es', 'en', 'it', 'pr'];

    constructor(
        @Inject('ProductDetails') private readonly _productDetails: typeof ProductDetails
    ) { }

    async savepDetails(bodyPDetails: ProductDetailsSaveJson): Promise<ProductDetailsUpdateJson> {

        return await new this._productDetails(bodyPDetails).save();
    }

    async showAllPDetails(languageType: string): Promise<ProductDetailsUpdateJson[]> {

        if (this.language.indexOf(languageType) >= 0) {

            return await this._productDetails.findAll({
                where: {
                    languageType
                }
            });
        }

        return;

    }

    async getDetails(pDetailsId: string){

        return await this._productDetails.findByPk(pDetailsId);
    }

    async updatepDetails(id: string, bodyPDetails: Partial<ProductDetailsUpdateJson>): Promise<[number, Array<any>]> {

        return await this._productDetails.update(bodyPDetails, {
            where: { id }
        });

    }

    async destroypDetails(pDetailsId: string): Promise<number> {

        return await this._productDetails.destroy({
            where: { id: pDetailsId },
        });
    }

    async find() {

        return await this._productDetails.findAll();

    }

}