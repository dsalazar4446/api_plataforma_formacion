import { Injectable, Inject } from "@nestjs/common";
import { ProductPricings } from "../models/product-pricings.model";
import { ProductPricingsSaveJson, ProductPricingsUpdateJson } from "../interfaces/product-pricings.interface";
import { TransactionItemsModel } from "../../transactions/models/transaction-items.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services ProductPricings
 * @Creado 02 Abril 2019
 */

@Injectable()
export class ProductPricingsServices {

    constructor(
        @Inject('ProductPricings') private readonly _productPricings: typeof ProductPricings
    ) { }

    async savepPricings(bodypPricings: ProductPricingsSaveJson): Promise<ProductPricingsUpdateJson> {

        return await new this._productPricings(bodypPricings).save();
    }

    async showAllpPricings(): Promise<ProductPricingsUpdateJson[]> {

        return await this._productPricings.findAll();

    }

    async getDetails(pPricingsId: string):Promise<ProductPricingsSaveJson>{

        return await this._productPricings.findByPk(pPricingsId,{
            include: [
                {
                    model: TransactionItemsModel
                }
            ]
        });
    }

    async updatepPricings(id: string, bodypPricings: Partial<ProductPricingsUpdateJson>): Promise<[number, Array<any>]> {

        return await this._productPricings.update(bodypPricings, {
            where: { id },
            returning: true
        });

    }

    async destroypPricings(pPricingsId: string): Promise<number> {

        return await this._productPricings.destroy({
            where: { id: pPricingsId },
        });
    }

}