import { Module, Provider, HttpModule } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { ProductCategories } from './models/produc-categories.model';
import { Products } from './models/products.model';
import { ProductMovements } from './models/product-movements.model';
import { ProductDetails } from './models/products-details.model';
import { ProductPricings } from './models/product-pricings.model';
import { ProductsServices } from './services/products.services';
import { ProductPricingsServices } from './services/product-pricings.services';
import { ProductMovementsServices } from './services/product-movements.service';
import { ProductDetailsService } from './services/product-details.services.ts';
import { ProductCategoriesServices } from './services/product-categories.services';
import { ProductDetailsController } from './controllers/product-details.controller';
import { ProductMovementsController } from './controllers/product-movements.controller';
import { ProductPricingsController } from './controllers/product-pricings.controller';
import { ProductsController } from './controllers/products.controller';
import { ProductCategoriesController } from './controllers/product-categories.controller';

const models = [
  ProductCategories, ProductDetails, ProductMovements, ProductPricings, Products
];
const controllers = [
  ProductCategoriesController, ProductDetailsController, ProductMovementsController, ProductPricingsController, ProductsController 
];
const providers: Provider[] = [
  ProductCategoriesServices, ProductDetailsService, ProductMovementsServices, ProductPricingsServices, ProductsServices
];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers,
  exports:[
    ProductsServices,
    ProductCategoriesServices,
    ProductDetailsService,
    ProductPricingsServices,
  ]
})
export class ProductsModule { }
