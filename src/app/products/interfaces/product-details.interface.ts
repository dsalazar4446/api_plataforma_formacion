import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional, IsNumber } from 'class-validator';
export class ProductDetailsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'productId property must a be uuid'})
    @IsNotEmpty({message: 'productId property not must null'})
    productId: string;

    @ApiModelProperty()
    @IsString({message: 'productName property must a be string'})
    @IsNotEmpty({message: 'productName property not must null'})
    productName: string;

    @ApiModelProperty()
    @IsString({message: 'productDetail property must a be string'})
    @IsNotEmpty({message: 'productDetail property not must null'})
    productDetail: string;

    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsString({message: 'languageType property must a be string'})
    @IsOptional()
    languageType: string;
}
export class ProductDetailsUpdateJson extends ProductDetailsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
