import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsBoolean, IsString } from 'class-validator';
export class ProductCategoriesSaveJson {

    @ApiModelProperty()
    @IsString({message: 'productCategory property must a be string'})
    @IsNotEmpty({message: 'productCategory property not must null'})
    productCategory: string;

    @ApiModelProperty()
    @IsBoolean({message: 'productCategoryStatus property must a be boolean'})
    @IsNotEmpty({message: 'productCategoryStatus property not must null'})
    productCategoryStatus: boolean;
}
export class ProductCategoriesUpdateJson extends ProductCategoriesSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
