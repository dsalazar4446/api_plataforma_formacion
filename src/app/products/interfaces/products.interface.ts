import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsBoolean } from 'class-validator';
export class ProductsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'productCategoryId property must a be uuid'})
    @IsNotEmpty({message: 'productCategoryId property not must null'})
    productCategoryId: string;

    @ApiModelProperty()
    productCode?: string;

    @ApiModelProperty()
    @IsBoolean({message: 'productStatus property must a be boolean'})
    @IsNotEmpty({message: 'productStatus property not must null'})
    productStatus: boolean;
}
export class ProductsUpdateJson extends ProductsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
