import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsNumber, IsString, IsDate } from 'class-validator';
export class ProductMovementsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'productId property must a be uuid'})
    @IsNotEmpty({message: 'productId property not must null'})
    productId: string;

    @ApiModelProperty()
    @IsString({message: 'productMovementType property must a be string'})
    @IsNotEmpty({message: 'productMovementType property not must null'})
    productMovementType: string;

    @ApiModelProperty()
    @IsDate()
    @IsNotEmpty({message: 'productMovementDate property not must null'})
    productMovementDate: Date;

    @ApiModelProperty()
    @IsNumber(null,{message: 'productMovement property must a be number'})
    @IsNotEmpty({message: 'productMovement property not must null'})
    productMovement: number;
}
export class ProductMovementsUpdateJson extends ProductMovementsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
