import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsBoolean, IsNumber } from 'class-validator';
export class ProductPricingsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'productId property must a be uuid'})
    @IsNotEmpty({message: 'productId property not must null'})
    productId: string;

    @ApiModelProperty()
    @IsNumber(null,{message: 'productPrice property must a be number'})
    @IsNotEmpty({message: 'productPrice property not must null'})
    productPrice: number;

    @ApiModelProperty()
    @IsNumber(null,{message: 'productPriceDeliveryLocal property must a be number'})
    @IsNotEmpty({message: 'productPriceDeliveryLocal property not must null'})
    productPriceDeliveryLocal: number;

    @ApiModelProperty()
    @IsNumber(null,{message: 'productPriceDeliveryInternational property must a be number'})
    @IsNotEmpty({message: 'productPriceDeliveryInternational property not must null'})
    productPriceDeliveryInternational: number;

    @ApiModelProperty()
    @IsNumber(null,{message: 'productTaxRate property must a be number'})
    @IsNotEmpty({message: 'productTaxRate property not must null'})
    productTaxRate: number;

    @ApiModelProperty()
    @IsBoolean()
    @IsNotEmpty({message: 'productPriceStatus property not must null'})
    productPriceStatus: boolean;
    
    @ApiModelProperty()
    @IsNumber()
    @IsNotEmpty({ message: 'productPriceStatus property not must null' })
    productOddoId: number;
}
export class ProductPricingsUpdateJson extends ProductPricingsSaveJson{
    
    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
