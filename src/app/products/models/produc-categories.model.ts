import { Table, Model, Column, DataType, CreatedAt, UpdatedAt, HasMany } from "sequelize-typescript";
import { Products } from "./products.model";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

@Table({
    tableName: 'product_categories',
})
export class ProductCategories extends Model<ProductCategories>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_category',
    })
    productCategory: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_category_status',
    })
    productCategoryStatus: boolean;

    /**
     * RELACIONES
     * ProductCategories pertenece a Products
     */   

    @ApiModelProperty({
        type: () => Products,
        isArray: true
    })
    @HasMany(() => Products)
    products: Products[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}