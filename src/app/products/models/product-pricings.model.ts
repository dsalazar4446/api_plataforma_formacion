import { Table, Model, Column, DataType, CreatedAt, UpdatedAt, BelongsTo, ForeignKey, BelongsToMany } from "sequelize-typescript";
import { Products } from "./products.model";
import { TransacItemsHProductPricings } from "../../transactions/models/trans-items-h-prod-pric.model";
import { TransactionItemsModel } from "../../transactions/models/transaction-items.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({
    tableName: 'product_pricings',
})
export class ProductPricings extends Model<ProductPricings>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Products)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_id',
    })
    productId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.NUMERIC,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_price',
    })
    productPrice: number;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.NUMERIC,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_price_delivery_local',
    })
    productPriceDeliveryLocal: number;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.NUMERIC,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_price_delivery_international',
    })
    productPriceDeliveryInternational: number;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.NUMERIC,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_tax_rate',
    })
    productTaxRate: number;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_price_status',
    })
    productPriceStatus: boolean;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_oddo_id',
    })
    productOddoId: number;


    /**
     * RELACIONES
     * ProductPricings pertenece a Products
     * ProductPricings pertenece a muchos TransactionItemsModel
     */    
   
     
    @BelongsTo(() => Products)
    products: Products;
    
    @ApiModelPropertyOptional({
        type: TransactionItemsModel,
        isArray: false,
        example: [
            {
                "id":"UUID",
                "billingTransactionsId":"UUID",
                "userId":"UUID",
                "transactionItemType":"ENUM",
                "transactionItemCant":"number",
                "transactionItemAmmount":"number",
                "transactionItemTaxes":"number",
                "transactionItemTotal":"number"
            }
        ]
    })
    @BelongsToMany(() => TransactionItemsModel, () => TransacItemsHProductPricings)
    transactionItemsModel: TransactionItemsModel[];


    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}