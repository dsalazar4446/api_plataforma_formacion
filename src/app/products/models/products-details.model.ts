import { Table, Model, Column, DataType, CreatedAt, UpdatedAt, BelongsTo, ForeignKey } from "sequelize-typescript";
import { Products } from "./products.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({
    tableName: 'product_details',
})
export class ProductDetails extends Model<ProductDetails>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Products)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_id',
    })
    productId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_name',
    })
    productName: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_detail',
    })
    productDetail: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es','en','it','pr'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;

    /**
     * RELACIONES
     * ProductDetails pertenece a Products
     */    
    
    @BelongsTo(() => Products)
    products: Products;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;



}