import { Table, Model, Column, DataType, CreatedAt, UpdatedAt, BelongsTo, ForeignKey } from "sequelize-typescript";
import { Products } from "./products.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({
    tableName: 'product_movements',
})
export class ProductMovements extends Model<ProductMovements>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Products)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_id',
    })
    productId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1','2'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_movement_type',
    })
    productMovementType: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_movement_date',
    })
    productMovementDate: Date;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.NUMERIC,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_movement',
    })
    productMovement: number;

    /**
     * RELACIONES
     * ProductMovements pertenece a Products
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => Products)
    products: Products;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;



}