import { Table, Model, Column, DataType, CreatedAt, UpdatedAt, BelongsTo, ForeignKey, HasMany } from "sequelize-typescript";
import { ProductCategories } from "./produc-categories.model";
import { ProductDetails } from "./products-details.model";
import { ProductMovements } from "./product-movements.model";
import { ProductPricings } from "./product-pricings.model";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

@Table({
    tableName: 'products',
})
export class Products extends Model<Products>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => ProductCategories)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_category_id',
    })
    productCategoryId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(9),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_code',
    })
    productCode: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        defaultValue: false,
        field: 'product_status',
    })
    productStatus: boolean;

    /**
     * RELACIONES
     * Products pertenece a  ProductCategories
     * Products tiene muchos ProductDetails
     * Products tiene muchos ProductMovements
     * Products tiene muchos ProductPricings
     */
    
    
    @ApiModelPropertyOptional({
        type: () => ProductDetails,
        isArray: true
    })
    @HasMany(() => ProductDetails)
    productDetails: ProductDetails[];
    
    @ApiModelPropertyOptional({
        type: () => ProductMovements,
        isArray: true
    })
    @HasMany(() => ProductMovements)
    productMovements: ProductMovements[];
    
    @ApiModelPropertyOptional({
        type: () => ProductPricings,
        isArray: true
    })
    @HasMany(() => ProductPricings)
    productPricings: ProductPricings[];
    
    @BelongsTo(() => ProductCategories)
    productCategories: ProductCategories;

    
    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}