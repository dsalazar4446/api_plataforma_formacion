import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, NotFoundException, Put, Delete, Param } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ProductPricingsServices } from "../services/product-pricings.services";
import { ProductPricingsSaveJson, } from "../interfaces/product-pricings.interface";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { ProductPricings } from "../models/product-pricings.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller ProductPricingsController
 * @Creado 03 Abril 2019
 */

@ApiUseTags('Module-Products')
@Controller('product-pricings')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class ProductPricingsController {

    constructor(private readonly _productPricingsServices: ProductPricingsServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: ProductPricings,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyProdPri: ProductPricingsSaveJson) {

        const createProdPri = await this._productPricingsServices.savepPricings(bodyProdPri);

        if (!createProdPri) {
            throw this.appUtilsService.httpCommonError('Product Pricings no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM127',
                languageType: 'es',
            });
        }

        return createProdPri;

    }


    @ApiResponse({
        status: 200,
        type: ProductPricings,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllProd() {
        const fetchProdAll = await this._productPricingsServices.showAllpPricings();
        if (!fetchProdAll[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return fetchProdAll;
    }


    @ApiResponse({
        status: 200,
        type: ProductPricings,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idProdPri: string) {

        const fetchProdPri = await this._productPricingsServices.getDetails(idProdPri);

        if (!fetchProdPri) {
            throw this.appUtilsService.httpCommonError('Product Pricings no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM128',
                languageType: 'es',
            });
        }

        return fetchProdPri;
    }


    @ApiResponse({
        status: 200,
        type: ProductPricings,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateProdPri(@Param('id') id: string, @Body() bodyProdPri: Partial<ProductPricingsSaveJson>) {

        const updateProdPri = await this._productPricingsServices.updatepPricings(id, bodyProdPri);

        if (!updateProdPri[1][0]) {
            throw this.appUtilsService.httpCommonError('Product Pricings no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM128',
                languageType: 'es',
            });
        }

        return updateProdPri;
    }


    @ApiResponse({
        status: 200,
        type: ProductPricings,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteProdPri(@Param('id') idProdPri: string) {

        const deleteProdPri = await this._productPricingsServices.destroypPricings(idProdPri);

        if (!deleteProdPri) {
            throw this.appUtilsService.httpCommonError('Product Pricings no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM128',
                languageType: 'es',
            });
        }

        return deleteProdPri;
    }
}