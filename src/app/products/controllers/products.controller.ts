import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, NotFoundException, Put, Delete, Param, Req } from "@nestjs/common";
import { ProductsServices } from "../services/products.services";
import { ProductsSaveJson } from "../interfaces/products.interface";
import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { Products } from "../models/products.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller productsController
 * @Creado 02 Abril 2019
 */

@ApiUseTags('Module-Products')
@Controller('products')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class ProductsController {

    constructor(private readonly _productsServices: ProductsServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: Products,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyProd: ProductsSaveJson) {

        const createProd = await this._productsServices.saveProducts(bodyProd);

        if (!createProd) {
            throw this.appUtilsService.httpCommonError('Product no ha sido creado', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM129',
                languageType: 'es',
            });
        }

        return createProd;

    }


    @ApiResponse({
        status: 200,
        type: Products,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAllProd() {
        return await this._productsServices.showAllProducts();
    }


    @ApiResponse({
        status: 200,
        type: Products,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllProdDeta(@Req() req) {
        const fetchAll = await this._productsServices.showAllProductsDetails(req.headers.language);

        if (!fetchAll[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetchAll;
    }


    @ApiResponse({
        status: 200,
        type: Products,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idProd: string) {

        const fetchProd = await this._productsServices.getDetails(idProd);

        if (!fetchProd) {
            throw this.appUtilsService.httpCommonError('Product no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM130',
                languageType: 'es',
            });
        }

        return fetchProd;
    }

    
    @ApiResponse({
        status: 200,
        type: Products,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateProd(@Param('id') id: string, @Body() bodyProd: Partial<ProductsSaveJson>) {

        const updateProd = await this._productsServices.updateProducts(id, bodyProd);

        if (!updateProd) {
            throw this.appUtilsService.httpCommonError('Product no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM130',
                languageType: 'es',
            });
        }

        return updateProd;
    }


    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteProd(@Param('id') idProd: string) {

        const deleteProd = await this._productsServices.destroyProducts(idProd);

        if (!deleteProd) {
            throw this.appUtilsService.httpCommonError('Product no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM130',
                languageType: 'es',
            });
        }

        return deleteProd;
    }

}