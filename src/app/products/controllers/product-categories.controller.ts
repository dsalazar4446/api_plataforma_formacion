import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, NotFoundException, Put, Delete, Param } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ProductCategoriesServices } from "../services/product-categories.services";
import { ProductCategoriesSaveJson, } from "../interfaces/product-categories.interface";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { ProductCategories } from "../models/produc-categories.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller ProductCategoriesController
 * @Creado 02 Abril 2019
 */

@ApiUseTags('Module-Products')
@Controller('product-categories')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class ProductCategoriesController {
    constructor(private readonly _productCategoriesServices: ProductCategoriesServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: ProductCategories,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyProdCat: ProductCategoriesSaveJson) {

        const createProdCat = await this._productCategoriesServices.savepCategories(bodyProdCat);

        if (!createProdCat) {
            throw this.appUtilsService.httpCommonError('Product Categories no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM121',
                languageType: 'es',
            });
        }

        return createProdCat;

    }


    @ApiResponse({
        status: 200,
        type: ProductCategories,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllProd() {
        return await this._productCategoriesServices.showAllpCategories();
    }


    @ApiResponse({
        status: 200,
        type: ProductCategories,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idProdCat: string) {

        const fetchProdCat = await this._productCategoriesServices.getDetails(idProdCat);

        if (!fetchProdCat) {
            throw this.appUtilsService.httpCommonError('Product Categories no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM122',
                languageType: 'es',
            });
        }

        return fetchProdCat;
    }


    @ApiResponse({
        status: 200,
        type: ProductCategories,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateProdCat(@Param('id') id: string, @Body() bodyProdCat: Partial<ProductCategoriesSaveJson>) {

        const updateProdCat = await this._productCategoriesServices.updatepCategories(id, bodyProdCat);

        if (!updateProdCat[1][0]) {
            throw this.appUtilsService.httpCommonError('Product Categories no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM122',
                languageType: 'es',
            });
        }

        return updateProdCat;
    }


    @ApiResponse({
        status: 200,
        type: ProductCategories,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteProdCat(@Param('id') idProd: string) {

        const deleteProdCat = await this._productCategoriesServices.destroypCategories(idProd);

        if (!deleteProdCat) {
            throw this.appUtilsService.httpCommonError('Product Categories no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM122',
                languageType: 'es',
            });
        }

        return deleteProdCat;
    }

}