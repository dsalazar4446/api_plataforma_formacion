import { Controller, UseFilters, UseInterceptors, Post, UsePipes, HttpStatus, Body, Get, NotFoundException, Put, Delete, Param, Req } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ProductDetailsService } from "../services/product-details.services.ts";
import { ProductDetailsSaveJson } from "../interfaces/product-details.interface";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { ProductDetails } from "../models/products-details.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller ProductDetailsController
 * @Creado 02 Abril 2019
 */

@ApiUseTags('Module-Products')
@Controller('product-details')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class ProductDetailsController {

    constructor(private readonly _productDetailsService: ProductDetailsService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: ProductDetails,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Req() req, @Body() bodyProdDetails: ProductDetailsSaveJson) {

        if (req.headers.language) {
            bodyProdDetails.languageType = req.headers.language;
        } else {
            bodyProdDetails.languageType = 'es';
        }

        const createProdDetails = await this._productDetailsService.savepDetails(bodyProdDetails);

        if (!createProdDetails) {
            throw this.appUtilsService.httpCommonError('Product Details no ha sido creado', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM123',
                languageType: 'es',
            });
        }

        return createProdDetails;

    }


    @ApiResponse({
        status: 200,
        type: ProductDetails,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idProdDetails: string) {

        const fetchProdDetails = await this._productDetailsService.getDetails(idProdDetails);

        if (!fetchProdDetails) {
            throw this.appUtilsService.httpCommonError('Product Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM124',
                languageType: 'es',
            });
        }

        return fetchProdDetails;
    }


    @ApiResponse({
        status: 200,
        type: ProductDetails,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAll() {

        const fetchAlll = await this._productDetailsService.find();

        if (!fetchAlll[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetchAlll;
    }


    @ApiResponse({
        status: 200,
        type: ProductDetails,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllProd(@Req() req) {
        const fetchAll = await this._productDetailsService.showAllPDetails(req.headers.language);

        if (!fetchAll[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetchAll;
    }


    @ApiResponse({
        status: 200,
        type: ProductDetails,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateProdDetails(@Req() req, @Param('id') id: string, @Body() bodyProdDetails: Partial<ProductDetailsSaveJson>) {

        if (req.headers.language) {
            bodyProdDetails.languageType = req.headers.language;
        }

        const updateProdDetails = await this._productDetailsService.updatepDetails(id, bodyProdDetails);

        if (!updateProdDetails[1][0]) {
            throw this.appUtilsService.httpCommonError('Product Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM124',
                languageType: 'es',
            });
        }

        return updateProdDetails;
    }


    @ApiResponse({
        status: 200,
        type: ProductDetails,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteProdDetails(@Param('id') idProdDetalis: string) {

        const deleteProdDetails = await this._productDetailsService.destroypDetails(idProdDetalis);

        if (!deleteProdDetails) {
            throw this.appUtilsService.httpCommonError('Product Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM124',
                languageType: 'es',
            });
        }

        return deleteProdDetails;
    }





}