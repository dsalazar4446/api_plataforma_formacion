import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, NotFoundException, Put, Delete, Param } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ProductMovementsServices } from "../services/product-movements.service";
import { ProductMovementsSaveJson } from "../interfaces/product-movements.interface";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { ProductMovements } from "../models/product-movements.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller ProductMovementsController
 * @Creado 02 Abril 2019
 */

@ApiUseTags('Module-Products')
@Controller('product-movements')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class ProductMovementsController {

    constructor(private readonly _productMovementsServices: ProductMovementsServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: ProductMovements,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyProdMove: ProductMovementsSaveJson) {

        const createProdMovem = await this._productMovementsServices.savepMovements(bodyProdMove);

        if (!createProdMovem) {
            throw this.appUtilsService.httpCommonError('Product Movements no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM125',
                languageType: 'es',
            });
        }

        return createProdMovem;

    }


    @ApiResponse({
        status: 200,
        type: ProductMovements,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idProd: string) {

        const fetchProdMovemDetails = await this._productMovementsServices.getDetails(idProd);

        if (!fetchProdMovemDetails) {
            throw this.appUtilsService.httpCommonError('Product Movements no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM126',
                languageType: 'es',
            });
        }

        return fetchProdMovemDetails;
    }


    @ApiResponse({
        status: 200,
        type: ProductMovements,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAll() {

        const fetchProdMove = await this._productMovementsServices.find();

        if (!fetchProdMove[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetchProdMove;
    }


    @ApiResponse({
        status: 200,
        type: ProductMovements,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get(':productMovementType')
    @ApiImplicitParam({ name: 'productMovementType', required: true, type: 'string' })
    async showAllProd(@Param('productMovementType') productMovementType: string) {
        const fetchAllProdMovem = await this._productMovementsServices.showAllpMovements(productMovementType);

        if (!fetchAllProdMovem[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetchAllProdMovem;
    }


    @ApiResponse({
        status: 200,
        type: ProductMovements,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateProdMovem(@Param('id') id: string, @Body() bodyProdMovem: Partial<ProductMovementsSaveJson>) {

        const updateProdMovem = await this._productMovementsServices.updatepMovements(id, bodyProdMovem);

        if (!updateProdMovem[1][0]) {
            throw this.appUtilsService.httpCommonError('Product Movements no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM126',
                languageType: 'es',
            });
        }

        return updateProdMovem;
    }


    @ApiResponse({
        status: 200,
        type: ProductMovements,
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteProdMovem(@Param('id') idProdMovem: string) {

        const deleteProdMovem = await this._productMovementsServices.destroypMovements(idProdMovem);

        if (!deleteProdMovem) {
            throw this.appUtilsService.httpCommonError('Product Movements no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM126',
                languageType: 'es',
            });
        }

        return deleteProdMovem;
    }

}