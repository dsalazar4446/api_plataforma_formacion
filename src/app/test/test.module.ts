import { Module, HttpModule, Provider } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';

import { UserTestController } from './controllers/user-test/user-test.controller';
import { FormationTestController } from './controllers/formation-test/formation-test.controller';
import { UserTestHasQuestionsOptionsController } from './controllers/user-test-has-questions-options/user-test-has-questions-options.controller';
import { TestDetailsController } from './controllers/test-details/test-details.controller';
import { QuestionDetailsController } from './controllers/question-details/question-details.controller';
import { QuestionOptionsController } from './controllers/question-options/question-options.controller';
import { QuestionOptionDetailsController } from './controllers/question-option-details/question-option-details.controller';
import { QuestionAttachmentsController } from './controllers/question-attachments/question-attachments.controller';
import { QuestionController } from './controllers/question/question.controller';
import { FormationClassesHasFormationTestsController } from './controllers/formation-classes-has-formation-test.controller';
import { CoursesHasFormationTestsController } from './controllers/courses-has-formation-test.controller';

import { UserTestService } from './services/user-test/user-test.service';
import { FormationTestService } from './services/formation-test/formation-test.service';
import { UserTestHasQuestionsOptionsService } from './services/user-test-has-questions-options/user-test-has-questions-options.service';
import { TestDetailsService } from './services/test-details/test-details.service';
import { QuestionService } from './services/question/question.service';
import { QuestionOptionsService } from './services/question-options/question-options.service';
import { QuestionOptionDetailsService } from './services/question-option-details/question-option-details.service';
import { QuestionAttachmentsService } from './services/question-attachments/question-attachments.service';
import { QuestionDetailsService } from './services/question-details/question-details.service';
import { CoursesHasFormationTestsService } from './services/courses-has-formation-test.service';
import { FormationClassesHasFormationTestsService } from './services/formation-classes-has-formation-test.services';


import { FormationTest } from './models/formation-test.models';
import { QuestionsAttachments } from './models/question-attachments.models';
import { UserTestHasQuestionOptions } from './models/user-tests-has-questions-options.models';
import { TestDetails } from './models/test-details.models';
import { Questions } from './models/question.models';
import { QuestionOptions } from './models/question-options.models';
import { QuestionOptionDetails } from './models/question-options.details.models';
import { QuestionDetails } from './models/question-details.models';
import { UserTest } from './models/user-tests.models';
import { FormationClassesHasFormationTests } from './models/formation-classes-has-formation-test.model';
import { CoursesHasFormationTests } from './models/courses-has-formation-test.model';
import { QuestionsHasMainVideos } from './models/question-has-main-videos.model';
import { QuestionsHasMainVideosService } from './services/question-has-main-videos.service';
import { QuestionsHasMainVideosController } from './controllers/question-has-main-videos.controller';
import { UserModule } from '../user/user.module';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @module TestModule
 * @Creado 13 de Marzo 2019
 */

const models = [
  FormationTest,
  QuestionsAttachments,
  QuestionDetails,
  QuestionOptionDetails,
  QuestionOptions,
  Questions,
  UserTestHasQuestionOptions,
  TestDetails,
  UserTest,
  FormationClassesHasFormationTests,
  CoursesHasFormationTests,
  QuestionsHasMainVideos
];
const controllers = [
  FormationTestController,
  UserTestController,
  UserTestHasQuestionsOptionsController,
  TestDetailsController,
  QuestionController,
  QuestionDetailsController,
  QuestionOptionsController,
  QuestionOptionDetailsController,
  QuestionAttachmentsController,
  FormationClassesHasFormationTestsController,
  CoursesHasFormationTestsController,
  QuestionsHasMainVideosController
];
const providers: Provider[] = [
  FormationTestService,
  UserTestService,
  UserTestHasQuestionsOptionsService,
  TestDetailsService,
  QuestionService,
  QuestionOptionsService,
  QuestionOptionDetailsService,
  QuestionAttachmentsService,
  QuestionDetailsService,
  CoursesHasFormationTestsService,
  FormationClassesHasFormationTestsService,
  QuestionsHasMainVideosService
];

@Module({
  imports: [
    UserModule,
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers
})
export class TestModule { }
