import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class UserTestHasQuestionsOptionsSaveJson{

    @ApiModelProperty()
    @IsUUID('4',{message: 'userTestId property must a be uuid'})
    @IsNotEmpty({message: 'userTestId property not must null'})
    userTestId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'questionOptionId property must a be uuid'})
    @IsNotEmpty({message: 'questionOptionId property not must null'})
    questionOptionId: string;

}

export class UserTestHasQuestionsOptionsUpdateJson extends UserTestHasQuestionsOptionsSaveJson {

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}