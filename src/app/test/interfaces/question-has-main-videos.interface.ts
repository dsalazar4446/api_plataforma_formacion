import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class QuestionsHasMainVideosSaveJson{

    @ApiModelProperty()
    @IsUUID('4',{message: 'questionId property must a be uuid'})
    @IsNotEmpty({message: 'questionId property not must null'})
    questionId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'mainVideoId property must a be uuid'})
    @IsNotEmpty({message: 'mainVideoId property not must null'})
    mainVideoId: string;

}