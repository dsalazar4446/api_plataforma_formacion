import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional } from 'class-validator';
export class QuestionAttachmentsSaveJson{

    @ApiModelProperty()
    @IsUUID('4',{message: 'questionId property must a be uuid'})
    @IsNotEmpty({message: 'questionId property not must null'})
    questionId: string;

    @ApiModelProperty()
    @IsString({message: 'questionsAttachment property must a be string'})
    @IsNotEmpty({message: 'questionId property not must null'})
    questionsAttachment: string;

    @ApiModelProperty()
    @IsString({message: 'questionsAttachmentType property must a be string'})
    @IsNotEmpty({message: 'questionsAttachmentType property not must null'})
    questionsAttachmentType: string;    
    
}

export class QuestionAttachmentsUpdateJson extends QuestionAttachmentsSaveJson {

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}