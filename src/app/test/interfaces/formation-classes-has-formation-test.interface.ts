import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class FormationClassesHasFormationTestSaveJson{

    @ApiModelProperty()
    @IsUUID('4',{message: 'formationClassId property must a be uuid'})
    @IsNotEmpty({message: 'formationClassId property not must null'})
    formationClassId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'formationTestId property must a be uuid'})
    @IsNotEmpty({message: 'formationTestId property not must null'})
    formationTestId: string;

}