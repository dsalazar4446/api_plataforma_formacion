import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional } from 'class-validator';
export class TestDetailSaveJson{
    
    @ApiModelProperty()
    @IsUUID('4',{message: 'formationTestId property must a be uuid'})
    @IsNotEmpty({message: 'formationTestId property not must null'})
    formationTestId: string;

    @ApiModelProperty()
    @IsString({message: 'testTitle property must a be string'})
    @IsNotEmpty({message: 'testTitle property not must null'})
    testTitle: string;

    @ApiModelProperty()
    @IsString({message: 'testDescription property must a be string'})
    @IsNotEmpty({message: 'testDescription property not must null'})
    testDescription: string;

    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsString({message: 'languageType property must a be string'})
    @IsOptional()
    languageType: string;
}

export class TestDetailUpdateJson extends TestDetailSaveJson {

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}