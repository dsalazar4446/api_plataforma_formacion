import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsNumber, IsString, IsOptional, IsDate } from 'class-validator';
export class FormationTestSaveJson{

    @ApiModelProperty()
    @IsOptional()
    testCode?: string;

    @ApiModelProperty()
    @IsNumber(null,{message: 'testTryOuts property must a be number'})
    @IsNotEmpty({message: 'testResponseTime property not must null'})
    testResponseTime: number;

    @ApiModelProperty()
    @IsNumber(null,{message: 'testTryOuts property must a be number'})
    @IsNotEmpty({message: 'testTryOuts property not must null'})
    testTryOuts: number;

    @ApiModelProperty()
    @IsNumber(null,{message: 'scoreForAproval property must a be number'})
    @IsNotEmpty({message: 'scoreForAproval property not must null'})
    scoreForAproval: number;

    @ApiModelProperty()
    @IsString({message: 'testStatus property must a be string'})
    @IsNotEmpty({message: 'testStatus property not must null'})
    testStatus: string;

    @IsOptional()
    countUserTest?: number;

    @IsOptional()
    userTest?: any;

}

export class FormationTestUpdateJson extends FormationTestSaveJson {
    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}