import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional } from 'class-validator';
export class QuestionOptionDetailsSaveJson{

    @ApiModelProperty()
    @IsUUID('4',{message: 'questionOptionId property must a be uuid'})
    @IsNotEmpty({message: 'questionOptionId property not must null'})
    questionOptionId: string;

    @ApiModelProperty()
    @IsString({message: 'questionOptionDetail property must a be string'})
    @IsNotEmpty({message: 'questionOptionDetail property not must null'})
    questionOptionDetail: string;

    @ApiModelProperty()
    @IsString({message: 'languageType property must a be string'})
    @IsOptional()
    languageType: string;    
    
}

export class QuestionOptionDetailsUpdateJson extends QuestionOptionDetailsSaveJson {

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}