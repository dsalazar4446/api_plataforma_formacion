import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsNotEmpty, IsNumber } from 'class-validator';
export class QuestionOptionsSaveJson{

    @ApiModelProperty()
    @IsUUID('4',{message: 'questionId property must a be uuid'})
    @IsNotEmpty({message: 'questionId property not must null'})
    questionId: string;

    @ApiModelProperty()
    @IsNumber(null,{message: 'questionOptionScore property must a be number'})
    @IsNotEmpty({message: 'questionOptionScore property not must null'})
    questionOptionScore: number;
    
}

export class QuestionOptionsUpdateJson extends QuestionOptionsSaveJson {

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}