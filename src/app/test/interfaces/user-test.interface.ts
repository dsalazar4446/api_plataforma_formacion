import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsBoolean, IsNumber, IsOptional, IsDate } from 'class-validator';
export class UserTestSaveJson{

    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    userId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'formationTestId property must a be string'})
    @IsNotEmpty({message: 'formationTestId property not must null'})
    formationTestId: string;

    @ApiModelProperty()
    @IsDate()
    @IsNotEmpty({message: 'testStart property not must null'})
    testStart: Date;

    @ApiModelProperty()
    @IsDate()
    @IsNotEmpty({message: 'testEnd property not must null'})
    testEnd: Date;

    @ApiModelProperty()
    @IsBoolean({message: 'testStatus property must a be boolean'})
    @IsNotEmpty({message: 'testStatus property not must null'})
    testStatus: boolean;

    @ApiModelProperty()
    @IsNumber(null,{message: 'scoreObtained property must a be number'})
    @IsNotEmpty({message: 'scoreObtained property not must null'})
    scoreObtained: number;

}

export class UserTestUpdateJson extends UserTestSaveJson {

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}