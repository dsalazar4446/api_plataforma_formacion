import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional } from 'class-validator';
export class QuestionDetailsSaveJson{

    @ApiModelProperty()
    @IsUUID('4',{message: 'questionId property must a be uuid'})
    @IsNotEmpty({message: 'questionId property not must null'})
    questionId: string;

    @ApiModelProperty()
    @IsString({message: 'question property must a be string'})
    @IsNotEmpty({message: 'question property not must null'})
    question: string;

    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsString({message: 'languageType property must a be string'})
    @IsOptional()
    languageType: string;    
    
}

export class QuestionDetailsUpdateJson extends QuestionDetailsSaveJson {

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}