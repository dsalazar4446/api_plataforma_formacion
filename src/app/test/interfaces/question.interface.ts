import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString } from 'class-validator';
export class QuestionSaveJson{

    @ApiModelProperty()
    @IsUUID('4',{message: 'formationTestId property must a be uuid'})
    @IsNotEmpty({message: 'formationTestId property not must null'})
    formationTestId: string;

    @ApiModelProperty()
    @IsString({message: 'questionType property must a be string'})
    @IsNotEmpty({message: 'questionType property not must null'})
    questionType: string;
    
    
}

export class QuestionUpdateJson extends QuestionSaveJson {
    
    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}