import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class CoursesHasFormationTestsSaveJson{

    @ApiModelProperty()
    @IsUUID('4',{message: 'courseId property must a be uuid'})
    @IsNotEmpty({message: 'courseId property not must null'})
    courseId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'formationTestId property must a be uuid'})
    @IsNotEmpty({message: 'formationTestId property not must null'})
    formationTestId: string;

}