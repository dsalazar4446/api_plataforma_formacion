export const FormationTestExample = 
{
    "id": "string",
    "classesId": "string",
    "testCode": "string",
    "testResponseTime": "Date",
    "testTryOuts": "number",
    "scoreForAproval": "number",
    "testStatus": "string"
}

export const FormationTestArrayExample = 
[
    FormationTestExample
]
