import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Questions } from './question.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';


@Table({
    tableName: 'question_attachments',
})
export class QuestionsAttachments extends Model<QuestionsAttachments>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Questions)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'question_id',
    })
    questionId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'question_attachment',
    })
    questionsAttachment: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2', '3'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'question_attachment_type',
    })
    questionsAttachmentType: string;

    /**
     * RELACIONES
     * QuestionsAttachments pertenece a Questions
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => Questions)
    questions: Questions;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}