import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany, ForeignKey, BelongsTo, BelongsToMany } from 'sequelize-typescript';
import { Questions } from './question.models';
import { QuestionOptionDetails } from './question-options.details.models';
import { UserTestHasQuestionOptions } from './user-tests-has-questions-options.models';
import { UserTest } from './user-tests.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';


@Table({
    tableName: 'question_options',
})
export class QuestionOptions extends Model<QuestionOptions>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Questions)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'question_id',
    })
    questionId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.FLOAT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'question_option_score',
    })
    questionOptionScore: number;
 

    @BelongsTo(() => Questions)
    questions: Questions;

    @ApiModelPropertyOptional({
        type: QuestionOptionDetails,
        isArray: false
    })
    @HasMany(() => QuestionOptionDetails)
    questionOptionDetails: QuestionOptionDetails[];


    // @ApiModelPropertyOptional({
    //     type: UserTest,
    //     isArray: false
    // })
    @BelongsToMany(() => UserTest, {
        through: {
            model: () => UserTestHasQuestionOptions,
            unique: true,
        }
    })
    userTest: UserTest[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}