import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { FormationTest } from './formation-test.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';


@Table({
    tableName: 'test_details'
})
export class TestDetails extends Model<TestDetails>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;


    @ApiModelPropertyOptional()
    @ForeignKey(() => FormationTest)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'formation_test_id',
    })
    formationTestId: string;

    
    
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'test_title',
    })
    testTitle: string;
    
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'test_description',
    })
    testDescription: string;
    
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'ir', 'pr'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;

    /**
     * RELACIONES
     * TestDetails pertenece a Test
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => FormationTest)
    formationTest: FormationTest;
    
    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
    
}
