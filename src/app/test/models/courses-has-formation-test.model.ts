import { Table, Model, DataType, Column,ForeignKey,  } from 'sequelize-typescript';
import { FormationTest } from './formation-test.models';
import { CoursesModel } from '../../courses/models/courses.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';


@Table({
    tableName: 'courses_has_formation_tests',
})
export class CoursesHasFormationTests extends Model<CoursesHasFormationTests>{

    @ApiModelPropertyOptional()
    @ForeignKey(() => CoursesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'course_id',
    })
    courseId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => FormationTest)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'formation_test_id',
    })
    formationTestId: string;

}