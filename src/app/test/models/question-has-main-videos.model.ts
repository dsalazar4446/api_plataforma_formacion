import { Table, Model, DataType, Column,ForeignKey } from 'sequelize-typescript';
import { Questions } from './question.models';
import { MainVideosModel } from '../../main-videos/models/main-videos.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'questions_has_main_videos',
})
export class QuestionsHasMainVideos extends Model<QuestionsHasMainVideos>{

    @ApiModelPropertyOptional()
    @ForeignKey(() => Questions)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'question_id',
    })
    questionId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => MainVideosModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'main_video_id',
    })
    mainVideoId: string;

}