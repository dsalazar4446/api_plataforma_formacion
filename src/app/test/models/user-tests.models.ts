import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany, ForeignKey, BelongsTo, BelongsToMany } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { FormationTest } from './formation-test.models';
import { UserTestHasQuestionOptions } from './user-tests-has-questions-options.models';
import { QuestionOptions } from './question-options.models';
import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

@Table({
    tableName: 'user_tests',
})
export class UserTest extends Model<UserTest>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => FormationTest)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'formation_test_id',
    })
    formationTestId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TIME,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'test_start',
    })
    testStart: Date;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TIME,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'test_end',
    })
    testEnd: Date;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'test_status',
    })
    testStatus: boolean;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.FLOAT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'score_obtained',
    })
    scoreObtained: number;

    @ApiModelPropertyOptional()
    @BelongsTo(() => UserModel)
    userModel: UserModel;

    @ApiModelPropertyOptional()
    @BelongsTo(() => FormationTest)
    formationTest: FormationTest;

    // @ApiModelPropertyOptional(
        // {
        //     example: [
        //         {
        //             "id": "string",
        //             "questionId": "string",
        //             "questionOptionScore": "number",
        //         }
        //     ]
        // }
    // )
    @BelongsToMany(() => QuestionOptions, {
        through: {
            model: () => UserTestHasQuestionOptions
        }
    })
    questionOptions: QuestionOptions[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}