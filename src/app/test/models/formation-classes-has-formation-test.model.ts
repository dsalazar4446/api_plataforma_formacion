import { Table, Model, DataType, Column,ForeignKey,  } from 'sequelize-typescript';
import { FormationTest } from './formation-test.models';
import { FormationClassesModel } from '../../classes/models/formation-classes.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';


@Table({
    tableName: 'formation_classes_has_formation_tests',
})
export class FormationClassesHasFormationTests extends Model<FormationClassesHasFormationTests>{

    @ApiModelPropertyOptional()
    @ForeignKey(() => FormationClassesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'formation_class_id',
    })
    formationClassId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => FormationTest)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'formation_test_id',
    })
    formationTestId: string;

}