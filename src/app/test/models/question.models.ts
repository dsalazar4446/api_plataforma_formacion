import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany, ForeignKey, BelongsTo, BelongsToMany } from 'sequelize-typescript';
import { QuestionOptions } from './question-options.models';
import { QuestionDetails} from './question-details.models';
import { QuestionsAttachments} from './question-attachments.models';
import { FormationTest } from './formation-test.models';
import { QuestionsHasMainVideos } from './question-has-main-videos.model';
import { MainVideosModel } from '../../main-videos/models/main-videos.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';


@Table({
    tableName: 'questions',
})
export class Questions extends Model<Questions>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => FormationTest)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'formation_test_id',
    })
    formationTestId: string;

    
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'question_type',
    })
    questionType: string;
    
    /**
     * RELACIONES
     * Questions pertenece a Test
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => FormationTest)
    formationTest: FormationTest;

    /**
     * RELACIONES
     * Questions tiene muchos QuestionsAttachments
     * Questions tiene muchos QuestionOptions
     * Questions tiene muchos QuestionDetails
     */    
    @ApiModelPropertyOptional({
        type: QuestionsAttachments,
        isArray: true
    })
    @HasMany(() => QuestionsAttachments)
    questionsAttachments: QuestionsAttachments[];
    
    @ApiModelPropertyOptional({
        type: QuestionOptions,
        isArray: true
    })
    @HasMany(() => QuestionOptions)
    questionOptions: QuestionOptions[];
    
    @ApiModelPropertyOptional({
        type: QuestionDetails,
        isArray: true
    })
    @HasMany(() => QuestionDetails)
    questionDetails: QuestionDetails[];

    @BelongsToMany(() => MainVideosModel, {
        through: {
            model: () => QuestionsHasMainVideos,
            unique: true,
        }
    })
    videos: MainVideosModel[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
    
}