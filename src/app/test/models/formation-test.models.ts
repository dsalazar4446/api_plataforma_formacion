import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany, BelongsTo, ForeignKey, BelongsToMany } from 'sequelize-typescript';
import { Questions } from './question.models';
import { TestDetails } from './test-details.models';
import { UserTest } from './user-tests.models';
import { FormationClassesModel } from '../../classes/models/formation-classes.model';
import { CoursesHasFormationTests } from './courses-has-formation-test.model';
import { CoursesModel } from '../../courses/models/courses.model';
import { FormationClassesHasFormationTests } from './formation-classes-has-formation-test.model';
import { ClassSequencesModel } from '../../classes/models/class-sequences.model';
import { ClassSequencesHasTestsModel } from '../../classes/models/class-sequences-has-tests.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';


@Table({
    tableName: 'formation_tests',
}) 
export class FormationTest extends Model<FormationTest>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(9),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'test_code',
    })
    testCode: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.INTEGER,
        defaultValue: 60,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'test_response_time',
    })
    testResponseTime: number;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'test_tryouts',
    })
    testTryOuts: number;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.FLOAT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'score_for_aproval',
    })
    scoreForAproval: number;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2', '3'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'test_status',
    })
    testStatus: string;


    /**
     * RELACIONES
     * FormationTest tiene muchos questions
     * FormationTest tiene muchos testDetails
     * FormationTest tiene muchos UserTest
     */

    @ApiModelPropertyOptional({
        type: Questions,
        isArray: true
    })
    @HasMany(() => Questions)
    questions: Questions[];

    @ApiModelPropertyOptional({
        type: TestDetails,
        isArray: false
    })
    @HasMany(() => TestDetails)
    testDetails: TestDetails[];

    @ApiModelPropertyOptional({
        type: UserTest,
        isArray: true
    })
    @HasMany(() => UserTest)
    userTest: UserTest[];

    @BelongsToMany(() => CoursesModel, {
        through: {
            model: () => CoursesHasFormationTests,
            unique: true,
        }
    })
    coursesModel: CoursesModel[];
    
    @BelongsToMany(() => ClassSequencesModel, () => ClassSequencesHasTestsModel)
    classSequences: ClassSequencesModel;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
