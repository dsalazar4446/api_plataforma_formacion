import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { QuestionOptions } from './question-options.models';
import { UserTest } from './user-tests.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';


@Table({
    tableName: 'user_test_has_question_options',
})
export class UserTestHasQuestionOptions extends Model<UserTestHasQuestionOptions>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserTest)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_test_id',
    })
    userTestId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => QuestionOptions)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'question_option_id',
    })
    questionOptionId: string;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}