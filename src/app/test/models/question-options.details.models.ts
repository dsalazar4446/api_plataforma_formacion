import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { QuestionOptions } from './question-options.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';



@Table({
    tableName: 'question_option_details',
})
export class QuestionOptionDetails extends Model<QuestionOptionDetails>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => QuestionOptions)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'question_option_id',
    })
    questionOptionId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'question_option_detail',
    })
    questionOptionDetail: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;

    /**
     * RELACIONES
     * questionOptionDetails pertenece a QuestionOptions
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => QuestionOptions)
    questionOptions: QuestionOptions;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}
