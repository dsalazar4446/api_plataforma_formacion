import { Controller, UseInterceptors, UseFilters, Post, UsePipes, Body, Get, Param, Delete, NotFoundException, HttpStatus, Req } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { QuestionDetailsSaveJson } from '../../interfaces/question-details.interface';
import { QuestionDetailsService } from '../../services/question-details/question-details.service';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from '@nestjs/swagger';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller QuestionDetailsController
 * @Creado 16 de Marzo 2019
 */

@ApiUseTags('Module-Test')
@Controller('question-details')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class QuestionDetailsController {

    constructor(
        private readonly _questionDetailsService: QuestionDetailsService,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Req() req, @Body() bodyQuestionDetails: QuestionDetailsSaveJson) {

        if (req.headers.language) {
            bodyQuestionDetails.languageType = req.headers.language;
        } else {
            bodyQuestionDetails.languageType = 'es';
        }

        const createQDetailsID = await this._questionDetailsService.saveQuestionDetails(bodyQuestionDetails);

        if (!createQDetailsID) {
            throw this.appUtilsService.httpCommonError('Question Details no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM71',
                languageType: 'es',
            });
        }

        return createQDetailsID;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllQuestionDetails() {
        const deleteDetails = await this._questionDetailsService.showAllQuestionDetails();
        if (!deleteDetails[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return deleteDetails;

    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteDetails(@Param('id') detailsID: string) {

        const deleteDetails = await this._questionDetailsService.destoyQuestionDetails(detailsID);

        if (!deleteDetails) {
            throw this.appUtilsService.httpCommonError('Question Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM72',
                languageType: 'es',
            });
        }

        return deleteDetails;
    }


}
