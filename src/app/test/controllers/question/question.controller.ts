import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus, Req } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { QuestionService } from '../../services/question/question.service';
import { QuestionSaveJson } from '../../interfaces/question.interface';
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { Questions } from '../../models/question.models';
import { FormationTestService } from '../../services/formation-test/formation-test.service';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller QuestionController
 * @Creado 16 de Marzo 2019
 */

@ApiUseTags('Module-Test')
@Controller('question')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class QuestionController {

    constructor(
        private readonly _questionService: QuestionService,
        private readonly appUtilsService: AppUtilsService) { }

    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyQuestion: QuestionSaveJson, @Req() req) {

        const createQuestion = await this._questionService.saveQuestion(bodyQuestion, req.headers.language);

        if (!createQuestion) {
            throw this.appUtilsService.httpCommonError('Question no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM67',
                languageType: 'es',
            });
        }

        return createQuestion;
    }

    @ApiResponse({
        status: 200,
        type: Questions,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllQuestion() {
        return await this._questionService.showAllQuestions();
    }

    @ApiResponse({
        status: 200,
        type: Questions,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async getQuestionDetails(@Param('id') questionID: string) {

        const fetchQuestion = await this._questionService.questionDetails(questionID);

        if (!fetchQuestion) {
            throw this.appUtilsService.httpCommonError('Question no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM68',
                languageType: 'es',
            });
        }

        return fetchQuestion;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateQuestion(@Param('id') id: string, @Body() bodyQuestion: Partial<QuestionSaveJson>) {

        const updateQuestion = await this._questionService.updateQuestion(id, bodyQuestion);

        if (!updateQuestion[1][0]) {
            throw this.appUtilsService.httpCommonError('Question no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM68',
                languageType: 'es',
            });;
        }

        return updateQuestion;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteQuestion(@Param('id') questionID: string) {
        const deleteQuestion = await this._questionService.destoyQuestion(questionID);

        if (!deleteQuestion) {
            throw this.appUtilsService.httpCommonError('Question no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM68',
                languageType: 'es',
            });;
        }

        return deleteQuestion;
    }


}
