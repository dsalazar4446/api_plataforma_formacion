import { Test, TestingModule } from '@nestjs/testing';
import { UserTestHasQuestionsOptionsController } from './user-test-has-questions-options.controller';

describe('UserTestHasQuestionsOptions Controller', () => {
  let controller: UserTestHasQuestionsOptionsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserTestHasQuestionsOptionsController],
    }).compile();

    controller = module.get<UserTestHasQuestionsOptionsController>(UserTestHasQuestionsOptionsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
