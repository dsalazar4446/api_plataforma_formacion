import { Controller, UseFilters, UseInterceptors, Post, Body, Delete, Param, NotFoundException, HttpStatus, Get } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { UserTestHasQuestionsOptionsService } from '../../services/user-test-has-questions-options/user-test-has-questions-options.service';
import { UserTestHasQuestionsOptionsSaveJson } from '../../interfaces/user-test-has-questions-options.interface';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from '@nestjs/swagger';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller UserTestHasQuestionsOptionsController
 * @Creado 16 de Marzo 2019
 */

@ApiUseTags('Module-Test')
@Controller('user-test-has-questions-options')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class UserTestHasQuestionsOptionsController {

    constructor(
        private readonly _userTHQPServices: UserTestHasQuestionsOptionsService,
        private readonly appUtilsService: AppUtilsService
    ) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyTest: UserTestHasQuestionsOptionsSaveJson) {
        const createUserTest = await this._userTHQPServices.createUserTestHQO(bodyTest);
        if (!createUserTest) {
            throw this.appUtilsService.httpCommonError('User Test has Question Options no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM81',
                languageType: 'es',
            });
        }

        return createUserTest;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async getAll() {
        const data = await this._userTHQPServices.getAll();

        if (!data[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return data;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({name: 'id', required: true, type: 'string'})
    async delete(@Param('id') testID: string) {

        const deleteTest = await this._userTHQPServices.destroyUserTestHQO(testID);
        if (!deleteTest) {
            throw this.appUtilsService.httpCommonError('User Test has Question Options no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM82',
                languageType: 'es',
            });
        }

        return deleteTest;
    }
}
