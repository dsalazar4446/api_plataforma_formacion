import { Test, TestingModule } from '@nestjs/testing';
import { TestDetailsController } from './test-details.controller';

describe('TestDetails Controller', () => {
  let controller: TestDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TestDetailsController],
    }).compile();

    controller = module.get<TestDetailsController>(TestDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
