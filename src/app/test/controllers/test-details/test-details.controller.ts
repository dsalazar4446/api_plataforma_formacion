import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus, Req } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { TestDetailsService } from '../../services/test-details/test-details.service';
import { TestDetailSaveJson, TestDetailUpdateJson } from '../../interfaces/test-detail.interface';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from '@nestjs/swagger';
import { FormationTestService } from '../../services/formation-test/formation-test.service';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller TestDetailsController
 * @Creado 16 de Marzo 2019
 */

@ApiUseTags('Module-Test')
@Controller('test-details')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class TestDetailsController {

    constructor(
        private readonly _testDetailsService: TestDetailsService,
        private readonly appUtilsService: AppUtilsService
        ) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Req() req, @Body() bodyTestDetails: TestDetailSaveJson) {

        if (req.headers.language) {
            bodyTestDetails.languageType = req.headers.language;
        } else {
            bodyTestDetails.languageType = 'es';
        }

        const createTestD = await this._testDetailsService.saveTestDetails(bodyTestDetails, bodyTestDetails.languageType);

        if (!createTestD) {
            throw this.appUtilsService.httpCommonError('Test Details no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM77',
                languageType: 'es',
            });
        }

        return createTestD;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') detailsID: string) {

        const fetchDetails = await this._testDetailsService.getTestDetails(detailsID);

        if (!fetchDetails) {
            throw this.appUtilsService.httpCommonError('Test Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM78',
                languageType: 'es',
            });
        }

        return fetchDetails;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAllTestDetails() {
        return this._testDetailsService.showAllTestDetails();
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllTest(@Req() req) {
        return this._testDetailsService.showAllTest(req.headers.language);
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateDetails(@Req() req, @Param('id') id: string, @Body() bodyDetails: Partial<TestDetailSaveJson>) {

        if (req.headers.language) {
            bodyDetails.languageType = req.headers.language;
        }

        const updateDetails = await this._testDetailsService.updateTestDetails(id, bodyDetails);

        if (!updateDetails[1][0]) {
            throw this.appUtilsService.httpCommonError('Test Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM78',
                languageType: 'es',
            });
        }

        return updateDetails;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteDetails(@Param('id') detailsID: string) {

        const deleteDetails = await this._testDetailsService.destroyTestDetails(detailsID);

        if (!deleteDetails) {
            throw this.appUtilsService.httpCommonError('Test Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM78',
                languageType: 'es',
            });
        }

        return deleteDetails;
    }

}
