import { Controller, UseFilters, UseInterceptors, Post, Body, Delete, Param, NotFoundException, HttpStatus, Get } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from '@nestjs/swagger';
import { QuestionsHasMainVideosService } from '../services/question-has-main-videos.service';
import { QuestionsHasMainVideosSaveJson } from '../interfaces/question-has-main-videos.interface';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller QuestionsHasMainVideosController
 * @Creado 03 de Mayo 2019
 */

@ApiUseTags('Module-Test')
@Controller('question-has-main-videos')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class QuestionsHasMainVideosController {

    constructor(
        private readonly _questionsHasMainVideosService: QuestionsHasMainVideosService,
        private readonly appUtilsService: AppUtilsService
    ) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() body: QuestionsHasMainVideosSaveJson) {
        const create = await this._questionsHasMainVideosService.create(body);
        if (!create) {
            throw this.appUtilsService.httpCommonError('Question has main videos no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM87',
                languageType: 'es',
            });
        }

        return create;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async list() {
        const data = await this._questionsHasMainVideosService.showALl();

        if (!data[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return data;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') testID: string) {

        const deleteTest = await this._questionsHasMainVideosService.destroy(testID);
        if (!deleteTest) {
            throw this.appUtilsService.httpCommonError('Question has main videos no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM88',
                languageType: 'es',
            });
        }

        return deleteTest;
    }
}
