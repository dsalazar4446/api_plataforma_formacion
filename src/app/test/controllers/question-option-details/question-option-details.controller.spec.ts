import { Test, TestingModule } from '@nestjs/testing';
import { QuestionOptionDetailsController } from './question-option-details.controller';

describe('QuestionOptionDetails Controller', () => {
  let controller: QuestionOptionDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [QuestionOptionDetailsController],
    }).compile();

    controller = module.get<QuestionOptionDetailsController>(QuestionOptionDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
