import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus, Req } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { QuestionOptionDetailsService } from '../../services/question-option-details/question-option-details.service';
import { QuestionOptionDetailsSaveJson } from '../../interfaces/question-options-details.interface';
import { ApiImplicitParam, ApiImplicitHeader, ApiUseTags } from '@nestjs/swagger';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller QuestionOptionDetailsController
 * @Creado 16 de Marzo 2019
 */
@ApiUseTags('Module-Test')
@Controller('question-option-details')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class QuestionOptionDetailsController {

    constructor(
        private readonly _questionOptDetailsService: QuestionOptionDetailsService,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Req() req, @Body() bodyQuestionOptDetails: QuestionOptionDetailsSaveJson) {

        if (req.headers.language) {
            bodyQuestionOptDetails.languageType = req.headers.language;
        } else {
            bodyQuestionOptDetails.languageType = 'es';
        }

        const createQuestionOpt = await this._questionOptDetailsService.saveQuestionOptionDetails(bodyQuestionOptDetails);

        if (!createQuestionOpt) {
            throw this.appUtilsService.httpCommonError('Question Opt Details no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM73',
                languageType: 'es',
            });
        }

        return createQuestionOpt;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') questionOptDetailsID: string) {

        const fetchOpt = await this._questionOptDetailsService.getQuestionOptionsDetails(questionOptDetailsID);

        if (!fetchOpt) {
            throw this.appUtilsService.httpCommonError('Question Opt Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM74',
                languageType: 'es',
            });
        }

        return fetchOpt;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllTestQuestioOptDetails(@Req() req, ) {
        return this._questionOptDetailsService.showAllQuestionOptionDetails(req.headers.language);
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateQuestionOptDetails(@Req() req, @Param('id') id: string, @Body() bodyQuestionOptDetails: Partial<QuestionOptionDetailsSaveJson>) {

        if (req.headers.language) {
            bodyQuestionOptDetails.languageType = req.headers.language;
        }

        const updateQuestionOptDetails = await this._questionOptDetailsService.updateQuestionOptionDetails(id, bodyQuestionOptDetails);

        if (!updateQuestionOptDetails[1][0]) {
            throw this.appUtilsService.httpCommonError('Question Opt Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM74',
                languageType: 'es',
            });
        }

        return updateQuestionOptDetails;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteOptDetails(@Param('id') questionOptDetailsID: string) {

        const deleteQuestionOptDetails = await this._questionOptDetailsService.destoyQuestionOptionDetails(questionOptDetailsID);

        if (!deleteQuestionOptDetails) {
            throw this.appUtilsService.httpCommonError('Question Opt Details no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM74',
                languageType: 'es',
            });
        }

        return deleteQuestionOptDetails;
    }


}
