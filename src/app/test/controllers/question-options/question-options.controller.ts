import { Controller, UseFilters, UseInterceptors, Body, UsePipes, Post, Get, Param, NotFoundException, Put, Delete, HttpStatus } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { QuestionOptionsService } from '../../services/question-options/question-options.service';
import { QuestionOptionsSaveJson, QuestionOptionsUpdateJson } from '../../interfaces/question-options.interface';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from '@nestjs/swagger';


/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller QuestionOptionsController
 * @Creado 16 de Marzo 2019
 */

@ApiUseTags('Module-Test')
@Controller('question-options')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class QuestionOptionsController {

    constructor(
        private readonly _questionOptsService: QuestionOptionsService,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyQuestionOpt: QuestionOptionsSaveJson) {

        const createQuestionOpt = await this._questionOptsService.saveQuestionOptions(bodyQuestionOpt);

        if (!createQuestionOpt) {
            throw this.appUtilsService.httpCommonError('Question Opt no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM75',
                languageType: 'es',
            });
        }

        return createQuestionOpt;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') questionOptID: string) {

        const fetchOpt = await this._questionOptsService.getQuestionOptions(questionOptID);

        if (!fetchOpt) {
            throw this.appUtilsService.httpCommonError('Question Opt no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM76',
                languageType: 'es',
            });
        }

        return fetchOpt;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllQuestioOpt() {
        return this._questionOptsService.showAllQuestionOptions();
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateQuestionOpt(@Param('id') id: string, @Body() bodyQuestionOpt: Partial<QuestionOptionsSaveJson>) {

        const updateQuestionOpt = await this._questionOptsService.updateQuestionOptions(id, bodyQuestionOpt);

        if (!updateQuestionOpt[1][0]) {
            throw this.appUtilsService.httpCommonError('Question Opt no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM76',
                languageType: 'es',
            });
        }

        return updateQuestionOpt;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteDetails(@Param('id') questionOptID: string) {

        const deleteQuestionOpt = await this._questionOptsService.destoyQuestionOpstions(questionOptID);

        if (!deleteQuestionOpt) {
            throw this.appUtilsService.httpCommonError('Question Opt no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM76',
                languageType: 'es',
            });
        }

        return deleteQuestionOpt;
    }
}
