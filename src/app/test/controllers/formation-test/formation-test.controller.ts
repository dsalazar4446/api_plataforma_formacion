import { Controller, UseFilters, UseInterceptors, Post, Body, Get, NotFoundException, Put, Delete, Param, Req, HttpStatus } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { FormationTestSaveJson } from '../../interfaces/formation-test.interface';
import { FormationTestService } from '../../services/formation-test/formation-test.service';
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { FormationTest } from '../../models/formation-test.models';
import { AppUtilsService } from '../../../shared/services/app-utils.service';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller FormationTestController
 * @Creado 16 de Marzo 2019
 */

@ApiUseTags('Module-Test')
@Controller('test')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class FormationTestController {

    constructor(
        private readonly _testServices: FormationTestService,
        private readonly appUtilsService: AppUtilsService
    ) { }


    @ApiResponse({
        status: 200,
        type: FormationTest,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyTest: FormationTestSaveJson) {
        const createTest = await this._testServices.saveTest(bodyTest);
        if (!createTest) {
            throw this.appUtilsService.httpCommonError('Test no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM65',
                languageType: 'es',
            });
        }
        return createTest;
    }


    @ApiResponse({
        status: 200,
        type: FormationTest,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAllTest(@Req() req) {
        const data = await this._testServices.showAllTest(req.headers.language);
        if (!data[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return data;
    }


    @ApiResponse({
        status: 200,
        type: FormationTest,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list/:testStatus/:description?')
    @ApiImplicitParam({ name: 'testStatus', required: true, type: 'string' })
    @ApiImplicitParam({ name: 'description', required: false, type: 'string' })
    async search(@Param('testStatus') testStatus: string, @Param('description') description?: string) {

        let body = {
            testStatus,
            description
        }

        const fetchTest = await this._testServices.listAdmin(body);

        if (!fetchTest) {
            throw this.appUtilsService.httpCommonError('Test no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM66',
                languageType: 'es',
            });
        }

        return fetchTest;
    }


    @ApiResponse({
        status: 200,
        type: FormationTest,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('byCode/:code')
    @ApiImplicitParam({ name: 'code', required: true, type: 'string' })
    async findByCode(@Param('code') testCode: string) {
        const data = await this._testServices.showByCode(testCode);
        if (!data) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return data;
    }


    @ApiResponse({
        status: 200,
        type: FormationTest,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAll() {
        const data = await this._testServices.showAll();
        if (!data[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return data;
    }


    @ApiResponse({
        status: 200,
        type: FormationTest,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async getTestList(@Param('id') testID: string, @Req() req) {

        const fetchTest = await this._testServices.testDetails(testID, req.headers.language);

        if (!fetchTest) {
            throw this.appUtilsService.httpCommonError('Test no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM66',
                languageType: 'es',
            });
        }

        return fetchTest;
    }

    @ApiResponse({
        status: 200,
        type: FormationTest,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateTest(@Param('id') id: string, @Body() bodyTest: Partial<FormationTestSaveJson>) {

        const updateTest = await this._testServices.updateTest(id, bodyTest);

        if (!updateTest[1][0]) {
            throw this.appUtilsService.httpCommonError('Test no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM66',
                languageType: 'es',
            });
        }

        return updateTest;
    }


    @ApiResponse({
        status: 200,
        type: FormationTest,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteTest(@Param('id') testID: string) {

        const deleteTest = await this._testServices.destroyTest(testID);

        if (!deleteTest) {
            throw this.appUtilsService.httpCommonError('Test no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM66',
                languageType: 'es',
            });
        }

        return deleteTest;
    }
}
