import { Test, TestingModule } from '@nestjs/testing';
import { FormationTestController } from './formation-test.controller';

describe('Test Controller', () => {
  let controller: FormationTestController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FormationTestController],
    }).compile();

    controller = module.get<FormationTestController>(FormationTestController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
