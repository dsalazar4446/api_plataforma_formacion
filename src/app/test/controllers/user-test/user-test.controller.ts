import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus, Req } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { UserTestSaveJson, UserTestUpdateJson } from '../../interfaces/user-test.interface';
import { UserTestService } from '../../services/user-test/user-test.service';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from '@nestjs/swagger';
/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller UserTestController
 * @Creado 16 de Marzo 2019
 */
@ApiUseTags('Module-Test')
@Controller('user-test')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class UserTestController {

    constructor(
        private readonly _userTestService: UserTestService,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyUserT: UserTestSaveJson, @Req() req) {
        const createUserTest = await this._userTestService.saveUserTest(bodyUserT, req.headers.language);

        if (!createUserTest) {
            throw this.appUtilsService.httpCommonError('User Test no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM79',
                languageType: 'es',
            });
        }

        return createUserTest;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') userTID: string) {
        const fetchUserT = await this._userTestService.getUserTest(userTID);

        if (!fetchUserT) {
            throw this.appUtilsService.httpCommonError('User Test no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM80',
                languageType: 'es',
            });
        }

        return fetchUserT;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('calification/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async calificate(@Param('id') userTID: string) {
        const fetchUserT = await this._userTestService.calificate(userTID);

        if (!fetchUserT) {
            throw this.appUtilsService.httpCommonError('User no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM80',
                languageType: 'es',
            });
        }

        return fetchUserT;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllTest() {
        return await this._userTestService.showAllUserTest();
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateUserT(@Param('id') id: string, @Body() bodyUserT: Partial<UserTestSaveJson>) {

        const updateUserT = await this._userTestService.updateUserTest(id, bodyUserT);

        if (!updateUserT[1][0]) {
            throw this.appUtilsService.httpCommonError('User Test no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM80',
                languageType: 'es',
            });
        }

        return updateUserT;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteUserT(@Param('id') userTID: string) {

        const deleteUserT = await this._userTestService.destoyUserTest(userTID);

        if (!deleteUserT) {
            throw this.appUtilsService.httpCommonError('User Test no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM80',
                languageType: 'es',
            });
        }

        return deleteUserT;
    }


}
