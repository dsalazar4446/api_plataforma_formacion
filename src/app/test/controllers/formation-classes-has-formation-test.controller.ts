import { Controller, UseFilters, UseInterceptors, Post, Body, Delete, Param, NotFoundException, HttpStatus, Get } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from '@nestjs/swagger';
import { FormationClassesHasFormationTestsService } from '../services/formation-classes-has-formation-test.services';
import { FormationClassesHasFormationTestSaveJson } from '../interfaces/formation-classes-has-formation-test.interface';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller FormationClassesHasFormationTestsController
 * @Creado 16 de Marzo 2019
 */

@ApiUseTags('Module-Test')
@Controller('formation-classes-has-formation-test')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class FormationClassesHasFormationTestsController {

    constructor(
        private readonly _formationClassesHasFormationTestsService: FormationClassesHasFormationTestsService,
        private readonly appUtilsService: AppUtilsService
    ) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() body: FormationClassesHasFormationTestSaveJson) {
        const create = await this._formationClassesHasFormationTestsService.create(body);
        if (!create) {
            throw this.appUtilsService.httpCommonError('Formation Classes Has Formation Tests no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM5',
                languageType: 'es',
            });
        }

        return create;
    }

    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async getAll() {
        const data = await this._formationClassesHasFormationTestsService.getAll();

        if (!data[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return data;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') testID: string) {

        const deleteTest = await this._formationClassesHasFormationTestsService.destroy(testID);
        if (!deleteTest) {
            throw this.appUtilsService.httpCommonError('Formation Classes Has Formation Tests no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM6',
                languageType: 'es',
            });
        }

        return deleteTest;
    }
}
