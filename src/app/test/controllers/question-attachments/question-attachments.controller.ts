import { Controller, UseFilters, UseInterceptors, Post, Body, Get, Delete, Param, NotFoundException, HttpStatus, FileFieldsInterceptor, Put } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { QuestionAttachmentsService } from '../../services/question-attachments/question-attachments.service';
import { QuestionAttachmentsSaveJson } from '../../interfaces/question-attachments.interface';
import { CONFIG } from '../../../../config';
import { ApiUseTags, ApiConsumes, ApiImplicitParam, ApiImplicitFile, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import * as file from '../infoFile';
import { QuestionsAttachments } from '../../models/question-attachments.models';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller QuestionAttachmentsController
 * @Creado 16 de Marzo 2019
 */

@ApiUseTags('Module-Test')
@Controller('question-attachments')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class QuestionAttachmentsController {

    constructor(
        private readonly _questionAttachmentService: QuestionAttachmentsService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: QuestionsAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'questionAttachment', description: 'Question Attachment File', required: true })
    async create(@Body() bodyQuestionAttach: QuestionAttachmentsSaveJson) {

        const createQuestionAttach = await this._questionAttachmentService.saveQuestionAttachments(bodyQuestionAttach);

        if (!createQuestionAttach) {
            throw this.appUtilsService.httpCommonError('Question Attachment no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM69',
                languageType: 'es',
            });
        }
        createQuestionAttach.questionsAttachment = `${CONFIG.storage.server}test/questionsAttachment/${createQuestionAttach.questionsAttachment}`;

        return createQuestionAttach;
    }


    @ApiResponse({
        status: 200,
        type: QuestionsAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllQuestionAttachment() {
        const fetchAll = await this._questionAttachmentService.showAllQuestionAttachments();

        if (!fetchAll[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        fetchAll.forEach(fetch => fetch.questionsAttachment = `${CONFIG.storage.server}test/questionsAttachment/${fetch.questionsAttachment}`);

        return fetchAll;
    }


    @ApiResponse({
        status: 200,
        type: QuestionsAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') questionAttachID: string) {

        const fetchQuestionAttach = await this._questionAttachmentService.getQuestionAttachments(questionAttachID);

        if (!fetchQuestionAttach) {
            throw this.appUtilsService.httpCommonError('Question Attachment no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM70',
                languageType: 'es',
            });
        }
        fetchQuestionAttach.questionsAttachment = `${CONFIG.storage.server}test/questionsAttachment/${fetchQuestionAttach.questionsAttachment}`;

        return fetchQuestionAttach;
    }


    @ApiResponse({
        status: 200,
        type: QuestionsAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'questionAttachment', description: 'Questions Attachments File', required: true })
    async updateNews(@Param('id') id: string, @Body() bodyQuestionAttach: Partial<QuestionAttachmentsSaveJson>) {

        const updateNews = await this._questionAttachmentService.updateNewsAttach(id, bodyQuestionAttach);

        if (!updateNews[1][0]) {
            throw this.appUtilsService.httpCommonError('Question Attachments no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM70',
                languageType: 'es',
            });
        }

        updateNews[1][0].questionsAttachment = `${CONFIG.storage.server}questionsAttachment/${updateNews[1][0].questionsAttachment}`;

        return updateNews;
    }

    @ApiResponse({
        status: 200,
        type: QuestionsAttachments,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteAttachments(@Param('id') questionAttachID: string) {

        const deleteQuestionAttach = await this._questionAttachmentService.destoyQuestionAttachments(questionAttachID);

        if (!deleteQuestionAttach) {
            throw this.appUtilsService.httpCommonError('Question Attachment no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM70',
                languageType: 'es',
            });
        }

        return deleteQuestionAttach;
    }
}
