import { Test, TestingModule } from '@nestjs/testing';
import { QuestionAttachmentsController } from './question-attachments.controller';

describe('QuestionAttachments Controller', () => {
  let controller: QuestionAttachmentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [QuestionAttachmentsController],
    }).compile();

    controller = module.get<QuestionAttachmentsController>(QuestionAttachmentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
