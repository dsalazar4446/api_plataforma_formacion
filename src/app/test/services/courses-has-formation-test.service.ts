import { Injectable, Inject } from '@nestjs/common';
import { CoursesHasFormationTests } from '../models/courses-has-formation-test.model';
import { CoursesHasFormationTestsSaveJson } from '../interfaces/courses-has-formation-test.interface';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service CoursesHasFormationTestsService
 * @Creado 26 de Abril 2019
 */

@Injectable()
export class CoursesHasFormationTestsService {

    constructor(
        @Inject('CoursesHasFormationTests') private readonly _coursesHasFormationTests: typeof CoursesHasFormationTests
    ) { }

    async create(body: CoursesHasFormationTestsSaveJson): Promise<CoursesHasFormationTestsSaveJson> {

        return await new this._coursesHasFormationTests(body).save();
    }

    async getAll(): Promise<CoursesHasFormationTestsSaveJson[]> {

        return await this._coursesHasFormationTests.findAll();
    }

    async destroy(id:string): Promise<number> {

        return await this._coursesHasFormationTests.destroy({
            where: { id: id }
        });
    }


}
