import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { QuestionsAttachments } from '../../models/question-attachments.models';
import { QuestionAttachmentsSaveJson, QuestionAttachmentsUpdateJson } from '../../interfaces/question-attachments.interface';
import * as path from 'path';
import * as fs from 'fs';
import { Questions } from '../../models/question.models';
import { AppUtilsService } from '../../../shared/services/app-utils.service';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service QuestionAttachmentsService
 * @Creado 15 de Marzo 2019
 */

@Injectable()
export class QuestionAttachmentsService {

    constructor(
        @Inject('QuestionsAttachments') private readonly _questionAttachments: typeof QuestionsAttachments,
        @Inject('Questions') private readonly _questions: typeof Questions,
        private readonly appUtilsService: AppUtilsService
    ) { }

    async saveQuestionAttachments(bodyQuestionAttachment: QuestionAttachmentsSaveJson): Promise<QuestionAttachmentsUpdateJson> {

        const question = await this._questions.findByPk(bodyQuestionAttachment.questionId);

        if (!question) {
            throw this.appUtilsService.httpCommonError('Question id no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM138',
                languageType: 'es',
            });
        }
        return await new this._questionAttachments(bodyQuestionAttachment).save();

    }

    async showAllQuestionAttachments(): Promise<QuestionAttachmentsUpdateJson[]> {

        return await this._questionAttachments.findAll();
    }

    async getQuestionAttachments(QuestionAttachmentID: string): Promise<QuestionAttachmentsUpdateJson> {

        return await this._questionAttachments.findByPk(QuestionAttachmentID);
    }

    async updateNewsAttach(id: string, bodyNewsAttach: Partial<QuestionAttachmentsSaveJson>): Promise<[number, Array<QuestionAttachmentsUpdateJson>]> {


        if (bodyNewsAttach.questionsAttachment) {
            await this.deleteImg(id);
        }

        return await this._questionAttachments.update(bodyNewsAttach, {
            where: { id },
            returning: true
        });

    }

    async destoyQuestionAttachments(QuestionAttachmentID: string): Promise<number> {

        await this.deleteImg(QuestionAttachmentID);
        return await this._questionAttachments.destroy({
            where: { id: QuestionAttachmentID }
        });
    }

    private async deleteImg(id: string) {
        const img = await this._questionAttachments.findByPk(id).then(item => item.questionsAttachment).catch(err => err);

        const pathImagen = path.resolve(__dirname, `../../../../../uploads/test/questionsAttachment/${img}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }
}
