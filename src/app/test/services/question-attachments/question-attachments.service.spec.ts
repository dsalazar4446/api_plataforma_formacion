import { Test, TestingModule } from '@nestjs/testing';
import { QuestionAttachmentsService } from './question-attachments.service';

describe('QuestionAttachmentsService', () => {
  let service: QuestionAttachmentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [QuestionAttachmentsService],
    }).compile();

    service = module.get<QuestionAttachmentsService>(QuestionAttachmentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
