import { Injectable, Inject } from '@nestjs/common';
import { FormationClassesHasFormationTests } from '../models/formation-classes-has-formation-test.model';
import { FormationClassesHasFormationTestSaveJson } from '../interfaces/formation-classes-has-formation-test.interface';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service FormationClassesHasFormationTestsService
 * @Creado 26 de Abril 2019
 */

@Injectable()
export class FormationClassesHasFormationTestsService {

    constructor(
        @Inject('FormationClassesHasFormationTests') private readonly _formationClassesHasFormationTests: typeof FormationClassesHasFormationTests
    ) { }

    async create(body: FormationClassesHasFormationTestSaveJson): Promise<FormationClassesHasFormationTestSaveJson> {

        return await new this._formationClassesHasFormationTests(body).save();
    }
    async getAll(): Promise<FormationClassesHasFormationTestSaveJson[]> {

        return await this._formationClassesHasFormationTests.findAll();
    }

    async destroy(id:string): Promise<number> {

        return await this._formationClassesHasFormationTests.destroy({
            where: { id: id }
        });
    }


}
