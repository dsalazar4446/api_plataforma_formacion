import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { QuestionOptions} from '../../models/question-options.models';
import { UserTest } from '../../models/user-tests.models';
import { QuestionOptionsSaveJson, QuestionOptionsUpdateJson } from '../../interfaces/question-options.interface';
import { Questions } from '../../models/question.models';
import { AppUtilsService } from '../../../shared/services/app-utils.service';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service QuestionOptionsService
 * @Creado 15 de Marzo 2019
 */

@Injectable()
export class QuestionOptionsService {

    constructor(
        @Inject('QuestionOptions') private readonly _questionOptions: typeof QuestionOptions,
        @Inject('Questions') private readonly _questions: typeof Questions,
        private readonly appUtilsService: AppUtilsService
    ) { }

    async saveQuestionOptions(bodyQuestionOptions: QuestionOptionsSaveJson): Promise<QuestionOptionsUpdateJson> {

        const question = await this._questions.findByPk(bodyQuestionOptions.questionId);

        if (!question) {
            throw this.appUtilsService.httpCommonError('Question id no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM138',
                languageType: 'es',
            });;
        }

        return await new this._questionOptions(bodyQuestionOptions).save();
    }

    async updateQuestionOptions(id: string, bodyquestionOptions: Partial<QuestionOptionsUpdateJson>): Promise<[number, Array<any>]> {

        return await this._questionOptions.update(bodyquestionOptions, {
            where: { id },
            returning: true
        });
    }

    async showAllQuestionOptions(): Promise<QuestionOptionsUpdateJson[]> {

        return await this._questionOptions.findAll();
    }

    async getQuestionOptions(questionOptionsID: string): Promise<QuestionOptionsUpdateJson> {

        return await this._questionOptions.findByPk(questionOptionsID, {
            include: [
                {
                    model: UserTest
                }
            ]
        });
    }

    async destoyQuestionOpstions(questionOptionsID: string): Promise<number> {

        return await this._questionOptions.destroy({
            where: { id: questionOptionsID }
        });
    }


}
