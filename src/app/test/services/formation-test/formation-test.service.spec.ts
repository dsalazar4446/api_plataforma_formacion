import { Test, TestingModule } from '@nestjs/testing';
import { FormationTestService } from './formation-test.service';

describe('TestService', () => {
  let service: FormationTestService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FormationTestService],
    }).compile();

    service = module.get<FormationTestService>(FormationTestService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
