import { FormationTestSaveJson, FormationTestUpdateJson } from '../../interfaces/formation-test.interface';
import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { FormationTest } from '../../models/formation-test.models';
import { TestDetails } from '../../models/test-details.models';
import { Questions } from '../../models/question.models';
import { QuestionDetails } from '../../models/question-details.models';
import { QuestionsAttachments } from '../../models/question-attachments.models';
import { QuestionOptions } from '../../models/question-options.models';
import { QuestionOptionDetails } from '../../models/question-options.details.models';
import { UserTest } from '../../models/user-tests.models';

import { Sequelize } from 'sequelize-typescript';
import * as ShortUniqueId from 'short-unique-id';
import { ClassSequencesModel } from '../../../classes/models/class-sequences.model';
import { AppUtilsService } from '../../../shared/services/app-utils.service';
const Op = Sequelize.Op;
const uid = new ShortUniqueId();

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service FormationTestService
 * @Creado 15 de Marzo 2019
 */

@Injectable()
export class FormationTestService {

    private include = [
        {
            model: TestDetails
        },
        {
            model: Questions,
            include: [
                {
                    model: QuestionDetails
                },
                {
                    model: QuestionsAttachments
                },
                {
                    model: QuestionOptions,
                    include: [
                        {
                            model: QuestionOptionDetails
                        }
                    ]
                }
            ]
        },
        {
            model: UserTest
        }
    ];

    constructor(
        @Inject('FormationTest') private readonly _FormationTest: typeof FormationTest,
        @Inject('TestDetails') private readonly _testDetails: typeof TestDetails,
        private readonly appUtilsService: AppUtilsService
    ) { }

    async saveTest(bodyTest: FormationTestSaveJson): Promise<FormationTestUpdateJson> {

        bodyTest.testCode = uid.randomUUID(9);

        const test = await new this._FormationTest(bodyTest).save();

        return test;
    }

    async showAllTest(languageType: string): Promise<FormationTestUpdateJson[]> {

        return await this._FormationTest.findAll({
            include: [
                {
                    model: TestDetails,
                    where: { languageType }
                },
                {
                    model: Questions,
                    include: [
                        {
                            model: QuestionDetails,
                            where: { languageType }
                        },
                        {
                            model: QuestionsAttachments
                        },
                        {
                            model: QuestionOptions,
                            include: [
                                {
                                    model: QuestionOptionDetails,
                                    where: { languageType }
                                }
                            ]
                        }
                    ]
                },
                {
                    model: UserTest
                }
            ]
        });

    }

    async updateTest(id: string, bodyTest: Partial<FormationTestUpdateJson>) {

        return await this._FormationTest.update(bodyTest, {
            where: { id },
            returning: true
        });
    }

    async showAll() {

        const data = await this._FormationTest.findAll({ include: this.include });

        const newData = data.map(item => {
            let countUserTest = item.userTest.length;
            item.dataValues.userTest = undefined;
            return {
                ...item.dataValues,
                countUserTest
            }
        });
        return newData;
    }

    async testDetails(formationTestId: string, languageType: string): Promise<any> {

        const data = await this._FormationTest.findByPk(formationTestId, {
            include: [
                {
                    model: TestDetails,
                    where: { languageType }
                },
                {
                    model: Questions,
                    include: [
                        {
                            model: QuestionDetails,
                            where: { languageType }
                        },
                        {
                            model: QuestionsAttachments
                        },
                        {
                            model: QuestionOptions,
                            include: [
                                {
                                    model: QuestionOptionDetails,
                                    where: { languageType }
                                }
                            ]
                        }
                    ]
                },
                {
                    model: UserTest
                }
            ]
        });

        if (!data) {
            throw this.appUtilsService.httpCommonError('Formation Test no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM66',
                languageType: 'es',
            });
        }
        if(data.userTest){

            let countUserTest = data.userTest.length;
            data.userTest = undefined;
            return {
                ...data.dataValues,
                countUserTest
            };
        }


        return {
            ...data.dataValues,
            countUserTest: 0
        };

    }

    async showByCode(testCode: string): Promise<any> {
        const data = await this._FormationTest.findOne({
            where: {
                testCode
            },
            include: this.include
        });

        if (!data) {
            throw this.appUtilsService.httpCommonError('Test Code does not exist!!', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if(data.userTest){

            let countUserTest = data.userTest.length;
            data.userTest = undefined;
            return {
                ...data.dataValues,
                countUserTest
            };
        }


        return {
            ...data.dataValues,
            countUserTest: 0
        };
    }

    async listAdmin(body) {

        let { testStatus, description } = body;

        return await this.consulta(testStatus, description);

    }

    async destroyTest(testID: string): Promise<number> {

        return await this._FormationTest.destroy({
            where: { id: testID }
        });
    }

    private consulta = async (testStatus: string, description: string) => {

        if (description != undefined) {

            return await this._testDetails.findAll({
                where: {
                    [Op.or]: [
                        {
                            testTitle: {
                                [Op.iLike]: `%${description}%`
                            }
                        },
                        {
                            testDescription: {
                                [Op.iLike]: `%${description}%`
                            }
                        }
                    ]
                },
                include: [
                    {
                        model: FormationTest,
                        where: {
                            testStatus
                        }
                    }
                ]
            });

        }
        if (testStatus !== undefined && description == undefined) {

            return await this._FormationTest.findAll({
                where: {
                    testStatus
                },
                include: [
                    {
                        model: TestDetails
                    }
                ]
            });
        }

        return
    }

}
