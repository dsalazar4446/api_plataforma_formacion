import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { UserTest } from '../../models/user-tests.models';
import { QuestionOptions } from '../../models/question-options.models';
import { FormationTest } from '../../models/formation-test.models';
import { UserTestUpdateJson, UserTestSaveJson } from '../../interfaces/user-test.interface';
import { FormationTestService } from '../formation-test/formation-test.service';
import { UsersService } from '../../../user/services/users.service';
import { AppUtilsService } from '../../../shared/services/app-utils.service';
import { UserModel } from 'src/app/user/models/user.Model';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service UserTestService
 * @Creado 15 de Marzo 2019
 */

@Injectable()
export class UserTestService {

    constructor(
        @Inject('UserTest') private readonly _userTest: typeof UserTest,
        @Inject('UserModel') private readonly _user: typeof UserModel,
        private readonly _testServices: FormationTestService,
        private readonly _userService: UsersService,
        private readonly appUtilsService: AppUtilsService
    ) { }

    async saveUserTest(bodyUserTest: UserTestSaveJson, language: string): Promise<UserTestUpdateJson> {

        await this._testServices.testDetails(bodyUserTest.formationTestId, language);
        const user = await this._user.findByPk(bodyUserTest.userId);

        if (!user) return;

        return await new this._userTest(bodyUserTest).save();
    }

    async showAllUserTest(): Promise<UserTestUpdateJson[]> {

        return await this._userTest.findAll();
    }

    async getUserTest(userTestID: string): Promise<UserTestUpdateJson> {

        return await this._userTest.findByPk(userTestID, {
            include: [
                {
                    model: QuestionOptions
                }
            ]
        });
    }

    async updateUserTest(id: string, bodyUserTest: Partial<UserTestUpdateJson>): Promise<[number, Array<any>]> {

        return await this._userTest.update(bodyUserTest, {
            where: { id },
            returning: true
        });
    }

    async destoyUserTest(userTestID: string): Promise<number> {

        return await this._userTest.destroy({
            where: { id: userTestID }
        });
    }

    async calificate(userId: string): Promise<any> {

        const data = await this._userTest.findOne({
            include: [
                {
                    model: QuestionOptions
                },
                {
                    model: FormationTest,
                }
            ],
            where: {
                userId
            }
        });

        if (!data) {
            console.log('no data');
            throw this.appUtilsService.httpCommonError('Usuario no existe, o no ha presentado pruebas!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM146',
                languageType: 'es',
            });
        }

        var scoreObtained: number = 0;
        var message: string = '';
        var scoreForAproval: number = Number(data.formationTest.scoreForAproval);
        var testTryOuts: number = Number(data.formationTest.testTryOuts);

        data.questionOptions.forEach(option => {
            
            scoreObtained = scoreObtained + Number(option.questionOptionScore);
        });
        await this.updateUserTest(userId, { scoreObtained });

        if (testTryOuts >= 3) {

            return {
                message: `No tienes mas intentos. Intentos: ${testTryOuts}`
            }

        } else {

            await this._testServices.updateTest(data.formationTest.id, { testTryOuts: testTryOuts + 1 });

            if (scoreObtained < scoreForAproval) {

                message = `Tu puntuación es ${scoreObtained}pts y necesitabas ${scoreForAproval}pts para aprobar. Intentos: ${testTryOuts}`;
            } else if (scoreObtained >= scoreForAproval) {

                scoreObtained = scoreForAproval;
                message = `Tu puntuación es ${scoreObtained}pts. Haz aprobado, felicidades.`;
            }

        }

        return {
            scoreObtained,
            scoreForAproval,
            message
        }
    }
}