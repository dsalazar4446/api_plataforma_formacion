import { Test, TestingModule } from '@nestjs/testing';
import { QuestionOptionDetailsService } from './question-option-details.service';

describe('QuestionOptionDetailsService', () => {
  let service: QuestionOptionDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [QuestionOptionDetailsService],
    }).compile();

    service = module.get<QuestionOptionDetailsService>(QuestionOptionDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
