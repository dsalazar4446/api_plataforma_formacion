import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { QuestionOptionDetails } from '../../models/question-options.details.models';
import { QuestionOptionDetailsSaveJson, QuestionOptionDetailsUpdateJson } from '../../interfaces/question-options-details.interface';
import { AppUtilsService } from '../../../shared/services/app-utils.service';
import { QuestionOptions } from '../../models/question-options.models';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service QuestionOptionDetailsService
 * @Creado 15 de Marzo 2019
 */

@Injectable()
export class QuestionOptionDetailsService {

    constructor(
        @Inject('QuestionOptionDetails') private readonly _questionOptionDetails: typeof QuestionOptionDetails,
        @Inject('QuestionOptions') private readonly _questionOptions: typeof QuestionOptions,
        private readonly appUtilsService: AppUtilsService
    ) { }

    async saveQuestionOptionDetails(bodyQuestionOptionDetails: QuestionOptionDetailsSaveJson): Promise<QuestionOptionDetailsUpdateJson> {

        const questionOpt = await this._questionOptions.findByPk(bodyQuestionOptionDetails.questionOptionId);

        if (!questionOpt) {
            throw this.appUtilsService.httpCommonError('Question Options id no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM139',
                languageType: 'es',
            });;
        }

        return await new this._questionOptionDetails(bodyQuestionOptionDetails).save();
    }

    async updateQuestionOptionDetails(id: string, bodyQuestionOptionDetails: Partial<QuestionOptionDetailsUpdateJson>): Promise<[number, Array<any>]> {

        return await this._questionOptionDetails.update(bodyQuestionOptionDetails, {
            where: { id },
            returning: true
        });
    }

    async getQuestionOptionsDetails(questionOptionsID: string): Promise<QuestionOptionDetailsUpdateJson> {

        return await this._questionOptionDetails.findByPk(questionOptionsID);
    }

    async showAllQuestionOptionDetails(languageType: string): Promise<QuestionOptionDetailsUpdateJson[]> {

        return await this._questionOptionDetails.findAll({
            where: {
                languageType
            }
        });
    }

    async destoyQuestionOptionDetails(questionOptionDetailsID: string): Promise<number> {

        return await this._questionOptionDetails.destroy({
            where: { id: questionOptionDetailsID }
        });
    }
}
