import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { QuestionDetails } from '../../models/question-details.models';
import { QuestionDetailsSaveJson, QuestionDetailsUpdateJson } from '../../interfaces/question-details.interface';
import { Questions } from '../../models/question.models';
import { AppUtilsService } from '../../../shared/services/app-utils.service';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service QuestionDetailsService
 * @Creado 15 de Marzo 2019
 */1

@Injectable()
export class QuestionDetailsService {

    constructor(
        @Inject('QuestionDetails') private readonly _questionDetails: typeof QuestionDetails,
        @Inject('Questions') private readonly _questions: typeof Questions,
        private readonly appUtilsService: AppUtilsService

    ) { }

    async saveQuestionDetails(bodyQuestionDetails: QuestionDetailsSaveJson): Promise<QuestionDetailsUpdateJson> {

        const question = await this._questions.findByPk(bodyQuestionDetails.questionId);

        if (!question) {
            throw this.appUtilsService.httpCommonError('Question id no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM138',
                languageType: 'es',
            });;
        }
        return await new this._questionDetails(bodyQuestionDetails).save();
    }

    async showAllQuestionDetails(): Promise<QuestionDetailsUpdateJson[]> {

        return await this._questionDetails.findAll();
    }

    async destoyQuestionDetails(QuestionDetailsID: string): Promise<number> {

        return await this._questionDetails.destroy({
            where: { id: QuestionDetailsID }
        });
    }
}
