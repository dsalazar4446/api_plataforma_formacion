import { Injectable, Inject } from '@nestjs/common';
import { Questions} from '../../models/question.models';
import { QuestionDetails } from '../../models/question-details.models';
import { QuestionSaveJson} from '../../interfaces/question.interface';
import { QuestionUpdateJson } from '../../interfaces/question.interface';
import { FormationTestService } from '../formation-test/formation-test.service';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service QuestionService
 * @Creado 15 de Marzo 2019
 */

@Injectable()
export class QuestionService {

    constructor(
        @Inject('Questions') private readonly _questions: typeof Questions,
        private readonly _formationTestService: FormationTestService
    ){ }

    async saveQuestion( bodyQuestion: QuestionSaveJson, language: string): Promise<QuestionUpdateJson>{

        await this._formationTestService.testDetails(bodyQuestion.formationTestId, language);

        return await new this._questions(bodyQuestion).save();
    }

    async showAllQuestions(): Promise<QuestionUpdateJson[]>{

        return await this._questions.findAll();
    }

    async questionDetails( questionId: string ){

        return await this._questions.findByPk( questionId, {
            include: [{
                model: QuestionDetails
            }]
        } );
    }

    async updateQuestion(id:string,  bodyQuestion: Partial<QuestionUpdateJson> ): Promise<[number, Array<any>]>{

        return await this._questions.update( bodyQuestion, {
            where: { id  },
            returning: true
        });
    }

    async destoyQuestion(questionID: string): Promise<number> {

        return await this._questions.destroy({
            where: { id: questionID }
        });
    }
}
