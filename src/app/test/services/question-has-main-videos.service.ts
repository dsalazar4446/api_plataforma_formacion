import { Injectable, Inject } from '@nestjs/common';
import { QuestionsHasMainVideos } from '../models/question-has-main-videos.model';
import { QuestionsHasMainVideosSaveJson } from '../interfaces/question-has-main-videos.interface';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service QuestionsHasMainVideosService
 * @Creado 03 de MAyo 2019
 */

@Injectable()
export class QuestionsHasMainVideosService {

    constructor(
        @Inject('QuestionsHasMainVideos') private readonly _questionsHasMainVideos: typeof QuestionsHasMainVideos
    ) { }

    async create(body: QuestionsHasMainVideosSaveJson): Promise<QuestionsHasMainVideosSaveJson> {

        return await new this._questionsHasMainVideos(body).save();
    }

    async showALl(): Promise<QuestionsHasMainVideosSaveJson[]>{
        return await this._questionsHasMainVideos.findAll();
    }

    async destroy(id:string): Promise<number> {

        return await this._questionsHasMainVideos.destroy({
            where: { id }
        });
    }


}
