import { Injectable, Inject } from '@nestjs/common';
import { TestDetails } from '../../models/test-details.models';
import { TestDetailSaveJson, TestDetailUpdateJson } from '../../interfaces/test-detail.interface';
import { FormationTestService } from '../formation-test/formation-test.service';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service TestDetailsService
 * @Creado 15 de Marzo 2019
 */

@Injectable()
export class TestDetailsService {

    constructor(
        @Inject('TestDetails') private readonly _testDetails: typeof TestDetails,
        private readonly _formationTestService: FormationTestService
    ) { }

    async saveTestDetails(bodyTestDetails: TestDetailSaveJson, language: string): Promise<TestDetailUpdateJson> {

        await this._formationTestService.testDetails(bodyTestDetails.formationTestId, language);

        return await new this._testDetails(bodyTestDetails).save();
    }

    async showAllTestDetails(): Promise<TestDetailUpdateJson[]> {

        return await this._testDetails.findAll();
    }

    async showAllTest(languageType: string): Promise<TestDetailUpdateJson[]> {

        return await this._testDetails.findAll({
            where: {
                languageType
            }
        });
    }

    async getTestDetails(testDetailsID: string): Promise<TestDetailUpdateJson> {

        return await this._testDetails.findByPk(testDetailsID);
    }

    async updateTestDetails(id: string, bodyTestDetails: Partial<TestDetailUpdateJson>): Promise<[number, Array<any>]> {

        return await this._testDetails.update(bodyTestDetails, {
            where: { id },
            returning: true
        });
    }

    async destroyTestDetails(testDetailsID: string): Promise<number> {

        return await this._testDetails.destroy({
            where: { id: testDetailsID }
        });
    }

}
