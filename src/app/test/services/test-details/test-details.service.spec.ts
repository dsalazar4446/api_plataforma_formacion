import { Test, TestingModule } from '@nestjs/testing';
import { TestDetailsService } from './test-details.service';

describe('TestDetailsService', () => {
  let service: TestDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TestDetailsService],
    }).compile();

    service = module.get<TestDetailsService>(TestDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
