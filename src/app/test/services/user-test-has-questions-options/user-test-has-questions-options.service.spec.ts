import { Test, TestingModule } from '@nestjs/testing';
import { UserTestHasQuestionsOptionsService } from './user-test-has-questions-options.service';

describe('UserTestHasQuestionsOptionsService', () => {
  let service: UserTestHasQuestionsOptionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserTestHasQuestionsOptionsService],
    }).compile();

    service = module.get<UserTestHasQuestionsOptionsService>(UserTestHasQuestionsOptionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
