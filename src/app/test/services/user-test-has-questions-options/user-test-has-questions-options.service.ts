import { Injectable, Inject } from '@nestjs/common';
import { UserTestHasQuestionsOptionsSaveJson, UserTestHasQuestionsOptionsUpdateJson } from '../../interfaces/user-test-has-questions-options.interface';
import { UserTestHasQuestionOptions } from '../../models/user-tests-has-questions-options.models';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service UserTestHasQuestionsOptionsService
 * @Creado 15 de Marzo 2019
 */

@Injectable()
export class UserTestHasQuestionsOptionsService {

    constructor(
        @Inject('UserTestHasQuestionOptions') private readonly _UserTestHQOService: typeof UserTestHasQuestionOptions
    ) { }

    async createUserTestHQO(bodyUserTestHQO: UserTestHasQuestionsOptionsSaveJson): Promise<UserTestHasQuestionsOptionsUpdateJson> {

        return await new this._UserTestHQOService(bodyUserTestHQO).save();
    }

    async getAll(): Promise<UserTestHasQuestionsOptionsSaveJson[]> {

        return await this._UserTestHQOService.findAll();
    }

    async destroyUserTestHQO(idUserTestHQO:string): Promise<number> {

        return await this._UserTestHQOService.destroy({
            where: { id: idUserTestHQO }
        });
    }


}
