import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, Req} from '@nestjs/common';
import { BroadcastMessageService } from '../services/broadcast-message.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { BroadcastMessageSaveJson, BroadcastMessageUpdateJson } from '../interfaces/broadcast-message.interface';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { BroadcastMessageModel } from '../models/broadcast-message.model';
import { UserModel } from '../../user/models/user.Model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
@Controller('broadcast-messages')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// @UseGuards(new JwtAuthGuard())
export class BroadcastMessagesController {
    constructor(
        private readonly broadcastMessageService: BroadcastMessageService,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('BroadcastModel') private readonly broadcastModel: typeof BroadcastModel,
        @Inject('BroadcastMessageModel') private readonly broadcastMessageModel: typeof BroadcastMessageModel,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        @Post()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: BroadcastMessageModel,
          })
        async create(@Body() broadcastMessage: BroadcastMessageSaveJson,@Req() req) {
            const data = await this.broadcastModel.findByPk(broadcastMessage.broadcastId);
            if(data == null){
                  throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                      messageCode: 'EA23',
                      languageType:  req.headers.language,
                  });
            }
            const data2 = await this.userModel.findByPk(broadcastMessage.userId);
            if(data2 == null){
                  throw this.appUtilsService.httpCommonError('Broadcast does not created!', HttpStatus.NOT_ACCEPTABLE, {
                      messageCode: 'EA4',
                      languageType:  req.headers.language,
                  });
            }
            return await this.broadcastMessageService.create(broadcastMessage);
        }
        @Get()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: BroadcastMessageModel,
            isArray: true
          })
        async getAll() {
            return await this.broadcastMessageService.findAll();
        }
        @Get(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: BroadcastMessageModel,
          })
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        async findById(@Param('id') idBroadcastMessage) {
            return await this.broadcastMessageService.findById(idBroadcastMessage);
        }
        @Get('broadcast/:broadcastId')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: BroadcastMessageModel,
          })
        @ApiImplicitParam({ name: 'broadcastId', required: true, type: 'string' })
        async findByBroadcastId(@Param('broadcastId') broadcastId) {
            return await this.broadcastMessageService.findByBroadcastId(broadcastId);
        }
        @Put(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: BroadcastMessageModel,
          })
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        async update(
            @Body()
            updateBroadcastMessage: Partial<BroadcastMessageUpdateJson>,
            @Param('id') idBroadcastMessage,
            @Req() req
        ) {
            if(updateBroadcastMessage.broadcastId){
                const data = await this.broadcastModel.findByPk(updateBroadcastMessage.broadcastId);
                if(data == null){
                    throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                        messageCode: 'EA23',
                        languageType:  req.headers.language,
                    });
                }
            }
            if(updateBroadcastMessage.userId){
                const data2 = await this.userModel.findByPk(updateBroadcastMessage.userId);
                if(data2 == null){
                    throw this.appUtilsService.httpCommonError('Broadcast does not created!', HttpStatus.NOT_ACCEPTABLE, {
                        messageCode: 'EA4',
                        languageType:  req.headers.language,
                    });
                }
            }

            const data3 = await this.broadcastMessageModel.findByPk(idBroadcastMessage);
            if(data3 == null){
                  throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                      messageCode: 'EA28',
                      languageType:  req.headers.language,
                  });
            }
            return await this.broadcastMessageService.update(idBroadcastMessage, updateBroadcastMessage);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Delete(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async delete(@Param('id') idBroadcastMessage) {
            return await this.broadcastMessageService.deleted(idBroadcastMessage);
        }
}
