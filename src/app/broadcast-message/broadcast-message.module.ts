import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { BroadcastMessagesController } from './controllers/broadcast-message.controller';
import { BroadcastMessageService } from './services/broadcast-message.service';
import { BroadcastMessageModel } from './models/broadcast-message.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { BroadcastModel } from '../broadcast/models/broadcast.model';

const models = [
  BroadcastMessageModel,
  BroadcastModel
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [BroadcastMessagesController],
  providers: [BroadcastMessageService]
})
export class BroadcastMessageModule {}
