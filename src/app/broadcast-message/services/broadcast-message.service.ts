import { Injectable, Inject } from '@nestjs/common';
import { BroadcastMessageSaveJson, BroadcastMessageUpdateJson } from '../interfaces/broadcast-message.interface';
import { BroadcastMessageModel } from '../models/broadcast-message.model';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
import { UserModel } from '../../user/models/user.Model';
import { completeUrl } from '../../shared/utils/completUrl';
@Injectable()
export class BroadcastMessageService {
    constructor(
        @Inject('BroadcastMessageModel') private readonly broadcastMessageModel: typeof BroadcastMessageModel,
        @Inject('BroadcastModel') private readonly broadcastModel: typeof BroadcastModel,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
    ) { }
    async findAll(): Promise<any[]> {
        const data = await this.broadcastMessageModel.findAll();
        const result:any[] = [];
        await Promise.all(
            await data.map(
                async (item: any)=>{
                    const dataUser = await this.userModel.findByPk(item.dataValues.userId);
                    dataUser.dataValues.pwd = undefined;
                    dataUser.dataValues.avatar = completeUrl('user/avatar',dataUser.dataValues.avatar);
                    const dataBroadcast = await this.broadcastModel.findByPk(item.dataValues.broadcastId);
                    item.dataValues.userModel = dataUser.dataValues;
                    item.dataValues.broadcastModel = dataBroadcast.dataValues;
                    result.push(item.dataValues);
                }
            )
        )
        return result;
    }
    async findById(id: string): Promise<any> {
        const data: any = await this.broadcastMessageModel.findByPk<BroadcastMessageModel>(id);
        const dataUser = await this.userModel.findByPk(data.dataValues.userId);
        dataUser.dataValues.pwd = undefined;
        dataUser.dataValues.avatar = completeUrl('user/avatar',dataUser.dataValues.avatar);
        const dataBroadcast = await this.broadcastModel.findByPk(data.dataValues.broadcastId);
        data.dataValues.userModel = dataUser.dataValues;
        data.dataValues.broadcastModel = dataBroadcast.dataValues;
        return data;
    }
    async findByBroadcastId(id: string): Promise<BroadcastMessageModel[]> {
        const result = await this.broadcastMessageModel.findAll({
            where :{
                broadcastId: id,
            }
        });
        await Promise.all(
            await result.map(
                async (data: any) => {
                    const broadcast =  await this.broadcastModel.findByPk(id);
                    const user =  await this.userModel.findByPk<UserModel>(data.dataValues.userId);
                    user.dataValues.pwd = undefined;
                    user.dataValues.avatar = completeUrl('user/avatar',user.dataValues.avatar);
                    data.dataValues.broadcastModel = broadcast;
                    data.dataValues.userModel = user;
                }
            )
        )
        return result;
    }
    async create(broadcastMessage: BroadcastMessageSaveJson): Promise<BroadcastMessageModel> {
        return await this.broadcastMessageModel.create<BroadcastMessageModel>(broadcastMessage, {
            returning: true,
        });
    }
    async update(idBroadcastMessage: string, broadcastMessageUpdate: Partial<BroadcastMessageUpdateJson>){
        return  await this.broadcastMessageModel.update(broadcastMessageUpdate, {
            where: {
                id: idBroadcastMessage,
            },
            returning: true,
        });
    }
    async deleted(idBroadcastMessage: string): Promise<any> {
        return await this.broadcastMessageModel.destroy({
            where: {
                id: idBroadcastMessage,
            },
        });
    }
}
