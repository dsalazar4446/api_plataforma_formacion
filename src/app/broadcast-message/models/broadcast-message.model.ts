import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
import { UserModel } from '../../user/models/user.Model';
import { ApiModelProperty } from '@nestjs/swagger';

@Table({
    tableName: 'broadcast_messages',
})
export class BroadcastMessageModel extends Model<BroadcastMessageModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => BroadcastModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'broadcast_id',
    })
    broadcastId: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'user_id',
    })
    userId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'broadcast_message',
    })
    broadcastMessage: string;

    @BelongsTo(() => UserModel) 
    user: UserModel;
    
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
