import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class BroadcastMessageSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    broadcastId: string;
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    userId: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    broadcastMessage: string;
    broascastModel?: any;
    userModel?: any;
}
// tslint:disable-next-line: max-classes-per-file
export class BroadcastMessageUpdateJson  extends BroadcastMessageSaveJson{
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}
