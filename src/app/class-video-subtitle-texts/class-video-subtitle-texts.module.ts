import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { ClassVideoSubtitleTextsModel } from './models/class-video-subtitle-texts.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { ClassVideoSubtitleTextsController } from './controllers/class-videos-subtitles.controller';
import { ClassVideoSubtitleTextsService } from './services/class-videos-subtitles.service';
import { ClassVideoSubtitlesModel } from '../class-video-subtitles/models/class-video-subtitles.model';

const models = [
  ClassVideoSubtitleTextsModel,
  ClassVideoSubtitlesModel
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [ClassVideoSubtitleTextsController],
  providers: [ClassVideoSubtitleTextsService]
})
export class ClassVideoSubtitleTextsModule {}
