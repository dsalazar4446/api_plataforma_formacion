import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, UsePipes, FileFieldsInterceptor, UseInterceptors, UseFilters, Inject, Req} from '@nestjs/common';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { ClassVideoSubtitleTextsSaveJson, ClassVideoSubtitleTextsUpdateJson } from '../interfaces/class-video-subtitle-texts.interface';
import { ClassVideoSubtitleTextsService } from '../services/class-videos-subtitles.service';
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { ClassVideoSubtitleTextsModel } from '../models/class-video-subtitle-texts.model';
import { ClassVideoSubtitlesModel } from '../../class-video-subtitles/models/class-video-subtitles.model';
import { AppUtilsService } from 'src/app/shared/services/app-utils.service';

@ApiUseTags('Class video subtitle texts')
@Controller('class-video-subtitle-texts')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class ClassVideoSubtitleTextsController {
    constructor(
        private readonly classVideoSubtitleTextsService: ClassVideoSubtitleTextsService,
        @Inject('ClassVideoSubtitleTextsModel') private readonly  classVideoSubtitleTextsModel: typeof  ClassVideoSubtitleTextsModel,
        @Inject('ClassVideoSubtitlesModel') private readonly classVideoSubtitlesModel: typeof ClassVideoSubtitlesModel,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        @Post()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: ClassVideoSubtitleTextsModel,
          })
        async create(@Body() classVideoSubtitleTexts: ClassVideoSubtitleTextsSaveJson, @Req() req) {
            const data3 = await this.classVideoSubtitlesModel.findByPk(classVideoSubtitleTexts.classVideoSubtitlesId);
            if(data3 == null){
                throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA17',
                    languageType:  req.headers.language,
                });
            }
            return await this.classVideoSubtitleTextsService.create(classVideoSubtitleTexts);
        }
        @Get()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: ClassVideoSubtitleTextsModel,
            isArray: true
          })
        async getAll() {
            return await this.classVideoSubtitleTextsService.findAll();
        }
        @Get(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: ClassVideoSubtitleTextsModel,
          })
        @ApiImplicitParam({name:'id', required: true})
        async findById(@Param('id') idClassVideoSubtitleTexts) {
            return await this.classVideoSubtitleTextsService.findById(idClassVideoSubtitleTexts);
        }
        @Put(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: ClassVideoSubtitleTextsModel,
          })
        @ApiImplicitParam({name:'id', required: true})
        async update(
            @Body()
            classVideoSubtitleTexts: Partial<ClassVideoSubtitleTextsUpdateJson>,
            @Param('id') idClassVideoSubtitleTexts,
            @Req() req,
        ) {
            const data3 = await this.classVideoSubtitleTextsModel.findByPk(idClassVideoSubtitleTexts);
            if(data3 == null){
                throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA17',
                    languageType:  req.headers.language,
                });
            }
            if(classVideoSubtitleTexts.classVideoSubtitlesId){
                const data2 = await this.classVideoSubtitlesModel.findByPk(classVideoSubtitleTexts.classVideoSubtitlesId);
                if(data2 == null){
                    throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                        messageCode: 'EA18',
                        languageType:  req.headers.language,
                    });
                }
            }
            return await this.classVideoSubtitleTextsService.update(idClassVideoSubtitleTexts, classVideoSubtitleTexts);
        }
        @Delete(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiImplicitParam({ name: 'id', required: true })
        async delete(@Param('id') idClassVideoSubtitleTexts) {
            return await this.classVideoSubtitleTextsService.deleted(idClassVideoSubtitleTexts);
        }
}
