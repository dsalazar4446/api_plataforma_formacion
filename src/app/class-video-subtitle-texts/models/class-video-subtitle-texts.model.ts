import { Column, DataType, Table, Model, CreatedAt, UpdatedAt } from 'sequelize-typescript';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'class_video_subtitles',
})
export class ClassVideoSubtitleTextsModel extends Model<ClassVideoSubtitleTextsModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'class_video_subtitles_id',
    })
    classVideoSubtitlesId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.TIME,
        allowNull: false,
        field: 'subtitle_time',
    })
    subtitleTime: string;
    @ApiModelProperty()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        field: 'subtitle_text',
    })
    subtitleText: string;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
