import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class ClassVideoSubtitleTextsSaveJson {

    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    classVideoSubtitlesId: string;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    subtitleTime: string;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    subtitleText: string;
}
// tslint:disable-next-line: max-classes-per-file
export class ClassVideoSubtitleTextsUpdateJson  extends ClassVideoSubtitleTextsSaveJson{

    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    id: string;
}
