import { Injectable, Inject } from '@nestjs/common';
import { ClassVideoSubtitleTextsSaveJson, ClassVideoSubtitleTextsUpdateJson } from '../interfaces/class-video-subtitle-texts.interface';
import { ClassVideoSubtitleTextsModel } from '../models/class-video-subtitle-texts.model';
@Injectable()
export class ClassVideoSubtitleTextsService {
    constructor(
        @Inject('ClassVideoSubtitleTextsModel') private readonly classVideoSubtitleTextsModel: typeof ClassVideoSubtitleTextsModel,
    ) { }
    async findAll(): Promise<ClassVideoSubtitleTextsModel[]> {
        return  await this.classVideoSubtitleTextsModel.findAll();
    }
    async findById(id: string): Promise<ClassVideoSubtitleTextsModel> {
        return await this.classVideoSubtitleTextsModel.findById<ClassVideoSubtitleTextsModel>(id);
    }
    async create(classVideoSubtitleTexts: ClassVideoSubtitleTextsSaveJson): Promise<ClassVideoSubtitleTextsModel> {
        return await this.classVideoSubtitleTextsModel.create<ClassVideoSubtitleTextsModel>(classVideoSubtitleTexts, {
            returning: true,
        });
    }
    async update(idClassVideoSubtitleTexts: string, mainVideosUpdate: Partial<ClassVideoSubtitleTextsUpdateJson>){
        return  await this.classVideoSubtitleTextsModel.update(mainVideosUpdate, {
            where: {
                id: idClassVideoSubtitleTexts,
            },
            returning: true,
        });
    }
    async deleted(idClassVideoSubtitleTexts: string): Promise<any> {
        return await this.classVideoSubtitleTextsModel.destroy({
            where: {
                id: idClassVideoSubtitleTexts,
            },
        });
    }
}
