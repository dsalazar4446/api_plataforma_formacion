import { Injectable, Inject } from '@nestjs/common';
import { BroadcastAttendanceSaveJson, BroadcastAttendanceUpdateJson } from '../interfaces/broadcast-attendance.interface';
import { BroadcastAttendanceModel } from '../models/broadcast-attendance.model';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
import { UserModel } from '../../user/models/user.Model';
@Injectable()
export class BroadcastAttendanceService {
    constructor(
        @Inject('BroadcastAttendanceModel') private readonly broadcastAttendanceModel: typeof BroadcastAttendanceModel,
        @Inject('BroadcastModel') private readonly broadcastModel: typeof BroadcastModel,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
    ) { }
    async create(broadcastAttendance: BroadcastAttendanceSaveJson): Promise<BroadcastAttendanceModel> {
        return await this.broadcastAttendanceModel.create<BroadcastAttendanceModel>(broadcastAttendance, {
            returning: true,
        });
    }
    async findAll(): Promise<any[]> {
        const data = await this.broadcastAttendanceModel.findAll();
        const result:any[] = [];
        await Promise.all(
            await data.map(
                async (item: any) =>{
                    const dataUserModel = await this.userModel.findByPk(item.dataValues.userId);
                    dataUserModel.dataValues.pwd = undefined;
                    const dataBroadcastModel = await this.broadcastModel.findByPk(item.dataValues.broadcastId);
                    item.dataValues.broadcastModel = dataBroadcastModel.dataValues;
                    item.dataValues.userModel = dataUserModel.dataValues;
                    result.push(item.dataValues);
                }
            )
        )
        return  result;
    }
    /*
    async findById(id: string): Promise<BroadcastAttendanceModel> {
        return await this.broadcastAttendanceModel.findById<BroadcastAttendanceModel>(id,{
            include:[UserModel],
        });
    }
  
    async update(idBroadcastAttendance: string, broadcastAttendanceUpdate: Partial<BroadcastAttendanceUpdateJson>){
        return  await this.broadcastAttendanceModel.update(broadcastAttendanceUpdate, {
            where: {
                id: idBroadcastAttendance,
            },
            returning: true,
        });
    }
    async deleted(idBroadcastAttendance: string): Promise<any> {
        return await this.broadcastAttendanceModel.destroy({
            where: {
                id: idBroadcastAttendance,
            },
        });
    }*/
}
