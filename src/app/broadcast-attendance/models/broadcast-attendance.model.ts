import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
import { ApiModelProperty } from '@nestjs/swagger';

@Table({
    tableName: 'broadcast_attendances',
})
export class BroadcastAttendanceModel extends Model<BroadcastAttendanceModel> {
    @ApiModelProperty()
    @ForeignKey(() => BroadcastModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'broadcast_id',
    })
    broadcastId: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'user_id',
    })
    userId: string;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
