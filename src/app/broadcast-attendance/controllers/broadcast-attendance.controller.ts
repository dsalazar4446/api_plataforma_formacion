import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, Req} from '@nestjs/common';
import { BroadcastAttendanceService } from '../services/broadcast-attendance.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { BroadcastAttendanceSaveJson, BroadcastAttendanceUpdateJson } from '../interfaces/broadcast-attendance.interface';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { BroadcastAttendanceModel } from '../models/broadcast-attendance.model';
import { UserModel } from '../../user/models/user.Model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { BroadcastModel } from '../../broadcast/models/broadcast.model';
@Controller('broadcast-attendance')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// @UseGuards(new JwtAuthGuard())
export class BroadcastAttendanceController {
    constructor(
        private readonly broadcastAttendanceService: BroadcastAttendanceService,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('BroadcastAttendanceModel') private readonly broadcastAttendanceModel: typeof BroadcastAttendanceModel,
        @Inject('BroadcastModel') private readonly broadcastModel: typeof BroadcastModel,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        @Post()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: BroadcastAttendanceModel,
          })
        async create(@Body() broadcastAttendance: BroadcastAttendanceSaveJson,@Req() req) {
            const data = await this.userModel.findByPk(broadcastAttendance.userId);
            if(data == null){
                  throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                      messageCode: 'EA4',
                      languageType:  req.headers.language,
                  });
            }
            const data2 = await this.broadcastModel.findByPk(broadcastAttendance.broadcastId);
            if(data2 == null){
                  throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                      messageCode: 'EA22',
                      languageType:  req.headers.language,
                  });
            }
            return await this.broadcastAttendanceService.create(broadcastAttendance);
        }
        @Get()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: BroadcastAttendanceModel,
            isArray: true
          })
        async getAll() {
            return await this.broadcastAttendanceService.findAll();
        }
        /*@Get(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: BroadcastAttendanceModel,
          })
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        async findById(@Param('id') idBroadcastAttendance) {
            return await this.broadcastAttendanceService.findById(idBroadcastAttendance);
        }
        @Put(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        }) 
        @ApiResponse({
            status: 200,
            type: BroadcastAttendanceModel,
          })
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        async update(
            @Body()
            updateBroadcastAttendance: Partial<BroadcastAttendanceUpdateJson>, 
            @Param('id') idBroadcastAttendance,
            @Req() req
        ) {
            if(updateBroadcastAttendance.userId){
                const data = await this.userModel.findByPk(updateBroadcastAttendance.userId);
                if(data == null){
                    throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                        messageCode: 'EA4',
                        languageType:  req.headers.language,
                    });
                }
            }
            if(updateBroadcastAttendance.broadcastId){
                const data2 = await this.broadcastModel.findByPk(updateBroadcastAttendance.broadcastId);
                if(data2 == null){
                    throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                        messageCode: 'EA22',
                        languageType:  req.headers.language,
                    });
                }
            }
            const data3 = await this.broadcastAttendanceModel.findByPk(idBroadcastAttendance);
            if(data3 == null){
                  throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                      messageCode: 'EA23',
                      languageType:  req.headers.language,
                  });
            }
            return await this.broadcastAttendanceService.update(idBroadcastAttendance, updateBroadcastAttendance);
        }
        @Delete(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        async delete(@Param('id') idBroadcastAttendance) {
            return await this.broadcastAttendanceService.deleted(idBroadcastAttendance);
        }*/
}
