import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class BroadcastAttendanceSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    broadcastId: string;
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    userId: string; 
}
// tslint:disable-next-line: max-classes-per-file
export class BroadcastAttendanceUpdateJson  extends BroadcastAttendanceSaveJson{
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}
