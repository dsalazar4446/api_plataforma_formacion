import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { BroadcastAttendanceController } from './controllers/broadcast-attendance.controller';
import { BroadcastAttendanceService } from './services/broadcast-attendance.service';
import { BroadcastAttendanceModel } from './models/broadcast-attendance.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { BroadcastModel } from '../broadcast/models/broadcast.model';
const models = [
  BroadcastAttendanceModel,
  BroadcastModel,
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [BroadcastAttendanceController],
  providers: [BroadcastAttendanceService]
})
export class BroadcastAttendanceModule {}
