import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, UsePipes, FileFieldsInterceptor, UseInterceptors, UseFilters, UseGuards, Inject, Req} from '@nestjs/common';
import { NotificationService } from '../services/notifications.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { NotificationSaveJson, NotificationUpdateJson } from '../interfaces/notification.interface';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiImplicitFile, ApiConsumes, ApiImplicitParam, ApiImplicitQuery, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { NotificationModel } from '../models/notifications.model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
@Controller('notifications')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class NotificationsController {
    constructor(
        private readonly notificationService: NotificationService,
        @Inject('NotificationModel') private readonly notificationModel: typeof NotificationModel,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        @ApiConsumes('multipart/form-data')
        @ApiImplicitFile({ name: 'notificationIcon', required: true})
        @ApiImplicitFile({ name: 'notificationMimeSoundFile', required: true})
        @Post()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: NotificationModel,
          })
        @UseInterceptors(FileFieldsInterceptor([
                { name: 'notificationIcon', maxCount: 1 },
                { name: 'notificationMimeSoundFile', maxCount: 1 },
            ], {
            storage: diskStorage({
            destination: './uploads'
            , filename: (req, file, cb) => {
                const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                if(file.fieldname === 'notificationIcon'){
                    req.body.notificationIcon = `${randomName}${extname(file.originalname)}`;
                }
                if(file.fieldname === 'notificationMimeSoundFile'){
                    req.body.notificationMimeSoundFile = `${randomName}${extname(file.originalname)}`;
                }
                cb(null, `${randomName}${extname(file.originalname)}`)
            }
            })
        }))
        async create(@Body() notification: NotificationSaveJson) {
            return await this.notificationService.create(notification);
        }
        @Get()
        @ApiImplicitHeader({ 
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: NotificationModel,
            isArray: true
          })
        async getAll() {
            return await this.notificationService.findAll();
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Get(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: NotificationModel,
          })
        async findById(@Param('id') idNotigication) {
            return await this.notificationService.findById(idNotigication);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @ApiConsumes('multipart/form-data')
        @ApiImplicitFile({ name: 'notificationIcon', required: true})
        @ApiImplicitFile({ name: 'notificationMimeSoundFile', required: true})
        @Put(':id')
        @ApiResponse({
            status: 200,
            type: NotificationModel,
          })
        @UseInterceptors(FileFieldsInterceptor([
            { name: 'notificationIcon', maxCount: 1 },
            { name: 'notificationMimeSoundFile', maxCount: 1 },
            ], {
            storage: diskStorage({
            destination: './uploads'
            , filename: (req, file, cb) => {
                const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                if(file.fieldname === 'notificationIcon'){
                    req.body.notificationIcon = `${randomName}${extname(file.originalname)}`;
                }
                if(file.fieldname === 'notificationMimeSoundFile'){
                    req.body.notificationMimeSoundFile = `${randomName}${extname(file.originalname)}`;
                }
                cb(null, `${randomName}${extname(file.originalname)}`)
            }
            })
        }))
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async update(
            @Body()
            updateNotification: Partial<NotificationUpdateJson>, 
            @Param('id') idNotification,
            @Req() req
        ) {
            const data3 = await this.notificationModel.findByPk(updateNotification.id);
            if(data3 == null){
                throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA14',
                    languageType:  req.headers.language,
                });
            }
            return await this.notificationService.update(idNotification, updateNotification);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Delete(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async delete(@Param('id') idNotification) {
            return await this.notificationService.deleted(idNotification);
        }
}
