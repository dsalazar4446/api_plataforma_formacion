import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { NotificationsController } from './controllers/notifications.controller';
import { NotificationService } from './services/notifications.service';
import { NotificationModel } from './models/notifications.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
const models = [
  NotificationModel,
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [NotificationsController],
  providers: [NotificationService]
})
export class NotificationsModule {}
