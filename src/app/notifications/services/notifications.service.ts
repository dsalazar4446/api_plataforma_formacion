import { Injectable, Inject } from '@nestjs/common';
import { NotificationSaveJson, NotificationUpdateJson } from '../interfaces/notification.interface';
import { NotificationModel } from '../models/notifications.model';
import { CONFIG } from '../../../config/index';
@Injectable()
export class NotificationService {
    constructor(
        @Inject('NotificationModel') private readonly notificationModel: typeof NotificationModel,
    ) { }
    async findAll(): Promise<NotificationModel[]> {
        const result  = await this.notificationModel.findAll();
        result.map(
            (data)=>{
                data.dataValues.notificationIcon = CONFIG.storage.server + data.dataValues.notificationIcon;
                data.dataValues.notificationMimeSoundFile = CONFIG.storage.server + data.dataValues.notificationMimeSoundFile;
                return data;
            }
        )
        return result;
    }
    async findById(id: string): Promise<NotificationModel> {
        const result =  await this.notificationModel.findById<NotificationModel>(id);
        result.dataValues.notificationIcon = CONFIG.storage.server + result.dataValues.notificationIcon;
        result.dataValues.notificationMimeSoundFile = CONFIG.storage.server + result.dataValues.notificationMimeSoundFile;
        return result;
    }
    async create(notification: NotificationSaveJson): Promise<NotificationModel> {
        const result =  await this.notificationModel.create<NotificationModel>(notification, {
            returning: true,
        });
        result.dataValues.notificationIcon = CONFIG.storage.server + result.dataValues.notificationIcon;
        result.dataValues.notificationMimeSoundFile = CONFIG.storage.server + result.dataValues.notificationMimeSoundFile;
        return result;
    }
    async update(idNotification: string, notificationUpdate: Partial<NotificationUpdateJson>){
        const result =  await this.notificationModel.update(notificationUpdate, {
            where: {
                id: idNotification,
            },
            returning: true,
        });
        result[1][0].dataValues.notificationIcon = CONFIG.storage.server + result[1][0].dataValues.notificationIcon;
        result[1][0].dataValues.notificationMimeSoundFile = CONFIG.storage.server + result[1][0].dataValues.notificationMimeSoundFile;
        return result;
    }
    async deleted(idNotification: string): Promise<any> {
        return await this.notificationModel.destroy({
            where: {
                id: idNotification,
            },
        });
    }
}
