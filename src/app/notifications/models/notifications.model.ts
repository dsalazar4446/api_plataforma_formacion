import { Column, DataType, Table, Model, CreatedAt, UpdatedAt } from 'sequelize-typescript';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'notifications',
})
export class NotificationModel extends Model<NotificationModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        field: 'notification_type',
    })
    notificationType: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'notification_icon',
    })
    notificationIcon: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(75),
        allowNull: false,
        field: 'notification_mime_sound_file',
    })
    notificationMimeSoundFile: string;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
