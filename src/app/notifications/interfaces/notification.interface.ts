import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class NotificationSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    notificationType: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    notificationIcon: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    notificationMimeSoundFile: string;
}
// tslint:disable-next-line: max-classes-per-file
export class NotificationUpdateJson  extends NotificationSaveJson{
@ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}
