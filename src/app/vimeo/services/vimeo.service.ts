import { Injectable, HttpService, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { CONFIG } from "../../../config";
import { AxiosResponse } from 'axios';
@Injectable()
export class VimeoServices {
    constructor(
        private readonly http: HttpService,
    ) {}
    auth(){
        return {
            'accessToken': CONFIG.vimeo.accessToken
        };
    }
    async list(){
        try {
            const url = 'https://api.vimeo.com/me/videos';
            const result: any = await this.http.get(url,{
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${CONFIG.vimeo.accessToken}`
                }
            }).toPromise();
            return result.data;
        } catch (error) {
            return error;
        }
    }
    async detail(id: string){
        try {
            const url = `https://api.vimeo.com/videos/${id}`;
            const result: any = await this.http.get(url,{
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${CONFIG.vimeo.accessToken}`
                }
            }).toPromise();
            return result.data;
        } catch (error) {
            return error;
        }
    }
    async delete(id: string){
        try {
            const url = `https://api.vimeo.com/videos/${id}`;
            const result: any = await this.http.delete(url,{
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${CONFIG.vimeo.accessToken}`
                }
            }).toPromise();
            return result.data;
        } catch (error) {
            return error;
        }
    }
}