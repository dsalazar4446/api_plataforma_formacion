import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, Param, NotFoundException, Put, Delete, Query, Res } from "@nestjs/common";
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { ApiImplicitParam, ApiUseTags } from "@nestjs/swagger";
import { VimeoServices } from "../services/vimeo.service";

@ApiUseTags('Module-Vimeo')
@Controller('vimeo')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class VimeoController {

    constructor(
       private vimeoServices: VimeoServices
    ) { }
    @Post('auth')
    async auth() {
        return await this.vimeoServices.auth();
    }
    @Get()
    async list() {
        return await this.vimeoServices.list();
    }
    @Get(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detail(@Param('id') idVideoVimeo) {
        return await this.vimeoServices.detail(idVideoVimeo);
    }
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') idVideoVimeo) {
        return await this.vimeoServices.delete(idVideoVimeo);
    }
}