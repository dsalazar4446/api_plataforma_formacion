import { Module, HttpModule, Provider } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { VimeoServices } from './services/vimeo.service';
import { VimeoController } from './controllers/vimeo.controller';


const models = [
];

const providers: Provider[] = [
  VimeoServices
];

const controllers = [
  VimeoController
];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers
})
export class VimeoModule { }
