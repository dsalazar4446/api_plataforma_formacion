import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { InboxController } from './controllers/inbox.controller';
import { InboxService } from './services/inbox.service';
import { InboxModel } from './models/inbox.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
const models = [
  InboxModel,
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [InboxController],
  providers: [InboxService]
})
export class InboxModule {}
