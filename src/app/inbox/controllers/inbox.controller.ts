import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, Req, HttpStatus } from '@nestjs/common';
import { InboxService } from '../services/inbox.service';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { InboxSaveJson, InboxUpdateJson } from '../interfaces/inbox.interface';
import { JwtAuthGuard } from '../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { InboxModel } from '../models/inbox.model';
import { UserModel } from '../../user/models/user.Model';
import { AppUtilsService } from '../../shared/services/app-utils.service';

@ApiUseTags('Module-Inbox')
@Controller('inbox')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class InboxController {
    constructor(
        private readonly inboxService: InboxService,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('InboxModel') private readonly inboxModel: typeof InboxModel,
        private readonly appUtilsService: AppUtilsService,
    ) { }
    @ApiResponse({
        status: 200,
        type: InboxModel,
      })
    @Post()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async create(@Body() inbox: InboxSaveJson, @Req() req) {
        const data2 = await this.userModel.findByPk(inbox.userId);
        if (data2 == null) {
            throw this.appUtilsService.httpCommonError('Parent not exists!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA3',
                languageType: req.headers.language,
            });
        }
        return await this.inboxService.create(inbox);
    }
    @Get()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: InboxModel,
        isArray: true
      })
    async getAll() {
        return await this.inboxService.findAll();
    }
    @Get(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: InboxModel,
      })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idInbox) {
        return await this.inboxService.findById(idInbox);
    }
    @Put(':id')
    @ApiImplicitHeader({ 
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: InboxModel,
      })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(
        @Body()
        updateInbox: Partial<InboxUpdateJson>,
        @Param('id') idInbox,
        @Req() req
    ) {
        if(updateInbox.userId){
            const data2 = await this.userModel.findByPk(updateInbox.userId);
            if (data2 == null) {
                throw this.appUtilsService.httpCommonError('Parent not exists!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA3',
                    languageType: req.headers.language,
                });
            }
        }
        const data3 = await this.inboxModel.findByPk(idInbox);
        if (data3 == null) {
            throw this.appUtilsService.httpCommonError('Parent not exists!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA31',
                languageType: req.headers.language,
            });
        }

        return await this.inboxService.update(idInbox, updateInbox);
    }
    @Delete(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') idInbox) {
        return await this.inboxService.deleted(idInbox);
    }
}
