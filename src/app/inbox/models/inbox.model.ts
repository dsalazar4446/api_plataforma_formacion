import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey, HasMany, BelongsTo} from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { InboxRecieversModel } from '../../inboxRecievers/models/inbox-recievers.model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'inboxes',
})
export class InboxModel extends Model<InboxModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'user_id',
    })
    userId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'subject',
    })
    subject: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'body',
    })
    body: string;

    @BelongsTo(() => UserModel)
    UserModel: UserModel;

    @HasMany(() => InboxRecieversModel)
    InboxRecieversModel: InboxRecieversModel[];
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
