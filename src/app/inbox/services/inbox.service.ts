import { Injectable, Inject } from '@nestjs/common';
import { InboxSaveJson, InboxUpdateJson } from '../interfaces/inbox.interface';
import { InboxModel } from '../models/inbox.model';
import { InboxRecieversModel } from '../../inboxRecievers/models/inbox-recievers.model';
@Injectable() 
export class InboxService {
    constructor(
        @Inject('InboxModel') private readonly inboxModel: typeof InboxModel
    ) { }

// tslint:disable-next-line: max-line-length
    async findAll(): Promise<any> {
        return await this.inboxModel.findAll();
    }

    async findById(id: string): Promise<InboxModel> {
        return await this.inboxModel.findById<InboxModel>(id,{
            include: [
                {
                    model: InboxRecieversModel
                }
            ]
        });
    }

    async create(inbox: InboxSaveJson): Promise<InboxModel> {
        return await this.inboxModel.create<InboxModel>(inbox, {
            returning: true,
        });
    }
    async update(idInbox: string, inboxUpdate: Partial<InboxUpdateJson>){
        return   await this.inboxModel.update(inboxUpdate, {
            where: {
                id: idInbox,
            },
            returning: true,
        });
    }
    async deleted(idInbox: string): Promise<any> {
        return await this.inboxModel.destroy({
            where: {
                id: idInbox,
            },
        });
    }
}
