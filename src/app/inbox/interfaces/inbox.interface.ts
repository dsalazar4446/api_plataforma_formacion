import { ApiModelProperty } from '@nestjs/swagger';
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class InboxSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    userId: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    subject: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    body: string;
}
// tslint:disable-next-line: max-classes-per-file
export class InboxUpdateJson extends InboxSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    id: string;
}