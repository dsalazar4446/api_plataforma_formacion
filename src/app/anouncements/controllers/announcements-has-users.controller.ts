import { Controller, UseFilters, UseInterceptors, Body, HttpStatus, Post, UsePipes, Delete, NotFoundException, Param, UseGuards, Get } from "@nestjs/common";
import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { AnnouncementsHasUsersServices } from "../services/announcements-has-users.service";
import { AnouncementsHasUsersSaveJson } from "../interfaces/announcements-has-users.interfece";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from "@nestjs/swagger";
import { JwtAuthGuard } from "../../auths/guards/jwt-auth.guard";


/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller AnnouncementsHasUsersController
 * @Creado 04 Abril 2019
 */
@ApiUseTags('Module-Announcements')
@Controller('announcements-has-users')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// @UseGuards(new JwtAuthGuard())
export class AnnouncementsHasUsersController {

    constructor(private readonly _announcementsHasUsersServices: AnnouncementsHasUsersServices,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyAnnouncements: AnouncementsHasUsersSaveJson) {

        const createAnnouncements = await this._announcementsHasUsersServices.saveAnnouncementsHasUsers(bodyAnnouncements);

        if (!createAnnouncements) {
            throw this.appUtilsService.httpCommonError('Announcements no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM1',
                languageType: 'es',
            });
        }

        return createAnnouncements; 

    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async getAll() {
        const data = await this._announcementsHasUsersServices.getAll();

        if (!data[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return data;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteAnnouncements(@Param('id') idAnnouncements: string) {

        const deleteAnnouncements = await this._announcementsHasUsersServices.destroyAnnouncementsHasUsers(idAnnouncements);

        if (!deleteAnnouncements) {
            throw this.appUtilsService.httpCommonError('Announcements no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM2',
                languageType: 'es',
            });
        }
        return deleteAnnouncements;
    }

}