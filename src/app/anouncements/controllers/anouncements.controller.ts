import { Controller, UseGuards, UseFilters, UseInterceptors, Post, Body, HttpStatus, Get, NotFoundException, Put, Delete, Param, Req, FileFieldsInterceptor } from "@nestjs/common";
import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { AnnouncementsServices } from "../services/announcements.service";
import { AnnouncementsSaveJson } from "../interfaces/anouncements.interface";
import { ApiUseTags, ApiImplicitParam, ApiConsumes, ApiImplicitFile, ApiImplicitHeader } from "@nestjs/swagger";
import { JwtAuthGuard } from "../../auths/guards/jwt-auth.guard";
import * as file from './infoFile';
import { CONFIG } from "../../../config";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller AnnouncementsController
 * @Creado 04 Abril 2019
 */

@ApiUseTags('Module-Announcements')
@Controller('announcements')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// @UseGuards(new JwtAuthGuard())
export class AnnouncementsController {

    constructor(private readonly _announcementsServices: AnnouncementsServices,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    @ApiConsumes('multipart/form-data')
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiImplicitFile({ name: 'Announcements', description: 'Announcements Picture File', required: true })
    async create(@Req() req, @Body() bodyAnnouncements: AnnouncementsSaveJson) {

        if (req.headers.language) {
            bodyAnnouncements.languageType = req.headers.language;
        } else {
            bodyAnnouncements.languageType = 'es';
        }

        const createAnnouncements = await this._announcementsServices.saveAnouncements(bodyAnnouncements);

        if (!createAnnouncements) {
            throw this.appUtilsService.httpCommonError('Announcements does not created!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM1',
                languageType: 'es',
            });
        }

        createAnnouncements.announcementImage = `${CONFIG.storage.server}announcementImage/${createAnnouncements.announcementImage}`;

        return createAnnouncements;

    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAllAnnouncements() {

        const fetchAll = await this._announcementsServices.showAllAnouncements();

        if (!fetchAll) {
            throw this.appUtilsService.httpCommonError('Announcements does not exist!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM2',
                languageType: 'es',
            });
        }
        fetchAll.forEach(fetch => fetch.announcementImage = `${CONFIG.storage.server}announcementImage/${fetch.announcementImage}`);

        return fetchAll;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idAnnouncements: string) {

        const fetchAnnouncements = await this._announcementsServices.getDetails(idAnnouncements);

        if (!fetchAnnouncements) {
            throw this.appUtilsService.httpCommonError('Announcements does not exist!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM2',
                languageType: 'es',
            });
        }
        fetchAnnouncements.announcementImage = `${CONFIG.storage.server}announcementImage/${fetchAnnouncements.announcementImage}`;
        return fetchAnnouncements;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllProd(@Req() req) {
        const fetchAll = await this._announcementsServices.showAll(req.headers.language);

        if (!fetchAll) {
            throw this.appUtilsService.httpCommonError('Language does not exist!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM3',
                languageType: 'es',
            });
        }

        fetchAll.forEach(fetch => fetch.announcementImage = `${CONFIG.storage.server}announcementImage/${fetch.announcementImage}`);

        return fetchAll;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiConsumes('multipart/form-data')
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateAnnouncements(@Req() req, @Param('id') id: string, @Body() bodyAnnouncements: AnnouncementsSaveJson) {

        if (req.headers.language) bodyAnnouncements.languageType = req.headers.language;

        const updateAnnouncements = await this._announcementsServices.updateAnouncements(id, bodyAnnouncements);

        if (!updateAnnouncements[1][0]) {
            throw this.appUtilsService.httpCommonError('Announcements does not exist!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM2',
                languageType: 'es',
            });
        }
        updateAnnouncements[1][0].announcementImage = `${CONFIG.storage.server}announcementImage/${updateAnnouncements[1][0].announcementImage}`;

        return updateAnnouncements;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteAnnouncements(@Param('id') idAnnouncements: string) {

        const deleteAnnouncements = await this._announcementsServices.destroyAnouncements(idAnnouncements);

        if (!deleteAnnouncements) {
            throw this.appUtilsService.httpCommonError('Announcements does not exist!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM2',
                languageType: 'es',
            });
        }

        return deleteAnnouncements;
    }


}