import { IsString, IsUUID, IsNotEmpty, IsOptional } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
export class AnnouncementsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    senderUserId: string;

    @ApiModelProperty()
    @IsString({message: 'announcementTitle property must a be string'})
    @IsNotEmpty({message: 'announcementTitle property not must null'})
    announcementTitle: string;

    @ApiModelProperty()
    @IsString({message: 'announcementDescription property must a be string'})
    @IsNotEmpty({message: 'announcementDescription property not must null'})
    announcementDescription: string;

    @ApiModelProperty()
    @IsString({message: 'announcementImage property must a be string'})
    @IsOptional()
    announcementImage?: string;

    @ApiModelProperty()
    @IsString({message: 'announcementStatus property must a be string'})
    @IsOptional()
    announcementStatus: string;
    
    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsString({message: 'languageType property must a be string'})
    @IsOptional()
    languageType: string;
}
export class AnnouncementsUpdateJson extends AnnouncementsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
