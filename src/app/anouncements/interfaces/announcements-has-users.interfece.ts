import { IsUUID, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
export class AnouncementsHasUsersSaveJson {
    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    userId: string;
    @ApiModelProperty()
    @IsUUID('4',{message: 'announcementId property must a be uuid'})
    @IsNotEmpty({message: 'announcementId property not must null'})
    announcementId: string;
}
export class AnouncementsHasUsersUpdateJson extends AnouncementsHasUsersSaveJson{
    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
