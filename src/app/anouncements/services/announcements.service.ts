import { Injectable, Inject } from "@nestjs/common";
import { Announcements } from "../models/anouncements.model";
import { AnnouncementsSaveJson, AnnouncementsUpdateJson } from "../interfaces/anouncements.interface";
import { UserModel } from "../../user/models/user.Model";
import { ValidateUUID } from "../../shared/utils/validateUUID";
import * as path from 'path';
import * as fs from 'fs';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services AnouncementsServices
 * @Creado 04 de abril 2019
 */

@Injectable()
export class AnnouncementsServices {

    private language = ['es', 'en', 'it', 'pr'];

    constructor(
        @Inject('Announcements') private readonly _announcements: typeof Announcements,
        private readonly _validateUUID: ValidateUUID
    ) { }

    async saveAnouncements(bodyAnnouncements: AnnouncementsSaveJson): Promise<AnnouncementsUpdateJson> {

        if (!bodyAnnouncements.announcementImage) bodyAnnouncements.announcementImage = 'default.jpeg';

        return await new this._announcements(bodyAnnouncements).save();
    }

    async showAllAnouncements(): Promise<AnnouncementsUpdateJson[]> {

        return await this._announcements.findAll();
    }

    async getDetails(anounId: string): Promise<AnnouncementsUpdateJson> {

        this._validateUUID.validateUUIID(anounId);
        return await this._announcements.findByPk(anounId, {
            include: [
                {
                    model: UserModel
                }
            ]
        });
    }

    async showAll(languageType: string): Promise<AnnouncementsUpdateJson[]> {

        if (this.language.indexOf(languageType) >= 0) {

            return await this._announcements.findAll({
                where: {
                    languageType
                }
            });
        }

        return;

    }

    async updateAnouncements(id: string, bodyAnnouncements: Partial<AnnouncementsUpdateJson>): Promise<[number, Array<any>]> {

        this._validateUUID.validateUUIID(id);
        if (bodyAnnouncements.announcementImage) {
            this.deleteImg(id);
        }
        return await this._announcements.update(bodyAnnouncements, {
            where: { id },
            returning: true
        });

    }

    async destroyAnouncements(announId: string): Promise<number> {

        this._validateUUID.validateUUIID(announId);
        this.deleteImg(announId);
        return await this._announcements.destroy({
            where: { id: announId }
        });
    }

    private async deleteImg(id: string) {

        const img = await this._announcements.findByPk(id).then(item => item.announcementImage).catch(err => err);

        if (img === 'default.jpeg') return;

        const pathImagen = path.resolve(__dirname, `../../../../uploads/announcementImage/${img}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }
}