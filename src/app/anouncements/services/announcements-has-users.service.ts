import { Injectable, Inject } from "@nestjs/common";
import { AnnouncementsHasUsers } from "../models/announcements-has-users.model";
import { AnouncementsHasUsersSaveJson, AnouncementsHasUsersUpdateJson } from "../interfaces/announcements-has-users.interfece";
import { ValidateUUID } from "../../shared/utils/validateUUID";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services AnnouncementsHasUsersServices
 * @Creado 04 de abril 2019
 */

@Injectable()
export class AnnouncementsHasUsersServices {

    constructor(
        @Inject('AnnouncementsHasUsers') private readonly _announcementsHasUsers: typeof AnnouncementsHasUsers,
        private readonly _validateUUID: ValidateUUID
    ) { }

    async saveAnnouncementsHasUsers(bodyAnnouncementsHasUsers: AnouncementsHasUsersSaveJson): Promise<AnouncementsHasUsersUpdateJson> {

        return await new this._announcementsHasUsers(bodyAnnouncementsHasUsers).save();
    }

    async getAll(): Promise<AnouncementsHasUsersUpdateJson[]> {

        return await this._announcementsHasUsers.findAll();
    }
    async destroyAnnouncementsHasUsers(bodyAnnouncementsHasUsersId: string): Promise<number> {

        this._validateUUID.validateUUIID(bodyAnnouncementsHasUsersId);
        return await this._announcementsHasUsers.destroy({
            where: { id: bodyAnnouncementsHasUsersId },
        });
    }



}