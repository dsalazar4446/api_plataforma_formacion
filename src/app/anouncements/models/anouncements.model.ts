import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsToMany } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { AnnouncementsHasUsers } from './announcements-has-users.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';


@Table({
    tableName: 'announcements',
})
export class Announcements extends Model<Announcements>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_id'
    })
    senderUserId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'announcement_title'
    })
    announcementTitle: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'announcement_description'
    })
    announcementDescription: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(45),
        allowNull: true,
        field: 'announcement_image'
    })
    announcementImage: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1','2','3'),
        defaultValue: '1',
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'announcement_status'
    })
    announcementStatus: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'language_type'
    })
    languageType: string;
    
    @ApiModelPropertyOptional()
    @BelongsToMany(() => UserModel, {
        through: {
          model: () => AnnouncementsHasUsers,
          unique: true,
        }
      })
    userModel: UserModel[];

    @CreatedAt created_at: Date;
    @UpdatedAt updated_at: Date;
}