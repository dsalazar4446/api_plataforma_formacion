import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany, ForeignKey } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { Announcements } from './anouncements.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';


@Table({
    tableName: 'announcements_has_users',
})
export class AnnouncementsHasUsers extends Model<AnnouncementsHasUsers>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_id'
    })
    userId: string;
    
    @ApiModelPropertyOptional()
    @ForeignKey(() => Announcements)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'announcement_id'
    })
    announcementId: string;

    @CreatedAt created_at: Date;
    @UpdatedAt updated_at: Date;

}