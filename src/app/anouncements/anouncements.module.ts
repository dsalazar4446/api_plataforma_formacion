import { Module, HttpModule, Provider } from "@nestjs/common";
import { SharedModule } from "../shared/shared.module";
import { DatabaseModule } from "../database/database.module";
import { Announcements } from './models/anouncements.model';
import { AnnouncementsHasUsers } from './models/announcements-has-users.model';
import { AnnouncementsHasUsersServices } from './services/announcements-has-users.service';
import { AnnouncementsServices } from './services/announcements.service';
import { AnnouncementsController } from './controllers/anouncements.controller';
import { AnnouncementsHasUsersController } from './controllers/announcements-has-users.controller';


const models = [ AnnouncementsHasUsers, Announcements ];
const controllers = [AnnouncementsController, AnnouncementsHasUsersController];
const providers: Provider[] = [AnnouncementsHasUsersServices, AnnouncementsServices];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers
})
export class AnouncementsModule { }
