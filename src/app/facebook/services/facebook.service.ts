import { Injectable, Inject, NotFoundException, HttpService } from '@nestjs/common';
import * as oauth2lib from "simple-oauth2";
import { CONFIG } from '../../../config';
@Injectable()
export class FacebookServices {
    options: any;
    oauth2: any;
    private url = "https://graph.facebook.com/2250058571910580/feed?access_token=EAAfZBapXnDbQBAHpu07wyhbjE0yd1wRG31V7l9oIACysLb8RKn9lg0z8RtdVpZBVcDqBQSb02YwRsfxTuDjYZAWNT3saGZBGonmHqpEYWDVkNVUyuQmwg1J2U5XDoYwZAmMlUN9oi2DZB98UJw70lZBjhmJwdz4XpoZD";

    constructor(
        private readonly _http: HttpService
    ) {
        this.options = {
            client: {
                id: '2250058571910580',
                secret: '2129ef640cd499b44cbcb3e9963705e3',
            },
            auth: {
                authorizeHost: 'https://facebook.com/',
                authorizePath: '/dialog/oauth',
                tokenHost: 'https://graph.facebook.com',
                tokenPath: '/oauth/access_token',
            },
        };
        this.oauth2 = oauth2lib.create(this.options);
    }
    

    async create(accessToken: string) {
        return await this._http.get(`https://graph.facebook.com/2250058571910580/feed?access_token=${accessToken}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).toPromise();
    }

    

    async auth() {
        return new Promise(
            (resolve, reject) => {
                try {
                    const authorizationUri = this.oauth2.authorizationCode.authorizeURL({
                        redirect_uri: 'http://localhost:3000/api/v1/facebook/auth/callback',
                        scope: ['email'],
                    });
                    resolve(authorizationUri);
                } catch (error) {
                    reject(error);
                }
            },
        );
    }
    async callback(code) {
        return new Promise(async (resolve, reject) => {
            const options = {
                code,
                redirect_uri: 'http://localhost:3000/api/v1/facebook/auth/callback',
            };
            try {
                const result = await this.oauth2.authorizationCode.getToken(options);
                const token = await this.oauth2.accessToken.create(result);
                resolve(token);
            } catch (error) {
                reject(error.message);
            }
        })
    }
    async refreshToken(token) { }

}
