import { Module, HttpModule, Provider } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { FacebookServices } from './services/facebook.service';
import { FacebookController } from './controllers/facebook.controller';


const models = [
];

const providers: Provider[] = [
  FacebookServices
];

const controllers = [
  FacebookController
];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers
})
export class FacebookModule { }
