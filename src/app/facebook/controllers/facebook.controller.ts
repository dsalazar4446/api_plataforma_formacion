import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, Param, NotFoundException, Put, Delete, Query, Res } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { FacebookServices } from "../services/facebook.service";
import { ApiUseTags, ApiImplicitParam } from "@nestjs/swagger";

@ApiUseTags('Module-Facebook')
@Controller('facebook')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class FacebookController {

    constructor(
        private facebookServices: FacebookServices
    ) { }
    @Get('auth')
    async auth() {
        return await this.facebookServices.auth();
    }
    @Get('auth/callback')
    async getToken(@Query('code') code) {
        return await this.facebookServices.callback(code);
    }
    @Get('post/:token')
    async create(@Param() token: string) {
        return await this.facebookServices.create(token);
    }
}