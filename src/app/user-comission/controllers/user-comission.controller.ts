import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, HttpStatus, Res, Req } from '@nestjs/common';
import { UserComissionService } from '../services/user-comission.service';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { UserComissionSaveJson, UserComissionUpdateJson, UserComissionMembershipSaveJson, UserComissionRegister } from '../interfaces/user-comission.interface';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiResponse, ApiImplicitHeader, ApiUseTags } from '@nestjs/swagger';
import { UserComissionModel } from '../models/user-comission.model';
import { UserModel } from '../../user/models/user.Model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { UserMembershipsModel } from '../../memberships/models/user-memberships.model';

@ApiUseTags('Module-User-Comission')
@Controller('user-comission')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class UserComissionController {
    constructor(
        private readonly userComissionService: UserComissionService,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('UserComissionModel') private readonly userComissionModel: typeof UserComissionModel,
        @Inject('UserMembershipsModel') private readonly userMembershipsModel: typeof UserMembershipsModel,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        @Post()
        @ApiImplicitHeader({
          name:'language',
          required: true,
        })
        @ApiResponse({
            status: 200,
            type: UserComissionModel,
          })
        async create(@Body() userComission: UserComissionSaveJson, @Req() req) {
          const data3 = await this.userModel.findByPk(userComission.usersRecieverId);
          if(data3 == null){
              throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
                  messageCode: 'EA4',
                  languageType:  req.headers.language,
              });
          }
            return await this.userComissionService.create(userComission);
        }

        @Post('membership')
        @ApiImplicitHeader({
          name:'language',
          required: true,
        })
        @ApiResponse({
            status: 200,
            type: UserComissionModel,
          })
        async createUserComissionByMembership(@Body() userComission: UserComissionRegister, @Req() req) {
          const data3 = await this.userModel.findByPk(userComission.userChildrenId);
          if(data3 == null){
              throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
                  messageCode: 'EA4',
                  languageType:  req.headers.language,
              });
          }
          const userMembership = await this.userMembershipsModel.findOne({
              where:{
                  userId: userComission.userChildrenId,
                  membershipStatus: true
              }
          })
          // console.log(userMembership)
          if(userMembership == null || userMembership == undefined){
            throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA27',
                languageType:  req.headers.language,
            });
          }
          return await this.userComissionService.createUserComissionByMembership(userComission.userChildrenId);
        }




        @ApiResponse({
            status: 200,
            type: UserComissionModel,
            isArray: true
          })
        @Get()
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        async getAll() {
            return await this.userComissionService.findAll();
        } 
        @ApiResponse({
            status: 200,
            type: UserComissionModel,
          })
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Get(':id')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        async findById(@Param('id') idUserComission) {
            return await this.userComissionService.findById(idUserComission);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @ApiResponse({
            status: 200,
            type: UserComissionModel,
          })
        @Put(':id')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        async update(
            @Body()
            updateUserComission: Partial<UserComissionUpdateJson>,
             @Param('id') idUserComission,
             @Req() req
            ) {
            if(updateUserComission.usersRecieverId){
                const data3 = await this.userModel.findByPk(updateUserComission.usersRecieverId);
                if(data3 == null){
                  throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
                      messageCode: 'EA4',
                      languageType:  req.headers.language,
                  });
                }
              }
            const data2 = await this.userComissionModel.findByPk(idUserComission);
            if(data2 == null){
              throw this.appUtilsService.httpCommonError('Device does not created!', HttpStatus.NOT_ACCEPTABLE, {
                  messageCode: 'EA10',
                  languageType:  req.headers.language,
              });
            }
            return await this.userComissionService.update(idUserComission, updateUserComission);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Delete(':id')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
      async delete(@Param('id') idUserComission) {
            return await this.userComissionService.deleted(idUserComission);
      }
}
