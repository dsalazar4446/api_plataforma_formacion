import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey, HasMany, BelongsTo, BelongsToMany } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { CityModel } from '../../city/models/city.model';
import { CreexPayCards } from '../../creex-pay-card/models/creex-pay-cards.model';
import { Downlines } from '../../downlines/models/downlines.model';
import { UserComissionsHasDownlineModel } from '../../user_comissions_has_downline/models/user_comissions_has_downline.model';
import { ApiModelProperty } from '@nestjs/swagger';
import { UserComissionStatusModel } from '../../user-comission-status/models/user-comission-status.model';

// Como aun no se dispone del modulo citie no se pudo agregar el foreign key con cities
@Table({
    tableName: 'user_comissions',
})
export class UserComissionModel extends Model<UserComissionModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'users_reciever_id',
    })
    usersRecieverId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.ENUM('membership', 'activation', 'broker', 'product', 'event'),
        allowNull: false,
        validate: {
            notEmpty: false,
        },
        field: 'comission_type',
    })
    comissionType: string;
    @ApiModelProperty()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        field: 'comission_ammount',
    })
    comissionAmmount: number;

    @ApiModelProperty()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        field: 'billing_ammount',
    })
    billingAmmount: number;

    @ApiModelProperty()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        field: 'comission_payable',
    })
    comissionPayable: boolean;
    
    @BelongsToMany(() => Downlines, {
        through: {
            model: () => UserComissionsHasDownlineModel,
            unique: true,
        },
        as: 'UserComissionUserComissionHasDownline',
    })

    @ApiModelProperty({
        type: UserComissionStatusModel,
        isArray: true
    })
    @HasMany( () => UserComissionStatusModel )
    userComissionStatusModel: UserComissionStatusModel[];

    @ApiModelProperty()
    @BelongsTo(() => UserModel)
    user: UserModel;
    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
