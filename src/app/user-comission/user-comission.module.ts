import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { UserComissionController } from './controllers/user-comission.controller';
import { UserComissionService } from './services/user-comission.service';
import { UserComissionModel } from './models/user-comission.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { MembershipModel } from '../memberships/models/membership.model';
import { MembershipsComissionsModel } from '../memberships/models/membership-comissions.model';
import { MembershipPricingsModel } from '../memberships/models/membership-pricings.model';
import { Downlines } from '../../app/downlines/models/downlines.model';
import { UserComissionStatusModel } from '../user-comission-status/models/user-comission-status.model';
import { UserComissionsHasDownlineModel } from '../user_comissions_has_downline/models/user_comissions_has_downline.model';
const models = [ 
  UserComissionModel,
  MembershipModel,
  MembershipsComissionsModel,
  MembershipPricingsModel,
  Downlines,
  UserComissionStatusModel,
  UserComissionsHasDownlineModel,
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  exports: [
    UserComissionService
  ],
  controllers: [UserComissionController],
  providers: [UserComissionService]
})
export class UserComissionModule {}
