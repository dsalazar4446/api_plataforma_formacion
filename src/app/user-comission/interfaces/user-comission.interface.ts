import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum, IsNumber, IsDate} from "class-validator";
export class UserComissionSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    usersRecieverId: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    comissionType: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    comissionAmmount: number;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    billingAmmount: number;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    comissionPayable: boolean;
    
}

// tslint:disable-next-line: max-classes-per-file
export class UserComissionMembershipSaveJson extends UserComissionSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    membershipId: string;
}

// tslint:disable-next-line: max-classes-per-file
export class UserComissionUpdateJson extends UserComissionSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    id: string;
}


// tslint:disable-next-line: max-classes-per-file
export class UserComissionRegister {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    userChildrenId: string;
}
