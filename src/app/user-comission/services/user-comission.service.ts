import { Injectable, Inject, Req, HttpStatus } from '@nestjs/common';
import { UserComissionSaveJson, UserComissionUpdateJson, UserComissionMembershipSaveJson } from '../interfaces/user-comission.interface';
import { UserComissionModel } from '../models/user-comission.model';
import { MembershipModel } from '../../memberships/models/membership.model';
import { MembershipsComissionsModel } from '../../memberships/models/membership-comissions.model';
import { MembershipPricingsModel } from '../../memberships/models/membership-pricings.model';
import { Downlines } from '../../downlines/models/downlines.model';
import { UserModel } from '../../user/models/user.Model';
import { UserMembershipsModel } from '../../memberships/models/user-memberships.model';
import { UserComissionStatusModel } from '../../user-comission-status/models/user-comission-status.model';
import { UserComissionsHasDownlineModel } from '../../user_comissions_has_downline/models/user_comissions_has_downline.model';
import { AppUtilsService } from '../../shared/services/app-utils.service';

@Injectable()
export class UserComissionService {
    constructor(
        @Inject('Downlines') private readonly downlines: typeof Downlines,
        @Inject('UserComissionModel') private readonly userComissionModel: typeof UserComissionModel,
        @Inject('UserMembershipsModel') private readonly userMembershipsModel: typeof UserMembershipsModel,
        @Inject('MembershipModel') private readonly membershipModel: typeof MembershipModel,
        @Inject('MembershipsComissionsModel') private readonly membershipsComissionsModel: typeof MembershipsComissionsModel,
        @Inject('MembershipPricingsModel') private readonly membershipPricingsModel: typeof MembershipPricingsModel,
        @Inject('UserComissionStatusModel') private readonly userComissionStatusModel: typeof UserComissionStatusModel,
        @Inject('UserComissionsHasDownlineModel') private readonly userComissionsHasDownlineModel: typeof UserComissionsHasDownlineModel,
        private readonly appUtilsService: AppUtilsService,
    ) { }
    async findAll(): Promise<UserComissionModel[]> {
        return await this.userComissionModel.findAll({
            include: [UserModel]
        });
    }
    async findById(id: string): Promise<UserComissionModel> {
        return await this.userComissionModel.findByPk<UserComissionModel>(id, {
            include: [UserModel]
        });
    }
    async create(userComission: UserComissionSaveJson): Promise<UserComissionModel> {
        return await this.userComissionModel.create<UserComissionModel>(userComission, {
            returning: true,
        });
    }

    async getComission(membershipId: string){
        const amounts = [];
        const membershipComissions = await this.membershipsComissionsModel.findAll({
            where: {
                membershipsId: membershipId,
            }
        });
        await Promise.all(
            await membershipComissions.map(
                async (membershipComission) => {
                    const dataMembershipPricing = await this.membershipPricingsModel.findOne({
                        where: {
                            membershipId,
                        },
                    });
                    const amount = dataMembershipPricing.dataValues.membershipPricingAmmount;
                    if(membershipComission.dataValues.comissionType == 'percentage') {
                        amounts.push({
                            amount,
                            amountComission: (amount*membershipComission.dataValues.comissionActivationAmmount)/100,
                            level: membershipComission.level
                        });
                    }
                    if(membershipComission.dataValues.comissionType == 'money') {
                        amounts.push({
                            amount,
                            amountComission: membershipComission.dataValues.comissionActivationAmmount,
                            level: membershipComission.level
                        });
                    }
                }
            )
        );
        return amounts;
    }

    async getAllFamilyForChildren(id: string){
        return await this.downlines.findAll({
              where:{
                  childrenUserId: id,
              },
              order: [
                  ['downline_level', 'ASC'],
              ],
          });
      }

    async createUserComissionByMembership(children: string){
        const userMembership = await this.userMembershipsModel.findOne({
            where:{
                userId: children,
                membershipStatus: true
            }
        })
        // VALIDAR QUE EL HIJO TENGA UNA MEMBRESIA ACTIVA
        const comissions:any = await this.getComission(userMembership.dataValues.membershipId);
        const families:any = await this.getAllFamilyForChildren(children);
        await Promise.all(
            await comissions.map(
                async (comission)=>{
                    await Promise.all(
                        await families.map(
                            async (family,index)=>{
                                var billingAmount = 0;
                                if(index == 0){
                                    billingAmount = comission.amount;
                                } else {
                                    billingAmount = 0;
                                }
                                if( comission.level == 0){
                                    const dataUserComission = {
                                        usersRecieverId: family.parentUserId,
                                        comissionType: 'membership',
                                        comissionAmmount: comission.amountComission,
                                        billingAmmount: billingAmount,
                                        comissionPayable: true,
                                    };
                                    const resultUserComission = await this.userComissionModel.create(dataUserComission);
                                    const dataUserComissionStatus = {
                                        userComissionsId: resultUserComission.dataValues.id,
                                        userComissionStatus: 'pending',
                                    };
                                    const resultUserComissionStatus = await this.userComissionStatusModel.create(dataUserComissionStatus);
                                    // UserComissionsHasDownlineModel
                                    const aux = {
                                        userComissionsId: resultUserComission.dataValues.id,
                                        downlinesId: family.id
                                    }
                                    const resultUserComissionsHasDownlineModel = await this.userComissionsHasDownlineModel.create(aux);

                                } else {
                                    if(comission.level == family.downlineLevel){
                                        const dataUserComission = {
                                            usersRecieverId: family.parentUserId,
                                            comissionType: 'membership',
                                            comissionAmmount: comission.amountComission,
                                            billingAmmount: billingAmount,
                                            comissionPayable: true,
                                        };
                                        const resultUserComission = await this.userComissionModel.create(dataUserComission);
                                        const dataUserComissionStatus = {
                                            userComissionsId: resultUserComission.dataValues.id,
                                            userComissionStatus: 'pending',
                                        };
                                        const resultUserComissionStatus = await this.userComissionStatusModel.create(dataUserComissionStatus);
                                        const aux = {
                                            userComissionsId: resultUserComission.dataValues.id,
                                            downlinesId: family.id
                                        }
                                        const resultUserComissionsHasDownlineModel = await this.userComissionsHasDownlineModel.create(aux);
                                    }
                                }
                            }
                        )
                    )
                }
            )
        )
        console.log('the commissions were created');
        return 'the commissions were created';
     }
    async update(idUserComission: string, userComissionUpdate: Partial<UserComissionUpdateJson>){
        return  await this.userComissionModel.update(userComissionUpdate, {
            where: {
                id: idUserComission,
            },
            returning: true,
        });
    }

    async deleted(idUserComission: string): Promise<any> {
        return await this.userComissionModel.destroy({
            where: {
                id: idUserComission,
            },
        });
    }

    async findAllPayment(): Promise<UserComissionModel[]> {
        const user = await this.userComissionModel.findAll({
            include: [UserModel,
                {
                    model: UserComissionStatusModel,
                    where: {
                        userComissionStatus: "aproved"
                    }
                }
            ]
        });

        console.log(user);


        return user;
    }
}
