import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { MainVideosController } from './controllers/main-videos.controller';
import { MainVideosService } from './services/main-videos.service';
import { MainVideosModel } from './models/main-videos.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { VimeoServices } from '../vimeo/services/vimeo.service';

const models = [
  MainVideosModel,
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [MainVideosController],
  providers: [MainVideosService, VimeoServices]
})
export class MainVideosModule {}
