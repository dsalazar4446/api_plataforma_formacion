export const MainVideosExample = 
{
    "id": "string",
    "videoAttachment": "string",
    "videoSourceId": "string",
    "videoTotalTime": "string",
    "videoSource": "string",
    "languageType": "string"

}

export const MainVideosArrayExample = 
[
    MainVideosExample
]
