import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, BelongsToMany, HasMany } from 'sequelize-typescript';
import { CoursesModel } from '../../courses/models/courses.model';
import { CoursesMainVideoModel } from '../../courses/models/courses-has-main-videos.model';
import { UserVideoRecordsModel } from '../../classes/models/user-video-records.model';
import { UserVideoFavoritesModel } from '../../classes/models/user-video-favorites.model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'main_videos',
})
export  class MainVideosModel extends Model<MainVideosModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'video_attachment',
    })
    videoAttachment: string;
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'video_source_id',
    })
    videoSourceId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'video_total_time',
    })
    videoTotalTime: string;
    @ApiModelProperty()
    @Column({
        type: DataType.ENUM(
            'vimeo',
            'youtube',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'video_source',
    })
    videoSource: string;
    @ApiModelProperty()
    @Column({
        type: DataType.ENUM(
            'es',
            'en',
            'it',
            'pr',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;

    @BelongsToMany(() => CoursesModel, () => CoursesMainVideoModel)
    courses: CoursesModel[];

    @HasMany(() => UserVideoRecordsModel)
    userVideoRecords: UserVideoRecordsModel[];

    @HasMany(() => UserVideoFavoritesModel)
    userVideoFavorites: UserVideoFavoritesModel[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
