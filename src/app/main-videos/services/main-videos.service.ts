import { Injectable, Inject } from '@nestjs/common';
import { MainVideosSaveJson, MainVideosUpdateJson } from '../interfaces/main-videos.interface';
import { MainVideosModel } from '../models/main-videos.model';
import { CONFIG } from '../../../config/index';
import { VimeoServices } from '../../vimeo/services/vimeo.service';
import { UserVideoRecordsModel } from '../../classes/models/user-video-records.model';
import { UserVideoFavoritesModel } from '../../classes/models/user-video-favorites.model';
@Injectable()
export class MainVideosService {
    private include = [
        {
            model: UserVideoRecordsModel
        }, 
        {
            model: UserVideoFavoritesModel
        }
    ]
    constructor(
        @Inject('MainVideosModel') private readonly mainVideosModel: typeof MainVideosModel,
        private vimeoServices: VimeoServices
    ) { }
    async findAll(): Promise<MainVideosModel[]> {
        const result =  await this.mainVideosModel.findAll({
            include: this.include
        });
        result.map(
            (data)=>{
                data.dataValues.videoAttachment = CONFIG.storage.server + 'mainVideos/'+ data.dataValues.videoAttachment;
                return data;
            }
        )
        return result;
    }
    async findById(id: string): Promise<MainVideosModel> {
        const result = await this.mainVideosModel.findById<MainVideosModel>(id, {
            include: this.include
        });
        result.dataValues.videoAttachment = CONFIG.storage.server + 'mainVideos/'+ result.dataValues.videoAttachment;
        return result;
    }
    async create(mainVideos: MainVideosSaveJson): Promise<MainVideosModel> {
        const result = await this.mainVideosModel.create<MainVideosModel>(mainVideos, {
            returning: true,
        });
        result.dataValues.videoAttachment = CONFIG.storage.server + 'mainVideos/'+result.dataValues.videoAttachment;
        return result;
    }
    async update(idMainVideo: string, mainVideosUpdate: Partial<MainVideosUpdateJson>){
        const result = await this.mainVideosModel.update(mainVideosUpdate, {
            where: {
                id: idMainVideo,
            },
            returning: true,
        });
        result[1][0].dataValues.videoAttachment = CONFIG.storage.server + 'mainVideos/'+ result[1][0].dataValues.videoAttachment;
        return result;
    }
    async deleted(idMainVideo: string): Promise<any> {
        const resultSearch = await this.mainVideosModel.findById<MainVideosModel>(idMainVideo);
        if( resultSearch.dataValues.videoSource === 'vimeo' ) {
            await this.vimeoServices.delete(resultSearch.dataValues.videoSourceId);
        }
        const result =  await this.mainVideosModel.destroy({
            where: {
                id: idMainVideo,
            },
        });
        return result;
    }
}
