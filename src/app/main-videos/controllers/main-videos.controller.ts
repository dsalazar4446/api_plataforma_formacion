import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, UsePipes, FileFieldsInterceptor, UseInterceptors, UseFilters} from '@nestjs/common';
import { MainVideosService } from '../services/main-videos.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { MainVideosSaveJson, MainVideosUpdateJson } from '../interfaces/main-videos.interface';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { ApiImplicitFile, ApiImplicitParam, ApiUseTags, ApiConsumes, ApiImplicitQuery, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { MainVideosModel } from '../models/main-videos.model';
@ApiUseTags('Main-videos')
@Controller('main-video')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class MainVideosController {
    constructor(
        private readonly mainVideosService: MainVideosService,
        ) {}
        @Post()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: MainVideosModel,
          })
        @ApiImplicitQuery({ name: 'languajeType', enum: ['es','en','it','pr'] })
        @UseInterceptors(FileFieldsInterceptor([
                { name: 'videoAttachment', maxCount: 1 },
            ], {
            storage: diskStorage({
            destination: './uploads/mainVideos'
            , filename: (req, file, cb) => {
                let extValidas = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG'];
                const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                if(file.fieldname === 'videoAttachment'){
                    req.body.videoAttachment = `${randomName}${extname(file.originalname)}`;
                }
                cb(null, `${randomName}${extname(file.originalname)}`)
            }
            })
        }))
        @ApiConsumes('multipart/form-data')
        @ApiImplicitFile({ name: 'videoAttachment', required: true, description: 'video' })
       
        async create(@Body() mainVideos: MainVideosSaveJson) {
            return await this.mainVideosService.create(mainVideos);
        }
        @Get()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: MainVideosModel,
            isArray: true
          })
        async getAll() {
            return await this.mainVideosService.findAll();
        }
        @Get(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: MainVideosModel,
          })
        @ApiImplicitParam({name:'id',required:true})
        async findById(@Param('id') idMainVideos) {
            return await this.mainVideosService.findById(idMainVideos);
        }
        @Put(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: MainVideosModel,
          })
        @ApiImplicitParam({ name: 'id', required: true })
        @ApiImplicitQuery({ name: 'languajeType', enum: ['es','en','it','pr'] })
        @UseInterceptors(FileFieldsInterceptor([
                { name: 'videoAttachment', maxCount: 1 },
            ], {
            storage: diskStorage({
            destination: './uploads'
            , filename: (req, file, cb) => {
                let extValidas = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG'];
                const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                if(file.fieldname === 'videoAttachment'){
                    req.body.videoAttachment = `${randomName}${extname(file.originalname)}`;
                }
                cb(null, `${randomName}${extname(file.originalname)}`)
            }
            })
        }))
        @ApiConsumes('multipart/form-data')
        @ApiImplicitFile({ name: 'videoAttachment', required: true, description: 'video' })
        async update(
            @Body()
            mainVideos: Partial<MainVideosUpdateJson>, 
            @Param('id') idMainVideos
        ) {
            return await this.mainVideosService.update(idMainVideos, mainVideos);
        }
        @Delete(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiImplicitParam({ name: 'id', required: true })
        async delete(@Param('id') idMainVideos) {
            return await this.mainVideosService.deleted(idMainVideos);
        }
}
