import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class MainVideosSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    videoAttachment: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    videoTotalTime: string;
    @ApiModelProperty({ enum: ['vimeo','youtube']})
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    videoSource: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    videoSourceId: string;
    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsNotEmpty()
    @IsString()
    languageType: string;
}
// tslint:disable-next-line: max-classes-per-file
export class MainVideosUpdateJson  extends MainVideosSaveJson{
    @ApiModelProperty()
    id: string;
}
