import { Column, DataType, Table, Model, CreatedAt, UpdatedAt} from 'sequelize-typescript';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'chats',
})
export class ChatModel extends Model<ChatModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string; 
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(75),
        allowNull: false,
        field: 'chat_name',
    })
    chatName: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'chat_avatar',
    })
    chatAvatar: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        field: 'chat_description',
    })
    chatDescription: string;
    @ApiModelProperty()
    @Column({
        type: DataType.ENUM(
            '1',
            '2',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'chat_type',
    })
    chatType: string;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
