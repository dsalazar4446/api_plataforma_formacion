import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { ChatController } from './controllers/chat.controller';
import { ChatService } from './services/chat.service';
import { ChatModel } from './models/chat.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { MessageModel } from '../messages/models/messages.model';
import { MessageAttachmentModel } from '../message-attachments/models/message-attachments.model';
const models = [
  ChatModel,
  MessageModel,
  MessageAttachmentModel,
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [ChatController],
  providers: [ChatService]
})
export class ChatModule {}
