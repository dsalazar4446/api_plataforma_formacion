import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, FileFieldsInterceptor, UseInterceptors, UseFilters, UseGuards } from '@nestjs/common';
import { ChatService } from '../services/chat.service';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { ChatSaveJson, ChatUpdateJson } from '../interfaces/chat.interface';
import { JwtAuthGuard } from '../../auths/guards/jwt-auth.guard';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { ApiUseTags, ApiConsumes, ApiImplicitParam, ApiImplicitFile, ApiImplicitQuery, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { ChatModel } from '../models/chat.model';

@ApiUseTags('Module-Chat')
@Controller('chat')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class ChatController {
    constructor(
        private readonly chatService: ChatService,
    ) { }
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'chatAvatar', maxCount: 1 },
    ], {
            storage: diskStorage({
                destination: './uploads/chat/avatar'
                , filename: (req, file, cb) => {
                    const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                    if (file.fieldname === 'chatAvatar') {
                        req.body.membershipIcon = `${randomName}${extname(file.originalname)}`;
                    }
                    cb(null, `${randomName}${extname(file.originalname)}`)
                }
            })
        }))
    @ApiImplicitQuery({ name: 'chatType', enum: ['1','2'] })
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'avatar', description: 'Avatar File', required: true })
    @ApiResponse({
        status: 200,
        type: ChatModel,
      })
    @Post()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async create(@Body() chat: ChatSaveJson) {
        return await this.chatService.create(chat);
    }
    @Get()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: ChatModel,
        isArray: true
      })
    async getAll() {
        return await this.chatService.findAll();
    }
    @Get(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: ChatModel,
      })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idChat) {
        return await this.chatService.findById(idChat);
    }
    @Put(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: ChatModel,
      })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'chatAvatar', maxCount: 1 },
    ], {
            storage: diskStorage({
                destination: './uploads/chat/avatar'
                , filename: (req, file, cb) => {
                    const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                    if (file.fieldname === 'chatAvatar') {
                        req.body.membershipIcon = `${randomName}${extname(file.originalname)}`;
                    }
                    cb(null, `${randomName}${extname(file.originalname)}`)
                }
            })
        }))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitQuery({ name: 'chatType', enum: ['1','2'] })
    @ApiImplicitFile({ name: 'avatar', description: 'Avatar File', required: true })
    @ApiResponse({
        status: 200,
        type: ChatModel,
      })
    async update(
        @Body()
        updateChat: Partial<ChatUpdateJson>,
        @Param('id') idChat
    ) {
        return await this.chatService.update(idChat, updateChat);
    }
    @Delete(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') idChat) {
        return await this.chatService.deleted(idChat);
    }
}
