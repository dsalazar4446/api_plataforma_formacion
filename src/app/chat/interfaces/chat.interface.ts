import { ApiModelProperty } from '@nestjs/swagger';
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum, IsOptional} from "class-validator";
export class ChatSaveJson {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    chatName: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    chatAvatar?: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    chatDescription: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    chatType: string;
}
// tslint:disable-next-line: max-classes-per-file
export class ChatUpdateJson extends ChatSaveJson {
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}