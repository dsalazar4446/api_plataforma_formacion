import { Injectable, Inject } from '@nestjs/common';
import { ChatSaveJson, ChatUpdateJson } from '../interfaces/chat.interface';
import { ChatModel } from '../models/chat.model';
import { Sequelize } from 'sequelize-typescript';
import { CONFIG } from '../../../config';
import * as path from 'path';
import * as fs from 'fs';
import { completeUrl } from '../../shared/utils/completUrl';
import { MessageModel } from '../../messages/models/messages.model';
const Op = Sequelize.Op;

@Injectable() 
export class ChatService {
    constructor(
        @Inject('ChatModel') private readonly chatModel: typeof ChatModel,
        @Inject('MessageModel') private readonly messageModel: typeof MessageModel
    ) { }

// tslint:disable-next-line: max-line-length
    async findAll(): Promise<any> {
        const result = await this.chatModel.findAll();
        await Promise.all(
            await  result.map(
                async (chat: any) => {
                    chat.dataValues.chatAvatar = completeUrl('chat/avatar',chat.dataValues.chatAvatar);
                    const dataMessage = await this.messageModel.findAll({
                        where:{
                            chatId: chat.dataValues.id
                        }
                    });
                    chat.dataValues.messageModel = dataMessage;
                }
            )
        )
        return result;
    }

    async findById(id: string): Promise<ChatModel> {
        const result:any = await this.chatModel.findByPk<ChatModel>(id);
        result.dataValues.chatAvatar = completeUrl('chat/avatar',result.dataValues.chatAvatar);
        const dataMessage = await this.messageModel.findAll({
            where:{
                chatId: result.dataValues.id
            }
        });
        result.dataValues.messageModel = dataMessage;
        return result;
    }

    async create(chat: ChatSaveJson): Promise<ChatModel> {

        if(!chat.chatAvatar) chat.chatAvatar = 'default.jpeg';
        const result: any = await this.chatModel.create<ChatModel>(chat, {
            returning: true,
        });
        result.dataValues.chatAvatar = completeUrl('chat/avatar',result.dataValues.chatAvatar);
        const dataMessage = await this.messageModel.findAll({
            where:{
                chatId: result.dataValues.id
            }
        });
        result.dataValues.messageModel = dataMessage;
        return result;
    }

    async update(idChat: string, chatUpdate: Partial<ChatUpdateJson>){

        if (chatUpdate.chatAvatar) {
            await this.deleteImg(idChat);
        }

        const result: any =   await this.chatModel.update(chatUpdate, {
            where: {
                id: idChat,
            },
            returning: true,
        });

        const dataMessage = await this.messageModel.findOne({
            where:{
                chatId: result[1][0].dataValues.id
            }
        });
        result[1][0].dataValues.messageModel = dataMessage.dataValues;
        result[1][0].dataValues.chatAvatar = CONFIG.storage.server + result[1][0].dataValues.chatAvatar;
        return result;
    }

    async deleted(idChat: string): Promise<any> {

        await this.deleteImg(idChat);
        return await this.chatModel.destroy({
            where: {
                id: idChat,
            },
        });
    }

    private async deleteImg(id: string) {
        const img = await this.chatModel.findByPk(id).then(item => item.chatAvatar).catch(err => err);
        if(img === 'default.jpeg') return;
        const pathImagen = path.resolve(__dirname, `../../../../uploads/chat/avatar/${img}`);
        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }
}
