import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { PrebuiltUserController } from './controllers/prebuilt-users.controller';
import { PrebuiltUserService } from './services/prebuilt-users.service';
import { PrebuiltUserModel } from './models/prebuilt-users.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
const models = [
  PrebuiltUserModel,
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [PrebuiltUserController],
  providers: [PrebuiltUserService]
})
export class PrebuiltUsersModule {}
