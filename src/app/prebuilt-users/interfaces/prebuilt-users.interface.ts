import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum, IsOptional} from "class-validator";
export class PrebuiltUserSaveJson {

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    userName: string;

    @ApiModelProperty()
    @IsString()
    @IsOptional()
    avatar?: string;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    userLastName: string;

    @ApiModelProperty()
    @IsUUID('4')
    @IsOptional()
    rankingId?: string;
}
// tslint:disable-next-line: max-classes-per-file
export class PrebuiltUserUpdateJson  extends PrebuiltUserSaveJson{
    
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}
