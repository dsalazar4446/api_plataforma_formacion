// tslint:disable-next-line: max-line-length
import { Controller, Post, Body, Get, Put, Delete, Param,FileFieldsInterceptor, UseInterceptors, UseFilters, UploadedFiles, UseGuards, Req} from '@nestjs/common';
import { PrebuiltUserService } from '../services/prebuilt-users.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { PrebuiltUserSaveJson } from '../interfaces/prebuilt-users.interface';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiConsumes, ApiImplicitFile, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import * as file from './infoFile';
import { PrebuiltUserModel } from '../models/prebuilt-users.model';

@Controller('prebuilt-users')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class PrebuiltUserController {
    constructor( 
        private readonly prebuiltUserService: PrebuiltUserService,
        ) {}
        @Post()
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        @ApiResponse({
            status: 200,
            type: PrebuiltUserModel,
          })
        @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
        @ApiConsumes('multipart/form-data')
        @ApiImplicitFile({ name: 'avatar', description: 'Prebuild Avatar File', required: true })
        async create(@Body() prebuilUser: PrebuiltUserSaveJson) {
            return await this.prebuiltUserService.create(prebuilUser);
        }
        @Get()
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        @ApiResponse({
            status: 200,
            type: PrebuiltUserModel,
            isArray: true
          })
        async getAll(@Req() req) {
            return await this.prebuiltUserService.findAll(req.headers.language);
        }
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @ApiResponse({
            status: 200,
            type: PrebuiltUserModel,
          })
        @Get(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async findById(@Param('id') idPrebuiltUser, @Req() req) {
            return await this.prebuiltUserService.findById(idPrebuiltUser, req.headers.language);
        }

        @Put(':id')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        @ApiResponse({
            status: 200,
            type: PrebuiltUserModel,
          })
        @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
        @ApiConsumes('multipart/form-data')
        @ApiImplicitFile({ name: 'avatar', description: 'Prebuild Avatar File', required: true })
        async update(
                @Body()
                updatePrebuiltUser: Partial<PrebuiltUserSaveJson>, 
                @Param('id') idPrebuiltUser
            ) {
            return await this.prebuiltUserService.update(idPrebuiltUser, updatePrebuiltUser);
        }
        @Delete(':id')
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        async delete(@Param('id') idPrebuiltUser) {
            return await this.prebuiltUserService.deleted(idPrebuiltUser);
        }
}
