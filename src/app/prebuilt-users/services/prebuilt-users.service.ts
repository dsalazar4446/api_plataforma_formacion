import { Injectable, Inject } from '@nestjs/common';
import { PrebuiltUserSaveJson } from '../interfaces/prebuilt-users.interface';
import { PrebuiltUserModel } from '../models/prebuilt-users.model';
import { CONFIG } from '../../../config/index';
import { RankingModel } from '../../ranking/models/ranking.model';
import * as path from 'path';
import * as fs from 'fs';
import { completeUrl } from '../../shared/utils/completUrl';
import { RankingDetails } from '../../ranking/models/ranking-details.model';

@Injectable()
export class PrebuiltUserService {

    private readonly folder = 'rankingImgs';
    constructor(
        @Inject('PrebuiltUserModel') private readonly prebuiltUserModel: typeof PrebuiltUserModel,
    ) { }
    async findAll(languageType: string): Promise<PrebuiltUserModel[]> {
        const result = await this.prebuiltUserModel.findAll({
            include: [
                {
                    model: RankingModel,
                    include: [{
                        model: RankingDetails,
                        where: { languageType }
                    }]
                }
            ]
        });
        
        result.map(
            (prebuiltUser) => {
                console.log(prebuiltUser.dataValues);
                prebuiltUser.dataValues.avatar = completeUrl('prebuild/user', prebuiltUser.avatar);
                if(prebuiltUser.dataValues.ranking){
                    prebuiltUser.dataValues.ranking.rankingIcon = completeUrl(this.folder, prebuiltUser.ranking.rankingIcon);
                    prebuiltUser.dataValues.ranking.rankingImgBar = completeUrl(this.folder, prebuiltUser.ranking.rankingImgBar);
                }
            }
        )
        return result;
    }
    async findById(id: string, languageType: string): Promise<any> {
        const result = await this.prebuiltUserModel.findByPk(id, {
            include: [
                {
                    model: RankingModel,
                    include: [{
                        model: RankingDetails,
                        where: { languageType }
                    }]
                }
            ]
        });

        result.avatar = completeUrl('prebuild/user', result.avatar)
        if(result.ranking){
            result.dataValues.ranking.rankingIcon = completeUrl(this.folder, result.dataValues.ranking.rankingIcon);
            result.dataValues.ranking.rankingImgBar = completeUrl(this.folder, result.dataValues.ranking.rankingImgBar);
        }

        return result;
    }
    async create(prebuiltUser: PrebuiltUserSaveJson): Promise<PrebuiltUserModel> {

        if (!prebuiltUser.avatar || prebuiltUser.avatar === 'null' || prebuiltUser.avatar === null) prebuiltUser.avatar = 'default.jpeg';

        const result = await this.prebuiltUserModel.create<PrebuiltUserModel>(prebuiltUser, {
            returning: true,
        });

        result.avatar = completeUrl('prebuild/user', result.avatar)

        return result;

    }
    async update(idPrebuiltUser: string, prebuiltUserUpdate: Partial<PrebuiltUserSaveJson>) {

        if (prebuiltUserUpdate.avatar) {
            await this.deleteImg(idPrebuiltUser);
        }

        const result: any = await this.prebuiltUserModel.update(prebuiltUserUpdate, {
            where: {
                id: idPrebuiltUser,
            },
            returning: true,
        });
        result[1][0].avatar = completeUrl('prebuild/user', result[1][0].avatar)

        return result;
    }
    async deleted(idPrebuiltUser: string): Promise<any> {

        await this.deleteImg(idPrebuiltUser);

        return await this.prebuiltUserModel.destroy({
            where: {
                id: idPrebuiltUser,
            },
        });
    }

    private async deleteImg(idPrebuiltUser: string) {
        const img = await this.prebuiltUserModel.findByPk(idPrebuiltUser).then(item => item.avatar).catch(err => err);
        if (img === 'default.jpeg') return;
        const pathImagen = path.resolve(__dirname, `../../../../uploads/prebuild/user/${img}`);
        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }


}
