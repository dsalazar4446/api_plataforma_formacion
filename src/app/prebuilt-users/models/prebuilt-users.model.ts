import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, BelongsTo, ForeignKey, HasMany } from 'sequelize-typescript';
import { RankingModel } from '../../ranking/models/ranking.model';
import { BroadcastPrebuiltMessageModel } from '../../broadcast-prebuilt-message/models/broadcast-prebuilt-message.model';
import { ApiModelProperty } from '@nestjs/swagger';
import { string } from 'prop-types';
@Table({
    tableName: 'prebuilt_users',
})
export class PrebuiltUserModel extends Model<PrebuiltUserModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        validate: {
            notEmpty: true,
        },
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(36),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_name',
    })
    userName: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_lastname',
    })
    userLastName: string;
    @ApiModelProperty()
    @ForeignKey(() => RankingModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: true,
        validate: {
            notEmpty: true,
        },
        field: 'rankings_id',
    })
    rankingId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'avatar',
    })
    avatar: string;
    @ApiModelProperty({
        type:RankingModel,
        isArray: false,
        example: {
            "id": "string",
            "rankingTiming": 0,
            "billingGoal": 0,
            "activeMemberBonusValue": 0,
            "eventBonusValue": 0,
            "corporateBonusValue": 0,
            "rankingColor1": "string",
            "rankingColor2": "string",
            "rankingIcon": "string",
            "rankingImgBar": "string"
          }
    })
    @BelongsTo(() => RankingModel)
    ranking: RankingModel;

    // @HasMany(() => BroadcastPrebuiltMessageModel)
    // broadcastPrebuiltMessageModel: BroadcastPrebuiltMessageModel[];
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
