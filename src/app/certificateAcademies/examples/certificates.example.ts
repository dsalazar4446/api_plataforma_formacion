export const CertificatesExample = `
{
    id: string,
    certificateCode: string,
    status: boolean
}
`

export const CertificatesArrayExample = `
[
    ${CertificatesExample}
]
`