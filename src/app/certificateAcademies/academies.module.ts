import { Module, HttpModule, Provider } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { AcademiesController } from './controllers/academies/academies.controller';
import { AcademiesDetailsController } from './controllers/academies/academies-details.controller';
import { AcademiesHasCertificatesController } from './controllers/academies/academies-has-certificates.controller';
import { CertificatesController} from './controllers/certificates/certificates.controller';
import { CertificatesDetailsController } from './controllers/certificates/certificates-details.controller';
import { CertificatesPricingsController } from './controllers/certificates/certificates-pricings.controller';
import { ProgramHasCertificatesController } from './controllers/certificates/program-has-certificates.controller';


import { Academies } from './models/academies.models';
import { AcademiesDetails } from './models/academies-details.models';
import { AcademiesHasCertificates } from './models/academies-has-certificates.models';
import { CertificateDetails } from './models/certificates-details.models';
import { CertificatePricings } from './models/certificates-pricings.models';
import { Certificates } from './models/certificates.models';
import { ProgramsHasCertificates } from './models/program-has-certificates.model';


import { AcademiesService} from './services/academies/academies.service';
import { AcademiesDetailsServices } from './services/academies/academies-details.service';
import { AcademiesHasCertificatesServices } from './services/academies/academies-has-certificates.service';
import { CertificatesDetailsServices} from './services/certificates/certificates-details.sevices';
import { CertificatesPricingsServices } from './services/certificates/certificates-pricings.services';
import { CertificatesServices } from './services/certificates/certificates.services';
import { ProgramHasCertificatesServices } from './services/certificates/program-has-certificates.services';


const models = [Academies, AcademiesDetails, AcademiesHasCertificates, CertificateDetails, CertificatePricings, Certificates, ProgramsHasCertificates];

const providers: Provider[] = [AcademiesService, AcademiesDetailsServices, AcademiesHasCertificatesServices, CertificatesDetailsServices, CertificatesPricingsServices, CertificatesServices, ProgramHasCertificatesServices];

const controllers = [AcademiesController, AcademiesDetailsController, AcademiesHasCertificatesController, CertificatesController, CertificatesDetailsController, CertificatesPricingsController, ProgramHasCertificatesController ];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers,
  exports: [
    CertificatesServices
  ]
})
export class CertificatesAcademiesModule { }
