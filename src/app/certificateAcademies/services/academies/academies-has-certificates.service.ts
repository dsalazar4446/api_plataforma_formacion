import { Injectable, Inject } from '@nestjs/common';
import { AcademiesHasCertificatesUpdateJson, AcademiesHasCertificatesSaveJson } from '../../interfaces/academies-has-certificates.interfaces';
import { AcademiesHasCertificates } from '../../models/academies-has-certificates.models';
import { ValidateUUID } from '../../../shared/utils/validateUUID';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services AcademiesHasCertificates
 * @Creado 26 de Marzo 2019
 */

@Injectable()
export class AcademiesHasCertificatesServices {

    constructor(
        @Inject('AcademiesHasCertificates') private readonly _academiesHasCertificatesServcs: typeof AcademiesHasCertificates,
        private readonly _validateUUID: ValidateUUID
    ) { }

    async saveAcademiesHasCertificates(body: AcademiesHasCertificatesSaveJson): Promise<AcademiesHasCertificatesUpdateJson> {

        return await new this._academiesHasCertificatesServcs(body).save();
    }

    async getAll(): Promise<AcademiesHasCertificatesUpdateJson[]> {

        return await this._academiesHasCertificatesServcs.findAll();
    }

    async destroyAcademiesHasCertificates(academiesHasCertificatesId: string): Promise<number> {

        this._validateUUID.validateUUIID(academiesHasCertificatesId);
        return await this._academiesHasCertificatesServcs.destroy({
            where: { id: academiesHasCertificatesId },
        });
    }
}