import { Injectable, Inject } from '@nestjs/common';
import { Academies } from '../../models/academies.models';
import { AcademiesDetails } from '../../models/academies-details.models';
import { Certificates } from '../../models/certificates.models';
import { AcademiesSaveJson, AcademiesUpdateJson } from '../../interfaces/academies.interfaces';
import * as path from 'path';
import * as fs from 'fs';
import { ValidateUUID } from '../../../shared/utils/validateUUID';
import { Sequelize } from 'sequelize-typescript';
const Op = Sequelize.Op;

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service AcademiesService
 * @Creado 27 de Marzo 2019
 */

@Injectable()
export class AcademiesService {

    private include = {
        include: [
            {
                model: AcademiesDetails
            },
            {
                model: Certificates
            }
        ]
    }

    private language = ['es', 'en', 'it', 'pr'];
    private readonly default = 'default.jpeg';
    private readonly folder = 'academiesAvatar';

    constructor(
        @Inject('Academies') private readonly _academiesServcs: typeof Academies,
        private readonly _validateUUID: ValidateUUID
    ) { }

    async saveAcademies(bodyAcademies: AcademiesSaveJson): Promise<AcademiesUpdateJson> {

        if (!bodyAcademies.avatar) bodyAcademies.avatar = this.default;
        if (!bodyAcademies.institutionPicture) bodyAcademies.institutionPicture = this.default;

        return await new this._academiesServcs(bodyAcademies).save();
    }

    async showAllAcademies(languageType: string): Promise<AcademiesUpdateJson[]> {

        if (this.language.indexOf(languageType) >= 0) {
            return await this._academiesServcs.findAll({
                where: {
                    status: true
                },
                include: [
                    {
                        model: AcademiesDetails,
                        where: {
                            languageType
                        },

                    },
                    {
                        model: Certificates
                    }
                ]
            });
        }
        return;
    }

    async getAcademiesDetails(academiesId: string) {

        this._validateUUID.validateUUIID(academiesId);
        return await this._academiesServcs.findByPk(academiesId, this.include);
    }

    async updateAcademies(id: string, bodyAcademies: Partial<AcademiesUpdateJson>): Promise<any> {

        this._validateUUID.validateUUIID(id);
        if (bodyAcademies.avatar) {
            let img = await this._academiesServcs.findByPk(id).then(item => item.avatar).catch(err => err);
            this.deletePath(img);
        }
        if (bodyAcademies.institutionPicture) {
            let img = await this._academiesServcs.findByPk(id).then(item => item.institutionPicture).catch(err => err);
            this.deletePath(img);
        }

        return await this._academiesServcs.update(bodyAcademies, {
            where: { id },
            returning: true
        });

    }

    async destroyAcademies(academiesId: string): Promise<number> {

        this._validateUUID.validateUUIID(academiesId);
        await this.deleteImg(academiesId);

        return await this._academiesServcs.destroy({
            where: { id: academiesId },

        });
    }

    async find() {

        return await this._academiesServcs.findAll(this.include);

    }

    private async deleteImg(id: string) {
        let img = await this._academiesServcs.findByPk(id).then(item => item.avatar).catch(err => err);
        if (img === this.default) return;
        this.deletePath(img);
        img = await this._academiesServcs.findByPk(id).then(item => item.institutionPicture).catch(err => err);
        if (img === this.default) return;
        this.deletePath(img);

    }

    private async deletePath(img: string) {
        let pathImagen = path.resolve(__dirname, `../../../../../uploads/${this.folder}/${img}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }

}
