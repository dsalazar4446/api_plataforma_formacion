import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { AcademiesDetails } from '../../models/academies-details.models';
import { AcademiesDetailsSaveJson, AcademiesDetailsUpdateJson } from '../../interfaces/academies-details.interfaces';
import { ValidateUUID } from '../../../shared/utils/validateUUID';
import { Academies } from '../../models/academies.models';
import { AppUtilsService } from '../../../shared/services/app-utils.service';
/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services AcademiesDetailsServices
 * @Creado 27 de Marzo 2019
 */

@Injectable()
export class AcademiesDetailsServices {

    constructor(
        @Inject('AcademiesDetails') private readonly _academiesDetailsServcs: typeof AcademiesDetails,
        @Inject('Academies') private readonly _academies: typeof Academies,
        private readonly _validateUUID: ValidateUUID,
        private readonly appUtilsService: AppUtilsService
    ) { }

    async saveAcademiesDetails(bodyAcademiesDetails: AcademiesDetailsSaveJson): Promise<AcademiesDetailsUpdateJson> {

        const academy = await this._academies.findByPk(bodyAcademiesDetails.academyId);

        if(!academy){
            throw this.appUtilsService.httpCommonError('Academias Id no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM140',
                languageType: 'es',
            });
        }

        return await new this._academiesDetailsServcs(bodyAcademiesDetails).save();
    }

    async showAll(): Promise<AcademiesDetailsUpdateJson[]> {

        return await this._academiesDetailsServcs.findAll();

    }

    async getDetails(academiesDetailsId: string) {

        this._validateUUID.validateUUIID(academiesDetailsId);
        return await this._academiesDetailsServcs.findByPk(academiesDetailsId);
    }

    async updateAcademiesDetails(id: string, bodyAcademiesDetails: Partial<AcademiesDetailsUpdateJson>): Promise<[number, Array<any>]> {
        this._validateUUID.validateUUIID(id);
        return await this._academiesDetailsServcs.update(bodyAcademiesDetails, {
            where: { id },
            returning: true
        });

    }

    async destroyAcademiesDetails(academiesDetailsId: string): Promise<number> {

        this._validateUUID.validateUUIID(academiesDetailsId);
        return await this._academiesDetailsServcs.destroy({
            where: { id: academiesDetailsId },
        });
    }
}