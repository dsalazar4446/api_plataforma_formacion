import { Injectable, Inject } from '@nestjs/common';
import * as ShortUniqueId from 'short-unique-id';
const uid = new ShortUniqueId();
import { Certificates} from '../../models/certificates.models';
import { CertificatePricings } from '../../models/certificates-pricings.models';
import { CertificateDetails } from '../../models/certificates-details.models';
import { Academies } from '../../models/academies.models';
import { CertificatesSaveJson, CertificatesUpdateJson } from '../../interfaces/certificates.interfaces';
import { ProgramsModel } from '../../../programs/models/programs.model';
import { ValidateUUID } from '../../../shared/utils/validateUUID';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service Certificates
 * @Creado 27 de Marzo 2019
 */

@Injectable()
export class CertificatesServices {

    private language = ['es', 'en', 'it', 'pr'];
    private include = [
            {
                model: CertificateDetails
            },
            {
                model: CertificatePricings
            },
            {
                model: Academies
            },
            {
                model: ProgramsModel
            }
        ]
    constructor(
        @Inject('Certificates') private readonly _certificatesServcs: typeof Certificates,
        private readonly _validateUUID: ValidateUUID
    ) { }

    async saveCertificates(bodyCertificates: CertificatesSaveJson): Promise<CertificatesUpdateJson> {

        bodyCertificates.certificateCode =  uid.randomUUID(9);

        return await new this._certificatesServcs(bodyCertificates).save();
    }

    async getDetailsCertificates(certificatesId: string) {

        this._validateUUID.validateUUIID(certificatesId);
        return await this._certificatesServcs.findByPk(certificatesId, {include: this.include});
    }

    async showAllCertificates(status: boolean): Promise<CertificatesUpdateJson[]> {

        return await this._certificatesServcs.findAll({
            where:{
                status
            },
            include: this.include
        });
    }

    async showAllCertificatesByLanguage(languageType: string): Promise<CertificatesUpdateJson[]> {

        if (this.language.indexOf(languageType) >= 0) {
            return await this._certificatesServcs.findAll({
                include: [
                    {
                        model: CertificateDetails,
                        where: {
                            languageType
                        },

                    },
                    {
                        model: CertificatePricings
                    },
                    {
                        model: Academies
                    },
                    {
                        model: ProgramsModel
                    }
                ]
            });
        }
        return;
    }


    async updateCertificates(id: string, bodyCertificates: Partial<CertificatesUpdateJson>): Promise<[number, Array<any>]> {

        this._validateUUID.validateUUIID(id);
        return await this._certificatesServcs.update(bodyCertificates, {
            where: { id },
            returning: true
        });

    }

    async destroyCertificates(certificatesId: string): Promise<number> {

        this._validateUUID.validateUUIID(certificatesId);
        return await this._certificatesServcs.destroy({
            where: { id: certificatesId },
        });
    }

    async findAll(){
        return await this._certificatesServcs.findAll({include: this.include});
    }
}