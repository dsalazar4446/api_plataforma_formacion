import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { CertificatePricings } from '../../models/certificates-pricings.models';
import { CertificatePricingsSaveJson, CertificatePricingsUpdateJson } from '../../interfaces/certificates-pricings.interfaces';
import { TransactionItemsModel } from '../../../transactions/models/transaction-items.model';
import { ValidateUUID } from '../../../shared/utils/validateUUID';
import { Certificates } from '../../models/certificates.models';
import { AppUtilsService } from '../../../shared/services/app-utils.service';
/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services CertificatesPricingsServices
 * @Creado 27 de Marzo 2019
 */

@Injectable()
export class CertificatesPricingsServices {

    constructor(
        @Inject('CertificatePricings') private readonly _certificatePricingsServcs: typeof CertificatePricings,
        private readonly _validateUUID: ValidateUUID,
        @Inject('Certificates') private readonly _certificates: typeof Certificates,
        private readonly appUtilsService: AppUtilsService,
    ) { }

    async saveCertificatesP(bodyCertificatesP: CertificatePricingsSaveJson): Promise<CertificatePricingsUpdateJson> {

        const certificate = await this._certificates.findByPk(bodyCertificatesP.certificatesId);

        if(!certificate){
            throw this.appUtilsService.httpCommonError('Certificates Id no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM141',
                languageType: 'es',
            });
        }

        return await new this._certificatePricingsServcs(bodyCertificatesP).save();
    }

    async showAll(): Promise<CertificatePricingsUpdateJson[]> {

        return await this._certificatePricingsServcs.findAll();

    }

    async getCertificatesPricings(certiPricingsId: string) {

        this._validateUUID.validateUUIID(certiPricingsId);

        return await this._certificatePricingsServcs.findByPk(certiPricingsId, {
            include: [
                {
                    model: TransactionItemsModel
                }
            ]
        });
    }

    async updateCertificatesPricings(id: string, bodyCertificatesP: Partial<CertificatePricingsUpdateJson>): Promise<[number, Array<any>]> {

        this._validateUUID.validateUUIID(id);
        return await this._certificatePricingsServcs.update(bodyCertificatesP, {
            where: { id },
            returning: true
        });

    }

    async destroyCertificatesPricings(certiPricingsId: string): Promise<number> {
        this._validateUUID.validateUUIID(certiPricingsId);
        return await this._certificatePricingsServcs.destroy({
            where: { id: certiPricingsId },
        });
    }
}