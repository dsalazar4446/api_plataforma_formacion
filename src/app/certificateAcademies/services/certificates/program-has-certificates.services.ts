import { Injectable, Inject } from '@nestjs/common';
import { ProgramsHasCertificates } from '../../models/program-has-certificates.model';
import { ProgramHasCertificatesSaveJson } from '../../interfaces/program-has-certificates.interface';
import { ValidateUUID } from '../../../shared/utils/validateUUID';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services ProgramHasCertificatesServices
 * @Creado 26 de Marzo 2019 
 */

@Injectable()
export class ProgramHasCertificatesServices {

    constructor(
        @Inject('ProgramsHasCertificates') private readonly _programHasCertificatesServcs: typeof ProgramsHasCertificates,
        private readonly _validateUUID: ValidateUUID
    ) { }

    async saveProgramHasCertificates(body: ProgramHasCertificatesSaveJson): Promise<ProgramHasCertificatesSaveJson> {

        return await new this._programHasCertificatesServcs(body).save();
    }

    async getAll(): Promise<ProgramHasCertificatesSaveJson[]> {

        return await this._programHasCertificatesServcs.findAll();
    }

    async destroyProgramHasCertificates(id: string): Promise<number> {

        this._validateUUID.validateUUIID(id);
        return await this._programHasCertificatesServcs.destroy({
            where: { id },
        });
    }
}