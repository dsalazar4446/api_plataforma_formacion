import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { CertificatesDetailsSaveJson, CertificatesDetailsUpdateJson } from '../../interfaces/certificates-details.interfaces';
import { CertificateDetails } from '../../models/certificates-details.models';
import * as path from 'path';
import * as fs from 'fs';
import { ValidateUUID } from '../../../shared/utils/validateUUID';
import { AppUtilsService } from '../../../shared/services/app-utils.service';
import { Certificates } from '../../models/certificates.models';
/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services CertificatesDetailsServices
 * @Creado 27 de Marzo 2019
 */

@Injectable()
export class CertificatesDetailsServices {

    constructor(
        @Inject('CertificateDetails') private readonly _certificatesDetailsServcs: typeof CertificateDetails,
        @Inject('Certificates') private readonly _certificates: typeof Certificates,
        private readonly appUtilsService: AppUtilsService,
        private readonly _validateUUID: ValidateUUID,
    ) { }

    async saveCertificatesDetails(bodyCertificatesDetails: CertificatesDetailsSaveJson): Promise<CertificatesDetailsUpdateJson> {

        const certificate = await this._certificates.findByPk(bodyCertificatesDetails.certificatesId);

        if(!certificate){
            throw this.appUtilsService.httpCommonError('Certificates Id no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM141',
                languageType: 'es',
            });
        }

        if(!bodyCertificatesDetails.certificateImg) bodyCertificatesDetails.certificateImg = 'default.jpeg';

        return await new this._certificatesDetailsServcs(bodyCertificatesDetails).save();
    }

    async showAll(): Promise<CertificatesDetailsUpdateJson[]> {

        return await this._certificatesDetailsServcs.findAll();

    }

    async getDetails(CertificatesDetailsId: string): Promise<any> {

        this._validateUUID.validateUUIID(CertificatesDetailsId);
        return await this._certificatesDetailsServcs.findByPk(CertificatesDetailsId);
    }

    async updateCertificatesDetails(id: string, bodyCertificatesDetails: Partial<CertificatesDetailsUpdateJson>): Promise<[number, Array<any>]> {

        this._validateUUID.validateUUIID(id);
        if( bodyCertificatesDetails.certificateImg ){
            await this.deleteImg(id);
        }

        return await this._certificatesDetailsServcs.update(bodyCertificatesDetails, {
            where: { id },
            returning: true
        });

    }

    async destroyCertificatesDetails(CertificatesDetailsId: string): Promise<number> {
        this._validateUUID.validateUUIID(CertificatesDetailsId);
        await this.deleteImg(CertificatesDetailsId);
        return await this._certificatesDetailsServcs.destroy({
            where: { id: CertificatesDetailsId },
        });
    }

    private async deleteImg(id: string) {
        const img = await this._certificatesDetailsServcs.findByPk(id).then(item => item.certificateImg).catch(err => err);
        if(img === 'default.jpeg') return;
        const pathImagen = path.resolve(__dirname, `../../../../../uploads/certificateImg/${img}`);
        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }
}