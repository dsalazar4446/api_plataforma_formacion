import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany, ForeignKey, BelongsTo, BelongsToMany } from 'sequelize-typescript';
import { Certificates } from './certificates.models';
import { TransactionItemsModel } from '../../transactions/models/transaction-items.model';
import { TransacItemsHCertiPricings } from '../../transactions/models/transac-items-h-certi-pricings.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';


@Table({
    tableName: 'certificate_pricings',
})
export class CertificatePricings extends Model<CertificatePricings>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => Certificates)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'certificate_id',
    })
    certificatesId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        defaultValue : 0,
        validate: {
            notEmpty: true,
        },
        field: 'certificate_pricing',
    })
    certificatePricing: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        defaultValue : true,
        validate: {
            notEmpty: true,
        },
        field: 'certificate_pricing_status',
    })
    certificatePricingStatus: boolean;
 
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        defaultValue: true,
        validate: {
            notEmpty: true,
        },
        field: 'certificate_odoo_id',
    })
    certificateOdooId: number;

    @BelongsTo(() => Certificates)
    certificates: Certificates;
    
    @ApiModelPropertyOptional({
        type: TransactionItemsModel,
        isArray: false,
        example: [
            {
                "id":"UUID",
                "billingTransactionsId":"UUID",
                "userId":"UUID",
                "transactionItemType":"ENUM",
                "transactionItemCant":"number",
                "transactionItemAmmount":"number",
                "transactionItemTaxes":"number",
                "transactionItemTotal":"number"
            }
        ]
    })
    @BelongsToMany(() => TransactionItemsModel, () => TransacItemsHCertiPricings)
    transactionItemsModel: TransactionItemsModel[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}