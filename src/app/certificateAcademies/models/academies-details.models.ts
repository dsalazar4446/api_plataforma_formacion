import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Academies } from './academies.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';


@Table({
    tableName: 'academiy_details',
})
export class AcademiesDetails extends Model<AcademiesDetails>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => Academies)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'academy_id',
    })
    academyId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'academy_detail',
    })
    academyDetail: string;

    /**
     * RELACIONES
     * AcademiesDetails pertenece a Academies
     */    

    @BelongsTo(() => Academies)
    academies: Academies;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;



}