import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany, BelongsToMany } from 'sequelize-typescript';
import { AcademiesDetails } from './academies-details.models';
import { AcademiesHasCertificates } from './academies-has-certificates.models';
import { Certificates } from './certificates.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'academies',
})
export class Academies extends Model<Academies>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'academy_name',
    })
    academyName: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'avatar',
    })
    avatar: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'website',
    })
    website: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'email',
    })
    email: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        defaultValue: true,
        field: 'status',
    })
    status: boolean;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'institution_picture',
    })
    institutionPicture: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.CHAR(128),
        allowNull: true,
        field: 'latitude',
    })
    latitude: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.CHAR(128),
        allowNull: true,
        field: 'longitude',
    })
    longitude: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'address_1',
    })
    address1: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: true,
        field: 'address_2',
    })
    address2: string;

    /**
     * RELACIONES
     * Academies tiene muchos AcademiesDetails
     * Academies tiene muchos Certificates
     */    
    @ApiModelPropertyOptional({
        type: () => AcademiesDetails,
        isArray: true
    })
    @HasMany(() => AcademiesDetails)
    academiesDetails: AcademiesDetails[];
    
    @ApiModelPropertyOptional({
        type: Certificates,
        isArray: false,
        example: [
            {
                "Certificates":"CertificatesModel"
            }
        ]
    })
    @BelongsToMany(() => Certificates, () => AcademiesHasCertificates)
    certificates: Certificates[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}