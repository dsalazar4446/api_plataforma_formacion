import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany, ForeignKey, BelongsToMany } from 'sequelize-typescript';
import { AcademiesHasCertificates } from './academies-has-certificates.models';
import { ProgramsModel } from './../../programs/models/programs.model';
import { CertificateDetails } from './certificates-details.models';
import { CertificatePricings } from './certificates-pricings.models';
import { Academies } from './academies.models';
import { ProgramsHasCertificates } from './program-has-certificates.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

 
@Table({
    tableName: 'certificates',
})
export class Certificates extends Model<Certificates>{
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(9),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'certificate_code',
    })
    certificateCode: string;
    @ApiModelPropertyOptional()
    @Column({
    type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'status',
    })
    status: boolean;

     /**
     * RELACIONES
     * Certificates tiene muchos AcademiesHasCertificates
     * Certificates tiene muchos CertificateDetails
     * Certificates tiene muchos CertificatePricings
     * Certificates tiene muchos Academies
     */
    
    @ApiModelPropertyOptional({
        type: CertificateDetails,
        isArray: false,
        example: [
            {
                "id":"UUID",
                "certificatesId":"UUID",
                "certificateDetail":"string",
                "certificateImg":"string"
            }
        ]
    })
    @HasMany(() => CertificateDetails)
    certificateDetails: CertificateDetails[];

    @ApiModelPropertyOptional({
        type: CertificatePricings,
        isArray: false,
        example: [
            {
                "id":"UUID",
                "certificatesId":"UUID",
                "certificatePricing":"number",
                "certificatePricingStatus":"boolean"
            }
        ]
    })
    @HasMany(() => CertificatePricings)
    certificatePricings: CertificatePricings[];
    
    @ApiModelPropertyOptional({
        type: Academies,
        isArray: false,
        example: [
            {
                "Academies":"AcademiesModel"
            }
        ]
    })
    @BelongsToMany(() => Academies, () => AcademiesHasCertificates)
    academies: Academies[];
    
    @ApiModelPropertyOptional({
        type: ProgramsModel,
        isArray: false,
        example: [
            {
                "id":"UUID",
                "categoriesId":"UUID",
                "programCode":"string",
                "programStatus":"ENUM",
                "programBackground":"string",
                "programIcon":"string",
                "fbPromotionImg":"string"
            }
        ]
    })
    @BelongsToMany(() => ProgramsModel, {
        through: {
          model: () => ProgramsHasCertificates,
          unique: true,
        }
      })
    programsModel: ProgramsModel[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}

