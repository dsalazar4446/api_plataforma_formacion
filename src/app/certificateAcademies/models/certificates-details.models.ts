import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Certificates } from './certificates.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';


@Table({
    tableName: 'certificate_details',
})
export class CertificateDetails extends Model<CertificateDetails>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => Certificates)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'certificates_id',
    })
    certificatesId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'certificate_detail',
    })
    certificateDetail: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'certificate_img',
    })
    certificateImg: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;

    /**
     * RELACIONES
     * CertificateDetails pertenece a Certificates
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => Certificates)
    certificates: Certificates;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}