import { Table, Model, DataType, Column, ForeignKey } from 'sequelize-typescript';
import { Certificates } from './certificates.models';
import { ProgramsModel } from '../../programs/models/programs.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'programs_has_certificates',
})
export class ProgramsHasCertificates extends Model<ProgramsHasCertificates>{

    @ApiModelPropertyOptional()
    @ForeignKey(() => ProgramsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'programs_id',
    })
    programsId: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => Certificates)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'certificates_id',
    })
    certificatesId: string;
}