import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Academies } from './academies.models';
import { Certificates } from './certificates.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';


@Table({
    tableName: 'academies_has_certificates',
})
export class AcademiesHasCertificates extends Model<AcademiesHasCertificates>{

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => Academies)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'academy_id',
    })
    academyId: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => Certificates)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'certificate_id',
    })
    certificateId: string;
    
    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}