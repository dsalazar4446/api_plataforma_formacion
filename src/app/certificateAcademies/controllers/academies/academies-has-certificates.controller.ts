import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Delete, NotFoundException, HttpStatus, Param, Get } from "@nestjs/common";
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { AcademiesHasCertificatesServices } from "../../services/academies/academies-has-certificates.service";
import { AcademiesHasCertificatesSaveJson } from "../../interfaces/academies-has-certificates.interfaces";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from "@nestjs/swagger";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller AcademiesHasCertificatesController
 * @Creado 27 de Marzo 2019
 */

@ApiUseTags('Module-Academies')
@Controller('academies-has-certificates')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class AcademiesHasCertificatesController {

    constructor(private readonly _academiesHasCertificatesServices: AcademiesHasCertificatesServices,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyAcaHasCertif: AcademiesHasCertificatesSaveJson) {

        const createAcaHasCertif = await this._academiesHasCertificatesServices.saveAcademiesHasCertificates(bodyAcaHasCertif);

        if (!createAcaHasCertif) {
            throw this.appUtilsService.httpCommonError('Academias no creadas!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM15',
                languageType: 'es',
            });
        }

        return createAcaHasCertif;

    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async getAll() {
        const data = await this._academiesHasCertificatesServices.getAll();

        if (!data) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return data;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteAcaHasCertif(@Param('id') idAcaHasCertif: string) {

        const deleteAcaHasCertif = await this._academiesHasCertificatesServices.destroyAcademiesHasCertificates(idAcaHasCertif);

        if (!deleteAcaHasCertif) {
            throw this.appUtilsService.httpCommonError('Academias no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM15',
                languageType: 'es',
            });
        }

        return deleteAcaHasCertif;
    }
}

