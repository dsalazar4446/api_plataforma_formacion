import { Controller, UseFilters, UseInterceptors, Post, FileFieldsInterceptor, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param, Req } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { AcademiesService } from '../../services/academies/academies.service';
import * as file from './infoFile';
import { CONFIG } from '../../../../config';
import { AcademiesSaveJson } from '../../interfaces/academies.interfaces';
import { ApiUseTags, ApiConsumes, ApiImplicitParam, ApiImplicitFile, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { Academies } from '../../models/academies.models';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller AcademiesController
 * @Creado 27 de Marzo 2019
 */

@ApiUseTags('Module-Academies')
@Controller('academies')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class AcademiesController {

    constructor(private readonly _academiesService: AcademiesService,
        private readonly appUtilsService: AppUtilsService) { }

    @ApiResponse({
        status: 200,
        type: Academies,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    @ApiConsumes('multipart/form-data')
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiImplicitFile({ name: 'avatar', description: 'Academies Avatar File', required: true })
    async create(@Body() bodyAcademies: AcademiesSaveJson) {

        const createAcademies = await this._academiesService.saveAcademies(bodyAcademies);

        if (!createAcademies) {
            throw this.appUtilsService.httpCommonError('Academias no creadas!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM15',
                languageType: 'es',
            });
        }
        createAcademies.avatar = `${CONFIG.storage.server}academiesAvatar/${createAcademies.avatar}`;
        createAcademies.institutionPicture = `${CONFIG.storage.server}academiesAvatar/${createAcademies.institutionPicture}`;


        return createAcademies;
    }


    @ApiResponse({
        status: 200,
        type: Academies,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idAcademies: string) {

        const fetchAcademies = await this._academiesService.getAcademiesDetails(idAcademies);

        if (!fetchAcademies) {
            throw this.appUtilsService.httpCommonError('Academias no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM15',
                languageType: 'es',
            });
        }
        fetchAcademies.avatar = `${CONFIG.storage.server}academiesAvatar/${fetchAcademies.avatar}`;
        fetchAcademies.institutionPicture = `${CONFIG.storage.server}academiesAvatar/${fetchAcademies.institutionPicture}`;


        return fetchAcademies;
    }


    @ApiResponse({
        status: 200,
        type: Academies,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async find() {

        const fetchAll = await this._academiesService.find();

        if (!fetchAll[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        fetchAll.forEach(fetch => {
            fetch.avatar = `${CONFIG.storage.server}academiesAvatar/${fetch.avatar}`;
            fetch.institutionPicture = `${CONFIG.storage.server}academiesAvatar/${fetch.institutionPicture}`;
        });

        return fetchAll;
    }


    @ApiResponse({
        status: 200,
        type: Academies,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showLanguage(@Req() req) {
        const fetch = await this._academiesService.showAllAcademies(req.headers.language);
        if (!fetch[0]) {
            throw this.appUtilsService.httpCommonError('El lenguaje no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM3',
                languageType: 'es',
            });
        }
        fetch.forEach(fetch => {
            fetch.avatar = `${CONFIG.storage.server}academiesAvatar/${fetch.avatar}`;
            fetch.institutionPicture = `${CONFIG.storage.server}academiesAvatar/${fetch.institutionPicture}`;
        });

        return fetch;
    }

    @ApiResponse({
        status: 200,
        type: Academies,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'academiesAvatar', description: 'Academies Avatar File', required: false })
    async update(@Param('id') id: string, @Body() bodyAcademies: Partial<AcademiesSaveJson>) {

        const updateAcademies = await this._academiesService.updateAcademies(id, bodyAcademies);

        if (!updateAcademies[1][0]) {
            throw this.appUtilsService.httpCommonError('Academias no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM15',
                languageType: 'es',
            });
        }

        updateAcademies[1][0].avatar = `${CONFIG.storage.server}academiesAvatar/${updateAcademies[1][0].avatar}`;
        updateAcademies[1][0].institutionPicture = `${CONFIG.storage.server}academiesAvatar/${updateAcademies[1][0].institutionPicture}`;


        return updateAcademies;
    }


    @ApiResponse({
        status: 200,
        type: Academies,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteNews(@Param('id') idAcademies: string) {

        const deleteNews = await this._academiesService.destroyAcademies(idAcademies);

        if (!deleteNews) {
            throw this.appUtilsService.httpCommonError('Academias no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM15',
                languageType: 'es',
            });
        }

        return deleteNews;
    }


}
