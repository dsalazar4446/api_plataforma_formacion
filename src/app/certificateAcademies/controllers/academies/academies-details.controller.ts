import { Controller, UseFilters, UseInterceptors, Post, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param, Req } from "@nestjs/common";
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { AcademiesDetailsServices } from "../../services/academies/academies-details.service";
import { AcademiesDetailsSaveJson } from "../../interfaces/academies-details.interfaces";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { AcademiesDetails } from "../../models/academies-details.models";
/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller AcademiesDetails
 * @Creado 27 de Marzo 2019
 */

@ApiUseTags('Module-Academies')
@Controller('academies-details')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class AcademiesDetailsController {

    constructor(private readonly _academiesDetailsServices: AcademiesDetailsServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: AcademiesDetails,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Req() req, @Body() bodyAcademiesDetails: AcademiesDetailsSaveJson) {

        bodyAcademiesDetails.languageType = req.headers.language;

        const createAcademiesDetails = await this._academiesDetailsServices.saveAcademiesDetails(bodyAcademiesDetails);

        if (!createAcademiesDetails) {
            throw this.appUtilsService.httpCommonError('Detalles de academias no creados!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM13',
                languageType: 'es',
            });
        }

        return createAcademiesDetails;

    }


    @ApiResponse({
        status: 200,
        type: AcademiesDetails,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAll() {
        return await this._academiesDetailsServices.showAll();
    }


    @ApiResponse({
        status: 200,
        type: AcademiesDetails,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idAcademiesDetails: string) {

        const fetchAcademiesDetails = await this._academiesDetailsServices.getDetails(idAcademiesDetails);

        if (!fetchAcademiesDetails) {
            throw this.appUtilsService.httpCommonError('Detalles de academias no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM14',
                languageType: 'es',
            });
        }

        return fetchAcademiesDetails;
    }


    @ApiResponse({
        status: 200,
        type: AcademiesDetails,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateNews(@Req() req, @Param('id') id: string, @Body() bodyAcademiesDetails: Partial<AcademiesDetailsSaveJson>) {

        bodyAcademiesDetails.languageType = req.headers.language;

        const updateAcademiesDetails = await this._academiesDetailsServices.updateAcademiesDetails(id, bodyAcademiesDetails);

        if (!updateAcademiesDetails) {
            throw this.appUtilsService.httpCommonError('Detalles de academias no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM14',
                languageType: 'es',
            });
        }

        return updateAcademiesDetails;
    }


    @ApiResponse({
        status: 200,
        type: AcademiesDetails,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteAcademiesDetails(@Param('id') idAcademiesDetails: string) {

        const deleteAcademiesDetails = await this._academiesDetailsServices.destroyAcademiesDetails(idAcademiesDetails);

        if (!deleteAcademiesDetails) {
            throw this.appUtilsService.httpCommonError('Detalles de academias no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM14',
                languageType: 'es',
            });
        }

        return deleteAcademiesDetails;
    }

}