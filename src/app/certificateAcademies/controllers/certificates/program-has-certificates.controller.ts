import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Delete, NotFoundException, HttpStatus, Param, Get } from "@nestjs/common";
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { AcademiesHasCertificatesServices } from "../../services/academies/academies-has-certificates.service";
import { AcademiesHasCertificatesSaveJson } from "../../interfaces/academies-has-certificates.interfaces";
import { ApiUseTags, ApiImplicitParam } from "@nestjs/swagger";
import { ProgramHasCertificatesServices } from "../../services/certificates/program-has-certificates.services";
import { ProgramHasCertificatesSaveJson } from "../../interfaces/program-has-certificates.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller ProgramHasCertificatesController
 * @Creado 27 de Marzo 2019
 */

@ApiUseTags('Module-Certificates')
@Controller('program-has-certificates')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class ProgramHasCertificatesController {

    constructor(private readonly _programHasCertificatesServices: ProgramHasCertificatesServices,
        private readonly appUtilsService: AppUtilsService){}

    @Post()
    async create(@Body() body: ProgramHasCertificatesSaveJson) {

        const createAcaHasCertif = await this._programHasCertificatesServices.saveProgramHasCertificates(body);

        if (!createAcaHasCertif) {
            throw this.appUtilsService.httpCommonError('Certificado no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM23',
                languageType: 'es',
            });
        }

        return createAcaHasCertif;

    }

    @Get()
    async getAll() {
        const data = await this._programHasCertificatesServices.getAll();

        if (!data) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return data;
    }

    @Delete(':id')
    @ApiImplicitParam({name: 'id', required: true, type: 'string'})
    async deleteProHasCertif(@Param('id') id: string) {

        const deleteProHasCertif = await this._programHasCertificatesServices.destroyProgramHasCertificates(id);

        if (!deleteProHasCertif) {
            throw this.appUtilsService.httpCommonError('Certificado no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM23',
                languageType: 'es',
            });
        }

        return deleteProHasCertif;
    }
}

