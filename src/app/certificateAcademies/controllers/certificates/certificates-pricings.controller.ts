import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param } from "@nestjs/common";
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { CertificatesPricingsServices } from "../../services/certificates/certificates-pricings.services";
import { CertificatePricingsSaveJson, CertificatePricingsUpdateJson } from "../../interfaces/certificates-pricings.interfaces";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { CertificatePricings } from "../../models/certificates-pricings.models";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller CertificatesPricingsController
 * @Creado 27 de Marzo 2019
 */

@ApiUseTags('Module-Certificates')
@Controller('certificates-pricings')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class CertificatesPricingsController {

    constructor(private readonly _certificatesPServices: CertificatesPricingsServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: CertificatePricings,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyCertificatesP: CertificatePricingsSaveJson) {

        const createCertificatesP = await this._certificatesPServices.saveCertificatesP(bodyCertificatesP);

        if (!createCertificatesP) {
            throw this.appUtilsService.httpCommonError('Precios del certificado no han sido creados!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM21',
                languageType: 'es',
            });
        }

        return createCertificatesP;

    }


    @ApiResponse({
        status: 200,
        type: CertificatePricings,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAll() {
        return await this._certificatesPServices.showAll();
    }


    @ApiResponse({
        status: 200,
        type: CertificatePricings,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idCertificatesP: string) {

        const fetchCertificatesP = await this._certificatesPServices.getCertificatesPricings(idCertificatesP);

        if (!fetchCertificatesP) {
            throw this.appUtilsService.httpCommonError('Precios del certificado no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM22',
                languageType: 'es',
            });
        }

        return fetchCertificatesP;
    }


    @ApiResponse({
        status: 200,
        type: CertificatePricings,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(@Param('id') id: string, @Body() bodyCertificatesP: CertificatePricingsUpdateJson) {

        const updateCertificatesP = await this._certificatesPServices.updateCertificatesPricings(id, bodyCertificatesP);

        if (!updateCertificatesP) {
            throw this.appUtilsService.httpCommonError('Precios del certificado no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM22',
                languageType: 'es',
            });
        }

        return updateCertificatesP;
    }


    @ApiResponse({
        status: 200,
        type: CertificatePricings,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteCertificatesP(@Param('id') idCertificatesP: string) {

        const deleteCertificatesP = await this._certificatesPServices.destroyCertificatesPricings(idCertificatesP);

        if (!deleteCertificatesP) {
            throw this.appUtilsService.httpCommonError('Precios del certificado no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM22',
                languageType: 'es',
            });
        }

        return deleteCertificatesP;
    }
}