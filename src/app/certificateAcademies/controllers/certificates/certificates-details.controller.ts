import { Controller, UseFilters, UseInterceptors, Post, Body, Get, Put, Delete, HttpStatus, FileFieldsInterceptor, Param } from "@nestjs/common";
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { CertificatesDetailsServices } from "../../services/certificates/certificates-details.sevices";
import { CertificatesDetailsSaveJson, CertificatesDetailsUpdateJson } from "../../interfaces/certificates-details.interfaces";
import { ApiUseTags, ApiConsumes, ApiImplicitParam, ApiImplicitFile, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { CONFIG } from "../../../../config";
import * as file from './infoFile';
import { CertificateDetails } from "../../models/certificates-details.models";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller CertificatesDetailsController
 * @Creado 27 de Marzo 2019
 */

@ApiUseTags('Module-Certificates')
@Controller('certificates-details')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class CertificatesDetailsController {

    constructor(private readonly _certificatesDetailsServices: CertificatesDetailsServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: CertificateDetails,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    @ApiConsumes('multipart/form-data')
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiImplicitFile({ name: 'certificateDetails', description: 'Certificate Details File', required: true })
    async create(@Body() bodyCertificatesDetails: CertificatesDetailsSaveJson) {

        const createCertificatesDetails = await this._certificatesDetailsServices.saveCertificatesDetails(bodyCertificatesDetails);

        if (!createCertificatesDetails) {
            throw this.appUtilsService.httpCommonError('Detalles del certificado no han sido creados!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM19',
                languageType: 'es',
            });
        }
        createCertificatesDetails.certificateImg = `${CONFIG.storage.server}certificateDetails/${createCertificatesDetails.certificateImg}`;

        return createCertificatesDetails;

    }


    @ApiResponse({
        status: 200,
        type: CertificateDetails,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAll() {

        const fetchAll: CertificatesDetailsUpdateJson[] = await this._certificatesDetailsServices.showAll();
        if (!fetchAll) {
            throw this.appUtilsService.httpCommonError('Detalles del certificado no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM20',
                languageType: 'es',
            });
        }

        fetchAll.forEach(fetch => fetch.certificateImg = `${CONFIG.storage.server}certificateDetails/${fetch.certificateImg}`);

        return fetchAll;
    }


    @ApiResponse({
        status: 200,
        type: CertificateDetails,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idCertificatesDetails: string) {

        const fetchCertificatesDetails = await this._certificatesDetailsServices.getDetails(idCertificatesDetails);

        if (!fetchCertificatesDetails) {
            throw this.appUtilsService.httpCommonError('Detalles del certificado no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM20',
                languageType: 'es',
            });
        }

        fetchCertificatesDetails.certificateImg = `${CONFIG.storage.server}certificateDetails/${fetchCertificatesDetails.certificateImg}`;

        return fetchCertificatesDetails;
    }


    @ApiResponse({
        status: 200,
        type: CertificateDetails,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'certificateDetails', description: 'Certificate Details File', required: true })
    async updateNews(@Param('id') id: string, @Body() bodyCertificatesDetails: Partial<CertificatesDetailsUpdateJson>) {

        const updateCertificatesDetails = await this._certificatesDetailsServices.updateCertificatesDetails(id, bodyCertificatesDetails);

        if (!updateCertificatesDetails[1][0]) {
            throw this.appUtilsService.httpCommonError('Detalles del certificado no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM20',
                languageType: 'es',
            });
        }

        updateCertificatesDetails[1][0].certificateImg = `${CONFIG.storage.server}certificateDetails/${updateCertificatesDetails[1][0].certificateImg}`;

        return updateCertificatesDetails;
    }


    @ApiResponse({
        status: 200,
        type: CertificateDetails,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteCertificatesDetails(@Param('id') idCertificatesDetails: string) {

        const deleteCertificatesDetails = await this._certificatesDetailsServices.destroyCertificatesDetails(idCertificatesDetails);

        if (!deleteCertificatesDetails) {
            throw this.appUtilsService.httpCommonError('Detalles del certificado no existen!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM20',
                languageType: 'es',
            });
        }

        return deleteCertificatesDetails;
    }
}