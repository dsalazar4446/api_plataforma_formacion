import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Get, NotFoundException, Put, Delete, HttpStatus, Param, Req } from "@nestjs/common";
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { CertificatesServices } from "../../services/certificates/certificates.services";
import { CertificatesSaveJson, CertificatesUpdateJson } from "../../interfaces/certificates.interfaces";
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from "@nestjs/swagger";
import { Certificates } from "../../models/certificates.models";
/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller CertificatesController
 * @Creado 27 de Marzo 2019
 */

@ApiUseTags('Module-Certificates')
@Controller('certificates')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class CertificatesController {

    constructor(private readonly _certificatesServices: CertificatesServices,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: Certificates,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyCertificates: CertificatesSaveJson) {

        const createCertificates = await this._certificatesServices.saveCertificates(bodyCertificates);

        if (!createCertificates) {
            throw this.appUtilsService.httpCommonError('Certificado no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM23',
                languageType: 'es',
            });
        }

        return createCertificates;
    }


    @ApiResponse({
        status: 200,
        type: Certificates,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list/:status?')
    @ApiImplicitParam({ name: 'status', required: false, type: 'boolean' })
    async showAll(@Param('status') status?: boolean) {

        if (!status) {
            return await this._certificatesServices.findAll();
        }

        const fetch = await this._certificatesServices.showAllCertificates(status);

        if (!fetch[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetch;
    }

    @ApiResponse({
        status: 200,
        type: Certificates,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllCertificatesByLanguage(@Req() req) {

        const fetch = await this._certificatesServices.showAllCertificatesByLanguage(req.headers.language);

        if (!fetch) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetch;
    }


    @ApiResponse({
        status: 200,
        type: Certificates,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idCertificates: string) {

        const fetchCertificates = await this._certificatesServices.getDetailsCertificates(idCertificates);

        if (!fetchCertificates) {
            throw this.appUtilsService.httpCommonError('Certificado no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM24',
                languageType: 'es',
            });
        }

        return fetchCertificates;
    }


    @ApiResponse({
        status: 200,
        type: Certificates,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(@Param('id') id: string, @Body() bodyCertificates: CertificatesUpdateJson) {
        const updateCertificates = await this._certificatesServices.updateCertificates(id, bodyCertificates);

        if (!updateCertificates[1][0]) {
            throw this.appUtilsService.httpCommonError('Certificado no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM24',
                languageType: 'es',
            });
        }

        return updateCertificates;
    }


    @ApiResponse({
        status: 200,
        type: Certificates,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteCertificates(@Param('id') idCertificates: string) {

        const deleteCertificates = await this._certificatesServices.destroyCertificates(idCertificates);

        if (!deleteCertificates) {
            throw this.appUtilsService.httpCommonError('Certificado no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM24',
                languageType: 'es',
            });
        }

        return deleteCertificates;
    }



}