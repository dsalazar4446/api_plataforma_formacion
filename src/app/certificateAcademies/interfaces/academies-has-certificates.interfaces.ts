import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsNotEmpty } from 'class-validator';
export class AcademiesHasCertificatesSaveJson {
    @ApiModelProperty()
    @IsUUID('4',{message: 'academyId property must a be uuid'})
    @IsNotEmpty({message: 'academyId property not must null'})
    academyId: string;
    @ApiModelProperty()
    @IsUUID('4',{message: 'certificateId property must a be uuid'})
    @IsNotEmpty({message: 'certificateId property not must null'})
    certificateId: string;
}
export class AcademiesHasCertificatesUpdateJson extends AcademiesHasCertificatesSaveJson {

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
