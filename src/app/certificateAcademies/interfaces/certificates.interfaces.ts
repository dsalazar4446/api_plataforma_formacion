import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsNotEmpty, IsString, IsNumber, IsBoolean } from 'class-validator';

export class CertificatesSaveJson {

    @ApiModelProperty()
    certificateCode?: string;

    @ApiModelProperty()
    @IsBoolean({message: 'status property must a be boolean'})
    @IsNotEmpty({message: 'status property not must null'})
    status: boolean;
}
export class CertificatesUpdateJson extends CertificatesSaveJson {

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
