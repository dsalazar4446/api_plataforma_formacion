import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsNotEmpty, IsEmail, IsUrl, IsString, IsOptional, IsNumber, IsBoolean } from 'class-validator';

export class AcademiesSaveJson {

    @ApiModelProperty()
    @IsString({message: 'academyName property must a be string'})
    @IsNotEmpty({message: 'academyName property not must null'})
    academyName: string;

    @ApiModelProperty()
    @IsString({message: 'avatar property must a be string'})
    @IsOptional()
    avatar?: string;

    @ApiModelProperty()
    @IsString({message: 'institutionPicture property must a be string'})
    @IsOptional()
    institutionPicture?: string;

    @ApiModelProperty()
    @IsString({message: 'latitude property must a be string'})
    @IsOptional()
    latitude?: string;

    @ApiModelProperty()
    @IsString({message: 'longitude property must a be string'})
    @IsOptional()
    longitude?: string;

    @ApiModelProperty()
    @IsString({message: 'address1 property must a be string'})
    @IsOptional()
    address1?: string;

    @ApiModelProperty()
    @IsString({message: 'address2 property must a be string'})
    @IsOptional()
    address2?: string;

    @ApiModelProperty()
    @IsString({message: 'website property not must string'})
    @IsNotEmpty({message: 'website property not must null'})
    @IsUrl()
    website: string;

    @ApiModelProperty()
    @IsString({message: 'email property must a be string'})
    @IsNotEmpty({message: 'email property not must null'})
    @IsEmail()
    email: string;

    @ApiModelProperty()
    @IsBoolean({message: 'status property must a be boolean'})
    @IsNotEmpty({message: 'status property not must null'})
    status: boolean;

}

export class AcademiesUpdateJson extends AcademiesSaveJson {

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
