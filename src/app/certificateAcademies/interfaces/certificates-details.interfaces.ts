import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsUUID, IsOptional } from 'class-validator';
export class CertificatesDetailsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'certificatesId property must a be uuid'})
    @IsNotEmpty({message: 'certificatesId property not must null'})
    certificatesId: string;

    @ApiModelProperty()
    @IsString({message: 'certificateDetail property must a be string'})
    @IsNotEmpty({message: 'certificateDetail property not must null'})
    certificateDetail: string;

    @ApiModelProperty()
    @IsString({message: 'certificateImg property must a be string'})
    @IsOptional()
    certificateImg?: string;

    @ApiModelProperty()
    @IsString({message: 'languageType property must a be string'})
    @IsOptional()
    languageType: string;
}
export class CertificatesDetailsUpdateJson extends CertificatesDetailsSaveJson {

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
