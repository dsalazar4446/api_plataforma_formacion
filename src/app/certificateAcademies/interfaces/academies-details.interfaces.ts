import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsNotEmpty, IsString, IsOptional } from 'class-validator';
export class AcademiesDetailsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'academyId property must a be uuid'})
    @IsNotEmpty({message: 'academyId property not must null'})
    academyId: string;

    @ApiModelProperty()
    @IsString({message: 'languageType property must a be string'})
    @IsOptional()
    languageType: string;

    @ApiModelProperty()
    @IsString({message: 'academyDetail property must a be string'})
    @IsNotEmpty({message: 'academyDetail property not must null'})
    academyDetail: string;
}
export class AcademiesDetailsUpdateJson extends AcademiesDetailsSaveJson {
    
    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
