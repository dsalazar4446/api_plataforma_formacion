import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsNotEmpty, IsNumber, IsBoolean } from 'class-validator';
export class CertificatePricingsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'certificatesId property must a be uuid'})
    @IsNotEmpty({message: 'certificatesId property not must null'})
    certificatesId: string;
    
    @ApiModelProperty()
    @IsNumber(null,{message: 'certificatePricing property must a be number'})
    @IsNotEmpty({message: 'certificatePricing property not must null'})
    certificatePricing: number;

    @ApiModelProperty()
    @IsBoolean({message: 'certificatePricingStatus property must a be boolean'})
    @IsNotEmpty({message: 'certificatePricingStatus property not must null'})
    certificatePricingStatus: boolean;

    @ApiModelProperty()
    @IsNumber(null, { message: 'certificatePricing property must a be number' })
    @IsNotEmpty({ message: 'certificatePricing property not must null' })
    certificateOdooId: number;
}
export class CertificatePricingsUpdateJson extends CertificatePricingsSaveJson {

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
