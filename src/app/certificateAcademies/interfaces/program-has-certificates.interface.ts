import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsNotEmpty } from 'class-validator';
export class ProgramHasCertificatesSaveJson {
    @ApiModelProperty()
    @IsUUID('4',{message: 'programsId property must a be uuid'})
    @IsNotEmpty({message: 'programsId property not must null'})
    programsId: string;
    @ApiModelProperty()
    @IsUUID('4',{message: 'certificateId property must a be uuid'})
    @IsNotEmpty({message: 'certificateId property not must null'})
    certificatesId: string;
}
