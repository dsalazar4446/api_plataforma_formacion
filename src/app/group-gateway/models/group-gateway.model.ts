import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany } from 'sequelize-typescript';
import { TransactionsPayments } from '../../transactions/models/transactions-payments.models';
import { ApiModelProperty } from '@nestjs/swagger';

@Table({
    tableName: 'group_gateways',
})
export class GroupGateways extends Model<GroupGateways>{

    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(75),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'name',
    })
    name: string;
    @ApiModelProperty()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'default_by',
    })
    defaultBy: boolean;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;

}