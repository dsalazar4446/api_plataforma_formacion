import { ApiUseTags, ApiConsumes, ApiImplicitFile, ApiImplicitParam, ApiOperation, ApiResponse, ApiImplicitHeader } from "@nestjs/swagger";
import { Controller, UseFilters, UseInterceptors, Post, FileFieldsInterceptor, Body, HttpStatus, Get, NotFoundException, Param, Put, Delete } from "@nestjs/common";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { GroupGatewaysServices } from "../services/group-gateway.service";
import { GroupGatewaysSaveJson, GroupGatewaysUpdateJson } from "../interfaces/group-gateway.interface";
import { GroupGateways } from "../models/group-gateway.model";

@ApiUseTags('Module-Gateways')
@Controller('group-gateways')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class GroupGatewaysController {

    constructor(
        private readonly groupGatewaysServices: GroupGatewaysServices,
    ) { }


    @ApiResponse({
        status: 200,
        type: GroupGateways,
    })
    @Post()
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    async create(@Body() body: GroupGatewaysSaveJson) {
        return await this.groupGatewaysServices.create(body);
    }


    // LIST SESSION
    @ApiResponse({
        status: 200,
        type: GroupGateways,
        isArray: true
    })
    @Get()
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    async getAll() {
        return await this.groupGatewaysServices.findAll();
    }


    // DETAIL SESSION
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Get(':id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: GroupGateways,
    })
    async findById(@Param('id') idGroupGateway) {
        return await this.groupGatewaysServices.findById(idGroupGateway);
    }


    // UPDATE SESSION
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Put(':id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: GroupGateways,
    })
    async update(
        @Body()
        updateGroupGateway: Partial<GroupGatewaysUpdateJson>,
        @Param('id') idGroupGateway
    ) {
        return await this.groupGatewaysServices.update(idGroupGateway, updateGroupGateway);
    }

    
    // DELETE SESSION
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @Delete(':id')
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    async delete(@Param('id') idGroupGateway) {
        return await this.groupGatewaysServices.deleted(idGroupGateway);
    }
}