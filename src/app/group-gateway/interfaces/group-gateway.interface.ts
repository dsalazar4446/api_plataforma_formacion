import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional, IsBoolean } from 'class-validator';
export class GroupGatewaysSaveJson {

    @ApiModelProperty()
    @IsString({message: 'name property must a be string'})
    @IsNotEmpty({message: 'name property not must null'})
    name: string;
    @ApiModelProperty()
    @IsBoolean({message: 'defaultBy property must a be string'})
    @IsNotEmpty({message: 'defaultBy property not must null'})
    defaultBy: boolean;
}
// tslint:disable-next-line: max-classes-per-file
export class GroupGatewaysUpdateJson extends GroupGatewaysSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
