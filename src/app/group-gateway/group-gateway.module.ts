import { Module, HttpModule, Provider } from "@nestjs/common";
import { SharedModule } from "../shared/shared.module";
import { DatabaseModule } from "../database/database.module";
import { GroupGateways } from "./models/group-gateway.model";
import { GroupGatewaysController } from "./controllers/group-gateway.controller";
import { GroupGatewaysServices } from "./services/group-gateway.service";



const models = [ GroupGateways ];
const controllers = [ GroupGatewaysController ];
const providers: Provider[] = [ GroupGatewaysServices ];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers,
  exports:[
    GroupGatewaysServices
  ]
})
export class GroupGatewayModule { }
