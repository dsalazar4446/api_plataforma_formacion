import { Injectable, Inject, HttpStatus, HttpException } from "@nestjs/common";
import { GroupGateways } from "../models/group-gateway.model";
import { GroupGatewaysSaveJson, GroupGatewaysUpdateJson } from "../interfaces/group-gateway.interface";
@Injectable()
export class GroupGatewaysServices {

    constructor(
        @Inject('GroupGateways') private readonly groupGateways: typeof GroupGateways
    ) { }

    async findAll(): Promise<any> {
        return await this.groupGateways.findAll();
    }

    async findById(id: string): Promise<GroupGateways> {
        return await this.groupGateways.findByPk<GroupGateways>(id);
    }

    async create(body: GroupGatewaysSaveJson): Promise<GroupGateways> {
        body.defaultBy = false;
        return await this.groupGateways.create<GroupGateways>(body, {
            returning: true,
        });
    }

    async update(idGroupGateways: string, groupGatewaysUpdate: Partial<GroupGatewaysUpdateJson>){
        const resultGroupGateway = await this.groupGateways.findByPk<GroupGateways>(idGroupGateways);
        if(resultGroupGateway.dataValues.defaultBy === true){
            throw new HttpException('No tiene los permisos necesarios para actualizar el grupo ingresado', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        return  await this.groupGateways.update(groupGatewaysUpdate, {
            where: {
                id: idGroupGateways,
            },
            returning: true,
        });
    }

    async deleted(idGroupGateways: string): Promise<any> {
        const resultGroupGateway = await this.groupGateways.findByPk<GroupGateways>(idGroupGateways);
        if(resultGroupGateway.dataValues.defaultBy === true){
            throw new HttpException('No tiene los permisos necesarios para eliminar el grupo ingresado', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        return await this.groupGateways.destroy({
            where: {
                id: idGroupGateways,
            },
        });
    }
}