
import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { BroadcastPrebuiltMessageHasCategoriesController } from './controllers/broadcast-prebuilt-message-has-categories.controller';
import { BroadcastPrebuiltMessageHasCategoriesService } from './services/broadcast-prebuilt-message-has-categories.service';
import { BroadcastPrebuiltMessageHasCategoriesModel } from './models/broadcast-prebuilt-message-has-categories.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { Categories } from '../categories/models/categories.model';
import { BroadcastPrebuiltMessageModel } from '../broadcast-prebuilt-message/models/broadcast-prebuilt-message.model';

const models = [
  BroadcastPrebuiltMessageHasCategoriesModel,
  Categories,
  BroadcastPrebuiltMessageModel
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [BroadcastPrebuiltMessageHasCategoriesController],
  providers: [BroadcastPrebuiltMessageHasCategoriesService]
})
export class BroadcastPrebuiltMessageHasCategoriesModule {}
