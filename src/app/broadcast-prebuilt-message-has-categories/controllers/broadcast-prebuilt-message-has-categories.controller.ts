import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, Req} from '@nestjs/common';
import { BroadcastPrebuiltMessageHasCategoriesService } from '../services/broadcast-prebuilt-message-has-categories.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { BroadcastPrebuiltMessageHasCategoriesSaveJson } from '../interfaces/broadcast-prebuilt-message-has-categories.interface';
import { ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { JwtAuthGuard } from '../../auths/guards/jwt-auth.guard';
import { BroadcastPrebuiltMessageHasCategoriesModel } from '../models/broadcast-prebuilt-message-has-categories.model';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import { Categories } from '../../categories/models/categories.model';
import { BroadcastPrebuiltMessageModel } from '../../broadcast-prebuilt-message/models/broadcast-prebuilt-message.model';

@Controller('broadcast-prebuilt-message-has-categories')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// @UseGuards(new JwtAuthGuard())
export class BroadcastPrebuiltMessageHasCategoriesController {
    constructor(
        private readonly broadcastPrebuiltMessageHasCategoriesService: BroadcastPrebuiltMessageHasCategoriesService,
        @Inject('BroadcastPrebuiltMessageModel') private readonly broadcastPrebuiltMessageModel: typeof BroadcastPrebuiltMessageModel,
        @Inject('Categories') private readonly categories: typeof Categories,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        @ApiResponse({
            status: 200,
            type: BroadcastPrebuiltMessageHasCategoriesModel,
          })
        @Post()
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        async create(@Body() broadcastPrebuiltMessageHasCategories: BroadcastPrebuiltMessageHasCategoriesSaveJson, @Req() req) {
          const data2= await this.broadcastPrebuiltMessageModel.findByPk(broadcastPrebuiltMessageHasCategories.broadcastPrebuiltMessageId);
          if(data2 == null){
                throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA30',
                    languageType:  req.headers.language,
                });
          } 
          const data3= await this.categories.findByPk(broadcastPrebuiltMessageHasCategories.categoriesId);
          if(data3 == null){
                throw this.appUtilsService.httpCommonError('BroadcastAttendance does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA24',
                    languageType:  req.headers.language,
                });
          }
          return await this.broadcastPrebuiltMessageHasCategoriesService.create(broadcastPrebuiltMessageHasCategories);
        }
        @ApiResponse({
            status: 200,
            type: BroadcastPrebuiltMessageHasCategoriesModel,
            isArray: true
          })
        @Get()
        @ApiImplicitHeader({
          name:'language',
          required: true,
      })
        async getAll() {
            return await this.broadcastPrebuiltMessageHasCategoriesService.findAll();
        }
}
