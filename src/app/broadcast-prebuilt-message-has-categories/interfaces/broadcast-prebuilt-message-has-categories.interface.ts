import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class BroadcastPrebuiltMessageHasCategoriesSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    broadcastPrebuiltMessageId: string;
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    categoriesId: string; 
}