import { Injectable, Inject } from '@nestjs/common';
import { BroadcastPrebuiltMessageHasCategoriesSaveJson} from '../interfaces/broadcast-prebuilt-message-has-categories.interface';
import { BroadcastPrebuiltMessageHasCategoriesModel } from '../models/broadcast-prebuilt-message-has-categories.model';
import { Categories } from '../../categories/models/categories.model';
import { BroadcastPrebuiltMessageModel } from '../../broadcast-prebuilt-message/models/broadcast-prebuilt-message.model';

@Injectable()
export class BroadcastPrebuiltMessageHasCategoriesService {
    constructor(
// tslint:disable-next-line: max-line-length
        @Inject('BroadcastPrebuiltMessageHasCategoriesModel') private readonly broadcastPrebuiltMessageHasCategoriesModel: typeof BroadcastPrebuiltMessageHasCategoriesModel,
        @Inject('BroadcastPrebuiltMessageModel') private readonly broadcastPrebuiltMessageModel: typeof BroadcastPrebuiltMessageModel,
        @Inject('Categories') private readonly categories: typeof Categories,
    ) { }
    async findAll(): Promise<BroadcastPrebuiltMessageHasCategoriesModel[]> {
        const data = await this.broadcastPrebuiltMessageHasCategoriesModel.findAll();
        const result: any[] = [];
        await Promise.all(
            await data.map(
                async( item: any )=>{
                    const dataBPM = await this.broadcastPrebuiltMessageModel.findByPk(item.dataValues.broadcastPrebuiltMessageId);
                    const dataCategories = await this.categories.findByPk(item.dataValues.categoriesId);
                    item.dataValues.broadcastPrebuiltMessageModel = dataBPM;
                    item.dataValues.categoriesModel = dataCategories;
                    result.push(item.dataValues);
                }
            )
        )
        return result;
    }
    async create(broadcastMessageHasCategories: BroadcastPrebuiltMessageHasCategoriesSaveJson): Promise<BroadcastPrebuiltMessageHasCategoriesModel> {
// tslint:disable-next-line: max-line-length
        return await this.broadcastPrebuiltMessageHasCategoriesModel.create<BroadcastPrebuiltMessageHasCategoriesModel>(broadcastMessageHasCategories, {
            returning: true,
        });
    }
}
