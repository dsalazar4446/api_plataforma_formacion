import { Column, DataType, Table, Model,ForeignKey, BelongsTo, BelongsToMany } from 'sequelize-typescript';
import { Categories } from '../../categories/models/categories.model';
import { BroadcastPrebuiltMessageModel } from '../../broadcast-prebuilt-message/models/broadcast-prebuilt-message.model';
import { ApiModelProperty } from '@nestjs/swagger';

// FALTA RELACIONAR CON CATEGORY
@Table({
    tableName: 'broadcast_prebuilt_message_has_categories',
})
export class BroadcastPrebuiltMessageHasCategoriesModel extends Model<BroadcastPrebuiltMessageHasCategoriesModel> {
    @ApiModelProperty()
    @ForeignKey(() => BroadcastPrebuiltMessageModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'broadcast_prebuilt_message_id',
    })
    broadcastPrebuiltMessageId: string; 
    @ApiModelProperty()
    @ForeignKey(() => Categories)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'category_id',
    })
    categoriesId: string; 
}
