import { Test, TestingModule } from '@nestjs/testing';
import { ClassHomeworksService } from './class-homeworks.service';

describe('ClassHomeworksService', () => {
  let service: ClassHomeworksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClassHomeworksService],
    }).compile();

    service = module.get<ClassHomeworksService>(ClassHomeworksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
