import { Injectable, Inject } from '@nestjs/common';
import { ClassHomeworksModel } from '../../models/class-homeworks.model';
import { ClassHomeworkDetailsModel } from '../../models/class-homeworks-details.model';
import { UserHomeworkAnswersModel } from '../../models/user-homework-answer.model';
import { ClassSequencesModel } from '../../models/class-sequences.model';
import * as ShortUniqueId from 'short-unique-id';
import { CreateClassHomeworksDto, UpdateClassHomeworksDto } from '../../dto/class-homeworks.dto';

@Injectable()
export class ClassHomeworksService {

    constructor(@Inject('ClassHomeworksModel') private readonly classHomeworks: typeof ClassHomeworksModel) { }

    async create(body: CreateClassHomeworksDto) {
        const uid = new ShortUniqueId();
        body.homeworkCode = uid.randomUUID(9);
        const createClassHomeworks = new ClassHomeworksModel(body);
        return await createClassHomeworks.save();
    }
    async list() {
        return await this.classHomeworks.findAll({
            include: [
                ClassHomeworkDetailsModel,
                UserHomeworkAnswersModel,
                ClassSequencesModel
            ]
        });
    }
    async detail(id: string) {
        return await this.classHomeworks.findByPk(id, {
            include: [
                ClassHomeworkDetailsModel,
                UserHomeworkAnswersModel,
                ClassSequencesModel
            ]
        });
    }
    async update(id: string, body: Partial<UpdateClassHomeworksDto>) {
        return await this.classHomeworks.update(body, { where: { id } });
    }
    async delete(id: string) {
        return this.classHomeworks.destroy({ where: { id } });
    }

}
