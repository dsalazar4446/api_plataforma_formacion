import { Test, TestingModule } from '@nestjs/testing';
import { UserVideoFavoritesService } from './user-video-favorites.service';

describe('UserVideoFavoritesService', () => {
  let service: UserVideoFavoritesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserVideoFavoritesService],
    }).compile();

    service = module.get<UserVideoFavoritesService>(UserVideoFavoritesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
