import { Injectable, Inject } from '@nestjs/common';
import { CreateUserVideoFavorites, UpdateUserVideoFavorites } from '../../dto/user-video-favorites.dto';
import { UserVideoFavoritesModel } from '../../models/user-video-favorites.model';

@Injectable()
export class UserVideoFavoritesService {
    constructor(@Inject('UserVideoFavoritesModel') private readonly userVideoFavorites: typeof UserVideoFavoritesModel) { }

    async create(body: CreateUserVideoFavorites) {
        const nuevo = new UserVideoFavoritesModel(body);
        return await nuevo.save();
    }

    async list() {
        return await this.userVideoFavorites.findAll();
    }

    async detail(id: string) {
        return await this.userVideoFavorites.findByPk(id)
    }

    async update(id: string, body: Partial<UpdateUserVideoFavorites>) {
        return await this.userVideoFavorites.update(body, { where: { id }, returning: true })
    }

    async delete(id: string) {
        return await this.userVideoFavorites.destroy({ where: { id } })
    }
}
