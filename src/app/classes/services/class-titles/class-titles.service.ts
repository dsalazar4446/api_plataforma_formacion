import { Injectable, Inject } from '@nestjs/common';
import { ClassTitlesModel } from '../../models/class-titles.model';
import { UpdateClassTitlesDto } from '../../dto/class-titles.dto';

@Injectable()
export class ClassTitlesService {

    constructor(@Inject('ClassTitlesModel') private readonly classTitles: typeof ClassTitlesModel) { }

    async create(body) {
        const createClassTitles = new ClassTitlesModel(body);
        return await createClassTitles.save();
    }
    async list(languageType: string) {
        return await this.classTitles.findAll({where: {languageType}});
    }
    async detail(id: string, languageType: string) {
        return await this.classTitles.findByPk(id, {where: {languageType}});
    }
    async update(id: string, body: Partial<UpdateClassTitlesDto>) {
        return await this.classTitles.update(body, { where: { id }, returning: true });
    }
    async delete(id: string) {
        return this.classTitles.destroy({ where: { id } });
    }

}
