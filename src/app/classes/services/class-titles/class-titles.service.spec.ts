import { Test, TestingModule } from '@nestjs/testing';
import { ClassTitlesService } from './class-titles.service';

describe('ClassTitlesService', () => {
  let service: ClassTitlesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClassTitlesService],
    }).compile();

    service = module.get<ClassTitlesService>(ClassTitlesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
