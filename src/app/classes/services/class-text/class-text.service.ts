import { Injectable, Inject } from '@nestjs/common';
import { ClassTextModel } from '../../models/class-text.model';
import { ClassSequencesModel } from '../../models/class-sequences.model';
import { UpdateClassTextDto } from '../../dto/class-text.dto';

@Injectable()
export class ClassTextService {

    constructor(@Inject('ClassTextModel') private readonly classText: typeof ClassTextModel) { }

    async create(body) {
        const createClassText = new ClassTextModel(body);
        return await createClassText.save();
    }
    async list(languageType: string) {  
        return await this.classText.findAll({
            include: [
                ClassSequencesModel
            ],
            where:{
                languageType
            }
        });
    }
    async detail(id: string, languageType: string) {
        return await this.classText.findByPk(id, {
            include: [
                ClassSequencesModel
            ],
            where:{
                languageType
            }
        });
    }
    async update(id: string, body: Partial<UpdateClassTextDto>) {
        return await this.classText.update(body, { where: { id } , returning: true});
    }
    async delete(id: string) {
        return this.classText.destroy({ where: { id } });
    }

}
