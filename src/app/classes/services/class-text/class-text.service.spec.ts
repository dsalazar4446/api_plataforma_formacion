import { Test, TestingModule } from '@nestjs/testing';
import { ClassTextService } from './class-text.service';

describe('ClassTextService', () => {
  let service: ClassTextService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClassTextService],
    }).compile();

    service = module.get<ClassTextService>(ClassTextService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
