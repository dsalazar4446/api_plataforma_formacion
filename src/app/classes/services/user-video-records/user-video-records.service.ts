import { Injectable, Inject } from '@nestjs/common';
import { UserVideoRecordsModel } from '../../models/user-video-records.model';
import {CreateUserVideoRecords, UpdateUserVideoRecords } from '../../dto/user-video-records.dto';

@Injectable()
export class UserVideoRecordsService {
    constructor(@Inject('UserVideoRecordsModel') private readonly userVideoRecords: typeof UserVideoRecordsModel){}

    async create(body: CreateUserVideoRecords){
        const nuevo = new UserVideoRecordsModel(body);
        return await nuevo.save();
    }

    async list(){
        return await this.userVideoRecords.findAll();
    }

    async detail(id: string){
        return await this.userVideoRecords.findByPk(id)
    }

    async update(id: string, body: Partial<UpdateUserVideoRecords>){
        return await this.userVideoRecords.update(body,{where:{id}, returning: true})
    }

    async delete(id: string){
        return await this.userVideoRecords.destroy({where: {id}})
    }

}
