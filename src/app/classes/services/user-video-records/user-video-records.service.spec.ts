import { Test, TestingModule } from '@nestjs/testing';
import { UserVideoRecordsService } from './user-video-records.service';

describe('UserVideoRecordsService', () => {
  let service: UserVideoRecordsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserVideoRecordsService],
    }).compile();

    service = module.get<UserVideoRecordsService>(UserVideoRecordsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
