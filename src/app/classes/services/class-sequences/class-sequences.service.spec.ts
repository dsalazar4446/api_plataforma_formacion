import { Test, TestingModule } from '@nestjs/testing';
import { ClassSequencesService } from './class-sequences.service';

describe('ClassSequencesService', () => {
  let service: ClassSequencesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClassSequencesService],
    }).compile();

    service = module.get<ClassSequencesService>(ClassSequencesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
