import { Injectable, Inject } from '@nestjs/common';
import { ClassSequencesModel } from '../../models/class-sequences.model';
import { MainVideosModel } from '../../../main-videos/models/main-videos.model';
import { ClassAttachmentsModel } from '../../models/class-attachments.model';
import { ClassHomeworksModel } from '../../models/class-homeworks.model';
import { ClassTextModel } from '../../models/class-text.model';
import { FormationTest } from '../../../test/models/formation-test.models';
import { UserClassesViewsModel } from '../../models/user-classes-views.model';
import { UpdateClassSequences } from '../../dto/class-sequences.dto';
import { CONFIG } from '../../../../config';

const include = [
    MainVideosModel,
    ClassAttachmentsModel,
    ClassHomeworksModel,
    ClassTextModel,
    FormationTest,
    UserClassesViewsModel,
]
@Injectable()
export class ClassSequencesService {

    constructor(@Inject('ClassSequencesModel') private readonly classSequences: typeof ClassSequencesModel) { }

    async create(body) {
        const createClassSequences = new ClassSequencesModel(body);
        return await createClassSequences.save();
    }
    async list() {
        const result = await this.classSequences.findAll({
            include,
        });
        result.map(element => {
            element.classAttachments.map(subElement =>{
              subElement.classAttachment = CONFIG.storage.server+'courses/'+subElement.classAttachment  
            })
          })
        return  result;
    }
    async detail(id: string) {
        const result = await this.classSequences.findByPk(id,{
            include,
        });
        result.classAttachments.map(subElement =>{
            subElement.classAttachment = CONFIG.storage.server+'courses/'+subElement.classAttachment 
        })
        
        return result
    }
    async update(id: string, body: Partial<UpdateClassSequences>) {
        return await this.classSequences.update(body, { where: { id }, returning: true});
    }
    async delete(id: string) {
        return this.classSequences.destroy({ where: { id } });
    }

}
