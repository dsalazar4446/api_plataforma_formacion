import { Injectable, Inject } from '@nestjs/common';
import { FormationClassesModel } from '../../models/formation-classes.model';
import { ClassTitlesModel } from '../../models/class-titles.model';
import { ClassSequencesModel } from '../../models/class-sequences.model';
import * as ShortUniqueId from 'short-unique-id';
import { CreateFormationClass, UpdateFormationClass } from '../../dto/formation-classes.dto';
import { FormationTest } from '../../../test/models/formation-test.models';
import { ClassProblemsModel } from '../../models/class-problems.model';
import { NotesModel } from '../../models/notes.model';
import { UserClassQuestionsAnswersModel } from '../../models/user-class-questions-answers.model';
    
@Injectable()
export class FormationClassesService {
    include = [
                ClassTitlesModel,
                ClassSequencesModel,
                FormationTest,
                ClassProblemsModel,
                NotesModel,
                UserClassQuestionsAnswersModel
            ] 
    constructor(@Inject('FormationClassesModel') private readonly formationClasses: typeof FormationClassesModel) { }

    async create(body: CreateFormationClass) {
        const uid = new ShortUniqueId();
         body.classCode = uid.randomUUID(9);
        const createFormationClasses = new FormationClassesModel(body);
        return await createFormationClasses.save();
    }
    async list() {
        return await this.formationClasses.findAll({
            include: this.include,
            where: { classStatus: true }
        });
    }
    async listAdmin() {
        return await this.formationClasses.findAll({
            include: this.include,
        });
    }
    async detail(id: string) {
        return await this.formationClasses.findByPk(id,{
            include: this.include,
            where: { classStatus: true }
        });
    }
    async detailAdmin(id: string) {
        return await this.formationClasses.findByPk(id, {
            include: this.include,
        });
    }
    async update(id: string, body: Partial<UpdateFormationClass>) {
        return await this.formationClasses.update(body, { where: { id }, returning: true });
    }
    async delete(id: string) {
        return this.formationClasses.destroy({ where: { id } });
    }

}
