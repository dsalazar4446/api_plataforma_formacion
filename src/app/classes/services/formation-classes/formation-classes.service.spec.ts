import { Test, TestingModule } from '@nestjs/testing';
import { FormationClassesService } from './formation-classes.service';

describe('FormationClassesService', () => {
  let service: FormationClassesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FormationClassesService],
    }).compile();

    service = module.get<FormationClassesService>(FormationClassesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
