import { Test, TestingModule } from '@nestjs/testing';
import { ClassSequencesHasTestsService } from './class-sequences-has-tests.service';

describe('ClassSequencesHasTestsService', () => {
  let service: ClassSequencesHasTestsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClassSequencesHasTestsService],
    }).compile();

    service = module.get<ClassSequencesHasTestsService>(ClassSequencesHasTestsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
