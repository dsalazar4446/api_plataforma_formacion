import { Injectable, Inject } from '@nestjs/common';
import { ClassSequencesHasTestsModel } from '../../models/class-sequences-has-tests.model';
import { CreateClassSequencesHasTestsDto } from '../../dto/class-sequences-has-tests.dto';
import { Op } from 'sequelize';

@Injectable()
export class ClassSequencesHasTestsService {
    constructor(@Inject('ClassSequencesHasTestsModel') private readonly classSequencesHasTests: typeof ClassSequencesHasTestsModel) {}

    async create(body: CreateClassSequencesHasTestsDto){
        const nuevo = new ClassSequencesHasTestsModel(body);
        return await nuevo.save();
    }

    async delete(id: string){
        return await this.classSequencesHasTests.destroy({where:{
            [Op.or]:[
                {
                    classSequenceId: id,
                },
                {
                    testId: id,
                }
            ]
        }});
    }
}
