import { Test, TestingModule } from '@nestjs/testing';
import { ClassHomeworksDetailsService } from './class-homeworks-details.service';

describe('ClassHomeworksDetailsService', () => {
  let service: ClassHomeworksDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClassHomeworksDetailsService],
    }).compile();

    service = module.get<ClassHomeworksDetailsService>(ClassHomeworksDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
