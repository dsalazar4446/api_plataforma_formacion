import { Injectable, Inject } from '@nestjs/common';
import { ClassHomeworkDetailsModel } from '../../models/class-homeworks-details.model';
import { UpdateClassHomeworksDetailsDto } from '../../dto/class-homeworks-details.dto';

@Injectable()
export class ClassHomeworksDetailsService {
    constructor(@Inject('ClassHomeworkDetailsModel') private readonly classHomeworkDetails: typeof ClassHomeworkDetailsModel) { }

    async create(body) {
        const createClassHomeworkDetails = new ClassHomeworkDetailsModel(body);
        return await createClassHomeworkDetails.save();
    }
    async list(languageType: string) {
        return await this.classHomeworkDetails.findAll({where: {languageType}});
    }
    async detail(id: string, languageType: string) {
        return await this.classHomeworkDetails.findByPk(id,{where: {languageType}});
    }
    async update(id: string, body: Partial<UpdateClassHomeworksDetailsDto>) {
        return await this.classHomeworkDetails.update(body, { where: { id } });
    }
    async delete(id: string) {
        return this.classHomeworkDetails.destroy({ where: { id } });
    }
}
