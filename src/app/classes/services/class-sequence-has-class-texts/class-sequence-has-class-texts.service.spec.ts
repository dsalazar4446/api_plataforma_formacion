import { Test, TestingModule } from '@nestjs/testing';
import { ClassSequenceHasClassTextsService } from './class-sequence-has-class-texts.service';

describe('ClassSequenceHasClassTextsService', () => {
  let service: ClassSequenceHasClassTextsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClassSequenceHasClassTextsService],
    }).compile();

    service = module.get<ClassSequenceHasClassTextsService>(ClassSequenceHasClassTextsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
