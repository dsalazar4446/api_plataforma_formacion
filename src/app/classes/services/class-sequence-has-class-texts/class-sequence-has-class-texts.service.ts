import { Injectable, Inject } from '@nestjs/common';
import { SequenceTextModel } from '../../models/class-sequence-has-class-texts.model';
import { Op } from 'sequelize';

@Injectable()
export class ClassSequenceHasClassTextsService {
    constructor(@Inject('SequenceTextModel') private readonly sequenceText: typeof SequenceTextModel) { }

    async create(body) {
        const createSequenceText = new SequenceTextModel(body);
        return await createSequenceText.save();
    }

    async delete(id: string) {
        return this.sequenceText.destroy({ where: { 
                [Op.or]: [
                    {
                        formationClassId: id,
                    },
                    {
                        classSequence: id,
                    }
                ]
            } 
        });
    }
}
