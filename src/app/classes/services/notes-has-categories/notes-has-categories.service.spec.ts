import { Test, TestingModule } from '@nestjs/testing';
import { NotesHasCategoriesService } from './notes-has-categories.service';

describe('NotesHasCategoriesService', () => {
  let service: NotesHasCategoriesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NotesHasCategoriesService],
    }).compile();

    service = module.get<NotesHasCategoriesService>(NotesHasCategoriesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
