import { Injectable, Inject } from '@nestjs/common';
import { CreateNotesHasCategories } from '../../dto/notes-has-categories.dto';
import { NotesHasCategoriesModel } from '../../models/notes-has-categories.model';
import { Op } from 'sequelize';

@Injectable()
export class NotesHasCategoriesService {
    constructor(@Inject('NotesHasCategoriesModel') private readonly notesHasCategories: typeof NotesHasCategoriesModel) 
    {}

    async create(body: CreateNotesHasCategories){
        const nuevo = new NotesHasCategoriesModel(body);
        return await nuevo.save();
    }

    delete(id: string){
        this.notesHasCategories.destroy({where:{
            [Op.or]:[
                {notesId: id},
                { categoryId: id}
            ]
            
        }})
    }
}
