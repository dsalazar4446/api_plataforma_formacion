import { Injectable, Inject } from '@nestjs/common';
import { QuestionAnswersReplysModel } from '../../models/question-answers-replys.model';
import { CreateQuestionAnswersReplys, UpdateQuestionAnswersReplys } from '../../dto/question-answers-replys.dto';

@Injectable()
export class QuestionAnswersReplysService {
    constructor(@Inject('QuestionAnswersReplysModel') private readonly QuestionAnswersReplys: typeof QuestionAnswersReplysModel ) {}

    async create(body: CreateQuestionAnswersReplys){
        const nuevo = new QuestionAnswersReplysModel(body);
        return await nuevo.save();
    }
    async list(){
        return await this.QuestionAnswersReplys.findAll()
    }
    async detail(id: string){
        return await this.QuestionAnswersReplys.findByPk(id);
    }
    async update(id: string ,body: Partial<UpdateQuestionAnswersReplys>){
        return await this.QuestionAnswersReplys.update(body, {where:{id}})
    }
    async delete(id: string){
        return await this.QuestionAnswersReplys.destroy({where:{id}});
    }
}
