import { Test, TestingModule } from '@nestjs/testing';
import { QuestionAnswersReplysService } from './question-answers-replys.service';

describe('QuestionAnswersReplysService', () => {
  let service: QuestionAnswersReplysService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [QuestionAnswersReplysService],
    }).compile();

    service = module.get<QuestionAnswersReplysService>(QuestionAnswersReplysService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
