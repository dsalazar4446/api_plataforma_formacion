import { Test, TestingModule } from '@nestjs/testing';
import { ClassSequencesHasClassHomeworksService } from './class-sequences-has-class-homeworks.service';

describe('ClassSequencesHasClassHomeworksService', () => {
  let service: ClassSequencesHasClassHomeworksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClassSequencesHasClassHomeworksService],
    }).compile();

    service = module.get<ClassSequencesHasClassHomeworksService>(ClassSequencesHasClassHomeworksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
