import { Injectable, Inject } from '@nestjs/common';
import { SequencesHomeworksModel } from '../../models/class-sequences-has-class-homeworks.model';
import { Op } from 'sequelize';

@Injectable()
export class ClassSequencesHasClassHomeworksService {
    constructor(@Inject('SequencesHomeworksModel') private readonly sequencesHomeworks: typeof SequencesHomeworksModel) { }

    async create(body) {
        const createSequencesHomeworks = new SequencesHomeworksModel(body);
        return await createSequencesHomeworks.save();
    }

    async delete(id: string) {
        return this.sequencesHomeworks.destroy({ where: { 
            [Op.or]: [
                {
                    classSequenceId: id,
                },
                {
                    classHomeworkId: id,
                }
            ]
         } });
    }
}
