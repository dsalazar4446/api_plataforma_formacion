import { Injectable, Inject } from "@nestjs/common";
import { NotesHasCategories } from "../../models/notes-has-categories.model";
import { NotesHasCategoriesSaveJson } from "../../dto/notes-has-categories.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services NotesHasCategoriesServices
 * @Creado 03 de MAyo 2019
 */

@Injectable()
export class NotesHasCategoriesServices {

    constructor(
        @Inject('NotesHasCategories') private readonly _notesHasCategoriesSrvcs: typeof NotesHasCategories
    ) { }

    async save(body: NotesHasCategoriesSaveJson): Promise<NotesHasCategoriesSaveJson> {
        return await new this._notesHasCategoriesSrvcs(body).save();
    }

    async list(): Promise<NotesHasCategoriesSaveJson[]> {
        return await this._notesHasCategoriesSrvcs.findAll();
    }

    async destroy(id: string): Promise<number> {
        return await this._notesHasCategoriesSrvcs.destroy({
            where: { id },
        });
    }
}