import { Injectable, Inject } from '@nestjs/common';
import { NotesResponses } from '../../models/notes-responses.model';
import { NotesResponsesSaveJson, NotesResponsesUpdateJson } from '../../dto/notes-responses.interface';
import { UserModel } from '../../../user/models/user.Model';
import { NotesModel } from '../../models/notes.model';
import { ValidateUUID } from '../../../shared/utils/validateUUID';

@Injectable()
export class NotesResponsesService {

    private include = {
        include: [UserModel, NotesModel]
    }

    constructor(@Inject('NotesResponses') private readonly _notesResponses: typeof NotesResponses,
    private readonly _validateUUID: ValidateUUID) { }

    async create(body: NotesResponsesSaveJson): Promise<NotesResponsesUpdateJson> {
        return await new this._notesResponses(body).save();
    }
    async list(): Promise<NotesResponsesUpdateJson[]> {
        return await this._notesResponses.findAll(this.include);
    }
    async detail(id: string): Promise<NotesResponsesUpdateJson> {

        this._validateUUID.validateUUIID(id);
        return await this._notesResponses.findByPk(id, this.include);
    }
    async update(id: string, body: Partial<NotesResponsesSaveJson>): Promise<[number, Array<NotesResponsesUpdateJson>]> {

        this._validateUUID.validateUUIID(id);
        return await this._notesResponses.update(body, { where: { id }, returning: true });
    }
    async delete(id: string) {

        this._validateUUID.validateUUIID(id);
        return await this._notesResponses.destroy({ where: { id } });
    }
}