import { Injectable, Inject } from '@nestjs/common';
import { NotesModel } from '../../models/notes.model';
import { NotesResponses } from '../../models/notes-responses.model';
import { ValidateUUID } from '../../../shared/utils/validateUUID';
import { Categories } from '../../../categories/models/categories.model';
import { CreateNotesDto } from '../../dto/notes.dto';

@Injectable()
export class NotesService {

    private include = [
            {
                model: NotesResponses
            },
            {
                model: Categories
            }
        ]
    

    constructor(@Inject('NotesModel') private readonly notes: typeof NotesModel,
    private readonly _validateUUID: ValidateUUID
    ) { }

    async create(body) {
        const createClassText = new NotesModel(body);
        return await createClassText.save();
    }
    async list() {
        return await this.notes.findAll({include: this.include});
    }
    async listLanguage(languageType: string) {
        return await this.notes.findAll({include: this.include, where: {languageType}});
    }
    async detail(id: string, languageType: string): Promise<any> {

        this._validateUUID.validateUUIID(id);
        return await this.notes.findByPk(id, {include: this.include, where:{languageType}});
    }
    async update(id: string, body: Partial<CreateNotesDto>) {

        this._validateUUID.validateUUIID(id);
        return await this.notes.update(body, { where: { id }, returning: true });
    }
    async delete(id: string) {

        this._validateUUID.validateUUIID(id);
        return this.notes.destroy({ where: { id } });
    }
}
