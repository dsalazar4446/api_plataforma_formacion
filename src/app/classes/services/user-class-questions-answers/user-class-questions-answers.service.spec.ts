import { Test, TestingModule } from '@nestjs/testing';
import { UserClassQuestionsAnswersService } from './user-class-questions-answers.service';

describe('UserClassQuestionsAnswersService', () => {
  let service: UserClassQuestionsAnswersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserClassQuestionsAnswersService],
    }).compile();

    service = module.get<UserClassQuestionsAnswersService>(UserClassQuestionsAnswersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
