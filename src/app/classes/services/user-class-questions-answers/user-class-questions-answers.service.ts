import { Injectable, Inject } from '@nestjs/common';
import { CreateUserClassQuestionAnswer, UpdateUserClassQuestionAnswer } from '../../dto/user-class-questions-answers.dto';
import { UserClassQuestionsAnswersModel } from '../../models/user-class-questions-answers.model';
import { QuestionAnswersReplysModel } from '../../models/question-answers-replys.model';

const include = [
    {
         model: QuestionAnswersReplysModel,
         as: 'replysParent',
    },
    {
         model: QuestionAnswersReplysModel,
         as: 'replysChildren',
    }
];

@Injectable()
export class UserClassQuestionsAnswersService {
    constructor(
        @Inject('UserClassQuestionsAnswersModel') 
        private readonly userClassQuestionsAnswers: typeof UserClassQuestionsAnswersModel
    ) {}

    async create(body: CreateUserClassQuestionAnswer){
        const nuevo = new UserClassQuestionsAnswersModel(body);
        return await nuevo.save();
    }
    async list(){
        return await this.userClassQuestionsAnswers.findAll({ include });
    }   
    async detail(id: string){
        return await this.userClassQuestionsAnswers.findByPk(id,{ include });
    }
    async update(id: string, body: Partial<UpdateUserClassQuestionAnswer>){
        return await this.userClassQuestionsAnswers.update(body, {where:{id},returning: true})
    }
    async delete(id: string){
        return await this.userClassQuestionsAnswers.destroy({where:{id}});
    }
}
