import { Injectable, Inject } from '@nestjs/common';
import { ClassProblemsModel } from '../../models/class-problems.model';
import { CreateClassProblemsDto, UpdateClassProblemsDto } from '../../dto/class-problems.dto';

@Injectable()
export class ClassProblemsService {
    constructor(@Inject('ClassProblemsModel') private readonly classProblems: typeof ClassProblemsModel) {}

    async create(body: CreateClassProblemsDto) {
        const newClassProblem = new ClassProblemsModel(body);
        return await newClassProblem.save()
    }
    async list() {
        return await this.classProblems.findAll();
    }
    async detail(id: string) {
        return await this.classProblems.findByPk(id);
    }

    async update(id: string, body: Partial<UpdateClassProblemsDto>) {    
        return await this.classProblems.update( body,{where:{id}});
    }

    async delete(id: string) {
        const result = await this.classProblems.destroy({where:{id}})
    }
}
