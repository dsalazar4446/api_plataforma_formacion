import { Test, TestingModule } from '@nestjs/testing';
import { ClassProblemsService } from './class-problems.service';

describe('ClassProblemsService', () => {
  let service: ClassProblemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClassProblemsService],
    }).compile();

    service = module.get<ClassProblemsService>(ClassProblemsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
