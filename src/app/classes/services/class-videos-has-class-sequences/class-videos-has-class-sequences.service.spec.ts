import { Test, TestingModule } from '@nestjs/testing';
import { ClassVideosHasClassSequencesService } from './class-videos-has-class-sequences.service';

describe('ClassVideosHasClassSequencesService', () => {
  let service: ClassVideosHasClassSequencesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClassVideosHasClassSequencesService],
    }).compile();

    service = module.get<ClassVideosHasClassSequencesService>(ClassVideosHasClassSequencesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
