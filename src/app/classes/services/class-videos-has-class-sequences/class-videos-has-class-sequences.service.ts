import { Injectable, Inject } from '@nestjs/common';
import { VideosSequencesModel } from '../../models/class-videos-has-class-sequences.model';
import { Op } from 'sequelize';

@Injectable()
export class ClassVideosHasClassSequencesService {

    constructor(@Inject('VideosSequencesModel') private readonly videosSequences: typeof VideosSequencesModel) { }

    async create(body) {
        const createVideosSequences = new VideosSequencesModel(body);
        return await createVideosSequences.save();
    }

    async delete(id: string) {
        return this.videosSequences.destroy({
            where: {
                [Op.or]: [
                    { classVideosId: id },
                    { classSequencesId: id }
                ] } });
    }
    
}
