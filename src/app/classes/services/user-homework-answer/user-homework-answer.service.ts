import { Injectable, Inject } from '@nestjs/common';
import { UserHomeworkAnswersModel } from '../../models/user-homework-answer.model';
import { CreateUserHomeworkAnswerDto, UpdateUserHomeworkAnswerDto } from '../../dto/user-homework-answer.tdo';
import { CONFIG } from '../../../../config';
@Injectable()
export class UserHomeworkAnswerService {
    constructor(@Inject('UserHomeworkAnswersModel') private readonly userHomeworkAnswers: typeof UserHomeworkAnswersModel) { }

    async create(body: CreateUserHomeworkAnswerDto) {
        const createUserHomeworkAnswers = new UserHomeworkAnswersModel(body);
        return await createUserHomeworkAnswers.save();
    }
    async list() {
        const result = await this.userHomeworkAnswers.findAll();
        if(result){
            result.map(homeworkAnswer => {
                homeworkAnswer.userHomeworkAttachments = CONFIG.storage.server + 'classes/userHomeworkAttachments/' + homeworkAnswer.userHomeworkAttachments;
            });
        }
        return result;
    }
    async detail(id: string) {
        const result = await this.userHomeworkAnswers.findByPk(id);
        if(result){
            result.userHomeworkAttachments = CONFIG.storage.server + 'classes/userHomeworkAttachments/' + result.userHomeworkAttachments;
        }
        return result;
    }
    async update(id: string, body: Partial<UpdateUserHomeworkAnswerDto>) {
        const result = await this.userHomeworkAnswers.update(body, { where: { id } });

        if(result){
            result[0][1].userHomeworkAttachments = CONFIG.storage.server + 'classes/userHomeworkAttachments/' + result[0][1].userHomeworkAttachments;
        }

        return result[0][1];
    }
    async delete(id: string) {
        return this.userHomeworkAnswers.destroy({ where: { id } });
    }
}
