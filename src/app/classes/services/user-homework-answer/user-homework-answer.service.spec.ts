import { Test, TestingModule } from '@nestjs/testing';
import { UserHomeworkAnswerService } from './user-homework-answer.service';

describe('UserHomeworkAnswerService', () => {
  let service: UserHomeworkAnswerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserHomeworkAnswerService],
    }).compile();

    service = module.get<UserHomeworkAnswerService>(UserHomeworkAnswerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
