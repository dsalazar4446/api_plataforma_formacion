import { Injectable, Inject } from '@nestjs/common';
import { SequencesAttachmentsModel } from '../../models/cl-sequences-h-cl-attachments.model';
import { Op } from 'sequelize';

@Injectable()
export class ClSequencesHClAttachmentsService {
    constructor(@Inject('SequencesAttachmentsModel') private readonly sequenceAttachment: typeof SequencesAttachmentsModel) {}

    async create(body){
        const createSequencesAttachments = new SequencesAttachmentsModel(body);
        return await createSequencesAttachments.save();
    }

    async delete(id: string){
        return this.sequenceAttachment.destroy({where: {
            [Op.or]:[
                {
                    classSequenceId: id
                },
                {
                    classAttachmentId: id  
                }
            ]
        }});
    }
}
