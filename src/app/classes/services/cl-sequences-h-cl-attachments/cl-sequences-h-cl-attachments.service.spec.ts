import { Test, TestingModule } from '@nestjs/testing';
import { ClSequencesHClAttachmentsService } from './cl-sequences-h-cl-attachments.service';

describe('ClSequencesHClAttachmentsService', () => {
  let service: ClSequencesHClAttachmentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClSequencesHClAttachmentsService],
    }).compile();

    service = module.get<ClSequencesHClAttachmentsService>(ClSequencesHClAttachmentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
