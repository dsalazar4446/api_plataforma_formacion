import { Test, TestingModule } from '@nestjs/testing';
import { ClassAttachmentsService } from './class-attachments.service';

describe('ClassAttachmentsService', () => {
  let service: ClassAttachmentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClassAttachmentsService],
    }).compile();

    service = module.get<ClassAttachmentsService>(ClassAttachmentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
