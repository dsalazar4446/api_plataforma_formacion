import { Injectable, Inject } from '@nestjs/common';
import { ClassAttachmentsModel } from '../../models/class-attachments.model';
import { ClassSequencesModel } from '../../models/class-sequences.model';
import { CONFIG } from '../../../../config';

@Injectable()
export class ClassAttachmentsService {
    constructor(@Inject('ClassAttachmentsModel') private readonly classAttachments: typeof ClassAttachmentsModel) { }

    async create(body) {
        const createClassAttachments = new ClassAttachmentsModel(body);
        return await createClassAttachments.save();
    }
    async list(){
        const result = await this.classAttachments.findAll({
            include: [ClassSequencesModel]
        });
        if(result)
        result.map(attachment => {
            attachment.classAttachment = CONFIG.storage.server + 'classes/classAttachment/' + attachment.classAttachment;
        }) 
        
        return result;
    }
    
    async detail(id: string){
        const result = await this.classAttachments.findByPk(id);
        if(result){
            result.classAttachment = CONFIG.storage.server + 'classes/user-homework-answer/' + result.classAttachment;
        }
        return result;
    }
    
    async update(id: string, body) {
        const result = await this.classAttachments.update(body, { where: { id }, returning: true });
        if (result){
            result[0][1].classAttachment = CONFIG.storage.server + 'classes/user-homework-answer/' + result[1][0].classAttachment;
        }
        return result[0][1];
    }
    
    async delete(id: string) {
        return this.classAttachments.destroy({ where: { id } });
    }
}
