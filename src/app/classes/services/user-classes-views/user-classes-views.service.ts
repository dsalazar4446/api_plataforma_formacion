import { Injectable, Inject } from '@nestjs/common';
import { UserClassesViewsModel } from '../../models/user-classes-views.model';
import { CreateUserClassesViews, UpdateUserClassesViews } from '../../dto/user-classes-views.dto';

@Injectable()
export class UserClassesViewsService {
    constructor(@Inject('UserClassesViewsModel') private readonly userClassesViews: typeof UserClassesViewsModel) { }

    async create(body: CreateUserClassesViews) {
        const nuevo = new UserClassesViewsModel(body);
        return await nuevo.save();
    }

    async list() {
        return await this.userClassesViews.findAll();
    }

    async detail(id: string) {
        return await this.userClassesViews.findByPk(id)
    }

    async update(id: string, body: Partial<UpdateUserClassesViews>) {
        return await this.userClassesViews.update(body, { where: { id }, returning: true })
    }

    async delete(id: string) {
        return await this.userClassesViews.destroy({ where: { id } })
    }

}
