import { Test, TestingModule } from '@nestjs/testing';
import { UserClassesViewsService } from './user-classes-views.service';

describe('UserClassesViewsService', () => {
  let service: UserClassesViewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserClassesViewsService],
    }).compile();

    service = module.get<UserClassesViewsService>(UserClassesViewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
