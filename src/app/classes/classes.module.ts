import { Module } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { SequencesAttachmentsModel } from './models/cl-sequences-h-cl-attachments.model';
import { ClassAttachmentsModel } from './models/class-attachments.model';
import { ClassHomeworkDetailsModel } from './models/class-homeworks-details.model';
import { ClassHomeworksModel } from './models/class-homeworks.model';
import { SequenceTextModel } from './models/class-sequence-has-class-texts.model';
import { SequencesHomeworksModel } from './models/class-sequences-has-class-homeworks.model';
import { ClassSequencesModel } from './models/class-sequences.model';
import { ClassTextModel } from './models/class-text.model';
import { ClassTitlesModel } from './models/class-titles.model';
import { VideosSequencesModel } from './models/class-videos-has-class-sequences.model';
import { FormationClassesModel } from './models/formation-classes.model';
import { UserHomeworkAnswersModel } from './models/user-homework-answer.model';
import { ClSequencesHClAttachmentsService } from './services/cl-sequences-h-cl-attachments/cl-sequences-h-cl-attachments.service';
import { ClassAttachmentsService } from './services/class-attachments/class-attachments.service';
import { ClassHomeworksDetailsService } from './services/class-homeworks-details/class-homeworks-details.service';
import { ClassHomeworksService } from './services/class-homeworks/class-homeworks.service';
import { ClassSequenceHasClassTextsService } from './services/class-sequence-has-class-texts/class-sequence-has-class-texts.service';
import { ClassSequencesHasClassHomeworksService } from './services/class-sequences-has-class-homeworks/class-sequences-has-class-homeworks.service';
import { ClassSequencesService } from './services/class-sequences/class-sequences.service';
import { ClassTextService } from './services/class-text/class-text.service';
import { ClassTitlesService } from './services/class-titles/class-titles.service';
import { ClassVideosHasClassSequencesService } from './services/class-videos-has-class-sequences/class-videos-has-class-sequences.service';
import { FormationClassesService } from './services/formation-classes/formation-classes.service';
import { UserHomeworkAnswerService } from './services/user-homework-answer/user-homework-answer.service';
import { ClassAttachmentsController } from './controllers/class-attachments/class-attachments.controller';
import { ClassHomeworksController } from './controllers/class-homeworks/class-homeworks.controller';
import { ClassHomeworksDetailsController } from './controllers/class-homeworks-details/class-homeworks-details.controller';
import { ClassSequencesController } from './controllers/class-sequences/class-sequences.controller';
import { ClassTextController } from './controllers/class-text/class-text.controller';
import { ClassTitlesController } from './controllers/class-titles/class-titles.controller';
import { FormationClassesController } from './controllers/formation-classes/formation-classes.controller';
import { UserHomeworkAnswerController } from './controllers/user-homework-answer/user-homework-answer.controller';
import { NotesService } from './services/notes/notes.service';
import { NotesController } from './controllers/notes/notes.controller';
import { NotesModel } from './models/notes.model';
import { NotesResponses } from './models/notes-responses.model';
import { NotesResponsesService } from './services/notes/notes-responses.service';
import { NotesResponsesController } from './controllers/notes/notes-responses.controller';
import { ClassProblemsModel } from './models/class-problems.model';
import { ClassProblemsService } from './services/class-problems/class-problems.service';
import { ClassProblemsController } from './controllers/class-problems/class-problems.controller';
import { ClSequencesHClAttachmentsController } from './controllers/cl-sequences-h-cl-attachments/cl-sequences-h-cl-attachments.controller';
import { ClassSequencesHasTestsService } from './services/class-sequences-has-tests/class-sequences-has-tests.service';
import { ClassSequencesHasTestsController } from './controllers/class-sequences-has-tests/class-sequences-has-tests.controller';
import { ClassVideosHasClassSequencesController } from './controllers/class-videos-has-class-sequences/class-videos-has-class-sequences.controller';
import { ClassSequenceHasClassTextsController } from './controllers/class-sequence-has-class-texts/class-sequence-has-class-texts.controller';
import { UserClassQuestionsAnswersController } from './controllers/user-class-questions-answers/user-class-questions-answers.controller';
import { UserClassQuestionsAnswersService } from './services/user-class-questions-answers/user-class-questions-answers.service';
import { UserClassQuestionsAnswersModel } from './models/user-class-questions-answers.model';
import { QuestionAnswersReplysModel } from './models/question-answers-replys.model';
import { ClassSequencesHasTestsModel } from './models/class-sequences-has-tests.model';
import { NotesHasCategoriesService } from './services/notes-has-categories/notes-has-categories.service';
import { NotesHasCategoriesController } from './controllers/notes-has-categories/notes-has-categories.controller';
import { NotesHasCategoriesModel } from './models/notes-has-categories.model';
import { ClassSequencesHasClassHomeworksController } from './controllers/class-sequences-has-class-homeworks/class-sequences-has-class-homeworks.controller';
import { UserVideoRecordsService } from './services/user-video-records/user-video-records.service';
import { UserVideoRecordsController } from './controllers/user-video-records/user-video-records.controller';
import { UserVideoRecordsModel } from './models/user-video-records.model';
import { UserClassesViewsModel } from './models/user-classes-views.model';
import { UserClassesViewsController } from './controllers/user-classes-views/user-classes-views.controller';
import { UserClassesViewsService } from './services/user-classes-views/user-classes-views.service';
import { UserVideoFavoritesController } from './controllers/user-video-favorites/user-video-favorites.controller';
import { UserVideoFavoritesService } from './services/user-video-favorites/user-video-favorites.service';
import { UserVideoFavoritesModel } from './models/user-video-favorites.model';
import { QuestionAnswersReplysController } from './controllers/question-answers-replys/question-answers-replys.controller';
import { QuestionAnswersReplysService } from './services/question-answers-replys/question-answers-replys.service'

const models = [
    SequencesAttachmentsModel,
    ClassAttachmentsModel,
    ClassHomeworkDetailsModel,
    ClassHomeworksModel,
    SequenceTextModel,
    SequencesHomeworksModel,
    ClassSequencesModel,
    ClassTextModel,
    ClassTitlesModel,
    VideosSequencesModel,
    FormationClassesModel,
    UserHomeworkAnswersModel,
    NotesModel,
    NotesResponses,
    ClassProblemsModel,
    UserClassQuestionsAnswersModel,
    QuestionAnswersReplysModel, 
    ClassSequencesHasTestsModel,
    NotesHasCategoriesModel,
    UserVideoRecordsModel,
    UserClassesViewsModel,
    UserVideoFavoritesModel
]

@Module({
    imports: [
        SharedModule,
        DatabaseModule.forRoot({
            sequelize:{
                models,
            },
            loki: false,
        })
    ],
    providers: [
        ClSequencesHClAttachmentsService, 
        ClassAttachmentsService, 
        ClassHomeworksDetailsService, 
        ClassHomeworksService, 
        ClassSequenceHasClassTextsService, 
        ClassSequencesHasClassHomeworksService, 
        ClassSequencesService, 
        ClassTextService, 
        ClassTitlesService, 
        ClassVideosHasClassSequencesService, 
        FormationClassesService, 
        UserHomeworkAnswerService, 
        NotesService, 
        NotesResponsesService, 
        ClassProblemsService, 
        ClassSequencesHasTestsService, 
        UserClassQuestionsAnswersService, 
        NotesHasCategoriesService, 
        UserVideoRecordsService,
        UserClassesViewsService, 
        UserVideoFavoritesService,
        QuestionAnswersReplysService
    ],
    controllers: [
        ClassAttachmentsController, 
        ClassHomeworksController, 
        ClassHomeworksDetailsController, 
        ClassSequencesController, 
        ClassTextController, 
        ClassTitlesController, 
        FormationClassesController, 
        UserHomeworkAnswerController, 
        NotesController, 
        NotesResponsesController, 
        ClassProblemsController, 
        ClSequencesHClAttachmentsController, 
        ClassSequencesHasTestsController, 
        ClassVideosHasClassSequencesController, 
        ClassSequenceHasClassTextsController, 
        UserClassQuestionsAnswersController, 
        NotesHasCategoriesController, 
        ClassSequencesHasClassHomeworksController, 
        UserVideoRecordsController, 
        UserClassesViewsController, 
        UserVideoFavoritesController,
        QuestionAnswersReplysController
    ],
})
export class ClassesModule {}
