import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString, IsOptional, IsBoolean } from "class-validator";

export class CreateNotesDto {
    @ApiModelProperty()
    @IsUUID('4')
    userId: string;
    @ApiModelProperty()
    @IsUUID('4')
    classId: string;
    @ApiModelProperty()
    @IsString()
    noteText: string;
    @ApiModelProperty()
    @IsBoolean()
    makePublic: boolean;
    @ApiModelProperty()
    @IsOptional()
    languageType:string;
}

export class UpdateNotesDto extends CreateNotesDto {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
} 