import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString } from "class-validator";

export class CreateUserVideoFavorites {
    @ApiModelProperty()
    @IsUUID('4')
    classVideoId: string;

    @ApiModelProperty()
    @IsUUID('4')
    userId: string;
    @ApiModelProperty()

    @IsString()
    video_time: Date;
    
    @ApiModelProperty()
    @IsString()
    favorite_text: string;
}

export class UpdateUserVideoFavorites extends CreateUserVideoFavorites {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}