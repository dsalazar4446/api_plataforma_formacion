import { ApiModelProperty } from "@nestjs/swagger";
import { IsOptional, IsUUID, IsString, IsBoolean } from "class-validator";

export class CreateFormationClass {
    @ApiModelProperty()
    @IsUUID('4')
    lessonId: string;
    @ApiModelProperty()
    @IsOptional()
    @IsString()
    classCode?: string;
    @ApiModelProperty()
    @IsBoolean()
    classStatus: boolean;
}

export class UpdateFormationClass extends CreateFormationClass {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}   