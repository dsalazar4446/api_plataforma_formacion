import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID } from "class-validator";

export class CreateSequencesHomeworksDto {
    @ApiModelProperty()
    @IsUUID('4')
    classSequenceId: string;
    @ApiModelProperty()
    @IsUUID('4')
    classHomeworkId: string;
}