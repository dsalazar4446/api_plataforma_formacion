import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString, IsNotEmpty } from "class-validator";

export class NotesResponsesSaveJson {
    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    userId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'notesId property must a be uuid'})
    @IsNotEmpty({message: 'notesId property not must null'})
    notesId: string;

    @ApiModelProperty()
    @IsString({message: 'noteMessage property must a be string'})
    @IsNotEmpty({message: 'noteMessage property not must null'})
    noteMessage: string;

    @ApiModelProperty()
    @IsString({message: 'noteMessageStatus property must a be string'})
    @IsNotEmpty({message: 'noteMessageStatus property not must null'})
    noteMessageStatus: string
}

export class NotesResponsesUpdateJson extends NotesResponsesSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
} 