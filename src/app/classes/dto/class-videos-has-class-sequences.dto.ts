import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID } from "class-validator";

export class CreateVideosSequencesDto {
    @ApiModelProperty()
    @IsUUID('4')
    classVideosId: string;
    @ApiModelProperty()
    @IsUUID('4')
    classSequencesId: string;
}