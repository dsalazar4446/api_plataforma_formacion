import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsNumber } from "class-validator";

export class CreateClassSequences {
    @ApiModelProperty()
    @IsUUID('4')
    formationClassId: string;
    @ApiModelProperty()
    @IsNumber()
    classSequence: number;
}

// tslint:disable-next-line: max-classes-per-file
export class UpdateClassSequences extends CreateClassSequences {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}