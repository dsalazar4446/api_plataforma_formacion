import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsUUID } from "class-validator";

export class CreateClassAttachmentsDto {
    @ApiModelProperty()
    @IsString()
    classAttachmentType: string;
    @ApiModelProperty()
    @IsString()
    classAttachment: string;
}

// tslint:disable-next-line: max-classes-per-file
export class UpdateClassAttachmentsDto extends CreateClassAttachmentsDto {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}