import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID } from "class-validator";

export class CreateClassSequencesHasTestsDto {
    @ApiModelProperty()
    @IsUUID('4')
    classSequenceId: string;
    @ApiModelProperty()
    @IsUUID('4')
    testId: string;
}