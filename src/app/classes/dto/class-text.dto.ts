import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsUUID, IsOptional } from "class-validator";

export class CreateClassTextDto {
    @ApiModelProperty()
    @IsString()
    classDescription: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    languageType: string;
}

// tslint:disable-next-line: max-classes-per-file
export class UpdateClassTextDto extends CreateClassTextDto{
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}