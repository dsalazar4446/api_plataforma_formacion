import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString, IsNumber } from "class-validator";

export class CreateUserHomeworkAnswerDto {
    @ApiModelProperty()
    @IsUUID('4')
    usersId: string;
    @ApiModelProperty()
    @IsString()
    classHomeworkId: string;
    @ApiModelProperty()
    @IsString()
    userHomeworkAnswer: string;
    @ApiModelProperty()
    @IsNumber()
    userHomeworkAnswerAproval: number;
    @ApiModelProperty()
    @IsString()
    userHomeworkAttachments: string;
}

export class UpdateUserHomeworkAnswerDto extends CreateUserHomeworkAnswerDto {
    @ApiModelProperty()
    @IsUUID('4')
    id: string ;
}