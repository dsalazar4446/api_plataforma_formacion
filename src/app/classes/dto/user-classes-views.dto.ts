import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString } from "class-validator";

export class CreateUserClassesViews {
    @ApiModelProperty()
    @IsString()
    userId: string;
    @ApiModelProperty()
    @IsString()
    classSequenceId: string;
    @ApiModelProperty()
    @IsString()
    startDateTime: Date;
    @ApiModelProperty()
    @IsString()
    endDateTime: Date;
}

export class UpdateUserClassesViews extends CreateUserClassesViews {
    @ApiModelProperty()
    @IsUUID('4')
    id: string
}