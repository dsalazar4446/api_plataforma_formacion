import { IsUUID } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateSequencesAttachmentsDto {
    @ApiModelProperty()
    @IsUUID('4')
    classSequenceId: string
    @ApiModelProperty()
    @IsUUID('4')
    classAttachmentId: string
}