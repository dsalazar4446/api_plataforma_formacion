import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID } from "class-validator";

export class NotesHasCategoriesSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    noteId: string;
    @ApiModelProperty()
    @IsUUID('4')
    categoryId: string;
}