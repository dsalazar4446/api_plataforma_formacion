import { ApiModelProperty } from "@nestjs/swagger";
import { IsOptional, IsUUID } from "class-validator";

export class CreateClassHomeworksDto {
    @ApiModelProperty()
    @IsOptional()
    homeworkCode?: string
}

// tslint:disable-next-line: max-classes-per-file
export class UpdateClassHomeworksDto extends CreateClassHomeworksDto {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}