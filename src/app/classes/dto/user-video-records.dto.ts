import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString } from "class-validator";

export class CreateUserVideoRecords {
    @ApiModelProperty()
    @IsUUID('4')
    userId: string;

    @ApiModelProperty()
    @IsUUID('4')
    classVideoId: string;

    @ApiModelProperty()
    @IsString()
    timeViewed: string;
}

export class UpdateUserVideoRecords extends CreateUserVideoRecords{
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}