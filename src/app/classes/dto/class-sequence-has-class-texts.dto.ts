import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID } from "class-validator";

export class CreateSequenceTextsDto {
    @ApiModelProperty()
    @IsUUID('4')
    classSequencesId: string;
    @ApiModelProperty()
    @IsUUID('4')
    classTextsId: string;
}
