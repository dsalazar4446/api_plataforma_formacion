import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID } from "class-validator";

export class CreateQuestionAnswersReplys {
    @ApiModelProperty()
    @IsUUID('4')
    parentId: string;

    @ApiModelProperty()
    @IsUUID('4')
    childrenId: string;

}

export class UpdateQuestionAnswersReplys extends CreateQuestionAnswersReplys {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}