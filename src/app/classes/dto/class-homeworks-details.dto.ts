import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString, IsOptional } from "class-validator";

export class CreateClassHomeworksDetailsDto {
    @ApiModelProperty()
    @IsUUID('4')
    classHomeworksId: string;

    @ApiModelProperty()
    @IsString()
    homeworkTitle: string;

    @ApiModelProperty()
    @IsString()
    homeworkDescription: string;

    @ApiModelProperty()
    @IsString()
    @IsOptional()
    languageType: string;
}

// tslint:disable-next-line: max-classes-per-file
export class UpdateClassHomeworksDetailsDto extends CreateClassHomeworksDetailsDto { 
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}