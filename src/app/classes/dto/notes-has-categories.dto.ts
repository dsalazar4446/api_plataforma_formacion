import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID } from "class-validator";

export class CreateNotesHasCategories {
    @ApiModelProperty()
    @IsUUID('4')
    categoryId: string;
    @ApiModelProperty()
    @IsUUID('4')
    noteId: string;    
}