import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsNumber, IsUUID, IsBoolean } from "class-validator";

/**
 * @author Daniel Salazar
 * @description Objeto de transferencia de datos para class_problems
 * @type class
 */

export class CreateClassProblemsDto{
    @ApiModelProperty()
    @IsUUID('4')
    public userId: string;
    @ApiModelProperty()
    @IsUUID('4')
    public classId: string
    @ApiModelProperty()
    @IsString()
    public problemType: string;
    @ApiModelProperty()
    @IsString()
    public problemExplanation: string;
    @ApiModelProperty()
    @IsBoolean()
    public problemStatus: boolean;
}

export class UpdateClassProblemsDto extends CreateClassProblemsDto {
    @ApiModelProperty()
    @IsUUID('4')
    public id: string;
}