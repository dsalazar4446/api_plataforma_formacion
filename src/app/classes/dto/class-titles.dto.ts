import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString, IsOptional } from "class-validator";

export class CreateClassTitlesDto {
    @ApiModelProperty()
    @IsUUID('4')
    formationClassId: string;
    @ApiModelProperty()
    @IsString()
    classTitle: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    languageType: string;
}

// tslint:disable-next-line: max-classes-per-file
export class UpdateClassTitlesDto extends CreateClassTitlesDto{
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}