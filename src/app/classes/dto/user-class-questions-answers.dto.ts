import { IsUUID, IsString, IsBoolean } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateUserClassQuestionAnswer {
    @ApiModelProperty()
    @IsUUID('4')
    public formationClassId: string;
    @ApiModelProperty()
    @IsUUID('4')
    public userId: string;
    @ApiModelProperty()
    @IsString()
    public questionAnswer: string;
    @ApiModelProperty()
    @IsBoolean()
    public usefull: boolean;
    @ApiModelProperty()
    @IsBoolean()
    public questionAnswerStatus: boolean;

}

export class UpdateUserClassQuestionAnswer {
    
    @ApiModelProperty()
    @IsUUID('4')
    id: string
}