import { Model, Table, Column, DataType, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from "sequelize-typescript";
import { MainVideosModel } from "src/app/main-videos/models/main-videos.model";
import { UserModel } from "src/app/user/models/user.Model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";
@Table({ tableName: 'user_video_records'})
export class UserVideoRecordsModel extends Model<UserVideoRecordsModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => MainVideosModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'class_video_id',
    })
    classVideoId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TIME,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'time_viewed',
    })
    timeViewed: string;

    @BelongsTo(() => UserModel)    
    user: UserModel;
    @BelongsTo(() => MainVideosModel)    
    mainVideos: MainVideosModel;

    @CreatedAt created_at: Date;
    
    @UpdatedAt updated_at: Date
}