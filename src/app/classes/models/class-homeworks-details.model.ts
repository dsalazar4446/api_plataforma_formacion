import { Model, Table, Column, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt } from 'sequelize-typescript';
import { ClassHomeworksModel } from './class-homeworks.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({ tableName: 'class_homework_details'})
export class ClassHomeworkDetailsModel extends Model<ClassHomeworkDetailsModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    
    @ApiModelPropertyOptional()
    @ForeignKey(() => ClassHomeworksModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'class_homework_id',
    })
    classHomeworksId: string;

    @BelongsTo(() => ClassHomeworksModel)
    homeworksModel: ClassHomeworksModel;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'homework_title',
    })
    homeworkTitle: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'homework_description',
    })
    homeworkDescription: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;

}