import { Model, Table, PrimaryKey, AutoIncrement, AllowNull, Validate, Unique, Column, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt, HasMany, BelongsToMany } from "sequelize-typescript";
import { UserModel } from "../../user/models/user.Model";
import { FormationClassesModel } from "./formation-classes.model";
import { NotesResponses } from "./notes-responses.model";
import { Categories } from "../../categories/models/categories.model";
import { NotesHasCategoriesModel } from "./notes-has-categories.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";
import { categoriesArrayExmple } from "src/app/categories/example/categories.example";
import { NotesResponsesArrayExample } from "../examples/notes-responses.example";
@Table({tableName: 'notes'})
export class NotesModel extends Model<NotesModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => FormationClassesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'class_id',
    })
    classId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'note_text',
    })
    noteText: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'make_public',
    })
    makePublic: boolean;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;

    @BelongsTo(() => UserModel)
    user: UserModel;
    
    @BelongsTo(() => FormationClassesModel)
    formationClassesModel: FormationClassesModel;
    
    @ApiModelPropertyOptional({example: NotesResponsesArrayExample})
    @HasMany(() => NotesResponses)
    notesResponses: NotesResponses;

    
    @ApiModelPropertyOptional({example: categoriesArrayExmple})
    @BelongsToMany(() => Categories, () => NotesHasCategoriesModel)
    categories: Categories
    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date

} 