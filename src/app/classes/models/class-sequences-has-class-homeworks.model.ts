import { Model, Table, PrimaryKey, AutoIncrement, AllowNull, Validate, Unique, Column, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt } from "sequelize-typescript";
import { ClassSequencesModel } from "./class-sequences.model";
import { ClassHomeworksModel } from "./class-homeworks.model";
@Table({ tableName: 'class_sequences_has_class_homeworks'})
export class SequencesHomeworksModel extends Model<SequencesHomeworksModel> {
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ForeignKey(() => ClassSequencesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'class_sequence_id',
    })
    classSequenceId: string;

    @BelongsTo(() => ClassSequencesModel)
    classSequences: ClassSequencesModel

    @ForeignKey(() => ClassHomeworksModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'class_homework_id',
    })
    classHomeworkId: string;

    @BelongsTo(() => ClassHomeworksModel)
    classHomeworks: ClassHomeworksModel

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;
}