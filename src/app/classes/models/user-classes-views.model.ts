import { Table, Model, DataType, Column, ForeignKey, BelongsTo, CreatedAt, UpdatedAt } from "sequelize-typescript";
import { UserModel } from "../../user/models/user.Model";
import { ClassSequencesModel } from "./class-sequences.model";

@Table({ tableName: 'user_classes_views'})
export class UserClassesViewsModel extends Model<UserClassesViewsModel> {
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;

    @BelongsTo(() => UserModel)
    user: UserModel;

    @ForeignKey(() => ClassSequencesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'class_sequences_id',
    })
    classSequencesId: string;

    @BelongsTo(() => ClassSequencesModel)
    classSequences: ClassSequencesModel;

    @Column({
        type: DataType.DATE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'start_date_time',
    })
    startDateTime: Date;

    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'end_date_time',
    })
    endDateTime: Date;

    @CreatedAt created_at: Date;

    @UpdatedAt updated_at: Date 
}