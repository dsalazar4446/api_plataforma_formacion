import { Model, Table, Column, DataType, CreatedAt, UpdatedAt, HasMany, BelongsToMany } from 'sequelize-typescript';
import { ClassHomeworkDetailsModel } from './class-homeworks-details.model';
import { UserHomeworkAnswersModel } from './user-homework-answer.model';
import { SequencesHomeworksModel } from './class-sequences-has-class-homeworks.model';
import { ClassSequencesModel } from './class-sequences.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { ClassSequencesArrayExample } from '../examples/class-sequences.example';
import { UserHomeworkAnswerArrayExample } from '../examples/user-class-homework-answer.example';
import { ClassHomeworkDetailsArrayExample } from '../examples/class-homework-detail';
@Table({ tableName: 'class_homeworks'})
export class ClassHomeworksModel extends Model<ClassHomeworksModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'homework_code',
    })
    homeworkCode: string
    @ApiModelPropertyOptional({example: ClassHomeworkDetailsArrayExample})
    @HasMany(() => ClassHomeworkDetailsModel)
    homeworkDetailsModel: ClassHomeworkDetailsModel[]
    @ApiModelPropertyOptional({example: UserHomeworkAnswerArrayExample})
    @HasMany(() => UserHomeworkAnswersModel)
    userHomeworkAnswers: UserHomeworkAnswersModel[];
    @ApiModelPropertyOptional({example: ClassSequencesArrayExample})
    @BelongsToMany(() => ClassSequencesModel, () => SequencesHomeworksModel)
    classSequences: ClassSequencesModel[]

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;
}