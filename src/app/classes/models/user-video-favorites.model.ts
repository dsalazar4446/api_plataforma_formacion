import { Model, Table, Column, DataType, ForeignKey, CreatedAt, UpdatedAt, BelongsTo } from "sequelize-typescript";
import { MainVideosModel } from "../../main-videos/models/main-videos.model";
import { UserModel } from "../../user/models/user.Model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({ tableName:'user_video_favorites'})
export class UserVideoFavoritesModel extends Model<UserVideoFavoritesModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => MainVideosModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'class_video_id',
    })
    classVideoId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'favorite_text',
    })
    favoriteText: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DATE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'video_time',
    })
    video_time: Date;

    @BelongsTo(() => UserModel)
    user: UserModel;
    @BelongsTo(() => MainVideosModel)
    mainVideos: MainVideosModel;

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date
}