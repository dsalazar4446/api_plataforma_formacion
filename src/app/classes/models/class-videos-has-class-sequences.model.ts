import { Model, Table, ForeignKey, AllowNull, Validate, Unique, Column, DataType, BelongsTo, PrimaryKey, AutoIncrement, CreatedAt, UpdatedAt } from "sequelize-typescript";
import { ClassSequencesModel } from "./class-sequences.model";
import { MainVideosModel } from "../../main-videos/models/main-videos.model";

@Table({ tableName: 'class_videos_has_class_sequences'})
export class VideosSequencesModel extends Model<VideosSequencesModel>{
    
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string
    
    @ForeignKey(() => MainVideosModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'class_video_id',
    })
    classVideosId: string;

    @BelongsTo(() => MainVideosModel)
    courses: MainVideosModel;

    @ForeignKey(() => ClassSequencesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'class_sequence_id',
    })
    classSequencesId: string;

    @BelongsTo(() => ClassSequencesModel)
    classSequences: ClassSequencesModel;

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;
}