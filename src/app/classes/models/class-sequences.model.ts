import { Model, Column, DataType, ForeignKey, BelongsTo, UpdatedAt, CreatedAt, BelongsToMany, Table, HasMany } from "sequelize-typescript";
import { FormationClassesModel } from "./formation-classes.model";
import { MainVideosModel } from "./../../main-videos/models/main-videos.model";
import { VideosSequencesModel } from "./class-videos-has-class-sequences.model";
import { SequenceTextModel } from "./class-sequence-has-class-texts.model";
import { SequencesAttachmentsModel } from "./cl-sequences-h-cl-attachments.model";
import { ClassAttachmentsModel } from "./class-attachments.model";
import { ClassHomeworksModel } from "./class-homeworks.model";
import { SequencesHomeworksModel } from "./class-sequences-has-class-homeworks.model";
import { ClassTextModel } from "./class-text.model";
import { ClassSequencesHasTestsModel } from "./class-sequences-has-tests.model";
import { FormationTest } from "../../test/models/formation-test.models";
import { UserClassesViewsModel } from "./user-classes-views.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";
import { ClassSequencesArrayExample } from "../examples/class-sequences.example";
import { MainVideosArrayExample } from "src/app/main-videos/example/main-videos.example";
import { ClassAttachmentArrayExample } from "../examples/class-attachment.example";
import { ClassHomeworkArrayExample } from "../examples/class-homework.example";
import { FormationTestArrayExample } from "src/app/test/examples/formation-test.example";
import { ClassTextArrayExample } from "../examples/class-text.example";
import { UserClassesViewsArrayExample } from "../examples/user-classes-views.example";

@Table({ tableName:'class_sequences'})
export class ClassSequencesModel extends Model<ClassSequencesModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => FormationClassesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'formation_class_id',
    })
    formationClassId: string;

    @BelongsTo(() => FormationClassesModel)
    formationClasses: FormationClassesModel;
    @ApiModelPropertyOptional({example:ClassSequencesArrayExample})
    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'class_sequence',
    })
    classSequence: number;
    @ApiModelPropertyOptional({example: MainVideosArrayExample})
    @BelongsToMany(() => MainVideosModel, () => VideosSequencesModel)
    mainVideos: MainVideosModel[];
    @ApiModelPropertyOptional({example:ClassAttachmentArrayExample})
    @BelongsToMany(() => ClassAttachmentsModel, () => SequencesAttachmentsModel)
    classAttachments: ClassAttachmentsModel[];
    @ApiModelPropertyOptional({example: ClassHomeworkArrayExample})
    @BelongsToMany(() => ClassHomeworksModel, () => SequencesHomeworksModel)
    classHomeworks: ClassHomeworksModel[];
    @ApiModelPropertyOptional({example: ClassTextArrayExample})
    @BelongsToMany(() => ClassTextModel, () => SequenceTextModel)
    classText: ClassTextModel[];
    @ApiModelPropertyOptional({example: FormationTestArrayExample}) 
    @BelongsToMany(() => FormationTest, () => ClassSequencesHasTestsModel)
    formationTest: FormationTest[]; 
    @ApiModelPropertyOptional({example: UserClassesViewsArrayExample})
    @HasMany(() => UserClassesViewsModel)
    UserClassesViewsModel: UserClassesViewsModel[]

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;
}