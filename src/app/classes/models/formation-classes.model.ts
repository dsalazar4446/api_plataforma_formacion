import { Model, Table, PrimaryKey, AutoIncrement, AllowNull, Column, Validate, Unique, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt, HasMany, BelongsToMany } from "sequelize-typescript";
import { LessonsModel } from "./../../lessons/models/lessons.model";
import { ClassTitlesModel } from "./class-titles.model";
import { ClassSequencesModel } from "./class-sequences.model";
import { FormationTest } from "../../test/models/formation-test.models";
import { FormationClassesHasFormationTests } from "../../test/models/formation-classes-has-formation-test.model";
import { ClassProblemsModel } from "./class-problems.model";
import { NotesModel } from "./notes.model";
import { UserClassQuestionsAnswersModel } from "./user-class-questions-answers.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";
import { FormationTestArrayExample } from "src/app/test/examples/formation-test.example";
import { ClassTitleArrayExample } from "../examples/class-titles.example";
import { ClassSequencesArrayExample } from "../examples/class-sequences.example";
import { ClassProblemsArrayExample } from "../examples/class-problems.example";
import { UserClassQuestionsAnswersArrayExample } from "../examples/user-class-question-answer.example";
import { NotesArrayExample } from "../examples/notes.example";

@Table({ tableName: 'formation_classes'})
export class FormationClassesModel extends Model<FormationClassesModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => LessonsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'lesson_id',
    })
    lessonId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'class_code',
    })
    classCode: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        defaultValue: false,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'class_status',
    })
    classStatus: boolean;

    /**
     * RELACIONES
     * FormationClassesModels pertenece a FormationTest
     */
    @ApiModelPropertyOptional({example: FormationTestArrayExample})
    @BelongsToMany(() => FormationTest, {
        through: {
          model: () => FormationClassesHasFormationTests,
          unique: true,
        }
      })
      formationTest: FormationTest[];
      
    @BelongsTo(() => LessonsModel)
    Lesson: LessonsModel;
    @ApiModelPropertyOptional({example: ClassTitleArrayExample})
    @HasMany(() => ClassTitlesModel)
    classTitles: ClassTitlesModel[];
    @ApiModelPropertyOptional({example: ClassSequencesArrayExample})
    @HasMany(() => ClassSequencesModel)
    classSequences: ClassSequencesModel[];
    @ApiModelPropertyOptional({example: ClassProblemsArrayExample})
    @HasMany(() => ClassProblemsModel)
    classProblems: ClassProblemsModel[];
    @ApiModelPropertyOptional({example: UserClassQuestionsAnswersArrayExample})
    @HasMany(() => UserClassQuestionsAnswersModel)
    userClassQuestionsAnswers: UserClassQuestionsAnswersModel[];
    @ApiModelPropertyOptional({example: NotesArrayExample})
    @HasMany(() => NotesModel)
    notes: NotesModel[];

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;
}