import { Model, Table, PrimaryKey, AutoIncrement, AllowNull, Validate, Unique, Column, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt } from "sequelize-typescript";
import { ClassHomeworksModel } from "./class-homeworks.model";
import { UserModel } from "../../user/models/user.Model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({ tableName: 'user_homework_answers'})
export class UserHomeworkAnswersModel extends Model<UserHomeworkAnswersModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    usersId: string;

    @BelongsTo(() => UserModel)
    user: UserModel;
    @ApiModelPropertyOptional()
    @ForeignKey(() => ClassHomeworksModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'class_homework_id',
    })
    classHomeworkId: string;

    @BelongsTo(() => ClassHomeworksModel)
    classHomeworks: ClassHomeworksModel;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        field: 'user_homework_answer',
    })
    userHomeworkAnswer: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TINYINT,
        allowNull: false,
        field: 'user_homework_answer_aproval',
    })
    userHomeworkAnswerAproval: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        field: 'user_homework_attachments',
    })
    userHomeworkAttachments: string;

// tslint:disable-next-line: variable-name
    @CreatedAt public created_at: Date;
// tslint:disable-next-line: variable-name
    @UpdatedAt public updated_at: Date;

}