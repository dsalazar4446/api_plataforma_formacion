import { Model, Table, Column, DataType, CreatedAt, UpdatedAt, ForeignKey, BelongsTo, HasMany } from "sequelize-typescript";
import { FormationClassesModel } from "./formation-classes.model";
import { UserModel } from "../../user/models/user.Model";
import { QuestionAnswersReplysModel } from "./question-answers-replys.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";
import { QuestionAnswerReplysArrayExample } from "../examples/question-answers-replys.example";

@Table({ tableName:'user_class_questions_answers'})
export class UserClassQuestionsAnswersModel extends Model<UserClassQuestionsAnswersModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => FormationClassesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'formation_class_id'
    })
    public formationClassId: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_id'
    })
    public userId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {notEmpty: true},
        field: 'question_answer'
    })
    public questionAnswer: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: { notEmpty: true },
        field: 'usefull'
    })
    public usefull: boolean;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: { notEmpty: true },
        field: 'question_answer_status'
    })
    public questionAnswerStatus: boolean;


    @BelongsTo(() => FormationClassesModel)
    formationClasses:FormationClassesModel;
    
    @BelongsTo(() => UserModel)
    user:UserModel;
    @ApiModelPropertyOptional({example: QuestionAnswerReplysArrayExample})
    @HasMany(() => QuestionAnswersReplysModel, 'parentId')
    replysParent: QuestionAnswersReplysModel[];
    @ApiModelPropertyOptional({example: QuestionAnswerReplysArrayExample})
    @HasMany(() => QuestionAnswersReplysModel, 'childrenId')
    replysChildren: QuestionAnswersReplysModel[];


    // tslint:disable-next-line: variable-name
    @CreatedAt public created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt public updated_at: Date;
}