import { Table, Model, ForeignKey, Column, DataType } from "sequelize-typescript";
import { ClassSequencesModel } from "./class-sequences.model";
import { UserModel } from "../../user/models/user.Model";
import { FormationTest } from "src/app/test/models/formation-test.models";

@Table({ tableName: 'class_sequences_has_tests'})
export class ClassSequencesHasTestsModel extends Model<ClassSequencesHasTestsModel>{
    @ForeignKey(() => ClassSequencesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'class_sequence_id',
    })

    classSequenceId: string;

    @ForeignKey(() => FormationTest)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'test_id',
    })

    testId: string;
}