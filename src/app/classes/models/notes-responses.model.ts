import { Model, Table, Column, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt } from "sequelize-typescript";
import { UserModel } from "../../user/models/user.Model";
import { NotesModel } from "./notes.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({tableName: 'note_responses'})
export class NotesResponses extends Model<NotesResponses> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => NotesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'notes_id',
    })
    notesId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'note_message',
    })
    noteMessage: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('0', '1', '2'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'note_message_status',
    })
    noteMessageStatus: string;

    @BelongsTo(() => UserModel)
    user: UserModel;

    @BelongsTo(() => NotesModel)
    NotesModel: NotesModel;

    @CreatedAt created_at: Date;
    @UpdatedAt updated_at: Date

} 