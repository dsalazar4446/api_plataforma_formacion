import { Model, Table, Column, DataType, ForeignKey, CreatedAt, UpdatedAt, BelongsTo, DefaultScope } from "sequelize-typescript";
import { UserClassQuestionsAnswersModel } from "./user-class-questions-answers.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

@Table({ tableName:'question_answers_replies'})
export class QuestionAnswersReplysModel extends Model<QuestionAnswersReplysModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    // @ForeignKey(() => UserClassQuestionsAnswersModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'parent_id',
    })
    parentId: string;

    @BelongsTo(() => UserClassQuestionsAnswersModel, 'parentId')
    parent: UserClassQuestionsAnswersModel;
    @ApiModelPropertyOptional()
    // @ForeignKey(() => UserClassQuestionsAnswersModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'children_id',
    })
    childrenId: string;

    @BelongsTo(() => UserClassQuestionsAnswersModel, 'childrenId')
    children: UserClassQuestionsAnswersModel;

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;
}