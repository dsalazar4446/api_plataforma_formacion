import { Model, Table, ForeignKey, Column, DataType, BelongsTo } from "sequelize-typescript";
import { NotesModel } from "./notes.model";
import { Categories } from "../../categories/models/categories.model";


@Table({ tableName:'notes_has_categories'})
export class NotesHasCategoriesModel extends Model<NotesHasCategoriesModel> {
    
    @ForeignKey(() => Categories)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'category_id',
    })
    categoryId: string;
    
    @ForeignKey(() => NotesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'note_id',
    })
    noteId: string;

    @BelongsTo(() => Categories)
    category: Categories;
    @BelongsTo(() => NotesModel)
    notes: NotesModel;
}