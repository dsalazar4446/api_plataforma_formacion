import { Table, Model, Column, DataType, ForeignKey, CreatedAt, UpdatedAt, BelongsTo } from "sequelize-typescript";
import { UserModel } from "../../user/models/user.Model";
import { FormationClassesModel } from "./formation-classes.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";

/**
 * @author Daniel Salazar
 * @description Modelo de conexion de la tabla class_problems
 * @type class
 */

@Table({tableName:'class_problems'})
export class ClassProblemsModel extends Model<ClassProblemsModel>{
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    public userId: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => FormationClassesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'formation_class_id',
    })
    public classId: string
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2', '3', '4', '5'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'problem_type',
    })
    public problemType: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'problem_explanation',
    })
    public problemExplanation: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'problem_status',
    })
    public problemStatus: boolean;
    
    @BelongsTo(() => UserModel)
    user: UserModel;

    @BelongsTo(() => FormationClassesModel)
    formationClasses: FormationClassesModel;

    // tslint:disable-next-line: variable-name
    @CreatedAt public created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt public updated_at: Date;
}