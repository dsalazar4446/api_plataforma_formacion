import { Model, Column, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt, Table } from "sequelize-typescript";
import { ClassSequencesModel } from "./class-sequences.model";
import { ClassAttachmentsModel } from "./class-attachments.model";
@Table({ tableName:'cl_sequences_h_cl_attachments'})
export class SequencesAttachmentsModel extends Model<SequencesAttachmentsModel> {
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ForeignKey(() => ClassSequencesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'class_sequence_id',
    })
    classSequenceId: string;

    @BelongsTo(() => ClassSequencesModel)
    classSequences: ClassSequencesModel;

    @ForeignKey(() => ClassAttachmentsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'class_attachment_id',
    })
    classAttachmentId: string;

    @BelongsTo(() => ClassAttachmentsModel)
    classAttachments: ClassAttachmentsModel;

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;
}