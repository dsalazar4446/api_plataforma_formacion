import { Model, Table, PrimaryKey, AutoIncrement, AllowNull, Validate, Unique, Column, DataType, CreatedAt, UpdatedAt, BelongsToMany } from "sequelize-typescript";
import { ClassSequencesModel } from "./class-sequences.model";
import { SequencesAttachmentsModel } from "./cl-sequences-h-cl-attachments.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";
import { ClassAttachmentArrayExample } from "../examples/class-attachment.example";

@Table({ tableName: 'class_attachments'})
export class ClassAttachmentsModel extends Model <ClassAttachmentsModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2', '3'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'class_attachment_type',
    })
    classAttachmentType: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'class_attachment',
    })
    classAttachment: string;
    @ApiModelPropertyOptional({example: ClassAttachmentArrayExample})
    @BelongsToMany(() => ClassSequencesModel, () => SequencesAttachmentsModel)
    classSequences: ClassSequencesModel[];

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;
}