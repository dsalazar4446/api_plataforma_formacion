import { Model, Table, PrimaryKey, AutoIncrement, AllowNull, Validate, Unique, Column, DataType, CreatedAt, UpdatedAt, BelongsToMany } from "sequelize-typescript";
import { ClassSequencesModel } from "./class-sequences.model";
import { SequenceTextModel } from "./class-sequence-has-class-texts.model";
import { ApiModelPropertyOptional } from "@nestjs/swagger";
import { ClassAttachmentArrayExample } from "../examples/class-attachment.example";

@Table({ tableName: 'class_texts'})
export class ClassTextModel extends Model<ClassTextModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'class_description',
    })
    classDescription: string
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string
    @ApiModelPropertyOptional({example: ClassAttachmentArrayExample})
    @BelongsToMany(() => ClassSequencesModel, () => SequenceTextModel)
    classSequencesModel: ClassSequencesModel[];

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;
}