import { Model, Table, PrimaryKey, AutoIncrement, AllowNull, Validate, Unique, Column, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt } from "sequelize-typescript";
import { ClassSequencesModel } from "./class-sequences.model";
import { ClassTextModel } from "./class-text.model";

@Table({ tableName: 'class_sequence_has_class_texts'})
export class SequenceTextModel extends Model<SequenceTextModel> {
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ForeignKey(() => ClassSequencesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'class_sequence_id',
    })
    classSequencesId: string;

    @BelongsTo(() => ClassSequencesModel)
    formationClasses: ClassSequencesModel;

    @ForeignKey(() => ClassTextModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        validate: {
            notEmpty: true,
        },
        field: 'class_text_id',
    })
    classTextsId: string;

    @BelongsTo(() => ClassTextModel)
    classText: ClassTextModel;

    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;
}