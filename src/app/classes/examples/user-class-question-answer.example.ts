export const UserClassQuestionsAnswersExample = {
    "id": "string",
    "formationClassId": "string",
    "userId": "string",
    "questionAnswer": "string",
    "usefull": "boolean",
    "questionAnswerStatus": "boolean"
}

export const UserClassQuestionsAnswersArrayExample = [
    UserClassQuestionsAnswersExample
]