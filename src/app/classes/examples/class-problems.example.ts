export const ClassProblemsExample = {
    "id": "string",
    "userId": "string",
    "classId": "string",
    "problemType": "string",
    "problemExplanation": "string",
    "problemStatus": "boolean"
}

export const ClassProblemsArrayExample = [
    ClassProblemsExample
]