export const NotesResponsesExample = {
    "id": "string",
    "userId": "string",
    "classId": "string",
    "noteText": "string",
    "makePublic": "boolean",
    "languageType": "string"
}

export const NotesResponsesArrayExample = [
    NotesResponsesExample
]