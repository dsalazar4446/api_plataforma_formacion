export const ClassHomeworkDetailsExample = {
    "id": "string",
    "classHomeworksId": "string",
    "homeworkTitle": "string",
    "homeworkDescription": "string",
    "languageType": "string"
}

export const ClassHomeworkDetailsArrayExample = [
    ClassHomeworkDetailsExample
]