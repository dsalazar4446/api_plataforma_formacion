export const ClassAttachmentExample = {
    "id": "string",
    "classAttachmentType": "string",
    "classAttachment": "string",
}

export const ClassAttachmentArrayExample = [
    ClassAttachmentExample
]