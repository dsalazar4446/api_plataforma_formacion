export const UserHomeworkAnswerExample = {
    "id": "string",
    "usersId": "string",
    "classHomeworkId": "string",
    "userHomeworkAnswer": "string",
    "userHomeworkAnswerAproval": "number",
    "userHomeworkAttachments": "string"
}

export const UserHomeworkAnswerArrayExample = [
    UserHomeworkAnswerExample
]