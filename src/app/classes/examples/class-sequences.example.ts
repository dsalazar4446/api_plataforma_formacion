export const ClassSequencesExample = {
    "id": "string",
    "formationClassId": "string",
    "classSequence": "number"
}

export const ClassSequencesArrayExample = [
    ClassSequencesExample
]