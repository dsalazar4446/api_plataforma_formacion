export const ClassTitleExample = {
    "id": "string",
    "formationClassId": "string",
    "classTitle": "string",
    "languageType": "string"
}

export const ClassTitleArrayExample = [
    ClassTitleExample
]