import { Controller, Post, UsePipes, Body, Get, Put, Param, Delete, UseInterceptors, UseFilters, UseGuards, FileFieldsInterceptor, HttpException, HttpStatus } from '@nestjs/common';
import { CreateClassAttachmentsDto, UpdateClassAttachmentsDto } from '../../dto/class-attachments.dto';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiConsumes, ApiImplicitFile, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { ClassAttachmentsService } from '../../services/class-attachments/class-attachments.service';
import * as path from 'path';
import { diskStorage } from 'multer';
import { ClassAttachmentsModel } from '../../models/class-attachments.model';
@ApiUseTags('Classes Module')
@Controller('classes/class-attachments')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // @UseGuards(new JwtAuthGuard())
export class ClassAttachmentsController {
    constructor(private readonly classAttachmentsService: ClassAttachmentsService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassAttachmentsModel, isArray: true })
    @UseInterceptors(new AppResponseInterceptor())
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name:'classAttachment',description:'Archivos relacionados con las clases',required: true})
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'classAttachment', maxCount: 1 },
    ], {
    fileFilter: (req, file, cb) => {
        const extValidas = [
            'jpg', 'jpeg','png', 'svg',
            'avi', 'mp4', 'mkv', 'flv', 'mov', 'wmv',
            'aac', 'wav', 'au', 'wma', 'midi', 'mp3'
        ];
        const ext = file.mimetype.toLocaleLowerCase()
        const result = extValidas.find(extvalid => {
            return ext === extvalid;
        })
        if(!result){
            cb(null,true)
        }else {
            cb(null, false)
        }
    },
    storage: diskStorage({
        destination: './uploads/classes/classAttachment'
      , filename: (req, file, cb) => {
          const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
          if(file.fieldname === 'classAttachment'){
            req.body.classAttachment = `${randomName}${path.extname(file.originalname)}`;
        }
          cb(null, `${randomName}${path.extname(file.originalname)}`)
      },
    }),
  }))
    async create(@Body() body: CreateClassAttachmentsDto) {
        return this.classAttachmentsService.create(body);
    }
 
    @Get()
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({ status: 200, type: ClassAttachmentsModel, isArray: true })
    @UseInterceptors(new AppResponseInterceptor())
    async list() {
        return this.classAttachmentsService.list();
    }


    @Get(':id')
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({ status: 200, type: ClassAttachmentsModel, isArray: true })
    @UseInterceptors(new AppResponseInterceptor())
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detail(@Param('id') id) {
        return this.classAttachmentsService.detail(id);
    }

    @Put(':id')
    @ApiImplicitHeader({name:'language',required: true})
    @ApiResponse({ status: 200, type: ClassAttachmentsModel, isArray: true })
    @UseInterceptors(new AppResponseInterceptor())
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'classAttachment', description: 'Archivos relacionados con las clases', required: true })
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'classAttachment', maxCount: 1 },
    ], {
            fileFilter: (req, file, cb) => {
                const extValidas = [
                    'jpg', 'jpeg', 'png', 'svg',
                    'avi', 'mp4', 'mkv', 'flv', 'mov', 'wmv',
                    'aac', 'wav', 'au', 'wma', 'midi', 'mp3'
                ];
                const ext = file.mimetype.toLocaleLowerCase()
                const result = extValidas.find(extvalid => {
                    return ext === extvalid;
                })
                if (!result) {
                    cb(null, true)
                } else {
                    cb(null, false)
                }
            },
            storage: diskStorage({
                destination: './uploads/classes/classAttachment'
                , filename: (req, file, cb) => {
                    const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                    if (file.fieldname === 'classAttachment') {
                        req.body.classAttachment = `${randomName}${path.extname(file.originalname)}`;
                    }
                    cb(null, `${randomName}${path.extname(file.originalname)}`)
                },
            }),
        }))
    async update(@Param('id') id: string, @Body() body: Partial<UpdateClassAttachmentsDto>) {
        return await this.classAttachmentsService.update(id, body);
    }

    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') id: string) {
        return this.classAttachmentsService.delete(id);
    }

}
