import { Test, TestingModule } from '@nestjs/testing';
import { ClassAttachmentsController } from './class-attachments.controller';

describe('ClassAttachments Controller', () => {
  let controller: ClassAttachmentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClassAttachmentsController],
    }).compile();

    controller = module.get<ClassAttachmentsController>(ClassAttachmentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
