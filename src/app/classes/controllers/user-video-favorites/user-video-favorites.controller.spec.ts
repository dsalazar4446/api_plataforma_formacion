import { Test, TestingModule } from '@nestjs/testing';
import { UserVideoFavoritesController } from './user-video-favorites.controller';

describe('UserVideoFavorites Controller', () => {
  let controller: UserVideoFavoritesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserVideoFavoritesController],
    }).compile();

    controller = module.get<UserVideoFavoritesController>(UserVideoFavoritesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
