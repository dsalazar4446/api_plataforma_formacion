import { Controller, Put, Post, Body, Get, Param, Delete } from '@nestjs/common';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { UserVideoFavoritesService } from '../../services/user-video-favorites/user-video-favorites.service';
import { CreateUserVideoFavorites, UpdateUserVideoFavorites } from '../../dto/user-video-favorites.dto';
import { UserVideoFavoritesModel } from '../../models/user-video-favorites.model';

@Controller('user-video-favorites')
@ApiUseTags('Classes Module')
export class UserVideoFavoritesController {
    constructor(private readonly userVideoFavorites: UserVideoFavoritesService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserVideoFavoritesModel})
    async create(@Body() body: CreateUserVideoFavorites) {
        return this.userVideoFavorites.create(body)
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserVideoFavoritesModel, isArray: true })
    async list() {
        return await this.userVideoFavorites.list();
    }

    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserVideoFavoritesModel})
    @ApiImplicitParam({ name: 'id', required: true, type: ' string' })
    async detail(@Param('id') id: string) {
        return await this.userVideoFavorites.detail(id);
    }
    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserVideoFavoritesModel})
    @ApiImplicitParam({ name: 'id', required: true, type: ' string' })
    async update(@Param('id') id: string, @Body() body: Partial<UpdateUserVideoFavorites>) {
        return await this.userVideoFavorites.update(id, body);
    }
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: ' string' })
    async delete(@Param('id') id: string) {
        return await this.userVideoFavorites.delete(id)
    }
}
