import { Controller, Post, Delete, Param, Body } from '@nestjs/common';
import { ClassVideosHasClassSequencesService } from '../../services/class-videos-has-class-sequences/class-videos-has-class-sequences.service';
import { ApiImplicitParam, ApiUseTags } from '@nestjs/swagger';
import { CreateVideosSequencesDto } from '../../dto/class-videos-has-class-sequences.dto';

@ApiUseTags('Classes Module')
@Controller('class-videos-has-class-sequences')
export class ClassVideosHasClassSequencesController {
    constructor(private readonly videosSequence: ClassVideosHasClassSequencesService){}

    @Post()
    create(@Body()body: CreateVideosSequencesDto){
        return this.videosSequence.create(body);
    }
    @Delete('id')
    @ApiImplicitParam({name:'id', required: true, type: 'string' })
    delete(@Param('id')  id: string) {
        return this.delete(id)
    }
}
