import { Test, TestingModule } from '@nestjs/testing';
import { ClassVideosHasClassSequencesController } from './class-videos-has-class-sequences.controller';

describe('ClassVideosHasClassSequences Controller', () => {
  let controller: ClassVideosHasClassSequencesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClassVideosHasClassSequencesController],
    }).compile();

    controller = module.get<ClassVideosHasClassSequencesController>(ClassVideosHasClassSequencesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
