import { Test, TestingModule } from '@nestjs/testing';
import { ClassTextController } from './class-text.controller';

describe('ClassText Controller', () => {
  let controller: ClassTextController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClassTextController],
    }).compile();

    controller = module.get<ClassTextController>(ClassTextController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
