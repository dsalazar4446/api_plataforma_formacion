
import { Controller, UseInterceptors, UseFilters, UseGuards, Post, UsePipes, Body, Get, Param, Put, Delete, Req } from '@nestjs/common';
import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';

import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { ClassTextService } from '../../services/class-text/class-text.service';
import { CreateClassTextDto, UpdateClassTextDto } from '../../dto/class-text.dto';
import { ClassTextModel } from '../../models/class-text.model';

@ApiUseTags('Classes Module')
@Controller('classes/class-text')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // // @UseGuards(new JwtAuthGuard())
export class ClassTextController {

    constructor(private readonly classTextService: ClassTextService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: ClassTextModel})
    async create(@Body() body: CreateClassTextDto, @Req() req) {
        body.languageType = req.headers.language
        return this.classTextService.create(body);
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassTextModel, isArray: true })
    async list(@Req() req) {
        return this.classTextService.list(req.headers.language);
    }


    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassTextModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detail(@Param('id') id, @Req() req) {
        return this.classTextService.detail(id, req.headers.language);
    }

    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassTextModel })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(@Param('id') id: string, @Body() body: Partial<UpdateClassTextDto>, @Req() req) {
        body.languageType = req.headers.language
        return await this.classTextService.update(id, body);
    }

    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') id: string) {
        return this.classTextService.delete(id);
    }


}
