import { Test, TestingModule } from '@nestjs/testing';
import { UserClassQuestionsAnswersController } from './user-class-questions-answers.controller';

describe('UserClassQuestionsAnswers Controller', () => {
  let controller: UserClassQuestionsAnswersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserClassQuestionsAnswersController],
    }).compile();

    controller = module.get<UserClassQuestionsAnswersController>(UserClassQuestionsAnswersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
