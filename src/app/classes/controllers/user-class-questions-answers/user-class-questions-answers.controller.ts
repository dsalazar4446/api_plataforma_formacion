import { Controller, Param, Body, Post, Get, Put, Delete } from '@nestjs/common';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { CreateUserClassQuestionAnswer, UpdateUserClassQuestionAnswer } from '../../dto/user-class-questions-answers.dto';
import { UserClassQuestionsAnswersService } from '../../services/user-class-questions-answers/user-class-questions-answers.service';
import { UserClassQuestionsAnswersModel } from '../../models/user-class-questions-answers.model';

@ApiUseTags('Classes Module')
@Controller('user-class-questions-answers')
export class UserClassQuestionsAnswersController {

    constructor(private readonly userClassQuestionsAnswers: UserClassQuestionsAnswersService) {}
    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: UserClassQuestionsAnswersModel })
    async create(@Body() body: CreateUserClassQuestionAnswer) {
        return await this.userClassQuestionsAnswers.create(body);
    }
    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserClassQuestionsAnswersModel, isArray: true })
    async list() {
        return await this.userClassQuestionsAnswers.list();
    }
    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserClassQuestionsAnswersModel })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detail(@Param('id') id: string) {
        return await this.userClassQuestionsAnswers.detail(id);
    }
    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserClassQuestionsAnswersModel })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(@Param('id') id: string, @Body() body: Partial<UpdateUserClassQuestionAnswer>) {
        return await this.userClassQuestionsAnswers.update(id, body)
    }
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') id: string) {
        return await this.userClassQuestionsAnswers.delete(id);
    }
}
