import { Controller, Post, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { UserClassesViewsService } from '../../services/user-classes-views/user-classes-views.service';
import { CreateUserClassesViews, UpdateUserClassesViews } from '../../dto/user-classes-views.dto';
import { UserClassesViewsModel } from '../../models/user-classes-views.model';

@Controller('user-classes-views')
@ApiUseTags('Classes Module')
export class UserClassesViewsController {
    constructor(private readonly userClassesViews: UserClassesViewsService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: UserClassesViewsModel })
    async create(@Body() body: CreateUserClassesViews) {
        return this.userClassesViews.create(body)
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserClassesViewsModel, isArray: true })
    async list() {
        return await this.userClassesViews.list();
    }

    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserClassesViewsModel })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detail(@Param('id') id: string) {
        return await this.userClassesViews.detail(id);
    }
    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserClassesViewsModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(@Param('id') id: string, @Body() body: Partial<UpdateUserClassesViews>) {
        return await this.userClassesViews.update(id, body);
    }
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') id: string) {
        return await this.userClassesViews.delete(id)
    }
}
