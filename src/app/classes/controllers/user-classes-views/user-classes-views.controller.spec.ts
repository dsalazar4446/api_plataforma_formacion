import { Test, TestingModule } from '@nestjs/testing';
import { UserClassesViewsController } from './user-classes-views.controller';

describe('UserClassesViews Controller', () => {
  let controller: UserClassesViewsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserClassesViewsController],
    }).compile();

    controller = module.get<UserClassesViewsController>(UserClassesViewsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
