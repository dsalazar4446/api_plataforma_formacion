import { Controller, Get, Put, Delete, Body, Post, Param } from '@nestjs/common';
import { ClassProblemsService } from '../../services/class-problems/class-problems.service';
import { CreateClassProblemsDto, UpdateClassProblemsDto } from '../../dto/class-problems.dto';
import { ApiUseTags, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { ClassProblemsModel } from '../../models/class-problems.model';

@Controller('class-problems')
@ApiUseTags('Classes Module')
export class ClassProblemsController {
    constructor(private readonly calssProblemsService: ClassProblemsService){}

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: ClassProblemsModel })
    create(@Body() body: CreateClassProblemsDto){
        return this.calssProblemsService.create(body);
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassProblemsModel, isArray: true })
    list(){
        return this.calssProblemsService.list();
    }

    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassProblemsModel})
    details(@Param('id') id: string){
        return this.calssProblemsService.detail(id);
    }

    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassProblemsModel })
    update(@Param('id') id: string, @Body() body: Partial<UpdateClassProblemsDto>){
        return this.calssProblemsService.update(id,body);
    }


    @Delete(':id')
    delete(@Param('id') id: string) {
        return this.calssProblemsService.delete(id);
    }


}
