import { Test, TestingModule } from '@nestjs/testing';
import { ClassProblemsController } from './class-problems.controller';

describe('ClassProblems Controller', () => {
  let controller: ClassProblemsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClassProblemsController],
    }).compile();

    controller = module.get<ClassProblemsController>(ClassProblemsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
