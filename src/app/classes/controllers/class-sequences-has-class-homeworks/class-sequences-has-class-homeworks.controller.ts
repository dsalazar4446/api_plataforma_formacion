import { Controller, Post, Delete, Body, Param } from '@nestjs/common';
import { ClassSequencesHasClassHomeworksService } from '../../services/class-sequences-has-class-homeworks/class-sequences-has-class-homeworks.service';
import { ApiImplicitParam, ApiUseTags } from '@nestjs/swagger';
import { CreateSequencesHomeworksDto } from '../../dto/class-sequences-has-class-homeworks.dto';

@ApiUseTags('Classes Module')
@Controller('class-sequences-has-class-homeworks')
export class ClassSequencesHasClassHomeworksController {
    constructor(private readonly sequencesHomeworks: ClassSequencesHasClassHomeworksService) { }

    @Post()
    async create(@Body() body: CreateSequencesHomeworksDto) {
        return this.sequencesHomeworks.create(body);
    }
    @Delete('id')
    @ApiImplicitParam({name:'id', required:true, type:' string'})
    async delete(@Param() id: string) {
       return this.sequencesHomeworks.delete(id);
    }
}
