import { Test, TestingModule } from '@nestjs/testing';
import { ClassSequencesHasClassHomeworksController } from './class-sequences-has-class-homeworks.controller';

describe('ClassSequencesHasClassHomeworks Controller', () => {
  let controller: ClassSequencesHasClassHomeworksController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClassSequencesHasClassHomeworksController],
    }).compile();

    controller = module.get<ClassSequencesHasClassHomeworksController>(ClassSequencesHasClassHomeworksController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
