import { Controller, Post, Body, Get, Param, Put, Delete, UseInterceptors, UseFilters } from '@nestjs/common';
import { QuestionAnswersReplysService } from '../../services/question-answers-replys/question-answers-replys.service';
import { CreateQuestionAnswersReplys, UpdateQuestionAnswersReplys } from '../../dto/question-answers-replys.dto';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { QuestionAnswersReplysModel } from '../../models/question-answers-replys.model';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
@ApiUseTags('Classes')
@Controller('question-answers-replys')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
export class QuestionAnswersReplysController {
    constructor(private readonly questionAnswersReplys: QuestionAnswersReplysService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: QuestionAnswersReplysModel })
    async create(@Body() body: CreateQuestionAnswersReplys) {
        return this.questionAnswersReplys.create(body);
    }
    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: QuestionAnswersReplysModel, isArray: true })
    async list() {
        return this.questionAnswersReplys.list();
    }
    @Get(':id')
    @ApiImplicitHeader({ name: 'language' })
    @ApiResponse({ status: 200, type: QuestionAnswersReplysModel })
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    async detail(id: string) {
        return this.questionAnswersReplys.detail(id);
    }
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true })
    async update(@Param('id') id: string, @Body() body: Partial<UpdateQuestionAnswersReplys>) {
        return this.questionAnswersReplys.update(id,body);
    }
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') id: string) {
        return this.questionAnswersReplys.delete(id);
    }
}
