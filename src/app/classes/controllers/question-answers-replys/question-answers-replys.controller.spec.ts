import { Test, TestingModule } from '@nestjs/testing';
import { QuestionAnswersReplysController } from './question-answers-replys.controller';

describe('QuestionAnswersReplys Controller', () => {
  let controller: QuestionAnswersReplysController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [QuestionAnswersReplysController],
    }).compile();

    controller = module.get<QuestionAnswersReplysController>(QuestionAnswersReplysController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
