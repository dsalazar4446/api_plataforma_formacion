import { Test, TestingModule } from '@nestjs/testing';
import { ClassHomeworksController } from './class-homeworks.controller';

describe('ClassHomeworks Controller', () => {
  let controller: ClassHomeworksController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClassHomeworksController],
    }).compile();

    controller = module.get<ClassHomeworksController>(ClassHomeworksController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
