
import { Controller, Param, Get, Body, Put, Delete, Post, UsePipes, UseInterceptors, UseFilters, UseGuards, Req } from '@nestjs/common';

import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { ClassHomeworksService } from '../../services/class-homeworks/class-homeworks.service';
import { CreateClassHomeworksDto, UpdateClassHomeworksDto } from '../../dto/class-homeworks.dto';
import { ClassHomeworksModel } from '../../models/class-homeworks.model';

@ApiUseTags('Classes Module')
@Controller('classes/class-homeworks')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // // @UseGuards(new JwtAuthGuard())
export class ClassHomeworksController {
    constructor(private readonly classHomeworksService: ClassHomeworksService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: ClassHomeworksModel})
    @UseInterceptors(new AppResponseInterceptor())
    async create(@Body() body: CreateClassHomeworksDto) {
        return this.classHomeworksService.create(body);
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassHomeworksModel, isArray: true })
    async list() {
        return this.classHomeworksService.list();
    }


    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassHomeworksModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detail(@Param('id') id) {
        return this.classHomeworksService.detail(id);
    }

    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassHomeworksModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(@Param('id') id: string, @Body() body: Partial<UpdateClassHomeworksDto>) {
        return await this.classHomeworksService.update(id, body);
    }

    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') id: string) {
        return this.classHomeworksService.delete(id);
    }
}
