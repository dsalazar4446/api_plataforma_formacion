import { ApiUseTags, ApiImplicitParam } from "@nestjs/swagger";
import { Controller, UseFilters, UseInterceptors, Post, Body, HttpStatus, Get, Param, NotFoundException, Put, Delete } from "@nestjs/common";
import { AppExceptionFilter } from "../../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { NotesResponsesService } from "../../services/notes/notes-responses.service";
import { NotesResponsesSaveJson } from "../../dto/notes-responses.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller NotesResponsesController
 * @Creado 29 de Marzo 2019
 */
@ApiUseTags('Classes Module')
@Controller('notes-responses')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class NotesResponsesController {

    constructor(private readonly _notesResponsesService: NotesResponsesService,
        private readonly appUtilsService: AppUtilsService) { }

    @Post()
    async create(@Body() bodyNotesResponses: NotesResponsesSaveJson) {

        const createNotesResponses = await this._notesResponsesService.create(bodyNotesResponses);

        if (!createNotesResponses) {
            return this.appUtilsService.httpCommonError('Notes Responses does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return createNotesResponses;

    }

    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idNotesResponses: string) {

        const fetchNotesResponses = await this._notesResponsesService.detail(idNotesResponses);

        if (!fetchNotesResponses) {
            throw new NotFoundException('Notes Responses does not exist!');
        }

        return fetchNotesResponses;
    }

    @Get()
    async findByAll() {

        const fetchAll = await this._notesResponsesService.list();

        if (!fetchAll) {
            throw new NotFoundException('Notes Responses does not exist!');
        }

        return fetchAll;
    }

    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateNotesResponses(@Param('id') id: string, @Body() bodyNotesResponses: Partial<NotesResponsesSaveJson>) {

        const updateNotesResponses = await this._notesResponsesService.update(id, bodyNotesResponses);

        if (!updateNotesResponses) {
            throw new NotFoundException('Notes Responses does not exist!');
        }

        return updateNotesResponses;
    }

    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteNotesResponses(@Param('id') idNotesResponses: string) {

        const deleteNotesResponses = await this._notesResponsesService.delete(idNotesResponses);

        if (!deleteNotesResponses) {
            throw new NotFoundException('Notes Responses does not exist!');
        }

        return deleteNotesResponses;
    }

}

