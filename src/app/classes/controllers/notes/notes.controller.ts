import { Controller, UseInterceptors, UseFilters, UseGuards, Post, UsePipes, Body, Get, Param, Put, Delete, Req, NotFoundException } from '@nestjs/common';
import { NotesService } from '../../services/notes/notes.service';
import { CreateNotesDto, UpdateNotesDto } from '../../dto/notes.dto';
import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { NotesModel } from '../../models/notes.model';

@ApiUseTags('Classes Module')
@Controller('classes/notes')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // // @UseGuards(new JwtAuthGuard())
export class NotesController {
    constructor(private readonly notesService: NotesService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: NotesModel })
    async create(@Req() req, @Body() body: CreateNotesDto) {

        if (req.headers.language) {
            body.languageType = req.headers.language;
        } else {
            body.languageType = 'es';
        }
        return await this.notesService.create(body);
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: NotesModel})
    async listLanguage(@Req() req) {
        const data = await this.notesService.listLanguage(req.headers.language);
        if(!data) throw new NotFoundException('Language does not exist!');
        return data;
    }

    @Get('list')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: NotesModel, isArray: true })
    async list() {
        return await this.notesService.list();
    }


    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: NotesModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detail(@Param('id') id, @Req() req) {
        return await this.notesService.detail(id, req.headers.language);
    }

    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: NotesModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(@Req() req, @Param('id') id: string, @Body() body: Partial<CreateNotesDto>) {

        if (req.headers.language) {
            body.languageType = req.headers.language;
        }
        return await this.notesService.update(id, body);
    }

    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') id: string) {
        return await this.notesService.delete(id);
    }
}
