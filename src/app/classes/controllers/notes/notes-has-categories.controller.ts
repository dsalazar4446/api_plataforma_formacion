import { Controller, UseFilters, UseInterceptors, Post, Body, HttpStatus, Delete, Param, NotFoundException, Get } from "@nestjs/common";
import { AppExceptionFilter } from "../../../shared/filters/app-exception.filter";
import { ApiUseTags, ApiImplicitParam } from "@nestjs/swagger";
import { AppResponseInterceptor } from "../../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { NotesHasCategoriesServices } from "../../services/notes/notes-has-categories.service";
import { NotesHasCategoriesSaveJson } from "../../dto/notes-has-categories.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller NotesHasCategoriesController
 * @Creado 03 Mayo 2019
 */

@ApiUseTags('Classes')
@Controller('notes-has-categories')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class NotesHasCategoriesController {

    constructor(private readonly _notesHasCategoriesServices: NotesHasCategoriesServices,
        private readonly appUtilsService: AppUtilsService) { }

    @Post()
    async create(@Body() bodyMagazines: NotesHasCategoriesSaveJson) {

        const create = await this._notesHasCategoriesServices.save(bodyMagazines);

        if (!create) {
            return this.appUtilsService.httpCommonError('NotesHasCategories does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return create;

    }

    @Get()
    async showAll(){
        return await this._notesHasCategoriesServices.list();
    }

    @Delete(':id')
    @ApiImplicitParam({name: 'id', required: true, type: 'string'})
    async delete(@Param('id') id: string) {

        const deleteNotes = await this._notesHasCategoriesServices.destroy(id);

        if (!deleteNotes) {
            throw new NotFoundException('NotesHasCategories does not exist!');
        }

        return deleteNotes;
    }

}