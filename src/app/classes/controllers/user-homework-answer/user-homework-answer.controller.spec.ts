import { Test, TestingModule } from '@nestjs/testing';
import { UserHomeworkAnswerController } from './user-homework-answer.controller';

describe('UserHomeworkAnswer Controller', () => {
  let controller: UserHomeworkAnswerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserHomeworkAnswerController],
    }).compile();

    controller = module.get<UserHomeworkAnswerController>(UserHomeworkAnswerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
