import { Controller, UseInterceptors, UseFilters, UseGuards, Post, UsePipes, Body, Get, Param, Put, Delete, FileFieldsInterceptor } from '@nestjs/common';
import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { ApiUseTags, ApiImplicitParam, ApiConsumes, ApiImplicitFile, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { UserHomeworkAnswerService } from '../../services/user-homework-answer/user-homework-answer.service';
import { CreateUserHomeworkAnswerDto, UpdateUserHomeworkAnswerDto } from '../../dto/user-homework-answer.tdo';
import * as path from 'path';
import { diskStorage } from 'multer';
import { UserHomeworkAnswersModel } from '../../models/user-homework-answer.model';
@ApiUseTags('Classes Module')
@Controller('classes/user-homework-answer')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// @UseGuards(new JwtAuthGuard())
export class UserHomeworkAnswerController {
    constructor(private readonly userHomeworkAnswerService: UserHomeworkAnswerService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: UserHomeworkAnswersModel })
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'userHomeworkAttachments', description: 'Archivos relacionados con las respuestas de las preguntas', required: true })
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'userHomeworkAttachments', maxCount: 1 },
    ], {
            fileFilter: (req, file, cb) => {
                const extValidas = [
                    'jpg', 'jpeg', 'png', 'svg',
                    'avi', 'mp4', 'mkv', 'flv', 'mov', 'wmv',
                    'aac', 'wav', 'au', 'wma', 'midi', 'mp3'
                ];
                const ext = file.mimetype.toLocaleLowerCase()
                const result = extValidas.find(extvalid => {
                    return ext === extvalid;
                })
                if (!result) {
                    cb(null, true)
                } else {
                    cb(null, false)
                }
            },
            storage: diskStorage({
                destination: './uploads/classes/userHomeworkAttachments'
                , filename: (req, file, cb) => {
                    const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                    if (file.fieldname === 'userHomeworkAttachments') {
                        req.body.userHomeworkAttachments = `${randomName}${path.extname(file.originalname)}`;
                    }
                    cb(null, `${randomName}${path.extname(file.originalname)}`)
                },
            }),
        }))
    async create(@Body() body: CreateUserHomeworkAnswerDto) {
        return this.userHomeworkAnswerService.create(body);
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserHomeworkAnswersModel, isArray: true })
    async list() {
        return this.userHomeworkAnswerService.list();
    }


    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserHomeworkAnswersModel })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detail(@Param('id') id) {
        return this.userHomeworkAnswerService.detail(id);
    }

    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserHomeworkAnswersModel })
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'userHomeworkAttachments', description: 'Archivos relacionados con las respuestas de las preguntas', required: true })
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'userHomeworkAttachments', maxCount: 1 },
    ], {
            fileFilter: (req, file, cb) => {
                const extValidas = [
                    'jpg', 'jpeg', 'png', 'svg',
                    'avi', 'mp4', 'mkv', 'flv', 'mov', 'wmv',
                    'aac', 'wav', 'au', 'wma', 'midi', 'mp3'
                ];
                const ext = file.mimetype.toLocaleLowerCase()
                const result = extValidas.find(extvalid => {
                    return ext === extvalid;
                })
                if (!result) {
                    cb(null, true)
                } else {
                    cb(null, false)
                }
            },
            storage: diskStorage({
                destination: './uploads/classes/userHomeworkAttachments'
                , filename: (req, file, cb) => {
                    const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                    if (file.fieldname === 'userHomeworkAttachments') {
                        req.body.userHomeworkAttachments = `${randomName}${path.extname(file.originalname)}`;
                    }
                    cb(null, `${randomName}${path.extname(file.originalname)}`)
                },
            }),
        }))
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(@Param('id') id: string, @Body() body: Partial<UpdateUserHomeworkAnswerDto>) {
        return await this.userHomeworkAnswerService.update(id, body);
    }

    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') id: string) {
        return this.userHomeworkAnswerService.delete(id);
    }
}
