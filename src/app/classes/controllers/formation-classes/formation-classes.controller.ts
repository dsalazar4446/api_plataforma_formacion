import { Controller, Body, Post, Get, Param, Put, UseInterceptors, UseFilters, UseGuards, UsePipes, Delete } from '@nestjs/common';
import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { FormationClassesService } from '../../services/formation-classes/formation-classes.service';
import { CreateFormationClass, UpdateFormationClass } from '../../dto/formation-classes.dto';
import { FormationClassesModel } from '../../models/formation-classes.model';

@ApiUseTags('Classes Module')
@Controller('classes/formation-classes')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// @UseGuards(new JwtAuthGuard())
export class FormationClassesController {
    constructor(private readonly formationClasses: FormationClassesService) {}

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: FormationClassesModel })
    async create(@Body() body: CreateFormationClass){
       return this.formationClasses.create(body);
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: FormationClassesModel, isArray: true })
    async list(){
        return this.formationClasses.list();
    }
    
    @Get('admin')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: FormationClassesModel, isArray: true })
    async listAdmin() {
        return this.formationClasses.listAdmin();
    }

    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: FormationClassesModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detail(@Param('id') id) {
        return this.formationClasses.detail(id);
    }

    @Get('admin/:id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: FormationClassesModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detailAdmin() {
        return this.formationClasses.listAdmin();
    }
    
    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: FormationClassesModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(@Param('id')id: string, @Body() body: Partial<UpdateFormationClass>){
        return await this.formationClasses.update(id,body);
    }

    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') id: string){
        return this.formationClasses.delete(id);
    }
}
