import { Test, TestingModule } from '@nestjs/testing';
import { FormationClassesController } from './formation-classes.controller';

describe('FormationClasses Controller', () => {
  let controller: FormationClassesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FormationClassesController],
    }).compile();

    controller = module.get<FormationClassesController>(FormationClassesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
