import { Test, TestingModule } from '@nestjs/testing';
import { ClSequencesHClAttachmentsController } from './cl-sequences-h-cl-attachments.controller';

describe('ClSequencesHClAttachments Controller', () => {
  let controller: ClSequencesHClAttachmentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClSequencesHClAttachmentsController],
    }).compile();

    controller = module.get<ClSequencesHClAttachmentsController>(ClSequencesHClAttachmentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
