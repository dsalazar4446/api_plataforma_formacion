import { Controller, Post, Body, Delete, Param } from '@nestjs/common';
import { ClSequencesHClAttachmentsService } from '../../services/cl-sequences-h-cl-attachments/cl-sequences-h-cl-attachments.service';
import { CreateSequencesAttachmentsDto } from '../../dto/cl-sequences-h-cl-attachments.dto';
import { ApiImplicitParam, ApiImplicitBody, ApiUseTags } from '@nestjs/swagger';

@Controller('sequences-attachments')
@ApiUseTags('Classes Module')
export class ClSequencesHClAttachmentsController {
    constructor(private readonly sequenceAttachment: ClSequencesHClAttachmentsService){}

    @Post()
    create(@Body() body: CreateSequencesAttachmentsDto) {
        return this.sequenceAttachment.create(body);
    }

    @Delete(':id')
    @ApiImplicitParam({name:'id', required: true, type: 'string'})
    delete(@Param('id') id: string){
        return this.sequenceAttachment.delete(id);
    }
}
