import { Controller, UseInterceptors, UseFilters, UseGuards, Post, UsePipes, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { ApiUseTags, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { ClassSequencesService } from '../../services/class-sequences/class-sequences.service';
import { CreateClassSequences, UpdateClassSequences } from '../../dto/class-sequences.dto';
import { ClassSequencesModel } from '../../models/class-sequences.model';

@ApiUseTags('Classes Module')
@Controller('classes/class-sequences')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // // @UseGuards(new JwtAuthGuard())
export class ClassSequencesController {

    constructor(private readonly classSequencesService: ClassSequencesService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: ClassSequencesModel})
    async create(@Body() body: CreateClassSequences) {
        return this.classSequencesService.create(body);
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassSequencesModel, isArray: true })
    async list() {
        return this.classSequencesService.list();
    }

    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassSequencesModel})
    async detail(@Param('id') id) {
        return this.classSequencesService.detail(id);
    }

    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassSequencesModel})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateClassSequences>) {
        return await this.classSequencesService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string) {
        return this.classSequencesService.delete(id);
    }


}
