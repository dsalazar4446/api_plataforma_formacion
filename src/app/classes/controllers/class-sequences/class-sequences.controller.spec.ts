import { Test, TestingModule } from '@nestjs/testing';
import { ClassSequencesController } from './class-sequences.controller';

describe('ClassSequences Controller', () => {
  let controller: ClassSequencesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClassSequencesController],
    }).compile();

    controller = module.get<ClassSequencesController>(ClassSequencesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
