import { Controller, UseInterceptors, UseFilters, UseGuards, Post, UsePipes, Body, Get, Param, Put, Delete, Req } from '@nestjs/common';

import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { ClassHomeworksDetailsService } from '../../services/class-homeworks-details/class-homeworks-details.service';
import { CreateClassHomeworksDetailsDto, UpdateClassHomeworksDetailsDto } from '../../dto/class-homeworks-details.dto';
import { ClassHomeworkDetailsModel } from '../../models/class-homeworks-details.model';

@ApiUseTags('Classes Module')
@Controller('classes/class-homeworks-details')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // // @UseGuards(new JwtAuthGuard())
export class ClassHomeworksDetailsController {

    constructor(private readonly classHomeworksDetailsService: ClassHomeworksDetailsService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language' })
    @ApiResponse({ status: 201, type: ClassHomeworkDetailsModel, isArray: true })
    async create(@Body() body: CreateClassHomeworksDetailsDto, @Req() req) {
        body.languageType = req.headers.language
        return this.classHomeworksDetailsService.create(body);
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassHomeworkDetailsModel, isArray: true })
    async list(@Req() req) {
        return this.classHomeworksDetailsService.list(req.headers.language);
    }


    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassHomeworkDetailsModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detail(@Param('id') id,@Req() req) {
        return this.classHomeworksDetailsService.detail(id,req.headers.language);
    }

    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassHomeworkDetailsModel })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(@Param('id') id: string, @Body() body: Partial<UpdateClassHomeworksDetailsDto>, @Req() req) {
        body.languageType = req.headers.language
        return await this.classHomeworksDetailsService.update(id, body);
    }

    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') id: string) {
        return this.classHomeworksDetailsService.delete(id);
    }

}
