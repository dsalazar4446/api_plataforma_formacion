import { Test, TestingModule } from '@nestjs/testing';
import { ClassHomeworksDetailsController } from './class-homeworks-details.controller';

describe('ClassHomeworksDetails Controller', () => {
  let controller: ClassHomeworksDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClassHomeworksDetailsController],
    }).compile();

    controller = module.get<ClassHomeworksDetailsController>(ClassHomeworksDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
