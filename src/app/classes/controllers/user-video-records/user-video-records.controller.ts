import { Controller, Get, Param, Body, Delete, Put, Post } from '@nestjs/common';
import { ApiUseTags, ApiModelProperty, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { CreateUserVideoRecords, UpdateUserVideoRecords } from '../../dto/user-video-records.dto';
import { UserVideoRecordsService } from '../../services/user-video-records/user-video-records.service';
import { UserVideoRecordsModel } from '../../models/user-video-records.model';

@Controller('user-video-records')
@ApiUseTags('Classes Module')
export class UserVideoRecordsController {
    constructor(private readonly userVideoRecords: UserVideoRecordsService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserVideoRecordsModel, isArray: true })
    async create(@Body() body: CreateUserVideoRecords) {
        return this.userVideoRecords.create(body)
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserVideoRecordsModel, isArray: true })
    async list() {
        return await this.userVideoRecords.list();
    }

    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserVideoRecordsModel, isArray: true })
    @ApiImplicitParam({name:'id', required: true, type:' string'})
    async detail(@Param('id') id: string) {
        return await this.userVideoRecords.detail(id);
    }
    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: UserVideoRecordsModel, isArray: true })
    @ApiImplicitParam({ name: 'id', required: true, type: ' string' })
    async update(@Param('id') id: string, @Body() body: Partial<UpdateUserVideoRecords>) {
        return await this.userVideoRecords.update(id, body);
    }
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: ' string' })
    async delete(@Param('id') id: string) {
        return await this.userVideoRecords.delete(id)
    }
}
