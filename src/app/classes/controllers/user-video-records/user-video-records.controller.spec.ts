import { Test, TestingModule } from '@nestjs/testing';
import { UserVideoRecordsController } from './user-video-records.controller';

describe('UserVideoRecords Controller', () => {
  let controller: UserVideoRecordsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserVideoRecordsController],
    }).compile();

    controller = module.get<UserVideoRecordsController>(UserVideoRecordsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
