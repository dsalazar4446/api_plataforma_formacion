import { Controller, Body, Post, Delete, Param } from '@nestjs/common';
import { ClassSequenceHasClassTextsService } from '../../services/class-sequence-has-class-texts/class-sequence-has-class-texts.service';
import { ApiUseTags } from '@nestjs/swagger';
import { CreateSequenceTextsDto } from '../../dto/class-sequence-has-class-texts.dto';

@Controller('class-sequence-has-class-texts')
@ApiUseTags('Classes Module')
export class ClassSequenceHasClassTextsController {
    constructor(private readonly sequenceText: ClassSequenceHasClassTextsService) {}

    @Post()
    create(@Body() body: CreateSequenceTextsDto) {
        return this.sequenceText.create(body);
    }

    @Delete(':id')
    delete(@Param('id') id: string) {
        return this.sequenceText.delete(id);
    }
}
