import { Test, TestingModule } from '@nestjs/testing';
import { ClassSequenceHasClassTextsController } from './class-sequence-has-class-texts.controller';

describe('ClassSequenceHasClassTexts Controller', () => {
  let controller: ClassSequenceHasClassTextsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClassSequenceHasClassTextsController],
    }).compile();

    controller = module.get<ClassSequenceHasClassTextsController>(ClassSequenceHasClassTextsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
