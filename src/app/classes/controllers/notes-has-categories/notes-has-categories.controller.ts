import { Controller, Post, Body, Delete, Param } from '@nestjs/common';
import { NotesHasCategoriesService } from '../../services/notes-has-categories/notes-has-categories.service';
import { CreateNotesHasCategories } from '../../dto/notes-has-categories.dto';
import { ApiImplicitParam, ApiUseTags } from '@nestjs/swagger';

@Controller('notes-has-categories')
@ApiUseTags('Classes Module')
export class NotesHasCategoriesController {
    constructor(private readonly notesHasCategories: NotesHasCategoriesService) { }
    @Post()
    async create(@Body() body: CreateNotesHasCategories) {
        return this.notesHasCategories.create(body)    
    }

    @Delete(':id')
    @ApiImplicitParam({name: 'id', required: true, type:'string'})
    delete(@Param('id') id: string) {
        return this.notesHasCategories.delete(id);
    }
}
