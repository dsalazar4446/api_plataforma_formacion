import { Test, TestingModule } from '@nestjs/testing';
import { NotesHasCategoriesController } from './notes-has-categories.controller';

describe('NotesHasCategories Controller', () => {
  let controller: NotesHasCategoriesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NotesHasCategoriesController],
    }).compile();

    controller = module.get<NotesHasCategoriesController>(NotesHasCategoriesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
