import { Controller, Post, Delete, Param, Body } from '@nestjs/common';
import { ApiUseTags, ApiImplicitParam } from '@nestjs/swagger';
import { ClassSequencesHasTestsService } from '../../services/class-sequences-has-tests/class-sequences-has-tests.service';
import { create } from 'domain';

@ApiUseTags('Classes Module')
@Controller('class-sequences-has-tests')
export class ClassSequencesHasTestsController {
    constructor(private readonly classSequencesHasTests: ClassSequencesHasTestsService) {}

    @Post()
    create(@Body() body){
        return this.classSequencesHasTests.create(body);
    }
    @Delete('id')
    @ApiImplicitParam({name:'id', required: true, type:'string'})
    delete(@Param('id') id: string){
        return this.classSequencesHasTests.delete(id);
    }
}
