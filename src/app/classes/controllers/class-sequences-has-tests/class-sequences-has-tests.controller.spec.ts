import { Test, TestingModule } from '@nestjs/testing';
import { ClassSequencesHasTestsController } from './class-sequences-has-tests.controller';

describe('ClassSequencesHasTests Controller', () => {
  let controller: ClassSequencesHasTestsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClassSequencesHasTestsController],
    }).compile();

    controller = module.get<ClassSequencesHasTestsController>(ClassSequencesHasTestsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
