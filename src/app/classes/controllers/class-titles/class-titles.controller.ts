
import { Controller, UseInterceptors, UseFilters, UseGuards, Post, UsePipes, Body, Get, Param, Put, Delete, Req } from '@nestjs/common';
import { JwtAuthGuard } from '../../../auths/guards/jwt-auth.guard';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { ClassTitlesService } from '../../services/class-titles/class-titles.service';
import { CreateClassTitlesDto, UpdateClassTitlesDto } from '../../dto/class-titles.dto';
import { ClassTitlesModel } from '../../models/class-titles.model';

@ApiUseTags('Classes Module')
@Controller('classes/class-titles')
@UseInterceptors(new AppResponseInterceptor())
@UseFilters(new AppExceptionFilter())
// // // @UseGuards(new JwtAuthGuard())
export class ClassTitlesController {
    constructor(private readonly classTitlesService: ClassTitlesService) { }

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 201, type: ClassTitlesModel})
    async create(@Body() body: CreateClassTitlesDto, @Req() req) {
        body.languageType = req.headers.language
        return this.classTitlesService.create(body);
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassTitlesModel, isArray: true })
    async list(@Req() req) {
        return this.classTitlesService.list(req.headers.language);
    }


    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassTitlesModel })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detail(@Param('id') id, @Req() req) {
        return this.classTitlesService.detail(id,req.headers.language);
    }

    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: ClassTitlesModel})
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(@Param('id') id: string, @Body() body: Partial<UpdateClassTitlesDto>, @Req() req) {
        body.languageType = req.headers.language
        return await this.classTitlesService.update(id, body);
    }

    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') id: string) {
        return this.classTitlesService.delete(id);
    }

}
