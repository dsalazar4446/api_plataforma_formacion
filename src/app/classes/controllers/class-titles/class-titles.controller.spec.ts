import { Test, TestingModule } from '@nestjs/testing';
import { ClassTitlesController } from './class-titles.controller';

describe('ClassTitles Controller', () => {
  let controller: ClassTitlesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClassTitlesController],
    }).compile();

    controller = module.get<ClassTitlesController>(ClassTitlesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
