import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { UserModel } from './../../user/models/user.Model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'creex_resources',
})
export class Resources extends Model<Resources>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    createbyUserId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'share_type',
    })
    shareType: string;
    
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'resource_type',
    })
    resourceType: string;
    
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        validate: {
            notEmpty: true,
        },
        field: 'resource_title',
    })
    resourceTitle: string;
    
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'resource_description',
    })
    resourceDescription: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('es', 'en', 'it', 'pr'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'language_type',
    })
    languageType: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'resource_attachment',
    })
    resourceAttachment: string;
    
    /**
     * RELACIONES
     * Resources tiene muchos UserModel
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => UserModel)
    userModel: UserModel;
    
    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}
