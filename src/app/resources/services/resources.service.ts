import { Injectable, Inject } from '@nestjs/common';
import { Resources } from '../models/resources.models';
import { ResourceSaveJson, ResourceUpdateJson } from '../interfaces/resource.interfaces';
import { UserModel } from './../../user/models/user.Model';
import * as path from 'path';
import * as fs from 'fs';
import { CONFIG } from "../../../config";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller ResourcesService
 * @Creado 25 de Marzo 2019
 */

@Injectable()
export class ResourcesService {

    private language = ['es', 'en', 'it', 'pr'];

    constructor(
        @Inject('Resources') private readonly _resourceService: typeof Resources
    ) { }

    async saveResource(bodyResource: ResourceSaveJson): Promise<ResourceUpdateJson> {

        const resource = await new this._resourceService(bodyResource).save();

        resource.resourceAttachment = `${CONFIG.storage.server}resourceAttachment/${resource.resourceAttachment}`;

        return resource;
    }

    async showAllResource(languageType: string): Promise<ResourceUpdateJson[]> {

        if (this.language.indexOf(languageType) >= 0) {
            const resource = await this._resourceService.findAll({
                where: {
                    languageType
                }
            });
            resource.forEach(item => item.resourceAttachment = `${CONFIG.storage.server}resourceAttachment/${item.resourceAttachment}`);
            return resource;
        }

        return;
    }

    async getDetails(resourceId: string) {

        const resource = await this._resourceService.findByPk(resourceId, {
            include: [
                {
                    model: UserModel
                }
            ]
        });

        resource.resourceAttachment = `${CONFIG.storage.server}resourceAttachment/${resource.resourceAttachment}`;

        return resource;
    }

    async updateResource(id: string, bodyResource: Partial<ResourceUpdateJson>): Promise<[number, Array<any>]> {

        if (bodyResource.resourceAttachment) {
            await this.deleteImg(id);
        }

        const resource = await this._resourceService.update(bodyResource, {
            where: { id },
            returning: true
        });          
        
        resource[1][0].resourceAttachment = `${CONFIG.storage.server}resourceAttachment/${resource[1][0].resourceAttachment}`;
        return resource;

    }

    async destroyResource(resourceId: string): Promise<number> {

        await this.deleteImg(resourceId);
        return await this._resourceService.destroy({
            where: { id: resourceId },
        });
    }

    async find() {

        const resource = await this._resourceService.findAll({
            include: [
                {
                    model: UserModel
                }
            ]
        });
        resource.forEach(item => item.resourceAttachment = `${CONFIG.storage.server}resourceAttachment/${item.resourceAttachment}`);
        return resource;

    }

    private async deleteImg(id: string) {
        const img = await this._resourceService.findByPk(id).then(item => item.resourceAttachment).catch(err => err);

        const pathImagen = path.resolve(__dirname, `../../../../uploads/resourceAttachment/${img}`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    }


}
