import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus, FileFieldsInterceptor, Req } from '@nestjs/common';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { ResourcesService } from '../services/resources.service';
import { ResourceSaveJson } from '../interfaces/resource.interfaces';
import { ApiUseTags, ApiImplicitParam, ApiImplicitFile, ApiConsumes, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import * as file from './infoFile';
import { Resources } from '../models/resources.models';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller ResourcesController
 * @Creado 25 de Marzo 2019
 */

@ApiUseTags('Module-Resources')
@Controller('resources')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class ResourcesController {

    constructor(private readonly _resourceService: ResourcesService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: Resources,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'resourceAttachment', description: 'Resource Attachment File', required: true })
    async create(@Req() req, @Body() bodyResource: ResourceSaveJson) {

        if (req.headers.language) {
            bodyResource.languageType = req.headers.language;
        } else {
            bodyResource.languageType = 'es';
        }

        const createResource = await this._resourceService.saveResource(bodyResource);

        if (!createResource) {
            throw this.appUtilsService.httpCommonError('resources no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM107',
                languageType: 'es',
            });
        }

        return createResource;

    }


    @ApiResponse({
        status: 200,
        type: Resources,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async find() {
        return await this._resourceService.find();
    }


    @ApiResponse({
        status: 200,
        type: Resources,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idResource: string) {

        const fetchResource = await this._resourceService.getDetails(idResource);

        if (!fetchResource) {
            throw this.appUtilsService.httpCommonError('resources no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM108',
                languageType: 'es',
            });
        }

        return fetchResource;
    }


    @ApiResponse({
        status: 200,
        type: Resources,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showLanguage(@Req() req) {

        const fetch = await this._resourceService.showAllResource(req.headers.language);

        if (!fetch) {
            throw this.appUtilsService.httpCommonError('Idioma no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM3',
                languageType: 'es',
            });
        }

        return fetch;
    }


    @ApiResponse({
        status: 200,
        type: Resources,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'resourceAttachment', description: 'Resource Attachment File', required: true })
    async updateResource(@Req() req, @Param('id') id: string, @Body() bodyResource: Partial<ResourceSaveJson>) {

        if (req.headers.language) {
            bodyResource.languageType = req.headers.language;
        }

        const updateResource = await this._resourceService.updateResource(id, bodyResource);

        if (!updateResource[1][0]) {
            throw this.appUtilsService.httpCommonError('resources no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM108',
                languageType: 'es',
            });
        }

        return updateResource;
    }


    @ApiResponse({
        status: 200,
        type: Resources,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteResource(@Param('id') idResource: string) {

        const deleteResource = await this._resourceService.destroyResource(idResource);

        if (!deleteResource) {
            throw this.appUtilsService.httpCommonError('resources no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM108',
                languageType: 'es',
            });
        }

        return deleteResource;
    }
}
