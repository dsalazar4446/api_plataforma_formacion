import { Module, HttpModule, Provider } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { ResourcesController } from './controllers/resources.controller';
import { ResourcesService } from './services/resources.service';
import { Resources } from './models/resources.models';

const models = [ Resources ];
const controllers = [ ResourcesController ];
const providers: Provider[] = [ ResourcesService ]

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  providers,
  controllers
})
export class ResourcesModule {}
