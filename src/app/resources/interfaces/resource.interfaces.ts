import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional } from 'class-validator';
export class ResourceSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    createbyUserId: string;

    @ApiModelProperty()
    @IsString({message: 'shareType property must a be string'})
    @IsNotEmpty({message: 'shareType property not must null'})
    shareType: string;

    @ApiModelProperty()
    @IsString({message: 'resourceType property must a be string'})
    @IsNotEmpty({message: 'resourceType property not must null'})
    resourceType: string;

    @ApiModelProperty()
    @IsString({message: 'resourceTitle property must a be string'})
    @IsNotEmpty({message: 'resourceTitle property not must null'})
    resourceTitle: string;

    @ApiModelProperty()
    @IsString({message: 'resourceDescription property must a be string'})
    @IsNotEmpty({message: 'resourceDescription property not must null'})
    resourceDescription: string;

    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsString({message: 'languageType property must a be string'})
    @IsOptional()
    languageType: string;

    @ApiModelProperty()
    @IsString({message: 'resourceAttachment property must a be string'})
    @IsNotEmpty({message: 'resourceAttachment property not must null'})
    resourceAttachment: string;
}
export class ResourceUpdateJson extends ResourceSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
