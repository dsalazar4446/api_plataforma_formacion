import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { UserSurveysService } from '../../services/user-surveys/user-surveys.service';
import { UserSurveysSaveJson } from '../../interfaces/user-surveys.interfaces';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from '@nestjs/swagger';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller UserSurveysController
 * @Creado 25 de Marzo 2019
 */

@ApiUseTags('Module-Surveys')
@Controller('user-surveys')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class UserSurveysController {

    constructor(private readonly _userSurveysService: UserSurveysService,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyUserSurveys: UserSurveysSaveJson) {
        const createUserSurveys = await this._userSurveysService.saveUserSurveys(bodyUserSurveys);

        if (!createUserSurveys) {
            return this.appUtilsService.httpCommonError('User Surveys no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM99',
                languageType: 'es',
            });
        }

        return createUserSurveys;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllUserSurveys() {

        return await this._userSurveysService.showAllUserSurveys();
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') userSurveysId: string) {

        const fetchUserSurveys = await this._userSurveysService.getUserSurveys(userSurveysId);

        if (!fetchUserSurveys) {
            throw this.appUtilsService.httpCommonError('User Surveys no existe', HttpStatus.NOT_FOUND, {
                messageCode: 'EM100',
                languageType: 'es',
            });
        }

        return fetchUserSurveys;
    }


    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateUserSurveys(@Param('id') id: string, @Body() bodyUserSurveys: Partial<UserSurveysSaveJson>) {

        const updateUserSurveys = await this._userSurveysService.updateUserSurveys(id, bodyUserSurveys);

        if (!updateUserSurveys[1][0]) {
            throw this.appUtilsService.httpCommonError('User Surveys no existe', HttpStatus.NOT_FOUND, {
                messageCode: 'EM100',
                languageType: 'es',
            });
        }

        return updateUserSurveys;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteUserSurveys(@Param('id') userSurveysId: string) {

        const deleteUserSurveys = await this._userSurveysService.destroyUserSurveys(userSurveysId);

        if (!deleteUserSurveys) {
            throw this.appUtilsService.httpCommonError('User Surveys no existe', HttpStatus.NOT_FOUND, {
                messageCode: 'EM100',
                languageType: 'es',
            });
        }

        return deleteUserSurveys;
    }
}
