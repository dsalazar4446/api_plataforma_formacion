import { Test, TestingModule } from '@nestjs/testing';
import { UserSurveysController } from './user-surveys.controller';

describe('UserSurveys Controller', () => {
  let controller: UserSurveysController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserSurveysController],
    }).compile();

    controller = module.get<UserSurveysController>(UserSurveysController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
