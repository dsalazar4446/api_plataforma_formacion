import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from "@nestjs/swagger";
import { Controller, UseFilters, UseInterceptors, Post, Body, HttpStatus, Delete, Param, NotFoundException, Get } from "@nestjs/common";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { SurveysHasUsersSaveJson } from "../interfaces/surveys-has-users.interface";
import { SurveysHasUsersServices } from "../services/surveys-has-users.service";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller SurveysHasUsersController
 * @Creado 22 Abril 2019
 */

@ApiUseTags('Module-Surveys')
@Controller('surveys-has-users')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class SurveysHasUsersController {

    constructor(private readonly _surveysHasUsersServices: SurveysHasUsersServices,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() body: SurveysHasUsersSaveJson) {

        const create = await this._surveysHasUsersServices.save(body);

        if (!create) {
            throw this.appUtilsService.httpCommonError('SurveysHasUsers no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM105',
                languageType: 'es',
            });
        }

        return create;

    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async getAll() {
        const data = await this._surveysHasUsersServices.getAll();

        if (!data[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return data;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteSurveys(@Param('id') id: string) {

        const deleteSurveys = await this._surveysHasUsersServices.destroy(id);

        if (!deleteSurveys) {
            throw this.appUtilsService.httpCommonError('SurveysHasUsers no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM106',
                languageType: 'es',
            });
        }

        return deleteSurveys;
    }
}
