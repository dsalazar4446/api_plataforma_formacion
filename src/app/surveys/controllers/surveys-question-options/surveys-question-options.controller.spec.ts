import { Test, TestingModule } from '@nestjs/testing';
import { SurveysQuestionOptionsController } from './surveys-question-options.controller';

describe('SurveysQuestionOptions Controller', () => {
  let controller: SurveysQuestionOptionsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SurveysQuestionOptionsController],
    }).compile();

    controller = module.get<SurveysQuestionOptionsController>(SurveysQuestionOptionsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
