import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { SurveysQuestionOptionsService } from '../../services/surveys-question-options/surveys-question-options.service';
import { SurveysQuestionOptionsSaveJson } from '../../interfaces/surveys-question.options.interfaces';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from '@nestjs/swagger';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller SurveysQuestionOptionsController
 * @Creado 25 de Marzo 2019
 */

@ApiUseTags('Module-Surveys')
@Controller('surveys-question-options')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class SurveysQuestionOptionsController {

    constructor(private readonly _surveysQuestionOptionsService: SurveysQuestionOptionsService,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodySurveysQuestionsOpts: SurveysQuestionOptionsSaveJson) {
        const createSurveysQuestionsOpts = await this._surveysQuestionOptionsService.saveSurveysQOpts(bodySurveysQuestionsOpts);

        if (!createSurveysQuestionsOpts) {
            throw this.appUtilsService.httpCommonError('Surveys Questions Opt no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM93',
                languageType: 'es',
            });
        }

        return createSurveysQuestionsOpts;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllSurveysQOpts() {

        return await this._surveysQuestionOptionsService.showAllSurveysQOpts();
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') surveysQuestionsOptsId: string) {

        const fetchSurveysQOpts = await this._surveysQuestionOptionsService.getSurveysQOpts(surveysQuestionsOptsId);

        if (!fetchSurveysQOpts) {
            throw this.appUtilsService.httpCommonError('Surveys Questions Opt no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM94',
                languageType: 'es',
            });
        }

        return fetchSurveysQOpts;
    }


    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateSurveysQOpts(@Param('id') id: string, @Body() bodySurveysQuestionsOpts: Partial<SurveysQuestionOptionsSaveJson>) {

        const updateSurveysQOpts = await this._surveysQuestionOptionsService.updateSurveysQOpts(id, bodySurveysQuestionsOpts);

        if (!updateSurveysQOpts[1][0]) {
            throw this.appUtilsService.httpCommonError('Surveys Questions Opt no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM94',
                languageType: 'es',
            });
        }

        return updateSurveysQOpts;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteSurveysQ(@Param('id') surveysQuestionsOptsId: string) {

        const deleteSurveysQOpts = await this._surveysQuestionOptionsService.destroySurveysQOpts(surveysQuestionsOptsId);

        if (!deleteSurveysQOpts) {
            throw this.appUtilsService.httpCommonError('Surveys Questions Opt no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM94',
                languageType: 'es',
            });
        }

        return deleteSurveysQOpts;
    }
}
