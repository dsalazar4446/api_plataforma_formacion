import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { SurveysQuestionsService } from '../../services/surveys-questions/surveys-questions.service';
import { SurveysQuestionsSaveJson } from '../../interfaces/surveys-questions.interfaces';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from '@nestjs/swagger';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller SurveysQuestionsController
 * @Creado 25 de Marzo 2019
 */

@ApiUseTags('Module-Surveys')
@Controller('surveys-questions')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class SurveysQuestionsController {

    constructor(private readonly _surveyQuestionsServcs: SurveysQuestionsService,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodySurveysQuestions: SurveysQuestionsSaveJson) {
        const createSurveysQuestions = await this._surveyQuestionsServcs.saveSurveysQ(bodySurveysQuestions);

        if (!createSurveysQuestions) {
            throw this.appUtilsService.httpCommonError('Surveys Questions no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM95',
                languageType: 'es',
            });
        }

        return createSurveysQuestions;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') surveysQuestionsId: string) {

        const fetchSurveysQ = await this._surveyQuestionsServcs.getSurveysQ(surveysQuestionsId);

        if (!fetchSurveysQ) {
            throw this.appUtilsService.httpCommonError('Surveys Questions no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM96',
                languageType: 'es',
            });
        }

        return fetchSurveysQ;

    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllSurveysQ() {

        return await this._surveyQuestionsServcs.showAllSurveysQ();
    }

    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateSurveysQ(@Param('id') id: string, @Body() bodySurveysQuestions: Partial<SurveysQuestionsSaveJson>) {

        const updateSurveysQ = await this._surveyQuestionsServcs.updateSurveysQ(id, bodySurveysQuestions);

        if (!updateSurveysQ[1][0]) {
            throw this.appUtilsService.httpCommonError('Surveys Questions no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM96',
                languageType: 'es',
            });
        }

        return updateSurveysQ;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteSurveysQ(@Param('id') surveysQuestionsId: string) {

        const deleteSurveysQ = await this._surveyQuestionsServcs.destroySurveysQ(surveysQuestionsId);

        if (!deleteSurveysQ) {
            throw this.appUtilsService.httpCommonError('Surveys Questions no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM96',
                languageType: 'es',
            });
        }

        return deleteSurveysQ;
    }
}
