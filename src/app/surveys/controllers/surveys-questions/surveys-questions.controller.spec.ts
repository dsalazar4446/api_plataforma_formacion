import { Test, TestingModule } from '@nestjs/testing';
import { SurveysQuestionsController } from './surveys-questions.controller';

describe('SurveysQuestions Controller', () => {
  let controller: SurveysQuestionsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SurveysQuestionsController],
    }).compile();

    controller = module.get<SurveysQuestionsController>(SurveysQuestionsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
