import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from "@nestjs/swagger";
import { Controller, UseFilters, UseInterceptors, Post, Body, HttpStatus, Delete, Param, NotFoundException, Get } from "@nestjs/common";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { SurveysHasRolesServices } from "../services/surveys-has-roles.service";
import { SurveysHasRolesSaveJson } from "../interfaces/surveys-has-roles.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller SurveysHasRolesController
 * @Creado 22 Abril 2019
 */

@ApiUseTags('Module-Surveys')
@Controller('surveys-has-roles')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class SurveysHasRolesController {

    constructor(private readonly _surveysHasRolesServices: SurveysHasRolesServices,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() body: SurveysHasRolesSaveJson) {

        const create = await this._surveysHasRolesServices.save(body);

        if (!create) {
            throw this.appUtilsService.httpCommonError('SurveysHasRoles no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM103',
                languageType: 'es',
            });
        }

        return create;

    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async getAll() {
        const data = await this._surveysHasRolesServices.getAll();

        if (!data[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }
        return data;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteSurveys(@Param('id') id: string) {

        const deleteSurveys = await this._surveysHasRolesServices.destroy(id);

        if (!deleteSurveys) {
            throw this.appUtilsService.httpCommonError('SurveysHasRoles no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM104',
                languageType: 'es',
            });
        }

        return deleteSurveys;
    }
}
