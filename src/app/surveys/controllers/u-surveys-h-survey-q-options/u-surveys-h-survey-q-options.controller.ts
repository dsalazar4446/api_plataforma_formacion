import { Controller, Post, UsePipes, Body, Delete, Param, NotFoundException, Get, Put, UseFilters, UseInterceptors, HttpStatus } from '@nestjs/common';
import { USurveysHSurveyQOptionsService } from '../../services/u-surveys-h-survey-q-options/u-surveys-h-survey-q-options.service';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { USurveysHSurveyQOptionsSaveJson } from '../../interfaces/u-surveys-h-survey-q-options.interfaces';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from '@nestjs/swagger';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller USurveysHSurveyQOptionsController
 * @Creado 25 de Marzo 2019
 */

@ApiUseTags('Module-Surveys')
@Controller('u-surveys-h-survey-q-options')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class USurveysHSurveyQOptionsController {

    constructor(private readonly _uSurveysHSurveyQOptionsService: USurveysHSurveyQOptionsService,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodyUSurveysQO: USurveysHSurveyQOptionsSaveJson) {
        const createUSurveysQO = await this._uSurveysHSurveyQOptionsService.save(bodyUSurveysQO);

        if (!createUSurveysQO) {
            return this.appUtilsService.httpCommonError('USurveysHSurveyQOptions no ha sido creadoc!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM97',
                languageType: 'es',
            });
        }

        return createUSurveysQO;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAll() {

        return await this._uSurveysHSurveyQOptionsService.showAll();
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') uSurveysQOId: string) {

        const fetchUSurveysQ = await this._uSurveysHSurveyQOptionsService.getOne(uSurveysQOId);

        if (!fetchUSurveysQ) {
            throw this.appUtilsService.httpCommonError('USurveysHSurveyQOptions Opt no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM98',
                languageType: 'es',
            });
        }

        return fetchUSurveysQ;
    }


    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateUSurveysQO(@Param('id') id: string, @Body() bodyUSurveysQO: Partial<USurveysHSurveyQOptionsSaveJson>) {

        const updateUSurveysQO = await this._uSurveysHSurveyQOptionsService.update(id, bodyUSurveysQO);

        if (!updateUSurveysQO[1][0]) {
            throw this.appUtilsService.httpCommonError('USurveysHSurveyQOptions Opt no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM98',
                languageType: 'es',
            });
        }

        return updateUSurveysQO;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteSurveysQ(@Param('id') uSurveysQOId: string) {

        const deleteUSurveysQ = await this._uSurveysHSurveyQOptionsService.destroy(uSurveysQOId);

        if (!deleteUSurveysQ) {
            throw this.appUtilsService.httpCommonError('USurveysHSurveyQOptions Opt no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM98',
                languageType: 'es',
            });
        }

        return deleteUSurveysQ;
    }
}
