import { Test, TestingModule } from '@nestjs/testing';
import { USurveysHSurveyQOptionsController } from './u-surveys-h-survey-q-options.controller';

describe('USurveysHSurveyQOptions Controller', () => {
  let controller: USurveysHSurveyQOptionsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [USurveysHSurveyQOptionsController],
    }).compile();

    controller = module.get<USurveysHSurveyQOptionsController>(USurveysHSurveyQOptionsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
