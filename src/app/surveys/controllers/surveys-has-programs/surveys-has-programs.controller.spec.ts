import { Test, TestingModule } from '@nestjs/testing';
import { SurveysHasProgramsController } from './surveys-has-programs.controller';

describe('SurveysHasPrograms Controller', () => {
  let controller: SurveysHasProgramsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SurveysHasProgramsController],
    }).compile();

    controller = module.get<SurveysHasProgramsController>(SurveysHasProgramsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
