import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { SurveysHasProgramsService } from '../../services/surveys-has-programs/surveys-has-programs.service';
import { SurveysHasProgramsSaveJson } from '../../interfaces/surveys-has-programs.interfaces';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from '@nestjs/swagger';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller SurveysHasProgramsController
 * @Creado 25 de Marzo 2019
 */

@ApiUseTags('Module-Surveys')
@Controller('surveys-has-programs')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class SurveysHasProgramsController {

    constructor(private readonly _surveysHasProgramsService: SurveysHasProgramsService,
        private readonly appUtilsService: AppUtilsService) { }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Body() bodySurveysHPrograms: SurveysHasProgramsSaveJson) {
        const createSurveysHPrograms = await this._surveysHasProgramsService.saveSurveysHPrograms(bodySurveysHPrograms);

        if (!createSurveysHPrograms) {
            throw this.appUtilsService.httpCommonError('Surveys has programs no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM91',
                languageType: 'es',
            });
        }

        return createSurveysHPrograms;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showAllSurveysHPrograms() {

        return await this._surveysHasProgramsService.showAllSurveysHPrograms();
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') surveyshProgramsId: string) {

        const fetchSurveysHPrograms = await this._surveysHasProgramsService.getSurveysHPrograms(surveyshProgramsId);

        if (!fetchSurveysHPrograms) {
            throw this.appUtilsService.httpCommonError('Surveys has programs no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM92',
                languageType: 'es',
            });
        }

        return fetchSurveysHPrograms;
    }


    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateSurveysHPrograms(@Param('id') id: string, @Body() bodySurveysHPrograms: SurveysHasProgramsSaveJson) {

        const updateSurveysHPrograms = await this._surveysHasProgramsService.updateSurveysHPrograms(id, bodySurveysHPrograms);

        if (!updateSurveysHPrograms[1][0]) {
            throw this.appUtilsService.httpCommonError('Surveys has programs no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM92',
                languageType: 'es',
            });
        }

        return updateSurveysHPrograms;
    }
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteSurveysHPrograms(@Param('id') surveyshProgramsId: string) {

        const deleteSurveysHPrograms = await this._surveysHasProgramsService.destroySurveysHPrograms(surveyshProgramsId);

        if (!deleteSurveysHPrograms) {
            throw this.appUtilsService.httpCommonError('Surveys has programs no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM92',
                languageType: 'es',
            });
        }

        return deleteSurveysHPrograms;
    }


}
