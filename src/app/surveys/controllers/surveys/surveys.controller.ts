import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Get, Param, NotFoundException, Put, Delete, HttpStatus, Req } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { SurveysService } from '../../services/surveys/surveys.service';
import { SurveysSaveJson, SurveysUpdateJson } from '../../interfaces/surveys.interfaces';
import { ApiUseTags, ApiImplicitParam, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { Surveys } from '../../models/surveys.models';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller SurveysController
 * @Creado 25 de Marzo 2019
 */

@ApiUseTags('Module-Surveys')
@Controller('surveys')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class SurveysController {

    constructor(private readonly _surveysService: SurveysService,
        private readonly appUtilsService: AppUtilsService) { }


    @ApiResponse({
        status: 200,
        type: Surveys,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Post()
    async create(@Req() req, @Body() bodySurveys: SurveysSaveJson) {

        if (req.headers.language) {
            bodySurveys.languageType = req.headers.language;
        } else {
            bodySurveys.languageType = 'es';
        }

        const createSurveys = await this._surveysService.saveSurveys(bodySurveys);

        if (!createSurveys) {
            throw this.appUtilsService.httpCommonError('Surveys no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM89',
                languageType: 'es',
            });
        }

        return createSurveys;
    }

    @ApiResponse({
        status: 200,
        type: Surveys,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('list')
    async showAll() {
        const fetch = await this._surveysService.find();

        if (!fetch[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetch;
    }


    @ApiResponse({
        status: 200,
        type: Surveys,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get()
    async showLanguage(@Req() req) {

        const fetch = await this._surveysService.showAllSurveys(req.headers.language);

        if (!fetch[0]) {
            throw this.appUtilsService.httpCommonError('No existen registros!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM4',
                languageType: 'es',
            });
        }

        return fetch;
    }


    @ApiResponse({
        status: 200,
        type: Surveys,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async getSurveysDetails(@Param('id') idSurveys: string) {

        const fetchSurveys = await this._surveysService.getSurveysDetails(idSurveys);

        if (!fetchSurveys) {
            throw this.appUtilsService.httpCommonError('Surveys no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM90',
                languageType: 'es',
            });
        }

        return fetchSurveys;
    }


    @ApiResponse({
        status: 200,
        type: Surveys,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updateSurveys(@Req() req, @Param('id') id: string, @Body() bodySurveys: Partial<SurveysUpdateJson>) {

        if (req.headers.language) {
            bodySurveys.languageType = req.headers.language;
        }

        const updateSurveys = await this._surveysService.updateSurveys(id, bodySurveys);

        if (!updateSurveys[1][0]) {
            throw this.appUtilsService.httpCommonError('Surveys no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM90',
                languageType: 'es',
            });
        }

        return updateSurveys;
    }


    @ApiResponse({
        status: 200,
        type: Surveys,
        isArray: true
    })
    @ApiImplicitHeader({
        name: 'language',
        required: true,
    })
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deleteTransaction(@Param('id') idSurveys: string) {

        const deleteSurveys = await this._surveysService.destroySurveys(idSurveys);

        if (!deleteSurveys) {
            throw this.appUtilsService.httpCommonError('Surveys no existe!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM90',
                languageType: 'es',
            });
        }
        return deleteSurveys;
    }
}
