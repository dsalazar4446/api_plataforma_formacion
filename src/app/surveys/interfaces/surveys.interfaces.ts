import { ApiModelProperty } from '@nestjs/swagger';
import { IsUUID, IsNotEmpty, IsString, IsOptional } from 'class-validator';
export class SurveysSaveJson {

    @ApiModelProperty()
    @IsString({message: 'surveyTitle property must a be string'})
    @IsNotEmpty({message: 'surveyTitle property not must null'})
    surveyTitle: string;

    @ApiModelProperty()
    @IsString({message: 'surveyDescription property must a be string'})
    @IsNotEmpty({message: 'surveyDescription property not must null'})
    surveyDescription: string;

    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsString({message: 'languageType property must a be string'})
    @IsOptional()
    languageType: string;
}
export class SurveysUpdateJson extends SurveysSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
