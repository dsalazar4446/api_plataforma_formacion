import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class SurveysHasUsersSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'surveyId property must a be uuid'})
    @IsNotEmpty({message: 'surveyId property not must null'})
    surveyId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    userId: string;
}
