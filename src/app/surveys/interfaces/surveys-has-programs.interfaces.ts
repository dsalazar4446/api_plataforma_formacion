import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class SurveysHasProgramsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'surveyId property must a be uuid'})
    @IsNotEmpty({message: 'surveyId property not must null'})
    surveyId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'programId property must a be uuid'})
    @IsNotEmpty({message: 'programId property not must null'})
    programId: string;
}
export class SurveysHasProgramsUpdateJson extends SurveysHasProgramsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
