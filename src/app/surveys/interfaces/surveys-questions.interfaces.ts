import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsUUID, IsNumber } from 'class-validator';
export class SurveysQuestionsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'surveyId property must a be uuid'})
    @IsNotEmpty({message: 'surveyId property not must null'})
    surveyId: string;

    @ApiModelProperty()
    @IsString({message: 'question property must a be string'})
    @IsNotEmpty({message: 'question property not must null'})
    question: string;

    @ApiModelProperty()
    @IsNumber(null,{message: 'questionType property must a be number'})
    @IsNotEmpty({message: 'questionType property not must null'})
    questionType: number;
}
export class SurveysQuestionsUpdateJson extends SurveysQuestionsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
