import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsUUID } from 'class-validator';
export class SurveysQuestionOptionsSaveJson {
    @ApiModelProperty()
    @IsUUID('4',{message: 'surveyQuestionId property must a be uuid'})
    @IsNotEmpty({message: 'surveyQuestionId property not must null'})
    surveyQuestionId: string;

    @ApiModelProperty()
    @IsString({message: 'questionOption property must a be string'})
    @IsNotEmpty({message: 'questionOption property not must null'})
    questionOption: string;
}
export class SurveysQuestionOptionsUpdateJson extends SurveysQuestionOptionsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
