import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class SurveysHasCoursesSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'surveyId property must a be uuid'})
    @IsNotEmpty({message: 'surveyId property not must null'})
    surveyId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'courseId property must a be uuid'})
    @IsNotEmpty({message: 'courseId property not must null'})
    courseId: string;
}