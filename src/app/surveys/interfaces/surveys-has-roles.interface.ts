import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class SurveysHasRolesSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'surveyId property must a be uuid'})
    @IsNotEmpty({message: 'surveyId property not must null'})
    surveyId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'rolesId property must a be uuid'})
    @IsNotEmpty({message: 'rolesId property not must null'})
    rolesId: string;
}
export class SurveysHasRolesUpdateJson extends SurveysHasRolesSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
