import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class UserSurveysSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'userId property must a be uuid'})
    @IsNotEmpty({message: 'userId property not must null'})
    userId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'surveyId property must a be uuid'})
    @IsNotEmpty({message: 'surveyId property not must null'})
    surveyId: string;
}
export class UserSurveysUpdateJson extends UserSurveysSaveJson{
    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
