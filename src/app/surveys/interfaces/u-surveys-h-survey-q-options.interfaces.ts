import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class USurveysHSurveyQOptionsSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'userSurveyId property must a be uuid'})
    @IsNotEmpty({message: 'userSurveyId property not must null'})
    userSurveyId: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'surveyQuestionOptionId property must a be uuid'})
    @IsNotEmpty({message: 'surveyQuestionOptionId property not must null'})
    surveyQuestionOptionId: string;
}
export class USurveysHSurveyQOptionsUpdateJson extends USurveysHSurveyQOptionsSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
