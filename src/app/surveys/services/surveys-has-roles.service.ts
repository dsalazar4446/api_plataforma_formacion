import { Injectable, Inject } from "@nestjs/common";
import { SurveysHasRoles } from "../models/surveys-has-roles.model";
import { SurveysHasRolesSaveJson } from "../interfaces/surveys-has-roles.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services SurveysHasRolesServices
 * @Creado 22 de abril 2019
 */

@Injectable()
export class SurveysHasRolesServices {

    constructor(
        @Inject('SurveysHasRoles') private readonly _surveysHasRoles: typeof SurveysHasRoles
    ) { }

    async save(body: SurveysHasRolesSaveJson): Promise<SurveysHasRolesSaveJson> {

        return await new this._surveysHasRoles(body).save();
    }

    async getAll(): Promise<SurveysHasRolesSaveJson[]> {

        return await this._surveysHasRoles.findAll();
    }

    async destroy(id: string): Promise<number> {

        return await this._surveysHasRoles.destroy({
            where: { id },
        });
    }

}