import { Injectable, Inject } from '@nestjs/common';
import { UserSurveys} from '../../models/user-surveys.models';
import { SurveyQuestionOptions } from '../../models/surveys-question-options.models';
import { UserSurveysSaveJson, UserSurveysUpdateJson } from '../../interfaces/user-surveys.interfaces';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service UserSurveysService
 * @Creado 25 de Marzo 2019
 */

@Injectable()
export class UserSurveysService {

    constructor(
        @Inject('UserSurveys') private readonly _userSurveys: typeof UserSurveys
    ){}

    async saveUserSurveys(bodyUserSurveys: UserSurveysSaveJson): Promise<UserSurveysUpdateJson> {

        return await new this._userSurveys(bodyUserSurveys).save();
    }

    async showAllUserSurveys(): Promise<UserSurveysUpdateJson[]> {

        return await this._userSurveys.findAll();
    }

    async getUserSurveys(userSurveysId: string): Promise<UserSurveysUpdateJson> {

        return await this._userSurveys.findByPk(userSurveysId, {
            include: [
                {
                    model: SurveyQuestionOptions
                }
            ]
        });
    }

    async updateUserSurveys(id: string, bodyUserSurveys: Partial<UserSurveysUpdateJson>): Promise<[number, Array<any>]> {

        return await this._userSurveys.update(bodyUserSurveys, { 
            where: { id } ,
            returning: true
        });
    }

    async destroyUserSurveys(userSurveysId: string): Promise<number> {

        return await this._userSurveys.destroy({
            where: {id: userSurveysId},
        });
    }

}
