import { Injectable, Inject } from "@nestjs/common";
import { SurveysHasUsers } from "../models/surveys-has-users.model";
import { SurveysHasUsersSaveJson } from "../interfaces/surveys-has-users.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services SurveysHasUsersServices
 * @Creado 22 de abril 2019
 */

@Injectable()
export class SurveysHasUsersServices {

    constructor(
        @Inject('SurveysHasUsers') private readonly _surveysHasUsers: typeof SurveysHasUsers
    ) { }

    async save(body: SurveysHasUsersSaveJson): Promise<SurveysHasUsersSaveJson> {

        return await new this._surveysHasUsers(body).save();
    }

    async getAll(): Promise<SurveysHasUsersSaveJson[]> {

        return await this._surveysHasUsers.findAll();
    }

    async destroy(id: string): Promise<number> {

        return await this._surveysHasUsers.destroy({
            where: { id },
        });
    }

}