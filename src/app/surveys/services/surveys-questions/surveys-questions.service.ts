import { Injectable, Inject } from '@nestjs/common';
import { SurveyQuestions } from '../../models/surveys-questions.models';
import { SurveysQuestionsSaveJson, SurveysQuestionsUpdateJson } from '../../interfaces/surveys-questions.interfaces';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service SurveysQuestionsService
 * @Creado 25 de Marzo 2019
 */

@Injectable()
export class SurveysQuestionsService {

    constructor(
        @Inject('SurveyQuestions') private readonly _surveysQuestionsServices: typeof SurveyQuestions
    ){}

    async saveSurveysQ(bodySurveysQ: SurveysQuestionsSaveJson): Promise<SurveysQuestionsUpdateJson> {

        return await new this._surveysQuestionsServices(bodySurveysQ).save();
    }

    async showAllSurveysQ(): Promise<SurveysQuestionsUpdateJson[]> {

        return await this._surveysQuestionsServices.findAll();
    }

    async getSurveysQ(surveysQId: string): Promise<SurveysQuestionsUpdateJson> {

        return await this._surveysQuestionsServices.findByPk(surveysQId);
    }

    async updateSurveysQ(id: string, bodySurveysQ: Partial<SurveysQuestionsUpdateJson>): Promise<[number, Array<any>]> {

        return await this._surveysQuestionsServices.update(bodySurveysQ, { 
            where: { id } ,
            returning: true
        });
    }

    async destroySurveysQ(surveysQId: string): Promise<number> {

        return await this._surveysQuestionsServices.destroy({
            where: {id: surveysQId},
        });
    }
}
