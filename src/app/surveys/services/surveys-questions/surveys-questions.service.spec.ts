import { Test, TestingModule } from '@nestjs/testing';
import { SurveysQuestionsService } from './surveys-questions.service';

describe('SurveysQuestionsService', () => {
  let service: SurveysQuestionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SurveysQuestionsService],
    }).compile();

    service = module.get<SurveysQuestionsService>(SurveysQuestionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
