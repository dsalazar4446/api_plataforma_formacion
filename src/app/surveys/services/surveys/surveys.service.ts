import { Injectable, Inject } from '@nestjs/common';
import { Surveys } from '../../models/surveys.models';
import { SurveyQuestions } from '../../models/surveys-questions.models';
import { UserSurveys } from '../../models/user-surveys.models';
import { SurveysSaveJson, SurveysUpdateJson } from '../../interfaces/surveys.interfaces';
import { ProgramsModel } from '../../../programs/models/programs.model';
import { RolesModel } from '../../../roles/models/roles.models';
import { CoursesModel } from '../../../courses/models/courses.model';
import { UserModel } from '../../../user/models/user.Model';
import { SurveyQuestionOptions } from '../../models/surveys-question-options.models';
/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service SurveysService
 * @Creado 25 de Marzo 2019
 */

@Injectable()
export class SurveysService {

    private language = ['es', 'en', 'it', 'pr'];

    private include = {
        include: [
            {
                model: SurveyQuestions,
                include: [SurveyQuestionOptions]
            },
            {
                model: UserSurveys
            },
            {
                model: ProgramsModel
            },
            {
                model: RolesModel
            },
            {
                model: CoursesModel
            },
            {
                model: UserModel
            }
        ]
    };

    constructor(
        @Inject('Surveys') private readonly _surveysServices: typeof Surveys
    ) { }

    async saveSurveys(bodySurveys: SurveysSaveJson): Promise<SurveysUpdateJson> {

        return await new this._surveysServices(bodySurveys).save();
    }

    async showAllSurveys(languageType: string): Promise<SurveysUpdateJson[]> {


        if (this.language.indexOf(languageType) >= 0) {

            return await this._surveysServices.findAll({
                include: [
                    {
                        model: SurveyQuestions,
                        include: [SurveyQuestionOptions]
                    },
                    {
                        model: UserSurveys
                    },
                    {
                        model: ProgramsModel
                    },
                    {
                        model: RolesModel
                    },
                    {
                        model: CoursesModel
                    },
                    {
                        model: UserModel
                    }
                ],
                where: {
                    languageType
                },

            });
        }

        return;
    }

    async getSurveysDetails(surveysId: string): Promise<any> {

        return await this._surveysServices.findByPk(surveysId, this.include);
    }

    async updateSurveys(id: string, bodySurveys: Partial<SurveysUpdateJson>): Promise<[number, Array<any>]> {

        return await this._surveysServices.update(bodySurveys, {
            where: { id },
            returning: true
        });
    }

    async destroySurveys(surveysId: string): Promise<number> {

        return await this._surveysServices.destroy({
            where: { id: surveysId },
        });
    }

    async find() {
        return await this._surveysServices.findAll(this.include);
    }

}
