import { Injectable, Inject } from "@nestjs/common";
import { SurveysHasCourses } from "../models/surveys-has-courses.model";
import { SurveysHasCoursesSaveJson } from "../interfaces/surveys-has-courses.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services SurveysHasCoursesServices
 * @Creado 22 de abril 2019
 */

@Injectable()
export class SurveysHasCoursesServices {

    constructor(
        @Inject('SurveysHasCourses') private readonly _surveysHasCourses: typeof SurveysHasCourses
    ) { }

    async save(body: SurveysHasCoursesSaveJson): Promise<SurveysHasCoursesSaveJson> {

        return await new this._surveysHasCourses(body).save();
    }

    async getAll(): Promise<SurveysHasCoursesSaveJson[]> {

        return await this._surveysHasCourses.findAll();
    }

    async destroy(id: string): Promise<number> {

        return await this._surveysHasCourses.destroy({
            where: { id },
        });
    }

}