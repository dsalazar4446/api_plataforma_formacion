import { Test, TestingModule } from '@nestjs/testing';
import { USurveysHSurveyQOptionsService } from './u-surveys-h-survey-q-options.service';

describe('USurveysHSurveyQOptionsService', () => {
  let service: USurveysHSurveyQOptionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [USurveysHSurveyQOptionsService],
    }).compile();

    service = module.get<USurveysHSurveyQOptionsService>(USurveysHSurveyQOptionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
