import { Injectable, Inject } from '@nestjs/common';
import { USurveysHSurveyQOptions } from '../../models/u-surveys-h-survey-q-options.models';
import { USurveysHSurveyQOptionsSaveJson, USurveysHSurveyQOptionsUpdateJson } from '../../interfaces/u-surveys-h-survey-q-options.interfaces';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service USurveysHSurveyQOptionsService
 * @Creado 25 de Marzo 2019
 */

@Injectable()
export class USurveysHSurveyQOptionsService {

    constructor(
        @Inject('USurveysHSurveyQOptions') private readonly _uSHSQOptsServcs: typeof USurveysHSurveyQOptions
    ) { }

    async save(bodyUserSurveys: USurveysHSurveyQOptionsSaveJson): Promise<USurveysHSurveyQOptionsUpdateJson> {

        return await new this._uSHSQOptsServcs(bodyUserSurveys).save();
    }

    async showAll(): Promise<USurveysHSurveyQOptionsUpdateJson[]> {

        return await this._uSHSQOptsServcs.findAll();
    }

    async getOne(userSurveysId: string): Promise<USurveysHSurveyQOptionsUpdateJson> {

        return await this._uSHSQOptsServcs.findByPk(userSurveysId);
    }

    async update(id: string, bodyUserSurveys: Partial<USurveysHSurveyQOptionsUpdateJson>): Promise<[number, Array<any>]> {

        return await this._uSHSQOptsServcs.update(bodyUserSurveys, {
            where: { id },
            returning: true
        });
    }

    async destroy(userSurveysId: string): Promise<number> {

        return await this._uSHSQOptsServcs.destroy({
            where: {id: userSurveysId},
        });
    }
}
