import { Injectable, Inject } from '@nestjs/common';
import { SurveysHasPrograms } from '../../models/surveys-has-programs.models';
import { SurveysHasProgramsSaveJson, SurveysHasProgramsUpdateJson } from '../../interfaces/surveys-has-programs.interfaces';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service SurveysHasProgramsService
 * @Creado 25 de Marzo 2019
 */

@Injectable()
export class SurveysHasProgramsService {

    constructor(
        @Inject('SurveysHasPrograms') private readonly _surveysHasPrograms: typeof SurveysHasPrograms
    ){}

    async saveSurveysHPrograms(bodySurveysHPrograms: SurveysHasProgramsSaveJson): Promise<SurveysHasProgramsUpdateJson> {

        return await new this._surveysHasPrograms(bodySurveysHPrograms).save();
    }

    async showAllSurveysHPrograms(): Promise<SurveysHasProgramsUpdateJson[]> {

        return await this._surveysHasPrograms.findAll();
    }

    async getSurveysHPrograms(surveysHasProgramsId: string): Promise<SurveysHasProgramsUpdateJson> {

        return await this._surveysHasPrograms.findByPk(surveysHasProgramsId);
    }

    async updateSurveysHPrograms(id: string, bodySurveysHPrograms: Partial<SurveysHasProgramsUpdateJson>): Promise<[number, Array<any>]> {

        return await this._surveysHasPrograms.update(bodySurveysHPrograms, { 
            where: { id } ,
            returning: true
        });
    }

    async destroySurveysHPrograms(surveysHasProgramsId: string): Promise<number> {

        return await this._surveysHasPrograms.destroy({
            where: {id: surveysHasProgramsId},
        });
    }
}
