import { Test, TestingModule } from '@nestjs/testing';
import { SurveysHasProgramsService } from './surveys-has-programs.service';

describe('SurveysHasProgramsService', () => {
  let service: SurveysHasProgramsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SurveysHasProgramsService],
    }).compile();

    service = module.get<SurveysHasProgramsService>(SurveysHasProgramsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
