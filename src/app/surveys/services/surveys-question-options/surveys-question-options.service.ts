import { Injectable, Inject } from '@nestjs/common';
import { SurveyQuestionOptions} from '../../models/surveys-question-options.models';
import { UserSurveys } from '../../models/user-surveys.models';
import { SurveysQuestionOptionsSaveJson, SurveysQuestionOptionsUpdateJson } from '../../interfaces/surveys-question.options.interfaces';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Service SurveysQuestionOptionsService
 * @Creado 25 de Marzo 2019
 */

@Injectable()
export class SurveysQuestionOptionsService {

    constructor(
        @Inject('SurveyQuestionOptions') private readonly _surveyQuestionOptionsSercs: typeof SurveyQuestionOptions
    ) { }

    async saveSurveysQOpts(bodySurveysQOpts: SurveysQuestionOptionsSaveJson): Promise<SurveysQuestionOptionsUpdateJson> {

        return await new this._surveyQuestionOptionsSercs(bodySurveysQOpts).save();
    }

    async showAllSurveysQOpts(): Promise<SurveysQuestionOptionsUpdateJson[]> {

        return await this._surveyQuestionOptionsSercs.findAll();
    }

    async getSurveysQOpts(surveysQOptsId: string): Promise<SurveysQuestionOptionsUpdateJson> {

        return await this._surveyQuestionOptionsSercs.findByPk(surveysQOptsId,{
            include: [
                {
                    model: UserSurveys
                }
            ]
        });
    }

    async updateSurveysQOpts(id: string, bodySurveysQOpts: Partial<SurveysQuestionOptionsUpdateJson>): Promise<[number, Array<any>]> {

        return await this._surveyQuestionOptionsSercs.update(bodySurveysQOpts, { 
            where: { id } ,
            returning: true
        });
    }

    async destroySurveysQOpts(surveysQOptsId: string): Promise<number> {

        return await this._surveyQuestionOptionsSercs.destroy({
            where: {id: surveysQOptsId},
        });
    }
}
