import { Test, TestingModule } from '@nestjs/testing';
import { SurveysQuestionOptionsService } from './surveys-question-options.service';

describe('SurveysQuestionOptionsService', () => {
  let service: SurveysQuestionOptionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SurveysQuestionOptionsService],
    }).compile();

    service = module.get<SurveysQuestionOptionsService>(SurveysQuestionOptionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
