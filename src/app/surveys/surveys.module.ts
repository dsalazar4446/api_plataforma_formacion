import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { Module, HttpModule, Provider } from '@nestjs/common';
import { SurveyQuestionOptions } from './models/surveys-question-options.models';
import { SurveyQuestions } from './models/surveys-questions.models';
import { Surveys } from './models/surveys.models';
import { SurveysHasPrograms } from './models/surveys-has-programs.models';
import { UserSurveys } from './models/user-surveys.models';
import { USurveysHSurveyQOptions } from './models/u-surveys-h-survey-q-options.models';
import { SurveysHasCourses } from './models/surveys-has-courses.model';
import { SurveysHasRoles } from './models/surveys-has-roles.model';
import { SurveysHasUsers } from './models/surveys-has-users.model';

import { SurveysService } from './services/surveys/surveys.service';
import { SurveysQuestionsService } from './services/surveys-questions/surveys-questions.service';
import { SurveysQuestionOptionsService } from './services/surveys-question-options/surveys-question-options.service';
import { SurveysHasProgramsService } from './services/surveys-has-programs/surveys-has-programs.service';
import { UserSurveysService } from './services/user-surveys/user-surveys.service';
import { USurveysHSurveyQOptionsService } from './services/u-surveys-h-survey-q-options/u-surveys-h-survey-q-options.service';
import { SurveysHasCoursesServices } from './services/surveys-has-courses.service';
import { SurveysHasRolesServices } from './services/surveys-has-roles.service';
import { SurveysHasUsersServices } from './services/surveys-has-users.service';

import { SurveysController } from './controllers/surveys/surveys.controller';
import { SurveysQuestionsController } from './controllers/surveys-questions/surveys-questions.controller';
import { SurveysQuestionOptionsController } from './controllers/surveys-question-options/surveys-question-options.controller';
import { UserSurveysController } from './controllers/user-surveys/user-surveys.controller';
import { USurveysHSurveyQOptionsController } from './controllers/u-surveys-h-survey-q-options/u-surveys-h-survey-q-options.controller';
import { SurveysHasProgramsController } from './controllers/surveys-has-programs/surveys-has-programs.controller';
import { SurveysHasCoursesController } from './controllers/surveys-has-courses.controller';
import { SurveysHasRolesController } from './controllers/surveys-has-roles.controller';
import { SurveysHasUsersController } from './controllers/surveys-has-users.controller';


const models = [
  SurveyQuestionOptions,
  SurveyQuestions,
  Surveys,
  SurveysHasPrograms,
  UserSurveys,
  USurveysHSurveyQOptions,
  SurveysHasCourses,
  SurveysHasRoles,
  SurveysHasUsers
];

const providers: Provider[] = [
  SurveysService,
  SurveysQuestionsService,
  SurveysQuestionOptionsService,
  SurveysHasProgramsService,
  UserSurveysService,
  USurveysHSurveyQOptionsService,
  SurveysHasCoursesServices,
  SurveysHasRolesServices,
  SurveysHasUsersServices
];

const controllers = [
  SurveysController,
  SurveysQuestionsController,
  SurveysQuestionOptionsController,
  UserSurveysController,
  USurveysHSurveyQOptionsController,
  SurveysHasProgramsController,
  SurveysHasCoursesController,
  SurveysHasRolesController,
  SurveysHasUsersController
];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers,
})
export class SurveysModule { }
