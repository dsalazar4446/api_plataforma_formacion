import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { UserSurveys } from './user-surveys.models';
import { SurveyQuestionOptions } from './surveys-question-options.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'u_surveys_h_survey_q_options',
})
export class USurveysHSurveyQOptions extends Model<USurveysHSurveyQOptions>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserSurveys)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_survey_id',
    })
    userSurveyId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => SurveyQuestionOptions)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'survey_question_option_id',
    })
    surveyQuestionOptionId: string;
    
    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}