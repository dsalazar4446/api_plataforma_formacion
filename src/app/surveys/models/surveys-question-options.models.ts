import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo, HasMany, BelongsToMany } from 'sequelize-typescript';
import { SurveyQuestions } from './surveys-questions.models';
import { USurveysHSurveyQOptions } from './u-surveys-h-survey-q-options.models';
import { UserSurveys } from './user-surveys.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'survey_question_options',
})
export class SurveyQuestionOptions extends Model<SurveyQuestionOptions>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => SurveyQuestions)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'survey_question_id',
    })
    surveyQuestionId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'question_option',
    })
    questionOption: string;

    /**
     * RELACIONES
     * SurveyQuestionOptions pertenece a SurveyQuestions
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => SurveyQuestions)
    surveyQuestions: SurveyQuestions;

    /**
     * RELACIONES
     * SurveyQuestionOptions tiene muchos UserSurveys
     */    
    @ApiModelPropertyOptional()
    @BelongsToMany(() => UserSurveys, () => USurveysHSurveyQOptions)
    userSurveys: UserSurveys[];

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}