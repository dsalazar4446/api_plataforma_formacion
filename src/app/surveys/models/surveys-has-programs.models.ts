import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Surveys } from './surveys.models';
import { ProgramsModel } from './../../programs/models/programs.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'surveys_has_programs',
})
export class SurveysHasPrograms extends Model<SurveysHasPrograms>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Surveys)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'survey_id',
    })
    surveyId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => ProgramsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'program_id',
    })
    programId: string;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}