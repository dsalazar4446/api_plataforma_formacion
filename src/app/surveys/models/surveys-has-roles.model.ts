import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey } from 'sequelize-typescript';
import { Surveys } from './surveys.models';
import { RolesModel } from '../../roles/models/roles.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'surveys_has_roles',
})
export class SurveysHasRoles extends Model<SurveysHasRoles>{


    @ApiModelPropertyOptional()
    @ForeignKey(() => Surveys)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'survey_id',
    })
    surveyId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => RolesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'roles_id',
    })
    rolesId: string;
}