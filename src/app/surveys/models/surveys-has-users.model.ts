import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey } from 'sequelize-typescript';
import { Surveys } from './surveys.models';
import { UserModel } from '../../user/models/user.Model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'surveys_has_users',
})
export class SurveysHasUsers extends Model<SurveysHasUsers>{


    @ApiModelPropertyOptional()
    @ForeignKey(() => Surveys)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'survey_id',
    })
    surveyId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;
}