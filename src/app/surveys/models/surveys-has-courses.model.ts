import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey } from 'sequelize-typescript';
import { Surveys } from './surveys.models';
import { CoursesModel } from '../../courses/models/courses.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'surveys_has_courses',
})
export class SurveysHasCourses extends Model<SurveysHasCourses>{


    @ApiModelPropertyOptional()
    @ForeignKey(() => Surveys)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'survey_id',
    })
    surveyId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => CoursesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'course_id',
    })
    courseId: string;
}