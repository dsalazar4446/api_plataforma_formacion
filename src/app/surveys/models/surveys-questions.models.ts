import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Surveys } from './surveys.models';
import { SurveyQuestionOptions } from './surveys-question-options.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'survey_questions',
})
export class SurveyQuestions extends Model<SurveyQuestions>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Surveys)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'survey_id',
    })
    surveyId: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'question',
    })
    question: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.SMALLINT,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'question_type',
    })
    questionType: number;

    /**
     * RELACIONES
     * SurveyQuestions pertenece a Surveys
     */
    @ApiModelPropertyOptional()
    @BelongsTo(() => Surveys)
    surveys: Surveys;

    /**
     * RELACIONES
     * SurveyQuestions tiene muchos SurveyQuestionOptions
     */
    @ApiModelPropertyOptional({
        type: SurveyQuestionOptions,
        isArray: true
    })
    @HasMany(() => SurveyQuestionOptions)
    surveyQuestionOptions: SurveyQuestionOptions;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}