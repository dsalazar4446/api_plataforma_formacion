import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany, BelongsToMany } from 'sequelize-typescript';
import { SurveyQuestions } from './surveys-questions.models';
import { UserSurveys } from './user-surveys.models';
import { SurveysHasPrograms } from './surveys-has-programs.models';
import { ProgramsModel } from '../../programs/models/programs.model';
import { UserModel } from '../../user/models/user.Model';
import { SurveysHasUsers } from './surveys-has-users.model';
import { RolesModel } from '../../roles/models/roles.models';
import { SurveysHasRoles } from './surveys-has-roles.model';
import { CoursesModel } from '../../courses/models/courses.model';
import { SurveysHasCourses } from './surveys-has-courses.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
 
@Table({
  tableName: 'surveys',
})
export class Surveys extends Model<Surveys>{

  @ApiModelPropertyOptional()
  @Column({
    type: DataType.UUIDV4,
    allowNull: false,
    autoIncrement: true,
    unique: true,
    primaryKey: true,
    validate: {
      notEmpty: true,
    },
  })
  id: string;

  @ApiModelPropertyOptional()
  @Column({
    type: DataType.STRING(45),
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'survey_title',
  })
  surveyTitle: string;

  @ApiModelPropertyOptional()
  @Column({
    type: DataType.STRING(45),
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'survey_description',
  })
  surveyDescription: string;


  @ApiModelPropertyOptional()
  @Column({
    type: DataType.ENUM('es', 'en', 'it', 'pr'),
    allowNull: false,
    validate: {
      notEmpty: true,
    },
    field: 'language_type',
  })
  languageType: string;

  /**
   * RELACIONES
   * Surveys tiene muchos SurveyQuestions
   * Surveys tiene muchos user_surveys
   * Surveys tiene muchos ProgramsModel
   */
  @ApiModelPropertyOptional({
    type: SurveyQuestions,
    isArray: true
  })
  @HasMany(() => SurveyQuestions)
  surveyQuestions: SurveyQuestions;

  @ApiModelPropertyOptional({
    type: UserSurveys,
    isArray: true
  })
  @HasMany(() => UserSurveys)
  userSurveys: UserSurveys;


  @BelongsToMany(() => ProgramsModel, () => SurveysHasPrograms)
  ProgramsModel: ProgramsModel[];

  @ApiModelPropertyOptional({
    example: {
      "documentTypeId": "UUID",
      "documentNumber": "string",
      "email": "string",
      "pwd": "string",
      "cellphone": "number",
      "username": "string",
      "birthDate": "Date",
      "firstName": "string",
      "lastName": "string",
      "phone": "number",
      "gender": "string",
      "avatar": "string",
      "roleId": "UUID",
      "ruc": "string"
    }
  })
  @BelongsToMany(() => UserModel, {
    through: {
      model: () => SurveysHasUsers,
      unique: true,
    }
  })
  userModel: UserModel[];


  @BelongsToMany(() => RolesModel, {
    through: {
      model: () => SurveysHasRoles,
      unique: true,
    }
  })
  rolesModel: RolesModel[];


  @BelongsToMany(() => CoursesModel, {
    through: {
      model: () => SurveysHasCourses,
      unique: true,
    }
  })
  coursesModel: CoursesModel[];

  @CreatedAt public created_at: Date;
  @UpdatedAt public updated_at: Date;
}