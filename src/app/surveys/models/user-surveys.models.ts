import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany, ForeignKey, BelongsTo, BelongsToMany } from 'sequelize-typescript';
import { Surveys } from './surveys.models';
import { UserModel } from './../../user/models/user.Model';
import { USurveysHSurveyQOptions } from './u-surveys-h-survey-q-options.models';
import { SurveyQuestionOptions } from './surveys-question-options.models';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

@Table({
    tableName: 'user_surveys',
})
export class UserSurveys extends Model<UserSurveys>{


    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => Surveys)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'survey_id',
    })
    surveyId: string;
    
    /**
     * RELACIONES
     * UserSurveys tiene muchos SurveyQuestionOptions
     */    
    @ApiModelPropertyOptional()
    @BelongsToMany(() => SurveyQuestionOptions, () => USurveysHSurveyQOptions)
    surveyQuestionOptions: SurveyQuestionOptions[];

    /**
     * RELACIONES
     * UserSurveys tiene muchos UserModel
     */    
    @ApiModelPropertyOptional()
    @BelongsTo(() => UserModel)
    userModel: UserModel;
    
    @ApiModelPropertyOptional()
    @BelongsTo(() => Surveys)
    Surveys: Surveys;
    
    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}