import { SharedModule } from '../shared/shared.module';
import { Module, HttpModule, Inject, MiddlewareConsumer, RequestMethod, forwardRef } from '@nestjs/common';
import { CoinbaseController } from './controllers/coinbase.controller';
import { TransactionController } from './controllers/transactions.controller';
import { CoinbaseService } from './services/coinbase.service';
import { Category } from 'typescript-logging';
import { APP_LOGGER } from '../../logger';
import { DatabaseModule } from '../database/database.module';
import { TransactionModel } from './models/transaction.model';
import { TransactionsService } from './services/transactions.service';
import { ClearDueCsrfTokensJob } from './cron-jobs/clear-due-csrf-tokens.job';
import { CronJobService } from '../shared/services/cron-job.service';
//import { ParseBearerMiddleware } from '../shared/middleware/parse-bearer.middleware';
import { PaymentAuthorizations } from './models/payment-authorizations.model';
import { PaymentConfigurations } from './models/payment-configurations.model';
import { PaymentAuthorizationsController } from './controllers/payment-authorizations.controller';
import { PaymentConfigurationsController } from './controllers/payment-configurations.controller';
import { PaymentAuthorizationsService } from './services/payment-authorizations.service';
import { PaymentConfigurationsService } from './services/payment-configurations.service';
//import { CronJobModule } from '../shared/abstract/cron-job-module.abstract';
import { CulqiService } from './services/culqi/culqi.service';
import { CulqiController } from './controllers/culqi/culqi.controller';
import { CronJobModule } from '../shared/abstract/cron-job-module.abstract';
import { OdooService } from './services/odoo.service';
import { OdooController } from './controllers/odoo.controller';
import { UserMembershipActivationPaymentsModel } from '../memberships/models/user-membership-activation-payments.model';
import { UserModel } from '../user/models/user.Model';
import { PaymentCheckService } from './services/payment-check.service';
import { PaymentCheckController } from './controllers/payment-check.controller';
import { BillingTransactionsModel } from '../transactions/models/billing-transactions.model';
import { Gateways } from '../gateway/models/gateway.model';
import { TransactionsPayments } from '../transactions/models/transactions-payments.models';
import { UserPaymentMethodService } from './services/user-payment-method/user-payment-method.service';
import { UserPaymentMethodController } from './controllers/user-payment-method/user-payment-method.controller';
import { UserPaymentMethodsModel } from './models/user-payment-method.model';
import { TransactionsPaymentHistory } from '../transactions/models/transactions-payment-history.models';
import { TransactionsModule } from '../transactions/transactions.module';
import { GatewayModule } from '../gateway/gateway.module';
import { BillingTransactionsService } from '../transactions/services/billing-transactions/billing-transactions.service';
import { NodemailerService } from '../shared/services/nodemailer.service';
// tslint:disable-next-line: no-unused-expression

const models = [
    TransactionModel,
    PaymentAuthorizations,
    PaymentConfigurations,
    UserMembershipActivationPaymentsModel,
    UserModel,
    BillingTransactionsModel,
    Gateways,
    TransactionsPayments,
    UserPaymentMethodsModel,
    BillingTransactionsModel,
    TransactionsPaymentHistory,
];

@Module({
    imports: [
        forwardRef(()=>SharedModule),
        forwardRef(() => TransactionsModule),
        HttpModule,
        DatabaseModule.forRoot({
            sequelize: {
                models
            },
            loki: true,
        }),
    ],
    controllers: [
        CoinbaseController,
        TransactionController,
        PaymentAuthorizationsController,
        PaymentConfigurationsController,
        CulqiController,
        OdooController,
        PaymentCheckController,
        UserPaymentMethodController
    ],
    providers: [
        
        CoinbaseService,
        TransactionsService,
        ClearDueCsrfTokensJob,
        PaymentAuthorizationsService,
        PaymentConfigurationsService,
        OdooService,
        PaymentCheckService,
        {
            provide: 'Logger',
            useValue: new Category('PAYMENT_MODULE', APP_LOGGER),
        },
        CulqiService,
        UserPaymentMethodService,
        GatewayModule,
        BillingTransactionsService,
        NodemailerService
    ],
    exports: [CulqiService, OdooService, UserPaymentMethodService]
})
export class PaymentModule  extends CronJobModule {
    constructor(
        private readonly clearDueCsrfTokensJob: ClearDueCsrfTokensJob,
        @Inject('CronJobService') protected readonly cronJobService: CronJobService,
    ) {
        super(cronJobService, [
            clearDueCsrfTokensJob,
        ]);
    }
    /*configure(consumer: MiddlewareConsumer) {
        consumer.apply(ParseBearerMiddleware)
            .forRoutes(
                {
                    path: 'payments/coinbase/transactions/request',
                    method: RequestMethod.POST,
                },
            );
    }*/
}