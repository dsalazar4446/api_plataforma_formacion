import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";
import { IsUUID, IsNotEmpty, IsString, IsOptional } from "class-validator";

export class paymentTransactionDto {
    
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    billId: string;

    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    tokenCard?: string;
    
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    cardType?: string;
}