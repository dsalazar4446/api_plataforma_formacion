import { ApiModelProperty } from "@nestjs/swagger";
import { IsNumber, IsString, IsUUID } from "class-validator";

export class CreateUserPaymentMethod {
    @ApiModelProperty()
    @IsUUID('4')
    userId: string;
    @ApiModelProperty()
    @IsUUID('4')
    getawaysId: string
    @ApiModelProperty()
    @IsString()
    lastNumbers: string;
    @ApiModelProperty()
    @IsString()
    paymentToken: string;
    @ApiModelProperty()
    @IsString()
    userPaymentMethodMain: boolean;
    @ApiModelProperty()
    @IsString()
    culqiCostumerId: string;
}

export class UpdateUserPaymentMethod extends CreateUserPaymentMethod {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}