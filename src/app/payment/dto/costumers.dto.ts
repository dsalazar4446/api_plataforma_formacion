import { ApiModelProperty } from "@nestjs/swagger";
import { MaxLength, MinLength, IsString } from "class-validator";

export class CreateCostumer {
    @ApiModelProperty()
    @IsString()
    @MinLength(2)
    @MaxLength(50)
    address: string;

    @ApiModelProperty()
    @IsString()
    @MinLength(2)
    @MaxLength(50)
    address_city: string;

    @ApiModelProperty()
    @IsString()
    @MaxLength(3)
    country_code:string;

    @ApiModelProperty()
    @IsString()
    @MinLength(5)
    @MaxLength(50)
    email:string; 

    @ApiModelProperty()
    @IsString()
    @MinLength(2)
    @MaxLength(50)
    first_name: string; 

    @ApiModelProperty()
    @IsString()
    @MinLength(2)
    @MaxLength(50)
    last_name: string; 
    
    @ApiModelProperty()
    @IsString()
    @MinLength(5)
    @MaxLength(15)
    phone_number: string;
    
    @ApiModelProperty()
    metadata: any
}