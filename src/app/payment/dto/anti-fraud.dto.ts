import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, MinLength, MaxLength, IsNumber, Min, Max } from "class-validator";


export class AntiFraudDetails {

    @ApiModelProperty()
    @IsString()
    @MinLength(5)
    @MaxLength(100)
    address: string;

    @ApiModelProperty()
    @IsString()
    @MinLength(2)
    @MaxLength(30)
    address_city: string;

    @ApiModelProperty()
    @IsString()
    @MinLength(2)
    @MaxLength(3)
    country_code: string

    @ApiModelProperty()
    @IsString()
    @MinLength(2)
    @MaxLength(50)
    last_name: string

    @ApiModelProperty()
    @IsNumber()
    @Min(5)
    @Max(15)
    phone_number: number;
}