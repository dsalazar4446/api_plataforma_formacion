import { ApiModelProperty } from "@nestjs/swagger";
import { IsBoolean, IsOptional, IsString, MinLength, MaxLength } from "class-validator";
import { any } from "bluebird";

export class CreateCards {
    
    @ApiModelProperty()
    @MinLength(25)
    @MaxLength(25)
    @IsString()
    customer_id: string;
    
    @ApiModelProperty()
    @IsString()
    @MinLength(25)
    @MaxLength(25)
    token_id: string;
    
    @ApiModelProperty()
    @IsOptional()
    @IsBoolean()
    validate: boolean;

    @ApiModelProperty()
    @IsOptional()
    metadata: any;
}

export class UpdateCards {
    
    @ApiModelProperty()
    @MinLength(25)
    @MaxLength(25)
    @IsString()
    id: string;

    @ApiModelProperty()
    @MinLength(25)
    @MaxLength(25)
    @IsString()
    token_id: string;

    @ApiModelProperty()
    @IsOptional()
    metadata: any;

}