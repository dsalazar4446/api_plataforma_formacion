import { IsNumber, Max, Min, IsBoolean, IsOptional, IsString, IsCurrency, IsEmail, MinLength, MaxLength } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";
import { AntiFraudDetails } from "./anti-fraud.dto";

export class CreateCharge{
    
    @ApiModelProperty()
    @IsNumber()
    @Min(100)
    @Max(999900)
    amount: number;
    
    @ApiModelProperty()
    @IsBoolean()
    @IsOptional()
    capture: boolean;

    @ApiModelProperty()
    @IsString()
    currency_code: string;
    
    
    @ApiModelProperty()
    @MinLength(5)
    @MaxLength(50)
    @IsEmail()
    email: string;
    
    @ApiModelProperty()
    @MinLength(25)
    @MaxLength(25)
    source_id: string;
    
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    @MinLength(5)
    @MaxLength(50)
    description?: string;
    
    @ApiModelProperty()
    @Min(2)
    @Max(48)
    @IsNumber()
    @IsOptional()
    installment?: number;

    @ApiModelProperty()
    @IsOptional()
    metadata?: any;


    @ApiModelProperty()
    antifraud_details: AntiFraudDetails;
}

export class UpdateCharge{
    @ApiModelProperty()
    @MaxLength(25)
    @MinLength(25)
    @IsString()
    id: string

    @ApiModelProperty()
    metadata: any
}