import { Controller, Get, Post, UseFilters, UseInterceptors, HttpCode, Body, UsePipes, Param, Req, HttpStatus } from '@nestjs/common';
import { CoinbaseService } from '../services/coinbase.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { CoinbaseTransactionBaseJson, CoinbaseProcessCodeJson, CoinbaseOauthTokenResponse, ChargeJson } from '../interfaces/coinbase.interface';
import * as Bluebird from 'bluebird';
import { ApiImplicitParam, ApiImplicitHeader } from '@nestjs/swagger';
import { AppUtilsService } from '../../shared/services/app-utils.service';

@Controller('payments/coinbase')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class CoinbaseController {

    constructor(
        private readonly coinbaseService: CoinbaseService,
        private readonly appUtilsService: AppUtilsService,
    ) { }
 
    /* ------ COINBASE OAUTH ----- */
    @Get('oauth')
    getOauthDetails() {
        return {
            authUrl: this.coinbaseService.genAuthorizationUrl(),
        };
    }

    @Post('oauth/process_code')
    @HttpCode(200)
    async processCoinBaseOauthCode(@Body() coinbaseProccessCode: CoinbaseProcessCodeJson): Promise<CoinbaseOauthTokenResponse> {
        return await this.coinbaseService.genExternalUserToken(coinbaseProccessCode.code, coinbaseProccessCode.state);
    }

    /* ------ COINBASE TRANSACTIONS ----- */
    @Post('transactions/request')
    @HttpCode(202)
    createTransaction(@Req() req, @Body() transferData: CoinbaseTransactionBaseJson) {
        return this.coinbaseService.transaction({
            type: 'request',
            to: transferData.to,
            amount: transferData.ammount,
            currency: transferData.currency,
            description: transferData.description,
        });
    }
    @Post('oauth/data_user/:code')
    @HttpCode(202)
    @ApiImplicitParam({ name: 'code', required: true, type: 'string' })
    async getUserData(@Param('code') code) {
        return await this.coinbaseService.getUserData(code);
    }
    /* ------ COINBASE  COMMERCER ----- */
    @Post('charge')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async createCharge(@Body() charge: ChargeJson, @Req() req) {
        if(charge.currency != "BTC" && charge.currency != "ETH" && charge.currency != "BCH"){
            throw this.appUtilsService.httpCommonError('CHarge does not created!', HttpStatus.NOT_ACCEPTABLE, {
                messageCode: 'EA26',
                languageType:  req.headers.language,
            });
        }
        return await this.coinbaseService.createCharge(charge);
    }
    @Get('charge/:chargeId')
    @ApiImplicitParam({ name: 'chargeId', required: true, type: 'string' })
    async detailCharge(@Param('chargeId') chargeId) {
        return await this.coinbaseService.getDataCharge(chargeId);
    }

    @Get('charge')
    async listCharge() {
        return await this.coinbaseService.listCharges();
    }

    @Get('exchange/:amount')
    @ApiImplicitParam({ name: 'amount', required: true, type: 'string' })
    async exchange(@Param('amount') amount) {
        return await this.coinbaseService.exchange(amount);
    }

    @Get('webhook')
    async webhook(@Req() req) {
        return await this.coinbaseService.webhook(req.rawBody, req.headers['x-cc-webhook-signature']);
    }
}
