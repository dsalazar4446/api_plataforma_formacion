import { Controller, Get, Post, UseFilters, Body, UseInterceptors, UsePipes, UseGuards } from '@nestjs/common';
import { TransactionsService } from '../services/transactions.service';
import { TransactionSaveJson } from '../interfaces/transaction.interface';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('Module-Payment')
@Controller('payments/transactions')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class TransactionController {

    constructor(
        private readonly transactionService: TransactionsService,
    ) { }

    @Get()
    async findAll() {
        const result = await this.transactionService.getAll();
        return result;
    }

    @Post()
    async save(@Body() transaction: TransactionSaveJson) {
        const result = await this.transactionService.save(transaction);
        return result;
    }
}
