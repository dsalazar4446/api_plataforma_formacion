import { Controller, Post, Get, Put, Body, Delete, Param } from '@nestjs/common';
import { UserPaymentMethodService } from '../../services/user-payment-method/user-payment-method.service';
import { UpdateUserPaymentMethod, CreateUserPaymentMethod } from '../../dto/user-paument-method.dto';

@Controller('user-payment-method')
export class UserPaymentMethodController {
    constructor(private readonly userPaymentMethodService: UserPaymentMethodService ){
        
    }
    @Post()
    async create(@Body() body: CreateUserPaymentMethod) {
        return await this.userPaymentMethodService.create(body)
    }
    @Get()
    async list() {
        return await this.userPaymentMethodService.list();
    }
    @Get(':id')
    async detail(@Param('id') id: string) {
        return await this.userPaymentMethodService.detail(id)
    }
    @Put(':id')
    async update(@Param('id') id: string, @Body() body: Partial<UpdateUserPaymentMethod>) {
        return await this.userPaymentMethodService.update(id, body);
    }
    @Delete(':id')
    async delete(@Param('id') id: string) {
        return await this.userPaymentMethodService.delete(id)
    }
}
