import { Test, TestingModule } from '@nestjs/testing';
import { CulqiController } from './culqi.controller';

describe('Culqi Controller', () => {
  let controller: CulqiController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CulqiController],
    }).compile();

    controller = module.get<CulqiController>(CulqiController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
