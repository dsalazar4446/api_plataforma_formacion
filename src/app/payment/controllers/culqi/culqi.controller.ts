import { Controller, Post, Get, Body, Param, Patch, Delete } from '@nestjs/common';
import { CulqiService } from '../../services/culqi/culqi.service';
import { CreateCharge } from '../../dto/charge.dto';
import { CreateCostumer } from '../../dto/costumers.dto';
import { CreateCards } from '../../dto/cards.dto';
import { ApiImplicitParam } from '@nestjs/swagger';
import { paymentTransactionDto } from '../../dto/payment-transaction.dto';

@Controller('culqi')
export class CulqiController {
    constructor(private readonly culqiService: CulqiService){}




    @Post('charges/create')
    CreateCharge(@Body() body: CreateCharge){
        return this.culqiService.createChargeAutomatic(body)
    }
    @Post('payment-transaction')
    async paymentTransaction(@Body() body:paymentTransactionDto){
        return await this.culqiService.createCharge(body.billId, body.tokenCard, body.cardType);
    }
    @Post('clients/create')
    CreateClient(@Body() body: CreateCostumer) {
        return this.culqiService.createClient(body)
    }

    @Post('cards/create')
    CreateCards(@Body() body: CreateCards) {
        return this.culqiService.createCard(body)
    }
    @Get('charges/list')
    listCharge(){
        return this.culqiService.listCharge();
    }

    @Get('clients/list')
    listClient() {
        return this.culqiService.listClient();
    }
    @Get('cards/list')
    listCards() {
        return this.culqiService.listCard();
    }

    @Get('charges/detail/:id')
    @ApiImplicitParam({name: 'userId', required: true, type: 'string'})
    detailCharge(@Param('id') id: string){
        return this.culqiService.detailCharge(id);
    }

    @Get('clients/detail/:id')
    @ApiImplicitParam({name: 'userId', required: true, type: 'string'})
    detailClient(@Param('id') id: string) {
        return this.culqiService.detailClient(id);
    }

    @Get('cards/detail/:id')
    @ApiImplicitParam({name: 'userId', required: true, type: 'string'})
    detailCards(@Param('id') id: string) {
        return this.culqiService.detailCard(id);
    }


    @Patch('charges/update/:id')
    @ApiImplicitParam({name: 'id', required: true, type: 'string'})
    updateCharge(@Param('id') id: string) {
        return this.culqiService.updateCharge(id);
    }

    @Patch('clients/update/:id')
    @ApiImplicitParam({name: 'userId', required: true, type: 'string'})
    updateClient(@Param('id') id: string) {
        return this.culqiService.updateClient(id);
    }

    @Patch('cards/update/:id')
    @ApiImplicitParam({name: 'userId', required: true, type: 'string'})
    updateCards(@Param('id') id: string) {
        return this.culqiService.updateCard(id);
    }

    @Get('webhook')
    webhook(){
        
    }
}
