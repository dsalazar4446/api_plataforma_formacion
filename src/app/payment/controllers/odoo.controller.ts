import { Controller, Get, Post, UseFilters, UseInterceptors, HttpCode, Body, UsePipes, Param, Req } from '@nestjs/common';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { OdooService } from '../services/odoo.service';
import { OdoCreateCustomerJson, OdoCheckCustomerJson, OdoBilJson, OdooProductCreexJson, SearchOdooBillCreexJson, OdoProductJson } from '../interfaces/odoo.interface';

@Controller('payments/odoo')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class OdooController {

    constructor(
        private odooService: OdooService,
    ) {}
    @Post('auth')
    async auth() {
        return await this.odooService.auth();
    }
    @Post('create-customer')
    async createCustomer(@Body() customer: OdoCreateCustomerJson) {
        return await this.odooService.createCustomer(customer);
    }
    @Post('check-customer')
    async checkCustomer(@Body() customer: OdoCheckCustomerJson) {
        return await this.odooService.checkCustomer(customer);
    }
    @Post('list-bill')
    async listBill(@Body() customer: SearchOdooBillCreexJson) {
        return await this.odooService.listBill(customer.customerId);
    }
    @Post('register-bil')
    async registerBil(@Body() bil: OdoBilJson) {
        return await this.odooService.registerBil(bil);
    }
    @Post('create-product')
    async createProduct(@Body() product: OdooProductCreexJson) {
        return await this.odooService.createProduct(product);
    }
}
