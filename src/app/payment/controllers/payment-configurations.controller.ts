import { Controller, UseFilters, UseInterceptors, Post, FileFieldsInterceptor, Body, HttpStatus, NotFoundException, Get, Param, Put, Delete } from "@nestjs/common";
import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { PaymentConfigurationsService } from "../services/payment-configurations.service";
// import { CONFIG } from "../../../config";
import { PayConfigSaveJson } from "../interfaces/payment-configurations.interface";
import { ApiUseTags, ApiConsumes, ApiImplicitParam, ApiImplicitFile } from "@nestjs/swagger";
// import * as file from './infoFile';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller PaymentConfigurationsController
 * @Creado 09 de Abril 2019
 */

@ApiUseTags('Module-Payment')
@Controller('payment-configurations')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class PaymentConfigurationsController {

    constructor(private readonly _payConfServc: PaymentConfigurationsService,
        private readonly appUtilsService: AppUtilsService, ) { }


    @Post()
    // @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    // @ApiConsumes('multipart/form-data')
    // @ApiImplicitFile({ name: 'paymentCheckImg', description: 'Payment Check Img File', required: true })
    async create(@Body() bodyPayConfg: PayConfigSaveJson) {

        const createPayConf = await this._payConfServc.savePayConfig(bodyPayConfg);

        if (!createPayConf) {
            throw this.appUtilsService.httpCommonError('Configuracion de pago no ha sido creado!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM161',
                languageType: 'es',
            });
        }
        // createPayConf.paymentCheckImg = `${CONFIG.storage.server}paymentConfiguration/paymentCheckImg/${createPayConf.paymentCheckImg}`;

        return createPayConf;

    }

    @Get()
    async showAllResource() {

        const fetchAll = await this._payConfServc.showAllPayConfig();

        if (!fetchAll) {
            throw this.appUtilsService.httpCommonError('Configuracion de pago no existe!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM162',
                languageType: 'es',
            });
        }

        // fetchAll.forEach(fetch => fetch.paymentCheckImg = `${CONFIG.storage.server}paymentConfiguration/paymentCheckImg/${fetch.paymentCheckImg}`);

        return fetchAll;
    }

    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idPayConf: string) {

        const fetchPayConf = await this._payConfServc.getDetails(idPayConf);

        if (!fetchPayConf) {
            throw this.appUtilsService.httpCommonError('Configuracion de pago no existe!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM162',
                languageType: 'es',
            });
        }
        // fetchPayConf.paymentCheckImg = `${CONFIG.storage.server}paymentConfiguration/paymentCheckImg/${fetchPayConf.paymentCheckImg}`;

        return fetchPayConf;
    }

    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    // @UseInterceptors(FileFieldsInterceptor(file.nameFile, file.validationFile))
    // @ApiConsumes('multipart/form-data')
    // @ApiImplicitFile({ name: 'paymentCheckImg', description: 'Payment Check Img File', required: true })
    async updatePayConf(@Param('id') id: string, @Body() bodyPayConfg: Partial<PayConfigSaveJson>) {

        const updatePayConf = await this._payConfServc.updatePayConfig(id, bodyPayConfg);

        if (!updatePayConf) {
            throw this.appUtilsService.httpCommonError('Configuracion de pago no existe!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM162',
                languageType: 'es',
            });
        }
        // updatePayConf[1][0].paymentCheckImg = `${CONFIG.storage.server}paymentConfiguration/paymentCheckImg/${updatePayConf[1][0].paymentCheckImg}`;

        return updatePayConf;
    }

    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deletePayConf(@Param('id') idPayConf: string) {

        const deletePayConf = await this._payConfServc.destroyPayConfig(idPayConf);

        if (!deletePayConf) {
            throw this.appUtilsService.httpCommonError('Configuracion de pago no existe!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM162',
                languageType: 'es',
            });
        }

        return deletePayConf;
    }







}