import { Controller, Get, Post, UseFilters, Body, UseInterceptors, UsePipes, UseGuards, Param } from '@nestjs/common';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { ApiUseTags, ApiImplicitParam } from '@nestjs/swagger';
import { PaymentCheckService } from '../services/payment-check.service';


@Controller('payments/check')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class PaymentCheckController {

    constructor(
        private readonly paymentCheckService: PaymentCheckService,
    ) { }

    @Get('activate/:userId')
    @ApiImplicitParam({name: 'userId', required: true, type: 'string'})
    async checkPayment(@Param('userId') idUser: string) {
        const result = await this.paymentCheckService.checkPaymentActivation(idUser);
        return result;
    }
    @Get('status/login/:userId')
    @ApiImplicitParam({name: 'userId', required: true, type: 'string'})
    async checkStatusPayment(@Param('userId') idUser: string) {
        const result = await this.paymentCheckService.checkStatusLoginPayment(idUser);
        return result;
    }
}
