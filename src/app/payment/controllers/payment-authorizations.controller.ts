import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, NotFoundException, Param, Put, Delete } from "@nestjs/common";
import { AppExceptionFilter } from "../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../shared/services/app-utils.service";
import { PaymentAuthorizationsService } from "../services/payment-authorizations.service";
import { AjvValidationPipe } from "../../shared/pipes/ajv-validation.pipe";
import { PayAuthSaveJson } from "../interfaces/payment-authorizations.interface";
import { ApiUseTags, ApiImplicitParam } from "@nestjs/swagger";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller PaymentAuthorizationsController
 * @Creado 26 de Marzo 2019
 */

@ApiUseTags('Module-Payment')
@Controller('payment-authorizations')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class PaymentAuthorizationsController {

    constructor(private readonly _paymentAuthorizationsService: PaymentAuthorizationsService,
        private readonly appUtilsService: AppUtilsService) { }

    @Post()
    async create(@Body() bodyPayAuth: PayAuthSaveJson) {

        const createPayAuth = await this._paymentAuthorizationsService.savePayAuth(bodyPayAuth);

        if (!createPayAuth) {
            throw this.appUtilsService.httpCommonError('Autorizacion de pago no ha sido creada!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM169',
                languageType: 'es',
            });
        }

        return createPayAuth;

    }

    @Get()
    async showAllPayAuth() {

        const fetch = await this._paymentAuthorizationsService.showAllPayAuth();

        if (!fetch) {
            throw this.appUtilsService.httpCommonError('Autorizacion de pago no existe!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM170',
                languageType: 'es',
            });
        }

        return fetch;
    }

    @Get('details/:id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idPayAuth: string) {

        const fetchPayAuth = await this._paymentAuthorizationsService.getDetails(idPayAuth);

        if (!fetchPayAuth) {
            throw this.appUtilsService.httpCommonError('Autorizacion de pago no existe!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM170',
                languageType: 'es',
            });
        }

        return fetchPayAuth;
    }

    @Put(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async updatePayAuth(@Param('id') id: string, @Body() bodyPayAuth: Partial<PayAuthSaveJson>) {

        const updatePayAuth = await this._paymentAuthorizationsService.updatePayAuth(id, bodyPayAuth);

        if (!updatePayAuth) {
            throw this.appUtilsService.httpCommonError('Autorizacion de pago no existe!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM170',
                languageType: 'es',
            });
        }

        return updatePayAuth;
    }

    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async deletePayAuth(@Param('id') idPayAuth: string) {

        const deletePayAuth = await this._paymentAuthorizationsService.destroyPayAuth(idPayAuth);

        if (!deletePayAuth) {
            throw this.appUtilsService.httpCommonError('Autorizacion de pago no existe!', HttpStatus.INTERNAL_SERVER_ERROR, {
                messageCode: 'EM170',
                languageType: 'es',
            });
        }

        return deletePayAuth;
    }

}