import { Table, Column, DataType, Model, CreatedAt, UpdatedAt } from "sequelize-typescript";

@Table({
    tableName: 'payment_configurations',
})
export class PaymentConfigurations extends Model<PaymentConfigurations>{


    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'payment_month_day'
    })
    paymentMonthDay: number;

    @Column({
        type: DataType.ENUM('1','2','3','4','5','6'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'payment_types',
    })
    paymentTypes: string;

    @Column({
        type: DataType.ENUM('left', 'center', 'right', 'uni', 'na'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'payment_side',
    })
    paymentSide: string;

    // @Column({
    //     type: DataType.STRING(75),
    //     allowNull: false,
    //     validate: {
    //         notEmpty: true,
    //     },
    //     field: 'payment_check_img',
    // })
    // paymentCheckImg: string;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;



}

