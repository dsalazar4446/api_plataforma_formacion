import { Table, Column, DataType, ForeignKey, Model, CreatedAt, UpdatedAt, BelongsTo } from "sequelize-typescript";
import { BillingTransactionsModel } from "../../transactions/models/billing-transactions.model";
import { UserModel } from "../../user/models/user.Model";

@Table({
    tableName: 'payment_authorizations',
})
export class PaymentAuthorizations extends Model<PaymentAuthorizations>{


    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'authorized_by_user',
    })
    authorizedByUser: string;

    @ForeignKey(() => BillingTransactionsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'billing_transactions_id',
    })
    billingTransactionsId: string;

    /**
     * RELACIONES
     * PaymentAuthorizations pertenece a UserModel
     * PaymentAuthorizations pertenece a billingTransactionsModel
     */
    @BelongsTo(() => UserModel)
    userModel: UserModel;

    @BelongsTo(() => BillingTransactionsModel)
    billingTransactionsModel: BillingTransactionsModel;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}