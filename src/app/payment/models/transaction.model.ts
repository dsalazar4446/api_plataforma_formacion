import { Column, DataType, Table, Model, ForeignKey, BelongsTo, CreatedAt, UpdatedAt } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';


@Table({
    tableName: 'billing_transactions',
})
export class TransactionModel extends Model<TransactionModel> {
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    public id: string;

    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_location_id',
    })
    public userLocationId: string;

    @ForeignKey(() => UserModel)
    @Column({
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;

    @Column({
        type: DataType.ENUM(
            '1',
            '2',
            '3',
            '4',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_type',
        get() {
            return parseInt(this.getDataValue('transactionType'), 10);
        },
        set(value: number) {
            this.setDataValue('transactionType', value.toString());
        },
    })
    transactionType: number;

    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_ammount',
    })
    transactionAmmount: string;

    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_shippment',
    })
    transactionShippment: string;

    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_taxes',
    })
    transactionTaxes: string;

    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_total',
    })
    transactionTotal: string;

    @Column({
        type: DataType.ENUM(
            '1',
            '2',
            '3',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_status',
        get() {
            return parseInt(this.getDataValue('transactionStatus'), 10);
        },
        set(value: number) {
            this.setDataValue('transactionStatus', value.toString());
        },
    })
    transactionStatus: number;

    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_tryouts',
        defaultValue: 0,
    })
    transactionTryouts: number;

    @Column({
        type: DataType.TEXT,
        allowNull: true,
        field: 'transaction_observations',
    })
    transactionObservations: string;

         // tslint:disable-next-line: variable-name
         @CreatedAt created_at: Date;
         // tslint:disable-next-line: variable-name
         @UpdatedAt updated_at: Date;
}
