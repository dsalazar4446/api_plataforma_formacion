export interface LokiCsrfDto {
    createdAt: number;
    token: string;
}
