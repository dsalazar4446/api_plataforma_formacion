import {IsUUID, IsBoolean, IsNumber, IsString, IsNotEmpty, IsOptional} from "class-validator";
import { ApiModelProperty } from '@nestjs/swagger';

export class CoinbaseProcessCodeJson {
    @ApiModelProperty()
    code: string;
    @ApiModelProperty()
    state: string;
}

// tslint:disable-next-line: max-classes-per-file
export class CoinbaseTransactionBaseApiOptions {
    @ApiModelProperty()
    type: string;
    @ApiModelProperty()
    to: string;
    @ApiModelProperty()
    amount: string;
    @ApiModelProperty()
    currency: string;
    @ApiModelProperty()
    description?: string;
}
// tslint:disable-next-line: max-classes-per-file
export class CoinbaseTransactionSendApiOptions extends CoinbaseTransactionBaseApiOptions {
    @ApiModelProperty()
    skip_notifications?: boolean;
    @ApiModelProperty()
    fee?: string;
    @ApiModelProperty()
    idem?: string;
    @ApiModelProperty()
    to_financial_institution?: boolean;
    @ApiModelProperty()
    financial_institution_website?: string;
}
// tslint:disable-next-line: max-classes-per-file
export class CoinbaseTransactionBaseJson {
    @ApiModelProperty()
    to: string;
    /**
     * @regex ^\\d+(\\.\\d+)?$
     */
    @ApiModelProperty()
    ammount: string;
    @ApiModelProperty()
    currency: string;
    @ApiModelProperty()
    description?: string;
}
// tslint:disable-next-line: max-classes-per-file
export class CoinbaseOauthTokenResponse {
    @ApiModelProperty()
    access_token: string;
    @ApiModelProperty()
    token_type: string;
    @ApiModelProperty()
    expires_in: number;
    @ApiModelProperty()
    refresh_token: string;
    @ApiModelProperty()
    scope: string;
    @ApiModelProperty()
    created_at: number;
}
// tslint:disable-next-line: max-classes-per-file
export class CoinbaseUserDataResponse {
    @ApiModelProperty()
    id: string;
    @ApiModelProperty()
    name: string;
    @ApiModelProperty()
    username: string;
    @ApiModelProperty()
    profile_location: string;
    @ApiModelProperty()
    profile_bio: null | string;
    @ApiModelProperty()
    profile_url: string;
    @ApiModelProperty()
    avatar_url: string;
    @ApiModelProperty()
    resource: string;
    @ApiModelProperty()
    resource_path: string;
}
// tslint:disable-next-line: max-classes-per-file
export class CoinbaseTimeResponse {
    @ApiModelProperty()
    iso: string;
    @ApiModelProperty()
    epoch: number;
}
// tslint:disable-next-line: max-classes-per-file
export class CoinbaseOauthHeader {
    @ApiModelProperty()
    Authorization: string;
    [key: string]: string;
}
// tslint:disable-next-line: max-classes-per-file
export class CoinbaseApiAuthHeaders {
    @ApiModelProperty()
    'Content-Type': string;
    @ApiModelProperty()
    'CB-ACCESS-KEY': string;
    @ApiModelProperty()
    'CB-ACCESS-SIGN': string;
    @ApiModelProperty()
    'CB-ACCESS-TIMESTAMP': string;
    [key: string]: string;
}



// tslint:disable-next-line: max-classes-per-file
export class ChargeJson{
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    name: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    description: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    amount: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    currency: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    billingTransactionId: string;
}