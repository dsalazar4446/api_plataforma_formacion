import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class PayAuthSaveJson {

    @ApiModelProperty()
    @IsUUID('4',{message: 'authorizedByUser property must a be uuid'})
    @IsNotEmpty({message: 'authorizedByUser property not must null'})
    authorizedByUser: string;

    @ApiModelProperty()
    @IsUUID('4',{message: 'billingTransactionsId property must a be uuid'})
    @IsNotEmpty({message: 'billingTransactionsId property not must null'})
    billingTransactionsId: string;
}
export class PayAuthUpdateJson extends PayAuthSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
