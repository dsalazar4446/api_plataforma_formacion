import {IsUUID, IsBoolean, IsNumber, IsString, IsNotEmpty, IsOptional, IsEmail} from "class-validator";
import { ApiModelProperty } from '@nestjs/swagger';
export class OdoCreateCustomerJson {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    name: string;
    @ApiModelProperty()
    @IsString()
    @IsEmail()
    @IsNotEmpty()
    email: string;
    @ApiModelProperty()
    @IsNumber()
    @IsNotEmpty()
    catalog06Id: number;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    dni: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    street: string;
}
// tslint:disable-next-line: max-classes-per-file
export class OdoCheckCustomerJson {
    @ApiModelProperty()
    @IsString()
    @IsEmail()
    @IsNotEmpty()
    email: string;
}

// tslint:disable-next-line: max-classes-per-file
export class OdoProductJson {
    @ApiModelProperty()
    @IsNumber()
    @IsNotEmpty()
    membershipId: number;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    description: string;
    @ApiModelProperty()
    @IsNumber()
    @IsNotEmpty()
    quantity: number;
    @ApiModelProperty()
    @IsNumber()
    @IsNotEmpty()
    price: number;
    @ApiModelProperty()
    @IsNumber()
    @IsOptional()
    discount?: number;
}
// tslint:disable-next-line: max-classes-per-file
export class OdoPaymentJson {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    origin: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    detail: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    transactionId: string;
}

// tslint:disable-next-line: max-classes-per-file
export class OdoBilJson{
    @ApiModelProperty()
    @IsNumber()
    @IsNotEmpty()
    customerId: number;

    @ApiModelProperty({
        type: OdoProductJson,
        isArray: true
    })
    lines: Array<OdoProductJson>

    @ApiModelProperty()
    @IsNumber()
    @IsNotEmpty()
    journalId: number;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    typeOperation: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    typeCurrency: string;
    @ApiModelProperty()
    @IsNotEmpty()
    payment: OdoPaymentJson;
}


// tslint:disable-next-line: max-classes-per-file
export class OdooProductCreexJson{
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    name: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    price: number;
}
 

// tslint:disable-next-line: max-classes-per-file
export class SearchOdooBillCreexJson{
    @ApiModelProperty()
    @IsNumber()
    @IsNotEmpty()
    customerId: number;
}