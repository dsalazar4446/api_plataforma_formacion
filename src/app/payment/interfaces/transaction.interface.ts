export interface TransactionSaveJson {
    /**
     * @regex ^\\d+(\\.\\d+)?$
     */
    transactionAmmount: string;
    transactionShippment: string;
    /**
     * @regex ^\\d+(\\.\\d+)?$
     */
    transactionTaxes: string;
    /**
     * @regex ^\\d+(\\.\\d+)?$
     */
    transactionTotal: string;
    transactionStatus: number;
    transactionTryouts: number;
    transactionObservations?: string;
}
