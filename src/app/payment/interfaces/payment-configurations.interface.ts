import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsString, IsOptional, IsNumber } from 'class-validator';
export class PayConfigSaveJson {

    @ApiModelProperty()
    @IsNumber(null,{message: 'paymentMonthDay property must a be string'})
    @IsNotEmpty({message: 'paymentMonthDay property not must null'})
    paymentMonthDay: number;

    @ApiModelProperty()
    @IsString({message: 'paymentTypes property must a be string'})
    @IsNotEmpty({message: 'paymentTypes property not must null'})
    paymentTypes: string;

    @ApiModelProperty()
    @IsString({message: 'paymentSide property must a be string'})
    @IsNotEmpty({message: 'paymentSide property not must null'})
    paymentSide: string;

    // @ApiModelProperty()
    // @IsString({message: 'paymentCheckImg property must a be string'})
    // @IsOptional()
    // paymentCheckImg?: string;
}
export class PayConfigUpdateJson extends PayConfigSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
