import { Injectable, HttpService, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { CONFIG } from '../../../config';
import { AxiosResponse } from 'axios';
import { UserMembershipActivationPaymentsModel } from '../../memberships/models/user-membership-activation-payments.model';
import { UserMembershipsModel } from '../../memberships/models/user-memberships.model';
import * as moment from 'moment';
import { UserModel } from '../../user/models/user.Model';
import { BillingTransactionsModel } from '../../transactions/models/billing-transactions.model';
import { Gateways } from '../../gateway/models/gateway.model';
import { TransactionsPayments } from '../../transactions/models/transactions-payments.models';
import { BillingTransactionsService } from '../../transactions/services/billing-transactions/billing-transactions.service';
import { NodemailerService } from '../../shared/services/nodemailer.service';
import { MembershipPricingsModel } from '../../memberships/models/membership-pricings.model';
import { UserPaymentMethodsModel } from '../models/user-payment-method.model';

@Injectable()
export class PaymentCheckService {
    constructor(
        private readonly http: HttpService,
// tslint:disable-next-line: max-line-length
        @Inject('UserMembershipActivationPaymentsModel') private readonly userMembershipActivationPaymentsModel: typeof UserMembershipActivationPaymentsModel,
        @Inject('UserMembershipsModel') private readonly userMembershipsModel: typeof UserMembershipsModel,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('BillingTransactionsModel') private readonly billingTransactionsModel: typeof BillingTransactionsModel,
        @Inject('Gateways') private readonly gatewayModel: typeof Gateways,
        @Inject('TransactionsPayments') private readonly transactionPaymentsModel: typeof TransactionsPayments,
        private billingTransactionsService: BillingTransactionsService,
        private nodemailerService: NodemailerService,
        @Inject('MembershipPricingsModel') private readonly membershipPricingsModel: typeof MembershipPricingsModel,
        @Inject('UserPaymentMethodsModel') private readonly userPaymentMethodsModel: typeof UserPaymentMethodsModel,
    ){}
    // VERIFICANDO PAGOS DE ACTIVACION DEL USUARIO
    async checkPaymentActivation(userId: string){
        //OBTENIENDO MEMBRESIA ACTIVA
        const dataDatesPayments:any =  await this.userMembershipsModel.findAll({
            where:{
                userId: userId,
                membershipStatus: true
            },
            include: [
                {
                    model: UserMembershipActivationPaymentsModel,
                }
            ],
        });
        // const dataPaymentActivated: any = [];
        // OBTENIENDO PAGOS DE ACTIVACION
        await Promise.all(
            await dataDatesPayments.map(
                async (date)=>{
                    await Promise.all(
                       await date.dataValues.activationPayments.map(
                            async (activate)=>{
                                const currentDay = moment(moment().format());
                                const differenceDay = moment(activate.paymentSchedule).diff(currentDay, 'days');
                                console.log("diferencia:",differenceDay)
                                if((differenceDay <= 0) && (activate.paymentStatus == false)){
                                    /*dataPaymentActivated.push(
                                        {
                                            activate,
                                            membershipId: date.dataValues.membershipId,
                                        }
                                    );*/
// tslint:disable-next-line: max-line-length
                                    const statusPaymentAutomatic = await this.billingTransactionsService.transactionPaymentAutomatic(userId,activate.id,'11');
                                    console.log("statusPaymentAutomatic:",statusPaymentAutomatic);
// tslint:disable-next-line: max-line-length
                                    this.notificationPaymentActivation(statusPaymentAutomatic.billingTransactionId,statusPaymentAutomatic.billingTransactionStatus,statusPaymentAutomatic.billingTransactionTryouts,userId,date.dataValues.membershipId);
                                }
                            }
                        )
                    )
                }
            )
        )
        // OBTENER EL ID DE LA MEMBRESIA
        return "success";
    }

    async getOptions(billingTransactionId, userId,membershipId, type){
        console.log('DATOS PARA OBTENER OPCIONES: ',billingTransactionId, userId,membershipId, type);
        let options;
        const dataGateway = await this.transactionPaymentsModel.findOne({
            where:{
                billingTransactionId,
            },
        });
        // GATEWAY
        const gateways = await this.gatewayModel.findByPk(dataGateway.dataValues.gatewaysId);
        const gatewayName = gateways.dataValues.gatewayName;

        // DATOS DEL CLIENTE
        const user = await this.userModel.findOne({
            where:{
                id: userId,
            }
        });

        // DATOS DE LA TARJETA DE CREDITO
        const card = await this.userPaymentMethodsModel.findOne({
            where:{
                userId,
                getawaysId: gateways.dataValues.id
            }
        });
        if(type == '3'){ // REJECT  - SI ES 2 ES SUCCESS
            options = {
                date: `${moment().format("YYYY-MM-DD h:mm:ss")}`,
                gatewayName,
                nameUser: `${user.dataValues.firstName} ${user.dataValues.lastName}`,
                email: user.dataValues.email,
                lastNumbers: card.dataValues.lastNumbers,
                subject: 'Pago automatico rechazado', /**ASUNTO */
                text: 'No hemos podido realizar el cobro automático de la renovación de tu membresia',
            }
        } else {
            const membershipPricing = await this.membershipPricingsModel.findOne({
                where:{
                    membershipId,
                    membershipPricingStatus: true,
                }
            })
            options = {
                date: `${moment().format("YYYY-MM-DD h:mm:ss")}`,
                gatewayName,
                nameUser: `${user.dataValues.firstName} ${user.dataValues.lastName}`,
                amount: membershipPricing.dataValues.membershipPricingAmmount,
                email: user.dataValues.email,
                lastNumbers: card.dataValues.lastNumbers,
                subject: 'Pago automatico aceptado', /**ASUNTO */
                text: 'Se realizo el cobro automático correspondiente a la renovación de su membresia',
            }
        }
        return options;
    }
    async notificationPaymentActivation(billingTransactionId,status,tryout,userId,membershipId) {
        try {
            let options;
            if(status != '2'){
                if(tryout>2){

                    // REALIZAR NOTIFICACION
                    // NOTIFICAR POR EMAIL
                    options = await this.getOptions(billingTransactionId, userId,membershipId, '3');
                    this.nodemailerService.sendEmail('payment-reject',options);
                    // NOTIFICAR POR PUSH NOTIFICATION
                }
            } else {
                // NOTIFICAR POR EMAIL
                options = await this.getOptions(billingTransactionId, userId,membershipId, '2');
                this.nodemailerService.sendEmail('payment-accept',options);
                // NOTIFICAR POR PUSH NOTIFICATION
            }
        } catch (error) {
            console.log(error);
        }
    }
    async notificationEmail(){

    }
    async notificationPushNotification(){

    }
    // INTENTO REALIZAR PAGOS
    async checkStatusLoginPayment(userId: string){
        return new Promise(async (resolve, reject)=>{
            try {
                const dataPayment: any =  await this.userMembershipsModel.findOne({
                    where:{
                        userId,
                        membershipStatus: true
                    }
                });
                if( dataPayment == null ) {
                    reject('The user has no activated membership');
                }
                const currentDay = moment(moment().format());
                if(dataPayment  == null){
                    resolve({
                        status_payment: false
                    })
                }
                const differenceDay = moment(dataPayment.dataValues.membershipEndDate).diff(currentDay, 'days');
                if(differenceDay<= 0){
                    this.userModel.findOne({
                       where:{
                        id: userId
                       }
                      }).then(async user => {
                        user.update({
                            userStatus: false
                        }).then(
                            ()=>{
                                resolve({
                                    status_payment: true
                                })
                            },error =>{
                                reject(error)
                            }
                        );
                      });
                }else{
                    resolve({
                        status_payment: false
                    })
                }
            } catch (error) {
                reject(error);
            }
        });
    }
    async checkStatusPayment(){
       /* const dataBillingTransaction = await this.billingTransactionsModel.findAll({
            where:{
                transactionStatus: 'pending',
            },
            include: [
                {
                    as: '',
                    model : TransactionsPayments
                }
            ]
        });

        const dataGateway = await this.gatewayModel.findAll({});
        await Promise.all(
            await dataGateway.map(
                async (gateway) =>{
                    if(gateway.gatewayName === 'Coinbase'){
                        const dataBillingTransaction = await this.billingTransactionsModel.findAll({
                            where:{
                                transactionStatus: 'pending',
                                gatewayId: gateway.id
                            }
                        });
                        this.checkStatusCoinbase
                    }           
                }
            )
        )*/
    }
    async checkStatusCoinbase(){
        
        const dataBillingTransaction = await this.billingTransactionsModel.findAll({
            where:{
                transactionStatus: 'pending',
            },
            include: [
                {
                    as: '',
                    model : TransactionsPayments
                }
            ]
        });

    }
}
