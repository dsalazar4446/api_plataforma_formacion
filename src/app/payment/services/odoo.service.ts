import { Injectable, HttpService, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { CONFIG } from '../../../config';
import { OdoCreateCustomerJson, OdoCheckCustomerJson, OdoBilJson, OdooProductCreexJson } from '../interfaces/odoo.interface';
import { AxiosResponse } from 'axios';
@Injectable()
export class OdooService {
    constructor(
        private readonly http: HttpService,
    ){}

    async auth(){
        try {
            const url = `${CONFIG.Odoo.host}web/session/authenticate`;
            const dataAuth = {
                jsonrpc: '2.0',
                method: 'call',
                params: {
                  db: CONFIG.Odoo.db + '',
                  login: CONFIG.Odoo.username + '',
                  password: CONFIG.Odoo.password + ''
                }
              }
            const result = await this.http.post(url, dataAuth).toPromise();
            return result.data;
        } catch (error) {
            return error;
        }
    }
    async createCustomer(customer: OdoCreateCustomerJson) {
        try {
            const url = `${CONFIG.Odoo.host}web/dataset/call_kw`;
            const login = await this.auth();
            const sesionId = login.result.session_id;
            const dataUser = await this.searchCustomer(customer.email,sesionId);
            if(dataUser.result.length> 0 ){
                throw new HttpException('email_duplicated',HttpStatus.UNPROCESSABLE_ENTITY);
            }
            var args = [
                {
                    name: customer.name,
                    email: customer.email,
                    catalog_06_id: customer.catalog06Id,
                    vat: customer.dni,
                    street: customer.street ,
                },
              ];//args
            const dataCostumer = {
                jsonrpc: '2.0',
                method: 'call',
                params: {
                  model: 'res.partner',
                  method: 'create',
                  args: args,
                  kwargs: {}
                }
            };
            const result = await this.http.post(url, dataCostumer, {
                headers: {
                    Cookie: `website_lang=es_PE; session_id=${sesionId}`
                }
            }).toPromise();

            return result.data;
        } catch (error) {
            return error;
        }
    }
    async registerProductTemplate(product: OdooProductCreexJson) {
        try {
            const url = `${CONFIG.Odoo.host}web/dataset/call_kw`;
            const login = await this.auth();
            const sesionId = login.result.session_id;
            // taxes_id
            // 1 = "IGV 18% Venta"  | 2 = "IGV 18% Compra" | 3 = "Percepción IGV 2%"
            //  'taxes_id': 1,
            // 'currency_id': 3,
            var args = [
                {
                    'categ_id': 1,
                    'name': product.name,
                    'uom_po_id': 1,
                    'uom_id': 1,
                    'list_price':product.price,
                    'sale_ok': true,
                    'purcharse_ok': true
                },
              ];
            const dataCostumer = {
                jsonrpc: '2.0',
                method: 'call',
                params: {
                  model: 'product.template',
                  method: 'create',
                  args: args,
                  kwargs: {}
                }
            };
            const resultProduct = await this.http.post(url, dataCostumer, {
                headers: {
                    Cookie: `website_lang=es_PE; session_id=${sesionId}`
                }
            }).toPromise();
            return {
                'productId': resultProduct.data.result,
                'sessionId': sesionId
            };
        } catch (error) {
            return error;
        }
    }
  
    
    async registerProductProduct(productId: number, sessionId: string){
        try {
            const url = `${CONFIG.Odoo.host}web/dataset/call_kw`;
            // taxes_id
            // 1 = "IGV 18% Venta"  | 2 = "IGV 18% Compra" | 3 = "Percepción IGV 2%"
            //  'taxes_id': 1,
            // 'currency_id': 3,
            const args = [
                {
                    'product_tmpl_id': productId,
                },
              ];
            const dataCostumer = {
                jsonrpc: '2.0',
                method: 'call',
                params: {
                  model: 'product.product',
                  method: 'create',
                  args: args,
                  kwargs: {}
                }
            };
            const resultProduct = await this.http.post(url, dataCostumer, {
                headers: {
                    Cookie: `website_lang=es_PE; session_id=${sessionId}`
                }
            }).toPromise();
            return resultProduct.data;
        } catch (error) {
            return error;
        }
    }
    async createProduct(product: OdooProductCreexJson){
        const resultProductTemplate: any = await this.registerProductTemplate(product);
        const resultProduct: any = await this.registerProductProduct(resultProductTemplate.productId, resultProductTemplate.sessionId);
        return resultProduct;
    }
    async checkCustomer(customer: OdoCheckCustomerJson){
        try {
            const url = `${CONFIG.Odoo.host}web/dataset/call_kw`;
            const login = await this.auth();
            const sesionId = login.result.session_id;
            const dataAuth = {
            jsonrpc: '2.0',
            method: 'call',
            params: {
              model: 'res.partner',
              method: 'search_read',
              args: [],
              kwargs:{
                  fields: ['id', 'name', 'catalog_06_id', 'vat', 'email'],
                  domain: [['email','=',customer.email]]
              }
            }
          };
            const result = await this.http.post(url, dataAuth, {
                headers: {
                    Cookie: `website_lang=es_PE; session_id=${sesionId}`
                }
            }).toPromise();
            return result.data;
        } catch (error) {
            return error;
        }
    }

    async listBill(parnetId: number){
        try {
            const url = `${CONFIG.Odoo.host}web/dataset/call_kw`;
            const login = await this.auth();
            const sesionId = login.result.session_id;
            const dataAuth = {
            jsonrpc: '2.0',
            method: 'call',
            params: {
              model: 'account.payment',
              method: 'search_read',
              args: [],
              kwargs:{
                  fields: ['id','"__last_update"'],
                  domain: [['partner_id','=',parnetId]]
              }
            }
          };
            const result = await this.http.post(url, dataAuth, {
                headers: {
                    Cookie: `website_lang=es_PE; session_id=${sesionId}`
                }
            }).toPromise();
            return result.data;
        } catch (error) {
            return error;
        }
    }

    async registerBil(bil: OdoBilJson){
        try {
            const url = `${CONFIG.Odoo.host}api/factura/create`;
            const login = await this.auth();
            const sesionId = login.result.session_id;
            const dataLines: any[] = [];
            var montoPagar = 0;
            bil.lines.map(
                (lines)=>{
                    const aux = {
                        "id": lines.membershipId,
                        "descripción": lines.description,
                        "cantidad": lines.quantity,
                        "precio": lines.price,
                        "descuento": lines.discount,
                    }
                    montoPagar = montoPagar + lines.quantity*lines.price;
                    dataLines.push(aux);
                }
            )
            const dataCreateCustomer = {
                'jsonrpc': '2.0',
                'method': 'call',
                'params': {
                  'data': {
                    'cliente_id': bil.customerId,
                    'lines': dataLines,
                    'moneda': bil.typeCurrency,
                    'journal_id': bil.journalId,
                    'tipo_operacion': bil.typeOperation,
                    'pago': {
                      'monto': montoPagar,
                      'procedencia': bil.payment.origin,
                      'detalle': bil.payment.detail,
                      'id_transaccion': bil.payment.transactionId,
                    }
                  }
                }
            }
            const result: any = await this.http.post(url, dataCreateCustomer, {
                headers: {
                    'Content-Type': 'application/json',
                    Cookie: `website_lang=es_PE; session_id=${sesionId}`
                }
            }).toPromise();
            return result.data;
        } catch (error) {
            return error;
        }
    }

   /************ FUNCTIONS *******/
   async searchCustomer(email: string, sessionId: string){
    try {
        const url = `${CONFIG.Odoo.host}web/dataset/call_kw`;
        const kwargs = {
            fields: ['id', 'name', 'catalog_06_id', 'vat', 'email', 'street'],
            domain: [['email','=',email]]
        };
        const dataAuth = {
        jsonrpc: '2.0',
        method: 'call',
        params: {
          model: 'res.partner',
          method: 'search_read',
          args: [],
          kwargs: kwargs
        }
      };
        const result = await this.http.post(url, dataAuth, {
            headers: {
                Cookie: `website_lang=es_PE; session_id=${sessionId}`
            }
        }).toPromise();
        return result.data;
    } catch (error) {
        return error;
    }
}

}
