import { Injectable, Inject } from "@nestjs/common";
import { PaymentConfigurations } from "../models/payment-configurations.model";
import { PayConfigSaveJson, PayConfigUpdateJson } from "../interfaces/payment-configurations.interface";
import * as path from 'path';
import * as fs from 'fs';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services PaymentConfigurationsService
 * @Creado 09 de Abril 2019
 */

@Injectable()
export class PaymentConfigurationsService {

    constructor(
        @Inject('PaymentConfigurations') private readonly _paymentConfigurations: typeof PaymentConfigurations
    ) { }

    async savePayConfig(bodyPayConfig: PayConfigSaveJson): Promise<PayConfigUpdateJson> {

        return await new this._paymentConfigurations(bodyPayConfig).save();
    }

    async showAllPayConfig(): Promise<PayConfigUpdateJson[]> {

        return await this._paymentConfigurations.findAll();
    }

    async getDetails(payConfigId: string){

        return await this._paymentConfigurations.findByPk(payConfigId);
    }

    async updatePayConfig(payConfigId: string, bodyPayment: Partial<PayConfigUpdateJson>): Promise<[number, Array<any>]> {

        return await this._paymentConfigurations.update(bodyPayment, {
            where: { id: payConfigId },
            returning: true
        });

    }

    async destroyPayConfig(payConfigId: string): Promise<number> {
        return await this._paymentConfigurations.destroy({
            where: { id: payConfigId },
        });
    }

    // private async deleteImg(id: string){
    //     const img = await this._paymentConfigurations.findByPk(id).then(item => item.paymentCheckImg).catch(err => err);
    
    //     const pathImagen = path.resolve(__dirname, `../../../../uploads/paymentConfiguration/paymentCheckImg/${img}`);

    //     if (fs.existsSync(pathImagen)) {
    //         fs.unlinkSync(pathImagen);
    //     }
    // }
}