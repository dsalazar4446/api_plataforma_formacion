import { Injectable, Inject } from '@nestjs/common';
import { TransactionModel } from '../models/transaction.model';
import { Category } from 'typescript-logging';
import { TransactionSaveJson } from '../interfaces/transaction.interface';

@Injectable()
export class TransactionsService {

    constructor(
        @Inject('TransactionModel') private readonly transactionModel: typeof TransactionModel,
        @Inject('SequelizeInstance') private readonly sequelizeInstance,
        @Inject('Logger') readonly logger: Category,
    ) { }

    async getAll(): Promise<TransactionModel[]> {
        return await this.transactionModel.findAll();
    }
    async save(transactionJson: TransactionSaveJson): Promise<TransactionModel> {
        return await this.transactionModel.create(transactionJson);
    }
}
