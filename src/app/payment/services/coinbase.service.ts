import { Injectable, HttpService, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { ConfigService } from '../../shared/services/config.service';
import {
    CoinbaseTransactionBaseApiOptions,
    CoinbaseOauthTokenResponse,
    CoinbaseUserDataResponse,
    CoinbaseTimeResponse,
    CoinbaseOauthHeader,
    CoinbaseApiAuthHeaders,
    CoinbaseTransactionSendApiOptions,
    ChargeJson,
} from '../interfaces/coinbase.interface';
import { AxiosResponse } from 'axios';
import { Observable, fromEventPattern } from 'rxjs';
import { catchError, take, map, flatMap } from 'rxjs/operators';
import * as requestPromise from 'request-promise';
import * as Bluebird from 'bluebird';
import * as uuid5 from 'uuid/v5';
import * as uuid4 from 'uuid/v4';
import * as Loki from 'lokijs';
import { LokiCsrfDto } from '../interfaces/loki-csrf.interface';
import * as crypto from 'crypto';
import { AppUtilsService } from '../../shared/services/app-utils.service';
import * as coinbase from 'coinbase-commerce-node';
import { CONFIG } from '../../../config';
import { BillingTransactionsModel } from '../../transactions/models/billing-transactions.model';
import { TransactionsPaymentHistory } from '../../transactions/models/transactions-payment-history.models';
import { TransactionsPayments } from '../../transactions/models/transactions-payments.models';
import * as moment from 'moment';
@Injectable()
export class CoinbaseService { 

    private coinbaseConfig: any;
    private redirectUri: string;
    private csrfCollection: Collection<LokiCsrfDto>;

    constructor(
        private readonly httpService: HttpService,
        private readonly configService: ConfigService,
        private readonly appRequestUtils: AppUtilsService,
        @Inject('BillingTransactionsModel') private readonly billingTransactionsModel: typeof BillingTransactionsModel,
        @Inject('TransactionsPaymentHistory') private readonly transactionsPaymentHistory: typeof TransactionsPaymentHistory,
        @Inject('TransactionsPayments') private readonly transactionsPayments: typeof TransactionsPaymentHistory,
        @Inject('LokijsInstance') private readonly lokiInstance: Loki,
    ) {
        this.coinbaseConfig = this.configService.get('coinbase');
        this.csrfCollection = this.lokiInstance.addCollection<LokiCsrfDto>(this.configService.get('CONFIG.database.lokijs.collections.csrfToken'));
        const oauthCnfg = this.coinbaseConfig.oauth;
        const serverCnfg = this.configService.get('server');
        this.redirectUri = `${serverCnfg.protocol}://${serverCnfg.host}:${serverCnfg.port}${oauthCnfg.redirectUrl}`;
    }

    /* ---- [Private utilitary functions] ---- */
    private genCoinbaseTokenHeader(token: string, extraHeaders: { [key: string]: string } = {}): CoinbaseOauthHeader {
        return {
            Authorization: `Bearer ${token}`,
            ...extraHeaders,
        };
    } 

    private genApiAuthHeaders(
        method: string,
        path: string,
        extraHeaders: { [key: string]: string } = {},
    ): Observable<CoinbaseApiAuthHeaders> {
        return this.coinbaseTime().pipe(
            take(1),
            map((coinbaseTime) => {
                const timestamp = coinbaseTime.epoch.toString();
                const toHash = `${timestamp}${method}${path}`;
                const key = this.coinbaseConfig.api.apiSecret;
                const coinbaseSign = crypto.createHmac('SHA256', key).update(toHash).digest('hex');
                return {
                    'CB-ACCESS-KEY': this.coinbaseConfig.api.appApiKey,
                    'CB-ACCESS-SIGN': coinbaseSign,
                    'CB-ACCESS-TIMESTAMP': timestamp,
                    'Content-Type': 'application/json',
                    ...extraHeaders,
                };
            }),
        );
    }

    /* ---- [Public functions] ---- */

    /* --- Coinbase oauth api --- */
    genAuthorizationUrl(): string {
        const oauthCnfg = this.coinbaseConfig.oauth;
        const authUrl = `${oauthCnfg.url}${oauthCnfg.authorize}`;
        const permissions = encodeURIComponent(oauthCnfg.permissions);
        const clientId = oauthCnfg.clientId;
        const redirectUri = encodeURIComponent(this.redirectUri);
        const generatedUUid = uuid5(uuid4(), this.configService.get('uuid.namespace'));
        // storing the csrf token
        this.csrfCollection.insert({
            token: generatedUUid,
            createdAt: Date.now(),
        });
        const state = encodeURIComponent(generatedUUid);
        const queryParams = `response_type=code&redirect_uri=${redirectUri}&scope=${permissions}&client_id=${clientId}&state=${state}`;
        return `${authUrl}?${queryParams}`;
    }

    // usin request-promise library instead because for some reasons it was not sending the data correctly
    async genExternalUserToken(userCode: string, state: string): Promise<CoinbaseOauthTokenResponse> {
        const oauthCnfg = this.coinbaseConfig.oauth;
        const url = `${oauthCnfg.url}${oauthCnfg.token}`;
        const storedToken = this.csrfCollection.findOne({
            token: state,
        });
        if (storedToken) {
            this.csrfCollection.remove(storedToken.$loki);
            const formData = {
                grant_type: 'authorization_code',
                code: userCode,
                client_id: oauthCnfg.clientId,
                client_secret: oauthCnfg.clientSecret,
                redirect_uri: this.redirectUri,
            };
            return await requestPromise.post(url, {
                formData,
                json: true,
            }).catch((data) => {
                throw this.appRequestUtils.requestPromiseHttpError('Error generando el token', data);
            });
        }
        throw this.appRequestUtils.httpCommonError(
            'El token no es valido o ha expirado',
            401,
        );

    } 

    /* --- Coinbase api --- */
    getUserData(token: string): Observable<CoinbaseUserDataResponse> {
        const coinbaseApiConfig = this.coinbaseConfig.api;
        const url = `${coinbaseApiConfig.url}${coinbaseApiConfig.user}`;
        return this.httpService.get(url, {
            headers: this.genCoinbaseTokenHeader(token),
        }).pipe(
            take(1),
            map((response) => {
                return response.data.data;
            }),
            catchError((responseData) => {
                throw this.appRequestUtils.httpCLienHttpError('Error intentando obtener el usuario en coinbase', responseData);
            }),
        );
    }

    transaction(
        data: CoinbaseTransactionBaseApiOptions & CoinbaseTransactionSendApiOptions,
    ): Observable<AxiosResponse<any>> {
        const transactionsPath = this.coinbaseConfig.api.transactions.replace(
            ':account_id',
            this.coinbaseConfig.creexAccount.userId,
        );
        const url = `${this.coinbaseConfig.api.url}${transactionsPath}`;
        const apiVersionUrl = this.coinbaseConfig.api.apiUrLVersion;
        return this.genApiAuthHeaders('POST', `/${apiVersionUrl}${transactionsPath}`).pipe(
            take(1),
            flatMap((headers) => {
                return this.httpService.post(url, data, {
                    headers,
                });
            }),
            take(1),
            catchError((responseData) => {
                throw this.appRequestUtils.httpCLienHttpError('La transaccion en coinbase no fue creada', responseData);
            }),
        )
    }

    coinbaseTime(): Observable<CoinbaseTimeResponse> {
        const url = `${this.coinbaseConfig.api.url}${this.coinbaseConfig.api.time}`;
        return this.httpService.get(url).pipe(
            map((response) => {
                return response.data.data;
            }),
        );
    }

    async createCharge(charge: ChargeJson){
        var currency = "BTC";
        var amount = 0;
        var address = '';
        var codeQR = '';
        return new Promise((resolve,reject)=>{
           try {
            const Client = coinbase.Client;
            const Charge = coinbase.resources.Charge;
            Client.init(CONFIG.coinbase.commerce.apiKey);
            const chargeData = {
                'name': charge.name,
                'description': charge.description,
                'local_price': {
                    'amount': charge.amount,
                    'currency': charge.currency
                },
                'pricing_type': 'fixed_price'
            }
            Charge.create(chargeData,async (error, response) =>{
                if(error){
                    reject(error);
                }

                if(charge.currency == "BTC"){
                    currency = "BTC";
                    amount = response.pricing.bitcoin.amount;
                    address = response.addresses.bitcoin;
                    codeQR = `https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=bitcoin:${address}?&amount=${amount}`;
                }
                if(charge.currency == "ETH"){
                    currency = "ETH";
                    amount = response.pricing.ethereum.amount;
                    address = response.addresses.ethereum;
                    codeQR = `https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=ethereum:${address}?&amount=${amount}`;
                }
                if(charge.currency == "BCH"){
                    currency = "BCH";
                    amount = response.pricing.bitcash.amount;
                    address = response.addresses.bitcash;
                    codeQR = `https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=bitcoincash:${address}?&amount=${amount}`;
                }

                const dataCharge = {
                    id: response.id,
                    name: response.name,
                    description: response.description,
                    code: response.code,
                    metadata: response.metadata,
                    payments: response.payments,
                    address,
                    codeQR,
                    pricing: {
                        currency,
                        amount,
                    },
                    pricing_type: response.pricing_type,
                    resource: response.resource,
                    timeline: response.timeline,
                    hosted_url: response.hosted_url,
                    created_at: response.created_at,
                    expires_at: response.expires_at,

                }
                /*const amountPayment =  await this.exchangeFull(charge.amount, charge.currency);
                // CREANDO EL TRANSACTION PAYMENT
                const resultTransactionPayment = await this.transactionsPayments.create({
                    billingTransactionId: charge.billingTransactionId,
                    gatewaysId: '',
                    transactionToken: response.id,
                    transactionPaymentAmmount: parseFloat(amountPayment),
                });

                // CREANDO EL TRANSACTION PAYMENT HISTORY
                const currentDay = moment(moment().format());
                await this.transactionsPayments.create({
                    transactionPaymentId: resultTransactionPayment.dataValues.id,
                    transactionPaymentStatus: 'pending',
                    transactionPaymentDate: currentDay,
                });*/

                resolve(dataCharge);
            });
           } catch (error) {
               reject(error);
           }
        });
    }

    async getDataCharge(chargeId: string){
        try {
            var codeQR = '';
            const url = `https://api.commerce.coinbase.com/charges/${chargeId}`;
            const headersRequest = {
                'X-CC-Api-Key': CONFIG.coinbase.commerce.apiKey, // afaik this one is not needed
                'X-CC-Version': `2018-03-22`,
            };
            const result: any = await this.httpService.get(url, { headers: headersRequest }).toPromise();
            const currency = result.data.data.pricing.local.currency;
            const amount = result.data.data.pricing.local.amount;
            let address = '';
            if(result.data.data.pricing.local.currency == "BTC"){
                address = result.data.data.addresses.bitcoin;
                codeQR = `https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=bitcoin:${address}?&amount=${amount}`;
            }
            if(result.data.data.pricing.local.currency == "ETH"){
                address = result.data.data.addresses.ethereum;
                codeQR = `https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=ethereum:${address}?&amount=${amount}`;
            }
            if(result.data.data.pricing.local.currency == "BCH"){
                address = result.data.data.addresses.bitcoincash;
                codeQR = `https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=bitcoincash:${address}?&amount=${amount}`;
            }
            const dataCharge = {
                id: result.data.data.id,
                name: result.data.data.name,
                description: result.data.data.description,
                code: result.data.data.code,
                metadata: result.data.data.metadata,
                payments: result.data.data.payments,
                address,
                codeQR,
                pricing: {
                    currency,
                    amount,
                },
                pricing_type: result.data.data.pricing_type,
                resource: result.data.data.resource,
                timeline: result.data.data.timeline,
                hosted_url: result.data.data.hosted_url,
                created_at: result.data.data.created_at,
                expires_at: result.data.data.expires_at,
            }
            return dataCharge;

        } catch (error) {
            return error;
        }
    }

    async listCharges(){
        return new Promise((resolve,reject)=>{
            try {
                const Client = coinbase.Client;
                const Charge = coinbase.resources.Charge;
                Client.init(CONFIG.coinbase.commerce.apiKey);
                Charge.list({}, (error, list, pagination) => {
                    if(error){
                        reject(error);
                    }
                    resolve({
                        'list': list,
                        'pagination': pagination
                    })
                  });
            } catch (error) {
                reject(error);
            }
        });
    }
    async exchange(amount: string){
        try {
            const url = `https://api.coinbase.com/v2/exchange-rates?currency=USD`;
            const result: any = await this.httpService.get(url).toPromise();
            const response = {
                BTC: result.data.data.rates.BTC*parseFloat(amount),
                BCH: result.data.data.rates.BCH*parseFloat(amount),
                ETH: result.data.data.rates.ETH*parseFloat(amount)
            }
            return response;
        } catch (error) {
            return error;
        }
    }
    async exchangeFull(amount: string, currency: string){
        try {
            const url = `https://api.coinbase.com/v2/exchange-rates?currency=${currency}`;
            const result: any = await this.httpService.get(url).toPromise();
            return result.data.data.rates.USD * parseFloat(amount);
        } catch (error) {
            return error;
        }
    }
    async webhook(rawBody,signature){
        const Webhook = require('coinbase-commerce-node').Webhook;
        const webhookSecret = CONFIG.coinbase.commerce.webhookSecret;
        try {
            const event = Webhook.verifySigHeader(rawBody, signature, webhookSecret);
            console.log('Successfully verified');
            console.log('Success', event.id);
            return event;
        } catch(error) {
            console.log('Failed');
            console.log(error);
            return error;
        }
    }
}
