import { Injectable, Inject, HttpStatus } from "@nestjs/common";
import { PaymentAuthorizations } from "../models/payment-authorizations.model";
import { PayAuthSaveJson, PayAuthUpdateJson } from "../interfaces/payment-authorizations.interface";
import { UserModel } from "../../user/models/user.Model";
import { BillingTransactionsModel } from "../../transactions/models/billing-transactions.model";
import { AppUtilsService } from "src/app/shared/services/app-utils.service";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services PaymentAuthorizationsService
 * @Creado 09 de Abril 2019
 */

@Injectable()
export class PaymentAuthorizationsService {

    constructor(
        @Inject('PaymentAuthorizations') private readonly _paymentAuthorizations: typeof PaymentAuthorizations,
        @Inject('UserModel') private readonly _userModel: typeof UserModel,
        @Inject('BillingTransactionsModel') private readonly _billingTransactionsModel: typeof BillingTransactionsModel,
        private readonly appUtilsService: AppUtilsService
    ) { }

    async savePayAuth(bodyNews: PayAuthSaveJson): Promise<PayAuthUpdateJson> {

        const user = await this._userModel.findByPk(bodyNews.authorizedByUser);
        const bill = await this._billingTransactionsModel.findByPk(bodyNews.billingTransactionsId);

        if (!user) {
            throw this.appUtilsService.httpCommonError('ID no asociado a ningun usuario!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM171',
                languageType: 'es',
            });
        }
        if (!bill) {
            throw this.appUtilsService.httpCommonError('ID no asociado a ningun transaction!', HttpStatus.NOT_FOUND, {
                messageCode: 'EM172',
                languageType: 'es',
            });
        }

        await this._billingTransactionsModel.update({
            transactionType:"4",
            transactionAmmount: 0,
            transactionShippment: 0,
            transactionTaxes: 0,
            transactionTotal: 0,
            transactionTryouts: 0,
            transactionDiscount: 0,
        }, {where: { id: bill.dataValues.id}});

        return await new this._paymentAuthorizations(bodyNews).save();
    }

    async showAllPayAuth(): Promise<PayAuthUpdateJson[]> {

        return await this._paymentAuthorizations.findAll({
            include: [
                {
                    model: UserModel
                },
                {
                    model: BillingTransactionsModel
                }
            ]
        });
    }

    async getDetails(payAuthId: string) {

        return await this._paymentAuthorizations.findByPk(payAuthId, {
            include: [
                {
                    model: UserModel
                },
                {
                    model: BillingTransactionsModel
                }
            ]
        });
    }

    async updatePayAuth(payAuthId: string, bodyNews: Partial<PayAuthUpdateJson>): Promise<[number, Array<PayAuthUpdateJson>]> {

        return await this._paymentAuthorizations.update(bodyNews, {
            where: { id: payAuthId },
            returning: true
        });

    }

    async destroyPayAuth(payAuthId: string): Promise<number> {

        return await this._paymentAuthorizations.destroy({
            where: { id: payAuthId },
        });
    }

}