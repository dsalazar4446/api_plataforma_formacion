export const API_URL = ' https://api.culqi.com/v2/';
export const ENDPOINT = {
    TOKENS: 'tokens',
    CHARGES: 'charges',
    REFOUNDS: 'refunds',
    COSTUMERS: 'customers',
    PLANS: 'plans',
    SUBSCRIPTIONS: 'subscriptions',
    EVENTS: 'events',
    TRANSFERS: 'transfers',
    CARDS: 'cards'
};

export enum declineCode {
    stolenCard = 'stolen_card',
    lostCard ='lost_card',
    insufficientFunds ='insufficient_funds',
    contactIssuer ='contact_issuer',
    incorrectCvv ='incorrect_cvv',
    invalidCvv = 'invalid_cvv',
    tooMany_attemptsCvv = 'too_many_attempts_cvv',
    issuerNotAvailable ='issuer_not_available',
    issuerDeclineOperation ='issuer_decline_operation',
    invalidCard ='invalid_card',
    processingError ='processing_error',
    fraudulent ='fraudulent',
    expiredCard = 'expired_card',
    culqiCard = 'culqi_card'

} 

export enum typeError {
    invalid_requestError ='invalid_request_error',
    authenticationError ='authentication_error',
    parameterError ='parameter_error',
    cardError ='card_error',
    limit_apiError ='limit_api_error',
    resourceError ='resource_error',
    apiError ='api_error'
}

export const currencyCode = {
    national: 'PEN',
    international: 'USD',
}