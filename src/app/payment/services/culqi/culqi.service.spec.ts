import { Test, TestingModule } from '@nestjs/testing';
import { CulqiService } from './culqi.service';

describe('CulqiService', () => {
  let service: CulqiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CulqiService],
    }).compile();

    service = module.get<CulqiService>(CulqiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
