import { Injectable, HttpService, HttpException, HttpStatus, Inject, forwardRef } from '@nestjs/common';
import { CONFIG } from '../../../../config';
import { API_URL, ENDPOINT, currencyCode } from './constanst';
import { map, catchError } from 'rxjs/operators';
import { AppUtilsService } from '../../../shared/services/app-utils.service';
import { CreateCharge } from '../../dto/charge.dto';
import { BillingTransactionsService } from '../../../transactions/services/billing-transactions/billing-transactions.service';
import { UsersService } from 'src/app/user/services/users.service';
import { AntiFraudDetails } from '../../dto/anti-fraud.dto';
import { GatewaysServices } from 'src/app/gateway/services/gateway.service';

@Injectable()
export class CulqiService {
    
    constructor(
        private readonly httpService: HttpService,
        private readonly appRequestUtils: AppUtilsService,
        @Inject(forwardRef(() => BillingTransactionsService)) private readonly billinTransaction: BillingTransactionsService,
        private readonly user: UsersService,
        private readonly gateway: GatewaysServices
    ){}

    private headers(status: string) {
        let headers = null;
        switch (status) {
            case 'public':
                headers = {
                    "Content-type": "application/json",
                    "Authorization": `Bearer ${CONFIG.culqi.publicKey}`
                }    
                break;
            case 'private':
                headers = {
                    "Content-type": "application/json",
                    "Authorization": `Bearer ${CONFIG.culqi.privateKey}`
                }
                break;
        }
        return headers;
    }
    private getUrl(endpoint: string, id?: string ){
        if(id){ 
            return `${API_URL}${endpoint}/${id}`
        }
        return `${API_URL}${endpoint}`
    }

    /**************************** CARGOS ****************************/

    async createCharge(billId: string,tokenCard?: string, cardType?: string){
        const bill = await this.billinTransaction.detail(billId);
        if (!bill) {
            throw new HttpException('Transaccion no encontrada', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        if(bill.transactionStatus === '2'){
            throw new HttpException('Este transaccion ya se ha pagado', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        const user = await this.user.detail(bill.userId);
        const location = await this.billinTransaction.loaction(user.id);
        
        let payMethod
        if(tokenCard == undefined){
            payMethod = await this.billinTransaction.paymentMethod(user.id) 
            console.log(payMethod.getawaysId)
            return await this.billinTransaction.processCulqi(user,bill,location,payMethod,payMethod.getawaysId,tokenCard);
        }
        else{
            if(!cardType){
                throw new HttpException('Tipo de tarjeta no enviado',HttpStatus.UNPROCESSABLE_ENTITY)
            }
            const gateways = await this.gateway.listAllGateways();
            const gatewayFound = gateways.find(gateway => {
                return gateway.gatewayName.toLowerCase() == cardType.toLowerCase();  
            });
            console.log(gatewayFound.gatewayName)
            // payMethod = await this.billinTransaction.findPaymentMethod(gateways.id)
            return await this.billinTransaction.processCulqi(user,bill,location,payMethod,gatewayFound.id,tokenCard);
        }
        
        
    }
    
    async createChargeAutomatic(payload: CreateCharge){
        try {
            const result: any = await this.httpService.post(this.getUrl(ENDPOINT.CHARGES), payload, {headers: this.headers('private')}).toPromise();
            // console.log(result)
            return result.data;
        } catch(e){
            return e.response.data
            console.log(e)
        }
    }

    async detailCharge(id: string) {
        try {
            const result = await this.httpService.get(this.getUrl(ENDPOINT.CHARGES, id), { headers: this.headers('private') }).toPromise();
            return result;
        } catch (e) {
            return e
        }
    }

    async listCharge() {
        try {
            const result = await this.httpService.get(this.getUrl(ENDPOINT.CHARGES), { headers: this.headers('private') }).toPromise();
            return result;
        } catch (e) {
            return e
        }
    }

    async updateCharge(id: string) {
        try {
            const result = await this.httpService.patch(this.getUrl(ENDPOINT.CHARGES, id), { headers: this.headers('private') }).toPromise();
            return result;
        } catch (e) {
            return e;
        }
    }
    
/**************************** CLIENTES ****************************/
    async createClient(payload: any) {
        try {
            const result = await this.httpService.post(this.getUrl(ENDPOINT.COSTUMERS), payload, { headers: this.headers('private') }).toPromise();
            return result;
        } catch (e) {
            return e;
        }
    }

    async detailClient(id: string) {
        try {
            const result = await this.httpService.get(this.getUrl(ENDPOINT.COSTUMERS, id), { headers: this.headers('private') }).toPromise();
            return result;
        } catch (e) {
            return e;
        }
    }

    async listClient() {
        try {
            const result = await this.httpService.get(this.getUrl(ENDPOINT.COSTUMERS), { headers: this.headers('private') }).toPromise();
            return result;
        } catch (e) {
            return e;
        }
    }

    async updateClient(id: string) {
        try {
            const result = await this.httpService.patch(this.getUrl(ENDPOINT.COSTUMERS, id), { headers: this.headers('private') }).toPromise();
            return result;
        } catch (e) {
            return e;
        }
    }

    /**************************** TARJETAS ****************************/
    async createCard(payload: any) {
        try {
            const result = await this.httpService.post(this.getUrl(ENDPOINT.CARDS), payload, { headers: this.headers('private') }).toPromise();
            return result;
        } catch (e) {
            return e;
        }
    }

    async detailCard(id: string) {
        try {
            const result = await this.httpService.get(this.getUrl(ENDPOINT.CARDS, id), { headers: this.headers('private') }).toPromise();
            return result;
        } catch (e) {
            return e;
        }
    }

    async listCard() {
        try {
            const result = await this.httpService.get(this.getUrl(ENDPOINT.CARDS), { headers: this.headers('private') }).toPromise();
            return result;
        } catch (e) {
            return e;
        }
    }

    async updateCard(id: string) {
        try {
            const result = await this.httpService.patch(this.getUrl(ENDPOINT.CARDS, id), { headers: this.headers('private') }).toPromise();
            return result;
        } catch (e) {
            return e;
        }
    }


}
