import { Injectable, Inject } from '@nestjs/common';
import { UserPaymentMethodsModel } from '../../models/user-payment-method.model';
import { CreateUserPaymentMethod, UpdateUserPaymentMethod } from '../../dto/user-paument-method.dto';

@Injectable()
export class UserPaymentMethodService {
    constructor(@Inject('UserPaymentMethodsModel') private readonly userPaymentMethod: typeof UserPaymentMethodsModel) {

    }

    async create(body: CreateUserPaymentMethod){
        const nuevo = new UserPaymentMethodsModel(body);
        return await nuevo.save();
    }
    async list(){
        return await this.userPaymentMethod.findAll();
    }
    async detail(id: string){
        return await this.userPaymentMethod.findByPk(id)
    }
    async findByUserId(userId: string) {
        return await this.userPaymentMethod.findOne({
            where: {
                userId
            }
        })
    }
    async findByGateawayId(gatewaysId: string) {
        return await this.userPaymentMethod.findOne({
            where: {
                getawaysId: gatewaysId
            }
        })
    }
    async update(id: string, body: Partial<UpdateUserPaymentMethod>){ 
        return await this.userPaymentMethod.update(body,{where:{id}, returning:true });
    }
    async delete(id: string){
        return await this.userPaymentMethod.destroy({where:{id}})
    }


}
