import { Inject, Logger } from '@nestjs/common';
import { Category } from 'typescript-logging';
import * as Loki from 'lokijs';
import { AppCronJob } from '../../shared/interfaces/app-cron-job.interface';
import { LokiCsrfDto } from '../interfaces/loki-csrf.interface';
import { ConfigService } from '../../shared/services/config.service';
import { CronJobService } from '../../shared/services/cron-job.service';


export class coinBaseTransactionJob implements AppCronJob {
    jobName: string;
    cronTime: string;
    description: string;
    private csrfCollection: Collection<LokiCsrfDto>;
    private tokenMaxAlive: number;

    constructor(
        @Inject('Logger') private readonly logger: Category,
        private readonly configService: ConfigService,
    ) {}

    async onTick() {
        try {
            const currentTimestamp = Date.now();
            await this.csrfCollection.findAndRemove({
                createdAt: {
                    $lte: currentTimestamp - this.tokenMaxAlive,
                },
            });
            this.logger.info('Success in ClearDueCsrfTokensJob');
        } catch (e) {
            this.logger.error('Error in ClearDueCsrfTokensJob', e);
        }
    }
}
