import { Inject, Logger } from '@nestjs/common';
import { Category } from 'typescript-logging';
import * as Loki from 'lokijs';
import { AppCronJob } from '../../shared/interfaces/app-cron-job.interface';
import { LokiCsrfDto } from '../interfaces/loki-csrf.interface';
import { ConfigService } from '../../shared/services/config.service';
import { CronJobService } from '../../shared/services/cron-job.service';


export class ClearDueCsrfTokensJob implements AppCronJob {
    jobName: string;
    cronTime: string;
    description: string;
    private csrfCollection: Collection<LokiCsrfDto>;
    private tokenMaxAlive: number;

    constructor(
        @Inject('LokijsInstance') private readonly lokiInstance: Loki,
        @Inject('Logger') private readonly logger: Category,
        private readonly configService: ConfigService,
        @Inject('CronJobService') private readonly cronJobService: CronJobService,
    ) {
        this.cronTime = this.configService.get('cronJobs.clearDueCsrfTokens.cron');
        this.jobName = this.configService.get('cronJobs.clearDueCsrfTokens.name');
        this.description = this.configService.get('cronJobs.clearDueCsrfTokens.description');
        this.csrfCollection = this.lokiInstance.addCollection<LokiCsrfDto>(this.configService.get('CONFIG.database.lokijs.collections.csrfToken'));
        this.tokenMaxAlive = this.configService.get('cronJobs.clearDueCsrfTokens.tokenMaxTimeAlive');
    }

    async onTick() {
        try {
            const currentTimestamp = Date.now();
            await this.csrfCollection.findAndRemove({
                createdAt: {
                    $lte: currentTimestamp - this.tokenMaxAlive,
                },
            });
            this.logger.info('Success in ClearDueCsrfTokensJob');
        } catch (e) {
            console.log('Error in ClearDueCsrfTokensJob', e)
            this.logger.error('Error in ClearDueCsrfTokensJob', e);
        }
    }
}
