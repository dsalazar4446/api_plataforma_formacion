import { Module } from '@nestjs/common';
import { DatabaseModuleOptionsForRoot } from './interfaces/database-module-options.interface';
import { getSequelizeProviders } from './providers/sequelize.provider';
import { Sequelize, Model } from 'sequelize-typescript';
import { CONFIG } from '../../config';
import { APP_LOGGER } from '../../logger';
import * as Loki from 'lokijs';
import { getLokiProvider } from './providers/loki.provider';
import { Category } from 'typescript-logging';

const sequelizePGOptions = CONFIG.database.sequelize.postgres;
sequelizePGOptions.logging = (message) => APP_LOGGER.debug(message);

@Module({
    imports: [],
    controllers: [],
    providers: [],
    exports: [],
})
export class DatabaseModule {
    static logger = new Category('DATABASE_MODULE', APP_LOGGER);

    static models: {
        [key: string]: typeof Model,
    } = {};

    static sequelizeInstance: Sequelize;

    static _initSequelize() {
        DatabaseModule.logger.info('Starting sequelize');
        const sequelize = new Sequelize(CONFIG.database.sequelize.postgres);
        DatabaseModule.logger.info('Sequelize initialization finished');
        this.sequelizeInstance = sequelize;
    }

    static lokiInstance = new Loki(CONFIG.database.lokijs.fileName);

    static forRoot(options: DatabaseModuleOptionsForRoot) {
        let providers = [];

        if (options.sequelize) {
            providers = providers.concat(getSequelizeProviders(this.sequelizeInstance, options.sequelize.models, DatabaseModule.models));
        }

        if (options.loki) {
            providers = providers.concat(getLokiProvider(this.lokiInstance));
        }

        return {
            module: DatabaseModule,
            providers,
            exports: providers,
        };
    }

}
