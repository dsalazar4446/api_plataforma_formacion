import { Model, Sequelize } from 'sequelize-typescript';
import { Provider } from '@nestjs/common';

export function getSequelizeProviders(
    sequelizeInstance: Sequelize,
    models: Array<typeof Model>,
    storedModels: {
        [key: string]: typeof Model,
    },
): Provider[] {
    return [
        {
            provide: 'SequelizeInstance',
            useFactory: () => {
                return sequelizeInstance;
            },
        },
        ...models.map((Comingmodel) => {
            storedModels[Comingmodel.name] = Comingmodel;
            return {
                provide: Comingmodel.name,
                useFactory: () => {
                    return Comingmodel;
                },
            };
        }),
    ];
}
