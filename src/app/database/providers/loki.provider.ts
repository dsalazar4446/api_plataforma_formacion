import { Provider } from '@nestjs/common';
import * as Loki from 'lokijs';

export function getLokiProvider(lokiInstance: Loki): Provider[] {
    return [
        {
            provide: 'LokijsInstance',
            useValue: lokiInstance,
        },
    ];
}
