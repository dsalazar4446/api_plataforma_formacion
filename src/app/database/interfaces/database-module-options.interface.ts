import { Model } from 'sequelize-typescript';

export interface DatabaseModuleOptionsForRoot {
    sequelize?: {
        models: Array<typeof Model>;
    };
    loki?: boolean;
}
