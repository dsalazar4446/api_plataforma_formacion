import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, FileFieldsInterceptor } from '@nestjs/common';
import { MessageAttachmentService } from '../services/message-attachments.service';
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { MessageAttachmentSaveJson, MessageAttachmentUpdateJson } from '../interfaces/message-attachments.interface';
import { JwtAuthGuard } from '../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiImplicitQuery, ApiResponse, ApiImplicitHeader, ApiConsumes, ApiImplicitFile } from '@nestjs/swagger';
import { MessageAttachmentModel } from '../models/message-attachments.model';
import { diskStorage } from 'multer';
import { extname } from 'path';

@Controller('message-attachment')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class MessageAttachmentController {
    constructor(
        private readonly messageAttachmentService: MessageAttachmentService,
    ) { }
    @ApiImplicitQuery({ name: 'messageAttachmentType', enum: [ 'audio','image','pdf','video'] })
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'messageAttachment', required: true})
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'messageAttachment', maxCount: 1 },
    ], {
        storage: diskStorage({
        destination: './uploads/messageAttachment'
        , filename: (req, file, cb) => {
            const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
            if(file.fieldname === 'messageAttachment'){
                req.body.messageAttachment = `${randomName}${extname(file.originalname)}`;
            }
            cb(null, `${randomName}${extname(file.originalname)}`)
        }
        })
    }))
    @Post()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: MessageAttachmentModel,
      })
    async create(@Body() message: MessageAttachmentSaveJson) {
        return await this.messageAttachmentService.create(message);
    }
    @Get()
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: MessageAttachmentModel,
        isArray: true
      })
    async getAll() {
        return await this.messageAttachmentService.findAll();
    }
    @Get(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: MessageAttachmentModel,
      })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async findById(@Param('id') idMessage) {
        return await this.messageAttachmentService.findById(idMessage);
    }
    @Put(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiResponse({
        status: 200,
        type: MessageAttachmentModel,
      })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @ApiImplicitQuery({ name: 'messageAttachmentType', enum: [ 'audio','image','pdf','video'] })
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'messageAttachment', required: true})
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'messageAttachment', maxCount: 1 },
    ], {
        storage: diskStorage({
        destination: './uploads/messageAttachment'
        , filename: (req, file, cb) => {
            const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
            if(file.fieldname === 'messageAttachment'){
                req.body.messageAttachment = `${randomName}${extname(file.originalname)}`;
            }
            cb(null, `${randomName}${extname(file.originalname)}`)
        }
        })
    }))
    async update(
        @Body()
        updateMessage: Partial<MessageAttachmentUpdateJson>,
        @Param('id') idMessage
    ) {
        return await this.messageAttachmentService.update(idMessage, updateMessage);
    }
    @Delete(':id')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') idMessage) {
        return await this.messageAttachmentService.deleted(idMessage);
    }
}
