import { ApiModelProperty } from '@nestjs/swagger';
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
export class MessageAttachmentSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    messageId: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    messageAttachment: string;
    @ApiModelProperty()
    @ApiModelProperty({ enum: ['audio', 'image', 'pdf','video']})
    @IsString()
    @IsNotEmpty()
    messageAttachmentType: string;
}
// tslint:disable-next-line: max-classes-per-file
export class MessageAttachmentUpdateJson extends MessageAttachmentSaveJson {
    @ApiModelProperty()
    @IsUUID('4')
    @IsNotEmpty()
    id: string;
}