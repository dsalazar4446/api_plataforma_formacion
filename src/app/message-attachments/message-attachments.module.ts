import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { MessageAttachmentController } from './controllers/message-attachments.controller';
import { MessageAttachmentService } from './services/message-attachments.service';
import { MessageAttachmentModel } from './models/message-attachments.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
import { ChatModel } from '../chat/models/chat.model';
import { UserModel } from '../user/models/user.Model';
import { MessageModel } from '../messages/models/messages.model';
const models = [
  MessageAttachmentModel,
  ChatModel,
  MessageModel,
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [MessageAttachmentController],
  providers: [MessageAttachmentService]
})
export class MessageAttachmentModule {}
