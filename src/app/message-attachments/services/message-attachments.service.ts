import { Injectable, Inject } from '@nestjs/common';
import { MessageAttachmentSaveJson, MessageAttachmentUpdateJson } from '../interfaces/message-attachments.interface';
import { MessageAttachmentModel } from '../models/message-attachments.model';
import { ChatModel } from '../../chat/models/chat.model';
import { UserModel } from '../../user/models/user.Model';
import { completeUrl } from '../../shared/utils/completUrl';
import { MessageModel } from '../../messages/models/messages.model';
@Injectable() 
export class MessageAttachmentService {
    constructor(
        @Inject('MessageAttachmentModel') private readonly messageAttachmentModel: typeof MessageAttachmentModel,
        @Inject('MessageModel') private readonly messageModel: typeof MessageModel,
    ) { }

// tslint:disable-next-line: max-line-length
    async findAll(): Promise<any> {
        const data = await this.messageAttachmentModel.findAll();
        const result: any[]= [];
        await Promise.all(
            await data.map(
                async (item:any)=>{
                    const dataChat = await this.messageModel.findByPk(item.dataValues.messageId);
                    item.dataValues.messageModel = dataChat.dataValues;
                    item.dataValues.messageAttachment = completeUrl('messageAttachment',item.dataValues.messageAttachment);
                    result.push(item.dataValues);
                }
            )
        )
        return result;
    }

    async findById(id: string): Promise<MessageAttachmentModel> {
        const result: any = await this.messageAttachmentModel.findByPk<MessageAttachmentModel>(id);
        const dataChat = await this.messageModel.findByPk(result.dataValues.messageId);
        result.dataValues.messageModel = dataChat.dataValues;
        result.dataValues.messageAttachment = completeUrl('messageAttachment',result.dataValues.messageAttachment);
        return result;
    }

    async create(message: MessageAttachmentSaveJson): Promise<MessageAttachmentModel> {
        const result: any = await this.messageAttachmentModel.create<MessageAttachmentModel>(message, {
            returning: true,
        });
        const dataChat = await this.messageModel.findByPk(result.dataValues.messageId);
        result.dataValues.messageModel = dataChat.dataValues;
        result.dataValues.messageAttachment = completeUrl('messageAttachment',result.dataValues.messageAttachment);
        return result;
    }
    async update(idMessage: string, messageUpdate: Partial<MessageAttachmentUpdateJson>){
        const result: any =  await this.messageAttachmentModel.update(messageUpdate, {
            where: {
                id: idMessage,
            },
            returning: true,
        });
        const dataChat = await this.messageModel.findByPk(result[1][0].dataValues.messageId);
        result[1][0].dataValues.messageModel = dataChat.dataValues;
        result[1][0].dataValues.messageAttachment = completeUrl('messageAttachment',result[1][0].dataValues.messageAttachment);
        return result;
    }
    async deleted(idMessage: string): Promise<any> {
        return await this.messageAttachmentModel.destroy({
            where: {
                id: idMessage,
            },
        });
    }
}
