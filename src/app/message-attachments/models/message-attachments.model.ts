import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey} from 'sequelize-typescript';
import { ChatModel } from '../../chat/models/chat.model';
import { UserModel } from '../../user/models/user.Model';
import { ApiModelProperty } from '@nestjs/swagger';
import { MessageModel } from '../../messages/models/messages.model';
@Table({
    tableName: 'message_attachments',
})
export class MessageAttachmentModel extends Model<MessageAttachmentModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => MessageModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'message_id',
    })
    messageId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.STRING(255),
        allowNull: false,
        field: 'message_attachment',
    })
    messageAttachment: string;
    @ApiModelProperty()
    @Column({
        type: DataType.ENUM(
            'audio',
            'image',
            'pdf',
            'video',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'message_attachment_type',
    })
    messageAttachmentType: string;
    @ApiModelProperty()
    @CreatedAt public created_at: Date;
    @ApiModelProperty()
    @UpdatedAt public updated_at: Date;
}
