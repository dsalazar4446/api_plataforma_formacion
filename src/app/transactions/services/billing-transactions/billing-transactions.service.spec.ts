import { Test, TestingModule } from '@nestjs/testing';
import { BillingTransactionsService } from './billing-transactions.service';

describe('BillingTransactionsService', () => {
  let service: BillingTransactionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BillingTransactionsService],
    }).compile();

    service = module.get<BillingTransactionsService>(BillingTransactionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
