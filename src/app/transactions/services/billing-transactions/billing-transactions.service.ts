import { Injectable, Inject, HttpException, HttpCode, HttpStatus, forwardRef } from '@nestjs/common';
import { BillingTransactionsModel } from '../../models/billing-transactions.model';
import { CreateBillingTransactions, UpdateBillingTransactions } from '../../interfaces/billing-transactions.interface';
import { Promotions } from '../../../promotions/models/promotions.model';
import * as ShortUniqueId from 'short-unique-id';
import { Sequelize, Op } from 'sequelize';
import { TransactionItemsModel } from '../../models/transaction-items.model';
import { TransactionItemsService } from '../transaction-items/transaction-items.service';
import { CreateTransactionItems } from '../../interfaces/transaction-items.interface';
import { TransactionPaymentSaveJson } from '../../interfaces/transactions-payments.interface';
import { ProductsServices } from '../../../products/services/products.services';
import { UserLocationService } from '../../../user-location/services/user-location.service';
import { UserLocationModel } from '../../../user-location/models/user-location.model';
import { CityService } from '../../../city/services/city.service';
import { CountriesService } from '../../../city/services/countries.service';
import { CityModel } from '../../../city/models/city.model';
import { Countries } from '../../../city/models/countries.model';
import { TransactionsPayments } from '../../models/transactions-payments.models';
import { PaymentAuthorizations } from '../../../payment/models/payment-authorizations.model';
import { UserMembershipsService } from '../../../memberships/services/user-memberships/user-memberships.service';
import { UserMembershipActivationPaymentService } from 'src/app/memberships/services/user-membership-activation-payment/user-membership-activation-payment.service';
import { UsersService } from '../../../user/services/users.service';
import { AppUtilsService } from '../../../shared/services/app-utils.service';
import { ProductPricingsServices } from '../../../products/services/product-pricings.services';
import { CulqiService } from '../../../payment/services/culqi/culqi.service';
import { UserPaymentMethodService } from '../../../payment/services/user-payment-method/user-payment-method.service';
import { GatewaysServices } from '../../../gateway/services/gateway.service';
import { AntiFraudDetails } from '../../../payment/dto/anti-fraud.dto';
import { CreateCharge } from '../../../payment/dto/charge.dto';
import { map } from 'rxjs/operators';
import { TransactionPaymentHistoryService } from '../transaction-payment-history/transaction-payment-history.service'; import { TransactionPaymentsService } from '../transaction-payments/transaction-payments.service';
import { TransactionPaymentHistorySaveJson } from '../../interfaces/transactions-payment-history.interface';
import * as moment from 'moment'
import { OdooService } from '../../../payment/services/odoo.service';
import { OdoBilJson, OdoProductJson, OdoCheckCustomerJson } from '../../../payment/interfaces/odoo.interface';
import { MembershipsService } from '../../../memberships/services/membership/membership.service';
import { PromotionsService } from '../../../promotions/services/promotions.service';
import { ChargeJson } from 'src/app/payment/interfaces/coinbase.interface';
import { UserModel } from '../../../user/models/user.Model';
import { EventsServices } from '../../../events/services/events.services';
import { CertificatesServices } from '../../../certificateAcademies/services/certificates/certificates.services';
import { MembershipPricingsModel } from '../../../memberships/models/membership-pricings.model';
import { ProductPricings } from '../../../products/models/product-pricings.model';
import { CertificatePricings } from '../../../certificateAcademies/models/certificates-pricings.models';
import { GroupGatewaysServices } from '../../../group-gateway/services/group-gateway.service';
import { CoursesService } from '../../../courses/services/courses/courses.service';
import { CoursesModel } from '../../../courses/models/courses.model';
@Injectable()
export class BillingTransactionsService {

    private readonly includes = [
        {
            model: TransactionItemsModel,
            include: [
                {
                    model: MembershipPricingsModel,
                },
                {
                    model: ProductPricings,
                },
                {
                    model: CertificatePricings,
                },
                {
                    model: CoursesModel
                }
            ]
        },
        TransactionsPayments,
        PaymentAuthorizations,
        Promotions

    ]

    constructor(
        @Inject('BillingTransactionsModel') private readonly billingTransactions: typeof BillingTransactionsModel,
        private readonly item: TransactionItemsService,
        private readonly products: ProductsServices,
        private readonly location: UserLocationService,
        private readonly city: CityService,
        private readonly country: CountriesService,
        private readonly userMembership: UserMembershipsService,
        private readonly membership: MembershipsService,
        private readonly activationPayment: UserMembershipActivationPaymentService,
        private readonly userService: UsersService,
        private readonly productPricingsServices: ProductPricingsServices,
        @Inject(forwardRef(() => CulqiService)) private readonly culqi: CulqiService,
        private readonly odoo: OdooService,
        private readonly userPaymentMethod: UserPaymentMethodService,
        private readonly gateway: GatewaysServices,
        private readonly groupGetaway: GroupGatewaysServices,
        private readonly locationUser: UserLocationService,
        private readonly transactionPayment: TransactionPaymentsService,
        private readonly transactionPaymentHistory: TransactionPaymentHistoryService,
        private readonly promotions: PromotionsService,
        private readonly events: EventsServices,
        private readonly certificates: CertificatesServices,
        private readonly courses: CoursesService
    ) { }

    async create(arg: CreateBillingTransactions) {
        const uid = new ShortUniqueId();
        arg.transactionCode = uid.randomUUID(9);
        const createBilling = new BillingTransactionsModel(arg);
        return await createBilling.save();
    }

    async list() {
        return await this.billingTransactions.findAll({
            include: this.includes
        });
    }

    async detail(id: string) {
        return await this.billingTransactions.findById(id, {
            include: this.includes
        });
    }

    async update(id: string, arg: UpdateBillingTransactions) {
        return await this.billingTransactions.update(arg, { where: { id } });
    }

    async delete(id: string) {
        return await this.billingTransactions.destroy({ where: { id } });
    }

    async pendingTransaction(userId) {
        return await this.billingTransactions.findOne({
            include: [
                TransactionItemsModel,
            ],
            where: {
                userId,
                transactionStatus: '0',
            }
        });
    }
    /**
     * @author Daniel Salazar
     * @param transactionId
     * @description Chequea el estado de una transaccion retorna -1 si no encontro la transaccion 
     * o retorna un objeto con del tipo 
     * {
     *   transactionstatus: string
     *   tryouts: number
     * }
     */
    async transactionCheck(transactionId) {
        const transaction = await this.detail(transactionId);
        if (!transaction) {
            return -1;
        }
        return {
            transactionstatus: transaction.transactionStatus,
            tryouts: transaction.transactionTryouts,
        };
    }

    async loaction(userId) {
        const userlocation = await this.locationUser.findByUserId(userId);
        if (!userlocation) {
            throw new HttpException('Imposible encontrar localizacion del usuario', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        const city = await this.city.findById(userlocation.cityId);
        if (!city) {
            throw new HttpException('Imposible encontrar la ciudad', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        const country = await this.country.getCountry(city.countryId);
        if (!country) {
            throw new HttpException('Imposible encontrar la pais', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        return {
            country,
            userlocation,
            city
        };
    }

    async paymentMethod(userId: string) {
        const paymentMethod = await this.userPaymentMethod.findByUserId(userId);
        if (!paymentMethod) {
            throw new HttpException('Imposible encontrar metodo de pago', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        return paymentMethod;
    }
    async findPaymentMethod(gatewayId: string) {
        const paymentMethod = await this.userPaymentMethod.findByGateawayId(gatewayId);
        if (!paymentMethod) {
            throw new HttpException('Imposible encontrar metodo de pago', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        return paymentMethod;
    }
    private async processOdoo(user: UserModel, bill: any, location: any) {
        let membershipId, membership, detail, productos;

        const checkCostumer: OdoCheckCustomerJson = {
            email: 'contacto@andresed.me',//user.email
        }

        const odooCostumer = await this.odoo.checkCustomer(checkCostumer);
        // console.log(odooCostumer)
        if (odooCostumer.result.length == 0) {
            throw new HttpException(`Odoo costumer no encotrado`, HttpStatus.UNPROCESSABLE_ENTITY)
        }

        let odooLines: OdoProductJson[] = []
        if (!bill.transactionItemsModel.length) {
            throw new HttpException('No se encontraron productos en la transaccion', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        // console.log(bill)
        bill.transactionItemsModel.forEach(item => {
            let price
            // pagode membrecias
            if (item.transactionItemType === '1' || item.transactionItemType === '2' ||
                item.transactionItemType === '3' || item.transactionItemType === '4' ||
                item.transactionItemType === '5' || item.transactionItemType === '6') {

                price = item.dataValues.membershipPricings.find(element => {
                    return element.dataValues.membershipPricingStatu == true
                });
                odooLines.push({
                    membershipId: parseInt(`${price.membershipOddoId}`),
                    description: 'Pago de membresias',
                    quantity: 1,
                    price: item.transactionItemTotal,
                    discount: 0
                });
            }
            // productos
            if (item.transactionItemType === '7' || item.transactionItemType === '8') {
                price = item.productPricings.find(element => {
                    return element.productPriceStatus == true
                });
                odooLines.push({
                    membershipId: parseInt(`${price.productOddoId}`),
                    description: 'Pago de productos',
                    quantity: item.transactionItemCant,
                    price: item.transactionItemTotal,
                    discount: 0
                });
            }
            // eventos
            if (item.transactionItemType === '9' || item.transactionItemType === '10') {
                odooLines.push({
                    membershipId: parseInt(`${item.courses[0].courseOdooId}`),
                    description: 'Pago de cursos',
                    quantity: 1,
                    price: item.transactionItemTotal,
                    discount: 0
                });
            }
            // activation
            if (item.transactionItemType === '11' || item.transactionItemType === '12') {
                price = item.membershipPricings.find(element => {
                    console.log('element', element)
                    return element.dataValues.membershipPricingStatu == true
                });
                odooLines.push({
                    membershipId: parseInt(`${price.membershipOddoId}`) || 0,
                    description: 'Pago de activacion',
                    quantity: 1,
                    price: item.transactionItemTotal,
                    discount: 0
                });
            }
            // Certificado
            if (item.transactionItemType === '17' || item.transactionItemType === '18') {
                price = item.certificatePricings.find(element => {
                    console.log('element', element)
                    return element.dataValues.certificatePricingStat == true
                });
                odooLines.push({
                    membershipId: parseInt(`${price.certificateOdooId}`),
                    description: 'Pago de certificado',
                    quantity: 1,
                    price: item.transactionItemTotal,
                    discount: 0
                })
            }
            // courses
            if (item.transactionItemType === '22' || item.transactionItemType === '23') {
                console.log('courses',item.courses)
                odooLines.push({
                    membershipId: parseInt(`${item.courses[0].courseOdooId}`),
                    description: 'Pago de certificado',
                    quantity: 1,
                    price: item.transactionItemTotal,
                    discount: 0
                })  
            }
            
        })


        const odooPayment = {
            origin: 'culqi',
            detail: 'Pago realizdo a travez de culqi',
            transactionId: bill.id,
        }
        const odooBill: OdoBilJson = {
            customerId: 11,//parseInt(`${user.oddoUserId}`),
            lines: odooLines,
            journalId: 13,
            typeOperation: '01',
            typeCurrency: 'USD',
            payment: odooPayment,
        }
        if (location.country.countryName.toLowerCase() !== 'peru') {
            odooBill.typeOperation = '02';
        }

        const registerOdoo = await this.odoo.registerBil(odooBill);


        console.log('registerOdoo', registerOdoo)
        const list = await this.odoo.listBill(user.oddoUserId);
        console.log('list', list)
        if (registerOdoo.result.success) {
            if (list) {
                if (list.length > 0) {
                    bill.billingTransactionOddoId = list[list.length - 1]
                }
            }
        }
        return registerOdoo
    }
    async findUserPaymentMethod(gateaway) {
        return await this.userPaymentMethod.findByGateawayId(gateaway.id);
    }
    replaceAll(text: string, busca: string, reemplaza: string) {
        while (text.toString().indexOf(busca) != -1)
            text = text.toString().replace(busca, reemplaza);
        return text;
    }
    async findGateway(name: string) {
        const gateway = await this.gateway.findByGatewayName(name);
        if (!gateway) {
            throw new HttpException('no se encontro el getaway', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        return gateway;
    }
    async processCulqi(user: UserModel, bill: BillingTransactionsModel, location, paymentMethod, gateway, tokenCard?: string) {
        let charge: CreateCharge;
        const antiFraudDetails: AntiFraudDetails = {
            address: location.userlocation.address1,
            address_city: location.city.cityName,
            country_code: location.country.countryAbreviation,
            last_name: user.lastName,
            phone_number: parseInt(user.cellphone),
        }
        console.log('ammount',bill.transactionAmmount+'00')
        charge = {
            amount: parseFloat(`${bill.transactionAmmount}`)*100,
            capture: true,
            currency_code: 'USD',
            email: user.email,
            source_id: null,
            antifraud_details: antiFraudDetails
        }
        if (paymentMethod) {
            charge.source_id = paymentMethod.paymentToken
        }
        // if (location.country.countryName.toLowerCase() !== 'peru') {
        //     charge.amount = parseInt(`${bill.transactionAmmount}`)
        //     charge.currency_code = 'USD'
        // }
        if (tokenCard) {
            charge.source_id = tokenCard;
        }

        const result = await this.culqi.createChargeAutomatic(charge);
        const tPBody: TransactionPaymentSaveJson = {
            billingTransactionId: bill.id,
            gatewaysId: gateway,
            transactionToken: result.id,
            transactionPaymentAmmount: parseFloat(`${bill.transactionAmmount}`)
        }
        let transactionPayment = await this.transactionPayment.saveTransaction(tPBody);

        if (!transactionPayment) {
            throw new HttpException('no se pudo agregar el pago de la transaccion', HttpStatus.UNPROCESSABLE_ENTITY);
        }


        let tPHBOdy: TransactionPaymentHistorySaveJson = {
            transactionPaymentId: transactionPayment.id,
            transactionPaymentStatus: '0',
            transactionPaymentDate: `${moment().format("YYYY-MM-DD h:mm:ss")}`,
        }

        let transactionPaymentHistory = await this.transactionPaymentHistory.saveTransaction(tPHBOdy)

        if (!transactionPaymentHistory) {
            throw new HttpException('no se pudo agregar el  historial del pago de la transaccion', HttpStatus.UNPROCESSABLE_ENTITY);
        }
        if (result.object === 'error') {
            tPHBOdy = {
                transactionPaymentId: transactionPayment.id,
                transactionPaymentStatus: '3',
                transactionPaymentDate: `${moment().format("YYYY-MM-DD h:mm:ss")}`,
            }
            transactionPayment = await this.transactionPayment.saveTransaction(tPBody);

            if (!transactionPayment) {
                throw new HttpException('no se pudo agregar el pago de la transaccion', HttpStatus.UNPROCESSABLE_ENTITY);
            }

            throw new HttpException(result, HttpStatus.UNPROCESSABLE_ENTITY)
        }
        let registerOdoo
        if (result.object === 'charge') {
            tPHBOdy = {
                transactionPaymentId: transactionPayment.id,
                transactionPaymentStatus: '2',
                transactionPaymentDate: `${moment().format("YYYY-MM-DD h:mm:ss")}`,
            }
            transactionPayment = await this.transactionPayment.saveTransaction(tPBody);

            if (!transactionPayment) {
                throw new HttpException('no se pudo agregar el pago de la transaccion', HttpStatus.UNPROCESSABLE_ENTITY);
            }

            registerOdoo = await this.processOdoo(user, bill, location);
            console.log('procecess culqui', registerOdoo.result['success:'])
            if (!registerOdoo.result['success:']) {
                throw new HttpException('Ocurrio un problema, el pago fue exitoso el proceso en odoo no se pudo completar', HttpStatus.UNPROCESSABLE_ENTITY);
            }
            bill.transactionStatus = '2'
            await bill.save()

            return {
                culqi: result,
                oddo: registerOdoo,
                bill
            }
        }
    }

    private async addProduct(billId: string, userId: string, productId: string, itemType: string, cantidad?: number) {

        const product = await this.products.getDetails(productId);
        let productPrice;
        if (!product || !product.productStatus) {
            throw new HttpException('no se encontro el producto indicado o el producto esa inactivo', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        if (!cantidad) {
            throw new HttpException('cantidad minima debe ser igual a 1', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        if (product.productPricings.length == 0) {
            throw new HttpException('no se encontro el precios para este producto', HttpStatus.UNPROCESSABLE_ENTITY)
        }

        productPrice = product.productPricings.find(price => {
            return price.productPriceStatus == true
        });

        if (!productPrice) {
            throw new HttpException('no se encontraron precios activos para este producto', HttpStatus.UNPROCESSABLE_ENTITY)
        }

        const taxes = (cantidad * parseFloat(`${productPrice.productPrice}`)) * parseFloat(`${productPrice.productTaxRate}`);
        const total = (cantidad * parseFloat(`${productPrice.productPrice}`)) + parseFloat(`${taxes}`);

        const item: CreateTransactionItems = {
            billingTransactionsId: billId,
            userId: userId,
            transactionItemType: itemType,
            transactionItemCant: cantidad,
            transactionItemAmmount: parseFloat(`${productPrice.productPrice}`),
            transactionItemTaxes: parseFloat(`${taxes}`),
            transactionItemTotal: total
        }
        const result = await this.item.create(item);
        if (!result) {
            throw new HttpException('error al crear el item', HttpStatus.UNPROCESSABLE_ENTITY)
        }

        result.$add('productPricings', [productPrice.id])
            .then(res => {
                // console.log('item/productPrice success')
            })
            .catch(
                err => {
                    // // console.log(err)
                }
            )

        const location = await this.loaction(userId);

        if (location.country.countryName.toLowerCase() === 'peru') {
            return productPrice.productPriceDeliveryLocal
        }
        return {
            item,
            odooId: productPrice.productOddoId,
            delivery: productPrice.productPriceDeliveryInternational
        }

    }

    private async addActivation(billId: string, userId: string, activationId: string, itemType: string, automatic: number = 0) {
        const activation = await this.activationPayment.detail(activationId);
        if (!activation) {
            throw new HttpException('no se encontro la activacion', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        if (activation.paymentStatus == 1) {
            throw new HttpException('activacion ya ha sido realizada', HttpStatus.UNPROCESSABLE_ENTITY)
        }

        const userMembership = await this.userMembership.detail(activation.userMembershipsId);

        if (!userMembership) {
            throw new HttpException('no se encontro usuario asociado a esta activacion', HttpStatus.UNPROCESSABLE_ENTITY)

        }
        if (userMembership.membershipPricings.length == 0) {
            throw new HttpException('no se encontro informacion del precio de la membresia', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        const membershipPrice = userMembership.membershipPricings.find(element => {
            return element.membershipPricingStatus == true;
        })
        if (!membershipPrice) {
            throw new HttpException('no se encontro precios activos', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        const taxes = parseFloat(`${activation.paymentAmmount}`) * parseFloat(`${membershipPrice.membershipPricingTaxes}`);
        const total = parseFloat(`${activation.paymentAmmount}`) + parseFloat(`${taxes}`);

        const item: CreateTransactionItems = {
            billingTransactionsId: billId,
            userId: userId,
            transactionItemType: itemType,
            transactionItemCant: 1,
            transactionItemAmmount: activation.paymentAmmount,
            transactionItemTaxes: taxes,
            transactionItemTotal: total
        }
        const result = await this.item.create(item);
        if (!result) {
            throw new HttpException('error al crear el item', HttpStatus.UNPROCESSABLE_ENTITY)
        }

        result.$add('userMembershipActivationPayments', [activationId])
            .then(res => {
                // console.log('item/userMemActivaton success')
            })
            .catch(
                err => {
                    // console.log(err)
                }
            )
        result.$add('membershipPricings', [membershipPrice.id])
            .then(res => {
                // console.log('item/userMemActivaton success')
            })
            .catch(
                err => {
                    // console.log(err)
                }
            )
        return {
            item: result,
            odooId: membershipPrice.membershipOddoId

        }
    }

    private async userMembershipItem(billId: string, userId: string, userMembershipId: string, itemType: string) {
        const userMembership = await this.userMembership.detail(userMembershipId);

        if (!userMembership) {
            throw new HttpException('no se pudo encontrar la membresia a pagar', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        const price = userMembership.membershipPricings.find(price => {
            return price.membershipPricingStatus == true;
        })
        if (!price) {
            throw new HttpException('no se encontro precio activo para esta membresia', HttpStatus.UNPROCESSABLE_ENTITY);
        }

        const ammount = parseFloat(`${price.membershipPricingMonthlyActivationValue}`)
        const taxes = parseFloat(`${price.membershipPricingMonthlyActivationValue}`) * parseFloat(`${price.membershipPricingTaxes}`)
        const total = parseFloat(ammount.toFixed(2)) + parseFloat(taxes.toFixed(2));
        const item: CreateTransactionItems = {
            billingTransactionsId: billId,
            userId: userId,
            transactionItemType: itemType,
            transactionItemCant: 1,
            transactionItemAmmount: parseFloat(ammount.toFixed(2)),
            transactionItemTaxes: parseFloat(taxes.toFixed(2)),
            transactionItemTotal: parseFloat(total.toPrecision(2))
        }

        const result = await this.item.create(item);
        if (!result) {
            throw new HttpException('error al crear el item', HttpStatus.UNPROCESSABLE_ENTITY)
        }

        result.$add('membershipPricings', [price.id])
            .then(res => {
                // console.log('item/membershipPrice success')
            })
            .catch(
                err => {
                    // console.log(err)
                }
            )
        return {
            item,
            odooId: price.membershipOddoId,
            userMembership
        }
    }
    private async addEvents(billId: string, userId: string, eventId: string, itemType: string) {
        const event = await this.events.getDetails(eventId)
        if (!event) {
            throw new HttpException('El evento no existe', HttpStatus.UNPROCESSABLE_ENTITY);
        }
        const ammount = parseFloat(`${event.eventValue}`)
        // const taxes = parseFloat(`${price.membershipPricingMonthlyActivationValue}`) * parseFloat(`${price.membershipPricingTaxes}`)
        const total = parseFloat(ammount.toFixed(2)); //+ parseFloat(taxes.toFixed(2));
        const item: CreateTransactionItems = {
            billingTransactionsId: billId,
            userId: userId,
            transactionItemType: itemType,
            transactionItemCant: 1,
            transactionItemAmmount: parseFloat(ammount.toFixed(2)),
            transactionItemTaxes: 0,
            transactionItemTotal: total
        }

        const result = await this.item.create(item);
        if (!result) {
            throw new HttpException('error al crear el item', HttpStatus.UNPROCESSABLE_ENTITY)
        }

        result.$add('events', [event.id])
            .then(res => {
                console.log('item/membershipPrice success')
            })
            .catch(
                err => {
                    console.log(err)
                }
            )

        return {
            item,
            odooId: event.eventOdooId,
            event,
        }
    }

    private async addCertificate(billId: string, userId: string, certificateId: string, itemType: string) {
        const certificate = await this.certificates.getDetailsCertificates(certificateId)
        if (!certificate) {
            throw new HttpException('El certificado no existe', HttpStatus.UNPROCESSABLE_ENTITY);
        }

        const certificatePrices = certificate.certificatePricings.find(price => {
            return price.certificatePricingStatus == true
        });

        if (!certificatePrices) {
            throw new HttpException('El precion del certificado no existe o no hay precios activos', HttpStatus.UNPROCESSABLE_ENTITY);
        }

        const ammount = parseFloat(`${certificatePrices.certificatePricing}`)
        const total = parseFloat(ammount.toFixed(2)); //+ parseFloat(taxes.toF  ixed(2));
        const item: CreateTransactionItems = {
            billingTransactionsId: billId,
            userId: userId,
            transactionItemType: itemType,
            transactionItemCant: 1,
            transactionItemAmmount: parseFloat(ammount.toFixed(2)),
            transactionItemTaxes: 0,
            transactionItemTotal: total
        }

        const result = await this.item.create(item);
        if (!result) {
            throw new HttpException('error al crear el item', HttpStatus.UNPROCESSABLE_ENTITY)
        }

        result.$add('certificatePricings', [certificatePrices.id])
            .then(res => {
                console.log('item/membershipPrice success')
            })
            .catch(
                err => {
                    console.log(err)
                }
            )

        return {
            item,
            odooId: certificatePrices.certificateOdooId,
            certificate,
        }
    }

    private async addCourses(billId: string, userId: string, courseId: string, itemType: string) {
        const course = await this.courses.detail(courseId);
        if (!course) {
            throw new HttpException('No se encontro el curso', HttpStatus.UNPROCESSABLE_ENTITY);
        }
        const ammount = parseFloat(`${course.premiumCreexPrice}`)
        const total = parseFloat(ammount.toFixed(2)); //+ parseFloat(taxes.toFixed(2));
        const item: CreateTransactionItems = {
            billingTransactionsId: billId,
            userId: userId,
            transactionItemType: itemType,
            transactionItemCant: 1,
            transactionItemAmmount: parseFloat(ammount.toFixed(2)),
            transactionItemTaxes: 0,
            transactionItemTotal: total
        }

        const result = await this.item.create(item);
        if (!result) {
            throw new HttpException('error al crear el item', HttpStatus.UNPROCESSABLE_ENTITY)
        }

        result.$add('courses', [courseId]).then(resp => {
            console.log('transaction/courses')
        }).catch(err => {
            console.log(err)
        })

        return {
            item: result,
            odooId: course.courseOdooId,
            course
        }

    }


    private async addItems(bill: BillingTransactionsModel, userId: string, productId: string, itemType: string, tokenCard?: string, cantidad?: number, locationId?: string, promotionCode?: string) {
        // pago de membresias
        if (
            itemType === '1' || itemType === '2' ||
            itemType === '3' || itemType === '4' ||
            itemType === '5' || itemType === '6'
        ) {
            const data = await this.userMembershipItem(bill.id, userId, productId, itemType);
            return data;
        }
        // producto 
        if (itemType === '7' || itemType === '8') {
            const product = await this.addProduct(bill.id, userId, productId, itemType, cantidad);
            if (bill.transactionShippment == 0) {
                bill.transactionShippment = product.delivery;
            } else {
                if (bill.transactionShippment < product.delivery) {
                    bill.transactionShippment = product.delivery;
                }
            }
            await bill.save()
            return product;
        }
        // evento
        if (itemType === '9' || itemType === '10') {
            const event = await this.events.getDetails(productId);
            return event
        }
        // activacion
        if (itemType === '11' || itemType === '12') {
            const items = await this.item.items(bill.id);
            if (items.length > 0) {
                items.forEach(item => {
                    if (item.transactionItemType === '11' || item.transactionItemType === '12') {
                        throw new HttpException('No pude ingresar mas de un pago de activacion', HttpStatus.UNPROCESSABLE_ENTITY);
                    }
                });
            }
            return await this.addActivation(bill.id, userId, productId, itemType, cantidad);

        }

        // certificate
        if (itemType === '17' || itemType === '18') {
            return await this.addCertificate(bill.id, userId, productId, itemType);
        }

        if (itemType === '22' || itemType === '23') {
            return this.addCourses(bill.id, userId, productId, itemType);
        }

    }

    private async calcTotalBill(billId, promotionCode: string) {

        const bill = await this.billingTransactions.findOne({
            include: this.includes,
            where: {
                id: billId
            }
        });
        console.log('verificando promotions')
        if (promotionCode) {
            const promotion = await this.promotions.findByCode(promotionCode);
            const date = moment(promotion.promotionValidTill);
            const today = moment();
            if (promotion.promotionStatus) {
                if (promotion.billingTransactionsModel.length >= promotion.promotionAvailableCant) {
                    throw new HttpException('Promocion no disponible', HttpStatus.UNPROCESSABLE_ENTITY);
                }
                if (today.isAfter(date)) {
                    throw new HttpException('Codigo de promocion a caducado', HttpStatus.UNPROCESSABLE_ENTITY);
                }
                bill.transactionDiscount = parseFloat(`${bill.transactionAmmount}`) * parseFloat(`${promotion.promotionPercentageDiscount}`)
                bill.transactionTotal = parseFloat(`${bill.transactionTotal}`) - parseFloat(`${bill.transactionDiscount}`)
            } else {
                throw new HttpException('Promocion inactiva', HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }
        console.log('no hay promotions')
        if (bill.transactionItemsModel.length > 0) {
            bill.transactionItemsModel.forEach(item => {
                bill.transactionAmmount = parseFloat(`${bill.transactionAmmount}`) + parseFloat(`${item.transactionItemAmmount}`)
                bill.transactionTaxes = parseFloat(`${bill.transactionTaxes}`) + parseFloat(`${item.transactionItemTaxes}`)
                bill.transactionTotal = parseFloat(`${bill.transactionTotal}`) + parseFloat(`${item.transactionItemTotal}`)
            })
            bill.transactionTotal = parseFloat(`${bill.transactionTotal}`) + parseFloat(`${bill.transactionShippment}`)
        }

        return bill;
    }

    async transactionItems(userId: string, productId: string, itemType: string, tokenCard: string, cantidad?: number, locationId?: string, promotionCode?: string) {
        let bill = await this.billingTransactions.findOne({
            where: {
                userId,
                transactionStatus: '0'
            },
            include: this.includes
        })

        if (!bill) {
            const newBill: CreateBillingTransactions = {
                userId: userId,
                userLocationId: locationId,
                transactionType: '1',
                transactionAmmount: 0,
                transactionShippment: 0,
                transactionTaxes: 0,
                transactionTotal: 0,
                transactionStatus: '0',
                transactionTryouts: 0,
                transactionObservations: '',
                transactionsUserObservations: '',
                transactionDiscount: 0,
                billingTransactionOddoId: 0,
            }
            console.log('creando bill')
            bill = await this.create(newBill);
            console.log('creando bill')
            bill = await this.billingTransactions.findByPk(bill.id, { include: this.includes });

            const result = await this.addItems(bill, userId, productId, itemType, tokenCard, cantidad, locationId, promotionCode);
            bill = await this.billingTransactions.findByPk(bill.id, { include: this.includes });
            console.log('calculando')
            if (promotionCode != undefined || promotionCode != null) {
                const promotion = await this.promotions.findByCode(promotionCode);
                const date = moment(promotion.promotionValidTill);
                const today = moment();
                if (promotion.promotionStatus) {
                    if (promotion.billingTransactionsModel.length >= promotion.promotionAvailableCant) {
                        throw new HttpException('Promocion no disponible', HttpStatus.UNPROCESSABLE_ENTITY);
                    }
                    if (today.isAfter(date)) {
                        throw new HttpException('Codigo de promocion a caducado', HttpStatus.UNPROCESSABLE_ENTITY);
                    }
                    bill.transactionDiscount = parseFloat(`${bill.transactionAmmount}`) * parseFloat(`${promotion.promotionPercentageDiscount}`)
                    bill.transactionTotal = parseFloat(`${bill.transactionTotal}`) - parseFloat(`${bill.transactionDiscount}`)
                } else {
                    throw new HttpException('Promocion inactiva', HttpStatus.UNPROCESSABLE_ENTITY);
                }
            }
            console.log('no hay promotions')
            if (bill.transactionItemsModel.length > 0) {
                bill.transactionItemsModel.forEach(item => {
                    bill.transactionAmmount = parseFloat(`${bill.transactionAmmount}`) + parseFloat(`${item.transactionItemAmmount}`)
                    bill.transactionTaxes = parseFloat(`${bill.transactionTaxes}`) + parseFloat(`${item.transactionItemTaxes}`)
                    bill.transactionTotal = parseFloat(`${bill.transactionTotal}`) + parseFloat(`${item.transactionItemTotal}`)
                })
                bill.transactionTotal = parseFloat(`${bill.transactionTotal}`) + parseFloat(`${bill.transactionShippment}`)
            }
            bill = await bill.save();
            console.log('Guarde el bill')
            // console.log(result)
            // return this.detail(bill.id);
            return {
                culqui: result.culqui,
                oddo: result.oddo,
                bill
            }
        } else {
            console.log('bill status  encontrado')
            await this.addItems(bill, userId, productId, itemType, tokenCard, cantidad, locationId, promotionCode);
            console.log('anadi el item')
            // const result = await this.calctotalBill(bill, promotionCode);
            console.log('promotionCode', promotionCode)
            if (promotionCode != undefined || promotionCode != null) {
                const promotion = await this.promotions.findByCode(promotionCode);
                const date = moment(promotion.promotionValidTill);
                const today = moment();
                if (promotion.promotionStatus) {
                    if (promotion.billingTransactionsModel.length >= promotion.promotionAvailableCant) {
                        throw new HttpException('Promocion no disponible', HttpStatus.UNPROCESSABLE_ENTITY);
                    }
                    if (today.isAfter(date)) {
                        throw new HttpException('Codigo de promocion a caducado', HttpStatus.UNPROCESSABLE_ENTITY);
                    }
                    bill.transactionDiscount = parseFloat(`${bill.transactionAmmount}`) * parseFloat(`${promotion.promotionPercentageDiscount}`)
                    bill.transactionTotal = parseFloat(`${bill.transactionTotal}`) - parseFloat(`${bill.transactionDiscount}`)
                } else {
                    throw new HttpException('Promocion inactiva', HttpStatus.UNPROCESSABLE_ENTITY);
                }
            }
            bill = await this.detail(bill.id)
            console.log(bill)

            console.log('no hay promotions')
            let taxes = 0;
            let ammount = 0;
            let total = 0;
            if (bill.transactionItemsModel.length > 0) {
                bill.transactionItemsModel.forEach(item => {
                    ammount += parseFloat(`${item.transactionItemAmmount}`)
                    taxes += parseFloat(`${item.transactionItemTaxes}`)
                    total += parseFloat(`${item.transactionItemTotal}`)
                })
                bill.transactionAmmount = ammount;
                bill.transactionTaxes = taxes;
                bill.transactionTotal = total + parseFloat(`${bill.transactionShippment}`)
            }
            await bill.save();
            return this.detail(bill.id);
        }
    }

    async payment(billId, userId) {
        let charge: CreateCharge;
        let bill = await this.billingTransactions.findByPk(billId);

        const result = await this.gateway.listAllGateways()

        const gateaway = result.find(element => {
            return element.gatewayName.toLowerCase() === 'culqi';
        })
        if (!gateaway) {
            throw new HttpException('no se encontro el getaway culqi', HttpStatus.UNPROCESSABLE_ENTITY)
        }

        let user = await this.userService.detail(userId);
        if (!user) {
            throw new HttpException('Imposible encontrar usuario', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        const paymentMethod = await this.userPaymentMethod.findByUserId(userId);
        if (!paymentMethod) {
            throw new HttpException('Imposible encontrar metodo de pago', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        const userlocation = await this.locationUser.findByUserId(userId);
        if (!userlocation) {
            throw new HttpException('Imposible encontrar localizacion del usuario', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        const city = await this.city.findById(userlocation.cityId);
        if (!city) {
            throw new HttpException('Imposible encontrar la ciudad', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        const country = await this.country.getCountry(city.countryId);
        if (!country) {
            throw new HttpException('Imposible encontrar la pais', HttpStatus.UNPROCESSABLE_ENTITY)
        }

        const antiFraudDetails: AntiFraudDetails = {
            address: userlocation.address1,
            address_city: city.cityName,
            country_code: country.countryAbreviation,
            last_name: user.lastName,
            phone_number: parseInt(user.cellphone),
        }
        charge = {
            amount: bill.transactionAmmount,
            capture: true,
            currency_code: 'PEN',
            email: user.email,
            source_id: paymentMethod.paymentToken,
            antifraud_details: antiFraudDetails
        }
        if (country.countryName.toLowerCase() !== 'peru') {
            charge.currency_code = 'USD'
        }
        const tPBody: TransactionPaymentSaveJson = {
            billingTransactionId: bill.id,
            gatewaysId: gateaway.id,
            transactionToken: null,
            transactionPaymentAmmount: bill.transactionAmmount,
        }
        const transactionPayment = await this.transactionPayment.saveTransaction(tPBody);

        if (!transactionPayment) {
            throw new HttpException('no se pudo agregar el pago de la transaccion', HttpStatus.UNPROCESSABLE_ENTITY);
        }

        // Se manda el cargo a culqi
        const resultOfCharge = await this.culqi.createChargeAutomatic(charge);

        const tPHBOdy: TransactionPaymentHistorySaveJson = {
            transactionPaymentId: transactionPayment.id,
            transactionPaymentStatus: '0',
            transactionPaymentDate: `${moment().format("YYYY-MM-DD h:mm:ss")}`,
        }
        let registerOdoo
        if (resultOfCharge.object === 'charge') {
            tPHBOdy.transactionPaymentStatus = '2'


            // const odooLines: OdoProductJson = {
            //     membershipId: parseInt(`${membershipOddoId.membershipOddoId}`),
            //     description: membershipDetail.membershipName,
            //     quantity: 1,
            //     price: item.transactionItemAmmount,
            //     discount: 0
            // }
            // const odooPayment = {
            //     origin: 'culqi',
            //     detail: 'Pago realizdo a travez de culqi',
            //     transactionId: bill.id,
            // }
            // const odooBill: OdoBilJson = {
            //     customerId: 11,//parseInt(`${user.oddoUserId}`),
            //     lines: odooLines,
            //     journalId: 13,
            //     typeOperation: '01',
            //     typeCurrency: 'USD',
            //     payment: odooPayment,
            // }
            // if (country.countryName.toLowerCase() !== 'peru') {
            //     odooBill.typeOperation = '02';
            // }


            // registerOdoo = await this.odoo.registerBil(odooBill);


            // if (!registerOdoo) {
            //     throw new HttpException('no se ha podido registrar en odoo', HttpStatus.SERVICE_UNAVAILABLE);
            // }

        }
        else {
            tPHBOdy.transactionPaymentStatus = '3'
        }
        const tPHistory = await this.transactionPaymentHistory.saveTransaction(tPHBOdy);

        if (!tPHistory) {
            throw new HttpException('no se pudo agregar el historial de la transaccion', HttpStatus.UNPROCESSABLE_ENTITY);
        }

        bill = await this.billingTransactions.findByPk(bill.id);

        user = await this.userService.detail(userId);
        if (bill.transactionTryouts === 3) {

            user.userStatus = false;
        }

        const userUpdated = await user.save()

        if (tPHistory.transactionPaymentStatus === '3') {
            bill.transactionTryouts++;
            bill.transactionStatus = '3';
        }
        if (tPHistory.transactionPaymentStatus === '2') {
            bill.transactionStatus = '2'
        }

        const list = await this.odoo.listBill(user.oddoUserId);

        if (list) {
            if (list.length > 0) {
                bill.billingTransactionOddoId = list[list.length - 1]
            }
        }

        bill = await bill.save();
        return resultOfCharge.data;
    }
    async transactionPaymentAutomatic(userId: string, productId: string, itemType: string) {
        let charge: CreateCharge;
        let bill = await this.billingTransactions.find({
            include: [
                {
                    model: TransactionItemsModel
                }
            ],
            where: {
                userId,
                transactionStatus: '3'

            }
        })

        if (!bill) {

            const newBill: CreateBillingTransactions = {
                userId: userId,
                userLocationId: null,
                transactionType: '1',
                transactionAmmount: 0,
                transactionShippment: 0,
                transactionTaxes: 0,
                transactionTotal: 0,
                transactionStatus: '0',
                transactionTryouts: 0,
                transactionObservations: '',
                transactionsUserObservations: '',
                transactionDiscount: 0,
                billingTransactionOddoId: 0,
            }
            bill = await this.create(newBill);
            const activation = await this.addActivation(bill.id, userId, productId, itemType, 1);
            const transactionItemAmmount = activation.item.transactionItemAmmount

            bill = await this.billingTransactions.findByPk(bill.id);
            bill.transactionAmmount = activation.item.transactionItemAmmount;
            bill.transactionTaxes = activation.item.transactionItemTaxes;
            bill.transactionTotal = parseFloat(`${activation.item.transactionItemTaxes}`) + parseFloat(`${activation.item.transactionItemAmmount}`)
            const billUpdated = await bill.save();

            let user = await this.userService.detail(userId);
            if (!user) {
                return {
                    status: 'error',
                    code: HttpStatus.UNPROCESSABLE_ENTITY,
                    message: 'Imposible encontrar usuario',
                }
            }
            console.log('Encontro usuario')
            const paymentMethod = await this.userPaymentMethod.findByUserId(userId);
            if (!paymentMethod) {
                return {
                    status: 'error',
                    code: HttpStatus.UNPROCESSABLE_ENTITY,
                    message: 'Imposible encontrar metodo de pago',
                }
            }
            console.log('Encontro metodo de pago')
            const userlocation = await this.locationUser.findByUserId(userId);
            if (!userlocation) {
                return {
                    status: 'error',
                    code: HttpStatus.UNPROCESSABLE_ENTITY,
                    message: 'Imposible encontrar localizacion del usuario',
                }
            }
            console.log('Encontro ubicacion')
            const city = await this.city.findById(userlocation.cityId);
            if (!city) {
                return {
                    status: 'error',
                    code: HttpStatus.UNPROCESSABLE_ENTITY,
                    message: 'Imposible encontrar la ciudad',
                }
            }
            console.log('Encontro ciudad')
            const country = await this.country.getCountry(city.countryId);
            if (!country) {
                return {
                    status: 'error',
                    code: HttpStatus.UNPROCESSABLE_ENTITY,
                    message: 'Imposible encontrar la pais',
                }
            }
            console.log('Encontro pais')
            const antiFraudDetails: AntiFraudDetails = {
                address: userlocation.address1,
                address_city: city.cityName,
                country_code: country.countryAbreviation,
                last_name: user.lastName,
                phone_number: parseInt(user.cellphone),
            }
            charge = {
                amount: parseFloat(`${billUpdated.transactionAmmount}`) * 100,
                capture: true,
                currency_code: 'USD',
                email: user.email,
                source_id: paymentMethod.paymentToken,
                antifraud_details: antiFraudDetails
            }

            const tPBody: TransactionPaymentSaveJson = {
                billingTransactionId: billUpdated.id,
                gatewaysId: paymentMethod.getawaysId,
                transactionToken: null,
                transactionPaymentAmmount: billUpdated.transactionAmmount,
            }
            const transactionPayment = await this.transactionPayment.saveTransaction(tPBody);

            if (!transactionPayment) {
                return {
                    status: 'error',
                    code: HttpStatus.UNPROCESSABLE_ENTITY,
                    message: 'no se pudo agregar el pago de la transaccion',
                }
            }
            console.log('Encontro registro de transaccion')

            // Se manda el cargo a culqi
            const resultOfCharge = await this.culqi.createChargeAutomatic(charge).catch(
                err => {
                    console.log('Error', err.response.data)
                    return err.response.data
                }
            );
            const tPHBOdy: TransactionPaymentHistorySaveJson = {
                transactionPaymentId: transactionPayment.id,
                transactionPaymentStatus: '0',
                transactionPaymentDate: `${moment().format("YYYY-MM-DD h:mm:ss")}`,
            }
            let registerOdoo
            if (resultOfCharge.object === 'charge') {
                tPHBOdy.transactionPaymentStatus = '2'
                const activation = await this.activationPayment.detail(productId);
                const userMembership = await this.userMembership.detail(activation.userMembershipsId);
                const membership = await this.membership.findById(userMembership.membershipId)
                const membershipOddoId = membership.membershipPricings.find(element => {
                    return element.membershipPricingStatus == true;
                })

                if (!membership.membershipDetails.length) {
                    return {
                        status: 'error',
                        code: HttpStatus.UNPROCESSABLE_ENTITY,
                        message: 'no hay precios relacionados con la membresia',
                    }
                }
                console.log('Encontro precio de membrecia')
                const membershipDetail = membership.membershipDetails.find(element => {
                    return element.membershipId === membership.id;
                })

                const odooLines: Array<OdoProductJson> = [{
                    membershipId: parseInt(`${membershipOddoId.membershipOddoId}`),
                    description: membershipDetail.membershipName,
                    quantity: 1,
                    price: transactionItemAmmount,
                    discount: 0
                }]
                const odooPayment = {
                    origin: 'culqi',
                    detail: 'Pago realizdo a travez de culqi',
                    transactionId: bill.id,
                }
                const odooBill: OdoBilJson = {
                    customerId: parseInt(`${user.oddoUserId}`),
                    lines: odooLines,
                    journalId: 13,
                    typeOperation: '01',
                    typeCurrency: 'USD',
                    payment: odooPayment,
                }
                if (country.countryName.toLowerCase() !== 'peru') {
                    odooBill.typeOperation = '02';
                }

                registerOdoo = await this.odoo.registerBil(odooBill);


                if (!registerOdoo) {
                    return {
                        status: 'error',
                        code: HttpStatus.UNPROCESSABLE_ENTITY,
                        message: 'no se ha podido registrar en odoo',
                    }
                }
                console.log('registro odoo')

            }
            else {
                tPHBOdy.transactionPaymentStatus = '3'
            }
            const tPHistory = await this.transactionPaymentHistory.saveTransaction(tPHBOdy);

            if (!tPHistory) {
                return {
                    status: 'error',
                    code: HttpStatus.UNPROCESSABLE_ENTITY,
                    message: 'no se pudo agregar el historial de la transaccion',
                }
            }
            console.log('registro de transaccion history')
            bill = await this.billingTransactions.findByPk(bill.id);

            user = await this.userService.detail(userId);
            if (bill.transactionTryouts === 3) {

                user.userStatus = false;
            }

            const userUpdated = await user.save()

            if (tPHistory.transactionPaymentStatus === '3') {
                bill.transactionTryouts++;
                bill.transactionStatus = '3';
            }
            if (tPHistory.transactionPaymentStatus === '2') {
                bill.transactionStatus = '2'
            }

            const list = await this.odoo.listBill(user.oddoUserId);

            if (list) {
                if (list.length > 0) {
                    bill.billingTransactionOddoId = list[list.length - 1]
                }
            }

            bill = await bill.save();

            return {
                status: 'success',
                code: HttpStatus.OK,
                message: 'Transaccion exitosa',
                billingTransactionId: bill.id,
                billingTransactionStatus: bill.transactionStatus,
                billingTransactionTryouts: bill.transactionTryouts,
                userStatus: userUpdated.userStatus
            }
        }
        else {

            // const result = await this.gateway.listAllGateways()

            // const gateaway = result.find(element => {
            //     return element.gatewayName.toLowerCase() === 'culqi';
            // })
            // if (!gateaway) {
            //     return {
            //         status: 'error',
            //         code: HttpStatus.UNPROCESSABLE_ENTITY,
            //         message: 'no se encontro el getaway culqi',
            //     }

            // }

            let user = await this.userService.detail(userId);
            if (!user) {
                return {
                    status: 'error',
                    code: HttpStatus.UNPROCESSABLE_ENTITY,
                    message: 'Imposible encontrar usuario',
                }
            }
            console.log('Encontro usuario')
            const paymentMethod = await this.userPaymentMethod.findByUserId(userId);
            if (!paymentMethod) {
                return {
                    status: 'error',
                    code: HttpStatus.UNPROCESSABLE_ENTITY,
                    message: 'Imposible encontrar metodo de pago',
                }
            }
            console.log('Encontro metodo dee pago')
            const userlocation = await this.locationUser.findByUserId(userId);
            if (!userlocation) {
                return {
                    status: 'error',
                    code: HttpStatus.UNPROCESSABLE_ENTITY,
                    message: 'Imposible encontrar localizacion del usuario',
                }
            }
            console.log('Encontro ubicacion')
            const city = await this.city.findById(userlocation.cityId);
            if (!city) {
                return {
                    status: 'error',
                    code: HttpStatus.UNPROCESSABLE_ENTITY,
                    message: 'Imposible encontrar la ciudad',
                }
            }
            const country = await this.country.getCountry(city.countryId);
            if (!country) {
                return {
                    status: 'error',
                    code: HttpStatus.UNPROCESSABLE_ENTITY,
                    message: 'Imposible encontrar la pais',
                }
            }
            console.log('Encontro pais')
            const antiFraudDetails: AntiFraudDetails = {
                address: userlocation.address1,
                address_city: city.cityName,
                country_code: country.countryAbreviation,
                last_name: user.lastName,
                phone_number: parseInt(user.cellphone),
            }
            charge = {
                amount: bill.transactionAmmount,
                capture: true,
                currency_code: 'USD',
                email: user.email,
                source_id: paymentMethod.paymentToken,
                antifraud_details: antiFraudDetails
            }
            if (country.countryName.toLowerCase() !== 'peru') {
                charge.currency_code = 'USD'
            }
            const tPBody: TransactionPaymentSaveJson = {
                billingTransactionId: bill.id,
                gatewaysId: paymentMethod.getawaysId,
                transactionToken: null,
                transactionPaymentAmmount: bill.transactionAmmount,
            }
            const transactionPayment = await this.transactionPayment.saveTransaction(tPBody);

            if (!transactionPayment) {
                return {
                    status: 'error',
                    code: HttpStatus.UNPROCESSABLE_ENTITY,
                    message: 'no se pudo agregar el pago de la transaccion',
                }
            }
            console.log('transaccion')
            // Se manda el cargo a culqi
            const resultOfCharge = await this.culqi.createChargeAutomatic(charge).catch(
                err => {
                    console.log('Error', err.response.data)
                    return err.response.data
                }
            );

            const tPHBOdy: TransactionPaymentHistorySaveJson = {
                transactionPaymentId: transactionPayment.id,
                transactionPaymentStatus: '0',
                transactionPaymentDate: `${moment().format("YYYY-MM-DD h:mm:ss")}`,
            }
            let registerOdoo
            if (resultOfCharge.object === 'charge') {
                tPHBOdy.transactionPaymentStatus = '2'
                const activation = await this.activationPayment.detail(productId);
                const userMembership = await this.userMembership.detail(activation.userMembershipsId);
                const membership = await this.membership.findById(userMembership.membershipId)
                const membershipOddoId = membership.membershipPricings.find(element => {
                    return element.membershipPricingStatus == true;
                })

                if (!membership.membershipDetails.length) {
                    return {
                        status: 'error',
                        code: HttpStatus.UNPROCESSABLE_ENTITY,
                        message: 'no hay precios relacionados con la membresia',
                    }
                }
                console.log('membrecia')
                const membershipDetail = membership.membershipDetails.find(element => {
                    return element.membershipId === membership.id;
                })

                const odooLines: Array<OdoProductJson> = [{
                    membershipId: parseInt(`${membershipOddoId.membershipOddoId}`),
                    description: membershipDetail.membershipName,
                    quantity: 1,
                    price: bill.transactionAmmount,
                    discount: 0
                }]
                const odooPayment = {
                    origin: 'culqi',
                    detail: 'Pago realizdo a travez de culqi',
                    transactionId: bill.id,
                }
                const odooBill: OdoBilJson = {
                    customerId: parseInt(`${user.oddoUserId}`),
                    lines: odooLines,
                    journalId: 13,
                    typeOperation: '01',
                    typeCurrency: 'USD',
                    payment: odooPayment,
                }
                if (country.countryName.toLowerCase() !== 'peru') {
                    odooBill.typeOperation = '02';
                }

                registerOdoo = await this.odoo.registerBil(odooBill);


                if (!registerOdoo) {
                    return {
                        status: 'error',
                        code: HttpStatus.UNPROCESSABLE_ENTITY,
                        message: 'no se ha podido registrar en odoo',
                    }
                }
                console.log('odoo')
            }
            else {
                tPHBOdy.transactionPaymentStatus = '3'
            }
            const tPHistory = await this.transactionPaymentHistory.saveTransaction(tPHBOdy);

            if (!tPHistory) {
                return {
                    status: 'error',
                    code: HttpStatus.UNPROCESSABLE_ENTITY,
                    message: 'no se pudo agregar el historial de la transaccion',
                }
            }
            console.log('registro odoo')

            bill = await this.billingTransactions.findByPk(bill.id);

            user = await this.userService.detail(userId);
            if (bill.transactionTryouts == 3) {

                user.userStatus = false;
            }

            const userUpdated = await user.save()

            if (tPHistory.transactionPaymentStatus === '3') {
                bill.transactionTryouts++;
                bill.transactionStatus = '3';
            }
            if (tPHistory.transactionPaymentStatus === '2') {
                bill.transactionStatus = '2'
            }

            const list = await this.odoo.listBill(user.oddoUserId);

            if (list) {
                if (list.length > 0) {
                    bill.billingTransactionOddoId = list[list.length - 1]
                }
            }

            bill = await bill.save();
            console.log({
                status: 'success',
                code: HttpStatus.OK,
                message: 'Transaccion exitosa',
                billingTransactionId: bill.id,
                billingTransactionStatus: bill.transactionStatus,
                billingTransactionTryouts: bill.transactionTryouts,
                userStatus: userUpdated.userStatus
            })
            return {
                status: 'success',
                code: HttpStatus.OK,
                message: 'Transaccion exitosa',
                billingTransactionId: bill.id,
                billingTransactionStatus: bill.transactionStatus,
                billingTransactionTryouts: bill.transactionTryouts,
                userStatus: userUpdated.userStatus
            }
        }

    }
}
