import { Injectable, Inject } from "@nestjs/common";
import { TransacItemsHProductPricings } from "../../models/trans-items-h-prod-pric.model";
import { TransItemsHProdPriceSaveJson, TransItemsHProdPriceUpdateJson } from "../../interfaces/trans-items-h-prod-pric.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services TransItemsHProdPricService
 * @Creado 03 Abril 2019
 */

@Injectable()
export class TransItemsHProdPricService {

    constructor(
        @Inject('TransacItemsHProductPricings') private readonly _transacItemsHProductPricings: typeof TransacItemsHProductPricings
    ) { }

    async save(body: TransItemsHProdPriceSaveJson): Promise<TransItemsHProdPriceUpdateJson> {

        return await new this._transacItemsHProductPricings(body).save();
    }

    async destroy(id: string): Promise<number> {

        return await this._transacItemsHProductPricings.destroy({
            where: { id },
        });
    }

}