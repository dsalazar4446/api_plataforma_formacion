import { Injectable, Inject } from "@nestjs/common";
import { TransacItemsHCertiPricings } from "../../models/transac-items-h-certi-pricings.model";
import { TransItemsHasCertiPricingsSaveJson, TransItemsHasCertiPricingsUpdateJson } from "../../interfaces/transac-items-h-certi-pricings.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services TransItemsHasCertiPricings
 * @Creado 01 Abril 2019
 */

@Injectable()
export class TransItemsHasCertiPricingsServices {

    constructor(
        @Inject('TransacItemsHCertiPricings') private readonly _transacItemsHCertiPricings: typeof TransacItemsHCertiPricings
    ) { }

    async save(body: TransItemsHasCertiPricingsSaveJson): Promise<TransItemsHasCertiPricingsUpdateJson> {

        return await new this._transacItemsHCertiPricings(body).save();
    }

    async destroy(id: string): Promise<number> {

        return await this._transacItemsHCertiPricings.destroy({
            where: { id },
        });
    }
}