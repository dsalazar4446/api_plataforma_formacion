import { Injectable, Inject } from "@nestjs/common";
import { TransactItemsHMemberPricings } from "../../models/transact-items-h-member-pricings.model";
import { TransactItemsHMemberPricingsSaveJson, TransactItemsHMemberPricingsUpdateJson } from "../../interfaces/transact-items-h-member-pricings.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services TransactItemsHMemberPricingsService
 * @Creado 03 Abril 2019
 */

@Injectable()
export class TransactItemsHMemberPricingsService {

    constructor(
        @Inject('TransactItemsHMemberPricings') private readonly _transactItemsHMemberPricings: typeof TransactItemsHMemberPricings
    ) { }

    async save(body: TransactItemsHMemberPricingsSaveJson): Promise<TransactItemsHMemberPricingsUpdateJson> {

        return await new this._transactItemsHMemberPricings(body).save();
    }

    async destroy(id: string): Promise<number> {

        return await this._transactItemsHMemberPricings.destroy({
            where: { id },
        });
    }

}