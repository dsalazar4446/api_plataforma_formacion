import { Injectable, Inject } from "@nestjs/common";
import { TransactionsItemsHasEvents } from "../../models/transactions-items-has-events.model";
import { TransactionsItemsHasEventsSaveJson, TransactionsItemsHasEventsUpdateJson } from "../../interfaces/transactions-items-has-events.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services TransactionsItemsHasEventsService
 * @Creado 03 Abril 2019
 */

@Injectable()
export class TransactionsItemsHasEventsService {

    constructor(
        @Inject('TransactionsItemsHasEvents') private readonly _transactionsItemsHasEvents: typeof TransactionsItemsHasEvents
    ) { }

    async save(body: TransactionsItemsHasEventsSaveJson): Promise<TransactionsItemsHasEventsUpdateJson> {

        return await new this._transactionsItemsHasEvents(body).save();
    }

    async destroy(id: string): Promise<number> {

        return await this._transactionsItemsHasEvents.destroy({
            where: { id },
        });
    }

}