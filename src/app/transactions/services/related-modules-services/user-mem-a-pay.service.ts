import { Injectable, Inject } from "@nestjs/common";
import { UserMemActPaymHTranItemsSaveJson, UserMemActPaymHTranItemsUpdateJson } from "../../interfaces/user-mem-a-pay.interface";
import { UserMemActPaymHTranItems } from "../../models/user-mem-a-pay.model";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Services UserMemAPayService
 * @Creado 01 Abril 2019
 */

@Injectable()
export class UserMemAPayService {

    constructor(
        @Inject('UserMemActPaymHTranItems') private readonly _userMemActPaymHTranItems: typeof UserMemActPaymHTranItems
    ) { }

    async save(body: UserMemActPaymHTranItemsSaveJson): Promise<UserMemActPaymHTranItemsUpdateJson> {

        return await new this._userMemActPaymHTranItems(body).save();
    }

    async destroy(id: string): Promise<number> {

        return await this._userMemActPaymHTranItems.destroy({
            where: { id },
        });
    }
}