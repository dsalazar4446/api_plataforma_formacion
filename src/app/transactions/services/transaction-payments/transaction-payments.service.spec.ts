import { Test, TestingModule } from '@nestjs/testing';
import { TransactionPaymentsService } from './transaction-payments.service';

describe('TransactionPaymentsService', () => {
  let service: TransactionPaymentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TransactionPaymentsService],
    }).compile();

    service = module.get<TransactionPaymentsService>(TransactionPaymentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
