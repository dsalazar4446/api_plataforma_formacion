import { TransactionPaymentSaveJson, TransactionPaymentUpdateJson } from '../../interfaces/transactions-payments.interface';
import { TransactionsPayments } from '../../models/transactions-payments.models';
import { Injectable, Inject } from '@nestjs/common';
import * as ShortUniqueId from 'short-unique-id';
import { Gateways } from '../../../gateway/models/gateway.model';
import { TransactionsPaymentHistory } from '../../models/transactions-payment-history.models';
const uid = new ShortUniqueId();

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller TransactionPaymentsService
 * @Creado 12 de Marzo 2019
 */

@Injectable()
export class TransactionPaymentsService {

    /**
     * @param TransactionsPayments Modelo de la tabla transactions_payments
     */
    constructor(
        @Inject('TransactionsPayments') private readonly _transactionsPayments: typeof TransactionsPayments,
    ) { }


    async saveTransaction(bodyTransaction: TransactionPaymentSaveJson): Promise<TransactionsPayments> {

        // bodyTransaction.transactionToken = uid.randomUUID(17) + uid.randomUUID(17);

        return await new this._transactionsPayments(bodyTransaction).save();
    }


    async showAllTransactions(): Promise<TransactionsPayments[]> {

        return await this._transactionsPayments.findAll();

    }


    async getTransaction(transactionID): Promise<any> {

        return await this._transactionsPayments.findByPk(transactionID,{
            include: [
                {
                    model: Gateways
                },
                {
                    model: TransactionsPaymentHistory
                }
            ]
        });
    }

    async updateTransaction(id: string, bodyTransaction: Partial<TransactionPaymentUpdateJson>) {

        return await this._transactionsPayments.update(bodyTransaction, { 
            where: { id },
            returning: true 
        });

    }


    async destroyTransaction(transactionID): Promise<number> {

        return await this._transactionsPayments.destroy({
            where: {id: transactionID},
        });
    }

}
