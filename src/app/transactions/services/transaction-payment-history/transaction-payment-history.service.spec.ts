import { Test, TestingModule } from '@nestjs/testing';
import { TransactionPaymentHistoryService } from './transaction-payment-history.service';

describe('TransactionPaymentHistoryService', () => {
  let service: TransactionPaymentHistoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TransactionPaymentHistoryService],
    }).compile();

    service = module.get<TransactionPaymentHistoryService>(TransactionPaymentHistoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
