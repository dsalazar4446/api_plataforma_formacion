import { TransactionPaymentHistorySaveJson, TransactionPaymentHistoryUpdateJson } from '../../interfaces/transactions-payment-history.interface';
import { TransactionsPaymentHistory } from '../../models/transactions-payment-history.models';
import { Injectable, Inject } from '@nestjs/common';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller TransactionPaymentHistoryService
 * @Creado 12 de Marzo 2019
 */

@Injectable()
export class TransactionPaymentHistoryService {

    /**
     * @param TransactionsPaymentHistory Modelo de la tabla transactions_payment_history
     */
    constructor(
        @Inject('TransactionsPaymentHistory') private readonly _transactionsPaymentHistory: typeof TransactionsPaymentHistory,
    ) { }


    async saveTransaction(bodyTransaction: TransactionPaymentHistorySaveJson): Promise<TransactionsPaymentHistory> {

        return await new this._transactionsPaymentHistory(bodyTransaction).save();
    }

    async showAllTransactions(): Promise<TransactionsPaymentHistory[]> {

        return await this._transactionsPaymentHistory.findAll();

    }


    async getTransaction(transactionID): Promise<any> {
        return await this._transactionsPaymentHistory.findByPk(transactionID);
    }


    async updateTransaction(id: string, bodyTransaction: Partial<TransactionPaymentHistoryUpdateJson>) {

        return await this._transactionsPaymentHistory.update(bodyTransaction, { 
            where: { id } ,
            returning: true
        });

    }

 
    async destroyTransaction(transactionID): Promise<number> {

        return await this._transactionsPaymentHistory.destroy({
            where: { id: transactionID },
        });
    }

}
