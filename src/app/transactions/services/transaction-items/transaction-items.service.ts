import { Injectable, Inject } from '@nestjs/common';
import { TransactionItemsModel } from '../../models/transaction-items.model';
import { CreateTransactionItems, UpdateTransactionItems } from '../../interfaces/transaction-items.interface';
import * as Bluebird from 'bluebird';
import { CertificatePricings } from '../../../certificateAcademies/models/certificates-pricings.models';
import { UserMembershipActivationPaymentsModel } from '../../../memberships/models/user-membership-activation-payments.model';
import { Events } from '../../../events/models/events.model';
import { MembershipPricingsModel } from '../../../memberships/models/membership-pricings.model';
import { ProductPricings } from '../../../products/models/product-pricings.model';


const include = [
    {
        model: CertificatePricings
    },
    {
        model: UserMembershipActivationPaymentsModel
    },
    {
        model: Events
    },
    {
        model: MembershipPricingsModel
    },
    {
        model: ProductPricings
    }
]
@Injectable()
export class TransactionItemsService {
    constructor(@Inject('TransactionItemsModel') private readonly transactionItems: typeof TransactionItemsModel ) {}

    async create(arg: CreateTransactionItems){
        const createBilling = new TransactionItemsModel(arg);
        return createBilling.save();
    }

    async list(){
        return this.transactionItems.findAll();
    }

    async detail(id: string) {
        return this.transactionItems.findById(id, {
            include
        });
    }

    async items(billingTransactionsId: string){
        return await this.transactionItems.findAll({
            include,
            where: {
                billingTransactionsId
            }
        })
    }
    async update(id: string, arg: UpdateTransactionItems) {
        return this.transactionItems.update(arg, {where: {id}});
    }

    async delete(id: string) {
        return this.transactionItems.destroy({where: {id}});
    }
}
