import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, Delete, NotFoundException, HttpStatus, Param } from "@nestjs/common";
import { AppExceptionFilter } from "../../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { AjvValidationPipe } from "../../../shared/pipes/ajv-validation.pipe";
import { TransactItemsHMemberPricingsService } from "../../services/related-modules-services/transact-items-h-member-pricings.service";
import { TransactItemsHMemberPricingsSaveJson } from "../../interfaces/transact-items-h-member-pricings.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller TransactItemsHMemberPricingsController
 * @Creado 01 Abril 2019
 */

@Controller('transact_items_h_member_pricings')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class TransactItemsHMemberPricingsController {

    constructor(private readonly _transactItemsHMemberPricingsService: TransactItemsHMemberPricingsService,
        private readonly appUtilsService: AppUtilsService
    ) { }

    @Post()
    async create(@Body() body: TransactItemsHMemberPricingsSaveJson) {

        const create = await this._transactItemsHMemberPricingsService.save(body);

        if (!create) {
            return this.appUtilsService.httpCommonError('TransactItemsHMemberPricings Has Users does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return create;

    }

    @Delete(':id')
    async deleteTrans(@Param('id') id: string) {

        const deleteTrans = await this._transactItemsHMemberPricingsService.destroy(id);

        if (!deleteTrans) {
            throw new NotFoundException('TransactItemsHMemberPricings has Users does not exist!');
        }

        return deleteTrans;
    }

}