import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Delete, NotFoundException, Param } from "@nestjs/common";
import { AppExceptionFilter } from "../../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { AjvValidationPipe } from "../../../shared/pipes/ajv-validation.pipe";
import { TransactionsItemsHasEventsService } from "../../services/related-modules-services/transactions-items-has-events.service";
import { TransactionsItemsHasEventsSaveJson } from "../../interfaces/transactions-items-has-events.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller TransactionsItemsHasEventsController
 * @Creado 01 Abril 2019
 */

@Controller('transactions-items-has-events')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class TransactionsItemsHasEventsController {

    constructor(private readonly _transactionsItemsHasEventsServcs: TransactionsItemsHasEventsService,
        private readonly appUtilsService: AppUtilsService
    ) { }

    @Post()
    async create(@Body() body: TransactionsItemsHasEventsSaveJson) {

        const create = await this._transactionsItemsHasEventsServcs.save(body);

        if (!create) {
            return this.appUtilsService.httpCommonError('TransactionsItemsHasEvents Has Users does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return create;

    }

    @Delete(':id')
    async deleteTrans(@Param('id') id: string) {

        const deleteTrans = await this._transactionsItemsHasEventsServcs.destroy(id);

        if (!deleteTrans) {
            throw new NotFoundException('TransactionsItemsHasEvents has Users does not exist!');
        }

        return deleteTrans;
    }

}