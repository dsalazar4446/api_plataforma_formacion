import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, NotFoundException, Delete, Param } from "@nestjs/common";
import { AppExceptionFilter } from "../../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { AjvValidationPipe } from "../../../shared/pipes/ajv-validation.pipe";
import { TransItemsHProdPriceSaveJson } from "../../interfaces/trans-items-h-prod-pric.interface";
import { TransItemsHProdPricService } from "../../services/related-modules-services/trans-items-h-prod-pric.service";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller TransItemsHProdPricController
 * @Creado 01 Abril 2019
 */

@Controller('trans-items-h-prod-pric')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class TransItemsHProdPricController {

    constructor(private readonly _transItemsHProdPricService: TransItemsHProdPricService,
        private readonly appUtilsService: AppUtilsService
    ) { }

    @Post()
    async create(@Body() body: TransItemsHProdPriceSaveJson) {

        const create = await this._transItemsHProdPricService.save(body);

        if (!create) {
            return this.appUtilsService.httpCommonError('TransItemsHProdPric Has Users does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return create;

    }

    @Delete(':id')
    async deleteTrans(@Param('id') id: string) {

        const deleteTrans = await this._transItemsHProdPricService.destroy(id);

        if (!deleteTrans) {
            throw new NotFoundException('TransItemsHProdPric has Users does not exist!');
        }

        return deleteTrans;
    }

}