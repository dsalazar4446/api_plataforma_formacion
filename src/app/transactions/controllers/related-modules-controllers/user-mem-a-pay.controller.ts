import { Controller, UseFilters, UseInterceptors, Post, UsePipes, HttpStatus, Body, Delete, NotFoundException, Param } from "@nestjs/common";
import { UserMemAPayService } from "../../services/related-modules-services/user-mem-a-pay.service";
import { UserMemActPaymHTranItemsSaveJson } from "../../interfaces/user-mem-a-pay.interface";
import { AppExceptionFilter } from "../../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { AjvValidationPipe } from "../../../shared/pipes/ajv-validation.pipe";


/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller UserMemAPayController
 * @Creado 01 Abril 2019
 */

@Controller('user-mem-act-paym-h-tran-items')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class UserMemAPayController {


    constructor(private readonly _userMemAPayService: UserMemAPayService,
        private readonly appUtilsService: AppUtilsService
    ) { }

    @Post()
    async create(@Body() body: UserMemActPaymHTranItemsSaveJson) {

        const create = await this._userMemAPayService.save(body);

        if (!create) {
            return this.appUtilsService.httpCommonError('UserMemActPaymHTranItems does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return create;

    }

    @Delete(':id')
    async deleteTrans(@Param('id') id: string) {

        const deleteTrans = await this._userMemAPayService.destroy(id);

        if (!deleteTrans) {
            throw new NotFoundException('UserMemActPaymHTranItems does not exist!');
        }

        return deleteTrans;
    }
}