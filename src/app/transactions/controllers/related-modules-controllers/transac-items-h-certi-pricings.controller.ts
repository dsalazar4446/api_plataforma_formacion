import { Controller, UseFilters, UseInterceptors, Body, HttpStatus, Delete, NotFoundException, Post, UsePipes, Param } from "@nestjs/common";
import { AppExceptionFilter } from "../../../shared/filters/app-exception.filter";
import { AppResponseInterceptor } from "../../../shared/interceptors/app-response.interceptor";
import { AppUtilsService } from "../../../shared/services/app-utils.service";
import { AjvValidationPipe } from "../../../shared/pipes/ajv-validation.pipe";
import { TransItemsHasCertiPricingsServices } from "../../services/related-modules-services/transac-items-h-certi-pricings.service";
import { TransItemsHasCertiPricingsSaveJson } from "../../interfaces/transac-items-h-certi-pricings.interface";

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller TransItemsHasCertiPricingsController
 * @Creado 01 Abril 2019
 */

@Controller('transac-items-h-certi-pricings')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class TransItemsHasCertiPricingsController {


    constructor(private readonly _transItemsHasCertiPricingsServices: TransItemsHasCertiPricingsServices,
        private readonly appUtilsService: AppUtilsService
    ) { }

    @Post()
    async create(@Body() body: TransItemsHasCertiPricingsSaveJson) {

        const create = await this._transItemsHasCertiPricingsServices.save(body);

        if (!create) {
            return this.appUtilsService.httpCommonError('TransItemsHasCertiPricings Has Users does not created!', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return create;

    }

    @Delete(':id')
    async deleteTrans(@Param('id') id: string) {

        const deleteTrans = await this._transItemsHasCertiPricingsServices.destroy(id);

        if (!deleteTrans) {
            throw new NotFoundException('TransItemsHasCertiPricings has Users does not exist!');
        }

        return deleteTrans;
    }
}