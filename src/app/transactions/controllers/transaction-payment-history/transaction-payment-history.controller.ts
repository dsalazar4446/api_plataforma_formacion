import { Controller, UseFilters, Post, Body, Get, Param, NotFoundException, UsePipes, Put, Delete, UseInterceptors, UseGuards } from '@nestjs/common';
import { AppExceptionFilter } from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { TransactionPaymentHistorySaveJson, TransactionPaymentHistoryUpdateJson } from '../../interfaces/transactions-payment-history.interface';
import { TransactionPaymentHistoryService } from '../../services/transaction-payment-history/transaction-payment-history.service';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller TransactionPaymentHistoryController
 * @Creado 12 de Marzo 2019
 */

@Controller('transactions/transaction-payment-history')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class TransactionPaymentHistoryController {

    constructor(private readonly _transactionPaymentHistServcs: TransactionPaymentHistoryService) { }

    /**
     * @param bodyTransaction Cuerpo de la transaction { transactionPaymentId: string; transactionPaymentStatus: string;transactionPaymentDate: string; }
     */
    @Post()
    async create(@Body() bodyTransaction: TransactionPaymentHistorySaveJson) {

        return await this._transactionPaymentHistServcs.saveTransaction(bodyTransaction);

    }

    /**
     * @param idTransaction ID de la transaction
     * Obtiene una transaccion especificada por el ID
     * ejemplo: 'e086d2c6-a9f9-4006-bb04-cdedf01d3c0d'
     */
    @Get('details/:id')
    async findById(@Param('id') idTransaction) {

        const fetchTransaction = await this._transactionPaymentHistServcs.getTransaction(idTransaction);

        if (!fetchTransaction) {
            throw new NotFoundException('Transaction does not exist!');
        }

        return fetchTransaction;
    }

    @Get()
    async findAll() {

        const fetchTransaction = await this._transactionPaymentHistServcs.showAllTransactions();

        if (!fetchTransaction) {
            throw new NotFoundException('Transaction does not exist!');
        }

        return fetchTransaction;
    }



    /**
     * @param idTransaction ID de la transaction ejemplo: 'e086d2c6-a9f9-4006-bb04-cdedf01d3c0d'
     * @param bodyTransaction Cuerpo de la transaction { transactionPaymentId: string; transactionPaymentStatus: string;transactionPaymentDate: string; }
     * Actualiza una transaccion especificada por el ID
     */
    @Put(':id')
    async updateTransaction(@Param('id') id: string, @Body() bodyTransaction: Partial<TransactionPaymentHistorySaveJson>) {

        const updateTransaction = await this._transactionPaymentHistServcs.updateTransaction(id, bodyTransaction);

        if (!updateTransaction) {
            throw new NotFoundException('Transaction does not exist!');
        }

        return updateTransaction;
    }

    /**
     * @param idTransaction ID de la transaction ejemplo: 'e086d2c6-a9f9-4006-bb04-cdedf01d3c0d'
     * Elimina una transaccion especificada por el ID
     */
    @Delete(':id')
    async deleteTransaction(@Param('id') transactionID) {

        const deleteTransaction = await this._transactionPaymentHistServcs.destroyTransaction(transactionID);

        if (!deleteTransaction) {
            throw new NotFoundException('Transaction does not exist!');
        }

        return deleteTransaction;
    }


}
