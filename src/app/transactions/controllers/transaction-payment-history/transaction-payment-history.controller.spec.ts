import { Test, TestingModule } from '@nestjs/testing';
import { TransactionPaymentHistoryController } from './transaction-payment-history.controller';

describe('TransactionPaymentHistory Controller', () => {
  let controller: TransactionPaymentHistoryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TransactionPaymentHistoryController],
    }).compile();

    controller = module.get<TransactionPaymentHistoryController>(TransactionPaymentHistoryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
