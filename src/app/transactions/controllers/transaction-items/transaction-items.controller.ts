import { Controller, Post, UsePipes, UseInterceptors, Body, Get, Param, Put, Delete, UseFilters, UseGuards } from '@nestjs/common';
import { TransactionItemsService } from '../../services/transaction-items/transaction-items.service';
import { UpdateTransactionItems, CreateTransactionItems } from '../../interfaces/transaction-items.interface';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { TransactionItemsModel } from '../../models/transaction-items.model';

@ApiUseTags('Transactions')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(new AppResponseInterceptor())
// // @UseGuards(new JwtAuthGuard())
@Controller('tansactions/transaction-items')
export class TransactionItemsController {
    constructor(private readonly transactionItemsService: TransactionItemsService) {}

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: TransactionItemsModel })
    async create(@Body() body: CreateTransactionItems) {
        return await this.transactionItemsService.create(body);
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: TransactionItemsModel })
    async list() {
        return await this.transactionItemsService.list();
    }
    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: TransactionItemsModel })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async detail(@Param('id') id: string) {
        return await this.transactionItemsService.detail(id);
    }
    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: TransactionItemsModel })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(@Body() body: UpdateTransactionItems,
                 @Param('id') id: string) {
                     return await this.transactionItemsService.update(id, body);
    }
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async delete(@Param('id') id: string){
        return await this.transactionItemsService.delete(id);
    }
}
