import { Test, TestingModule } from '@nestjs/testing';
import { TransactionPaymentsController } from './transaction-payments.controller';

describe('TransactionPayments Controller', () => {
  let controller: TransactionPaymentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TransactionPaymentsController],
    }).compile();

    controller = module.get<TransactionPaymentsController>(TransactionPaymentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
