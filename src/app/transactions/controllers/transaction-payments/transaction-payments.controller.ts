import { Controller, UseFilters, Post, Body, Get, Param, NotFoundException, UsePipes, Put, Delete, UseInterceptors, UseGuards } from '@nestjs/common';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { TransactionPaymentSaveJson } from '../../interfaces/transactions-payments.interface';
import { TransactionPaymentsService } from '../../services/transaction-payments/transaction-payments.service';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';

/**
 * @author Marcos André Llumiquinga Therman
 * @correo mallth194@gmail.com
 * @contacto +584128373552
 * @Controller TransactionPaymentHistoryController
 * @Creado 12 de Marzo 2019
 */

@Controller('transactions/transaction-payments')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
// // @UseGuards(new JwtAuthGuard())
export class TransactionPaymentsController {

    constructor(private readonly _transactionPaymentServcs: TransactionPaymentsService) { }


    @Post()
    async create(@Body() bodyTransaction: TransactionPaymentSaveJson) {

        return await this._transactionPaymentServcs.saveTransaction(bodyTransaction);

    }


    @Get('details/:id')
    async findById(@Param('id') idTransaction) {

        const fetchTransaction = await this._transactionPaymentServcs.getTransaction(idTransaction);

        if (!fetchTransaction) {
            throw new NotFoundException('Transaction does not exist!');
        }

        return fetchTransaction;
    }

    @Get()
    async findAll() {

        const fetchTransaction = await this._transactionPaymentServcs.showAllTransactions();

        if (!fetchTransaction) {
            throw new NotFoundException('Transaction does not exist!');
        }

        return fetchTransaction;
    }



    @Put(':id')
    async updateTransaction(@Param('id') id: string, @Body() bodyTransaction: Partial<TransactionPaymentSaveJson>) {

        const updateTransaction = await this._transactionPaymentServcs.updateTransaction(id, bodyTransaction);

        if (!updateTransaction) {
            throw new NotFoundException('Transaction does not exist!');
        }

        return updateTransaction;
    }



    @Delete(':id')
    async deleteTransaction(@Param('id') transactionID) {

        const deleteTransaction = await this._transactionPaymentServcs.destroyTransaction(transactionID);

        if (!deleteTransaction) {
            throw new NotFoundException('Transaction does not exist!');
        }

        return deleteTransaction;
    }


}

