import { Controller, Post, Get, Put, Delete, Body, UsePipes, UseInterceptors, Param, UseFilters, UseGuards } from '@nestjs/common';
import { BillingTransactionsService } from '../../services/billing-transactions/billing-transactions.service';
import { CreateBillingTransactions, UpdateBillingTransactions } from '../../interfaces/billing-transactions.interface';
import { AppExceptionFilter} from '../../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../../shared/pipes/ajv-validation.pipe';
import { JwtAuthGuard } from './../../../auths/guards/jwt-auth.guard';
import * as ShortUniqueId from 'short-unique-id';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader, ApiResponse } from '@nestjs/swagger';
import { exists } from 'fs';
import { BillingTransactionsModel } from '../../models/billing-transactions.model';

@ApiUseTags('Transactions')
@UseFilters(new AppExceptionFilter())
// // @UseGuards(new JwtAuthGuard())
@Controller('transactions/billing-transactions')
export class BillingTransactionsController {
    constructor(private readonly billingTransaction: BillingTransactionsService) {}

    @Post()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: BillingTransactionsModel })
    @UseInterceptors(new AppResponseInterceptor())
    async create(@Body() body: CreateBillingTransactions) {
        const uid = new ShortUniqueId();
        body.transactionCode = uid.randomUUID(8);
        return await this.billingTransaction.create(body);
    }

    @Get()
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: BillingTransactionsModel })
    @UseInterceptors(new AppResponseInterceptor())
    async list() {
        return await this.billingTransaction.list();
    }
    @Get(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: BillingTransactionsModel })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(new AppResponseInterceptor())
    async detail(@Param('id') id: string) {
        return await this.billingTransaction.detail(id);
    }   

    @Get(':userId/:itemType/:productId/:cant?/:token?/:locationId?/:promotionCode?')
    @ApiImplicitParam({ name: 'userId', required: true, type: 'string' })
    @ApiImplicitParam({ name: 'itemType', required: true, type: 'string' })
    @ApiImplicitParam({ name: 'productId', required: true, type: 'string' })
    @ApiImplicitParam({ name: 'cant', required: false, type: 'string' })
    @ApiImplicitParam({ name: 'token', required: false, type: 'string' })
    @ApiImplicitParam({ name: 'locationId', required: false, type: 'string' })
    @ApiImplicitParam({ name: 'promotionCode', required: false, type: 'string' })
    addItems(
        @Param('userId') userId: string, @Param('productId') productId: string, @Param('itemType') itemType: string,
        @Param('cant') cant: number, @Param('token') token, @Param('locationId') locationId: string, @Param('promotionCode') promotionCode: string
    ) {
        return this.billingTransaction.transactionItems(userId, productId, itemType, token, cant, locationId, promotionCode)
        // return this.billingTransaction.transactionPaymentAutomatic(userId, productId, itemType)
    }

    @Get(':billId/:userId')
    payment(
        @Param('userId') userId: string, @Param('billId') billId: string
    ) {
        return this.billingTransaction.payment(billId,userId)
    }
    @Put(':id')
    @ApiImplicitHeader({ name: 'language', required: true })
    @ApiResponse({ status: 200, type: BillingTransactionsModel })
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    async update(@Body() body: UpdateBillingTransactions,
                 @Param('id') id: string) {
                     return await this.billingTransaction.update(id, body);
    }
    @Delete(':id')
    @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
    @UseInterceptors(new AppResponseInterceptor())
    async delete(@Param('id') id: string){
        return await this.billingTransaction.delete(id);
    }
    @Delete()
    async deleteAll(){
        const transaction = await this.billingTransaction.list();
        transaction.forEach(element => {
            this.billingTransaction.delete(element.id)
        });
    }
    @Get('exist/:userId')
    @ApiImplicitParam({ name: 'UserId', required: true, type: 'string' })
    exists(@Param('userId') userId){
        // return this.billingTransaction.checkout(userId);
    }

}
