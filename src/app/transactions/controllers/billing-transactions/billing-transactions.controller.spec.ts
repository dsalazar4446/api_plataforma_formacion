import { Test, TestingModule } from '@nestjs/testing';
import { BillingTransactionsController } from './billing-transactions.controller';

describe('BillingTransactions Controller', () => {
  let controller: BillingTransactionsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BillingTransactionsController],
    }).compile();

    controller = module.get<BillingTransactionsController>(BillingTransactionsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
