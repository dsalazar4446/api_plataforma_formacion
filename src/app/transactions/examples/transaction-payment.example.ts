export const TransactionPaymentExample = `
{
    id: string,
    billingTransactionId: string,
    gatewaysId: string,
    transactionToken: string,
    transactionPaymentAmmount: number
}
`

export const TransactionPaymentArrayExample = `
[
    ${TransactionPaymentExample}
]
`