import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { BillingTransactionsController } from './controllers/billing-transactions/billing-transactions.controller';
import { BillingTransactionsService } from './services/billing-transactions/billing-transactions.service';
import { BillingTransactionsModel } from './models/billing-transactions.model';
import { TransactionPaymentHistoryController } from './controllers/transaction-payment-history/transaction-payment-history.controller';
import { TransactionPaymentsController } from './controllers/transaction-payments/transaction-payments.controller';
import { TransactionPaymentHistoryService } from './services/transaction-payment-history/transaction-payment-history.service';
import { TransactionPaymentsService } from './services/transaction-payments/transaction-payments.service';
import { TransactionsPaymentHistory} from './models/transactions-payment-history.models';
import { TransactionsPayments } from './models/transactions-payments.models';
import { TransactionItemsModel } from './models/transaction-items.model';
import { TransactionItemsService } from './services/transaction-items/transaction-items.service';
import { TransactionItemsController } from './controllers/transaction-items/transaction-items.controller';
import { TransacItemsHCertiPricings } from './models/transac-items-h-certi-pricings.model';
import { UserMemActPaymHTranItems } from './models/user-mem-a-pay.model';
import { TransacItemsHProductPricings } from './models/trans-items-h-prod-pric.model';
import { TransactItemsHMemberPricings } from './models/transact-items-h-member-pricings.model';
import { TransactionsItemsHasEvents } from './models/transactions-items-has-events.model';
import { TransItemsHasCertiPricingsServices } from './services/related-modules-services/transac-items-h-certi-pricings.service';
import { UserMemAPayService } from './services/related-modules-services/user-mem-a-pay.service';
import { TransItemsHProdPricService } from './services/related-modules-services/trans-items-h-prod-pric.service';
import { TransactionsItemsHasEventsService } from './services/related-modules-services/transactions-items-has-events.service';
import { TransactItemsHMemberPricingsService } from './services/related-modules-services/transact-items-h-member-pricings.service';
import { TransItemsHasCertiPricingsController } from './controllers/related-modules-controllers/transac-items-h-certi-pricings.controller';
import { UserMemAPayController } from './controllers/related-modules-controllers/user-mem-a-pay.controller';
import { TransItemsHProdPricController } from './controllers/related-modules-controllers/trans-items-h-prod-pric.controller';
import { TransactionsItemsHasEventsController } from './controllers/related-modules-controllers/transactions-items-has-events.controller';
import { TransactItemsHMemberPricingsController } from './controllers/related-modules-controllers/transact-items-h-member-pricings.controller';
import { ProductsModule } from '../products/products.module';
import { UserLocationModule } from '../user-location/user-location.module';
import { CityModule } from '../city/city.module';
import { PaymentModule } from '../payment/payment.module';
import { GatewayModule } from '../gateway/gateway.module';
import { PromotionsModule } from '../promotions/promotions.module';
import { EventsModule } from '../events/events.module';
import { CertificatesAcademiesModule } from '../certificateAcademies/academies.module';
import { GroupGatewayModule } from '../group-gateway/group-gateway.module';
import { CoursesModule } from '../courses/courses.module';
import { TransactionCoursesModel } from './models/transaction-items-h-courses.model';


const models = [
    TransactionsPaymentHistory,
    TransactionsPayments,
    BillingTransactionsModel,
    TransactionItemsModel,
    TransacItemsHCertiPricings,
    UserMemActPaymHTranItems,
    TransacItemsHProductPricings,
    TransactionsItemsHasEvents,
    TransactItemsHMemberPricings,
    TransactionCoursesModel
];

const providers = [
    BillingTransactionsService,
    TransactionPaymentHistoryService,
    TransactionPaymentsService,
    TransactionItemsService,
    TransItemsHasCertiPricingsServices,
    UserMemAPayService,
    TransItemsHProdPricService,
    TransactionsItemsHasEventsService,
    TransactItemsHMemberPricingsService
];

const controllers = [
    BillingTransactionsController,
    TransactionPaymentHistoryController,
    TransactionPaymentsController,
    TransactionItemsController,
    TransItemsHasCertiPricingsController,
    UserMemAPayController,
    TransItemsHProdPricController,
    TransactionsItemsHasEventsController,
    TransactItemsHMemberPricingsController
];



@Module({
    imports: [
        HttpModule,
        DatabaseModule.forRoot({
            sequelize: {
                models,
            },
            loki: true,
        }),
        ProductsModule,
        UserLocationModule,
        CityModule,
        forwardRef(() => PaymentModule),
        GatewayModule,
        GroupGatewayModule,
        PromotionsModule,
        EventsModule,
        CertificatesAcademiesModule,
        CoursesModule
    ],
    controllers,
    providers,
    exports: [
        BillingTransactionsService,
        TransactionPaymentHistoryService,
        TransactionPaymentsService,
        TransactionItemsService
    ]
})
export class TransactionsModule { }
