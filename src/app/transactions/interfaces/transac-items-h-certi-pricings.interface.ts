export interface TransItemsHasCertiPricingsSaveJson {
    transactionItemId: string;
    certificatePricingId: string;
}
export interface TransItemsHasCertiPricingsUpdateJson extends TransItemsHasCertiPricingsSaveJson {
    id: string;
}