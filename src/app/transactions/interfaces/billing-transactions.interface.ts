import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString, IsNumber, IsOptional, IsEmpty } from "class-validator";

export class CreateBillingTransactions {
    @ApiModelProperty()
    @IsUUID('4')
    userId?: string;
    @ApiModelProperty()
    @IsOptional()
    @IsUUID('4')
    userLocationId?: string;
    @ApiModelProperty()
    @IsOptional()
    @IsString()
    transactionCode?: string;
    @ApiModelProperty()
    @IsString()
    transactionType?: string;
    @ApiModelProperty()
    @IsNumber()
    transactionAmmount?: number;
    @ApiModelProperty()
    @IsNumber()
    transactionShippment?: number;
    @ApiModelProperty()
    @IsNumber()
    transactionTaxes?: number;
    @ApiModelProperty()
    @IsNumber()
    transactionTotal?: number;
    @ApiModelProperty()
    @IsString()
    transactionStatus?: string;
    @ApiModelProperty()
    @IsNumber()
    transactionTryouts?: number;
    @ApiModelProperty()
    @IsString()
    transactionObservations?: string;
    @ApiModelProperty()
    @IsString()
    transactionsUserObservations?: string;
    @ApiModelProperty()
    @IsNumber()
    transactionDiscount: number;
    @ApiModelProperty()
    @IsNumber()
    billingTransactionOddoId: number;


}

export class UpdateBillingTransactions extends CreateBillingTransactions {
    @ApiModelProperty()
    @IsUUID('4')
    id?: string;
}