export interface TransItemsHProdPriceSaveJson {
    transactionItemId: string;
    productPricingId: string;
}
export interface TransItemsHProdPriceUpdateJson extends TransItemsHProdPriceSaveJson {
    id: string;
}