import { ApiModelProperty } from "@nestjs/swagger";
import { IsUUID, IsString, IsNumber } from "class-validator";

export class CreateTransactionItems {
    
    @ApiModelProperty()
    @IsUUID('4')
    billingTransactionsId: string;
    
    @ApiModelProperty()
    @IsUUID('4')
    userId: string;
    
    @ApiModelProperty()
    @IsString()
    transactionItemType: string;
    
    @ApiModelProperty()
    @IsNumber()
    transactionItemCant: number;
    
    @ApiModelProperty()
    @IsNumber()
    transactionItemAmmount: number;
    
    @ApiModelProperty()
    @IsNumber()
    transactionItemTaxes: number;

    @ApiModelProperty()
    @IsNumber()
    transactionItemTotal: number;

}
export class UpdateTransactionItems extends CreateTransactionItems {
    @ApiModelProperty()
    @IsUUID('4')
    id: string;
}