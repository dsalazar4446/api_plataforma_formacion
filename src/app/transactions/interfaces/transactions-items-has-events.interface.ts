export interface TransactionsItemsHasEventsSaveJson {
    transactionItemId: string;
    eventId: string;
}
export interface TransactionsItemsHasEventsUpdateJson extends TransactionsItemsHasEventsSaveJson {
    id: string;
}