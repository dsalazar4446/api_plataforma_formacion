import { IsUUID, IsNotEmpty, IsString } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class TransactionPaymentHistorySaveJson {
    
    @ApiModelProperty()
    @IsUUID('4',{message: 'transactionPaymentId property must a be uuid'})
    @IsNotEmpty({message: 'transactionPaymentId property not must null'})
    transactionPaymentId: string;

    @IsString({message: 'transactionPaymentStatus property must a be uuid'})
    @IsNotEmpty({message: 'transactionPaymentStatus property not must null'})
    transactionPaymentStatus: string;

    @IsString({message: 'transactionPaymentDate property must a be uuid'})
    @IsNotEmpty({message: 'transactionPaymentDate property not must null'})
    transactionPaymentDate: string;
}
export class TransactionPaymentHistoryUpdateJson extends TransactionPaymentHistorySaveJson{
    
    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
