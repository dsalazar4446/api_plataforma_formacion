export interface TransactItemsHMemberPricingsSaveJson {
    transactionItemId: string;
    membershipPricingId: string;
}
export interface TransactItemsHMemberPricingsUpdateJson extends TransactItemsHMemberPricingsSaveJson {
    id: string;
}