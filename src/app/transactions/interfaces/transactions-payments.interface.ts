import { IsNotEmpty, IsUUID, IsNumber, IsOptional } from "class-validator";
import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";

export class TransactionPaymentSaveJson {

    @ApiModelProperty()
    @IsUUID('4', { message: 'gatewaysId property must a be uuid' })
    @IsNotEmpty({ message: 'gatewaysId property not must null' })
    billingTransactionId: string;
    
    @ApiModelProperty()
    @IsUUID('4',{message: 'gatewaysId property must a be uuid'})
    @IsNotEmpty({message: 'gatewaysId property not must null'})
    gatewaysId: string;
    
    @ApiModelPropertyOptional()
    @IsOptional()
    transactionToken?: string;

    
    @ApiModelProperty()
    @IsNumber(null, {message: 'transactionPaymentAmmount property must a be number'})
    @IsNotEmpty({message: 'transactionPaymentAmmount property not must null'})
    transactionPaymentAmmount: number;
}
export class TransactionPaymentUpdateJson extends TransactionPaymentSaveJson{

    @IsUUID('4',{message: 'id property must a be uuid'})
    @IsNotEmpty({message: 'id property not must null'})
    id: string;
}
