export interface UserMemActPaymHTranItemsSaveJson {
    transactionItemId: string;
    userMemAPaymentsId: string;
}
export interface UserMemActPaymHTranItemsUpdateJson extends UserMemActPaymHTranItemsSaveJson {
    id: string;
}