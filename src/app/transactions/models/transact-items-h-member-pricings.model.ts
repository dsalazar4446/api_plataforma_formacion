import { Table, Column, ForeignKey, DataType, CreatedAt, UpdatedAt, Model, BelongsTo } from "sequelize-typescript";
import { TransactionItemsModel } from "./transaction-items.model";
import { MembershipPricingsModel } from "../../memberships/models/membership-pricings.model";

@Table({
    tableName: 'transact_items_h_member_pricings',
})
export class TransactItemsHMemberPricings extends Model<TransactItemsHMemberPricings>{


    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ForeignKey(() => TransactionItemsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_item_id',
    })
    transactionItemId: string;
    
    @BelongsTo(() => TransactionItemsModel)
    transactionItems: TransactionItemsModel;
    

    @ForeignKey(() => MembershipPricingsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'membership_pricing_id',
    })
    membershipPricingId: string;
    
    @BelongsTo(() => MembershipPricingsModel)
    membershipPricings: MembershipPricingsModel;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}