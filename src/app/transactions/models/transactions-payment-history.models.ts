import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, BelongsTo, ForeignKey } from 'sequelize-typescript';
import { TransactionsPayments } from './transactions-payments.models';

@Table({
    tableName: 'transaction_payment_history',
})
export class TransactionsPaymentHistory extends Model<TransactionsPaymentHistory>{

    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;


    @ForeignKey(() => TransactionsPayments)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_payments_id',
    })
    transactionPaymentId: string;

    
    @Column({
        type: DataType.ENUM('0','1', '2', '3', '4', '5', '6'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_payment_status',
    })
    transactionPaymentStatus: string;
    
    @Column({
        type: DataType.TIME,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_payment_date',
    })
    transactionPaymentDate: Date;
    
    /**
     * RELACIONES
     * TransactionsPaymentHistory pertenece a TransactionsPayments
     */
    @BelongsTo(() => TransactionsPayments)
    transactionsPayments: TransactionsPayments;
    
    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
    
}