import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { TransactionItemsModel } from './transaction-items.model';
import { ProductPricings } from '../../products/models/product-pricings.model';

@Table({
    tableName: 'transac_items_h_product_pricings',
})
export class TransacItemsHProductPricings extends Model<TransacItemsHProductPricings>{

    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    
    @ForeignKey(() => TransactionItemsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_item_id',
    })
    transactionItemId: string;

    @BelongsTo(() => TransactionItemsModel)
    transactionItems: TransactionItemsModel;


    @ForeignKey(() => ProductPricings)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'product_pricing_id',
    })
    productPricingId: string;
    @BelongsTo(() => ProductPricings)
    productPricings: ProductPricings;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;


}