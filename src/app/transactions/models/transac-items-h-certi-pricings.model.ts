import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { TransactionItemsModel } from './transaction-items.model';
import { CertificatePricings } from '../../certificateAcademies/models/certificates-pricings.models';

@Table({
    tableName: 'transac_items_h_certi_pricings',
})
export class TransacItemsHCertiPricings extends Model<TransacItemsHCertiPricings>{

    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ForeignKey(() => TransactionItemsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_item_id',
    })
    transactionItemId: string;
    @BelongsTo(() => TransactionItemsModel)
    transactionItems:TransactionItemsModel;

    @ForeignKey(() => CertificatePricings)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'certificate_pricing_id',
    })
    certificatePricingId: string;
    @BelongsTo(() => CertificatePricings)
    certificatePricings: CertificatePricings;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;


}