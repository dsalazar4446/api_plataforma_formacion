import { Table, Model, Column, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt, HasMany, BelongsToMany } from 'sequelize-typescript';
import { UserModel } from '../../user/models/user.Model';
import { UserLocationModel } from '../../user-location/models/user-location.model';
import { TransactionsPayments } from './transactions-payments.models';
import { TransactionItemsModel } from './transaction-items.model';
import { PaymentAuthorizations } from '../../payment/models/payment-authorizations.model';
import { Promotions } from '../../promotions/models/promotions.model';
import { PromotionsHasTransactions } from '../../promotions/models/promotions-has-transactions.model';
import { UserRankings } from '../../ranking/models/user-ranking.model';
import { UserRankingsHasTransactions } from '../../ranking/models/user-ranking-has-transactions.model';
import { RankingTransactions } from '../../ranking/models/ranking-transactions.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { TransactionIteArrayExample } from '../examples/transaction-items.example';

@Table({
    tableName: 'billing_transactions',
})
export class BillingTransactionsModel extends Model<BillingTransactionsModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        unique: true,
        autoIncrement: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
            isUUID: 4
        },
    })
    id: string;
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;

    @ApiModelPropertyOptional()
    @ForeignKey(() => UserLocationModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: true,
        validate: {
            notEmpty: true,
        },
        field: 'user_location_id',
    })
    userLocationId: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_code',
    })
    transactionCode: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2', '3', '4'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_type',
    })
    transactionType: string;

    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_ammount',
    })
    transactionAmmount: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_shippment',
    })
    transactionShippment: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_taxes',
    })
    transactionTaxes: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_total',
    })
    transactionTotal: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('0', '1', '2', '3'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transactions_status',
    })
    transactionStatus: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_tryouts',
    })
    transactionTryouts: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: true,
        // validate: {
        //     notEmpty: true,
        // },
        field: 'transaction_observations',
    })
    transactionObservations: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.TEXT,
        allowNull: true,
        // validate: {
        //     notEmpty: true,
        // },
        field: 'transactions_user_observations',
    })
    transactionsUserObservations: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_discount',
    })
    transactionDiscount: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'billing_transaction_oddo_id',
    })
    billingTransactionOddoId: number;



    // tslint:disable-next-line: variable-name
    @CreatedAt created_at: Date;
    // tslint:disable-next-line: variable-name
    @UpdatedAt updated_at: Date;

    @BelongsTo(() => UserLocationModel)
    userLocation: UserLocationModel;

    @ApiModelPropertyOptional({ example: TransactionIteArrayExample })
    @HasMany(() => TransactionsPayments)
    payments: TransactionsPayments[];
    @ApiModelPropertyOptional({ example: TransactionIteArrayExample })
    @HasMany(() => TransactionItemsModel)
    transactionItemsModel: TransactionItemsModel[];
    @ApiModelPropertyOptional()
    @HasMany(() => PaymentAuthorizations)
    paymentAuthorizations: PaymentAuthorizations[];
    // @BelongsToMany(() => Promotions, () => PromotionsHasTransactions)
    @ApiModelPropertyOptional()
    @BelongsToMany(() => Promotions, () => PromotionsHasTransactions)
    promotions: Promotions[];

    @BelongsTo(() => UserModel)
    user: UserModel;
    
    @ApiModelPropertyOptional()
    @BelongsToMany(() => UserRankings, {
        through: {
            model: () => UserRankingsHasTransactions,
            unique: true,
        }
    })

    userRankings: UserRankings[];
    @ApiModelPropertyOptional()
    @HasMany(() => RankingTransactions)
    billingTransactionsRanking: RankingTransactions;    
}