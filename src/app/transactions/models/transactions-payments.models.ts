import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, HasMany, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { TransactionsPaymentHistory } from './transactions-payment-history.models';
import { BillingTransactionsModel } from './billing-transactions.model';
import { Gateways } from 'src/app/gateway/models/gateway.model';

@Table({
    tableName: 'transaction_payments',
})
export class TransactionsPayments extends Model<TransactionsPayments>{


    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ForeignKey(() => BillingTransactionsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'billing_transaction_id',
    })
    billingTransactionId: string;

    
    @ForeignKey(() => Gateways)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'gateways_id',
    })
    gatewaysId: string;
    
    @Column({
        type: DataType.STRING,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_token',
    })
    transactionToken: string;
    
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_payment_ammount',
    })
    transactionPaymentAmmount: number;
    
    /**
     * RELACIONES
     * TransactionsPayments tiene muchos Transactions
     */
    @BelongsTo(() => BillingTransactionsModel)
    billingTransactionsModel: BillingTransactionsModel;

    @BelongsTo(() => Gateways)
    gateways: Gateways;
    
    /**
     * RELACIONES
     * TransactionsPayments tiene muchos TransactionsPaymentHistory
     */
    @HasMany(() => TransactionsPaymentHistory)
    transactionsPaymentHistory: TransactionsPaymentHistory[];
    
    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;
}