import { Table, Column, ForeignKey, DataType, CreatedAt, UpdatedAt, Model, BelongsTo } from "sequelize-typescript";
import { TransactionItemsModel } from "./transaction-items.model";
import { UserMembershipActivationPaymentsModel } from "../../memberships/models/user-membership-activation-payments.model";

@Table({
    tableName: 'user_mem_act_paym_h_tran_items',
})
export class UserMemActPaymHTranItems extends Model<UserMemActPaymHTranItems>{


    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ForeignKey(() => TransactionItemsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_item_id',
    })
    transactionItemId: string;

    @BelongsTo(() => TransactionItemsModel)
    transactionItems: TransactionItemsModel;    

    @ForeignKey(() => UserMembershipActivationPaymentsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_mem_a_payments_id',
    })
    userMemAPaymentsId: string;

    @ForeignKey(() => UserMembershipActivationPaymentsModel)


    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}