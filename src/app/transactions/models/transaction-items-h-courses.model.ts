import { Table, Model, Column, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt } from "sequelize-typescript";
import { CoursesModel } from "src/app/courses/models/courses.model";
import { TransactionItemsModel } from "./transaction-items.model";

@Table({ tableName: 'transaction_items_h_courses' })
export class TransactionCoursesModel extends Model<TransactionCoursesModel>{
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;
    
    @ForeignKey(() => TransactionItemsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_item_id',
    })
    transactionItemId: string;

    @BelongsTo(() => TransactionItemsModel)
    transactionItems: TransactionItemsModel;


    @ForeignKey(() => CoursesModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'course_id',
    })
    productPricingId: string;
    
    @BelongsTo(() => CoursesModel)
    courses: CoursesModel;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;

}