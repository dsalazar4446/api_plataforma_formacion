import { Table, Model, Column, DataType, ForeignKey, BelongsTo, CreatedAt, UpdatedAt, BelongsToMany } from 'sequelize-typescript';
import { BillingTransactionsModel } from './billing-transactions.model';
import { CertificatePricings } from '../../certificateAcademies/models/certificates-pricings.models';
import { UserMembershipActivationPaymentsModel } from '../../memberships/models/user-membership-activation-payments.model';
import { ProductPricings } from '../../products/models/product-pricings.model';
import { Events } from '../../events/models/events.model';
import { MembershipPricingsModel } from '../../memberships/models/membership-pricings.model';
import { UserModel } from '../../user/models/user.Model';
import { TransacItemsHProductPricings } from './trans-items-h-prod-pric.model';
import { TransacItemsHCertiPricings } from './transac-items-h-certi-pricings.model';
import { UserMemActPaymHTranItems } from './user-mem-a-pay.model';
import { TransactionsItemsHasEvents } from './transactions-items-has-events.model';
import { TransactItemsHMemberPricings } from './transact-items-h-member-pricings.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { CoursesModel } from 'src/app/courses/models/courses.model';
import { TransactionCoursesModel } from './transaction-items-h-courses.model';

@Table({
    tableName: 'transaction_items',
})

export class TransactionItemsModel extends Model<TransactionItemsModel> {
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string; 
    @ApiModelPropertyOptional()
    @ForeignKey(() => BillingTransactionsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'billing_transaction_id',
    })
    billingTransactionsId: string;
    @BelongsTo(() => BillingTransactionsModel)
    bllingTransaction: BillingTransactionsModel;
    @ApiModelPropertyOptional()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'user_id',
    })
    userId: string;

    @BelongsTo(() => UserModel)
    user: UserModel;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.ENUM('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_item_type',
    })
    transactionItemType: string;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_item_cant',
    })
    transactionItemCant: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_item_ammount',
    })
    transactionItemAmmount: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_item_taxes',
    })
    transactionItemTaxes: number;
    @ApiModelPropertyOptional()
    @Column({
        type: DataType.DOUBLE,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_item_total',
    })
    transactionItemTotal: number;

    /**
     * RELACIONES
     * TransactionItemsModel pertenece a BillingTransactionsModel
     * TransactionItemsModel pertenece a muchos CertificatePricings
     * TransactionItemsModel pertenece a muchos UserMembershipActivationPaymentsModel
     * TransactionItemsModel pertenece a muchos ProductPricings
     * TransactionItemsModel pertenece a muchos Events
     * TransactionItemsModel pertenece a muchos MembershipPricingsModel
     */

    @BelongsTo(() => BillingTransactionsModel)
    billingTransactionsModel: BillingTransactionsModel[];

    @ApiModelPropertyOptional()
    @BelongsToMany(() => ProductPricings, () => TransacItemsHProductPricings)
    productPricings: ProductPricings[];
    
    @ApiModelPropertyOptional()
    @BelongsToMany(() => CertificatePricings, () => TransacItemsHCertiPricings)
    certificatePricings: CertificatePricings[];
    
    @ApiModelPropertyOptional({ type: UserMembershipActivationPaymentsModel, isArray: true })
    @BelongsToMany(() => UserMembershipActivationPaymentsModel, () => UserMemActPaymHTranItems)
    userMembershipActivationPayments: UserMembershipActivationPaymentsModel[];
    
    @ApiModelPropertyOptional()
    @BelongsToMany(() => Events, () => TransactionsItemsHasEvents)
    events: Events[];
    
    @ApiModelPropertyOptional()
    @BelongsToMany(() => MembershipPricingsModel, () => TransactItemsHMemberPricings)
    membershipPricings: MembershipPricingsModel[];

    @ApiModelPropertyOptional()
    @BelongsToMany(() => CoursesModel, () => TransactionCoursesModel)
    courses: CoursesModel[];

    @CreatedAt created_at: Date;
    @UpdatedAt updated_at: Date;

}