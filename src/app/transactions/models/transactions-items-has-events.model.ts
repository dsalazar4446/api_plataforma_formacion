import { Table, Model, DataType, Column, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { TransactionItemsModel } from './transaction-items.model';
import { Events } from '../../events/models/events.model';

@Table({
    tableName: 'transactions_items_has_events',
})
export class TransactionsItemsHasEvents extends Model<TransactionsItemsHasEvents>{

    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
        validate: {
            notEmpty: true,
        },
    })
    id: string;

    @ForeignKey(() => TransactionItemsModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'transaction_item_id',
    })
    transactionItemId: string;

    @BelongsTo(() => TransactionItemsModel)
    transactionItems: TransactionItemsModel;
    
    @ForeignKey(() => Events)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'event_id',
    })
    eventId: string;

    @BelongsTo(() => Events)
    events: Events;

    @CreatedAt public created_at: Date;
    @UpdatedAt public updated_at: Date;


}