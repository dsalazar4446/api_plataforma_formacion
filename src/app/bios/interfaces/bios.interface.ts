import { ApiModelProperty } from '@nestjs/swagger';
import {IsInt,IsBoolean, IsString, IsEnum, IsEmail,IsNotEmpty, IsUUID} from "class-validator";
export class BiosSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    userId: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    bio: string;
    @ApiModelProperty({ enum: ['es','en','it','pr']})
    @IsString()
    @IsNotEmpty()
    languajeType: string;
}
// tslint:disable-next-line: max-classes-per-file
export class BiosUpdateJson extends BiosSaveJson {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsUUID('4')
    id: string;
}
