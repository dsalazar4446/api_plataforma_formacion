import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { BiosController } from './controllers/bios.controller';
import { BiosService } from './services/bios.service';
import { BiosModel } from './models/bios.model';
import { DatabaseModule } from '../database/database.module';
import { SharedModule } from '../shared/shared.module';
const models = [
  BiosModel,
];
@Module({
  imports: [
    SharedModule,
    HttpModule,
     DatabaseModule.forRoot({
        sequelize: {
            models,
        },
        loki: true,
    }),
  ],
  controllers: [BiosController],
  providers: [BiosService]
})
export class BiosModule {}
