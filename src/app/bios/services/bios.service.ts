import { Injectable, Inject } from '@nestjs/common';
import { BiosSaveJson, BiosUpdateJson } from '../interfaces/bios.interface';
import { BiosModel } from '../models/bios.model';
import { Sequelize } from 'sequelize-typescript';
const Op = Sequelize.Op;
@Injectable()
export class BiosService {
    constructor(
        @Inject('BiosModel') private readonly biosModel: typeof BiosModel,
    ) { }

    async findAll(limit: number, page: number): Promise<any> {
        const tagsFilters = ['language_type'];
        const counter = await this.biosModel.findAndCountAll();
        const pages = Math.ceil(counter.count / limit);
        const offset = limit * (page - 1);
        const bios = await this.biosModel.findAll({
            attributes: tagsFilters,
            limit,
            offset,
            order: [
                ['language_type', 'ASC'],
                ['bio', 'ASC'],
            ],
        });
        const dataResult = {
            result: bios,
            count: counter.count,
            pages,
        };
        return dataResult;
    }

    async findById(id: string): Promise<BiosModel> {
        return await this.biosModel.findById<BiosModel>(id);
    }

    async create(bio: BiosSaveJson): Promise<BiosModel> {
        return await this.biosModel.create<BiosModel>(bio, {
            returning: true,
        });
    }

    async update(idBios: string, biosUpdate: Partial<BiosUpdateJson>){
        return  await this.biosModel.update(biosUpdate, {
            where: {
                id: idBios,
            },
            returning: true,
        });
    }

    async deleted(idBios: string): Promise<any> {
        return await this.biosModel.destroy({
            where: {
                id: idBios,
            },
        });
    }
}
