import { Column, DataType, Table, Model, CreatedAt, UpdatedAt, ForeignKey } from 'sequelize-typescript';
import { DeviceModel } from '../../device/models/device.model';
import { UserModel } from '../../user/models/user.Model';
import { ApiModelProperty } from '@nestjs/swagger';
@Table({
    tableName: 'bios',
})
export class BiosModel extends Model<BiosModel> {
    @ApiModelProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    })
    public id: string;
    @ApiModelProperty()
    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
        field: 'user_id',
    })
    userId: string;
    @ApiModelProperty()
    @Column({
        type: DataType.TEXT,
        allowNull: false,
        field: 'bio',
    })
    bio: string;
    @ApiModelProperty()
    @Column({
        type: DataType.ENUM(
            'es',
            'en',
            'it',
            'pr',
        ),
        allowNull: false,
        validate: {
            notEmpty: true,
        },
        field: 'languaje_type',
    })
    languajeType: string;
}
