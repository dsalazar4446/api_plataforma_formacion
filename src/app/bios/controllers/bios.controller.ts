import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes, UseInterceptors, UseFilters, UseGuards, Inject, Req, HttpStatus} from '@nestjs/common';
import { BiosService } from '../services/bios.service';
import { AppExceptionFilter} from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { AjvValidationPipe } from '../../shared/pipes/ajv-validation.pipe';
import { BiosSaveJson,BiosUpdateJson } from '../interfaces/bios.interface';
import { JwtAuthGuard } from './../../auths/guards/jwt-auth.guard';
import { ApiImplicitParam, ApiImplicitQuery, ApiResponse, ApiImplicitHeader } from '@nestjs/swagger';
import { BiosModel } from '../models/bios.model';
import { UserModel } from '../../user/models/user.Model';
import { AppUtilsService } from '../../shared/services/app-utils.service';

@Controller('bios')
@UseInterceptors(AppResponseInterceptor)
@UseFilters(new AppExceptionFilter())
// @UseGuards(new JwtAuthGuard())

export class BiosController {
    constructor(
        private readonly biosService: BiosService,
        @Inject('UserModel') private readonly userModel: typeof UserModel,
        @Inject('BiosModel') private readonly biosModel: typeof BiosModel,
        private readonly appUtilsService: AppUtilsService,
        ) {}
        // REGISTER SESSION
        @Post()
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        @ApiResponse({
            status: 200,
            type: BiosModel,
          })
        @ApiImplicitQuery({ name: 'languajeType', enum: ['es','en','it','pr'] })
        async create(@Body() bio: BiosSaveJson, @Req() req) {
            const data3 = await this.userModel.findByPk(bio.userId);
            if(data3 == null){
                throw this.appUtilsService.httpCommonError('User does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA3',
                    languageType:  req.headers.language,
                });
            }
            return await this.biosService.create(bio);
        }
        // LIST SESSION

        @ApiResponse({
            status: 200,
            type: BiosModel,
            isArray: true
          })
        @ApiImplicitParam({ name: 'limit', required: true, type: 'number' })
        @ApiImplicitParam({ name: 'page', required: true, type: 'number' })
        @Get(':limit/:page')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async getAll(
            @Param('limit') limit,
            @Param('page') page,
        ) {
            return await this.biosService.findAll(limit, page);
        }
        // DETAIL SESSION
        @ApiResponse({
            status: 200,
            type: BiosModel,
          })
        @ApiResponse({
            status: 200,
            type: BiosModel,
          })
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Get(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async findById(@Param('id') idBio) {
            return await this.biosService.findById(idBio);
        }
        // UPDATE SESSION
        @ApiResponse({
            status: 200,
            type: BiosModel,
          })
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @ApiImplicitQuery({ name: 'languajeType', enum: ['es','en','it','pr'] })
        @Put(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async update(
            @Body()
            updateBio: Partial<BiosUpdateJson>,
            @Param('id') idBio,
            @Req() req,
        ) {
            if(updateBio.userId){
                const data3 = await this.userModel.findByPk(updateBio.userId);
                if(data3 == null){
                    throw this.appUtilsService.httpCommonError('Bio does not created!', HttpStatus.NOT_ACCEPTABLE, {
                        messageCode: 'EA3',
                        languageType:  req.headers.language,
                    });
                }
            }
            const data2 = await this.biosModel.findByPk(idBio);
            if(data2 == null){
                throw this.appUtilsService.httpCommonError('Bio does not created!', HttpStatus.NOT_ACCEPTABLE, {
                    messageCode: 'EA7',
                    languageType:  req.headers.language,
                });
            }
            return await this.biosService.update(idBio, updateBio);
        }
        // DELETE SESSION
        @ApiImplicitParam({ name: 'id', required: true, type: 'string' })
        @Delete(':id')
        @ApiImplicitHeader({
            name:'language',
            required: true,
        })
        async delete(@Param('id') idBio) {
            return await this.biosService.deleted(idBio);
        }
}
