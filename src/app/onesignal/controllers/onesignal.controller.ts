import { Controller, UseFilters, UseInterceptors, Post, UsePipes, Body, HttpStatus, Get, Param, NotFoundException, Put, Delete, Query, Res } from "@nestjs/common";
import { AppExceptionFilter } from '../../shared/filters/app-exception.filter';
import { AppResponseInterceptor } from '../../shared/interceptors/app-response.interceptor';
import { ApiUseTags, ApiImplicitParam, ApiImplicitHeader } from "@nestjs/swagger";
import { OneSignalService } from "../services/onesignal.service";
import { MessageOneSignalJSON, MessageGroupOneSignalJSON } from "../interfaces/onesignal.interface";

@ApiUseTags('Module-Onesignal')
@Controller('onesignal')
@UseFilters(new AppExceptionFilter())
@UseInterceptors(AppResponseInterceptor)
export class OneSignalController {

    constructor(
        private oneSignalService: OneSignalService
    ) { }
    @Post('device/all')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async allDevice(@Body() message: MessageOneSignalJSON) {
        return await this.oneSignalService.allDevice(message);
    }
    @Post('device/group')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async groupDevice(@Body() message: MessageGroupOneSignalJSON) {
        return await this.oneSignalService.allDevice(message);
    }
    @Post('browser/all')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async allBrowser(@Body() message: MessageOneSignalJSON) {
        return await this.oneSignalService.allDevice(message);
    }
    @Post('browser/group')
    @ApiImplicitHeader({
        name:'language',
        required: true,
    })
    async groupBrowser(@Body() message: MessageGroupOneSignalJSON) {
        return await this.oneSignalService.allDevice(message);
    }
}