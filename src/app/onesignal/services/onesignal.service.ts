import { Injectable, HttpService, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { CONFIG } from '../../../config';
import { AxiosResponse } from 'axios';
import { MessageOneSignalJSON, MessageGroupOneSignalJSON } from '../interfaces/onesignal.interface';
@Injectable()
export class OneSignalService {
    constructor(
        private readonly http: HttpService,
    ){}

    async allDevice(message: MessageOneSignalJSON){
        try {
            const url = `https://onesignal.com/api/v1/notifications`;
            const data = {
                'app_id': CONFIG.onesignal.appId,
                'included_segments': ['Active Users', 'Inactive Users'],
                'data': message.data,
                'contents': message.contents,
                'headings': message.headings
            }
            const result = await this.http.post(url, data,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Basic ${CONFIG.onesignal.apiKey}`
                    }
                } ).toPromise();
            return result.data;
        } catch (error) {
            return error;
        }
    }
    async groupDevice(message: MessageGroupOneSignalJSON){
        try {
            const url = `https://onesignal.com/api/v1/notifications`;
            const data = {
                'app_id': CONFIG.onesignal.appId,
                'data': message.data,
                'contents': message.contents,
                'headings': message.headings,
                "include_player_ids": message.includePlayerIds
            }
            const result = await this.http.post(url, data,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Basic ${CONFIG.onesignal.apiKey}`
                    }
                } ).toPromise();
            return result.data;
        } catch (error) {
            return error;
        }
    }

    /*async allBrowser(message: MessageOneSignalJSON){
        try {
            const url = `https://onesignal.com/api/v1/notifications`;
            const data = {
                'app_id': CONFIG.onesignal.appId,
                'contents': message.contents,
                'headings': message.headings,
                'url': message.url,
                'chrome_web_image': message.image
            }
            const result = await this.http.post(url, data,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Basic ${CONFIG.onesignal.apiKey}`
                    }
                } ).toPromise();
            return result.data;
        } catch (error) {
            return error;
        }
    }*/

}
