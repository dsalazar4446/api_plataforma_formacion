import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt,IsBoolean, IsString, IsEmail,IsNotEmpty, IsUUID, IsEnum} from "class-validator";
// tslint:disable-next-line: max-classes-per-file
export class ContentMessageOneSignalJSON{
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    en: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    es: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    it: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    pr: string;
}

// tslint:disable-next-line: max-classes-per-file
export class HeaderMessageOneSignalJSON{
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    en: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    es: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    it: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    pr: string;
}

// tslint:disable-next-line: max-classes-per-file
export class MessageOneSignalJSON{
    @ApiModelProperty()
// tslint:disable-next-line: ban-types
    data?: Object;
    @ApiModelProperty()
    contents: ContentMessageOneSignalJSON;
    @ApiModelProperty()
    headings: HeaderMessageOneSignalJSON;
}



// tslint:disable-next-line: max-classes-per-file
export class MessageGroupOneSignalJSON extends MessageOneSignalJSON{
    @ApiModelProperty()
// tslint:disable-next-line: ban-types
    includePlayerIds: string[];
}


// tslint:disable-next-line: max-classes-per-file
export class MessageBrowserOneSignalJSON{
    @ApiModelProperty()
    contents: ContentMessageOneSignalJSON;
    @ApiModelProperty()
    headings: HeaderMessageOneSignalJSON;
    @ApiModelProperty()
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    // tslint:disable-next-line: ban-types
    url: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    chromeWebImage: string;
}

// tslint:disable-next-line: max-classes-per-file
export class MessageBrowserGroupOneSignalJSON extends MessageBrowserOneSignalJSON{
    @ApiModelProperty()
// tslint:disable-next-line: ban-types
    includePlayerIds: string[];
}