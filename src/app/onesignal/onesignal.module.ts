import { Module, HttpModule, Provider } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import { DatabaseModule } from '../database/database.module';
import { OneSignalService } from './services/onesignal.service';
import { OneSignalController } from './controllers/onesignal.controller';
const models = [
];

const providers: Provider[] = [
  OneSignalService
];

const controllers = [
  OneSignalController
];

@Module({
  imports: [
    SharedModule,
    HttpModule,
    DatabaseModule.forRoot({
      sequelize: {
        models,
      },
      loki: true,
    }),
  ],
  providers,
  controllers
})
export class OneSignalModule { }
