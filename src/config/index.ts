import * as fs from 'fs';
import * as YAML from 'yaml';
import * as _ from 'lodash';

const NODE_ENV = process.env.NODE_ENV;
let defaultConfig = {};

let envConfig = {};

defaultConfig = YAML.parse(fs.readFileSync('config/default.yml', 'utf8'));
if (NODE_ENV) {
  envConfig = YAML.parse(fs.readFileSync(`config/${NODE_ENV}.yml`, 'utf8'));
}
export const CONFIG: any = _.merge(defaultConfig, envConfig);
