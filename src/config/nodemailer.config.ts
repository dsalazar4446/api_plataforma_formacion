import { CONFIG } from '.';

export const nodemailer = {
  host: CONFIG.nodeMailer.host,
  port: CONFIG.nodeMailer.port,
  secure: CONFIG.nodeMailer.secure,
  auth: {
    user: CONFIG.nodeMailer.auth.user,
    pass: CONFIG.nodeMailer.auth.pass,
  },
};
