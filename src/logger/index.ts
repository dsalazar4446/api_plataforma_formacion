import {
  Category,
  CategoryServiceFactory,
  CategoryConfiguration,
  LogLevel,
} from 'typescript-logging';

CategoryServiceFactory.setDefaultConfiguration(
  new CategoryConfiguration(LogLevel.Info),
);

export const APP_LOGGER = new Category('CREEXAPP');
export const CREEX_INIT_LOGGER = new Category('CREEX_INIT', APP_LOGGER);
