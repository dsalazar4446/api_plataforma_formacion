import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import { CONFIG } from './config';
import { APP_LOGGER } from './logger';
import { TranslateService } from './app/shared/services/translate.service';
import { json } from 'body-parser';
import * as express from 'express';
import { DatabaseModule } from './app/database/database.module';
import * as cors from 'cors';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
async function bootstrap() {
  try {
    APP_LOGGER.info(`Creex api is starting...`);
    DatabaseModule._initSequelize();
    const app = await NestFactory.create(AppModule);
    app.useGlobalPipes( new ValidationPipe());
    app.use(cors());
    app.use(json({ limit: '50mb' }));
    app.use(express.static('uploads'));
    app.setGlobalPrefix(CONFIG.server.prefix);
    APP_LOGGER.info('Initializing Sequelize Models...');
    DatabaseModule.sequelizeInstance.addModels(
      Object.values(DatabaseModule.models),
    );
    /******************* SWAGGER  ************************/
    const options = new DocumentBuilder()
    .setTitle('Creex')
    .setDescription('creex API')
    .setVersion('1.0')
    .addTag('creex', 'Super Api Creex')
    .addTag('Roles','Crud de roles')
    .setBasePath('api/v1/')
    .build()
     
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('doc', app, document);

    /***********************************************************/
    APP_LOGGER.info('Sequelize Models initialization has been successfull');
    APP_LOGGER.info('Initializing creex application resources...');
    // Initializing app translation service
    APP_LOGGER.info('Initializing creex translation service resources...');
    const translateService = app.get(TranslateService);
    await translateService._init();
    APP_LOGGER.info('Creex resources initialization has been successfull');
    // Initializing application server
    await app.listen(CONFIG.server.port);
    APP_LOGGER.info(
      `Creex api has been started on port: ${CONFIG.server.port}`,
    );
    // APP_LOGGER.info(
    //   `Creex api initialization tooks: ${Date.now() - startTime.getTime()}ms`,
    // );

  } catch (e) {
    APP_LOGGER.error('Error initializing Creex APP', e);
  }
}
bootstrap();
